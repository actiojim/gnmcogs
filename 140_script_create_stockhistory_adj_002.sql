
set role scire;
SET search_path = public, pg_catalog;


--- Recursive view for stock history
drop view if exists store_stock_history_view;

create view store_stock_history_view as
with recursive prior_total_adjustments as (

	select seq,store_sk,storeproduct_sk,product_gnm_sk,
	date_actual,
	coalesce((select takeon_total from store_stock_history_adj sh inner join store s on sh.store_sk = s.sk and s.acquisition_date = sh.source_staged where sh.storeproduct_sk = storeproduct_sk limit 1),0)
	as takeon_total, 
	dailytotal as cumulative_total, 
	0 as prior_total,
	case when (product_gnm_sk = 0 and date_actual > s.acquisition_date) then dailytotal else 0 end as offcat_total,
	s.acquisition_date
	from daily_adjustments_sum 
	inner join store s on store_sk = s.sk  
	where seq = 1
	
union all

	select ds.seq,ds.store_sk,ds.storeproduct_sk,ds.product_gnm_sk,
	ds.date_actual,
	psm.takeon_total, 
	psm.cumulative_total + ds.dailytotal, 
	psm.cumulative_total::integer as prior_total,
	case when (ds.product_gnm_sk = 0 and ds.date_actual > acquisition_date) then psm.cumulative_total + ds.dailytotal else 0 end as offcat_total,
	acquisition_date
	from prior_total_adjustments psm 
	inner join daily_adjustments_sum ds on 
		ds.store_sk = psm.store_sk and ds.storeproduct_sk = psm.storeproduct_sk and ds.product_gnm_sk = psm.product_gnm_sk
	where psm.seq + 1 = ds.seq
	
)
select seq, store_sk, storeproduct_sk, product_gnm_sk, date_actual, takeon_total, cumulative_total, prior_total, offcat_total from prior_total_adjustments order by date_actual;

--select * from store_stock_history_view v where prior_total > 0;
-- select takeon_total,* from store_stock_history_adj sh inner join store s on sh.store_sk = s.sk and s.acquisition_date = sh.source_staged where sh.storeproduct_sk = storeproduct_sk 
--- backup for now

/*
 * 
select * into backup_store_stock_history_adj from store_stock_history_adj;
select * from  backup_store_stock_history_adj;

truncate table public.store_stock_history_adj;


*/

/*
select count(*) from (
select store_sk,storeproduct_sk, count(*) from store_stock_history_adj group by store_sk,storeproduct_sk 
) x
*/

--- determine product


/*
--
-- Name: store_stock_history_adj_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

drop view if exists view_store_stock_history_takeon;
drop view if exists view_store_stock_history_gnm;


create or replace view view_store_stock_history_takeon as
select st.*, s.name as storename ,s.chainid,s.store_nk from
(
select 
	store_sk, 
	storeproduct_sk, 
	ssh.product_gnm_sk, 
	total, prior_total, 
	employee_sk, 
	source_staged, 
	ssh.created,
	sp.productid, sp.type,sp.name,sp.description,sp.frame_model,sp.frame_colour,sp.frame_brand
from  public.store_stock_history_adj ssh 
	inner join  ( select nk as last_nk, store_sk as last_store_sk, storeproduct_sk as last_takeon_storeproduct_sk,max(source_staged) as last_source_staged 
   	from store_stock_history_adj ssh group by nk,store_sk,storeproduct_sk ) ls 
   		on ls.last_store_sk = ssh.store_sk and ls.last_takeon_storeproduct_sk = ssh.storeproduct_sk and ssh.source_staged = ls.last_source_staged
   	left join storeproduct sp on sp.sk = ssh.storeproduct_sk
) st
left join "store" s on s.sk = st.store_sk;


create or replace view view_store_stock_history_gnm as
select st.*, s.name as storename ,s.chainid,s.store_nk from
(
select  
	store_sk, 
	storeproduct_sk, 
	ssh.product_gnm_sk, 
	total, 
	prior_total, 
	employee_sk, 
	source_staged, 
	ssh.created,
	pg.productid, pg.type,pg.name,pg.description,pg.frame_model,pg.frame_colour,pg.frame_brand
from  public.store_stock_history_adj ssh 
	inner join ( select nk as last_nk, store_sk as last_store_sk, product_gnm_sk as last_product_gnm_sk,max(source_staged) as last_source_staged 
   	from store_stock_history_adj ssh group by nk,store_sk,product_gnm_sk ) ls 
   		on ls.last_store_sk = ssh.store_sk and ls.last_product_gnm_sk = ssh.product_gnm_sk and ssh.source_staged = ls.last_source_staged
   	left join product_gnm pg on pg.sk = ssh.product_gnm_sk
) st
left join "store" s on s.sk = st.store_sk;

*/


/*
select * from stage.today_gnm_sunix_vrestock


select "FIRSTPUR","LASTSALE","LASTSALE2","QUANTITY",* from stage.today_gnm_sunix_vframe
where "LASTSALE" is not null
order by "LASTSALE" desc 

*/


