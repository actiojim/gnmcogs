--set role scire;
--SET search_path = public, pg_catalog;

-- begin;

-- rollback
-- commit
--- create stockmovements tables

--
-- Name: adjustment_type; Type: TABLE; Schema: public; Owner: -
-- drop table adjustment_type  cascade ;

CREATE TABLE adjustment_type (
    sk serial NOT NULL,
    adjustment_typeid character varying(250) NOT NULL,
    name character varying(250) DEFAULT 'Other'::character varying NOT NULL,
    othername character varying(250) NOT NULL,
    Primary key(sk)
);


-- select * from adjustment_type


--
-- Name: order_history; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE order_history (
    sk serial NOT NULL,
    store_sk integer NOT NULL,
    customer_sk integer NOT NULL,
    nk character varying(100),
    order_type character varying(100) NOT NULL,
    queue character varying(100),
    status character varying(100),
    source_created timestamp without time zone NOT NULL,
    source_modified timestamp without time zone NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    Primary key(sk)
);


--
-- Name: stock_price_history; Type: TABLE; Schema: public; Owner: -
-- alter table stock_price_history add column end_date timestamp without time zone;


CREATE TABLE stock_price_history (
    sk serial NOT NULL,
    store_sk integer NOT NULL,
    nk character varying(100) NOT NULL,
    storeproduct_sk integer NOT NULL,
    supplier_cost numeric(19,2),
    supplier_cost_gst numeric(19,2),
    actual_cost numeric(19,2),
    actual_cost_gst numeric(19,2),
    supplier_rrp numeric(19,2),
    supplier_rrp_gst numeric(19,2),
    calculated_retail_price numeric(19,2),
    calculated_retail_price_gst numeric(19,2),
    override_retail_price numeric(19,2),
    override_retail_price_gst numeric(19,2),
    retail_price numeric(19,2),
    retail_price_gst numeric(19,2),
    created timestamp without time zone DEFAULT now() NOT NULL,
    end_date timestamp without time zone,
    product_gnm_sk integer,
    Primary key(sk)
);

--
-- Name: store_stock_history; Type: TABLE; Schema: public; Owner: -
--
-- drop table store_stock_history
CREATE TABLE store_stock_history (
    sk serial NOT NULL,
    store_sk integer NOT NULL,
    nk character varying(100) NOT NULL,
    storeproduct_sk integer NOT NULL,
    product_gnm_sk integer NOT NULL,
    total integer NOT NULL,
    prior_total integer NOT NULL,
    reason character varying(20) NOT NULL,
    comments character varying(500),
    employee_sk integer,
    created timestamp without time zone DEFAULT now(),
    Primary key(sk)
);


--
-- Name: store_stockmovement; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE store_stockmovement (
    sk serial NOT NULL,
    store_sk integer NOT NULL,
    nk character varying(100) NOT NULL,
    storeproduct_sk integer NOT NULL,
    adjustmentdate timestamp without time zone,
    stock_price_history_sk integer NOT NULL,
    adjustment integer NOT NULL,
    reason character varying(20) NOT NULL,
    invoice_sk integer,
    customer_sk integer,
    shipping_ref character varying(100),
    shipping_operator character varying(100),
    comments character varying(500),
    employee_sk integer,
    created timestamp without time zone DEFAULT now() NOT NULL,
    adjustmentdate_sk integer,
    product_gnm_sk integer,
    adjustment_type_sk integer DEFAULT 0 NOT NULL,
    actual_cost numeric(19,2),
    actual_cost_value numeric(19,2),
    moving_quantity numeric(19,2),
    moving_actual_cost_value numeric(19,2),
    moving_cost_per_unit numeric(19,2),
    Primary key(sk)
);


--
-- Name: d_date; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE d_date (
    sk serial NOT NULL,
    date_dim_id integer NOT NULL,
    date_actual date NOT NULL,
    epoch bigint NOT NULL,
    day_suffix character varying(4) NOT NULL,
    day_name character varying(9) NOT NULL,
    day_of_week integer NOT NULL,
    day_of_month integer NOT NULL,
    day_of_quarter integer NOT NULL,
    day_of_year integer NOT NULL,
    week_of_month integer NOT NULL,
    week_of_year integer NOT NULL,
    week_of_year_iso character(10) NOT NULL,
    month_actual integer NOT NULL,
    month_name character varying(9) NOT NULL,
    month_name_abbreviated character(3) NOT NULL,
    year_month_name character varying(8) NOT NULL,
    quarter_actual integer NOT NULL,
    quarter_name character varying(9) NOT NULL,
    year_calendar integer NOT NULL,
    year_calendar_name character varying(7) NOT NULL,
    year_financial integer NOT NULL,
    year_financial_name character varying(7) NOT NULL,
    first_day_of_week date NOT NULL,
    last_day_of_week date NOT NULL,
    first_day_of_month date NOT NULL,
    last_day_of_month date NOT NULL,
    first_day_of_quarter date NOT NULL,
    last_day_of_quarter date NOT NULL,
    first_day_of_year date NOT NULL,
    last_day_of_year date NOT NULL,
    yyyymm character(6) NOT NULL,
    yyyyq character(5) NOT NULL,
    weekend_indr boolean NOT NULL,
    Primary key(sk)
);




CREATE TABLE job_history (
    sk serial NOT NULL,
    nk character varying(100),
    order_id character varying(100) NOT NULL,
    order_sk integer NOT NULL,
    job_type character varying(100) NOT NULL,
    product_sk integer NOT NULL,
    supplier_sk integer NOT NULL,
    queue character varying(100),
    status character varying(100),
    
    frame_glazing_action character varying(100),
    zeiss_glazing_action character varying(100),
    
    source_created timestamp without time zone NOT NULL,
    source_modified timestamp without time zone NOT NULL,
    
    created timestamp without time zone DEFAULT now() NOT NULL,
    cancel_type character varying(100),
    Primary key(sk)
);


--
-- Name: product_gnm; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE product_gnm (
    sk serial NOT NULL,
    productid character varying(500),
    type character varying(100),
    datecreated timestamp without time zone,

    name character varying(500),
    description character varying(500),
    retailprice numeric(19,4),
    wholesaleprice numeric(19,4),
    modified timestamp without time zone,
    last_purchase timestamp without time zone,
    last_sale timestamp without time zone,
    quantity integer,
    frame_model character varying(100) DEFAULT ''::character varying,
    frame_colour character varying(100) DEFAULT ''::character varying,
    frame_brand character varying(100) DEFAULT ''::character varying,
    frame_temple numeric(8,2) DEFAULT 0,
    frame_bridge numeric(8,2) DEFAULT 0,
    frame_eye_size numeric(8,2) DEFAULT 0,
    frame_depth numeric(8,2) DEFAULT 0,
    frame_material character varying(100) DEFAULT ''::character varying,
    lens_type character varying(100) DEFAULT ''::character varying,
    lens_refractive_index numeric(19,4) DEFAULT 0,
    lens_stock_grind character varying(10) DEFAULT ''::character varying,
    lens_size numeric(4,2) DEFAULT 0,
    lens_segment_size numeric(4,2) DEFAULT 0,

    brand_name character varying(100),
    consignment bool,
    apn character varying(100),
    supplier_sku character varying (100),
    internal_sku character varying(100),
    distributor character varying(100),
    eyetalk_id character varying(100),

    created timestamp without time zone DEFAULT now() NOT NULL,
    Primary key(sk),
    supplier_sk integer
);

-- drop table if exists store_stock_history_adj cascade;

CREATE TABLE store_stock_history_adj (
    sk serial NOT NULL,
    store_sk integer NOT NULL,
    nk character varying(100),
    storeproduct_sk integer REFERENCES storeproduct(sk),
    total integer NOT NULL,
    prior_total integer NOT NULL,
    employee_sk integer,
    source_staged timestamp without time zone NOT NULL,
    created timestamp without time zone DEFAULT now(),
    product_gnm_sk integer REFERENCES product_gnm(sk),
    primary key(sk)
);

-- drop table stocktake cascade;


CREATE TABLE stocktake
(
    sk serial not null,
	nk text NOT NULL,
    store_sk bigint NOT NULL,
    planned_stocktake_start_date_time timestamp with time zone NOT NULL,
    planned_stocktake_end_date_time timestamp with time zone NOT NULL,
    actual_stocktake_start_date_time timestamp with time zone,
    actual_stocktake_end_date_time timestamp with time zone,
    created_user character varying(255) COLLATE pg_catalog."default" NOT NULL DEFAULT 'system'::character varying,
    created_date_time timestamp without time zone NOT NULL DEFAULT timezone('utc'::text, now()),
    modified_user character varying(255) COLLATE pg_catalog."default" NOT NULL DEFAULT 'system'::character varying,
    modified_date_time timestamp without time zone NOT NULL DEFAULT timezone('utc'::text, now()),
   	created timestamp without time zone NOT NULL DEFAULT timezone('utc'::text, now()),
   	
   	state CHARACTER varying(50),
   	
    CONSTRAINT pk_stocktake PRIMARY KEY (sk),
    CONSTRAINT "fk_stocktake__store_id -> store" FOREIGN KEY (store_sk)
        REFERENCES store (sk) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

-- drop table stocktake_item ;

CREATE TABLE stocktake_item
(
    sk serial not null,
    nk character varying(255) COLLATE pg_catalog."default" NOT NULL DEFAULT 'system'::character varying,
    stocktake_sk integer NOT NULL,
    product_gnm_sk integer REFERENCES product_gnm(sk),
    store_product_sk integer references storeproduct,
    expected_qty integer,
    actual_qty integer,
    diff integer,
    avg_cost_incl_gst numeric(19,2) , 
    
    avg_cost_gst numeric(19,2) ,
    pms_user character varying(25) COLLATE pg_catalog."default" NOT NULL,
    last_purchased_date timestamp without time zone,
    created_user character varying(255) COLLATE pg_catalog."default" NOT NULL DEFAULT 'system'::character varying,
    created_date_time timestamp without time zone NOT NULL DEFAULT timezone('utc'::text, now()),
    modified_user character varying(255) COLLATE pg_catalog."default" NOT NULL DEFAULT 'system'::character varying,
    modified_date_time timestamp without time zone NOT NULL DEFAULT timezone('utc'::text, now()),
    created timestamp without time zone NOT NULL DEFAULT timezone('utc'::text, now()),
    CONSTRAINT pk_stocktake_item PRIMARY KEY (sk),
    CONSTRAINT fk_stocktake_item__stocktake_sk FOREIGN KEY (stocktake_sk)
        REFERENCES stocktake (sk) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

drop table if exists product_gnm_average_cost_history cascade;

CREATE TABLE product_gnm_average_cost_history (
    sk serial NOT NULL,
    
 	storeproduct_sk integer REFERENCES storeproduct(sk),
 	product_gnm_sk integer references product_gnm(sk),
 	store_sk integer references store(sk),
 	
    adjustmentdate timestamp without time zone not null,
    end_date timestamp without time zone,
    	
    store_stockmovement_sk integer references store_stockmovement(sk),
 	quantity integer,
	prior_avg_cost numeric(19,2),
	adjustment integer,
	running_total integer,

    actual_cost numeric(19,2),
    actual_cost_gst numeric(19,2),
    average_cost numeric(19,2),
    average_cost_gst numeric(19,2),
    
    created timestamp without time zone DEFAULT now(),
    primary key(sk)
);

create index on product_gnm_average_cost_history(store_stockmovement_sk);
create index on product_gnm_average_cost_history(storeproduct_sk,adjustmentdate,end_date);
create index on product_gnm_average_cost_history(product_gnm_sk,adjustmentdate,end_date);

drop table if exists store_stock_history_adj cascade;

CREATE TABLE store_stock_history_adj (
    sk serial NOT NULL,
    store_sk integer NOT NULL,
    nk character varying(100),
    
    storeproduct_sk integer REFERENCES storeproduct(sk),
    
    takeon_total integer not null default 0,
    total integer NOT null default 0,
    prior_total integer NOT null default 0,
    offcatalogue_reciepted_total integer not null default 0,
    
    employee_sk integer,
    source_staged timestamp without time zone NOT NULL,
    created timestamp without time zone DEFAULT now(),
    product_gnm_sk integer REFERENCES product_gnm(sk),
    primary key(sk)
);

drop table if exists public.daily_adjustments_sum;

CREATE TABLE public.daily_adjustments_sum
(	
	seq integer,
    store_sk integer,
    storeproduct_sk integer,
    product_gnm_sk integer,
    date_actual date,
    dailytotal bigint,
    primary key (store_sk,storeproduct_sk,product_gnm_sk,date_actual)
);

CREATE TABLE stock_value_adjustments (
    sk serial NOT NULL,
    stock_adjust_value_id bigint,
    nk text,
    stock_adjust_value_entry_id bigint,
    store_product_sk bigint,
    adjusted_average_cost_excl_gst numeric(19,2),
    comment text,
    current_average_cost_excl_gst numeric(19,2),
    current_average_cost_gst numeric(19,2),
    current_average_cost_incl_gst numeric(19,2),
    modified_user character varying(150),
    modified_date_time timestamp without time zone,
    created timestamp without time zone DEFAULT now(),
    transact_ref character varying(150),
    total_stock_on_hand character varying(150),
    store_sk bigint,
    primary key(sk)
);

CREATE TABLE invoice_item_prices (
    sk serial NOT NULL,
    pms_stock_price_history_sk integer,
    gnm_stock_price_history_sk integer,
    adjustment_price_history_sk integer,
    invoice_sk integer NOT NULL,
    invoiceitemid character varying(60) NOT NULL,
    invoice_datetime timestamp without time zone,
    created timestamp without time zone DEFAULT now(),
    primary key(sk)
);


CREATE TABLE adjustment_price_history (
    sk serial NOT NULL,
    storeproduct_sk integer,
    product_gnm_sk integer,
    adjusteddate timestamp without time zone NOT NULL,
    adjusteddate_sk integer,
    actual_cost numeric(19,2),
    actual_cost_value numeric(19,2),
    moving_cost_per_unit numeric(19,2),
    moving_cost_per_unit_gst numeric(19,2),
    created timestamp without time zone DEFAULT now(),
    primary key(sk)
);

-- drop table daily_adjustments_sum

CREATE TABLE daily_adjustments_sum (
    sk serial NOT NULL,
    seq integer,
    store_sk integer NOT NULL,
    storeproduct_sk integer NOT NULL,
    product_gnm_sk integer NOT NULL,
    date_actual date NOT NULL,
    dailytotal bigint,
    primary key(sk)
);


---- incremental tracking record used for processing stockmovements
drop table if exists store_stockmovement_checkpoint cascade;

CREATE TABLE store_stockmovement_checkpoint (
    sk serial NOT NULL,
    
 	storeproduct_sk integer REFERENCES storeproduct(sk),
 	product_gnm_sk integer references product_gnm(sk),
 	store_sk integer references store(sk),
 	
    adjustmentdate timestamp without time zone not null,
    end_date timestamp without time zone,
    	
    store_stockmovement_sk integer references store_stockmovement(sk),
 	quantity integer,
	prior_avg_cost numeric(19,2),
	adjustment integer,
	running_total integer,

    actual_cost numeric(19,2),
    actual_cost_gst numeric(19,2),
    average_cost numeric(19,2),
    average_cost_gst numeric(19,2),
    
    created timestamp without time zone DEFAULT now(),
    stock_value_adjustments_sk bigint,
    primary key(sk)
);

---- following indexes are required for performance to be acceptable
----
--CREATE INDEX  ON store_stockmovement USING btree(stocktake_item_sk);
CREATE INDEX  ON store_stockmovement_checkpoint USING btree(storeproduct_sk);
CREATE INDEX  ON store_stockmovement_checkpoint USING btree(store_sk,product_gnm_sk);
CREATE INDEX  ON store_stockmovement_checkpoint USING btree(store_sk,product_gnm_sk,adjustmentdate);
CREATE INDEX  ON store_stockmovement_checkpoint USING btree(storeproduct_sk,adjustmentdate);
CREATE INDEX  ON store_stockmovement_checkpoint (storeproduct_sk,product_gnm_sk,adjustmentdate);

