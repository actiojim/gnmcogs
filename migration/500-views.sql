
CREATE VIEW last_job AS
 SELECT jh.sk,
    jh.nk,
    jh.order_id,
    jh.order_sk,
    jh.job_type,
    jh.product_sk,
    jh.supplier_sk,
    jh.queue,
    jh.status,
    jh.zeiss_glazing_action AS glazing_action,
    jh.source_created,
    jh.source_modified,
    jh.created
   FROM (job_history jh
     LEFT JOIN ( SELECT jh_1.nk,
            max(jh_1.sk) AS max_sk,
            max(jh_1.source_modified) AS max_date,
            count(*) AS count
           FROM job_history jh_1
          GROUP BY jh_1.nk) x ON ((x.max_sk = jh.sk)));


CREATE VIEW store_stock_history_view AS
 WITH RECURSIVE prior_total_adjustments AS (
         SELECT daily_adjustments_sum.seq,
            daily_adjustments_sum.store_sk,
            daily_adjustments_sum.storeproduct_sk,
            daily_adjustments_sum.product_gnm_sk,
            daily_adjustments_sum.date_actual,
            COALESCE(( SELECT sh.takeon_total
                   FROM (store_stock_history_adj sh
                     JOIN store s_1 ON (((sh.store_sk = s_1.sk) AND (s_1.acquisition_date = sh.source_staged))))
                  WHERE (sh.storeproduct_sk = sh.storeproduct_sk)
                 LIMIT 1), 0) AS takeon_total,
            daily_adjustments_sum.dailytotal AS cumulative_total,
            0 AS prior_total,
                CASE
                    WHEN ((daily_adjustments_sum.product_gnm_sk = 0) AND (daily_adjustments_sum.date_actual > s.acquisition_date)) THEN daily_adjustments_sum.dailytotal
                    ELSE (0)::bigint
                END AS offcat_total,
            s.acquisition_date
           FROM (daily_adjustments_sum
             JOIN store s ON ((daily_adjustments_sum.store_sk = s.sk)))
          WHERE (daily_adjustments_sum.seq = 1)
        UNION ALL
         SELECT ds.seq,
            ds.store_sk,
            ds.storeproduct_sk,
            ds.product_gnm_sk,
            ds.date_actual,
            psm.takeon_total,
            (psm.cumulative_total + ds.dailytotal),
            (psm.cumulative_total)::integer AS prior_total,
                CASE
                    WHEN ((ds.product_gnm_sk = 0) AND (ds.date_actual > psm.acquisition_date)) THEN (psm.cumulative_total + ds.dailytotal)
                    ELSE (0)::bigint
                END AS offcat_total,
            psm.acquisition_date
           FROM (prior_total_adjustments psm
             JOIN daily_adjustments_sum ds ON (((ds.store_sk = psm.store_sk) AND (ds.storeproduct_sk = psm.storeproduct_sk) AND (ds.product_gnm_sk = psm.product_gnm_sk))))
          WHERE ((psm.seq + 1) = ds.seq)
        )
 SELECT prior_total_adjustments.seq,
    prior_total_adjustments.store_sk,
    prior_total_adjustments.storeproduct_sk,
    prior_total_adjustments.product_gnm_sk,
    prior_total_adjustments.date_actual,
    prior_total_adjustments.takeon_total,
    prior_total_adjustments.cumulative_total,
    prior_total_adjustments.prior_total,
    prior_total_adjustments.offcat_total
   FROM prior_total_adjustments
  ORDER BY prior_total_adjustments.date_actual;


CREATE VIEW v_customer AS
 SELECT customer.sk,
    customer.createddate AS created_date,
    customer.customerid AS customer_id,
    customer.firstname AS first_name,
    customer.lastname AS last_name,
    customer.middlename AS middle_name,
    customer.title,
    customer.gender,
    customer.birthdate AS birth_date,
    customer.preferredname AS preferred_name,
    customer.preferredcontactmethod AS preferred_contact_method,
    customer.email,
    customer.emailcreated AS email_created,
    customer.phonecountryid AS phone_country_id,
    customer.phonecreated AS phone_created,
    customer.address1,
    customer.address2,
    customer.address3,
    customer.state,
    customer.suburb,
    customer.postcode,
    customer.firstvisit AS first_visit,
    customer.lastvisit AS last_visit,
    customer.lastconsult AS last_consult,
    customer.lastrecall AS last_recall,
    customer.nextrecall AS next_recall,
    customer.lastrecalldesc AS last_recall_desc,
    customer.nextrecalldesc AS next_recall_desc,
    customer.lastoptomseen AS last_optom_seen,
    customer.storesk AS store_sk,
    customer.healthfund AS health_fund,
    customer.modified,
    customer.optout_recall AS opt_out_recall,
    customer.optout_marketing AS opt_out_marketing,
    customer.deceased,
    customer.homephone AS home_phone,
    customer.mobile,
    customer.workphone AS work_phone,
    customer.isaddrvalid AS is_addr_valid
   FROM customer;


--
-- Name: v_employee; Type: VIEW; Schema: new_scire; Owner: -
--

CREATE VIEW v_employee AS
 SELECT employee.sk,
    employee.createddate AS created_date,
    employee.employeeid AS employee_id,
    employee.employeetype AS employee_type,
    employee.employmentstartdate AS employment_start_date,
    employee.employmentenddate AS employment_end_date,
    employee.firstname AS first_name,
    employee.lastname AS last_name,
    employee.middlename AS middle_name,
    employee.title,
    employee.gender,
    employee.birthdate AS birth_date,
    employee.preferredname AS preferred_name,
    employee.preferredcontactmethod AS preferred_contact_method,
    employee.email,
    employee.emailcreated AS email_created,
    employee.phonecountryid AS phone_country_id,
    employee.phonenumber AS phone_number,
    employee.phonerawnumber AS phone_raw_number,
    employee.phonemobile AS phone_mobile,
    employee.phonesmsenabled AS phone_sms_enabled,
    employee.phonefax AS phone_fax,
    employee.phonelandline AS phone_landline,
    employee.phonecreated AS phone_created,
    employee.storesk AS store_sk
   FROM employee;


--
-- Name: v_invoice; Type: VIEW; Schema: new_scire; Owner: -
--

CREATE VIEW v_invoice AS
 SELECT invoice.sk,
    invoice.invoiceid AS invoice_id,
    invoice.createddate AS created_date,
    invoice.storesk AS store_sk,
    invoice.employeesk AS employee_sk,
    invoice.customersk AS customer_sk,
    invoice.amount_incl_gst,
    invoice.modified,
    invoice.amount_gst,
    invoice.reversalref AS reversal_ref,
    invoice.rebateref AS rebate_ref,
    invoice.is_writeoff_invoice AS is_write_off_invoice,
    invoice.writeoffref AS write_off_ref,
    invoice.isdeleted AS is_deleted
   FROM invoice;


--
-- Name: v_product_gnm; Type: VIEW; Schema: new_scire; Owner: -
--

CREATE VIEW v_product_gnm AS
 SELECT product_gnm.sk,
    product_gnm.productid AS product_id,
    product_gnm.type,
    product_gnm.datecreated AS date_created,
    product_gnm.name,
    product_gnm.description,
    product_gnm.retailprice AS retail_price,
    product_gnm.wholesaleprice AS wholesale_price,
    product_gnm.modified,
    product_gnm.last_purchase,
    product_gnm.last_sale,
    product_gnm.quantity,
    product_gnm.frame_model,
    product_gnm.frame_colour,
    product_gnm.frame_brand,
    product_gnm.frame_temple,
    product_gnm.frame_bridge,
    product_gnm.frame_eye_size,
    product_gnm.frame_depth,
    product_gnm.frame_material,
    product_gnm.lens_type,
    product_gnm.lens_refractive_index,
    product_gnm.lens_stock_grind,
    product_gnm.lens_size,
    product_gnm.lens_segment_size,
    product_gnm.brand_name,
    product_gnm.consignment,
    product_gnm.apn,
    product_gnm.supplier_sku,
    product_gnm.internal_sku,
    product_gnm.distributor,
    product_gnm.eyetalk_id,
    product_gnm.created
   FROM product_gnm;


--
-- Name: v_product_gnm_average_cost_history; Type: VIEW; Schema: new_scire; Owner: -
--

CREATE VIEW v_product_gnm_average_cost_history AS
 SELECT product_gnm_average_cost_history.sk,
    product_gnm_average_cost_history.storeproduct_sk AS store_product_sk,
    product_gnm_average_cost_history.product_gnm_sk,
    product_gnm_average_cost_history.store_sk,
    product_gnm_average_cost_history.adjustmentdate AS adjustment_date,
    product_gnm_average_cost_history.end_date,
    product_gnm_average_cost_history.store_stockmovement_sk AS store_stock_movement_sk,
    product_gnm_average_cost_history.quantity,
    product_gnm_average_cost_history.prior_avg_cost,
    product_gnm_average_cost_history.adjustment,
    product_gnm_average_cost_history.running_total,
    product_gnm_average_cost_history.actual_cost,
    product_gnm_average_cost_history.actual_cost_gst,
    product_gnm_average_cost_history.average_cost,
    product_gnm_average_cost_history.average_cost_gst,
    product_gnm_average_cost_history.created
   FROM product_gnm_average_cost_history;


--
-- Name: v_stocktake; Type: VIEW; Schema: new_scire; Owner: -
--

CREATE VIEW v_stocktake AS
 SELECT stocktake.sk,
    stocktake.nk,
    stocktake.store_sk,
    stocktake.planned_stocktake_start_date_time,
    stocktake.planned_stocktake_end_date_time,
    stocktake.actual_stocktake_start_date_time,
    stocktake.actual_stocktake_end_date_time,
    stocktake.created_user,
    stocktake.created_date_time,
    stocktake.modified_user,
    stocktake.modified_date_time,
    stocktake.created,
    stocktake.state
   FROM stocktake;


--
-- Name: v_stocktake_item; Type: VIEW; Schema: new_scire; Owner: -
--

CREATE VIEW v_stocktake_item AS
 SELECT stocktake_item.sk,
    stocktake_item.nk,
    stocktake_item.stocktake_sk,
    stocktake_item.product_gnm_sk,
    stocktake_item.store_product_sk,
    stocktake_item.expected_qty,
    stocktake_item.actual_qty,
    ((stocktake_item.expected_qty > 0) OR (stocktake_item.actual_qty > 0)) AS is_active_qty,
    stocktake_item.avg_cost_incl_gst,
    stocktake_item.avg_cost_gst,
    stocktake_item.pms_user,
    stocktake_item.last_purchased_date,
    stocktake_item.created_user,
    stocktake_item.created_date_time,
    stocktake_item.modified_user,
    stocktake_item.modified_date_time,
    stocktake_item.created,
    stocktake_item.diff
   FROM stocktake_item;


--
-- Name: v_store; Type: VIEW; Schema: new_scire; Owner: -
--

CREATE VIEW v_store AS
 SELECT store.sk,
    store.storeid AS store_id,
    store.name,
    store.chainid AS chain_id,
    store.isactive AS is_active,
    store.pms,
    store.store_nk,
    store.acquisition_date,
    store.nk
   FROM store;


--
-- Name: v_store_product; Type: VIEW; Schema: new_scire; Owner: -
--

CREATE VIEW v_store_product AS
 SELECT sp.sk,
    s.sk AS store_sk,
    sp.productid AS product_id,
    sp.type,
    sp.datecreated AS date_created,
    sp.name,
    sp.description,
    sp.retailprice AS retail_price,
    sp.wholesaleprice AS wholesale_price,
    sp.modified,
    sp.last_purchase,
    sp.last_sale,
    sp.quantity,
    sp.frame_model,
    sp.frame_colour,
    sp.frame_brand,
    sp.frame_temple,
    sp.frame_bridge,
    sp.frame_eye_size,
    sp.frame_depth,
    sp.frame_material,
    sp.lens_type,
    sp.lens_refractive_index,
    sp.lens_stock_grind,
    sp.lens_size,
    sp.lens_segment_size,
    sp.internal_sku,
    sp.distributor,
    sp.product_gnm_sk
   FROM storeproduct sp,
    store s
  WHERE ((s.storeid)::text = (sp.storeid)::text);


--
-- Name: v_store_stock_history_adj; Type: VIEW; Schema: new_scire; Owner: -
--

CREATE VIEW v_store_stock_history_adj AS
 SELECT store_stock_history_adj.sk,
    store_stock_history_adj.store_sk,
    store_stock_history_adj.nk,
    store_stock_history_adj.storeproduct_sk AS store_product_sk,
    store_stock_history_adj.takeon_total AS take_on_total,
    store_stock_history_adj.total,
    store_stock_history_adj.prior_total,
    store_stock_history_adj.offcatalogue_reciepted_total AS off_catalogue_receipted_total,
    GREATEST(0, (store_stock_history_adj.total - store_stock_history_adj.offcatalogue_reciepted_total)) AS take_on_diminishing_total,
    store_stock_history_adj.employee_sk,
    store_stock_history_adj.source_staged,
    store_stock_history_adj.created,
    store_stock_history_adj.product_gnm_sk
   FROM store_stock_history_adj;


--
-- Name: v_store_stock_latest; Type: MATERIALIZED VIEW; Schema: new_scire; Owner: -
--

CREATE MATERIALIZED VIEW v_store_stock_latest AS
 SELECT sp.sk AS store_product_sk,
    s.sk AS store_sk,
    pg.sk AS product_gnm_sk,
    ssha.sk AS store_stock_history_adj_sk,
        CASE
            WHEN pg.consignment THEN 'Consignment'::text
            WHEN (sp.product_gnm_sk > 0) THEN 'G&M'::text
            WHEN (ssha.takeon_total > 0) THEN 'Take On'::text
            ELSE 'Off Catalogue'::text
        END AS stock_type
   FROM (((storeproduct sp
     LEFT JOIN store s ON (((s.storeid)::text = (sp.storeid)::text)))
     LEFT JOIN product_gnm pg ON ((pg.sk = sp.product_gnm_sk)))
     LEFT JOIN store_stock_history_adj ssha ON ((ssha.sk = ( SELECT max(ssha2.sk) AS max
           FROM (store_stock_history_adj ssha2
             JOIN store s_1 ON (((s_1.sk = ssha2.store_sk) AND (s_1.acquisition_date < ssha2.source_staged))))
          WHERE (ssha2.storeproduct_sk = sp.sk)))))
  WITH NO DATA;


--
-- Name: v_store_stock_latest_off_cat; Type: VIEW; Schema: new_scire; Owner: -
--

CREATE VIEW v_store_stock_latest_off_cat AS
 SELECT sp.store_product_sk,
    sp.store_sk,
    sp.product_gnm_sk,
    sp.store_stock_history_adj_sk,
    sp.stock_type
   FROM v_store_stock_latest sp
  WHERE (sp.stock_type = ANY (ARRAY['Take On'::text, 'Off Catalogue'::text]));


--
-- Name: v_store_stock_latest_off_cat_old; Type: VIEW; Schema: new_scire; Owner: -
--

CREATE VIEW v_store_stock_latest_off_cat_old AS
 SELECT ssha.sk,
    ssha.store_sk,
    ssha.storeproduct_sk AS store_product_sk,
    ssha.product_gnm_sk,
    ssha.takeon_total AS take_on_total,
    ssha.total,
    ssha.prior_total,
    ssha.offcatalogue_reciepted_total AS off_catalogue_receipted_total,
    GREATEST(0, (ssha.total - ssha.offcatalogue_reciepted_total)) AS take_on_diminishing_total,
    ssha.employee_sk,
    ssha.source_staged,
    ssha.created,
    sp.productid AS product_id,
    sp.type,
    sp.name,
    sp.description,
    sp.frame_model,
    sp.frame_colour,
    sp.frame_brand,
    s.name AS store_name,
    s.chainid AS chain_id,
    s.store_nk
   FROM (((store_stock_history_adj ssha
     JOIN store s ON ((s.sk = ssha.store_sk)))
     JOIN storeproduct sp ON ((sp.sk = ssha.storeproduct_sk)))
     JOIN ( SELECT ssha2.store_sk,
            ssha2.storeproduct_sk,
            max(ssha2.source_staged) AS last_source_staged
           FROM (store_stock_history_adj ssha2
             JOIN store s2 ON (((s2.sk = ssha2.store_sk) AND (s2.acquisition_date < ssha2.source_staged))))
          WHERE (COALESCE(ssha2.product_gnm_sk, 0) = 0)
          GROUP BY ssha2.store_sk, ssha2.storeproduct_sk) ls ON (((ls.store_sk = ssha.store_sk) AND (ls.storeproduct_sk = ssha.storeproduct_sk) AND (ls.last_source_staged = ssha.source_staged))));


--
-- Name: v_store_stock_latest_on_cat; Type: VIEW; Schema: new_scire; Owner: -
--

CREATE VIEW v_store_stock_latest_on_cat AS
 SELECT sp.store_product_sk,
    sp.store_sk,
    sp.product_gnm_sk,
    sp.store_stock_history_adj_sk,
    sp.stock_type
   FROM v_store_stock_latest sp
  WHERE (sp.stock_type = ANY (ARRAY['Consignment'::text, 'G&M'::text]));


--
-- Name: v_store_stock_latest_on_cat_old; Type: VIEW; Schema: new_scire; Owner: -
--

CREATE VIEW v_store_stock_latest_on_cat_old AS
 SELECT ssha.sk,
    ssha.store_sk,
    ssha.storeproduct_sk AS store_product_sk,
    ssha.product_gnm_sk,
    ssha.takeon_total AS take_on_total,
    ssha.total,
    ssha.prior_total,
    ssha.offcatalogue_reciepted_total AS off_catalogue_receipted_total,
    GREATEST(0, (ssha.total - ssha.offcatalogue_reciepted_total)) AS take_on_diminishing_total,
    ssha.employee_sk,
    ssha.source_staged,
    ssha.created,
    pg.productid AS product_id,
    pg.type,
    pg.name,
    pg.description,
    pg.frame_model,
    pg.frame_colour,
    pg.frame_brand,
    s.name AS store_name,
    s.chainid AS chain_id,
    s.store_nk
   FROM (((store_stock_history_adj ssha
     JOIN store s ON ((s.sk = ssha.store_sk)))
     JOIN product_gnm pg ON ((pg.sk = ssha.product_gnm_sk)))
     JOIN ( SELECT ssha2.store_sk,
            ssha2.product_gnm_sk,
            max(ssha2.source_staged) AS last_source_staged
           FROM (store_stock_history_adj ssha2
             JOIN store s2 ON (((s2.sk = ssha2.store_sk) AND (s2.acquisition_date < ssha2.source_staged))))
          WHERE (COALESCE(ssha2.product_gnm_sk, 0) <> 0)
          GROUP BY ssha2.store_sk, ssha2.product_gnm_sk) ls ON (((ls.store_sk = ssha.store_sk) AND (ls.product_gnm_sk = ssha.product_gnm_sk) AND (ls.last_source_staged = ssha.source_staged))));
