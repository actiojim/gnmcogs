
--SET search_path = prod_scire, pg_catalog;
 
-- begin;
-- rollback
-- commit
--
-- Name: adjustment_type sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY adjustment_type ALTER COLUMN sk SET DEFAULT nextval('adjustment_type_sk_seq'::regclass);

--
-- Name: job_history sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY job_history ALTER COLUMN sk SET DEFAULT nextval('job_history_sk_seq'::regclass);

--
-- Name: order_history sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY order_history ALTER COLUMN sk SET DEFAULT nextval('order_history_sk_seq'::regclass);

--
-- Name: stock_price_history sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY stock_price_history ALTER COLUMN sk SET DEFAULT nextval('stock_price_history_sk_seq'::regclass);

--
-- Name: store_stock_history sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stock_history ALTER COLUMN sk SET DEFAULT nextval('store_stock_history_sk_seq'::regclass);

--
-- Name: store_stockmovement sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stockmovement ALTER COLUMN sk SET DEFAULT nextval('store_stockmovement_sk_seq'::regclass);

ALTER TABLE ONLY product_gnm ALTER COLUMN sk SET DEFAULT nextval('product_gnm_sk_seq'::regclass);


ALTER TABLE ONLY d_date ALTER COLUMN sk SET DEFAULT nextval('d_date_sk_seq'::regclass);

  

