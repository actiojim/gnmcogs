-- begin;

-- rollback
-- commit
--SET search_path = public;

--
-- Name: idx_adjustment_type_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX idx_adjustment_type_id ON adjustment_type USING btree (adjustment_typeid);


--
-- Name: idx_adjustment_type_name; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_adjustment_type_name ON adjustment_type USING btree (name);


--
-- Name: idx_d_date_dateactual; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_d_date_dateactual ON d_date USING btree (date_actual);


--
-- Name: idx_d_date_day_of_year; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_d_date_day_of_year ON d_date USING btree (day_of_year);


--
-- Name: idx_d_date_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX idx_d_date_id ON d_date USING btree (date_dim_id);


--
-- Name: idx_d_date_year_calendar; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_d_date_year_calendar ON d_date USING btree (year_calendar);


--
-- Name: idx_invoiceitemwithdate_createddate; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_invoiceitemwithdate_createddate ON invoiceitem USING btree (createddate);


--
-- Name: idx_invoiceitemwithdate_createddate_sk; Type: INDEX; Schema: public; Owner: -
--

-- CREATE INDEX idx_invoiceitemwithdate_createddate_sk ON invoiceitem USING btree (createddate_sk);


--
-- Name: idx_invoiceitemwithdate_customer_sk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_invoiceitemwithdate_customer_sk ON invoiceitem USING btree (customersk);


--
-- Name: idx_invoiceitemwithdate_employee_sk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_invoiceitemwithdate_employee_sk ON invoiceitem USING btree (employeesk);


--
-- Name: idx_invoiceitemwithdate_invoice_sk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_invoiceitemwithdate_invoice_sk ON invoiceitem USING btree (invoicesk);


--
-- Name: idx_invoiceitemwithdate_payee_sk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_invoiceitemwithdate_payee_sk ON invoiceitem USING btree (payeesk);


--
-- Name: idx_invoiceitemwithdate_product_sk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_invoiceitemwithdate_product_sk ON invoiceitem USING btree (productsk);


--
-- Name: idx_invoiceitemwithdate_serviceemployee_sk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_invoiceitemwithdate_serviceemployee_sk ON invoiceitem USING btree (serviceemployeesk);


--
-- Name: idx_invoiceitemwithdate_store_sk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_invoiceitemwithdate_store_sk ON invoiceitem USING btree (storesk);


--
-- Name: idx_model; Type: INDEX; Schema: public; Owner: -
--

-- CREATE INDEX idx_model ON supplierproduct USING btree (frame_model);


--
-- Name: idx_order_history_sourceid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_order_history_sourceid ON order_history USING btree (nk);


--
-- Name: idx_orderid_order_history; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_orderid_order_history ON order_history USING btree (nk);

--
-- Name: idx_stock_price_history_gnmproduct_date; Type: INDEX; Schema: public; Owner: -
--
--- drop index idx_stock_price_history_gnmproduct_date;
CREATE INDEX idx_stock_price_history_gnmproduct_date ON stock_price_history USING btree (product_gnm_sk, created DESC);


--
-- Name: idx_stock_price_history_source_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX idx_stock_price_history_nk ON stock_price_history USING btree (nk);


--
-- Name: idx_stock_price_history_takeon_product_date; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_stock_price_history_takeon_product_date ON stock_price_history USING btree (storeproduct_sk, created DESC);


--
-- Name: idx_store_nk; Type: INDEX; Schema: public; Owner: -
--

-- CREATE UNIQUE INDEX idx_store_nk ON store USING btree (store_nk);


--
-- Name: idx_store_stock_history_storeproduct_date; Type: INDEX; Schema: public; Owner: -
--
-- drop index idx_store_stock_history_storeproduct_date
CREATE INDEX idx_store_stock_history_storeproduct_date ON store_stock_history USING btree (product_gnm_sk, created DESC);


--
-- Name: idx_store_stock_history_takeon_product_date; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_store_stock_history_takeon_product_date ON store_stock_history USING btree (storeproduct_sk, created DESC);


--
-- Name: idx_store_stockmovement_adjustmentdate; Type: INDEX; Schema: public; Owner: -
--
-- drop index idx_store_stockmovement_adjustmentdate;
CREATE INDEX idx_store_stockmovement_adjustmentdate ON store_stockmovement USING btree (product_gnm_sk, adjustmentdate);


--
-- Name: idx_store_stockmovement_adjustmentdate2; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_store_stockmovement_adjustmentdate2 ON store_stockmovement USING btree (storeproduct_sk, adjustmentdate);


--
-- Name: idx_store_stockmovement_adjustmentdate_sk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_store_stockmovement_adjustmentdate_sk ON store_stockmovement USING btree (adjustmentdate_sk);


--
-- Name: idx_store_stockmovement_adjustmenttype_sk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_store_stockmovement_adjustmenttype_sk ON store_stockmovement USING btree (adjustment_type_sk);


--
-- Name: idx_store_stockmovement_invoice_sk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_store_stockmovement_invoice_sk ON store_stockmovement USING btree (invoice_sk);


--
-- Name: idx_store_stockmovement_source_id; Type: INDEX; Schema: public; Owner: -
-- drop index idx_store_stockmovement_nk

CREATE UNIQUE INDEX idx_store_stockmovement_nk ON store_stockmovement USING btree (nk,adjustment_type_sk);


--
-- Name: idx_store_stockmovement_store_sk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_store_stockmovement_store_sk ON store_stockmovement USING btree (store_sk);

--
-- Name: idx_store_stockmovement_storeproduct_sk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_store_stockmovement_storeproduct_sk ON store_stockmovement USING btree (storeproduct_sk);

--
-- Name: productid_on_product; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX productid_on_product ON storeproduct USING btree (productid);

--
-- Name: productid_on_product_gnm; Type: INDEX; Schema: public; Owner: -
--
-- drop index productid_on_product_gnm
CREATE UNIQUE INDEX productid_on_product_gnm ON product_gnm USING btree (productid);

--
-- Name: job_history job_history_product_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--
--- Added primary key ref for product_gnm here as it didn't exist and was causing an error for the foreign key constraint
-- ALTER TABLE product_gnm ADD PRIMARY KEY (sk);


--- alter table job_history drop constraint job_history_product_sk_fkey
ALTER TABLE ONLY job_history
    ADD CONSTRAINT job_history_product_sk_fkey FOREIGN KEY (product_sk) REFERENCES product_gnm(sk);


--
-- Name: job_history job_history_supplier_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY job_history
    ADD CONSTRAINT job_history_supplier_sk_fkey FOREIGN KEY (supplier_sk) REFERENCES supplier(sk);


--
-- Name: order_history order_history_customer_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY order_history
    ADD CONSTRAINT order_history_customer_sk_fkey FOREIGN KEY (customer_sk) REFERENCES customer(sk);


--
-- Name: order_history order_history_store_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY order_history
    ADD CONSTRAINT order_history_store_sk_fkey FOREIGN KEY (store_sk) REFERENCES store(sk);


--
-- Name: stock_price_history stock_price_history_store_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY stock_price_history
    ADD CONSTRAINT stock_price_history_store_sk_fkey FOREIGN KEY (store_sk) REFERENCES store(sk);


--
-- Name: stock_price_history stock_price_history_storeproduct_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY stock_price_history
    ADD CONSTRAINT stock_price_history_storeproduct_sk_fkey FOREIGN KEY (storeproduct_sk) REFERENCES storeproduct(sk);


--
-- Name: store_stock_history store_stock_history_employee_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stock_history
    ADD CONSTRAINT store_stock_history_employee_sk_fkey FOREIGN KEY (employee_sk) REFERENCES employee(sk);


--
-- Name: store_stock_history store_stock_history_store_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stock_history
    ADD CONSTRAINT store_stock_history_store_sk_fkey FOREIGN KEY (store_sk) REFERENCES store(sk);


--
-- Name: store_stock_history store_stock_history_storeproduct_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stock_history
    ADD CONSTRAINT store_stock_history_product_gnm_sk_fkey FOREIGN KEY (product_gnm_sk) REFERENCES product_gnm(sk);


--
-- Name: store_stock_history store_stock_history_storeproduct_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stock_history
    ADD CONSTRAINT store_stock_history_storeproduct_sk_fkey FOREIGN KEY (storeproduct_sk) REFERENCES storeproduct(sk);


--
-- Name: store_stockmovement store_stockmovement_customer_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stockmovement
    ADD CONSTRAINT store_stockmovement_customer_sk_fkey FOREIGN KEY (customer_sk) REFERENCES customer(sk);


--
-- Name: store_stockmovement store_stockmovement_employee_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stockmovement
    ADD CONSTRAINT store_stockmovement_employee_sk_fkey FOREIGN KEY (employee_sk) REFERENCES employee(sk);


--
-- Name: store_stockmovement store_stockmovement_invoice_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stockmovement
    ADD CONSTRAINT store_stockmovement_invoice_sk_fkey FOREIGN KEY (invoice_sk) REFERENCES invoice(sk);


--
-- Name: store_stockmovement store_stockmovement_stock_price_history_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stockmovement
    ADD CONSTRAINT store_stockmovement_stock_price_history_sk_fkey FOREIGN KEY (stock_price_history_sk) REFERENCES stock_price_history(sk);


--
-- Name: store_stockmovement store_stockmovement_store_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stockmovement
    ADD CONSTRAINT store_stockmovement_store_sk_fkey FOREIGN KEY (store_sk) REFERENCES store(sk);


--
-- Name: store_stockmovement store_stockmovement_storeproduct_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stockmovement
    ADD CONSTRAINT store_stockmovement_storeproduct_sk_fkey FOREIGN KEY (storeproduct_sk) REFERENCES storeproduct(sk);


