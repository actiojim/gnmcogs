-- SET search_path = public, pg_catalog;

INSERT INTO store(
	sk, storeid, name, chainid, isactive, pms, store_nk, acquisition_date, nk)
	VALUES (0, '', '', '', false, '', '', '2000-01-01', '0')
    on conflict(sk) do nothing ;

INSERT INTO supplier(
            sk, externalref, supplierid, name)
    VALUES (0, 0, '', '')
    on conflict(sk) do nothing ;

INSERT INTO supplierproduct(
        sk, suppliersku, suppliersk, frame_model, frame_colour, internal_sku, 
        frame_brand)
VALUES (0, '', 0, '', '', '', 
        '')
    on conflict(sk) do nothing ;

INSERT INTO storeproduct(
    sk, storeid, productid, type, datecreated, name, description, 
    retailprice, wholesaleprice, modified, last_purchase, last_sale, 
    quantity, frame_model, frame_colour, frame_brand, frame_temple, 
    frame_bridge, frame_eye_size, frame_depth, frame_material, lens_type, 
    lens_refractive_index, lens_stock_grind, lens_size, lens_segment_size, 
    internal_sku, distributor)
VALUES (0, '', '', '', now(), '', '', 
    0, 0, now(), now(), now(), 
    0, '', '', '', 0, 
    0, 0, 0, '', '', 
    0, '', 0, 0, 
    '', '')
    on conflict(sk) do nothing ;
    
INSERT INTO storeproduct(
    sk, storeid, productid, type, datecreated, name, description, 
    retailprice, wholesaleprice, modified, last_purchase, last_sale, 
    quantity, frame_model, frame_colour, frame_brand, frame_temple, 
    frame_bridge, frame_eye_size, frame_depth, frame_material, lens_type, 
    lens_refractive_index, lens_stock_grind, lens_size, lens_segment_size, 
    internal_sku, distributor)
VALUES (-1, 'ZZZ', 'ZZZ', '', now(), '', '', 
    0, 0, now(), now(), now(), 
    0, '', '', '', 0, 
    0, 0, 0, '', '', 
    0, '', 0, 0, 
    '', '')
    on conflict(sk) do nothing ;
    
--  sk, productid, "type", datecreated, "name", description, retailprice, wholesaleprice, modified, last_purchase, last_sale, quantity, frame_model, 
-- frame_colour, frame_brand, frame_temple, frame_bridge, frame_eye_size, frame_depth, frame_material, lens_type, lens_refractive_index, lens_stock_grind, lens_size, 
-- lens_segment_size, internal_sku, distributor, created

INSERT INTO product_gnm(
    sk, productid, type, datecreated, name, description, 
    retailprice, wholesaleprice, modified, last_purchase, last_sale, 
    quantity, frame_model, frame_colour, frame_brand, frame_temple, 
    frame_bridge, frame_eye_size, frame_depth, frame_material, lens_type, 
    lens_refractive_index, lens_stock_grind, lens_size, lens_segment_size, 
    internal_sku, distributor)
VALUES (0, '', '', now(), '', '', 
    0, 0, now(), now(), now(), 
    0, '', '', '', 0, 
    0, 0, 0, '', '', 
    0, '', 0, 0, 
    '', '')
    on conflict(sk) do nothing ;    


INSERT INTO stock_price_history(
            sk, store_sk, 
            nk, 
           storeproduct_sk, 
 --           storeproduct_sk, 
            supplier_cost, supplier_cost_gst, actual_cost, 
            actual_cost_gst, supplier_rrp, supplier_rrp_gst, calculated_retail_price, 
            calculated_retail_price_gst, override_retail_price, override_retail_price_gst, 
            retail_price, retail_price_gst, created)
    VALUES (0, 0, '', 0, 
 --           0, 
            null, null, null, 
            null, null, null, null, 
            null, null, null, 
            null, null, now())
    on conflict(sk) do nothing ;


INSERT INTO invoice(
            sk, invoiceid, createddate, storesk, employeesk, customersk, 
            amount_incl_gst, modified, amount_gst, reversalref, rebateref)
    VALUES (0, '', now(), 0, 0, 0, 
            null, now(), null, '', '')
    on conflict(sk) do nothing ;
    
            
INSERT INTO customer(
        sk, createddate, customerid, firstname, lastname, middlename, 
        title, gender, birthdate, preferredname, preferredcontactmethod, 
        email, emailcreated, phonecountryid, phonecreated, address1, 
        address2, address3, state, suburb, postcode, firstvisit, lastvisit, 
        lastconsult, lastrecall, nextrecall, lastrecalldesc, nextrecalldesc, 
        lastoptomseen, storesk, healthfund, modified, optout_recall, 
        optout_marketing, deceased, homephone, mobile, workphone)
VALUES (0, now(), '', '', '', '', 
        '', '', now(), '', '', 
        '', now(), 0, now(), '', 
        '', '', '', '', 0, now(), now(), 
        now(), now(), now(), '', '', 
        0, 0, '', now(), false, 
        false, false, '', '', '')
    on conflict(sk) do nothing ;

INSERT INTO employee(
            sk, createddate, employeeid, employeetype, employmentstartdate, 
            employmentenddate, firstname, lastname, middlename, title, gender, 
            birthdate, preferredname, preferredcontactmethod, email, emailcreated, 
            phonecountryid, phonenumber, phonerawnumber, phonemobile, phonesmsenabled, 
            phonefax, phonelandline, phonecreated, storesk)
    VALUES (0, now(), '', '', now(), 
            now(), '', '', '', '', '', 
            now(), '', '', '', now(), 
            0, '', '', false, false, 
            false, false, now(), 0)
    on conflict(sk) do nothing ;
            
update "store" set nk = store_nk;

-------------------------------------------------------------------------------------------

insert into d_date(
date_dim_id,
date_actual,
epoch,
day_suffix,
day_name,
day_of_week,
day_of_month,
day_of_quarter,
day_of_year,
week_of_month,
week_of_year,
week_of_year_iso,
month_actual,
month_name,
month_name_abbreviated,
year_month_name,
quarter_actual,
quarter_name,
year_calendar,
year_calendar_name,
year_financial,
year_financial_name,
first_day_of_week,
last_day_of_week,
first_day_of_month,
last_day_of_month,
first_day_of_quarter,
last_day_of_quarter,
first_day_of_year,
last_day_of_year,
yyyymm,
yyyyq,
weekend_indr
)
SELECT TO_CHAR(datum,'yyyymmdd')::INT AS date_dim_id,
       datum AS date_actual,
       EXTRACT(epoch FROM datum) AS epoch,
       TO_CHAR(datum,'Dth') AS day_suffix,
       TO_CHAR(datum,'Day') AS day_name,
       EXTRACT(isodow FROM datum) AS day_of_week,
       EXTRACT(DAY FROM datum) AS day_of_month,
       datum - DATE_TRUNC('quarter',datum)::DATE +1 AS day_of_quarter,
       EXTRACT(doy FROM datum) AS day_of_year,
       TO_CHAR(datum,'W')::INT AS week_of_month,
       EXTRACT(week FROM datum) AS week_of_year,
       TO_CHAR(datum,'YYYY"-W"IW-D') AS week_of_year_iso,
       EXTRACT(MONTH FROM datum) AS month_actual,
       TO_CHAR(datum,'Month') AS month_name,
       TO_CHAR(datum,'Mon') AS month_name_abbreviated,
       TO_CHAR(datum,'Mon yyyy') AS year_month_name,
       ((EXTRACT(quarter FROM datum)+1)::int%4+1) AS quarter_actual,
       'Q' || ((EXTRACT(quarter FROM datum)+1)::int%4+1) AS quarter_name,
       EXTRACT(isoyear FROM datum) AS year_calendar,
       'CY ' || EXTRACT(isoyear FROM datum) AS year_calendar_name,
       EXTRACT(isoyear FROM datum) + (CASE
         WHEN EXTRACT(quarter FROM datum) >=3 THEN 1 ELSE 0 END) AS year_financial,
       'FY ' || EXTRACT(isoyear FROM datum) + (CASE
         WHEN EXTRACT(quarter FROM datum) >=3 THEN 1 ELSE 0 END) AS year_financial_name,
       datum +(1 -EXTRACT(isodow FROM datum))::INT AS first_day_of_week,
       datum +(7 -EXTRACT(isodow FROM datum))::INT AS last_day_of_week,
       datum +(1 -EXTRACT(DAY FROM datum))::INT AS first_day_of_month,
       (DATE_TRUNC('MONTH',datum) +INTERVAL '1 MONTH - 1 day')::DATE AS last_day_of_month,
       DATE_TRUNC('quarter',datum)::DATE AS first_day_of_quarter,
       (DATE_TRUNC('quarter',datum) +INTERVAL '3 MONTH - 1 day')::DATE AS last_day_of_quarter,
       TO_DATE(EXTRACT(isoyear FROM datum) || '-01-01','YYYY-MM-DD') AS first_day_of_year,
       TO_DATE(EXTRACT(isoyear FROM datum) || '-12-31','YYYY-MM-DD') AS last_day_of_year,
       TO_CHAR(datum,'yyyymm') AS yyyymm,
       TO_CHAR(datum,'yyyy') || ((EXTRACT(quarter FROM datum)+1)::int%4+1) AS yyyyq,
       (CASE
         WHEN EXTRACT(isodow FROM datum) IN (6,7) THEN TRUE
         ELSE FALSE
       END) AS weekend_indr
FROM (SELECT '1970-01-01'::DATE+ SEQUENCE.DAY AS datum
      FROM GENERATE_SERIES (0,29219) AS SEQUENCE (DAY)
      GROUP BY SEQUENCE.DAY) DQ
ORDER BY 1;


--------------------------------------------------------------------------------------------------------
--------

-- select * from employee;
-- select * from customer order by sk;
-- select * from stock_price_history order by sk;
-- select * from storeproduct order by sk;
-- select * from supplier order by sk;
-- select * from "store"
-- select count(*) from store_stockmovement 





        