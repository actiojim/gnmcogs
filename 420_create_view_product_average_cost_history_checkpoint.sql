----------- Create view that calcuates averages, save into a table of cogs averages, update end_date ranges for the table
set role scire;
SET search_path = public, pg_catalog;

drop view if exists view_product_gnm_average_cost_history_checkpoint;

create or replace view view_product_gnm_average_cost_history_checkpoint as
with recursive cogs(
	sk,
	adjustmentdate,
	quantity,
	running_total,
	store_sk,
	product_gnm_sk,
	storeproduct_sk,
	actual_cost,
	actual_cost_gst,
	prior_avg_cost,
	adjustment,
	average_cost,
	average_cost_gst,
	next_sk
) as(
	select sk,
	adjustmentdate,
	quantity,
	running_total,
	store_sk,
	product_gnm_sk,
	storeproduct_sk,
	actual_cost,
	actual_cost_gst,
	prior_avg_cost,
	adjustment,
	average_cost,
	average_cost_gst,
	next_sk
	from (select
			smc.store_stockmovement_sk as sk,
			smc.adjustmentdate,
			smc.quantity,
			smc.running_total,
			smc.store_sk,
			smc.product_gnm_sk,
			smc.storeproduct_sk,
			smc.actual_cost,
			smc.actual_cost_gst,
			smc.prior_avg_cost,
			smc.adjustment,
			smc.average_cost,
			smc.average_cost_gst,
			lead (sm.sk,1) OVER (PARTITION BY sm.storeproduct_sk ORDER by sm.adjustmentdate,sm.sk) next_sk,
			row_number() over (PARTITION BY sm.storeproduct_sk ORDER by sm.adjustmentdate,sm.sk) r
	from 
		store_stockmovement_checkpoint smc 
		inner join (select max(sk) max_sk from store_stockmovement_checkpoint group by storeproduct_sk) smc2 on smc2.max_sk = smc.sk
		left join store_stockmovement sm on sm.storeproduct_sk = smc.storeproduct_sk and sm.adjustmentdate >= smc.adjustmentdate 
		inner join adjustment_type a on a.sk = sm.adjustment_type_sk where a.Name in ('Receipt') or (smc.store_stockmovement_sk = sm.sk)) x where x.r = 1 --and storeproduct_sk = 2076915 
union all
	select
		sm.sk,
		sm.adjustmentdate,
		sm.adjustment as quantity,
		sm.adjustment + c.running_total as running_total,
		sm.store_sk,
		sm.product_gnm_sk,
		sm.storeproduct_sk,
		sp.actual_cost::numeric(19,2),
		sp.actual_cost_gst::numeric(19,2),
		c.average_cost,
		sm.adjustment,
		coalesce( case when (c.running_total) <> 0
			then ( (c.average_cost*( c.running_total - sm.adjustment) + sp.actual_cost*sm.adjustment ) /  c.running_total )::numeric(19,2)
			else sp.actual_cost
		end, sp.actual_cost )
		as average_cost,
		coalesce(case when sm.adjustment + ( c.running_total) <> 0
			then ((c.average_cost_gst*( c.running_total)  + sp.actual_cost_gst*sm.adjustment) / (sm.adjustment + (c.running_total)))::numeric(19,2) 
			else sp.actual_cost_gst
		end, sp.actual_cost_gst ) as average_cost_gst,
		seq.next_sk 
	from cogs c 
	  	inner join store_stockmovement sm on sm.sk = c.next_sk  	
	  	left join stock_price_history sp on sp.product_gnm_sk = sm.product_gnm_sk and sp.created <= sm.adjustmentdate and sp.end_date > sm.adjustmentdate
		inner join (select sm.sk, lead(sm.sk,1) over (partition by storeproduct_sk order by adjustmentdate,sm.sk ) next_sk 
				from store_stockmovement sm inner join adjustment_type a on a.sk = sm.adjustment_type_sk and a.Name in ('Receipt') where sm.product_gnm_sk <> 0  ) seq on seq.sk = sm.sk
	where  sm.product_gnm_sk <> 0 	
) select  *  from cogs

--where storeproduct_sk = 2076915 ;
----  select * from view_product_gnm_average_cost_history_checkpoint where storeproduct_sk = 2076915 ;
--- (c.average_cost * (running_total - adjustment) + actual_cost * adjustment) / running total
/*
select sm.sk, lead(sm.sk,1) over (partition by storeproduct_sk order by adjustmentdate,sm.sk ) next_sk 				
from store_stockmovement sm inner join adjustment_type a on a.sk = sm.adjustment_type_sk 
where a.Name in ('Receipt') and sm.product_gnm_sk <> 0 and storeproduct_sk = 2076915 ;

select * from view_product_gnm_average_cost_history_checkpoint where storeproduct_sk = 2076915 ;
select * from view_product_gnm_average_cost_history_checkpoint2 where storeproduct_sk = 2076915 ;

*/