set role scire;
SET search_path = public, pg_catalog;

/*
already defined in script 050_
-- Create view for the last job
drop view if exists last_job cascade;

create view last_job as
select jh.* from public.job_history jh
right join 
(select nk,max(sk) as max_sk from public.job_history jh group by nk) x
on x.max_sk = jh.sk
--where x.max_date <> jh.source_modified
order by nk;

*/

drop view if exists public.invoice_orders_frame cascade;

CREATE VIEW public.invoice_orders_frame AS
 SELECT "substring"(sp.spectaclejobid::text, 10, length(sp.spectaclejobid::text)) AS pms_orderid,
    oh.sk AS order_sk,
    sp.invoicesk,
    oh.nk AS gnm_order_id,
    sp.framesk,
    sp.right_lenssk,
    sp.left_lenssk,
    sp.storesk,
    sp.pms_invoicenum,
    lj.sk AS job_sk,
    lj.nk AS job_id,
    oh.order_type,
    lj.status,
    stp.sk AS storeproduct_sk,
    lj.product_sk AS job_product_gnm_sk,
    stp.product_gnm_sk AS reconcile_product_gnm_sk,
    'frame'::text AS type,
    coalesce(oh.source_modified,oh.source_created) as order_moddate,
    coalesce(lj.source_modified,lj.source_created) as job_moddate,
    coalesce(pg.consignment,false) consignment,
    lj.glazing_action
    FROM spectaclejob sp
     LEFT JOIN order_history oh ON oh.nk::text = "substring"(sp.spectaclejobid::text, 10, length(sp.spectaclejobid::text))
     LEFT JOIN last_job lj ON lj.order_sk = oh.sk
     LEFT JOIN storeproduct stp ON stp.sk = sp.framesk
     left join product_gnm pg on pg.sk = stp.product_gnm_sk
  WHERE oh.sk IS NOT NULL AND lj.product_sk = stp.product_gnm_sk
UNION ALL
 SELECT "substring"(sp.spectaclejobid::text, 10, length(sp.spectaclejobid::text)) AS pms_orderid,
    oh.sk AS order_sk,
    sp.invoicesk,
    oh.nk AS gnm_order_id,
    sp.framesk,
    sp.right_lenssk,
    sp.left_lenssk,
    sp.storesk,
    sp.pms_invoicenum,
    lj.sk AS job_sk,
    lj.nk AS job_id,
    oh.order_type,
    lj.status,
    stp.sk AS storeproduct_sk,
    lj.product_sk AS job_product_gnm_sk,
    stp.product_gnm_sk AS reconcile_product_gnm_sk,
    'rightlens'::text AS type,
    coalesce(oh.source_modified,oh.source_created) as order_moddate,
    coalesce(lj.source_modified,lj.source_created) as job_moddate,
    false,
    lj.glazing_action
   FROM spectaclejob sp
     LEFT JOIN order_history oh ON oh.nk::text = "substring"(sp.spectaclejobid::text, 10, length(sp.spectaclejobid::text))
     LEFT JOIN last_job lj ON lj.order_sk = oh.sk
     LEFT JOIN storeproduct stp ON stp.sk = sp.right_lenssk
  WHERE oh.sk IS NOT NULL AND lj.product_sk = stp.product_gnm_sk
UNION ALL
 SELECT "substring"(sp.spectaclejobid::text, 10, length(sp.spectaclejobid::text)) AS pms_orderid,
    oh.sk AS order_sk,
    sp.invoicesk,
    oh.nk AS gnm_order_id,
    sp.framesk,
    sp.right_lenssk,
    sp.left_lenssk,
    sp.storesk,
    sp.pms_invoicenum,
    lj.sk AS job_sk,
    lj.nk AS job_id,
    oh.order_type,
    lj.status,
    stp.sk AS storeproduct_sk,
    lj.product_sk AS job_product_gnm_sk,
    stp.product_gnm_sk AS reconcile_product_gnm_sk,
    'leftlens'::text AS type,
    coalesce(oh.source_modified,oh.source_created) as order_moddate,
    coalesce(lj.source_modified,lj.source_created) as job_moddate,
    false,
    lj.glazing_action
   FROM spectaclejob sp
     LEFT JOIN order_history oh ON oh.nk::text = "substring"(sp.spectaclejobid::text, 10, length(sp.spectaclejobid::text))
     LEFT JOIN last_job lj ON lj.order_sk = oh.sk
     LEFT JOIN storeproduct stp ON stp.sk = sp.left_lenssk
  WHERE oh.sk IS NOT NULL AND lj.product_sk = stp.product_gnm_sk;




