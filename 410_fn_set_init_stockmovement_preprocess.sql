set role scire;
SET search_path = public, pg_catalog;

drop function fn_set_init_stockmovement_preprocess();

create function fn_set_init_stockmovement_preprocess() returns integer as $$

	--- init the stockmovements checkpoint table such that it has initial record to process stockmovements from
	--- Starting checkpoint for adjustments
	insert into store_stockmovement_checkpoint (   
    store_stockmovement_sk,
	adjustmentdate,
	quantity,
	running_total,
	store_sk,
 	product_gnm_sk,
 	storeproduct_sk,
    actual_cost , 
	prior_avg_cost, 
    actual_cost_gst,
	adjustment,
    average_cost, 
    average_cost_gst
    )
	select
		sm.sk,
		sm.adjustmentdate,
		sm.adjustment quantity,
		sm.adjustment running_total,
		sm.store_sk,
		sm.product_gnm_sk,
		sm.storeproduct_sk,
		sp.actual_cost::numeric(19,2),
		sp.actual_cost::numeric(19,2) as prior_cost,
		sp.actual_cost_gst::numeric(19,2),
		sm.adjustment,
		sp.actual_cost::numeric(19,2) as average_cost,
		sp.actual_cost_gst::numeric(19,2) as average_cost_gst
	from 
		(select sm.sk,row_number() over (partition by sm.storeproduct_sk,sm.product_gnm_sk order by sm.adjustmentdate,sm.sk ) r ,
			lead (sm.sk,1) OVER (PARTITION BY sm.storeproduct_sk,product_gnm_sk ORDER by sm.adjustmentdate)	as next_sk
			from store_stockmovement sm  where sm.adjustment_type_sk in (select sk from adjustment_type a where a.Name in ('Receipt'))
			) seq
		inner join store_stockmovement sm on seq.sk = sm.sk and sm.product_gnm_sk <> 0
		inner join adjustment_type a on a.sk = sm.adjustment_type_sk  and a.Name in ('Receipt')  
		left join  -- get the base price by date
			stock_price_history sp on sp.product_gnm_sk = sm.product_gnm_sk and sp.created <= sm.adjustmentdate and sp.end_date > sm.adjustmentdate
		left join store_stockmovement_checkpoint smc on smc.storeproduct_sk = sm.storeproduct_sk
	where seq.r = 1 and smc.sk is null 	;

	--- initial condition for picking up stocktakes to initialise a mac
	insert into store_stockmovement_checkpoint (   
    store_stockmovement_sk,
	adjustmentdate,
	quantity,
	running_total,
	store_sk,
 	product_gnm_sk,
 	storeproduct_sk,
    actual_cost , 
	prior_avg_cost, 
    actual_cost_gst,
	adjustment,
    average_cost, 
    average_cost_gst
    )
	select
		sm.sk,
		sm.adjustmentdate,
		sm.adjustment quantity,
		0 running_total,
		sm.store_sk,
		sm.product_gnm_sk,
		sm.storeproduct_sk,
		sp.actual_cost::numeric(19,2),
		sp.actual_cost::numeric(19,2) as prior_cost,
		sp.actual_cost_gst::numeric(19,2),
		sm.adjustment,
		sp.actual_cost::numeric(19,2) as average_cost,
		sp.actual_cost_gst::numeric(19,2) as average_cost_gst
	from 
		(select sm.sk,row_number() over (partition by sm.storeproduct_sk,sm.product_gnm_sk order by sm.adjustmentdate,sm.sk ) r ,
			lead (sm.sk,1) OVER (PARTITION BY sm.storeproduct_sk,product_gnm_sk ORDER by sm.adjustmentdate)	as next_sk
			from store_stockmovement sm  where sm.adjustment_type_sk in (select sk from adjustment_type a where a.Name in ('Stocktake Receipt'))
				) seq
		inner join store_stockmovement sm on seq.sk = sm.sk and sm.product_gnm_sk <> 0
		inner join adjustment_type a on a.sk = sm.adjustment_type_sk  and a.Name in ('Stocktake Receipt')  
		left join  -- get the base price by date
			stock_price_history sp on sp.product_gnm_sk = sm.product_gnm_sk and sp.created <= sm.adjustmentdate and sp.end_date > sm.adjustmentdate
		left join store_stockmovement_checkpoint smc on smc.storeproduct_sk = sm.storeproduct_sk and smc.adjustmentdate < sm.adjustmentdate
	where seq.r = 1 and smc.sk is null 	;
	
	------------- pickup the last average for processing
	insert into store_stockmovement_checkpoint (            
		    storeproduct_sk,
		    product_gnm_sk,
		    store_sk,
		   adjustmentdate,
		   store_stockmovement_sk,
		    quantity,
		    running_total,
		    prior_avg_cost, 
		    adjustment,
		   actual_cost ,
		   actual_cost_gst,
		   average_cost,
		   average_cost_gst)	
	select 
	    	ach.storeproduct_sk,
		    ach.product_gnm_sk,
		    ach.store_sk,
		   ach.adjustmentdate,
		   ach.store_stockmovement_sk,
		    ach.quantity,
		    ach.running_total,
		    ach.prior_avg_cost, 
		    ach.adjustment,
		   ach.actual_cost ,
		   ach.actual_cost_gst,
		   ach.average_cost,
		   ach.average_cost_gst
	from product_gnm_average_cost_history ach 
	inner join     
	        (select storeproduct_sk,max(adjustmentdate) max_adjustmentdate from product_gnm_average_cost_history group by storeproduct_sk) x 
	        	on x.storeproduct_sk = ach.storeproduct_sk and ach.adjustmentdate = x.max_adjustmentdate
	left join store_stockmovement_checkpoint smc on smc.store_stockmovement_sk = ach.store_stockmovement_sk 
	where smc.sk is null;

	--
	select 1;
$$ language sql;


--- truncate table store_stockmovement_checkpoint;
-- select fn_set_init_stockmovement_preprocess();

-- select * from  store_stockmovement_checkpoint where storeproduct_sk = 2076915;
