
set role scire;
SET search_path = public, pg_catalog;

drop view if exists public.vview_invoiceitem_cogs;

create view public.vview_invoiceitem_cogs as 
SELECT distinct
	ii.*
	,gnm_order_id
	,job_id
	,order_type
	,status

	,coalesce( ssm.actual_cost,gnmph.actual_cost, ii.cost_incl_gst ) as calc_actual_cost
    ,coalesce( ssm.actual_cost_gst, gnmph.actual_cost_gst,ii.cost_gst ) as calc_actual_cost_gst
    ,coalesce( ssm.average_cost, gnmph.actual_cost,ii.cost_incl_gst ) as calc_cost
    ,coalesce( ssm.average_cost_gst, gnmph.actual_cost_gst,ii.cost_gst ) as calc_cost_gst
    
    ,coalesce(io.consignment, false) consignment
    ,io.glazing_action
FROM public.invoiceitem ii
	inner join invoice i on i.sk = ii.invoicesk
	inner join store s on s.sk = ii.storesk
	inner join storeproduct sp on ii.productsk = sp.sk
	
	left join product_gnm_average_cost_history ssm on ssm.storeproduct_sk = ii.productsk and ii.createddate >= ssm.adjustmentdate and ii.createddate < ssm.end_date
	left join invoice_orders_frame io on io.invoicesk = ii.invoicesk  and io.storeproduct_sk = ii.productsk
	left join view_last_stock_history_price gnmph on  gnmph.product_gnm_sk = sp.product_gnm_sk and ii.createddate >= gnmph.created and ii.createddate < gnmph.end_date 
where  ii.createddate > '2017-07-01';


------------------------------------------------------------------------------------------------------------
--- create a materialzed view for performance comparison
-- drop  view public.view_invoiceitem_cogs;
--drop materialized view public.view_invoiceitem_cogs;

create materialized view public.view_invoiceitem_cogs as 
SELECT distinct
	ii.*
	,gnm_order_id
	,job_id
	,order_type
	,status

	,coalesce( ssm.actual_cost,gnmph.actual_cost, ii.cost_incl_gst ) as calc_actual_cost
    ,coalesce( ssm.actual_cost_gst, gnmph.actual_cost_gst,ii.cost_gst ) as calc_actual_cost_gst
    ,coalesce( ssm.average_cost, gnmph.actual_cost,ii.cost_incl_gst ) as calc_cost
    ,coalesce( ssm.average_cost_gst, gnmph.actual_cost_gst,ii.cost_gst ) as calc_cost_gst
    
    ,coalesce(io.consignment, false) consignment
    ,io.glazing_action
     
FROM public.invoiceitem ii
	inner join invoice i on i.sk = ii.invoicesk
	inner join store s on s.sk = ii.storesk
	inner join storeproduct sp on ii.productsk = sp.sk
	
	left join product_gnm_average_cost_history ssm on ssm.storeproduct_sk = ii.productsk and ii.createddate >= ssm.adjustmentdate and ii.createddate < ssm.end_date
	left join invoice_orders_frame io on io.invoicesk = ii.invoicesk  and io.storeproduct_sk = ii.productsk
	left join view_last_stock_history_price gnmph on  gnmph.product_gnm_sk = sp.product_gnm_sk and ii.createddate >= gnmph.created and ii.createddate < gnmph.end_date 
where  ii.createddate > '2017-07-01';


--------------------------------------------------------------------

drop view if exists view_invoiceitem_cogs_display;

create view public.view_invoiceitem_cogs_display as 
SELECT distinct
	i.invoiceid
	,s.nk storename
	,sp.productid
	,ii.*

	,gnm_order_id
	,job_id
	,order_type
	,status

	,coalesce( ssm.actual_cost,gnmph.actual_cost, ii.cost_incl_gst ) as calc_actual_cost
    ,coalesce( ssm.actual_cost_gst, gnmph.actual_cost_gst,ii.cost_gst ) as calc_actual_cost_gst
    ,coalesce( ssm.average_cost, gnmph.actual_cost,ii.cost_incl_gst ) as calc_cost
    ,coalesce( ssm.average_cost_gst, gnmph.actual_cost_gst,ii.cost_gst ) as calc_cost_gst
    
    ,coalesce(io.consignment, false) consignment
    ,io.glazing_action
     
FROM public.invoiceitem ii
	inner join invoice i on i.sk = ii.invoicesk
	inner join store s on s.sk = ii.storesk
	inner join storeproduct sp on ii.productsk = sp.sk
	
	left join product_gnm_average_cost_history ssm on ssm.storeproduct_sk = ii.productsk and ii.createddate >= ssm.adjustmentdate and ii.createddate < ssm.end_date
	left join invoice_orders_frame io on io.invoicesk = ii.invoicesk  and io.storeproduct_sk = ii.productsk
	
--	left join stock_price_history sph on sph.storeproduct_sk = sp.sk and ii.createddate >= sph.created and ii.createddate < sph.end_date 
	left join view_last_stock_history_price gnmph on  gnmph.product_gnm_sk = sp.product_gnm_sk and ii.createddate >= gnmph.created and ii.createddate < gnmph.end_date 
where  ii.createddate > '2017-07-01'
