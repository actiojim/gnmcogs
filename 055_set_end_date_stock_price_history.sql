
--- Stock price history clean up


--- 
-- set default end date in the future
update stock_price_history set end_date = '2030-01-01' where end_date is null;

-- calculate date range from history
update stock_price_history set end_date = Z.end_date
from (select x.sk,x1.created end_date,x1.product_gnm_sk from 
(select sk,storeproduct_sk, product_gnm_sk,created, row_number() over (partition by product_gnm_sk order by sh.created,sh.sk) r
from stock_price_history sh where product_gnm_sk <> 0) x 
inner join
(select sk,storeproduct_sk, product_gnm_sk,created, row_number() over (partition by product_gnm_sk order by sh.created,sh.sk) r
from stock_price_history sh where product_gnm_sk <> 0) x1 on x.product_gnm_sk = x1.product_gnm_sk and x.r+1 = x1.r
order by x.product_gnm_sk) Z 
where Z.sk = stock_price_history.sk;

