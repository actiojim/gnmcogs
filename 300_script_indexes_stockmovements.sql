
--drop index idx_stock_price_history_gnmproduct_date;

CREATE INDEX idx_stock_price_history_gnmproduct_date
    ON stock_price_history USING btree
    (product_gnm_sk, created DESC, end_date desc)
    TABLESPACE pg_default;
	
--drop index idx_stock_price_history_takeon_product_date;

CREATE INDEX idx_stock_price_history_takeon_product_date
    ON stock_price_history USING btree
    (storeproduct_sk, created DESC, end_date desc)
    TABLESPACE pg_default;
	