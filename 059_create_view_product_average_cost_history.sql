----------- Create view that calcuates averages, save into a table of cogs averages, update end_date ranges for the table
---------------------------------------------------------------------------
--- use a sequence table but using row number to generate it

-- set role postgres;
drop view if exists view_product_gnm_average_cost_history;

create or replace view view_product_gnm_average_cost_history as
with recursive cogs(
	sk,
	adjustmentdate,
	quantity,
	running_total,
	store_sk,
	product_gnm_sk,
	storeproduct_sk,
	actual_cost,
	actual_cost_gst,
	prior_avg_cost,
	adjustment,
	average_cost,
	average_cost_gst,
	next_sk,
	seq
) as(
	select
		sm.sk,
		sm.adjustmentdate,
		1 quantity,
		1 running_total,
		sm.store_sk,
		sm.product_gnm_sk,
		sm.storeproduct_sk,
		sp.actual_cost::numeric(19,2),
		sp.actual_cost_gst::numeric(19,2),
		sp.actual_cost_gst::numeric(19,2),
		0,
		sp.actual_cost::numeric(19,2) as average_cost,
		sp.actual_cost_gst::numeric(19,2) as average_cost_gst,
        seq2.sk as next_sk,
        seq2.r
	from
		(select sm.sk,row_number() over (partition by sm.store_sk,sm.product_gnm_sk order by sm.adjustmentdate,sm.sk ) r 
				from store_stockmovement sm  where sm.adjustment_type_sk in (select sk from adjustment_type a where a.Name in ('Receipt'))
				) seq
		inner join store_stockmovement sm on seq.sk = sm.sk and sm.product_gnm_sk <> 0
		left join (select sk,sm.store_sk,sm.product_gnm_sk,row_number() over (partition by sm.store_sk,sm.product_gnm_sk order by sm.adjustmentdate,sm.sk ) r from store_stockmovement sm ) seq2
			on  seq.r+1 = seq2.r and seq2.product_gnm_sk = sm.product_gnm_sk and seq2.store_sk = sm.store_sk 
		inner join adjustment_type a on a.sk = sm.adjustment_type_sk  and a.Name in ('Receipt')  
		left join  -- get the base price by date
			stock_price_history sp on sp.product_gnm_sk = sm.product_gnm_sk and sp.created <= sm.adjustmentdate and sp.end_date > sm.adjustmentdate
	where seq.r = 1
union all
	select
		sm.sk,
		sm.adjustmentdate,
		sm.adjustment as quantity,
		sm.adjustment + c.running_total as running_total,
		sm.store_sk,
		sm.product_gnm_sk,
		sm.storeproduct_sk,
		sp.actual_cost::numeric(19,2),
		sp.actual_cost_gst::numeric(19,2),
		c.average_cost,
		sm.adjustment,
		case when sm.adjustment + c.running_total <> 0
			then ((c.average_cost*c.running_total + sp.actual_cost*sm.adjustment) / (sm.adjustment + c.running_total))::numeric(19,2)
			else sp.actual_cost
		end
		as average_cost,
		case when sm.adjustment + c.running_total <> 0
			then ((c.average_cost_gst*c.running_total  + sp.actual_cost_gst*sm.adjustment) / (sm.adjustment + c.running_total))::numeric(19,2) 
			else sp.actual_cost_gst
		end
		as average_cost_gst,
		seq2.sk next_sk,
		seq2.r
	from cogs c 
	  	inner join store_stockmovement sm on sm.sk = c.next_sk and sm.product_gnm_sk <> 0 
		left join adjustment_type a on a.sk = sm.adjustment_type_sk and a.Name in ('Receipt') 	
		left join stock_price_history sp on sp.product_gnm_sk = sm.product_gnm_sk and sp.created <= sm.adjustmentdate and sp.end_date > sm.adjustmentdate
		left join 
			(select sk,sm.store_sk,sm.product_gnm_sk,row_number() over (partition by sm.store_sk,sm.product_gnm_sk order by sm.adjustmentdate,sm.sk ) r 
			from store_stockmovement sm ) seq2 on  c.seq+1 = seq2.r and seq2.product_gnm_sk = sm.product_gnm_sk and seq2.store_sk = sm.store_sk 
) select  * 
 from cogs;


-- set role postgres;



/*
 
 alter table store_stockmovement  owner to scire ;
 alter table product_gnm owner to scire;
 
 
 */




