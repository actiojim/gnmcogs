

set role scire;
SET search_path = public, pg_catalog;

drop function fn_process_cogs_averages();

create function fn_process_cogs_averages() returns integer as $$

insert into product_gnm_average_cost_history (   
 	storeproduct_sk,
 	product_gnm_sk,
 	store_sk,
    adjustmentdate,
    store_stockmovement_sk,
 	quantity, running_total,
	prior_avg_cost, adjustment,

    actual_cost , actual_cost_gst,
    average_cost, average_cost_gst
    )
select vah.storeproduct_sk,vah.product_gnm_sk,
	vah.store_sk,
	vah.adjustmentdate,
	vah.sk,
	vah.quantity,vah.running_total,
	vah.prior_avg_cost,vah.adjustment,
	vah.actual_cost,vah.actual_cost_gst,
	vah.average_cost,vah.average_cost_gst
from view_product_gnm_average_cost_history_checkpoint vah 
left join product_gnm_average_cost_history ph on ph.store_stockmovement_sk = vah.sk
where 
	ph.store_stockmovement_sk is null and vah.adjustmentdate is not null;

	--
	select 1;
$$ language sql;