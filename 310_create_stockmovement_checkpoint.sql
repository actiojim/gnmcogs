
---- incremental tracking record used for processing stockmovements
drop table if exists store_stockmovement_checkpoint cascade;

CREATE TABLE store_stockmovement_checkpoint (
    sk serial NOT NULL,
    
 	storeproduct_sk integer REFERENCES storeproduct(sk),
 	product_gnm_sk integer references product_gnm(sk),
 	store_sk integer references store(sk),
 	
    adjustmentdate timestamp without time zone not null,
    end_date timestamp without time zone,
    	
    store_stockmovement_sk integer references store_stockmovement(sk),
 	quantity integer,
	prior_avg_cost numeric(19,2),
	adjustment integer,
	running_total integer,

    actual_cost numeric(19,2),
    actual_cost_gst numeric(19,2),
    average_cost numeric(19,2),
    average_cost_gst numeric(19,2),
    
    created timestamp without time zone DEFAULT now(),
    stock_value_adjustments_sk bigint,
    primary key(sk)
);

---- following indexes are required for performance to be acceptable
----
--CREATE INDEX  ON store_stockmovement USING btree(stocktake_item_sk);
CREATE INDEX  ON store_stockmovement_checkpoint USING btree(storeproduct_sk);
CREATE INDEX  ON store_stockmovement_checkpoint USING btree(store_sk,product_gnm_sk);
CREATE INDEX  ON store_stockmovement_checkpoint USING btree(store_sk,product_gnm_sk,adjustmentdate);
CREATE INDEX  ON store_stockmovement_checkpoint USING btree(storeproduct_sk,adjustmentdate);
CREATE INDEX  ON store_stockmovement_checkpoint (storeproduct_sk,product_gnm_sk,adjustmentdate);

