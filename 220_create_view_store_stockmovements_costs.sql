
set role scire;
SET search_path = public, pg_catalog;
	

-- drop materialized view store_stockmovement_costs;


create materialized view store_stockmovement_costs as
select 
	sm.sk as store_stockmovement_sk, 
	pga.sk as product_gnm_average_cost_history_sk,
	pga.adjustmentdate as avg_adjustmentdate,
	pga.end_date as avg_end_date,
	pga.quantity as avg_quantity,
	pga.adjustment as avg_adjustment,
	pga.running_total as avg_running_total,
	pga.actual_cost as avg_actual_cost,
	pga.actual_cost_gst as avg_actual_cost_gst,
	pga.average_cost,
	pga.average_cost_gst,
	sph.sk as stock_price_history_sk,
	coalesce(sph.store_sk, pga.store_sk,  0) as store_sk,
	coalesce(sph.storeproduct_sk, pga.storeproduct_sk, 0) as storeproduct_sk,
	coalesce(sph.product_gnm_sk, pga.product_gnm_sk, 0) as product_gnm_sk,
	supplier_cost, 
	supplier_cost_gst, 
	sph.actual_cost as history_actual_cost, 
	sph.actual_cost_gst as history_actual_cost_gst, 
	supplier_rrp, 
	supplier_rrp_gst, 
	calculated_retail_price, 
	calculated_retail_price_gst, 
	override_retail_price, 
	override_retail_price_gst, 
	retail_price, 
	retail_price_gst, 
	sph.created as stock_price_history_created, 
	sph.end_date as stock_price_history_end_date
from  store_stockmovement sm
left join stock_price_history sph on sph.storeproduct_sk = sm.storeproduct_sk and sm.adjustmentdate >= sph.created and sm.adjustmentdate < sph.end_date
left join product_gnm_average_cost_history pga on pga.storeproduct_sk = sm.storeproduct_sk and sm.adjustmentdate >= pga.adjustmentdate and sm.adjustmentdate < pga.end_date
where sm.storeproduct_sk <> 0;

create index on store_stockmovement_costs (store_stockmovement_sk);



