set role scire;
SET search_path = public, pg_catalog;

drop function if exists fn_apply_cost_adjustment(in_storeproduct_sk int, 
	in_product_gnm_sk int, 
	in_actual_cost numeric(19,2), 
	in_actual_cost_gst numeric(19,2), 
	startperiod timestamp without time zone, 
	endperiod timestamp without time zone);

create or replace function fn_apply_cost_adjustment(in_storeproduct_sk int, 
	in_product_gnm_sk int, 
	in_actual_cost numeric(19,2), 
	in_actual_cost_gst numeric(19,2), 
	startperiod timestamp without time zone, 
	endperiod timestamp without time zone) returns integer as $do$
begin
	
	if ( in_storeproduct_sk = 0 ) then
		-- storeproduct cannot be zero, must be a valid storeproduct entry associated to a practice
		 RETURN 0;
	elseif ( in_product_gnm_sk = 0 ) THEN
		-- insert new record
		INSERT INTO stock_price_history (
			store_sk, nk, storeproduct_sk, supplier_cost, supplier_cost_gst, actual_cost, actual_cost_gst, 
			supplier_rrp, supplier_rrp_gst, 
			calculated_retail_price, 
			calculated_retail_price_gst, 
			override_retail_price, 
			override_retail_price_gst, 
			retail_price, 
			retail_price_gst, 
			created, 
			product_gnm_sk,
			end_date)
	   select sph.store_sk, 'adjustment-' || now() || sph.nk,
			storeproduct_sk, supplier_cost, supplier_cost_gst, 
			in_actual_cost, in_actual_cost_gst, 
			supplier_rrp, supplier_rrp_gst, calculated_retail_price, 
			calculated_retail_price_gst, override_retail_price, 
			override_retail_price_gst, retail_price, 
			retail_price_gst, startperiod, product_gnm_sk, endperiod
		from stock_price_history sph
		where sph.sk in (select max(sk) from stock_price_history where storeproduct_sk = in_storeproduct_sk 
						group by storeproduct_sk ) and storeproduct_sk = in_storeproduct_sk and product_gnm_sk = in_product_gnm_sk;

		--- WILL NEED TO HANDLE different business logic assumptiosn 
		--- Assumption 1. adjustment extends to max end date
		--- 2. Adjustment is AFTER created date for existing stock_cost_history records and applies to records only AFTER the adjustment
	
		update public.stock_price_history set end_date = Z.end_date
		-- select * 
		from 
		(select x.sk,x1.created end_date,x1.product_gnm_sk 
		from (select sk,storeproduct_sk, product_gnm_sk,created, row_number() over (partition by storeproduct_sk,product_gnm_sk order by sh.created,sh.sk) r
					from stock_price_history sh where
						x.product_gnm_sk = in_product_gnm_sk and in_storeproduct_sk = storeproduct_sk) x 
		inner join (select sk,storeproduct_sk, product_gnm_sk,created, row_number() over (partition by storeproduct_sk,product_gnm_sk order by sh.created,sh.sk) r
					from stock_price_history sh where in_storeproduct_sk = storeproduct_sk) x1
						on x.product_gnm_sk = x1.product_gnm_sk and x.r+1 = x1.r 
						and x.product_gnm_sk = in_product_gnm_sk and x.storeproduct_sk = in_storeproduct_sk
		order by x.product_gnm_sk) Z 
		where Z.sk = stock_price_history.sk;
	ELSE
		insert into store_stockmovement_checkpoint (            
		    storeproduct_sk,
		    product_gnm_sk,
		    store_sk,
		    adjustmentdate,
--		    end_date,
		    store_stockmovement_sk,
		    quantity,
		    running_total,
		    prior_avg_cost, 
		    adjustment,
		   actual_cost,
		   actual_cost_gst,
		   average_cost,
		   average_cost_gst
		   )
		select 
		    sp.sk,
		    sp.product_gnm_sk,
		    s.sk as store_sk,
		    now() as adjustmentdate,
		    null,
		   	coalesce(smc.quantity,0) as quantity,
		    coalesce(smc.running_total,0)  as running_total,
		    smc.prior_avg_cost,
		    0 as adjustment,
		    smc.actual_cost,
		    smc.actual_cost_gst,
		    in_actual_cost,
		    in_actual_cost_gst
		-- select * 
		from storeproduct sp 
		inner join store s on s.storeid = sp.storeid
		left join store_stockmovement_checkpoint smc on smc.storeproduct_sk = sp.sk
		inner join (select max(sk) maxsk 
					from store_stockmovement_checkpoint where storeproduct_sk = in_storeproduct_sk group by storeproduct_sk) x on x.maxsk = smc.sk
		where sp.sk = in_storeproduct_sk;

		--- recalculate end dates
		update public.store_stockmovement_checkpoint set end_date = Z.end_date
		-- select * 
		from 
		(select x.sk,x1.created end_date,x1.product_gnm_sk 
		from (select sk,storeproduct_sk, product_gnm_sk,created, 
				row_number() over (partition by storeproduct_sk,product_gnm_sk order by sh.adjustmentdate,sh.sk) r
					from store_stockmovement_checkpoint sh 
					where
						sh.product_gnm_sk = in_product_gnm_sk and in_storeproduct_sk = sh.storeproduct_sk) x 
		inner join (select sk,storeproduct_sk, product_gnm_sk,created, row_number() over (partition by storeproduct_sk,product_gnm_sk order by sh.adjustmentdate,sh.sk) r
					from store_stockmovement_checkpoint sh where in_storeproduct_sk = storeproduct_sk) x1
						on x.product_gnm_sk = x1.product_gnm_sk and x.r+1 = x1.r 
						and x.product_gnm_sk = in_product_gnm_sk and x.storeproduct_sk = in_storeproduct_sk
		order by x.product_gnm_sk) Z 
		where Z.sk = store_stockmovement_checkpoint.sk;

		update public.store_stockmovement_checkpoint set end_date = '2030-01-01 12:00:00' where end_date is null;
	END IF;
	--
	return 1;
end
$do$ language plpgsql;




