

--set role postgres; 
drop view if exists vstore_stockmovement;

--------------------------------------------------------------------------------------------------
create view vstore_stockmovement as
select vsm.*, a.othername, a."name",a.adjustment_typeid
from store_stockmovement vsm inner join adjustment_type a on a.sk = vsm.adjustment_type_sk;

 ------------------------------------------------
-- drop view last_job cascade;

 
CREATE or replace VIEW last_job AS
 SELECT jh.sk,
    jh.nk,
    jh.order_id,
    jh.order_sk,
    jh.job_type,
    jh.product_sk,
    jh.supplier_sk,
    jh.queue,
    jh.status,
    jh.zeiss_glazing_action glazing_action,
    jh.source_created,
    jh.source_modified,
    jh.created
   FROM (job_history jh
     LEFT JOIN ( SELECT jh_1.nk,
            max(jh_1.sk) AS max_sk,
            max(jh_1.source_modified) AS max_date,
            count(*) AS count
           FROM job_history jh_1
          GROUP BY jh_1.nk) x ON ((x.max_sk = jh.sk)));
