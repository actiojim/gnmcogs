
/*

select * from daily_gnm_totals;

-- select type,count(*) from storeproduct group by type
 -- select date_part('doy'::text,  cast('2017-10-19 00:00:00'as timestamp)),date_part('dow'::text,  cast('2017-10-18 00:00:00'as timestamp))
 
select dsb_store_sk, count(*) from vdaily_invoice where year = 2017 and doy = 292 group by dsb_store_sk order by count(*) ;
select dsb_store_sk, count(*) from vdaily_stockmovement where year = 2017 and doy = 292 group by dsb_store_sk order by count(*) ;
select dsb_store_sk, count(*) from vdaily_job where year = 2017 and doy = 292 group by dsb_store_sk order by count(*) ;

select * from vdaily_stockmovement where year = 2017 and doy = 292 and dsb_store_sk = 7;
select * from vdaily_invoice where year = 2017 and doy = 292 and dsb_store_sk = 7

*/
--------------------------------------------------------------------------------------------------
--- question
-- list of invoices that sell a frame and have stock movement
-- This means they were a sale of floor stock
/*
select count(*) from  (
select ii.storesk,ii.invoiceitemid,ii.cost_gst,a.name,sp.*,sm.*,sh.* from invoiceitem ii 
left join storeproduct sp on sp.sk = ii.productsk
left join store_stockmovement sm on sm.invoice_sk = ii.invoicesk
left join adjustment_type a on a.sk = sm.adjustment_type_sk
left join stock_price_history sh on sh.sk = sm.stock_price_history_sk
where ii.modified > '2017-06-01 00:00:00' and lower(sp.type) = 'frame' and sm.sk is not null  
) x;
*/
select  (now() - interval '1  day' )::date,(now()  );

select count(*) from (
    select * from invoice i where createddate >= (now() - interval '1  day' )::date and createddate <=(now()  )::date
) x;

select count(*) from (
    select * from invoiceitem i where createddate >= (now() - interval '1  day' )::date and createddate <= (now() )::date
) x;


select count(*) from (
    select * from view_invoiceitem_cogs vc  where createddate >= (now() - interval '1  day' )::date and createddate <= (now()  )::date
) y;


select count(*) from (
    select * from vview_invoiceitem_cogs vc  where createddate >= (now() - interval '1  day' )::date and createddate <= (now()  )::date
) y;

select count(*) from (
    select * from invoice_orders_frame vc  where job_moddate >= (now() - interval '1  day' )::date and job_moddate <= (now()  )::date
) y;


select ddate,count(*) from (
    select * from store_stockmovement vc  where adjustmentdate >= (now() - interval '5  day' )::date and adjustmentdate <= (now()  )::date
) y group by adjustmentdate;

/*
select * from store_stockmovement where adjustmentdate < '2017-12-25' order by adjustmentdate desc;

invoice_orders_frame
select count(*) from (
select * from stage.today_gnm_sunix_vrestock where "DATE" >= '2017-12-21' and "DATE" < '2017-12-22' order by "DATE" desc
) zz;



select count(*) from (
select * from product_gnm_average_cost_history order by adjustmentdate desc
) z;



select count(*) from (
select store_sk,storeproduct_sk, count(*) from store_stock_history_adj group by store_sk,storeproduct_sk 
) x;


*/

