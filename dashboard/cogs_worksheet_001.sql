

select * from store_stockmovement

alter table store_stockmovement drop column adjustmenttype;


select * from invoiceitem;

drop view vbyd_invoiceitem;

CREATE VIEW vbyd_invoiceitem AS
 SELECT date_part('year'::text, sm.createddate) AS year,
    date_part('doy'::text, sm.createddate) AS doy,
    date_part('month'::text, sm.createddate) AS month,
    date_part('dow'::text, sm.createddate) AS dow,
    date_part('day'::text, sm.createddate) AS day,
    sm.invoicesk,
    sm.pos
   FROM invoiceitem sm
  GROUP BY (date_part('year'::text, sm.createddate)), (date_part('doy'::text, sm.createddate)), (date_part('month'::text, sm.createddate)), (date_part('dow'::text, sm.createddate)), (date_part('day'::text, sm.createddate)), sm.invoicesk, sm.pos;

  
 select * from product_gnm;
 
 select * from storeproduct;
 
select gp.productid,gp.type,gp.name,tp.internal_sku,tp.productid,tp.type,tp.name,tp.description from  public.product_gnm gp 
left join public.storeproduct tp on  (tp.internal_sku <> '' AND gp.productid = tp.internal_sku)
where tp.sk is not null

.product_gnm_sk is not null and tp.product_gnm_sk <> 0 


 select ss.*, * from product_gnm pg left join  
  (select substring(productid,13, length(productid)) as pid,productid,sk from public.storeproduct where storeid != '0') ss on pg.name = ss.pid
 where ss.pid is not null
 
 
 
 select * from storeproduct where product_gnm_sk is not null and product_gnm_sk <> 0;
 
 select * from store_stockmovement where product_gnm_sk is not null and product_gnm_sk <> 0
 
 
 /*****************
 begin;
 update storeproduct as tp set product_gnm_sk = gp.sk from  public.product_gnm gp 
where tp.internal_sku <> '' AND gp.productid = tp.internal_sku;

 update storeproduct as tp set product_gnm_sk = 0  where product_gnm_sk is null;
commit;

***************/

 select sp.internal_sku,pg.productid from storeproduct sp left join product_gnm pg on pg.sk = sp.product_gnm_sk where pg.sk is not null and pg.sk <>0;
 
 select sp.internal_sku,* from storeproduct sp where  sp.product_gnm_sk is  null;
 
 select sp.internal_sku,* from storeproduct sp where  sp.product_gnm_sk = 0;
 
 -- rollback;
  select count(*) from storeproduct sp;
  
  
select * from pms_invoice_orders

select * from invoiceitem ii 
left join pms_invoice_orders io on io.invoicesk = ii.invoicesk
left join last_job lj on lj.order_sk = io.ordersk
left join order_history oh on oh.sk = io.ordersk
left join product_gnm pg on pg.sk = lj.product_sk and pg.type = 'frame'
left join store_stockmovement ssm on 
  
  select * from invoiceitem ii 
-- left join storeproduct pg on pg.sk = ii.productsk and pg.type = 'frame'
inner join store_stockmovement ssm on ssm.invoice_sk = ii.invoicesk
inner join product_gnm pg on pg.sk = ssm.product_gnm_sk
--inner join spectaclejob sj on sj.invoicesk = ii.invoicesk
where 
	ssm.invoice_sk <> 0
	
	
select * from stage.today_gnm_sunix_vframe

select "INVNO",substring("INVNO",5),count(*) from stage.today_gnm_sunix_vrestock group by "INVNO"

 select count(*) from spectaclejob where invoicesk <> 0 and invoicesk is not null;
 
 select * from spectaclejob where invoicesk <> 0 and invoicesk is not null;
 
 
 1. check price histor is correct
 
 select * from stock_price_history where takeon_storeproduct_sk  <> 0
 
 
 select * from stock_price_history where product_gnm_sk  <> 0
 
 
  select aj.name,count(*) from store_stockmovement ssm 
 inner join adjustment_type aj on aj.sk = ssm.adjustment_type_sk
 --where moving_actual_cost_value is not null 
 
 group by aj.name 
 order by count(*) desc;
 
 select aj.name,count(*) from store_stockmovement ssm 
 inner join adjustment_type aj on aj.sk = ssm.adjustment_type_sk
 where moving_actual_cost_value is not null 
 
 group by aj.name 
 order by count(*) desc
 
 and product_gnm_sk <> 0;
 
 
-------------------------------------------------------------------------------------------------------------------
---- Create Matched event bucket

drop table if exists daily_matched_bucket cascade ;
create table 
daily_matched_bucket (
	sk serial not null,
	invoice_daily_store_bucket_sk integer references daily_store_bucket(sk),
	job_daily_store_bucket_sk integer references daily_store_bucket(sk),
	stockmove_daily_store_bucket_sk integer references daily_store_bucket(sk),
	invoice_sk integer references invoice(sk),
	invoiceitemid varchar(50) ,
	job_sk  integer references job_history(sk),
	store_stockmovement_sk integer references store_stockmovement(sk),
	confidence_stockmovement integer,
	confidence_job integer,
	confidence_job_stockmovement integer
);
----

drop table pms_invoice_orders;

CREATE TABLE pms_invoice_orders (
    orderid text,
    ordersk integer,
    invoicesk integer,
    framesk integer,
    right_lenssk integer,
    left_lenssk integer,
    storesk integer,
    pms_invoicenum character varying(100)
);

select sj.*,spframe.description,sprlens.description,spllens.description from spectaclejob sj
inner join storeproduct spframe on spframe.sk = sj.framesk
inner join storeproduct sprlens on sprlens.sk = sj.right_lenssk
inner join storeproduct spllens on spllens.sk = sj.left_lenssk

insert into pms_invoice_orders 
select substring(spectaclejobid,10, length(spectaclejobid)) as orderid, oh.sk, sp.invoicesk, sp.framesk,sp.right_lenssk,sp.left_lenssk,sp.storesk,sp.pms_invoicenum 
from spectaclejob sp 
left join order_history oh on oh.source_id = substring(spectaclejobid,10, length(spectaclejobid))
LEFT JOIN  pms_invoice_orders pio on pio.orderid = substring(spectaclejobid,10, length(spectaclejobid)) and pio.invoicesk = sp.invoicesk
where pio.orderid is null;

--- populate matched bucket
-- select * from pms_invoice_orders where ordersk is not null and invoicesk is not null;

---- Brute force single match based upon pms_invoice_orders table
/*
select * from invoiceitem ii 
left join pms_invoice_orders io on io.invoicesk = ii.invoicesk
left join last_job lj on lj.order_sk = io.order_sk
left join order_history oh on oh.sk = io.order_sk
left join product_gnm pg on pg.sk = lj.product_sk and pg.type = 'frame'
left join store_stockmovement ssm_pms on ssm_pms.invoice_sk = io.invoice_sk
left join store_stockmovement ssm on ssm.invoice_sk = ii.invoicesk
  
  select * from invoiceitem ii 
-- left join storeproduct pg on pg.sk = ii.productsk and pg.type = 'frame'
inner join store_stockmovement ssm on ssm.invoice_sk = ii.invoicesk
inner join product_gnm pg on pg.sk = ssm.product_gnm_sk
--inner join spectaclejob sj on sj.invoicesk = ii.invoicesk
where 
	ssm.invoice_sk <> 0
*/
-- match gnm products


select count(*) from invoiceitem ii 
inner join pms_invoice_orders pio on pio.invoicesk = ii.invoicesk  -- and pio.framesk = ii.productsk
;


select ii.dsb_sk, j.dsb_sk, sm.dsb_sk, ii.*
from vdaily_invoice ii 
inner join pms_invoice_orders pio on pio.invoicesk = ii.invoicesk   and pio.ordersk is not null and pio.framesk is not null and pio.framesk <> 0-- and pio.framesk = ii.productsk
inner join vdaily_stockmovement sm on sm.invoice_sk = pio.invoicesk and sm.invoice_sk <> 0
inner join vdaily_job j on j.order_sk = pio.ordersk and pio.ordersk is not null and pio.framesk is not null and pio.framesk <> 0
where	
	ii.year = 2017 and ii.year = sm.year and ii.doy = sm.doy and ii.year = j.year and ii.doy = j.doy 
	and 
	ii.dsb_sk = j.dsb_sk and ii.dsb_sk = sm.dsb_sk
	
-----------------------------------------------------------------------------------------------------------------------------------------
------------ BRUTE FORCE JOIN of all events
INSERT INTO public.daily_matched_bucket
(invoice_daily_store_bucket_sk, job_daily_store_bucket_sk, stockmove_daily_store_bucket_sk, 
invoice_sk, invoiceitemid, 
job_sk, 
store_stockmovement_sk, 
confidence_stockmovement, confidence_job, confidence_job_stockmovement)
select ii.dsb_sk as invoice_bucket, j.dsb_sk as job_bucket, sm.dsb_sk as sm_bucket, 
ii.invoicesk, invoiceitemid,
j.sk,
sm.sk,
ABS(ii.doy -  sm.doy), ABS(ii.doy - j.doy), ABS(sm.doy - j.doy)
from vdaily_invoice ii 
inner join vdaily_stockmovement sm on sm.invoice_sk = ii.invoicesk and sm.invoice_sk <> 0
inner join pms_invoice_orders pio on pio.invoicesk = ii.invoicesk 
inner join vdaily_job j on j.order_sk = pio.ordersk and j.supplier_sk <> 61 -- exclude zeiss
inner join adjustment_type adj on adj.sk = sm.adjustment_type_sk
where
	ii.dsb_storeproduct_sk = sm.dsb_storeproduct_sk and ii.dsb_product_gnm_sk = sm.dsb_product_gnm_sk and
	ii.dsb_product_gnm_sk = j.dsb_product_gnm_sk and
	adj.name = 'Sale' and ii.year = 2017 and ii.doy >  160 
	and pio.ordersk is not null and pio.framesk is not null and pio.framesk <> 0

select * from daily_store_bucket dsb where dsb.sk in (2215151,2298518,2215135)
	
select * from job_history where order_id = '350522';

	select * from vdaily_job where sk = 11050
	order_id = '153433'
	
	
select * from job_history where order_id =  '153433'
	
	
	
	
---- display query
	
select 
s.store_nk,
ii.invoiceitemid,
e.employeeid,
ii.createddate,
ii.modified,
sp.description,
sp.productid,
pg.productid,
pg.description,
ii.description,
ii.amount_gst,
ii.amount_incl_gst,
ii.discount_gst,
ii.discount_incl_gst,
ii.cost_incl_gst,
ii.cost_gst,
ii.bulkbillref
-- ii.*
from vdaily_invoice ii 
inner join store s on s.sk = ii.storesk
inner join storeproduct sp on sp.sk = ii.productsk and sp.type = 'Frame'
left join employee e on e.sk = ii.employeesk
left join product_gnm pg on pg.sk = sp.product_gnm_sk
where ii.year = 2017 and ii.doy >  160
order by ii.doy


inner join vdaily_job j on j.order_sk = pio.ordersk
where sm.invoice_sk <> 0 and pio.ordersk is not null and pio.framesk is not null and pio.framesk <> 0
;
select * from vdaily_job j inner join pms_invoice_orders pio on pio.ordersk = j.order_sk
where
	pio.ordersk is not null and pio.framesk is not null and pio.framesk <> 0
	
select * 
from vdaily_invoice ii 
inner join vdaily_job j on ii.invoicesk = j. -- and j.product_sk = pio.framesk

select * 
from vdaily_invoice ii 
inner join vdaily_stockmovement sm on sm.invoice_sk = ii.invoicesk 
and sm.dsb_storeproduct_sk = ii.dsb_storeproduct_sk and sm.dsb_product_gnm_sk = sm.dsb_product_gnm_sk

drop index idx_invoiceitemid_invoiceite

create index if not exists idx_invoiceitemid_invoiceite on invoiceitem(invoicesk,invoiceitemid);

create index if not exists idx_invoice_sk_store_stockmovement on store_stockmovement(invoice_sk);

-- match takeon products

select * from supplier


INSERT INTO public.daily_matched_bucket
(invoice_daily_store_bucket_sk, job_daily_store_bucket_sk, stockmove_daily_store_bucket_sk, 
invoice_sk, invoiceitemid, 
job_sk, 
store_stockmovement_sk, 
confidence_stockmovement, confidence_job, confidence_job_stockmovement)
select ii.dsb_sk as invoice_bucket, j.dsb_sk as job_bucket, sm.dsb_sk as sm_bucket, 
ii.invoicesk, invoiceitemid,
j.sk,
sm.sk,
ABS(ii.doy -  sm.doy), ABS(ii.doy - j.doy), ABS(sm.doy - j.doy)
from vdaily_invoice ii 
inner join vdaily_stockmovement sm on sm.invoice_sk = ii.invoicesk and sm.invoice_sk <> 0
inner join pms_invoice_orders pio on pio.invoicesk = ii.invoicesk 
inner join vdaily_job j on j.order_sk = pio.ordersk and j.supplier_sk <> 61 -- exclude zeiss
inner join adjustment_type adj on adj.sk = sm.adjustment_type_sk
where
	ii.dsb_storeproduct_sk = sm.dsb_storeproduct_sk and ii.dsb_product_gnm_sk = sm.dsb_product_gnm_sk and
	ii.dsb_product_gnm_sk = j.dsb_product_gnm_sk and
	adj.name = 'Sale' and ii.year = 2017 and ii.doy >  160 
	and pio.ordersk is not null and pio.framesk is not null and pio.framesk <> 0
	

	INSERT INTO public.daily_matched_bucket
(invoice_daily_store_bucket_sk, job_daily_store_bucket_sk, stockmove_daily_store_bucket_sk, 
invoice_sk, invoiceitemid, 
job_sk, 
store_stockmovement_sk, 
confidence_stockmovement, confidence_job, confidence_job_stockmovement)
select ii.dsb_sk as invoice_bucket, j.dsb_sk as job_bucket, sm.dsb_sk as sm_bucket, 
ii.invoicesk, invoiceitemid,
j.sk,
sm.sk,
ABS(ii.doy -  sm.doy), ABS(ii.doy - j.doy), ABS(sm.doy - j.doy)
select * from vdaily_invoice ii 
inner join vdaily_stockmovement sm on sm.invoice_sk = ii.invoicesk and sm.invoice_sk <> 0
      
INSERT INTO public.daily_matched_bucket
(invoice_daily_store_bucket_sk, job_daily_store_bucket_sk, stockmove_daily_store_bucket_sk, 
invoice_sk, invoiceitemid, 
job_sk, 
store_stockmovement_sk, 
confidence_stockmovement, confidence_job, confidence_job_stockmovement)
select ii.dsb_sk as invoice_bucket, j.dsb_sk as job_bucket, sm.dsb_sk as sm_bucket, 
ii.invoicesk, invoiceitemid,
coalesce(j.sk,0),
coalesce(sm.sk,0),
ABS(ii.doy -  sm.doy), ABS(ii.doy - j.doy), ABS(sm.doy - j.doy)
from vdaily_invoice ii 
left join vdaily_stockmovement sm on sm.invoice_sk = ii.invoicesk and sm.invoice_sk <> 0
left join pms_invoice_orders pio on pio.invoicesk = ii.invoicesk and pio.ordersk is not null and pio.framesk is not null and pio.framesk <> 0
left join vdaily_job j on j.order_sk = pio.ordersk and j.supplier_sk <> 61 -- exclude zeiss
left join adjustment_type adj on adj.sk = sm.adjustment_type_sk and adj.name = 'Sale' 
where
--	ii.dsb_storeproduct_sk = sm.dsb_storeproduct_sk and ii.dsb_product_gnm_sk = sm.dsb_product_gnm_sk and
--	ii.dsb_product_gnm_sk = j.dsb_product_gnm_sk and
	ii.year = 2017 and ii.doy >  160 
--	and j.dsb_sk  is not null
--	and pio.ordersk is not null and pio.framesk is not null and pio.framesk <> 0
	
------------------------------------------------------------------------------------------------------------------------------
--
--	matching invoice item to stock movement
--

INSERT INTO public.daily_matched_bucket
(invoice_daily_store_bucket_sk, job_daily_store_bucket_sk, stockmove_daily_store_bucket_sk, 
invoice_sk, invoiceitemid, 
job_sk, 
store_stockmovement_sk, 
confidence_stockmovement, confidence_job, confidence_job_stockmovement)
select ii.dsb_sk as invoice_bucket, null, sm.dsb_sk as sm_bucket, 
ii.invoicesk, invoiceitemid,
null,
coalesce(sm.sk,0),
ABS(ii.doy -  sm.doy), null, null
from vdaily_invoice ii 
inner join vdaily_stockmovement sm on sm.invoice_sk = ii.invoicesk and sm.invoice_sk <> 0 
	and sm.dsb_store_sk = ii.dsb_store_sk and sm.dsb_product_gnm_sk = ii.dsb_product_gnm_sk and sm.dsb_storeproduct_sk = ii.dsb_storeproduct_sk
inner join adjustment_type adj on adj.sk = sm.adjustment_type_sk and adj.name = 'Sale' 
where
	ii.year = 2017 and ii.doy >  160 
	

select count(*) from daily_matched_bucket

select dsbi.*,dsbsm.*,dmb.*
from daily_matched_bucket dmb 
inner join daily_store_bucket dsbi on dsbi.sk = dmb.invoice_daily_store_bucket_sk
inner join daily_store_bucket dsbsm on dsbsm.sk = dmb.stockmove_daily_store_bucket_sk
	
------------------------------------------------------------------------------------------------------------------------------
--
--	matching invoice item to orders
--


-- what hub orders do not appear in pms_invoice_orders ?
select j.sk,j.product_sk,pio.invoicesk from last_job j 
full outer join pms_invoice_orders pio on pio.ordersk = j.order_sk
where	
	j.sk is not null and pio.invoicesk is not null and pio.invoicesk <> 0;
	
-- what jobs can be matched with invoices that also have stockmovements
select * from daily_matched_bucket dmb inner join
(select j.sk,j.product_sk,pio.invoicesk from last_job j 
full outer join pms_invoice_orders pio on pio.ordersk = j.order_sk
where	
	j.sk is not null and pio.invoicesk is not null and pio.invoicesk <> 0) x on x.invoicesk = dmb.invoice_sk
	
-- what jobs can be matched directly with invoice items based upon productid
select ABS(j.doy - i.doy),i.dsb_sk,i.year,i.doy,i.dsb_store_sk,i.dsb_product_gnm_sk,i.dsb_storeproduct_sk,
j.dsb_sk,j.year,j.doy,j.dsb_store_sk,j.dsb_product_gnm_sk,j.dsb_storeproduct_sk from vdaily_invoice i 
inner join vdaily_job j on j.dsb_product_gnm_sk = i.dsb_product_gnm_sk 
	and j.dsb_store_sk = i.dsb_store_sk and j.dsb_product_gnm_sk <> 0
	and ABS(j.doy - i.doy) < 1 and i.year = 2017 and i.doy > 150 and i.year = j.year 
order by ABS(j.doy - i.doy)


select ABS(j.doy - i.doy),* from vdaily_invoice i 
inner join vdaily_job j on j.dsb_sk = i.dsb_sk
order by ABS(j.doy - i.doy)

--- which product_gnm buckets don't have a corresponding storeproduct lookup

select * from daily_store_bucket dsb
left join daily_store_bucket dsb2 on dsb2.product_gnm_sk = dsb.product_gnm_sk and dsb.doy = dsb2.doy 
and dsb.year = dsb2.year and dsb.store_sk = dsb2.store_sk

select * from storeproduct



-------------------------------------------------------------------------------------------------------------------------------
----- found internak sku in storeproduct that don't match 
--- zero pad to improve matching

select storeid,description, internal_sku,LPAD(internal_sku,8,'0'),
sp.*
-- count(*) 
from storeproduct sp
where internal_sku is not null and internal_sku <> '' and  LPAD(internal_sku,8,'0') not in (select productid from product_gnm) and storeid  = '2850-026'
group by storeid,description


select storeid,description, internal_sku,
sp.*
-- count(*) 
from storeproduct sp
where internal_sku is not null and internal_sku <> '' and internal_sku not in (select internal_sku from supplierproduct) and storeid  = '2850-026'
group by storeid,description

select LPAD(internal_sku,9,'0'),internal_sku from storeproduct where length(internal_sku) < 8 and internal_sku <> ''

------------------------------------------------
-- improved linking of records 
--


--
-- add brand fields to product_gnm
-- re-import product, price_history, 


-- tasks 1.
-- load prepped 
--
alter table product_gnm add column    brand_name character varying(100);
alter table product_gnm add column        consignment bool;
alter table product_gnm add column        apn character varying(100);
alter table product_gnm add column        supplier_sku character varying (100);
alter table product_gnm add column       internal_sku character varying(100);
alter table product_gnm add column       distributor character varying(100);
alter table product_gnm add column       eyetalk_id character varying(100);


begin;
-- get brand apn, sku, consignment
update product_gnm pg set brand_name = b.name, consignment = p.consignment, apn = p.apn, supplier_sku = p.supplier_sku
from gnm.product p
inner join gnm.brand b on b.id = p.brand_id
where p.product_id = pg.productid;

 -- see if eyetalk codes can same us? nope only extras
 -- update product_gnm set eyetalk_id = null;
update product_gnm pg set eyetalk_id = pa.value
from gnm.product p
inner join gnm.brand b on b.id = p.brand_id
inner join gnm.product_attribute pa on pa.name = 'eyetalk_id' and pa.product_id = p.id
where p.product_id = pg.productid;

commit;
select * from product_gnm where brand_name is not null;
select * from product_gnm where eyetalk_id is not null;

select * from gnm.product p 
inner join gnm.product_attribute pa on pa.product_id = p.id
where 
	pa.name = 'eyetalk_id'

select name,value, count(*) from gnm.product_attribute pa group by "name",value order by count(*) desc

--
-- build buckets again as required
-- 







