---- Normalise the Events by topology
-- current topology/dimensions
-- dimbucket == sk, year, doy, store, product

-- dimbucket, event, store_stockmenvet_sk, adjustmenttype
---dimbucket, event, invoice, pos
---dimbucket, event, orders + ordertype + jobs + job status

-- Matched events / Matched Events
--- source bucket for event,dimbucket, matching event invoice, matching event order, matching event stockmovment

-- UnMatched events
--- events in bucket not in Matched events


---- Create Dim Bucket
--- year + doy + store_sk


drop table if exists prod_ref cascade;

create table if not exists  prod_ref (
	sk  serial primary key not null,
	brand varchar(200) not null,
	model varchar(200) not null,
	colour varchar(200) not null,
	total integer
);
create unique index unique_index_prod_ref on prod_ref(brand,model,colour);


drop table if exists prod_common;
create table if not exists prod_common 
(
	sk  serial primary key not null,
	prod_ref_sk integer not null references prod_ref(sk),
	product_gnm_sk integer not null references product_gnm(sk),
	storeproduct_sk integer not null references storeproduct(sk)
);

create  index unique_index_prod_common on prod_common(prod_ref_sk,product_gnm_sk,storeproduct_sk);
create  index unique_index_prod_common1 on prod_common(product_gnm_sk);
create  index unique_index_prod_common2 on prod_common(storeproduct_sk);

/*
================================================================================================
*/
--- create the reference table from store product
insert into prod_ref (brand,model,colour,total)
select upper(sp.frame_brand) brand,upper(sp.frame_model) model,upper(sp.frame_colour) colour, count(*) total
from storeproduct sp -- where product_gnm_sk = 4792
left join prod_ref pr on pr.brand = upper(sp.frame_brand) and pr.model = upper(sp.frame_model) and pr.colour = upper(sp.frame_colour)
where pr.sk is null
group by upper(sp.frame_brand),upper(sp.frame_model),upper(sp.frame_colour) order by count(*) desc;

--- insert any missing ones from product_gnm
insert into prod_ref (brand,model,colour,total)
select coalesce(upper(sp.brand_name),'') brand,coalesce(upper(sp.name),'') model,'', count(*) total
from product_gnm sp -- where product_gnm_sk = 4792
left join prod_ref pr on pr.brand = upper(sp.brand_name) and pr.model = upper(sp.name) 
 where pr.sk is null and upper(sp."type") = 'FRAME'
group by upper(sp.brand_name),upper(sp.name) order by count(*) desc;

/*
select count(*) from (
--insert into prod_ref (brand,model,colour,total)
select upper(sp.brand_name),upper(sp.name),'',count(*) 
from product_gnm sp
--inner join prod_ref pr on pr.brand = upper(sp.brand_name) and pr.model = upper(sp.name) and pr.colour = ''
--where type = 'frame'
group by brand_name, name
) x
*/

--select * from product_gnm

/*
--- SELECT COUNT(*) FROM PROD_REF

--- now populate prod_common
-- RULE 1 match on internal sku to gnm.productid
insert into prod_common(product_gnm_sk,storeproduct_sk)
select pg.sk,sp.sk
-- sp.internal_sku,pg.productid,upper(sp.frame_brand),upper(sp.frame_model),upper(sp.frame_colour),
-- upper(pg.frame_brand),upper(pg.frame_model),upper(pg.frame_colour), pg.*
from storeproduct sp 
inner join product_gnm pg on pg.productid = sp.internal_sku

-- RULE 2 match on brand, model, colour
insert into prod_common(prod_ref_sk,product_gnm_sk,storeproduct_sk)
select pr.sk,pg.sk,sp.sk from 
storeproduct sp
inner join product_gnm pg on  upper(pg.frame_brand) = upper(sp.frame_brand) and upper(pg.frame_model) = upper(sp.frame_model) and upper(pg.frame_colour) = upper(sp.frame_colour)
inner join prod_ref pr on pr.brand = upper(sp.frame_brand) and pr.model = upper(sp.frame_model) and pr.colour = upper(sp.frame_colour)
group by pr.sk,pg.sk,sp.sk
*/
-- POPULATE PROD_COMMON
-- truncate table prod_common
--- 
--- process all the store products that have a 
insert into prod_common(prod_ref_sk,product_gnm_sk,storeproduct_sk)
select pr.sk,sp.product_gnm_sk,sp.sk
from storeproduct sp 
inner join prod_ref pr on pr.brand = upper(sp.frame_brand) and pr.model = upper(sp.frame_model) and pr.colour = upper(sp.frame_colour) 
left join prod_common pc on pc.prod_ref_sk = pr.sk and pc.product_gnm_sk=sp.product_gnm_sk and pc.storeproduct_sk = sp.sk
where 
	pc is null and  pr.sk is not null 
	and upper(SP.type) = 'FRAME' and datecreated > '2015-06-01 00:00:00';

--- 
--- process all the gnm products that have a 
insert into prod_common(prod_ref_sk,product_gnm_sk,storeproduct_sk)
select pr.sk,sp.sk,0
from product_gnm sp 
-- must try and match colour = '' otherwise will match on multiple
left join prod_ref pr on pr.brand = upper(sp.brand_name) and pr.model = upper(sp.name) and pr.colour = ''
left join prod_common pc on pc.prod_ref_sk = pr.sk and pc.product_gnm_sk=sp.sk and pc.storeproduct_sk = 0
where pc is null and  
pr.sk is not null and upper(SP.type) = 'FRAME';

/**
--- 1. confirm that prod_common references 
select * from (
-- Compare with store product
select 1 as seq, count(*) from prod_common
union all
select 2 as seq, count(*) from (
select * from prod_common where storeproduct_sk <> 0 and product_gnm_sk = 0
) x
union all
select 3 as seq, count(*) from storeproduct sp	
where  upper(sp.type) = 'FRAME' and datecreated > '2017-06-01 00:00:00'  and product_gnm_sk <> 0
union all
select 2 as seq, count(*) from (
select * from prod_common where storeproduct_sk <> 0 and product_gnm_sk = 0
) x
union all
select 3 as seq, count(*) from storeproduct sp	
where  upper(sp.type) = 'FRAME' and datecreated > '2017-06-01 00:00:00'  and product_gnm_sk <> 0
union all
select 4 as seq, count(*) from (
select * from prod_common where storeproduct_sk = 0 and product_gnm_sk <> 0
)x
union all
select 4 as seq, count(*) from (
select * from product_gnm pg 
where pg.type = 'frame'  -- and pg.sk not in (select product_gnm_sk from prod_common)
) x ) y order by seq;

--- check for duplicates
select count(*) from (
select pc.prod_ref_sk,pc.product_gnm_sk,pc.storeproduct_sk,count(*)
from prod_common pc 
where storeproduct_sk = 0 and product_gnm_sk <> 0
-- having count(*) > 1
group by pc.prod_ref_sk,pc.product_gnm_sk,pc.storeproduct_sk 
order by product_gnm_sk, storeproduct_sk
) x
union all
--- check for duplicates
select count(*) from (
select pc.product_gnm_sk,pc.storeproduct_sk,count(*)
from prod_common pc 
where storeproduct_sk = 0 and product_gnm_sk <> 0
group by pc.product_gnm_sk,pc.storeproduct_sk 
having count(*) > 1
order by product_gnm_sk, storeproduct_sk
) x

--- discouver why duplicates of product_gnm

select pr.*,pc.product_gnm_sk from (
select pc.product_gnm_sk,pc.storeproduct_sk,count(*)
from prod_common pc 
where storeproduct_sk = 0 and product_gnm_sk <> 0
group by pc.product_gnm_sk,pc.storeproduct_sk 
--having count(*) > 1
order by product_gnm_sk, storeproduct_sk
) y inner join prod_common pc on pc.product_gnm_sk = y.product_gnm_sk and pc.storeproduct_sk = 0
inner join prod_ref pr on pr.sk = pc.prod_ref_sk
order by product_gnm_sk

-- RESULT - because product_gnm.color is not specified and matching on brand + model 
-- then product_gnm  matched all of them creating multiple common entries
-- need to have colour = ''

select * from product_gnm where frame_colour <> ''
*/
/*
================================================================================================
*/
drop table if exists daily_store_bucket cascade;
create table IF NOT EXISTS daily_store_bucket (
	sk serial primary key not null,
	d_date_sk integer not null references d_date(sk),
	store_sk integer not null references store(sk),
	prod_ref_sk integer not null references prod_ref(sk)
--	product_gnm_sk integer not null references product_gnm(sk),
--	storeproduct_sk integer not null references storeproduct(sk)
);
create unique index unique_index_daily_store_bucket on daily_store_bucket(d_date_sk,store_sk,prod_ref_sk);
create index index_daily_store_bucket_product on daily_store_bucket(store_sk,prod_ref_sk);

-- enforce an uniquenss index
--- Create From InvoicesItems
-- 
/*
 * 
truncate "daily_store_bucket";

*/
/*
================================================================================================
*/
insert into daily_store_bucket (d_date_sk,store_sk,prod_ref_sk)
select dd.sk, storesk, pc.prod_ref_sk
from invoiceitem sm 
inner join storeproduct sp on sp.sk = sm.productsk
inner join d_date dd on dd.year_calendar = date_part('year'::text, sm.createddate) and dd.day_of_year = (date_part('doy'::text, sm.createddate))
inner join prod_common pc on pc.storeproduct_sk = sp.sk and pc.product_gnm_sk = sp.product_gnm_sk
left join daily_store_bucket dsb on dd.sk = dsb.d_date_sk and sm.storesk = dsb.store_sk and dsb.prod_ref_sk = pc.prod_ref_sk
where dd.year_calendar = 2017 and dsb.sk is null
group by dd.sk, storesk, pc.prod_ref_sk;

------------validate no buckets are missed
/*
select count(*) from (
select dd.sk, storesk, pc.prod_ref_sk,sp.sk,sp.product_gnm_sk
from invoiceitem sm 
inner join storeproduct sp on sp.sk = sm.productsk
inner join d_date dd on dd.year_calendar = date_part('year'::text, sm.createddate) and dd.day_of_year = (date_part('doy'::text, sm.createddate)) and dd.year_calendar = 2017
left join prod_common pc on pc.storeproduct_sk = sp.sk and pc.product_gnm_sk = sp.product_gnm_sk
where pc.sk is null and upper(sp.type) ='FRAME'
) x 

*/
--------------------------------------------------------------------------------------

-- select * from store_stockmovement
-- Create From Stockmovements
insert into daily_store_bucket (d_date_sk,store_sk,prod_ref_sk)
select dd.sk, sm.store_sk, pc.prod_ref_sk --, -- count(*) --, dsb.*
from store_stockmovement sm
inner join d_date dd on dd.year_calendar = date_part('year'::text, sm.adjustmentdate) and dd.day_of_year = (date_part('doy'::text, sm.adjustmentdate))
inner join storeproduct sp on sp.sk = sm.takeon_storeproduct_sk
inner join prod_common pc on pc.storeproduct_sk = sp.sk and pc.product_gnm_sk = sp.product_gnm_sk
left join daily_store_bucket dsb on dsb.d_date_sk = dd.sk and dsb.store_sk = sm.store_sk and dsb.prod_ref_sk = pc.prod_ref_sk
where  dd.year_calendar = 2017 and dsb.sk is null
group by dd.sk, sm.store_sk, pc.prod_ref_sk
--having count(*) > 1;

-- Create From Orders
-- select * from store_stockmovement where takeon_storeproduct_sk is null
-- select * from store_stockmovement where takeon_storeproduct_sk = 0
-- select * from storeproduct where sk = 0
-- select * from prod_common where storeproduct_sk = 0 and product_gnm_sk = 0

insert into daily_store_bucket (d_date_sk,store_sk,prod_ref_sk)
select dd.sk, oh.store_sk, pc.prod_ref_sk
-- into dailytemp
from job_history sm 
inner join order_history oh on sm.order_sk = oh.sk
inner join product_gnm pg on pg.sk = sm.product_sk
inner join prod_common pc on pc.storeproduct_sk = 0 and pc.product_gnm_sk = pg.sk
inner join d_date dd on dd.year_calendar = date_part('year'::text, sm.source_created) and dd.day_of_year = (date_part('doy'::text, sm.source_created))
left join "store" s on oh.store_sk = s.sk
left join daily_store_bucket dsb on dsb.d_date_sk = dd.sk and dsb.prod_ref_sk = pc.prod_ref_sk and oh.store_sk = dsb.store_sk
where dsb.sk is null and dd.year_calendar = 2017 
group by dd.sk, oh.store_sk, pc.prod_ref_sk;

/*
================================================================================================
*/

-- create invoice bucket
drop  table if exists daily_invoice_bucket cascade;
create table daily_invoice_bucket (
	sk serial primary key not null,
	daily_store_bucket_sk integer not null references daily_store_bucket(sk),
	invoice_sk integer not null references invoice(sk),
	invoiceitemid varchar(50) not null 
	-- pos integer not null 
);
create unique index unique_index_daily_invoice_bucket on daily_invoice_bucket(daily_store_bucket_sk, invoice_sk, invoiceitemid);

-- insert invoice items
insert into daily_invoice_bucket (daily_store_bucket_sk, invoice_sk, invoiceitemid)
select dsb.sk, sm.invoicesk, sm.invoiceitemid
from invoiceitem sm 
inner join storeproduct sp on sp.sk = sm.productsk
inner join d_date dd on dd.year_calendar = date_part('year'::text, sm.createddate) and dd.day_of_year = (date_part('doy'::text, sm.createddate))
inner join prod_common pc on pc.storeproduct_sk = sp.sk and pc.product_gnm_sk = sp.product_gnm_sk
left join daily_store_bucket dsb on dd.sk = dsb.d_date_sk and sm.storesk = dsb.store_sk and dsb.prod_ref_sk = pc.prod_ref_sk
left join daily_invoice_bucket dib on dib.daily_store_bucket_sk = dsb.sk and dib.invoice_sk = sm.invoicesk
and dib.invoiceitemid = sm.invoiceitemid
where dd.year_calendar = 2017 and dib.sk is null


---------------------------- validation - check all invoice are in the bucker
/*
select count(*) from (
select * from daily_invoice_bucket
) x


select count(*) from (
select invoicesk,invoiceitemid,productsk,createddate,storesk,dd.sk,count(*)
from invoiceitem ii 
inner join storeproduct sp on ii.productsk = sp.sk
inner join d_date dd on dd.year_calendar = date_part('year'::text, ii.createddate) and dd.day_of_year = (date_part('doy'::text, ii.createddate))
where 
--	invoiceitemid not in (select invoiceitemid from daily_invoice_bucket)
 --   and 
    invoicesk not in (select invoice_sk  from daily_invoice_bucket dib 
    		group by invoice_sk having count(*) > 2) 
	and upper(sp.type) = 'FRAME'
	and dd.year_calendar = 2017 
group by invoicesk,invoiceitemid,productsk,createddate,storesk,dd.sk
order by invoicesk,invoiceitemid
) x



select * from storeproduct sp 
inner join prod_common pc on pc.storeproduct_sk = sp.sk and sp.product_gnm_sk = pc.product_gnm_sk
where sp.sk = 663732 and sp.sk in 
(select storeproduct_sk from prod_common)

select count(*) from (
select x.*,sp.product_gnm_sk,pc.*,dsb.* from (
select invoicesk,invoiceitemid,productsk,createddate,storesk,dd.sk as d_date_sk
from invoiceitem ii 
inner join storeproduct sp on ii.productsk = sp.sk
inner join d_date dd on dd.year_calendar = date_part('year'::text, ii.createddate) and dd.day_of_year = (date_part('doy'::text, ii.createddate))
where 
--	invoiceitemid not in (select invoiceitemid from daily_invoice_bucket)
 --   and 
    invoicesk not in (select invoice_sk  from daily_invoice_bucket dib 
group by invoice_sk
having count(*) > 2) 
	and sp.type = 'Frame'
	and dd.year_calendar = 2017 
order by invoicesk,invoiceitemid
) x 
left join storeproduct sp on sp.sk = x.productsk
inner join prod_common pc on pc.storeproduct_sk = sp.sk and pc.product_gnm_sk = sp.product_gnm_sk
inner join daily_store_bucket dsb on x.d_date_sk = dsb.d_date_sk and x.storesk = dsb.store_sk and dsb.prod_ref_sk = pc.prod_ref_sk
where pc.sk is not null 
) z


select count(*) from (
select ii.invoicesk, ii.invoiceitemid 
from daily_invoice_bucket dib 
right join invoiceitem ii on ii.invoicesk = dib.invoice_sk and ii.invoiceitemid = dib.invoiceitemid
where
	ii.createddate >= '2017-01-01' 
	and ii.productsk in (select sk from storeproduct where type = 'Frame')
	and dib.sk is null
) x



select invoice_sk,count(*)  from daily_invoice_bucket dib 
group by invoice_sk
having count(*) > 2
*/


---------------------------------------------------------------------------------------
-- create stockmovement bucket
drop table if exists daily_stockmovement_bucket cascade;
create table 
daily_stockmovement_bucket (
	sk serial primary key not null,
	daily_store_bucket_sk integer not null references daily_store_bucket(sk),
	store_stockmovement_sk integer not null references store_stockmovement(sk)
);
create unique index unique_index_daily_stockmovement_bucket on daily_stockmovement_bucket(daily_store_bucket_sk, store_stockmovement_sk);

insert into daily_stockmovement_bucket (daily_store_bucket_sk, store_stockmovement_sk)
select dsb.sk, sm.sk
from store_stockmovement sm
inner join d_date dd on dd.year_calendar = date_part('year'::text, sm.adjustmentdate) and dd.day_of_year = (date_part('doy'::text, sm.adjustmentdate))
inner join storeproduct sp on sp.sk = sm.takeon_storeproduct_sk
inner join prod_common pc on pc.storeproduct_sk = sp.sk and pc.product_gnm_sk = sp.product_gnm_sk
inner join daily_store_bucket dsb on dsb.d_date_sk = dd.sk and dsb.store_sk = sm.store_sk and dsb.prod_ref_sk = pc.prod_ref_sk
left join daily_stockmovement_bucket dsmb on dsmb.daily_store_bucket_sk = dsb.sk and dsmb.store_stockmovement_sk = sm.sk
where  dd.year_calendar = 2017 and dsmb.sk is null

-------------------------------- validate stock movements
/*
select count(*) from (
select * from store_stockmovement  ssm
inner join storeproduct sp on sp.sk =  ssm.takeon_storeproduct_sk and sp.type = 'Frame'
where ssm.sk not in
(select store_stockmovement_sk from daily_stockmovement_bucket)
and ssm.adjustmentdate >= '2017-01-01'
) x

*/
-------------------------------------------------------------------------------------------------------
-- create order bucket
-- truncate table daily_job_bucket;
-- select * from daily_job_bucket;
drop table if exists daily_job_bucket cascade;
create table 
daily_job_bucket (
	sk serial primary key not null,
	daily_store_bucket_sk integer not null references daily_store_bucket(sk),
	job_sk integer not null references job_history(sk)
);
create unique index unique_index_daily_job_bucket on daily_job_bucket(daily_store_bucket_sk, job_sk);

insert into daily_job_bucket (daily_store_bucket_sk, job_sk)
select dsb.sk, sm.sk
from last_job sm 
inner join order_history oh on sm.order_sk = oh.sk
inner join product_gnm pg on pg.sk = sm.product_sk
inner join prod_common pc on pc.storeproduct_sk = 0 and pc.product_gnm_sk = pg.sk
inner join d_date dd on dd.year_calendar = date_part('year'::text, sm.source_created) and dd.day_of_year = (date_part('doy'::text, sm.source_created))
left join daily_store_bucket dsb on dsb.d_date_sk = dd.sk and dsb.prod_ref_sk = pc.prod_ref_sk and oh.store_sk = dsb.store_sk
left join daily_job_bucket djb on djb.daily_store_bucket_sk = dsb.sk and djb.job_sk = sm.sk
where dd.year_calendar = 2017 and djb.sk is null
group by dsb.sk, sm.sk;


-- select count(*) from last_job


----------------------------------------------------------------------------------------------------------

-- select * from unmatched_daily_events where year = 2017 and doy > 100 and type = 'job'

---- utility views 

drop materialized view if exists mdaily_invoice cascade;
create MATERIALIZED view mdaily_invoice as
select dsb.sk as dsb_sk, dsb.d_date_sk, dsb.prod_ref_sk, dsb.store_sk as dsb_store_sk,
pr.brand,pr.model,pr.colour,
dd.date_actual,dd.day_name,dd.day_of_year doy,dd.year_calendar as year,s.storeid as store_name,i.invoiceid,
case when sp.product_gnm_sk = 0 then 'takeon' else 'gnm' end as "source",
case when pg.productid = '' then sp.productid else sp.productid || '-' || pg.productid end gnm_productid, 
ii.* 
from daily_invoice_bucket dib 
inner join daily_store_bucket dsb on dsb.sk = dib.daily_store_bucket_sk
inner join invoiceitem ii on ii.invoiceitemid = dib.invoiceitemid and ii.invoicesk = dib.invoice_sk
inner join d_date dd on dd.sk = dsb.d_date_sk
inner join prod_ref pr on pr.sk = dsb.prod_ref_sk
inner join store s on s.sk = ii.storesk
inner join  storeproduct sp on sp.sk = ii.productsk
inner join invoice i on i.sk = ii.invoicesk
inner join product_gnm pg on pg.sk = sp.product_gnm_sk
;

-- select * from store
-- select * from storeproduct

drop materialized view if exists mdaily_stockmovement cascade ;
create materialized view mdaily_stockmovement as
select dsb.sk as dsb_sk, dsb.d_date_sk, dsb.prod_ref_sk, dsb.store_sk as dsb_store_sk,
pr.brand,pr.model,pr.colour,
dd.date_actual,dd.day_name,dd.day_of_year doy,dd.year_calendar as year, s.storeid as store_name, sp.productid, pg.productid as gnm_productid, i.invoiceid, a."name" as adjustment_type,
ssm.* 
from daily_stockmovement_bucket dib 
inner join daily_store_bucket dsb on dsb.sk = dib.daily_store_bucket_sk
inner join store_stockmovement ssm on ssm.sk = dib.store_stockmovement_sk
inner join d_date dd on dd.sk = dsb.d_date_sk
inner join prod_ref pr on pr.sk = dsb.prod_ref_sk
inner join storeproduct sp on sp.sk = ssm.takeon_storeproduct_sk
inner join invoice i on i.sk = ssm.invoice_sk
inner join store s on s.sk = dsb.store_sk
inner join adjustment_type a on a.sk = ssm.adjustment_type_sk
inner join product_gnm pg on pg.sk = sp.product_gnm_sk
;


drop MATERIALIZED view if exists mdaily_job cascade;
create MATERIALIZED view mdaily_job as
select dsb.sk as dsb_sk, dsb.d_date_sk, dsb.prod_ref_sk, dsb.store_sk as dsb_store_sk,
pr.brand,pr.model,pr.colour,
dd.date_actual,dd.day_name,dd.day_of_year doy,dd.year_calendar as year, jh.* 
from daily_job_bucket dib 
inner join daily_store_bucket dsb on dsb.sk = dib.daily_store_bucket_sk
inner join job_history jh on jh.sk = dib.job_sk
inner join d_date dd on dd.sk = dsb.d_date_sk
inner join prod_ref pr on pr.sk = dsb.prod_ref_sk;

-- 

drop view if  exists vdaily_invoice cascade;
CREATE VIEW vdaily_invoice as 
select * from mdaily_invoice;

-- select * from mdaily_invoice

drop  view if  exists vdaily_stockmovement cascade;
CREATE VIEW vdaily_stockmovement as 
select * from mdaily_stockmovement;

-- select * from vdaily_stockmovement

drop view if  exists vdaily_job cascade;
CREATE VIEW vdaily_job as 
select * from mdaily_job;



---  ==========================================================================
--- 

-------------------------------------------------------------------------------------------------------
---- Create Matched event bucket
-- truncate table daily_matched_bucket;
-- select * from daily_matched_bucket;

drop table if exists daily_matched_bucket cascade ;
create table 
daily_matched_bucket (
	sk serial not null,
	invoice_daily_store_bucket_sk integer references daily_store_bucket(sk),
	job_daily_store_bucket_sk integer references daily_store_bucket(sk),
	stockmove_daily_store_bucket_sk integer references daily_store_bucket(sk),
	invoice_sk integer references invoice(sk),
	invoiceitemid varchar(50) ,
	job_sk  integer references job_history(sk),
	store_stockmovement_sk integer references store_stockmovement(sk),
	confidence_stockmovement integer,
	confidence_job integer,
	confidence_job_stockmovement integer
);


--- ============================ MATCH DATA

-- create some performance indexes over materialized views
create index idx_vinvoice_invoice on vdaily_invoice(invoicesk);
create index idx_vsm_invoice on vdaily_stockmovement(invoice_sk);



INSERT INTO public.daily_matched_bucket
(invoice_daily_store_bucket_sk, stockmove_daily_store_bucket_sk, 
invoice_sk, invoiceitemid, 
store_stockmovement_sk, 
confidence_stockmovement)
select 
ii.dsb_sk as invoice_bucket, sm.dsb_sk as sm_bucket, 
ii.invoicesk, invoiceitemid,
sm.sk,
ABS(ii.doy -  sm.doy)
from vdaily_invoice ii 
inner join vdaily_stockmovement sm on sm.invoice_sk = ii.invoicesk and sm.invoice_sk <> 0
inner join adjustment_type adj on adj.sk = sm.adjustment_type_sk
where
	ii.prod_ref_sk = sm.prod_ref_sk and sm.year = ii.year and
	-- adj.name = 'Sale' and 
	ii.year = 2017 -- and ii.doy >  160 
	


-----------------------------------------------------------------------------------------------------------
-- Unmatched View
drop view if exists unmatched_daily_events;

create view unmatched_daily_events as
select dib.sk,'invoice' as type ,dsb.year,dsb.doy,dsb.store_sk,dsb.prod_ref_sk
from daily_invoice_bucket  dib
inner join daily_store_bucket dsb on dsb.sk = dib.daily_store_bucket_sk
left join daily_matched_bucket dmb on dib.daily_store_bucket_sk = dmb.invoice_daily_store_bucket_sk and dib.invoice_sk = dmb.invoice_sk and dib.invoiceitemid = dmb.invoiceitemid
where dmb.invoice_daily_store_bucket_sk is null
union all
select dib.sk,'stock' as type ,dsb.year,dsb.doy,dsb.store_sk,dsb.prod_ref_sk
from daily_stockmovement_bucket  dib
inner join daily_store_bucket dsb on dsb.sk = dib.daily_store_bucket_sk
left join daily_matched_bucket dmb on dib.daily_store_bucket_sk = dmb.stockmove_daily_store_bucket_sk and dib.store_stockmovement_sk = dmb.store_stockmovement_sk
where dmb.stockmove_daily_store_bucket_sk is null
union all
select dib.sk,'job' as type ,dsb.year,dsb.doy,dsb.store_sk,dsb.prod_ref_sk
from daily_job_bucket  dib
inner join daily_store_bucket dsb on dsb.sk = dib.daily_store_bucket_sk
left join daily_matched_bucket dmb on dib.daily_store_bucket_sk = dmb.job_daily_store_bucket_sk and dib.job_sk = dmb.job_sk
where dmb.job_daily_store_bucket_sk is null;

--------------------------------------------

