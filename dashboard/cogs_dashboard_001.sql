
-- commit;
--- view for invoice ----
-- 1. store_stockmovement
--
-- Sanity check
-- total stock movements

-- select count(*) from store_stockmovement;

-- last stock movment
select max(adjustmentdate) from store_stockmovement;

--- Dashboard viewsmbyperiod

--- order_history

select doy,count(*) from vbyd_store_stockmovement where year = 2017 
group by doy
order by doy desc;

select doy,count(*) from vbyd_order_history where year = 2017 
group by doy
order by doy desc;

select doy,count(*) from vbyd_job_history where year = 2017 
group by doy
order by doy desc;

select doy,count(*) from vbyd_invoice where year = 2017 
group by doy
order by doy desc;

select doy,count(*) from vbyd_invoiceitem where year = 2017 
group by doy
order by doy desc;

 
select ngnm.doy,nongnm,gnm from 
(select doy,count(*) as nongnm from vbyd_store_stockmovement vssm inner join store_stockmovement sm on sm.sk = vssm.sk
where year = 2017 and sm.product_gnm_sk = 0
group by doy) ngnm left join 
(select doy,count(*) as gnm from vbyd_store_stockmovement vssm inner join store_stockmovement sm on sm.sk = vssm.sk
where year = 2017 and sm.product_gnm_sk <> 0
group by doy) gnmc on gnmc.doy = ngnm.doy
order by ngnm.doy desc;


 -- select date_part('doy'::text,  cast('2017-10-19 00:00:00'as timestamp)),date_part('dow'::text,  cast('2017-10-18 00:00:00'as timestamp))
 
select dsb_store_sk, count(*) from vdaily_invoice where year = 2017 and doy = 292 group by dsb_store_sk order by count(*) ;
select dsb_store_sk, count(*) from vdaily_stockmovement where year = 2017 and doy = 292 group by dsb_store_sk order by count(*) ;
select dsb_store_sk, count(*) from vdaily_job where year = 2017 and doy = 292 group by dsb_store_sk order by count(*) ;

select * from vdaily_stockmovement where year = 2017 and doy = 292 and dsb_store_sk = 7;
select * from vdaily_invoice where year = 2017 and doy = 292 and dsb_store_sk = 7


--------------------------------------------------------------------------------------------------
--- question
-- list of invoices that sell a frame and have stock movement
-- This means they were a sale of floor stock
select count(*) from  (
select ii.storesk,ii.invoiceitemid,ii.cost_gst,a.name,sp.*,sm.*,sh.* from invoiceitem ii 
left join storeproduct sp on sp.sk = ii.productsk
left join store_stockmovement sm on sm.invoice_sk = ii.invoicesk
left join adjustment_type a on a.sk = sm.adjustment_type_sk
left join stock_price_history sh on sh.sk = sm.stock_price_history_sk
where ii.modified > '2017-06-01 00:00:00' and lower(sp.type) = 'frame' and sm.sk is not null  
) x


---------------------------------------------------------------------------------------------------------
----
---- Match the orders to invoice

select count(*) from  (
select ii.storesk,ii.invoiceitemid,ii.cost_gst,sp.*,sj.*,oh.* from invoiceitem ii 
left join storeproduct sp on sp.sk = ii.productsk
left join spectaclejob sj on sj.invoicesk = ii.invoicesk
left join order_history oh on oh.source_id = substring(sj.spectaclejobid,10, length(sj.spectaclejobid))
where ii.modified > '2017-06-01 00:00:00' and lower(sp.type) = 'frame'
and sj.invoicesk is not null and oh.sk is not null
) x

select count(*) 
select * from order_history oh
left join spectaclejob sj on substring(sj.spectaclejobid,10, length(sj.spectaclejobid)) = oh.source_id
where createddate > '2017-06-01 00:00:00' and framesk is not null 
) x


-- select count(*) from order_history;

----------------------------------------------------------------------------------------------------------

select date '0001-01-01' + interval '2016 YEARS' + INTERVAL '230 DAYS'

select count(*) from prod_common

select count(*) from  (
select pr.sk,sp.product_gnm_sk,sp.sk
from storeproduct sp 
inner join prod_ref pr on pr.brand = upper(sp.frame_brand) and pr.model = upper(sp.frame_model) and pr.colour = upper(sp.frame_colour) 
left join prod_common pc on pc.prod_ref_sk = pr.sk and pc.product_gnm_sk=sp.product_gnm_sk and pc.storeproduct_sk = sp.sk
where pc is null and  pr.sk is not null and upper(SP.type) = 'FRAME'
) x

select count(*) from storeproduct where datecreated > '2017-06-01 00:00:00'

select count(*) from (
select prod_ref_sk,product_gnm_sk,count(*) from prod_common group by prod_ref_sk,product_gnm_sk
order by prod_ref_sk,product_gnm_sk,count(*) desc
) x




---- display query
drop view invoiceitem_cogs_view;

create view invoiceitem_cogs_view as 
select 
s.store_nk,
ii.invoiceitemid,
e.employeeid,
ii.createddate,
ii.modified,
ii.productsk,
ii.invoicesk,
ii.storesk,
sp.description frame_description,
sp.productid,
pg.name,
ii.description,
ii.amount_gst,
ii.amount_incl_gst,
ii.discount_gst,
ii.discount_incl_gst,
ii.cost_incl_gst,
ii.cost_gst,
ii.bulkbillref
-- ii.*
from invoiceitem ii 
inner join store s on s.sk = ii.storesk
inner join storeproduct sp on sp.sk = ii.productsk and sp.type = 'Frame'
left join employee e on e.sk = ii.employeesk
left join product_gnm pg on pg.sk = sp.product_gnm_sk

------------------------------------------------------------------------
-------------- REPRORT for vita
----- 
select ii.*,a.name,
sm.adjustment,
sm.adjustmentdate,
sm.moving_actual_cost_value,
sm.moving_quantity,
sm.moving_cost_per_unit,
case when sp.product_gnm_sk <> 0 then 'GNM' else'TAKEON STOCK'end,
'FRAME TO FOLLOW'
from invoiceitem_cogs_view ii 
left join storeproduct sp on sp.sk = ii.productsk
left join store_stockmovement sm on sm.invoice_sk = ii.invoicesk
left join adjustment_type a on a.sk = sm.adjustment_type_sk
left join stock_price_history sh on sh.sk = sm.stock_price_history_sk
where ii.modified > '2017-06-01 00:00:00' and lower(sp.type) = 'frame' and sm.sk is not null  

select ii.*,
sj.*,oh.* 
from invoiceitem_cogs_view ii 
left join storeproduct sp on sp.sk = ii.productsk
left join stock_price_history sph on sp.
left join spectaclejob sj on sj.invoicesk = ii.invoicesk
left join order_history oh on oh.source_id = substring(sj.spectaclejobid,10, length(sj.spectaclejobid))
where ii.modified > '2017-06-01 00:00:00' and lower(sp.type) = 'frame'
and sj.invoicesk is not null and oh.sk is not null


-- Invoice item view

create view invoiceitem_cogs_view as 
SELECT invoicesk, pos, productsk, storesk, employeesk, serviceemployeesk, customersk, createddate, 
ii.quantity, amount_incl_gst,ii.modified, amount_gst, discount_incl_gst, discount_gst, cost_incl_gst, 
cost_gst, ii.description, payeesk, invoiceitemid, return_incl_gst, return_gst, bulkbillref, createddate_sk, 
pms_cost, gnm_current_cost, takeon_avg_cost, ssm.avg_cost_price as  avg_cost, oh.order_type, oh.source_id as order_id
	FROM public.invoiceitem ii
	left join store_stockmovement ssm on ssm.invoice_sk = ii.invoicesk -- and ssm.product_gnm_sk = ii.productsk
	left join storeproduct sp on ii.productsk = sp.sk
	left join invoice_orders io on io.invoice_sk = ii.invoicesk
	left join order_history oh on oh.sk = io.order_sk
	left join last_job lj on lj.order_sk = oh.sk
	


--------------------------------------------------------------------
---- 

GNM001212		8/21/2017	8/21/2017	0	1	1	Restock	R	GNM	MAUI JIM 9536685	178184	F	FGNM001212		299	RE1	Maui Jim, H415-26B, Rootbeer Fade, 60-18, 125	8/21/2017							
GNM001212		8/21/2017	8/21/2017	1	1	0	Restock	R	GNM	MAUI JIM 9536685	178184	F	FGNM001212		299	RE1	Maui Jim, H415-26B, Rootbeer Fade, 60-18, 125	8/21/2017							
	

select ii.*,a.name,
sm.adjustment,
sm.adjustmentdate,
sm.moving_actual_cost_value,
sm.moving_quantity,
sm.moving_cost_per_unit,
case when sp.product_gnm_sk <> 0 then 'GNM' else'TAKEON STOCK'end,
'FRAME TO FOLLOW'
from 
 invoiceitem_cogs_view ii 
left join storeproduct sp on sp.sk = ii.productsk
left join store_stockmovement sm on sm.invoice_sk = ii.invoicesk
left join adjustment_type a on a.sk = sm.adjustment_type_sk
left join stock_price_history sh on sh.sk = sm.stock_price_history_sk
where ii.modified > '2017-06-01 00:00:00' and lower(sp.type) = 'frame' and sm.sk is not null  




