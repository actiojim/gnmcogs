---- Normalise the Events by topology
-- current topology/dimensions
-- dimbucket == sk, year, doy, store, product

-- dimbucket, event, store_stockmenvet_sk, adjustmenttype
---dimbucket, event, invoice, pos
---dimbucket, event, orders + ordertype + jobs + job status

-- Matched events / Matched Events
--- source bucket for event,dimbucket, matching event invoice, matching event order, matching event stockmovment

-- UnMatched events
--- events in bucket not in Matched events


---- Create Dim Bucket
--- year + doy + gnm_product_sk + takeon_storeproduct + store_sk


drop table if exists prod_ref;

create table if not exists  prod_ref (
	sk  serial primary key not null,
	brand varchar(200),
	model varchar(200),
	colour varchar(200),
	total integer
);
create unique index unique_index_prod_ref on prod_ref(brand,model,colour);


drop table if exists prod_common;
create table if not exists prod_common 
(
	sk  serial primary key not null,
	prod_ref_sk integer not null references prod_ref(sk),
	product_gnm_sk integer not null references product_gnm(sk),
	storeproduct_sk integer not null references storeproduct(sk)
);


--------------------------------------------------------------------------------------------------------------------------------------
--- create the reference table from store product
insert into prod_ref (brand,model,colour,total)
select upper(sp.frame_brand) brand,upper(sp.frame_model) model,upper(sp.frame_colour) colour, count(*) total
from storeproduct sp -- where product_gnm_sk = 4792
inner join prod_ref pr on pr.brand = upper(sp.frame_brand) and pr.model = upper(sp.frame_model) and pr.colour = upper(sp.frame_colour)
where pr.sk is null
group by upper(sp.frame_brand),upper(sp.frame_model),upper(sp.frame_colour) order by count(*) desc;

--- insert any missing ones from product_gnm
insert into prod_ref (brand,model,colour,total)
select upper(sp.frame_brand) brand,upper(sp.frame_model) model,upper(sp.frame_colour) colour, count(*) total
from product_gnm sp -- where product_gnm_sk = 4792
inner join prod_ref pr on pr.brand = upper(sp.frame_brand) and pr.model = upper(sp.frame_model) and pr.colour = upper(sp.frame_colour)
where pr.sk is null
group by upper(sp.frame_brand),upper(sp.frame_model),upper(sp.frame_colour) order by count(*) desc;


--- now populate prod_common
-- RULE 1 match on internal sku to gnm.productid
insert into prod_common(product_gnm_sk,storeproduct_sk)
select pg.sk,sp.sk
-- sp.internal_sku,pg.productid,upper(sp.frame_brand),upper(sp.frame_model),upper(sp.frame_colour),
-- upper(pg.frame_brand),upper(pg.frame_model),upper(pg.frame_colour), pg.*
from storeproduct sp 
inner join product_gnm pg on pg.productid = sp.internal_sku

-- RULE 2 match on brand, model, colour
insert into prod_common(prod_ref_sk,product_gnm_sk,storeproduct_sk)
select pr.sk,pg.sk,sp.sk from 
storeproduct sp
inner join product_gnm pg on  upper(pg.frame_brand) = upper(sp.frame_brand) and upper(pg.frame_model) = upper(sp.frame_model) and upper(pg.frame_colour) = upper(sp.frame_colour)
inner join prod_ref pr on pr.brand = upper(sp.frame_brand) and pr.model = upper(sp.frame_model) and pr.colour = upper(sp.frame_colour)
group by pr.sk,pg.sk,sp.sk


-------------------------------------------------------------------
drop table if exists daily_store_bucket cascade;
create table IF NOT EXISTS daily_store_bucket (
	sk serial primary key not null,
	"year" integer not null,
	"doy"  integer not null,
	store_sk integer not null references store(sk),
	prod_common_sk integer not null references prod_common(sk)
--	product_gnm_sk integer not null references product_gnm(sk),
--	storeproduct_sk integer not null references storeproduct(sk)
);
create unique index unique_index_daily_store_bucket on daily_store_bucket("year","doy",store_sk,product_gnm_sk,storeproduct_sk);
create index index_daily_store_bucket_product on daily_store_bucket(store_sk,product_gnm_sk,storeproduct_sk);

-- enforce an uniquenss index

-- year + day + store + storeproduct + product_gnm + sk

--- Create From InvoicesItems
-- 
/*
 * 
truncate "daily_store_bucket";

select * from "daily_store_bucket"
select * from product_gnm where sk = 0
select * from order_history;
select * from job_history;
select * from storeproduct; 
select * from invoiceitem;

select count(*) from (select invoicesk, pos,count(*)  from invoiceitem where pos  = 0 group by invoicesk, pos having count(*) > 1) x
select count(*) from (select invoicesk, invoiceitemid,count(*)  from invoiceitem  group by invoicesk, invoiceitemid having count(*) > 1) x

update store_stockmovement set product_gnm_sk = 0 where product_gnm_sk is null
update storeproduct set  product_gnm_sk = 0 where product_gnm_sk is null

select count(*) from daily_store_bucket

2303747
2263211
2283969


*/

insert into daily_store_bucket ("year","doy",store_sk,product_gnm_sk,storeproduct_sk)
select (date_part('year'::text, sm.createddate)), (date_part('doy'::text, sm.createddate)), storesk, coalesce(sp.product_gnm_sk,0), productsk 
from invoiceitem sm 
inner join storeproduct sp on sp.sk = sm.productsk
inner join 
left join daily_store_bucket dsb on dsb.year = (date_part('year'::text, sm.createddate)) and dsb.doy = (date_part('doy'::text, sm.createddate)) and sm.storesk = dsb.store_sk
and sp.product_gnm_sk = dsb.product_gnm_sk and sp.sk = dsb.storeproduct_sk
where dsb.year is null and (date_part('year'::text, sm.createddate)) = 2017 and (date_part('doy'::text, sm.createddate)) > 100
group by (date_part('year'::text, sm.createddate)), (date_part('doy'::text, sm.createddate)), storesk, sp.product_gnm_sk,productsk;


-- select * from store_stockmovement
-- Create From Stockmovements
insert into daily_store_bucket ("year","doy",store_sk,product_gnm_sk,storeproduct_sk)
select (date_part('year'::text, sm.adjustmentdate)), (date_part('doy'::text, sm.adjustmentdate)), sm.store_sk, coalesce(sm.product_gnm_sk,0), coalesce(sm.takeon_storeproduct_sk,0)
from store_stockmovement sm
left join daily_store_bucket dsb on dsb.year = (date_part('year'::text, sm.adjustmentdate)) and dsb.doy = (date_part('doy'::text, sm.adjustmentdate)) and sm.store_sk = dsb.store_sk
and sm.product_gnm_sk = dsb.product_gnm_sk and sm.takeon_storeproduct_sk = dsb.storeproduct_sk
where dsb.year is null and (date_part('year'::text, sm.adjustmentdate)) = 2017 and (date_part('doy'::text, sm.adjustmentdate)) > 100 
-- and (sm.product_gnm_sk is not null and sm.product_gnm_sk <> 0 and (sm.takeon_storeproduct_sk is null or sm.takeon_storeproduct_sk = 0))
group by (date_part('year'::text, sm.adjustmentdate)), (date_part('doy'::text, sm.adjustmentdate)), sm.store_sk, sm.product_gnm_sk,sm.takeon_storeproduct_sk

-- Create From Orders
-- 
-- select * from storeproduct
--

insert into daily_store_bucket ("year","doy",store_sk,product_gnm_sk,storeproduct_sk)
select (date_part('year'::text, sm.source_created)) as year, (date_part('doy'::text, sm.source_created)) as doy, oh.store_sk, 
sm.product_sk as product_gnm_sk , coalesce(sp.sk, 0) as storeproduct_sk 
-- into dailytemp
from job_history sm 
inner join order_history oh on sm.order_sk = oh.sk
left join storeproduct sp on sp.product_gnm_sk = sm.product_sk  
left join "store" s on s.store_nk = sp.storeid and oh.store_sk = s.sk
left join daily_store_bucket dsb on dsb.year = (date_part('year'::text, sm.source_created)) and dsb.doy = (date_part('doy'::text, sm.source_created)) and oh.store_sk = dsb.store_sk
and sm.product_sk = dsb.product_gnm_sk and dsb.storeproduct_sk = coalesce(sp.sk,0)
where dsb.year is null and (date_part('year'::text, sm.source_created)) = 2017 and (date_part('doy'::text, sm.source_created)) > 100 
group by (date_part('year'::text, sm.source_created)), (date_part('doy'::text, sm.source_created)), oh.store_sk, sm.product_sk, sp.sk

/*
 * investigated mistmatched storeproduct & gnm product
 * 
select dt.year, dt.doy, dt.store_sk, dt.product_gnm_sk, count(*) from dailytemp dt 
left join daily_store_bucket dsb on dsb.year = dt.year and dsb.doy = dt.doy and dsb.store_sk = dt.store_sk 
where 
	dt.product_gnm_sk = dsb.product_gnm_sk
group by dt.year, dt.doy, dt.store_sk, dt.product_gnm_sk

-- Need strict controls on detecting gnm product and non gnm product

select * from storeproduct where product_gnm_sk <> 0 

*/

-- go back and update product_gnm for storeproducts where possible

-- create invoice bucket
drop  table  daily_invoice_bucket cascade;

create table daily_invoice_bucket (
	sk serial primary key not null,
	daily_store_bucket_sk integer not null references daily_store_bucket(sk),
	invoice_sk integer not null references invoice(sk),
	invoiceitemid varchar(50) not null 
	-- pos integer not null 
);
create unique index unique_index_daily_invoice_bucket on daily_invoice_bucket(daily_store_bucket_sk, invoice_sk, invoiceitemid)

-- insert invoice items
insert into daily_invoice_bucket (daily_store_bucket_sk, invoice_sk, invoiceitemid)
select dsb.sk, sm.invoicesk, sm.invoiceitemid
from invoiceitem sm
inner join storeproduct sp on sp.sk = sm.productsk
inner join daily_store_bucket dsb on dsb.year = (date_part('year'::text, sm.createddate)) and dsb.doy = (date_part('doy'::text, sm.createddate)) and sm.storesk = dsb.store_sk
and sp.product_gnm_sk = dsb.product_gnm_sk and sp.sk = dsb.storeproduct_sk


-- create stockmovement bucket
drop table daily_stockmovement_bucket cascade;
create table 
daily_stockmovement_bucket (
	sk serial primary key not null,
	daily_store_bucket_sk integer not null references daily_store_bucket(sk),
	store_stockmovement_sk integer not null references store_stockmovement(sk)
);
create unique index unique_index_daily_stockmovement_bucket on daily_stockmovement_bucket(daily_store_bucket_sk, store_stockmovement_sk);

insert into daily_stockmovement_bucket (daily_store_bucket_sk, store_stockmovement_sk)
select dsb.sk, sm.sk
from store_stockmovement sm
left join daily_store_bucket dsb on dsb.year = (date_part('year'::text, sm.adjustmentdate)) and dsb.doy = (date_part('doy'::text, sm.adjustmentdate)) and sm.store_sk = dsb.store_sk
and sm.product_gnm_sk = dsb.product_gnm_sk and sm.takeon_storeproduct_sk = dsb.storeproduct_sk


-- create order bucket
-- truncate table daily_job_bucket;
-- select * from daily_job_bucket;
drop table daily_job_bucket cascade;
create table 
daily_job_bucket (
	sk serial primary key not null,
	daily_store_bucket_sk integer not null references daily_store_bucket(sk),
	job_sk integer not null references job_history(sk)
);
create unique index unique_index_daily_job_bucket on daily_job_bucket(daily_store_bucket_sk, job_sk);

insert into daily_job_bucket (daily_store_bucket_sk, job_sk)
select dsb.sk, sm.sk
from last_job sm 
inner join order_history oh on sm.order_sk = oh.sk
left join daily_store_bucket dsb on dsb.year = (date_part('year'::text, sm.source_created)) and dsb.doy = (date_part('doy'::text, sm.source_created)) and oh.store_sk = dsb.store_sk
and sm.product_sk = dsb.product_gnm_sk and dsb.storeproduct_sk = 0
;

-- select * from last_job

-------------------------------------------------------------------------------------------------------------------
---- Create Matched event bucket
-- truncate table daily_matched_bucket;
-- select * from daily_matched_bucket;

drop table if exists daily_matched_bucket cascade ;
create table 
daily_matched_bucket (
	sk serial not null,
	invoice_daily_store_bucket_sk integer references daily_store_bucket(sk),
	job_daily_store_bucket_sk integer references daily_store_bucket(sk),
	stockmove_daily_store_bucket_sk integer references daily_store_bucket(sk),
	invoice_sk integer references invoice(sk),
	invoiceitemid varchar(50) ,
	job_sk  integer references job_history(sk),
	store_stockmovement_sk integer references store_stockmovement(sk),
	confidence_stockmovement integer,
	confidence_job integer,
	confidence_job_stockmovement integer
);

-------------------------------------------------------------------------------------------------------------------
-- Unmatched View
drop view if exists unmatched_daily_events;

create view unmatched_daily_events as
select dib.sk,'invoice' as type ,dsb.year,dsb.doy,dsb.store_sk,dsb.product_gnm_sk,storeproduct_sk
from daily_invoice_bucket  dib
inner join daily_store_bucket dsb on dsb.sk = dib.daily_store_bucket_sk
left join daily_matched_bucket dmb on dib.daily_store_bucket_sk = dmb.invoice_daily_store_bucket_sk and dib.invoice_sk = dmb.invoice_sk and dib.invoiceitemid = dmb.invoiceitemid
where dmb.invoice_daily_store_bucket_sk is null
union all
select dib.sk,'stock' as type ,dsb.year,dsb.doy,dsb.store_sk,dsb.product_gnm_sk,storeproduct_sk
from daily_stockmovement_bucket  dib
inner join daily_store_bucket dsb on dsb.sk = dib.daily_store_bucket_sk
left join daily_matched_bucket dmb on dib.daily_store_bucket_sk = dmb.stockmove_daily_store_bucket_sk and dib.store_stockmovement_sk = dmb.store_stockmovement_sk
where dmb.stockmove_daily_store_bucket_sk is null
union all
select dib.sk,'job' as type ,dsb.year,dsb.doy,dsb.store_sk,dsb.product_gnm_sk,storeproduct_sk
from daily_job_bucket  dib
inner join daily_store_bucket dsb on dsb.sk = dib.daily_store_bucket_sk
left join daily_matched_bucket dmb on dib.daily_store_bucket_sk = dmb.job_daily_store_bucket_sk and dib.job_sk = dmb.job_sk
where dmb.job_daily_store_bucket_sk is null;

-------------------------------------------------------------------------------------------------------------------

-- select * from unmatched_daily_events where year = 2017 and doy > 100 and type = 'job'

---- utility views 

drop view if exists vdaily_invoice cascade;
create view vdaily_invoice as
select dsb.sk as dsb_sk, dsb."year", dsb.doy, dsb.product_gnm_sk as dsb_product_gnm_sk, dsb.store_sk as dsb_store_sk, dsb.storeproduct_sk as dsb_storeproduct_sk,ii.* 
from daily_invoice_bucket dib 
inner join daily_store_bucket dsb on dsb.sk = dib.daily_store_bucket_sk
inner join invoiceitem ii on ii.invoiceitemid = dib.invoiceitemid and ii.invoicesk = dib.invoice_sk;

-- select * from vdaily_invoice where year = 2017 and doy = 290

drop view if exists vdaily_stockmovement cascade;
create view vdaily_stockmovement as
select dsb.sk as dsb_sk, dsb."year", dsb.doy, dsb.product_gnm_sk as dsb_product_gnm_sk, dsb.store_sk as dsb_store_sk, dsb.storeproduct_sk as dsb_storeproduct_sk, ssm.* 
from daily_stockmovement_bucket dib 
inner join daily_store_bucket dsb on dsb.sk = dib.daily_store_bucket_sk
inner join store_stockmovement ssm on ssm.sk = dib.store_stockmovement_sk;

--  select * from vdaily_stockmovement where year = 2017 and doy = 290

drop view if exists vdaily_job cascade;
create view vdaily_job as
select dsb.sk as dsb_sk, dsb."year", dsb.doy, dsb.product_gnm_sk as dsb_product_gnm_sk, dsb.store_sk as dsb_store_sk, dsb.storeproduct_sk as dsb_storeproduct_sk,jh.* 
from daily_job_bucket dib 
inner join daily_store_bucket dsb on dsb.sk = dib.daily_store_bucket_sk
inner join job_history jh on jh.sk = dib.job_sk;

-- select * from vdaily_job where year = 2017 and doy = 290

---  =================================================================================================================================
--- 
insert into pms_invoice_orders 
select substring(spectaclejobid,10, length(spectaclejobid)) as orderid, oh.sk, sp.invoicesk, sp.framesk,sp.right_lenssk,sp.left_lenssk,sp.storesk,sp.pms_invoicenum 
from spectaclejob sp 
left join order_history oh on oh.source_id = substring(spectaclejobid,10, length(spectaclejobid))
LEFT JOIN  pms_invoice_orders pio on pio.orderid = substring(spectaclejobid,10, length(spectaclejobid)) and pio.invoicesk = sp.invoicesk
where pio.orderid is null;

-- create some indexes to improve performance

create index if not exists idx_orderid_order_history on order_history(source_id);

create index if not exists idx_orderid_pms_invoice_orders on pms_invoice_orders(orderid);

drop view if exists last_job;
create view last_job as
select jh.* from public.job_history jh
left join 
(select source_id,max(sk) as max_sk,max(source_modified) as max_date, count(*) from public.job_history jh group by source_id) x
on x.max_sk = jh.sk
where x.max_date <> jh.source_modified
order by source_id;


---------------------------------------------------------------------------------------------------------------------------------------------------------------


