

select * from store_stockmovement

alter table store_stockmovement drop column adjustmenttype;


select * from invoiceitem;

drop view vbyd_invoiceitem;

CREATE VIEW vbyd_invoiceitem AS
 SELECT date_part('year'::text, sm.createddate) AS year,
    date_part('doy'::text, sm.createddate) AS doy,
    date_part('month'::text, sm.createddate) AS month,
    date_part('dow'::text, sm.createddate) AS dow,
    date_part('day'::text, sm.createddate) AS day,
    sm.invoicesk,
    sm.pos
   FROM invoiceitem sm
  GROUP BY (date_part('year'::text, sm.createddate)), (date_part('doy'::text, sm.createddate)), (date_part('month'::text, sm.createddate)), (date_part('dow'::text, sm.createddate)), (date_part('day'::text, sm.createddate)), sm.invoicesk, sm.pos;

  
 select * from product_gnm;
 
 select * from storeproduct;
 
select gp.productid,gp.type,gp.name,tp.internal_sku,tp.productid,tp.type,tp.name,tp.description from  public.product_gnm gp 
left join public.storeproduct tp on  (tp.internal_sku <> '' AND gp.productid = tp.internal_sku)
where tp.sk is not null

.product_gnm_sk is not null and tp.product_gnm_sk <> 0 


 select ss.*, * from product_gnm pg left join  
  (select substring(productid,13, length(productid)) as pid,productid,sk from public.storeproduct where storeid != '0') ss on pg.name = ss.pid
 where ss.pid is not null
 
 
 
 select * from storeproduct where product_gnm_sk is not null and product_gnm_sk <> 0;
 
 select * from store_stockmovement where product_gnm_sk is not null and product_gnm_sk <> 0
 
 
 /*****************
 begin;
 update storeproduct as tp set product_gnm_sk = gp.sk from  public.product_gnm gp 
where tp.internal_sku <> '' AND gp.productid = tp.internal_sku;

 update storeproduct as tp set product_gnm_sk = 0  where product_gnm_sk is null;
commit;

***************/

 select sp.internal_sku,pg.productid from storeproduct sp left join product_gnm pg on pg.sk = sp.product_gnm_sk where pg.sk is not null and pg.sk <>0;
 
 select sp.internal_sku,* from storeproduct sp where  sp.product_gnm_sk is  null;
 
 select sp.internal_sku,* from storeproduct sp where  sp.product_gnm_sk = 0;
 
 -- rollback;
  select count(*) from storeproduct sp;
  
  
select * from pms_invoice_orders

select * from invoiceitem ii 
left join pms_invoice_orders io on io.invoicesk = ii.invoicesk
left join last_job lj on lj.order_sk = io.ordersk
left join order_history oh on oh.sk = io.ordersk
left join product_gnm pg on pg.sk = lj.product_sk and pg.type = 'frame'
left join store_stockmovement ssm on 
  
  select * from invoiceitem ii 
-- left join storeproduct pg on pg.sk = ii.productsk and pg.type = 'frame'
inner join store_stockmovement ssm on ssm.invoice_sk = ii.invoicesk
inner join product_gnm pg on pg.sk = ssm.product_gnm_sk
--inner join spectaclejob sj on sj.invoicesk = ii.invoicesk
where 
	ssm.invoice_sk <> 0
	
	
select * from stage.today_gnm_sunix_vframe

select "INVNO",substring("INVNO",5),count(*) from stage.today_gnm_sunix_vrestock group by "INVNO"

 select count(*) from spectaclejob where invoicesk <> 0 and invoicesk is not null;
 
 select * from spectaclejob where invoicesk <> 0 and invoicesk is not null;
 
 
 1. check price histor is correct
 
 select * from stock_price_history where takeon_storeproduct_sk  <> 0
 
 
 select * from stock_price_history where product_gnm_sk  <> 0
 
 
  select aj.name,count(*) from store_stockmovement ssm 
 inner join adjustment_type aj on aj.sk = ssm.adjustment_type_sk
 --where moving_actual_cost_value is not null 
 
 group by aj.name 
 order by count(*) desc;
 
 select aj.name,count(*) from store_stockmovement ssm 
 inner join adjustment_type aj on aj.sk = ssm.adjustment_type_sk
 where moving_actual_cost_value is not null 
 
 group by aj.name 
 order by count(*) desc
 
 and product_gnm_sk <> 0;
 
 
-------------------------------------------------------------------------------------------------------------------
---- Create Matched event bucket

drop table if exists daily_matched_bucket cascade ;
create table 
daily_matched_bucket (
	sk serial not null,
	invoice_daily_store_bucket_sk integer references daily_store_bucket(sk),
	job_daily_store_bucket_sk integer references daily_store_bucket(sk),
	stockmove_daily_store_bucket_sk integer references daily_store_bucket(sk),
	invoice_sk integer references invoice(sk),
	invoiceitemid varchar(50) ,
	job_sk  integer references job_history(sk),
	store_stockmovement_sk integer references store_stockmovement(sk),
	confidence_stockmovement integer,
	confidence_job integer,
	confidence_job_stockmovement integer
);
----

drop table pms_invoice_orders;

CREATE TABLE pms_invoice_orders (
    orderid text,
    ordersk integer,
    invoicesk integer,
    framesk integer,
    right_lenssk integer,
    left_lenssk integer,
    storesk integer,
    pms_invoicenum character varying(100)
);

select sj.*,spframe.description,sprlens.description,spllens.description from spectaclejob sj
inner join storeproduct spframe on spframe.sk = sj.framesk
inner join storeproduct sprlens on sprlens.sk = sj.right_lenssk
inner join storeproduct spllens on spllens.sk = sj.left_lenssk

insert into pms_invoice_orders 
select substring(spectaclejobid,10, length(spectaclejobid)) as orderid, oh.sk, sp.invoicesk, sp.framesk,sp.right_lenssk,sp.left_lenssk,sp.storesk,sp.pms_invoicenum 
from spectaclejob sp 
left join order_history oh on oh.source_id = substring(spectaclejobid,10, length(spectaclejobid))
LEFT JOIN  pms_invoice_orders pio on pio.orderid = substring(spectaclejobid,10, length(spectaclejobid)) and pio.invoicesk = sp.invoicesk
where pio.orderid is null;

--- populate matched bucket
-- select * from pms_invoice_orders where ordersk is not null and invoicesk is not null;

---- Brute force single match based upon pms_invoice_orders table
/*
select * from invoiceitem ii 
left join pms_invoice_orders io on io.invoicesk = ii.invoicesk
left join last_job lj on lj.order_sk = io.order_sk
left join order_history oh on oh.sk = io.order_sk
left join product_gnm pg on pg.sk = lj.product_sk and pg.type = 'frame'
left join store_stockmovement ssm_pms on ssm_pms.invoice_sk = io.invoice_sk
left join store_stockmovement ssm on ssm.invoice_sk = ii.invoicesk
  
  select * from invoiceitem ii 
-- left join storeproduct pg on pg.sk = ii.productsk and pg.type = 'frame'
inner join store_stockmovement ssm on ssm.invoice_sk = ii.invoicesk
inner join product_gnm pg on pg.sk = ssm.product_gnm_sk
--inner join spectaclejob sj on sj.invoicesk = ii.invoicesk
where 
	ssm.invoice_sk <> 0
*/
-- match gnm products


select count(*) from invoiceitem ii 
inner join pms_invoice_orders pio on pio.invoicesk = ii.invoicesk  -- and pio.framesk = ii.productsk
;


select ii.dsb_sk, j.dsb_sk, sm.dsb_sk, ii.*
from vdaily_invoice ii 
inner join pms_invoice_orders pio on pio.invoicesk = ii.invoicesk   and pio.ordersk is not null and pio.framesk is not null and pio.framesk <> 0-- and pio.framesk = ii.productsk
inner join vdaily_stockmovement sm on sm.invoice_sk = pio.invoicesk and sm.invoice_sk <> 0
inner join vdaily_job j on j.order_sk = pio.ordersk and pio.ordersk is not null and pio.framesk is not null and pio.framesk <> 0
where	
	ii.year = 2017 and ii.year = sm.year and ii.doy = sm.doy and ii.year = j.year and ii.doy = j.doy 
	and 
	ii.dsb_sk = j.dsb_sk and ii.dsb_sk = sm.dsb_sk
	
-----------------------------------------------------------------------------------------------------------------------------------------
------------ BRUTE FORCE JOIN of all events
INSERT INTO public.daily_matched_bucket
(invoice_daily_store_bucket_sk, job_daily_store_bucket_sk, stockmove_daily_store_bucket_sk, 
invoice_sk, invoiceitemid, 
job_sk, 
store_stockmovement_sk, 
confidence_stockmovement, confidence_job, confidence_job_stockmovement)
select ii.dsb_sk as invoice_bucket, j.dsb_sk as job_bucket, sm.dsb_sk as sm_bucket, 
ii.invoicesk, invoiceitemid,
j.sk,
sm.sk,
ABS(ii.doy -  sm.doy), ABS(ii.doy - j.doy), ABS(sm.doy - j.doy)
from vdaily_invoice ii 
inner join vdaily_stockmovement sm on sm.invoice_sk = ii.invoicesk and sm.invoice_sk <> 0
inner join pms_invoice_orders pio on pio.invoicesk = ii.invoicesk 
inner join vdaily_job j on j.order_sk = pio.ordersk and j.supplier_sk <> 61 -- exclude zeiss
inner join adjustment_type adj on adj.sk = sm.adjustment_type_sk
where
	ii.dsb_storeproduct_sk = sm.dsb_storeproduct_sk and ii.dsb_product_gnm_sk = sm.dsb_product_gnm_sk and
	ii.dsb_product_gnm_sk = j.dsb_product_gnm_sk and
	adj.name = 'Sale' and ii.year = 2017 and ii.doy >  160 
	and pio.ordersk is not null and pio.framesk is not null and pio.framesk <> 0

select * from daily_store_bucket dsb where dsb.sk in (2215151,2298518,2215135)
	
select * from job_history where order_id = '350522';

	select * from vdaily_job where sk = 11050
	order_id = '153433'
	
	
select * from job_history where order_id =  '153433'
	
	
	
	
---- display query
	
select 
ii.doy,
s.store_nk,
ii.invoiceitemid,
e.employeeid,
ii.createddate,
ii.modified,
sp.description,
sp.productid,
pg.productid,
pg.description,
ii.description,
ii.amount_gst,
ii.amount_incl_gst,
ii.discount_gst,
ii.discount_incl_gst,
ii.cost_incl_gst,
ii.cost_gst,
ii.bulkbillref
-- ii.*
from vdaily_invoice ii 
inner join store s on s.sk = ii.storesk
inner join storeproduct sp on sp.sk = ii.productsk and sp.type = 'Frame'
left join employee e on e.sk = ii.employeesk
left join product_gnm pg on pg.sk = sp.product_gnm_sk
where ii.year = 2017 and ii.doy >  160
order by ii.doy


inner join vdaily_job j on j.order_sk = pio.ordersk
where sm.invoice_sk <> 0 and pio.ordersk is not null and pio.framesk is not null and pio.framesk <> 0
;
select * from vdaily_job j inner join pms_invoice_orders pio on pio.ordersk = j.order_sk
where
	pio.ordersk is not null and pio.framesk is not null and pio.framesk <> 0
	
select * 
from vdaily_invoice ii 
inner join vdaily_job j on ii.invoicesk = j. -- and j.product_sk = pio.framesk

select * 
from vdaily_invoice ii 
inner join vdaily_stockmovement sm on sm.invoice_sk = ii.invoicesk 
and sm.dsb_storeproduct_sk = ii.dsb_storeproduct_sk and sm.dsb_product_gnm_sk = sm.dsb_product_gnm_sk

drop index idx_invoiceitemid_invoiceite

create index if not exists idx_invoiceitemid_invoiceite on invoiceitem(invoicesk,invoiceitemid);

create index if not exists idx_invoice_sk_store_stockmovement on store_stockmovement(invoice_sk);

-- match takeon products

select * from supplier


INSERT INTO public.daily_matched_bucket
(invoice_daily_store_bucket_sk, job_daily_store_bucket_sk, stockmove_daily_store_bucket_sk, 
invoice_sk, invoiceitemid, 
job_sk, 
store_stockmovement_sk, 
confidence_stockmovement, confidence_job, confidence_job_stockmovement)
select ii.dsb_sk as invoice_bucket, j.dsb_sk as job_bucket, sm.dsb_sk as sm_bucket, 
ii.invoicesk, invoiceitemid,
j.sk,
sm.sk,
ABS(ii.doy -  sm.doy), ABS(ii.doy - j.doy), ABS(sm.doy - j.doy)
from vdaily_invoice ii 
inner join vdaily_stockmovement sm on sm.invoice_sk = ii.invoicesk and sm.invoice_sk <> 0
inner join pms_invoice_orders pio on pio.invoicesk = ii.invoicesk 
inner join vdaily_job j on j.order_sk = pio.ordersk and j.supplier_sk <> 61 -- exclude zeiss
inner join adjustment_type adj on adj.sk = sm.adjustment_type_sk
where
	ii.dsb_storeproduct_sk = sm.dsb_storeproduct_sk and ii.dsb_product_gnm_sk = sm.dsb_product_gnm_sk and
	ii.dsb_product_gnm_sk = j.dsb_product_gnm_sk and
	adj.name = 'Sale' and ii.year = 2017 and ii.doy >  160 
	and pio.ordersk is not null and pio.framesk is not null and pio.framesk <> 0
	

	INSERT INTO public.daily_matched_bucket
(invoice_daily_store_bucket_sk, job_daily_store_bucket_sk, stockmove_daily_store_bucket_sk, 
invoice_sk, invoiceitemid, 
job_sk, 
store_stockmovement_sk, 
confidence_stockmovement, confidence_job, confidence_job_stockmovement)
select ii.dsb_sk as invoice_bucket, j.dsb_sk as job_bucket, sm.dsb_sk as sm_bucket, 
ii.invoicesk, invoiceitemid,
j.sk,
sm.sk,
ABS(ii.doy -  sm.doy), ABS(ii.doy - j.doy), ABS(sm.doy - j.doy)
select * from vdaily_invoice ii 
inner join vdaily_stockmovement sm on sm.invoice_sk = ii.invoicesk and sm.invoice_sk <> 0
      
INSERT INTO public.daily_matched_bucket
(invoice_daily_store_bucket_sk, job_daily_store_bucket_sk, stockmove_daily_store_bucket_sk, 
invoice_sk, invoiceitemid, 
job_sk, 
store_stockmovement_sk, 
confidence_stockmovement, confidence_job, confidence_job_stockmovement)
select ii.dsb_sk as invoice_bucket, j.dsb_sk as job_bucket, sm.dsb_sk as sm_bucket, 
ii.invoicesk, invoiceitemid,
coalesce(j.sk,0),
coalesce(sm.sk,0),
ABS(ii.doy -  sm.doy), ABS(ii.doy - j.doy), ABS(sm.doy - j.doy)
from vdaily_invoice ii 
left join vdaily_stockmovement sm on sm.invoice_sk = ii.invoicesk and sm.invoice_sk <> 0
left join pms_invoice_orders pio on pio.invoicesk = ii.invoicesk and pio.ordersk is not null and pio.framesk is not null and pio.framesk <> 0
left join vdaily_job j on j.order_sk = pio.ordersk and j.supplier_sk <> 61 -- exclude zeiss
left join adjustment_type adj on adj.sk = sm.adjustment_type_sk and adj.name = 'Sale' 
where
--	ii.dsb_storeproduct_sk = sm.dsb_storeproduct_sk and ii.dsb_product_gnm_sk = sm.dsb_product_gnm_sk and
--	ii.dsb_product_gnm_sk = j.dsb_product_gnm_sk and
	ii.year = 2017 and ii.doy >  160 
--	and j.dsb_sk  is not null
--	and pio.ordersk is not null and pio.framesk is not null and pio.framesk <> 0
	
------------------------------------------------------------------------------------------------------------------------------
--
--	matching invoice item to stock movement
--

INSERT INTO public.daily_matched_bucket
(invoice_daily_store_bucket_sk, job_daily_store_bucket_sk, stockmove_daily_store_bucket_sk, 
invoice_sk, invoiceitemid, 
job_sk, 
store_stockmovement_sk, 
confidence_stockmovement, confidence_job, confidence_job_stockmovement)
select ii.dsb_sk as invoice_bucket, null, sm.dsb_sk as sm_bucket, 
ii.invoicesk, invoiceitemid,
null,
coalesce(sm.sk,0),
ABS(ii.doy -  sm.doy), null, null
from vdaily_invoice ii 
inner join vdaily_stockmovement sm on sm.invoice_sk = ii.invoicesk and sm.invoice_sk <> 0 
	and sm.dsb_store_sk = ii.dsb_store_sk and sm.dsb_product_gnm_sk = ii.dsb_product_gnm_sk and sm.dsb_storeproduct_sk = ii.dsb_storeproduct_sk
inner join adjustment_type adj on adj.sk = sm.adjustment_type_sk and adj.name = 'Sale' 
where
	ii.year = 2017 and ii.doy >  160 
	

select count(*) from daily_matched_bucket

select dsbi.*,dsbsm.*,dmb.*
from daily_matched_bucket dmb 
inner join daily_store_bucket dsbi on dsbi.sk = dmb.invoice_daily_store_bucket_sk
inner join daily_store_bucket dsbsm on dsbsm.sk = dmb.stockmove_daily_store_bucket_sk
	
------------------------------------------------------------------------------------------------------------------------------
--
--	matching invoice item to orders
--


-- what hub orders do not appear in pms_invoice_orders ?
select j.sk,j.product_sk,pio.invoicesk from last_job j 
full outer join pms_invoice_orders pio on pio.ordersk = j.order_sk
where	
	j.sk is not null and pio.invoicesk is not null and pio.invoicesk <> 0;
	
-- what jobs can be matched with invoices that also have stockmovements
select * from daily_matched_bucket dmb inner join
(select j.sk,j.product_sk,pio.invoicesk from last_job j 
full outer join pms_invoice_orders pio on pio.ordersk = j.order_sk
where	
	j.sk is not null and pio.invoicesk is not null and pio.invoicesk <> 0) x on x.invoicesk = dmb.invoice_sk
	
-- what jobs can be matched directly with invoice items based upon productid
select ABS(j.doy - i.doy),i.dsb_sk,i.year,i.doy,i.dsb_store_sk,i.dsb_product_gnm_sk,i.dsb_storeproduct_sk,
j.dsb_sk,j.year,j.doy,j.dsb_store_sk,j.dsb_product_gnm_sk,j.dsb_storeproduct_sk from vdaily_invoice i 
inner join vdaily_job j on j.dsb_product_gnm_sk = i.dsb_product_gnm_sk 
	and j.dsb_store_sk = i.dsb_store_sk and j.dsb_product_gnm_sk <> 0
	and ABS(j.doy - i.doy) < 1 and i.year = 2017 and i.doy > 150 and i.year = j.year 
order by ABS(j.doy - i.doy)


select ABS(j.doy - i.doy),* from vdaily_invoice i 
inner join vdaily_job j on j.dsb_sk = i.dsb_sk
order by ABS(j.doy - i.doy)

--- which product_gnm buckets don't have a corresponding storeproduct lookup

select * from daily_store_bucket dsb
left join daily_store_bucket dsb2 on dsb2.product_gnm_sk = dsb.product_gnm_sk and dsb.doy = dsb2.doy 
and dsb.year = dsb2.year and dsb.store_sk = dsb2.store_sk

select * from storeproduct



-------------------------------------------------------------------------------------------------------------------------------
----- found internak sku in storeproduct that don't match 
--- zero pad to improve matching

select storeid,description, internal_sku,LPAD(internal_sku,8,'0'),
sp.*
-- count(*) 
from storeproduct sp
where internal_sku is not null and internal_sku <> '' and  LPAD(internal_sku,8,'0') not in (select productid from product_gnm) and storeid  = '2850-026'
group by storeid,description


select storeid,description, internal_sku,
sp.*
-- count(*) 
from storeproduct sp
where internal_sku is not null and internal_sku <> '' and internal_sku not in (select internal_sku from supplierproduct) and storeid  = '2850-026'
group by storeid,description

select LPAD(internal_sku,9,'0'),internal_sku from storeproduct where length(internal_sku) < 8 and internal_sku <> ''

------------------------------------------------
-- improved linking of records 
--


--
-- add brand fields to product_gnm
-- re-import product, price_history, 


-- tasks 1.
-- load prepped 
--
alter table product_gnm add column    brand_name character varying(100);
alter table product_gnm add column        consignment bool;
alter table product_gnm add column        apn character varying(100);
alter table product_gnm add column        supplier_sku character varying (100);
alter table product_gnm add column       internal_sku character varying(100);
alter table product_gnm add column       distributor character varying(100);
alter table product_gnm add column       eyetalk_id character varying(100);


begin;
-- get brand apn, sku, consignment
update product_gnm pg set brand_name = b.name, consignment = p.consignment, apn = p.apn, supplier_sku = p.supplier_sku
from gnm.product p
inner join gnm.brand b on b.id = p.brand_id
where p.product_id = pg.productid;

 -- see if eyetalk codes can same us? nope only extras
 -- update product_gnm set eyetalk_id = null;
update product_gnm pg set eyetalk_id = pa.value
from gnm.product p
inner join gnm.brand b on b.id = p.brand_id
inner join gnm.product_attribute pa on pa.name = 'eyetalk_id' and pa.product_id = p.id
where p.product_id = pg.productid;

commit;
select * from product_gnm where brand_name is not null;
select * from product_gnm where eyetalk_id is not null;

select * from gnm.product p 
inner join gnm.product_attribute pa on pa.product_id = p.id
where 
	pa.name = 'eyetalk_id'

select name,value, count(*) from gnm.product_attribute pa group by "name",value order by count(*) desc

--
-- build buckets again as required
-- 
-- update store product

begin;
update storeproduct as tp set product_gnm_sk = gp.sk 
from  public.product_gnm gp 
where tp.internal_sku <> '' AND upper(gp.productid) = upper(LPAD(tp.internal_sku,8,'0'));
commit;

select * from product_gnm pg inner join 
(
select product_gnm_sk,count(*) cnt  
from storeproduct sp 
where sp.type = 'Frame'
group by product_gnm_sk 
) x on x.product_gnm_sk = pg.sk
order by cnt desc




----- 
-- work out the variation on brand, model, color by store

select storeid,upper(sp.frame_brand) brand,upper(sp.frame_model) model,upper(sp.frame_colour) colour, count(*) total
from storeproduct sp -- where product_gnm_sk = 4792
--inner join prod_ref pr on pr.brand = upper(sp.frame_brand) and pr.model = upper(sp.frame_model) and pr.colour = upper(sp.frame_colour)
-- where pr.sk is null
group by storeid,upper(sp.frame_brand),upper(sp.frame_model),upper(sp.frame_colour) 
order by count(*) desc;

------- different types of products in invoices

select sp.type,count(*)
from invoiceitem ii 
inner join storeproduct sp on sp.sk = ii.productsk
where ii.modified > '2017-06-01 00:00:00' 
group by sp.type order by count(*) desc


---- total invoiceitems related to products of type frame
select count(*) from  (
select sp.type
from invoiceitem ii 
inner join storeproduct sp on sp.sk = ii.productsk
where ii.modified > '2017-06-01 00:00:00' and lower(sp.type) = 'frame'
--group by sp.type
) x

-- blank product type
select count(*) from  (
select sp.type,*
from invoiceitem ii 
inner join storeproduct sp on sp.sk = ii.productsk
where ii.modified > '2017-06-01 00:00:00' and lower(sp.type) = ''
--group by sp.type
) x


-- which of this set has a matching stock movement
select sp.type,count(*) from store_stockmovement sm 
left join storeproduct sp on sm.takeon_storeproduct_sk = sp.sk
group by sp.type;

-- which of these is gnm
select sp.type,count(*) from store_stockmovement sm 
left join product_gnm sp on sm.product_gnm_sk = sp.sk
group by sp.type;

-- gnm products going through store product
select pg.type,count(*) from store_stockmovement sm 
left join storeproduct sp on sm.takeon_storeproduct_sk = sp.sk
left join product_gnm pg on pg.sk = sp.product_gnm_sk
group by pg.type;


--------------------------------------------------------------------------------------------------
--- question
-- list of invoices that sell a frame and have stock movement
-- This means they were a sale of floor stock
select count(*) from  (
select ii.storesk,ii.invoiceitemid,ii.cost_gst,a.name,sp.*,sm.*,sh.* from invoiceitem ii 
left join storeproduct sp on sp.sk = ii.productsk
left join store_stockmovement sm on sm.invoice_sk = ii.invoicesk
left join adjustment_type a on a.sk = sm.adjustment_type_sk
left join stock_price_history sh on sh.sk = sm.stock_price_history_sk
where ii.modified > '2017-06-01 00:00:00' and lower(sp.type) = 'frame' and sm.sk is not null  
) x


---------------------------------------------------------------------------------------------------------
----
---- Match the orders to invoice

select count(*) from  (
select ii.storesk,ii.invoiceitemid,ii.cost_gst,sp.*,sj.*,oh.* from invoiceitem ii 
left join storeproduct sp on sp.sk = ii.productsk
left join spectaclejob sj on sj.invoicesk = ii.invoicesk
left join order_history oh on oh.source_id = substring(sj.spectaclejobid,10, length(sj.spectaclejobid))
where ii.modified > '2017-06-01 00:00:00' and lower(sp.type) = 'frame'
and sj.invoicesk is not null and oh.sk is not null
) x

select count(*) 
select * from order_history oh
left join spectaclejob sj on substring(sj.spectaclejobid,10, length(sj.spectaclejobid)) = oh.source_id
where createddate > '2017-06-01 00:00:00' and framesk is not null 
) x


-- select count(*) from order_history;

----------------------------------------------------------------------------------------------------------

select date '0001-01-01' + interval '2016 YEARS' + INTERVAL '230 DAYS'

select count(*) from prod_common

select count(*) from  (
select pr.sk,sp.product_gnm_sk,sp.sk
from storeproduct sp 
inner join prod_ref pr on pr.brand = upper(sp.frame_brand) and pr.model = upper(sp.frame_model) and pr.colour = upper(sp.frame_colour) 
left join prod_common pc on pc.prod_ref_sk = pr.sk and pc.product_gnm_sk=sp.product_gnm_sk and pc.storeproduct_sk = sp.sk
where pc is null and  pr.sk is not null and upper(SP.type) = 'FRAME'
) x

select count(*) from storeproduct where datecreated > '2017-06-01 00:00:00'

select count(*) from (
select prod_ref_sk,product_gnm_sk,count(*) from prod_common group by prod_ref_sk,product_gnm_sk
order by prod_ref_sk,product_gnm_sk,count(*) desc
) x


insert into pms_invoice_orders 
select substring(spectaclejobid,10, length(spectaclejobid)) as orderid, oh.sk, sp.invoicesk, sp.framesk,sp.right_lenssk,sp.left_lenssk,sp.storesk,sp.pms_invoicenum 
from spectaclejob sp 
left join order_history oh on oh.source_id = substring(spectaclejobid,10, length(spectaclejobid))
LEFT JOIN  pms_invoice_orders pio on pio.orderid = substring(spectaclejobid,10, length(spectaclejobid)) and pio.invoicesk = sp.invoicesk
where pio.orderid is null;

-- create some indexes to improve performance

create index if not exists idx_orderid_order_history on order_history(source_id);

create index if not exists idx_orderid_pms_invoice_orders on pms_invoice_orders(orderid);

drop view if exists last_job;
create view last_job as
select jh.* from public.job_history jh
left join 
(select source_id,max(sk) as max_sk,max(source_modified) as max_date, count(*) from public.job_history jh group by source_id) x
on x.max_sk = jh.sk
where x.max_date <> jh.source_modified
order by source_id;



select * from mdaily_stockmovement sm where moving_cost_per_unit is not null


select * from storeproduct sp where productid like '2088-012-%-110' + 'associate rebate with product'

select * from invoiceitem where productsk = 319779

select * from vdaily_stockmovement where adjustment_type = 'Receipt'


select * from adjustment_type


select * from store_stockmovement ssm 
inner join storeproduct sp on sp.sk = ssm.takeon_storeproduct_sk


--- Reports 
--- 1. financials stock movement + gnm price + avg price

--- Matching stockmovement to actual price at time +


---- 2. ?


---- 3. ?


---- 4. ? Actual Stock levels + Avg Price 
--- validate stock price history is correct

1. look at each reciept
- get first reciept for all stock
- get all stock that starts from 0 to a valid reciept to greater than zero

select * from store s;

select count(*) from (
select store_sk,takeon_storeproduct_sk,min(sk) firstsk, max(sk) lastsk, min(adjustmentdate), max(adjustmentdate), count(*), sum(adjustment) 
from store_stockmovement 
group by store_sk,takeon_storeproduct_sk 
having count(*) > 1 --sum(adjustment) > 0
order by store_sk,takeon_storeproduct_sk,count(*) desc
) x
where store_sk = 1


---- Looking at first Stockmovement
select x.store_sk,a.name,count(*) from (
select store_sk,takeon_storeproduct_sk,min(sk) firstsk, max(sk) lastsk, min(adjustmentdate), max(adjustmentdate), count(*), sum(adjustment) 
from store_stockmovement 
group by store_sk,takeon_storeproduct_sk 
--having count(*) > 1 --sum(adjustment) > 0
order by store_sk,takeon_storeproduct_sk,count(*) desc
) x 
inner join store_stockmovement sm on sm.sk = firstsk
inner join adjustment_type a on a.sk = adjustment_type_sk
group by x.store_sk,a.name
order by x.store_sk, count(*) desc

-- of the set of things ... get a good set to work with.

1. of current stock - aka stock in store
2. of last position that hit zero (or less than) 
3. what is the adjustment after hitting zero

----------------------------------------------------------------------

1. for Vita stockmovements with correct pricing.

- for stockmovments that have GNM 


1. association of pricing from each of the fucking things -
-- pricing is correct

            select "FRAMENUM","QTY",s.acquisition_date,
            source,"DESC","LOCATION",
            coalesce(s.sk,0) as store_sk,
            coalesce(tp.sk,0) as takeon_storeproduct_sk,
            coalesce(
              (select sk from public.stock_price_history h
                where (h.takeon_storeproduct_sk = tp.sk)
                order by created desc limit 1)
              ,0) as stock_price_history_sk,
	    aj.sk as adjustment_type_sk
            from stage.today_gnm_sunix_vfrstake
            left outer join public.store s on (s.store_nk = source)
	    inner join public.adjustment_type aj on aj.adjustment_typeid = 'STOCKTAKE'
            inner join public.storeproduct tp on (tp.productid = (s.storeid || '-PF-' || "FRAMENUM") AND internal_sku='')
            where "QTY"<>''
------------------------------------------------------------------------------------------------------------------------------------------
          select s.sk as store_sk,
          i.id,
          coalesce(tp.sk,0) as takeon_storeproduct_sk,
          coalesce(gp.sk,0) as product_gnm_sk,
          actual_qty,
          d.sk as adjustmentdate_sk,
          d.date_actual,
          a.sk as adjustment_type_sk
          from gnm.stocktake_item i
          inner join gnm.stocktake t on (i.stocktake_id = t.id)
          inner join gnm.store ts on (t.store_id = ts.id)
          left outer join public.store s on (s.store_nk = ts.store_id)
          inner join public.storeproduct tp on (tp.productid = (s.storeid || '-PF-' || pms_product_id))
          left outer join public.product_gnm gp on (tp.internal_sku <> '' AND gp.productid = tp.internal_sku)
          left outer join d_date d on (d.date_actual = '2017-01-01')
          inner join adjustment_type a on ('RECEIPT' = a.adjustment_typeid)

 
SELECT id, store_id, planned_stocktake_start_date_time, planned_stocktake_end_date_time, actual_stocktake_start_date_time, actual_stocktake_end_date_time, created_user, created_date_time, modified_user, modified_date_time
FROM gnm.stocktake;

SELECT "FRAMENUM", "EXAVGCOST", "SCANLOC", "ENTRYORDER", "QTY", "BARCODE",
"LOCATION", "RRP", "DIFF", "DESC", "QTYSET", "AVGCOST", "QTYINSTK", 
customer_frame_product_id, "store", "source", "view", gnm_account_code
FROM stage.today_gnm_sunix_vfrstake;


          select s.sk as store_sk,
          i.id,
          coalesce(tp.sk,0) as takeon_storeproduct_sk,
          coalesce(gp.sk,0) as product_gnm_sk,
          actual_qty,
          d.sk as adjustmentdate_sk,
          d.date_actual,
          a.sk as adjustment_type_sk
          from gnm.stocktake_item i
          inner join gnm.stocktake t on (i.stocktake_id = t.id)
          inner join gnm.store ts on (t.store_id = ts.id)
          left outer join public.store s on (s.store_nk = ts.store_id)
          inner join public.storeproduct tp on (tp.productid = (s.storeid || '-PF-' || pms_product_id))
          left outer join public.product_gnm gp on (tp.internal_sku <> '' AND gp.productid = tp.internal_sku)
          left outer join d_date d on d.date_actual = coalesce(s.acquisition_date - interval '1 month')
          inner join adjustment_type a on ('RECEIPT' = a.adjustment_typeid)

          
          select * from stock_price_history
          
          select * from store_stock_history
          
          select max(createddate) from invoiceitem
          
          
         select * from store s 
         
         select * from takeon_product
    
         select * from gnm.stocktake
         
         
          select "ID","NOTE","QTY_NEW","QTY_OLD","CODE","DATE","REASON","LOCATION",source,coalesce(s.sk,0) as store_sk,
          coalesce(tp.sk,0) as takeon_storeproduct_sk,
          coalesce(gp.sk,0) as product_gnm_sk,
          coalesce(j.invoicesk,0) as invoice_sk,
          coalesce(c.sk,0) as customer_sk,
          coalesce(
            (select sk from public.stock_price_history h
              where (h.product_gnm_sk = gp.sk AND gp.sk is not null) AND h.created <= ("DATE"::timestamp)
              order by created desc limit 1)
          ,
            (select sk from public.stock_price_history h
              where (h.takeon_storeproduct_sk = tp.sk) AND h.created <= ("DATE"::timestamp)
              order by created desc limit 1)
          ,0) as stock_price_history_sk,
          coalesce(d.sk,0) as adjustmentdate_sk,
          coalesce(a.sk,0) as adjustment_type_sk
          from
          stage.today_gnm_sunix_vrestock r
          left outer join public.store s on (s.store_nk = source)
          left outer join public.storeproduct tp on (tp.productid = (s.storeid || '-PF-' || "CODE"))
          left outer join public.product_gnm gp on (tp.internal_sku <> '' AND gp.productid = tp.internal_sku)
          left outer join public.spectaclejob j on (("REASON" = 'Sale' OR "REASON" = 'Cancel Sale') AND j.spectaclejobid = (s.storeid || '-' || substring("INVNO",5)))
          left outer join public.customer c on (c.customerid = (s.storeid || '-' || "REFNUM"))
          left outer join d_date d on (TO_CHAR((r."DATE"::timestamp),'yyyymmdd')::INT = d.date_dim_id)
          left outer join adjustment_type a on (upper(r."REASON") = a.adjustment_typeid)
          where ("DATE"::timestamp) >= s.acquisition_date - interval '1 month' AND ("DATE"::timestamp) >= s.acquisition_date AND
          ("DATE"::timestamp) <= now() AND "STOCKTYPE" = 'F'
          
   select * from adjustment_type
   
          
select a.name,sph.*,sm.* from store_stockmovement sm 
inner join adjustment_type a on a.sk = sm.adjustment_type_sk
left join stock_price_history sph on sph.sk = sm.stock_price_history_sk
where
	a.name = 'Receipt' or a.name = 'Stocktake' and sm.product_gnm_sk <> 0
	
	      SELECT source || '-' || "FRAMENUM" as nk,
          coalesce(s.sk,0) as store_sk,
          coalesce(tp.sk,0) as takeon_storeproduct_sk,
          coalesce(gp.sk,0) as product_gnm_sk,
          "QUANTITY"::int as total,
          0 as employee_sk,
          now()::date as source_staged
          FROM stage.today_gnm_sunix_vframe
          left outer join public.store s on (s.store_nk = source)
          left outer join public.storeproduct tp on (tp.productid = (s.storeid || '-PF-' || "FRAMENUM"))
          left outer join public.product_gnm gp on (tp.internal_sku <> '' AND gp.productid = tp.internal_sku)
 ------------------------------------------------------------------------------------------------------------------         
 select * from storeproduct
 select * from store_stock_history
 select * from stage.gnm_sunix_vframe
 
 
            select "FRAMENUM","QTY",s.acquisition_date,
            source,"DESC","LOCATION",
            coalesce(s.sk,0) as store_sk,
            coalesce(tp.sk,0) as takeon_storeproduct_sk,
            coalesce(
              (select sk from public.stock_price_history h
                where (h.takeon_storeproduct_sk = tp.sk)
                order by created desc limit 1)
              ,0) as stock_price_history_sk,
	    aj.sk as adjustment_type_sk
            from stage.today_gnm_sunix_vfrstake
            left outer join public.store s on (s.store_nk = source)
	    	inner join public.adjustment_type aj on aj.adjustment_typeid = 'STOCKTAKE'
            inner join public.storeproduct tp on (tp.productid = (s.storeid || '-PF-' || "FRAMENUM") AND internal_sku='')
            where "QTY"<>''
            
---------- all reciepts and stock takes must have a price associated.
 -- the records without a price associated with them either wholesale or retail
select count(*) from (
            select * from 
            	store_stockmovement sm
            	inner join storeproduct sp on sp.sk = sm.takeon_storeproduct_sk
            	where (sp.wholesaleprice is null or sp.wholesaleprice < 1 ) and (retailprice = 0 or retailprice is null)
) x

----------------------- gimme all records above that are stockmovement or receipt
select count(*) from (
            select * from 
            	store_stockmovement sm
            	inner join storeproduct sp on sp.sk = sm.takeon_storeproduct_sk and sp.type = 'Frame' -- and sp.product_gnm_sk = 0
            	inner join adjustment_type a on a.sk = sm.adjustment_type_sk
            	where (sp.wholesaleprice is null or sp.wholesaleprice < 1 ) and (retailprice = 0 or retailprice is null)
            		and upper(a.name) in ('RECEIPT','STOCKTAKE')
) x

--- same data but using stock_price_history
select count(*) from (
            select sp.productid,sp.product_gnm_sk,sp.wholesaleprice,sp.retailprice,sm.actual_cost,
            sm.actual_cost_value,sm.moving_actual_cost_value,sm.moving_cost_per_unit from 
            	store_stockmovement sm
            	inner join storeproduct sp on sp.sk = sm.takeon_storeproduct_sk and sp.type = 'Frame' --  and sp.product_gnm_sk = 0
            	inner join adjustment_type a on a.sk = sm.adjustment_type_sk
            	left join stock_price_history sph on sph.sk = sm.stock_price_history_sk
            	where upper(a.name) in ('RECEIPT','STOCKTAKE') --and ( stock_price_history_sk is null or stock_price_history_sk = 0)
) x
--- same data but using stock_price_history
select count(*) from (
            select sp.productid,sp.product_gnm_sk,sp.wholesaleprice,sp.retailprice,sph.actual_cost,sm.actual_cost,
            sm.actual_cost_value,sm.moving_actual_cost_value,sm.moving_cost_per_unit from 
            	store_stockmovement sm
            	inner join storeproduct sp on sp.sk = sm.takeon_storeproduct_sk and sp.type = 'Frame' --  and sp.product_gnm_sk = 0
            	inner join adjustment_type a on a.sk = sm.adjustment_type_sk
            	left join stock_price_history sph on sph.sk = sm.stock_price_history_sk
            	where upper(a.name) in ('RECEIPT','STOCKTAKE') and ( stock_price_history_sk is not null and stock_price_history_sk <> 0) 
            	and sph.actual_cost is null
 --           	and sm.actual_cost is not null
) x


---- so relatively few -- so calculate cost for all the above
select count(*) from (
select * from 
store_stockmovement
) x

---
select * from "store"

select sm.takeon_storeproduct_sk,h.actual_cost,max(h.sk),count(*)
from public.stock_price_history h
right join store_stockmovement sm on sm.stock_price_history_sk = h.sk
inner join public.store s on s.sk = h.store_sk
where h.created <= sm.adjustmentdate
group by sm.takeon_storeproduct_sk,h.actual_cost
having count(*) > 1
order by sm.takeon_storeproduct_sk,h.actual_cost 


drop view stage.today_gnm_sunix_vrestock;

select stage.generate_view_for_table('gnm_sunix_vcode');
select stage.generate_view_for_table('gnm_sunix_vframe');
select stage.generate_view_for_table('gnm_sunix_vfrstake');
select stage.generate_view_for_table('gnm_sunix_vrestock');

select * from store


select store_sk,count(*) from public.store_stockmovement 
group by store_sk
;

select count(*) from public.product_gnm;

select count(*) from public.order_history

select runid,createddate,configname,pipename,parameters from stage.gnm_sunix_vframe


-- 6023-037

select * from store;

select store_sk,count(*) from public.store_stockmovement 
group by store_sk
;

select count(*) from public.product_gnm;

select count(*) from public.order_history

select runid,createddate,configname,pipename,parameters from stage.gnm_sunix_vframe 
where createddate in
(select max(createddate) from stage.gnm_sunix_vframe)

	
select source,count(*) from stage.today_gnm_sunix_vrestock group by source order by count(*);

select distinct "INVOICEID","DATE_ADDED1",s.sk as storesk, s.storeid from stage.today_gnm_optomate_v_stock_invoice_item
          inner join public.store s on ((chain || '-' || "BRANCH_IDENTIFIER") = store_nk)
            
select chain,"BRANCH_IDENTIFIER",* from stage.today_gnm_optomate_v_stock_invoice_item



select * from "store"

begin;
update store set store_nk = 'EYELINES-BUR' where storeid = '7320-016';
update store set store_nk = 'EYELINES-DEV' where storeid = '7310-014';
update store set store_nk = 'EYELINES-GLE' where storeid = '7010-017';
update store set store_nk = 'EYELINES-HOB' where storeid = '7000-013';
update store set store_nk = 'EYELINES-HUO' where storeid = '7109-018';
update store set store_nk = 'EYELINES-KIN' where storeid = '7050-019';
update store set store_nk = 'EYELINES-KM' where storeid = '7249-020';
update store set store_nk = 'EYELINES-LAU' where storeid = '7250-015';
update store set store_nk = 'EYELINES-LEG' where storeid = '7277-021';
--NN	
update store set store_nk = 'EYELINES-PRO' where storeid = '7250-022';
update store set store_nk = 'EYELINES-ROS' where storeid = '7018-023';
update store set store_nk = 'EYELINES-SOR' where storeid = '7172-024';
--TAS
--ADM

commit;

select * from store s
inner join store_stockmovement sm on sm.store_sk = s.sk
where pms = 'Optomate';
---------------------------
---------------------------------------------------------------------------
            select
            0 as store_sk,
            0 as takeon_storeproduct_sk,
            coalesce(p.sk,0) as product_gnm_sk,
            coalesce(supplier_cost,0) as supplier_cost,
            coalesce(supplier_cost_gst,0) as supplier_cost_gst,
            coalesce(actual_cost,0) as actual_cost,
            coalesce(actual_cost_gst,0) as actual_cost_gst,
            coalesce(supplier_rrp,0) as supplier_rrp,
            coalesce(supplier_rrp_gst,0) as supplier_rrp_gst,
            coalesce(retail_price,0) as retail_price,
            coalesce(retail_price_gst,0) as retail_price_gst,
            ('GNM-' || id) as source_id,
            effective_from_time as created
            from public.product_price_history h
            left outer join public.storeproduct p
            on (p.productid = h.product_id AND p.storeid = '0')
            
            
            
            
  select "ID","NOTE","QTY_NEW","QTY_OLD","CODE","DATE","REASON","LOCATION",source,coalesce(s.sk,0) as store_sk,
          coalesce(tp.sk,0) as takeon_storeproduct_sk,
          coalesce(gp.sk,0) as product_gnm_sk,
          coalesce(j.invoicesk,0) as invoice_sk,
          coalesce(c.sk,0) as customer_sk,
          coalesce(
            (select sk from public.stock_price_history h
              where (h.product_gnm_sk = gp.sk AND gp.sk is not null) AND h.created <= ("DATE"::timestamp)
              order by created desc limit 1)
          ,
            (select sk from public.stock_price_history h
              where (h.takeon_storeproduct_sk = tp.sk) AND h.created <= ("DATE"::timestamp)
              order by created desc limit 1)
          ,0) as stock_price_history_sk,
          coalesce(d.sk,0) as adjustmentdate_sk,
          coalesce(a.sk,0) as adjustment_type_sk
          from
          stage.today_gnm_sunix_vrestock r
          left outer join public.store s on (s.store_nk = source)
          left outer join public.storeproduct tp on (tp.productid = (s.storeid || '-PF-' || "CODE"))
          left outer join public.product_gnm gp on (tp.internal_sku <> '' AND gp.productid = tp.internal_sku)
          left outer join public.spectaclejob j on (("REASON" = 'Sale' OR "REASON" = 'Cancel Sale') AND j.spectaclejobid = (s.storeid || '-' || substring("INVNO",5)))
          left outer join public.customer c on (c.customerid = (s.storeid || '-' || "REFNUM"))
          left outer join d_date d on (TO_CHAR((r."DATE"::timestamp),'yyyymmdd')::INT = d.date_dim_id)
          left outer join adjustment_type a on (upper(r."REASON") = a.adjustment_typeid)
          where ("DATE"::timestamp) >= '2017-07-01' AND ("DATE"::timestamp) >= s.acquisition_date AND
          ("DATE"::timestamp) <= now() AND "STOCKTYPE" = 'F'
          
          
          
          
          
select "FRAMENUM","QTY",s.acquisition_date,
            source,"DESC","LOCATION",
            coalesce(s.sk,0) as store_sk,
            coalesce(tp.sk,0) as takeon_storeproduct_sk,
            coalesce(
              (select sk from public.stock_price_history h
                where (h.takeon_storeproduct_sk = tp.sk)
                order by created desc limit 1)
              ,0) as stock_price_history_sk,
	    aj.sk as adjustment_type_sk
from stage.today_gnm_sunix_vfrstake
     left outer join public.store s on (s.store_nk = source)
	 inner join public.adjustment_type aj on aj.adjustment_typeid = 'STOCKTAKE'
     inner join public.storeproduct tp on (tp.productid = (s.storeid || '-PF-' || "FRAMENUM") AND internal_sku='')
where "QTY"<>''
            
 select count(*) from           
            (select
            coalesce(s.sk,0) as store_sk,
            s.acquisition_date,
            coalesce(p.sk,0) as takeon_storeproduct_sk,
            "LISTPRICE",
            "EXLISTPR",
            "COSTPRICE",
            "EXCOSTPR",
            "RRP",
            s.storeid,
            "FRAMENUM"
            from stage.today_gnm_sunix_vframe f
            inner join public.store s on (s.store_nk = f.source)
            left outer join public.storeproduct p on (p.productid = (s.storeid || '-PF-' || "FRAMENUM"))
            where "SUPPLIER" <> 'GNM') z



select * 
from stage.today_gnm_sunix_vfrstake
--------------------------------------------------------------------------------------------------------------------------

--- seed averge cost calc

	select 
    	sk,
        adjustmentdate,
        store_sk,
          product_gnm_sk,
          adjustment,
          actual_cost::numeric(19,2),
          (adjustment * actual_cost)::numeric as actual_cost_value,
          adjustment as moving_quantity,
          (adjustment * actual_cost)::numeric as moving_actual_cost_value,
          actual_cost::numeric(19,2) as moving_cost_per_unit,
		(select im.sk from public.store_stockmovement im
            where im.product_gnm_sk = Y.product_gnm_sk and im.store_sk = Y.store_sk and
            ((im.adjustmentdate = Y.adjustmentdate and im.sk > Y.sk) or im.adjustmentdate > Y.adjustmentdate)
            order by adjustmentdate,im.sk limit 1
            ) next_sk
    from (
        select sk,adjustmentdate,store_sk,product_gnm_sk,adjustment,gnm_actual_cost as actual_cost,
            row_number() over (partition by store_sk,product_gnm_sk order by adjustmentdate,sk ) r
        from (
          --- match the stockmovement to the pricing for that period
          select m.sk,m.adjustmentdate,m.store_sk,m.product_gnm_sk,m.adjustment,
                      (select actual_cost from public.stock_price_history h
                          where (h.product_gnm_sk = m.product_gnm_sk) AND h.created <= m.adjustmentdate
                          order by created desc limit 1) as gnm_actual_cost
          from public.store_stockmovement m
          inner join public.adjustment_type a on m.adjustment_type_sk = a.sk and a.name = 'Receipt'
          where product_gnm_sk <> 0
       ) T where T.gnm_actual_cost is not null
     ) Y where r = 1
          
          
        select sk,adjustmentdate,store_sk,product_gnm_sk,adjustment,gnm_actual_cost as actual_cost,
            row_number() over (partition by store_sk,product_gnm_sk order by adjustmentdate,sk ) r
        from (
          --- match the stockmovement to the pricing for that period
          select m.sk,m.adjustmentdate,m.store_sk,m.product_gnm_sk,m.adjustment,
                      (select actual_cost from public.stock_price_history h
                          where (h.product_gnm_sk = m.product_gnm_sk) AND h.created <= m.adjustmentdate
                          order by created desc limit 1) as gnm_actual_cost
          from public.store_stockmovement m
          inner join public.adjustment_type a on m.adjustment_type_sk = a.sk and a.name = 'Receipt'
          where product_gnm_sk <> 0
       ) T where T.gnm_actual_cost is not null
       order by store_sk,product_gnm_sk,adjustmentdate

      select * from x 
      
 select * from store_stockmovement;
 
 select  row_number() over (partition by takeon_storeproduct_sk order by adjustmentdate,sk) r,*
 from store_stockmovement sm 
 inner join adjustment_type 
 inner join (select takeon_storeproduct_sk tsk from store_stockmovement group by takeon_storeproduct_sk having count(*) > 1) x on x.tsk = sm.takeon_storeproduct_sk
 order by takeon_storeproduct_sk, r
      
 
with recursive cogs(
	sk,
	adjustmentdate,
	takeon_storeproduct_sk,
	adjustment,
	actual_cost,
	actual_cost_value,
	moving_quantity,
	moving_actual_cost_value,
	moving_cost_per_unit,
	next_sk
) as(
	select
		sk,
		adjustmentdate,
		takeon_storeproduct_sk,
		adjustment,
		actual_cost::numeric(
			19,
			2
		),
		(
			adjustment * actual_cost
		)::numeric as actual_cost_value,
		adjustment as moving_quantity,
		(
			adjustment * actual_cost
		)::numeric as moving_actual_cost_value,
		actual_cost::numeric(
			19,
			2
		) as moving_cost_per_unit,
		(
			select
				im.sk
			from
				public.store_stockmovement im
			where
				im.takeon_storeproduct_sk = Y.takeon_storeproduct_sk
				and(
					(
						im.adjustmentdate = Y.adjustmentdate
						and im.sk > Y.sk
					)
					or im.adjustmentdate > Y.adjustmentdate
				)
			order by
				adjustmentdate,
				im.sk limit 1
		),
		r
	from
		(
			select
				sk,
				adjustmentdate,
				takeon_storeproduct_sk,
				adjustment,
				gnm_actual_cost as actual_cost,
				row_number() over(
					partition by takeon_storeproduct_sk
				order by
					adjustmentdate,
					sk
				) r
			from
				(
					select
						m.sk,
						m.adjustmentdate,
						takeon_storeproduct_sk,
						m.adjustment,
						(
							select
								actual_cost
							from
								public.stock_price_history h
							where
								(
									h.takeon_storeproduct_sk = m.takeon_storeproduct_sk
								)
								and h.created <= m.adjustmentdate
							order by
								created desc limit 1
						) as gnm_actual_cost
					from
						public.store_stockmovement m inner join public.adjustment_type a on
						(
							m.adjustment_type_sk = a.sk
							and a.name in('Receipt')
						) -- where product_gnm_sk =0
				) T
			where
				T.gnm_actual_cost is not null
		) Y
	where
		r > 1
union all select
		sk,
		adjustmentdate,
		takeon_storeproduct_sk,
		adjustment,
		actual_cost::numeric(
			19,
			2
		),
		(
			adjustment * actual_cost
		)::numeric(
			19,
			2
		),
		prev_moving_quantity + adjustment,
		(
			prev_moving_actual_cost_value +(
				adjustment * actual_cost
			)
		)::numeric(
			19,
			2
		),
		(
			case
				when is_receipt = true
				and(
					prev_moving_quantity + adjustment
				)<> 0 then(
					prev_moving_actual_cost_value +(
						adjustment * actual_cost
					)
				)/(
					prev_moving_quantity + adjustment
				)
				else actual_cost
			end
		)::numeric(
			19,
			2
		),
		next_sk
	from
		(
			select
				m.sk,
				m.adjustmentdate,
				m.takeon_storeproduct_sk,
				m.adjustment,
				c.moving_cost_per_unit as prev_moving_cost_per_unit,
				c.moving_quantity as prev_moving_quantity,
				c.moving_actual_cost_value as prev_moving_actual_cost_value,
				(
					case
						when name = 'Receipt' then(
							select
								actual_cost
							from
								public.stock_price_history h
							where
								(
									h.takeon_storeproduct_sk = m.takeon_storeproduct_sk
								)
								and h.created <= m.adjustmentdate
								and actual_cost is not null
							order by
								created desc limit 1
						)
						else c.moving_cost_per_unit
					end
				) as actual_cost,
				(
					case
						when name = 'Receipt' then true
						else false
					end
				) as is_receipt,
				(
					select
						im.sk
					from
						public.store_stockmovement im
					where
						im.takeon_storeproduct_sk = m.takeon_storeproduct_sk
						and(
							(
								im.adjustmentdate = m.adjustmentdate
								and im.sk > m.sk
							)
							or im.adjustmentdate > m.adjustmentdate
						)
					order by
						im.adjustmentdate,
						im.sk limit 1
				) as next_sk
			from
				cogs c,
				public.store_stockmovement m inner join public.adjustment_type a on
				(
					m.adjustment_type_sk = a.sk
				)
			where
				c.next_sk = m.sk
				and c.next_sk is not null
		) T
)

          select sk,actual_cost,actual_cost_value,moving_quantity,moving_actual_cost_value,moving_cost_per_unit from cogs
          
          
          select * from store_stockmovement where adjustmentdate_sk is null
           select * from store_stockmovement where adjustmentdate is null
 -------------------------------------------------------------------------------------------------------         
          
          select sm.store_sk, sm.product_gnm_sk, sm.nk, 
          a.name
          ,sm.moving_quantity
          ,moving_actual_cost_value
          ,moving_cost_per_unit
          ,adjustment
          ,actual_cost
          ,actual_cost_value
          
          ,case product_gnm_sk  when 0 then actual_cost_value
          						else moving_cost_per_unit end as cogs_cost
          ,case product_gnm_sk  when 0 then actual_cost_value / 10
          						else moving_cost_per_unit / 10 end as cogs_tax_cost 
          , invoice_sk
          from store_stockmovement sm 
          	inner join adjustment_type a on a.sk = sm.adjustment_type_sk
          	inner join d_date d on d.sk = adjustmentdate_sk
          	where a.name = 'Sale' 
          	and moving_quantity > 1
   	
          	-- select * from store_stockmovement  
          
          	
--- Breakdown of stock movements, the first type of Adjustment on Record        
select name, count(*) from (      	
         select t.sk as productsk, t.product_gnm_sk, t.first_stockmove, a.name, sm.nk, sm.adjustmentdate, sm.adjustment, sm.reason
         from  	
	         (select sp.sk,sp.product_gnm_sk,
	         	(select sm.sk from store_stockmovement sm  
		         	where sm.takeon_storeproduct_sk = sp.sk 
		         	order by sm.sk,sm.adjustmentdate limit 1) as first_stockmove
	         from storeproduct sp ) t 
        	 inner join store_stockmovement sm on sm.sk = t.first_stockmove
        	 inner join adjustment_type a on a.sk = sm.adjustment_type_sk
) x group by name order by count(*) desc 	 ;
          	
-- Get the first Adjustment that is a Reciept or Stocktake
select count(*) from  (
 		select productsk,count(*)
 		from 
 		(select t.sk as productsk, t.product_gnm_sk, t.first_stockmove, a.name, sm.nk, sm.adjustmentdate, sm.adjustment, sm.reason
         from  	
	         (select sp.sk,sp.product_gnm_sk,
	         	(select sm.sk from store_stockmovement sm  
	         		inner join adjustment_type a on a.sk = sm.adjustment_type_sk
	         	where sm.takeon_storeproduct_sk = sp.sk and a.name in ('Stocktake','Recept')
	         	order by sm.sk,sm.adjustmentdate limit 1) as first_stockmove
	         from storeproduct sp ) t 
        	 inner join store_stockmovement sm on sm.sk = t.first_stockmove
        	 inner join adjustment_type a on a.sk = sm.adjustment_type_sk) stock1
        	 group by productsk
) x;

--- baseline total number of store products in the system
select count(*) from storeproduct;

-- get the total number of storeproducts that have a stockmovement associated with them
select count(*) from storeproduct sp 
	where sp.sk in (select distinct takeon_storeproduct_sk from store_stockmovement where adjustmentdate > '2016-06-01') and sp.product_gnm_sk = 0;

-- get the total number of storeproducts that have a stockmovement associated with them that gnm
select count(*) from storeproduct sp 
	where sp.sk in (select distinct takeon_storeproduct_sk from store_stockmovement where adjustmentdate > '2016-06-01') and sp.product_gnm_sk <> 0;
        	

          	
CREATE TABLE store_stock_history_adj (
    sk serial NOT NULL,
    store_sk integer NOT NULL,
    nk character varying(100),
    takeon_storeproduct_sk integer REFERENCES storeproduct(sk),
    total integer NOT NULL,
 --   prior_total integer NOT NULL,
 --   employee_sk integer,
    source_staged timestamp without time zone NOT NULL,
    created timestamp without time zone DEFAULT now(),
    product_gnm_sk integer REFERENCES product_gnm(sk),
    primary key(sk)
);

--- Recursive view for stock history
create view view_adjustment_price_history as
with recursive adjustment_price_history as (

--	select (select sk from store_stockmovement s where s.takeon_storeproduct_sk = ssm.takeon_storeproduct_sk order by s.sk limit 1) firstsk	from store_stockmovement ssm
	select takeon_storeproduct_sk, adjustmentdate, adjustmentdate_sk, adjustment_type_sk,  actual_cost,actual_cost_value, moving_quantity, 
	moving_cost_per_unit 
	from store_stockmovement ssm 
	where sk in
	(select min(sk) from store_stockmovement ssm where moving_cost_per_unit is not null group by ssm.takeon_storeproduct_sk)
	
union all	
	select takeon_storeproduct_sk,adjustmentdate_sk, adjustment_type_sk,  actual_cost,actual_cost_value, moving_quantity, 
	moving_cost_per_unit 
	from adjustment_price_history aph
	inner join 
		(select takeon_storeproduct_sk,adjustmentdate_sk, adjustment_type_sk,  actual_cost,actual_cost_value, moving_quantity, moving_cost_per_unit
			from store_stockmovement sm 
				inn
			where )
)
select takeon_storeproduct_sk,adjustmentdate_sk, adjustment_type_sk,  actual_cost,actual_cost_value, moving_quantity, 
	moving_cost_per_unit 
from adjustment_price_history 
order by date_actual;


/*

1. stockmovements stocktake import and price reset
2. adjustment price history view for david

3. Today -  deploy those last 2 and 

*/

          
          ---------------------------------------------------------------------------------------------------------------------------------------
          

          WITH RECURSIVE cogs(sk,adjustmentdate,store_sk,product_gnm_sk,adjustment,actual_cost,actual_cost_value,moving_quantity,moving_actual_cost_value,moving_cost_per_unit,next_sk) AS (

          select sk,
          adjustmentdate,
          store_sk,
          product_gnm_sk,
          adjustment,
          actual_cost::numeric(19,2),
          (adjustment * actual_cost)::numeric as actual_cost_value,
          adjustment as moving_quantity,
          (adjustment * actual_cost)::numeric as moving_actual_cost_value,
          actual_cost::numeric(19,2) as moving_cost_per_unit,
          (select im.sk from public.store_stockmovement im
            where im.product_gnm_sk = Y.product_gnm_sk and im.store_sk = Y.store_sk and
            ((im.adjustmentdate = Y.adjustmentdate and im.sk > Y.sk) or im.adjustmentdate > Y.adjustmentdate)
            order by adjustmentdate,im.sk limit 1
            )
            from (
            select sk,adjustmentdate,store_sk,product_gnm_sk,adjustment,gnm_actual_cost as actual_cost,
            row_number() over (partition by store_sk,product_gnm_sk order by adjustmentdate,sk ) r
            from (
              select m.sk,m.adjustmentdate,m.store_sk,m.product_gnm_sk,m.adjustment,
                      (select actual_cost from public.stock_price_history h
                          where (h.product_gnm_sk = m.product_gnm_sk) AND h.created <= m.adjustmentdate
                          order by created desc limit 1) as gnm_actual_cost
          from public.store_stockmovement m
          inner join public.adjustment_type a on (m.adjustment_type_sk = a.sk and a.name = 'Receipt')
          where product_gnm_sk <> 0
          ) T where T.gnm_actual_cost is not null
          ) Y where r = 1


          UNION ALL

          select sk,adjustmentdate,store_sk,product_gnm_sk,adjustment,
          actual_cost::numeric(19,2),
          (adjustment * actual_cost)::numeric(19,2),
          prev_moving_quantity + adjustment,
          (prev_moving_actual_cost_value + (adjustment * actual_cost))::numeric(19,2),
          (CASE WHEN is_receipt = true AND (prev_moving_quantity + adjustment) <> 0 THEN
          (prev_moving_actual_cost_value + (adjustment * actual_cost)) / (prev_moving_quantity + adjustment)
           ELSE actual_cost END)::numeric(19,2),
          next_sk
          from (
          select m.sk,
          m.adjustmentdate,
          m.store_sk,
          m.product_gnm_sk,
          m.adjustment,
          c.moving_cost_per_unit as prev_moving_cost_per_unit,
          c.moving_quantity as prev_moving_quantity,
          c.moving_actual_cost_value as prev_moving_actual_cost_value,
          (CASE WHEN name = 'Receipt' THEN
            (select actual_cost from public.stock_price_history h
               where (h.product_gnm_sk = m.product_gnm_sk) AND h.created <= m.adjustmentdate
               AND actual_cost is not null
               order by created desc limit 1)
           ELSE c.moving_cost_per_unit END) as actual_cost,
          (CASE WHEN name = 'Receipt' THEN true else false END) as is_receipt,
          (select im.sk from public.store_stockmovement im
          where im.product_gnm_sk = m.product_gnm_sk and  im.store_sk = m.store_sk and
          ((im.adjustmentdate = m.adjustmentdate and im.sk > m.sk) or im.adjustmentdate > m.adjustmentdate)
          order by im.adjustmentdate,im.sk limit 1
          ) as next_sk
          from cogs c, public.store_stockmovement m
          inner join public.adjustment_type a on (m.adjustment_type_sk = a.sk)
          where c.next_sk = m.sk and c.next_sk is not null
          ) T

          )

          select sk,store_sk,actual_cost,actual_cost_value,moving_quantity,moving_actual_cost_value,moving_cost_per_unit from cogs
 
          
          ---------------------------------------------------------------------------------------------------------------------------------------
          
          
           WITH RECURSIVE cogs(sk,adjustmentdate,takeon_storeproduct_sk,adjustment,actual_cost,actual_cost_value,
           moving_quantity,moving_actual_cost_value,moving_cost_per_unit,next_sk) AS (

          select sk,
          adjustmentdate,
          takeon_storeproduct_sk,
          adjustment,
          actual_cost::numeric(19,2),
          (adjustment * actual_cost)::numeric as actual_cost_value,
          adjustment as moving_quantity,
          (adjustment * actual_cost)::numeric as moving_actual_cost_value,
          actual_cost::numeric(19,2) as moving_cost_per_unit,
          (select im.sk from public.store_stockmovement im
          where im.takeon_storeproduct_sk = Y.takeon_storeproduct_sk and
          ((im.adjustmentdate = Y.adjustmentdate and im.sk > Y.sk) or im.adjustmentdate > Y.adjustmentdate)
          order by adjustmentdate,im.sk limit 1
          )
          from (
          select sk,adjustmentdate,takeon_storeproduct_sk,adjustment,gnm_actual_cost as actual_cost, row_number() over (partition by takeon_storeproduct_sk order by adjustmentdate,sk ) r from (
          select m.sk,m.adjustmentdate,takeon_storeproduct_sk,m.adjustment,
          (select actual_cost from public.stock_price_history h
                        where (h.takeon_storeproduct_sk = m.takeon_storeproduct_sk) AND h.created <= m.adjustmentdate
                        order by created desc limit 1) as gnm_actual_cost
          from public.store_stockmovement m
          inner join public.adjustment_type a on (m.adjustment_type_sk = a.sk and a.name = 'Receipt')
          where product_gnm_sk =0
          ) T where T.gnm_actual_cost is not null
          ) Y where r = 1


          UNION ALL

          select sk,adjustmentdate,takeon_storeproduct_sk,adjustment,
          actual_cost::numeric(19,2),
          (adjustment * actual_cost)::numeric(19,2),
          prev_moving_quantity + adjustment,
          (prev_moving_actual_cost_value + (adjustment * actual_cost))::numeric(19,2),
          (CASE WHEN is_receipt = true AND (prev_moving_quantity + adjustment) <> 0 THEN
          (prev_moving_actual_cost_value + (adjustment * actual_cost)) / (prev_moving_quantity + adjustment)
           ELSE actual_cost END)::numeric(19,2),
          next_sk
          from (
          select m.sk,
          m.adjustmentdate,
          m.takeon_storeproduct_sk,
          m.adjustment,
          c.moving_cost_per_unit as prev_moving_cost_per_unit,
          c.moving_quantity as prev_moving_quantity,
          c.moving_actual_cost_value as prev_moving_actual_cost_value,
          (CASE WHEN name = 'Receipt' THEN
           (select actual_cost from public.stock_price_history h
              where (h.takeon_storeproduct_sk = m.takeon_storeproduct_sk) AND h.created <= m.adjustmentdate
              AND actual_cost is not null
              order by created desc limit 1)
           ELSE c.moving_cost_per_unit END) as actual_cost,
          (CASE WHEN name = 'Receipt' THEN true else false END) as is_receipt,
          (select im.sk from public.store_stockmovement im
          where im.takeon_storeproduct_sk = m.takeon_storeproduct_sk and
          ((im.adjustmentdate = m.adjustmentdate and im.sk > m.sk) or im.adjustmentdate > m.adjustmentdate)
          order by im.adjustmentdate,im.sk limit 1
          ) as next_sk
          from cogs c, public.store_stockmovement m
          inner join public.adjustment_type a on (m.adjustment_type_sk = a.sk)
          where c.next_sk = m.sk and c.next_sk is not null
          ) T

          )

          select sk,actual_cost,actual_cost_value,moving_quantity,moving_actual_cost_value,moving_cost_per_unit from cogs
         
          
          
          ---------------------------------------------------------------------------------------------------------------------------------------
 /*
      		- import order history & job history (how to handle changed data) - add to datapipes the sql
      			- why is it creating duplicates?
      		- import optomate data - incrementally - use lookback, and avoid duplicates
      			- remote terminal, set schedule for sync, after wang
      			- after each where modified_time >= (current_time - offset)
      
          1. set initial price for all products
          		- rules for takeon & off catalog are the same ? take from pms cost
          			
          		- rules for gnm - take from history.
          			1. History is used to show period price history
          			2. Else use History as an audit log, insert a reset Price entry
          				- Average price is calculated per store stock entry
          				- Read only approach, Reset Price Entry approach
          				G
          
          				
          2. Import Stock movements
          		- sunix
          		- optomate
          
          3. Calculate Stock levels
          	- Stock entry table import
          	- use it set base stock level
          	
          	
          
*/     
          
          
          select "DATE", count(*) from stage.today_gnm_sunix_vrestock group by "DATE" order by "DATE" desc

          select * from today_gnm_sunix_vframe
          
          select * from store_stockmovement;
          
          select * from adjustment_type
          
          
         select * from stock_price_history
 
         
         
set role scire;

drop table adjustment_price_history;

CREATE TABLE adjustment_price_history (
    sk serial NOT NULL,
 	storeproduct_sk integer REFERENCES storeproduct(sk),
 	product_gnm_sk integer references product_gnm(sk),
 	
    adjusteddate timestamp without time zone not null,
    adjusteddate_sk integer references d_date(sk),
 
    actual_cost numeric(19,2), 
    actual_cost_value numeric(19,2),
    moving_cost_per_unit numeric(19,2),
    moving_cost_per_unit_gst numeric(19,2),
    
    created timestamp without time zone DEFAULT now(),

    primary key(sk)
);


  insert into adjustment_price_history (storeproduct_sk,product_gnm_sk,adjusteddate,adjusteddate_sk,
  				actual_cost,actual_cost_value,moving_cost_per_unit,moving_cost_per_unit_gst)	
  select takeon_storeproduct_sk, sp.product_gnm_sk, adjustmentdate,adjustmentdate_sk, actual_cost, actual_cost_value,moving_cost_per_unit,moving_cost_per_unit/10
  from store_stockmovement sm
  inner join adjustment_type a on a.sk = sm.adjustment_type_sk
  inner join storeproduct sp on sp.sk = sm.takeon_storeproduct_sk
  where a.name = 'Receipt' and moving_cost_per_unit is not null and adjustmentdate_sk <> 0
  group by takeon_storeproduct_sk,sp.product_gnm_sk, adjustmentdate,adjustmentdate_sk, actual_cost, actual_cost_value, moving_cost_per_unit
 
--                    having count(*) > 1
--   order by adjustmentdate_sk asc

          
          select * from  adjustment_price_history
          
          
      select * from daily_gnm_totals order by date_actual desc limit 100;
      
      
      1. things to do mothr
      
      

      
      
      
      
      DROP MATERIALIZED VIEW public.last_takeon_product_price;

CREATE MATERIALIZED VIEW public.last_takeon_product_price
TABLESPACE pg_default
AS
 SELECT pgs.sk,
    pgs.store_sk,
    pgs.nk,
    pgs.takeon_storeproduct_sk,
    pgs.supplier_cost,
    pgs.supplier_cost_gst,
    pgs.actual_cost,
    pgs.actual_cost_gst,
    pgs.supplier_rrp,
    pgs.supplier_rrp_gst,
    pgs.calculated_retail_price,
    pgs.calculated_retail_price_gst,
    pgs.override_retail_price,
    pgs.override_retail_price_gst,
    pgs.retail_price,
    pgs.retail_price_gst,
    pgs.created,
    pgs.product_gnm_sk
   FROM stock_price_history pgs
     JOIN ( SELECT stock_price_history.takeon_storeproduct_sk,
            max(stock_price_history.sk) AS maxsk
           FROM stock_price_history
          GROUP BY stock_price_history.takeon_storeproduct_sk) x ON x.maxsk = pgs.sk
WITH DATA;


---- non gnm stock that doesnt have a price history
select * from storeproduct sp 
left join stock_price_history sph on sph.takeon_storeproduct_sk = sp.sk
where 
	sph.sk is null and sp.product_gnm_sk = 0 

--- gnm stock that doesnt have a price history through the takeon key process
select * from storeproduct sp 
left join stock_price_history sph on sph.takeon_storeproduct_sk = sp.sk
where 
	sph.sk is null and sp.product_gnm_sk <> 0

-- gnm stock that doesnt have a price history -- there is none
select * from storeproduct sp 
left join stock_price_history sph on sph.product_gnm_sk = sp.product_gnm_sk
where
	sph.sk is null and sp.product_gnm_sk <> 0
	


	
