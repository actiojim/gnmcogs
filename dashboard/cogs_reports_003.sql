
select count(*) from (
          select sk,
          adjustmentdate,
          store_sk,
          product_gnm_sk,
          adjustment,
          actual_cost::numeric(19,2),
          (adjustment * actual_cost)::numeric as actual_cost_value,
          adjustment as moving_quantity,
          (adjustment * actual_cost)::numeric as moving_actual_cost_value,
          actual_cost::numeric(19,2) as moving_cost_per_unit,
          (select im.sk from public.store_stockmovement im
            where im.product_gnm_sk = Y.product_gnm_sk and im.store_sk = Y.store_sk and
            ((im.adjustmentdate = Y.adjustmentdate and im.sk > Y.sk) or im.adjustmentdate > Y.adjustmentdate)
            order by adjustmentdate,im.sk limit 1
            )
            from (
            select sk,adjustmentdate,store_sk,product_gnm_sk,adjustment,gnm_actual_cost as actual_cost,
            row_number() over (partition by store_sk,product_gnm_sk order by adjustmentdate,sk ) r
            from (
              select m.sk,m.adjustmentdate,m.store_sk,m.product_gnm_sk,m.adjustment,
                      (select actual_cost from public.stock_price_history h
                          where (h.product_gnm_sk = m.product_gnm_sk) AND h.created <= m.adjustmentdate
                          order by created desc limit 1) as gnm_actual_cost
          from public.store_stockmovement m
          inner join public.adjustment_type a on (m.adjustment_type_sk = a.sk and a.name in ('Stocktake','Receipt'))
          where product_gnm_sk <> 0
          ) T where T.gnm_actual_cost is not null
          ) Y where r = 1
) x

---- too many moving cost avg is null 
select count(*) from (
select * from store_stockmovement sm 
inner join adjustment_type a on a.sk = sm.adjustment_type_sk
left join stock_price_history sph on sph.sk = sm.stock_price_history_sk
where upper(a.name) = 'SALE' 
and sph.actual_cost is not null
-- and sm.moving_actual_cost_value is not null
) x 


select count(*) from (
          WITH RECURSIVE cogs(sk,adjustmentdate,store_sk,product_gnm_sk,adjustment,actual_cost,actual_cost_value,moving_quantity,moving_actual_cost_value,moving_cost_per_unit,next_sk) AS (

          select sk,
          adjustmentdate,
          store_sk,
          product_gnm_sk,
          adjustment,
          actual_cost::numeric(19,2),
          (adjustment * actual_cost)::numeric as actual_cost_value,
          adjustment as moving_quantity,
          (adjustment * actual_cost)::numeric as moving_actual_cost_value,
          actual_cost::numeric(19,2) as moving_cost_per_unit,
          (select im.sk from public.store_stockmovement im
            where im.product_gnm_sk = Y.product_gnm_sk and im.store_sk = Y.store_sk and
            ((im.adjustmentdate = Y.adjustmentdate and im.sk > Y.sk) or im.adjustmentdate > Y.adjustmentdate)
            order by adjustmentdate,im.sk limit 1
            )
            from (
            select sk,adjustmentdate,store_sk,product_gnm_sk,adjustment,gnm_actual_cost as actual_cost,
            row_number() over (partition by store_sk,product_gnm_sk order by adjustmentdate,sk ) r
            from (
              select m.sk,m.adjustmentdate,m.store_sk,m.product_gnm_sk,m.adjustment,
                      (select actual_cost from public.stock_price_history h
                          where (h.product_gnm_sk = m.product_gnm_sk) AND h.created <= m.adjustmentdate
                          order by created desc limit 1) as gnm_actual_cost
          from public.store_stockmovement m
          inner join public.adjustment_type a on (m.adjustment_type_sk = a.sk and a.name = 'Receipt')
          where product_gnm_sk <> 0
          ) T where T.gnm_actual_cost is not null
          ) Y where r = 1


          UNION ALL

          select sk,adjustmentdate,store_sk,product_gnm_sk,adjustment,
          actual_cost::numeric(19,2),
          (adjustment * actual_cost)::numeric(19,2),
          prev_moving_quantity + adjustment,
          (prev_moving_actual_cost_value + (adjustment * actual_cost))::numeric(19,2),
          (CASE WHEN is_receipt = true AND (prev_moving_quantity + adjustment) <> 0 THEN
          (prev_moving_actual_cost_value + (adjustment * actual_cost)) / (prev_moving_quantity + adjustment)
           ELSE actual_cost END)::numeric(19,2),
          next_sk
          from (
          select m.sk,
          m.adjustmentdate,
          m.store_sk,
          m.product_gnm_sk,
          m.adjustment,
          c.moving_cost_per_unit as prev_moving_cost_per_unit,
          c.moving_quantity as prev_moving_quantity,
          c.moving_actual_cost_value as prev_moving_actual_cost_value,
          (CASE WHEN name = 'Receipt' THEN
            (select actual_cost from public.stock_price_history h
               where (h.product_gnm_sk = m.product_gnm_sk) AND h.created <= m.adjustmentdate
               AND actual_cost is not null
               order by created desc limit 1)
           ELSE c.moving_cost_per_unit END) as actual_cost,
          (CASE WHEN name = 'Receipt' THEN true else false END) as is_receipt,
          (select im.sk from public.store_stockmovement im
          where im.product_gnm_sk = m.product_gnm_sk and  im.store_sk = m.store_sk and
          ((im.adjustmentdate = m.adjustmentdate and im.sk > m.sk) or im.adjustmentdate > m.adjustmentdate)
          order by im.adjustmentdate,im.sk limit 1
          ) as next_sk
          from cogs c, public.store_stockmovement m
          inner join public.adjustment_type a on (m.adjustment_type_sk = a.sk)
          where c.next_sk = m.sk and c.next_sk is not null
          ) T

          )

          select sk,store_sk,actual_cost,actual_cost_value,moving_quantity,moving_actual_cost_value,moving_cost_per_unit from cogs 
 ) x
 
 
 ------------------------------------------------------------------------
-------------- REPRORT for vita
----- 

select count(*) from (
select s.storeid,a.name adjustment_type, -- ii.invoiceitemid invoiceitemid,
sm.adjustment,
sm.adjustmentdate,
case when sp.product_gnm_sk <> 0 then 'GNM' else'TAKEON STOCK'end,
sp.productid,sp.wholesaleprice,sp.retailprice,sph.actual_cost,sm.actual_cost,
            sm.actual_cost_value,sm.moving_actual_cost_value,sm.moving_cost_per_unit
--'FRAME TO FOLLOW'
from store_stockmovement sm 
left join store s on s.sk = sm.store_sk
--left join invoiceitem ii on sm.invoice_sk = ii.invoicesk 
left join storeproduct sp on sp.sk = sm.takeon_storeproduct_sk
left join adjustment_type a on a.sk = sm.adjustment_type_sk
left join stock_price_history sph on sph.sk = sm.stock_price_history_sk
where lower(sp.type) = 'frame' -- and ii.invoiceitemid is null
order by sm.store_sk, sm.adjustmentdate
) x


select * from store_stockmovement sm 

select * from d_date;

SELECT null , o.order_id, 
oc.first_name, oc.last_name, oc.telephone, 
-- s.store_id, 
o.order_type_id,
o.queue, o.status, 
o.created_date_time, o.modified_date_time, *
FROM gnm."order" o 
inner join gnm.order_customer oc on oc.id = o.customer_id
-- inner join gnm."store" s on s.id = o.store_id

left outer join public.store ps on ps.storeid = s.store_id
left join public.customer c on c.firstname = oc.first_name and c.lastname = oc.last_name and c.storesk = ps.sk;

 
SELECT product_id, p.type, p.created_date_time, p.modified_date_time, p.name,  p.description, supplier_sku,p.apn,b.name brandname 
FROM gnm.product p inner join gnm.brand b on b.id = p.brand_id inner join gnm.supplier s on s.id = p.supplier_id;


SELECT o.order_id, oc.first_name, oc.last_name, oc.telephone, s.store_id, o.order_type_id, o.queue, o.status, o.created_date_time, o.modified_date_time 
FROM gnm.order o inner join gnm.order_customer oc on oc.id = o.customer_id inner join gnm.store s on s.id = o.store_id

SELECT 
0, id, 0, sp.sk, 
pch.supplier_cost, pch.supplier_cost_gst, 
pch.actual_cost, pch.actual_cost_gst, 
pch.supplier_rrp, pch.supplier_rrp_gst, pch.calculated_retail_price, pch.calculated_retail_price_gst, 
0,0,
0,0,
pch.modified_date_time
k


select js.job_id, js.order_id,js.job_type,js.queue, js.status,js.source_created, js.source_modified
from stage.job_stage js 



select * from stage.gnm_db_job limit 1


select stage.get_latest_data_by_tablename('gnm_db_job');

 SELECT get_latest_data_by_tablename.data ->> 'FIRSTPUR'::text AS "FIRSTPUR",
    get_latest_data_by_tablename.data ->> 'PHOTONAME'::text AS "PHOTONAME",
    get_latest_data_by_tablename.data ->> 'LASTSALE'::text AS "LASTSALE",
    get_latest_data_by_tablename.data ->> 'FRAMENUM'::text AS "FRAMENUM",
    get_latest_data_by_tablename.data ->> 'WSALEIT'::text AS "WSALEIT",
    get_latest_data_by_tablename.data ->> 'REORDDATE'::text AS "REORDDATE",
    get_latest_data_by_tablename.data ->> 'STKTAKE'::text AS "STKTAKE",
    get_latest_data_by_tablename.data ->> 'LLABORDER'::text AS "LLABORDER",
    get_latest_data_by_tablename.data ->> 'LDOWNLOAD'::text AS "LDOWNLOAD",
    get_latest_data_by_tablename.data ->> 'EXAVGCOST'::text AS "EXAVGCOST",
    get_latest_data_by_tablename.data ->> 'LIFESTYLE'::text AS "LIFESTYLE",
    get_latest_data_by_tablename.data ->> 'NOTE'::text AS "NOTE",
    get_latest_data_by_tablename.data ->> 'PKEY'::text AS "PKEY",
    get_latest_data_by_tablename.data ->> 'TAXPC'::text AS "TAXPC",
    get_latest_data_by_tablename.data ->> 'DISPC'::text AS "DISPC",
    get_latest_data_by_tablename.data ->> 'FRSTATUS'::text AS "FRSTATUS",
    get_latest_data_by_tablename.data ->> 'PHOTO'::text AS "PHOTO",
    get_latest_data_by_tablename.data ->> 'SUPPLIER'::text AS "SUPPLIER",
    get_latest_data_by_tablename.data ->> 'FGID'::text AS "FGID",
    get_latest_data_by_tablename.data ->> 'ISTOCKCODE'::text AS "ISTOCKCODE",
    get_latest_data_by_tablename.data ->> 'XFER'::text AS "XFER",
    get_latest_data_by_tablename.data ->> 'LOGSTR'::text AS "LOGSTR",
    get_latest_data_by_tablename.data ->> 'LISTPRICE'::text AS "LISTPRICE",
    get_latest_data_by_tablename.data ->> 'RELEASE'::text AS "RELEASE",
    get_latest_data_by_tablename.data ->> 'CHANGE'::text AS "CHANGE",
    get_latest_data_by_tablename.data ->> 'DIAG'::text AS "DIAG",
    get_latest_data_by_tablename.data ->> 'DQTY'::text AS "DQTY",
    get_latest_data_by_tablename.data ->> 'FRDESC'::text AS "FRDESC",
    get_latest_data_by_tablename.data ->> 'RETURNBY'::text AS "RETURNBY",
    get_latest_data_by_tablename.data ->> 'RRPSRV'::text AS "RRPSRV",
    get_latest_data_by_tablename.data ->> 'DPREEXCOST'::text AS "DPREEXCOST",
    get_latest_data_by_tablename.data ->> 'EXPREVCOST'::text AS "EXPREVCOST",
    get_latest_data_by_tablename.data ->> 'SUPBARCODE'::text AS "SUPBARCODE",
    get_latest_data_by_tablename.data ->> 'SXFRAME'::text AS "SXFRAME",
    get_latest_data_by_tablename.data ->> 'REFRESH'::text AS "REFRESH",
    get_latest_data_by_tablename.data ->> 'REORDQTY'::text AS "REORDQTY",
    get_latest_data_by_tablename.data ->> 'QTYONAPPRO'::text AS "QTYONAPPRO",
    get_latest_data_by_tablename.data ->> 'TEMPLE'::text AS "TEMPLE",
    get_latest_data_by_tablename.data ->> 'PSCREATED'::text AS "PSCREATED",
    get_latest_data_by_tablename.data ->> 'FCOLOUR'::text AS "FCOLOUR",
    get_latest_data_by_tablename.data ->> 'BASEC'::text AS "BASEC",
    get_latest_data_by_tablename.data ->> 'LIFECYCLE'::text AS "LIFECYCLE",
    get_latest_data_by_tablename.data ->> 'LASTPUR'::text AS "LASTPUR",
    get_latest_data_by_tablename.data ->> 'FRAMENUM0'::text AS "FRAMENUM0",
    get_latest_data_by_tablename.data ->> 'ORDERAGAIN'::text AS "ORDERAGAIN",
    get_latest_data_by_tablename.data ->> 'FRAMEGROUP'::text AS "FRAMEGROUP",
    get_latest_data_by_tablename.data ->> 'QTYAPPRO'::text AS "QTYAPPRO",
    get_latest_data_by_tablename.data ->> 'PSUPDATEAT'::text AS "PSUPDATEAT",
    get_latest_data_by_tablename.data ->> 'USER'::text AS "USER",
    get_latest_data_by_tablename.data ->> 'MINPD'::text AS "MINPD",
    get_latest_data_by_tablename.data ->> 'EXLISTPR'::text AS "EXLISTPR",
    get_latest_data_by_tablename.data ->> 'DPRECOST'::text AS "DPRECOST",
    get_latest_data_by_tablename.data ->> 'MODEL'::text AS "MODEL",
    get_latest_data_by_tablename.data ->> 'FROTHER2'::text AS "FROTHER2",
    get_latest_data_by_tablename.data ->> 'BARCODE'::text AS "BARCODE",
    get_latest_data_by_tablename.data ->> 'PHOTOEXT'::text AS "PHOTOEXT",
    get_latest_data_by_tablename.data ->> 'AVAILFROM'::text AS "AVAILFROM",
    get_latest_data_by_tablename.data ->> 'DEPTH'::text AS "DEPTH",
    get_latest_data_by_tablename.data ->> 'QTY3'::text AS "QTY3",
    get_latest_data_by_tablename.data ->> 'LASTSALE2'::text AS "LASTSALE2",
    get_latest_data_by_tablename.data ->> 'SPH2'::text AS "SPH2",
    get_latest_data_by_tablename.data ->> 'FROTHER'::text AS "FROTHER",
    get_latest_data_by_tablename.data ->> 'PSSUPFIT'::text AS "PSSUPFIT",
    get_latest_data_by_tablename.data ->> 'SIZE'::text AS "SIZE",
    get_latest_data_by_tablename.data ->> 'COSTPRICE'::text AS "COSTPRICE",
    get_latest_data_by_tablename.data ->> 'EXRRPSRV'::text AS "EXRRPSRV",
    get_latest_data_by_tablename.data ->> 'LOCATION'::text AS "LOCATION",
    get_latest_data_by_tablename.data ->> 'LOCATION2'::text AS "LOCATION2",
    get_latest_data_by_tablename.data ->> 'CYL2'::text AS "CYL2",
    get_latest_data_by_tablename.data ->> 'RRP'::text AS "RRP",
    get_latest_data_by_tablename.data ->> 'LISTSRV'::text AS "LISTSRV",
    get_latest_data_by_tablename.data ->> 'AVAILTILL'::text AS "AVAILTILL",
    get_latest_data_by_tablename.data ->> 'BASECURVE'::text AS "BASECURVE",
    get_latest_data_by_tablename.data ->> 'MODKEY'::text AS "MODKEY",
    get_latest_data_by_tablename.data ->> 'SUNGRX'::text AS "SUNGRX",
    get_latest_data_by_tablename.data ->> 'PVINACTIVE'::text AS "PVINACTIVE",
    get_latest_data_by_tablename.data ->> 'EXCOSTPR'::text AS "EXCOSTPR",
    get_latest_data_by_tablename.data ->> 'QUANTITY'::text AS "QUANTITY",
    get_latest_data_by_tablename.data ->> 'SUPPLIER2'::text AS "SUPPLIER2",
    get_latest_data_by_tablename.data ->> 'SRVCHARGE'::text AS "SRVCHARGE",
    get_latest_data_by_tablename.data ->> 'QTYONORDER'::text AS "QTYONORDER",
    get_latest_data_by_tablename.data ->> 'DELFLAG'::text AS "DELFLAG",
    get_latest_data_by_tablename.data ->> 'PROSUPPLY'::text AS "PROSUPPLY",
    get_latest_data_by_tablename.data ->> 'SPH1'::text AS "SPH1",
    get_latest_data_by_tablename.data ->> 'PKEY0'::text AS "PKEY0",
    get_latest_data_by_tablename.data ->> 'AVGCOST'::text AS "AVGCOST",
    get_latest_data_by_tablename.data ->> 'MANUFACT'::text AS "MANUFACT",
    get_latest_data_by_tablename.data ->> 'FRAMETYPE'::text AS "FRAMETYPE",
    get_latest_data_by_tablename.data ->> 'APPORDER'::text AS "APPORDER",
    get_latest_data_by_tablename.data ->> 'FRANGE'::text AS "FRANGE",
    get_latest_data_by_tablename.data ->> 'WSALEET'::text AS "WSALEET",
    get_latest_data_by_tablename.data ->> 'FRSTATUS2'::text AS "FRSTATUS2",
    get_latest_data_by_tablename.data ->> 'MODIFIED'::text AS "MODIFIED",
    get_latest_data_by_tablename.data ->> 'PROVISION'::text AS "PROVISION",
    get_latest_data_by_tablename.data ->> 'REORDER'::text AS "REORDER",
    get_latest_data_by_tablename.data ->> 'SUPSTATUS'::text AS "SUPSTATUS",
    get_latest_data_by_tablename.data ->> 'LASTINV'::text AS "LASTINV",
    get_latest_data_by_tablename.data ->> 'EXLISTSRV'::text AS "EXLISTSRV",
    get_latest_data_by_tablename.data ->> 'PREVCOST'::text AS "PREVCOST",
    get_latest_data_by_tablename.data ->> 'CYL1'::text AS "CYL1",
    get_latest_data_by_tablename.data ->> 'LOCATION3'::text AS "LOCATION3",
    get_latest_data_by_tablename.parameters ->> 'customer_frame_product_id'::text AS customer_frame_product_id,
    get_latest_data_by_tablename.parameters ->> 'store'::text AS store,
    get_latest_data_by_tablename.parameters ->> 'source'::text AS source,
    get_latest_data_by_tablename.parameters ->> 'view'::text AS view,
    get_latest_data_by_tablename.parameters ->> 'gnm_account_code'::text AS gnm_account_code
   FROM stage.get_latest_data_by_tablename('gnm_sunix_vframe'::bpchar) get_latest_data_by_tablename(data, parameters);

   
drop view view_stockmovement ;

create view view_stockmovement as
select sm.sk,sm.source_id,s.storeid,a.name adjustment_type, -- ii.invoiceitemid invoiceitemid,
sm.adjustment,
sm.adjustmentdate,
case when sp.product_gnm_sk <> 0 then 'GNM' else'TAKEON STOCK'end,
	s.pms,
	sp.productid,
	sp.wholesaleprice pms_wholesaleprice,
	sp.retailprice pms_retailprice,
	sph.actual_cost as price_history_actual_cost,
	sm.actual_cost as sm_actual_cost,
    sm.actual_cost_value as actual_cost_value,
    sm.moving_actual_cost_value as cogs_moving_actual,
    sm.moving_cost_per_unit as cogs_moving_cost_per_unit
--'FRAME TO FOLLOW'
from store_stockmovement sm 
left join store s on s.sk = sm.store_sk
--left join invoiceitem ii on sm.invoice_sk = ii.invoicesk 
left join storeproduct sp on sp.sk = sm.takeon_storeproduct_sk
left join adjustment_type a on a.sk = sm.adjustment_type_sk
left join stock_price_history sph on sph.sk = sm.stock_price_history_sk

where lower(sp.type) in  ('frame','sunglasses') -- and ii.invoiceitemid is null
-- order by sm.store_sk, sm.adjustmentdate;
;


select * from view_stockmovement
where 
	storeid = '2444-001' and adjustment_type = 'Receipt' and adjustmentdate  > '2017-08-20' and adjustmentdate  < '2017-08-23'
order by storeid, adjustmentdate

----------------------------------
--- from the raw staging file see what data didn't make it into the processed stockmovement record

select ssm."source" || '-' || ssm."LOCATION" || '-' || ssm."ID",sm.source_id,sm.sk, "CODE",sp.type,* from 
stage.today_gnm_sunix_vrestock  ssm 
left join store_stockmovement sm on sm.source_id = ssm."source" || '-' || ssm."LOCATION" || '-' || ssm."ID"
left join view_stockmovement vs on vs.sk = sm.sk
left join storeproduct sp on sp.sk = sm.takeon_storeproduct_sk
where 
-- sm.sk is not null and 
source =  '2444-001' and "DATE"  > '2017-08-20' and "DATE"  < '2017-08-23'



select * from stage.today_gnm_sunix_vrestock 
where 
	source =  '2444-001' and "DATE"  > '2017-08-20' and "DATE"  < '2017-08-23'


	select count(*) from (
		select * from view_stockmovement vs
		where storeid =  '2444-001' and adjustment_type = 'Receipt'
	) x 


-- Invoice item view

create view view_invoiceitem_cogs as 
-- select count(*) from (
SELECT 
	invoicesk, pos, 
	sp.productid, 
	s.storeid, 
	employeesk, 
	serviceemployeesk, 
	customersk, 
	createddate, 
	ii.quantity, 
	amount_incl_gst,
	ii.modified, 
	amount_gst, 
	discount_incl_gst, 
	discount_gst, 
	cost_incl_gst, 
	cost_gst, 
	ii.description, 
	payeesk, 
	invoiceitemid, 
	return_incl_gst, 
	return_gst, 
	bulkbillref, 
	ssm.adjustmentdate,
	ssm.moving_quantity stockmoving_quantity,
	ssm.actual_cost pms_actual_cost, 
	ssm.actual_cost_value pmsactual_cost_value,
	ssm.moving_actual_cost_value cogs_moving_actual_cost_value, 
	ssm.moving_cost_per_unit cogs_moving_cost_per_unit, 
	 'floor stock' as order_type
FROM public.invoiceitem ii
	inner join store s on s.sk = ii.storesk
	left join store_stockmovement ssm on ssm.invoice_sk = ii.invoicesk -- and ssm.product_gnm_sk = ii.productsk
	left join storeproduct sp on ii.productsk = sp.sk
where	
	ii.productsk = ssm.takeon_storeproduct_sk

select * from view_invoiceitem_cogs


SELECT 
	invoicesk, pos, 
	sp.productid, 
	s.storeid, 
	employeesk, 
	serviceemployeesk, 
	customersk, 
	createddate, 
	ii.quantity, 
	amount_incl_gst,
	ii.modified, 
	amount_gst, 
	discount_incl_gst, 
	discount_gst, 
	cost_incl_gst, 
	cost_gst, 
	ii.description, 
	payeesk, 
	invoiceitemid, 
	return_incl_gst, 
	return_gst, 
	bulkbillref, 
	ssm.adjustmentdate,
	ssm.moving_quantity stockmoving_quantity,
	ssm.actual_cost pms_actual_cost, 
	ssm.actual_cost_value pmsactual_cost_value,
	ssm.moving_actual_cost_value cogs_moving_actual_cost_value, 
	ssm.moving_cost_per_unit cogs_moving_cost_per_unit, 
	 'floor stock' as order_type
FROM public.invoiceitem ii
	inner join store s on s.sk = ii.storesk
--	left join store_stockmovement ssm on ssm.invoice_sk = ii.invoicesk -- and ssm.product_gnm_sk = ii.productsk
	left join storeproduct sp on ii.productsk = sp.sk

	left join invoice_orders io on io.invoice_sk = ii.invoicesk
	left join order_history oh on oh.sk = io.order_sk
	left join last_job lj on lj.order_sk = oh.sk
	left join product_gnm pg on pg.sk = lj.product_sk

where	
	ii.productsk = ssm.takeon_storeproduct_sk


	
	