set role scire;
SET search_path = public, pg_catalog;

--- create stock_price_history view for each product
-- drop materialized view last_gnm_product_price;

create materialized view last_gnm_product_price as
select pgs.* from
stock_price_history pgs 
inner join (select product_gnm_sk,max(sk) maxsk from stock_price_history group by product_gnm_sk) x on x.maxsk = pgs.sk;

create unique index last_gnm_product_price_idx on last_gnm_product_price (sk);

-- drop materialized view last_takeon_product_price;

create materialized view last_takeon_product_price as
select pgs.* from
stock_price_history pgs 
inner join (select takeon_storeproduct_sk,max(sk) maxsk from stock_price_history group by takeon_storeproduct_sk) x on x.maxsk = pgs.sk;

create unique index last_takeon_product_price_idx on last_takeon_product_price (sk);

---convenience view for invoices and orders -----------
drop view if exists invoice_orders_frame cascade; 

create or replace view invoice_orders_frame as
select substring(spectaclejobid,10, length(spectaclejobid)) as pms_orderid
, oh.sk order_sk
, sp.invoicesk
, oh.nk gnm_order_id
, sp.framesk
, sp.right_lenssk
, sp.left_lenssk
, sp.storesk
, sp.pms_invoicenum
, lj.sk job_sk
, lj.nk job_id, oh.order_type
, lj.status
, frame_sp.sk job_storeproduct_sk
, lj.product_sk  job_gnm_product_sk
, frame_sp.description
, frame_sp.product_gnm_sk
, ltpp.supplier_cost tp_supplier_cost
, ltpp.actual_cost tp_actual_cost
, lgpp.supplier_cost gp_supplier_cost
, lgpp.actual_cost gp_actual_cost
, 'frame' prod_type
from spectaclejob sp 
left join order_history oh on oh.nk = substring(spectaclejobid,10, length(spectaclejobid))
left join last_job lj on lj.order_sk = oh.sk
inner join storeproduct frame_sp on frame_sp.sk = sp.framesk
left join product_gnm pg on frame_sp.product_gnm_sk = pg.sk
left join last_takeon_product_price  ltpp on ltpp.takeon_storeproduct_sk = frame_sp.sk
left join last_gnm_product_price lgpp on lgpp.product_gnm_sk = pg.sk
union all
select substring(spectaclejobid,10, length(spectaclejobid)) as pms_orderid
, oh.sk order_sk
, sp.invoicesk
, oh.nk gnm_order_id
, sp.framesk
, sp.right_lenssk
, sp.left_lenssk
, sp.storesk
, sp.pms_invoicenum
, lj.sk job_sk
, lj.nk job_id, oh.order_type
, lj.status
, left_sp.sk job_storeproduct_sk
, lj.product_sk  job_gnm_product_sk
, left_sp.description
, left_sp.product_gnm_sk
, ltpp.supplier_cost tp_supplier_cost
, ltpp.actual_cost tp_actual_cost
, lgpp.supplier_cost gp_supplier_cost
, lgpp.actual_cost gp_actual_cost
, 'leftlens' prod_type
from spectaclejob sp 
left join order_history oh on oh.nk = substring(spectaclejobid,10, length(spectaclejobid))
left join last_job lj on lj.order_sk = oh.sk
inner join storeproduct left_sp on left_sp.sk = sp.left_lenssk
left join product_gnm pg on left_sp.product_gnm_sk = pg.sk
left join last_takeon_product_price  ltpp on ltpp.takeon_storeproduct_sk = left_sp.sk
left join last_gnm_product_price lgpp on lgpp.product_gnm_sk = pg.sk
union all
select substring(spectaclejobid,10, length(spectaclejobid)) as pms_orderid
, oh.sk order_sk
, sp.invoicesk
, oh.nk gnm_order_id
, sp.framesk
, sp.right_lenssk
, sp.left_lenssk
, sp.storesk
, sp.pms_invoicenum
, lj.sk job_sk
, lj.nk job_id, oh.order_type
, lj.status
, right_sp.sk job_storeproduct_sk
, lj.product_sk job_gnm_product_sk
, right_sp.description
, right_sp.product_gnm_sk
, ltpp.supplier_cost tp_supplier_cost
, ltpp.actual_cost tp_actual_cost
, lgpp.supplier_cost gp_supplier_cost
, lgpp.actual_cost gp_actual_cost
, 'rightlens' prod_type
from spectaclejob sp 
left join order_history oh on oh.nk = substring(spectaclejobid,10, length(spectaclejobid))
left join last_job lj on lj.order_sk = oh.sk
inner join storeproduct right_sp on right_sp.sk = sp.right_lenssk
left join product_gnm pg on right_sp.product_gnm_sk = pg.sk
left join last_takeon_product_price  ltpp on ltpp.takeon_storeproduct_sk = right_sp.sk
left join last_gnm_product_price lgpp on lgpp.product_gnm_sk = pg.sk;

---convenience view for invoices and orders :: REPORTING VIEW for yellowfin -----------
drop materialized view if exists minvoice_orders_frame cascade; 

create materialized view minvoice_orders_frame as
-- select count(*) from (
select substring(spectaclejobid,10, length(spectaclejobid)) as pms_orderid
, oh.sk order_sk
, sp.invoicesk
, oh.nk gnm_order_id
, sp.framesk
, sp.right_lenssk
, sp.left_lenssk
, sp.storesk
, sp.pms_invoicenum
, lj.sk job_sk
, lj.nk job_id, oh.order_type
, lj.status
, frame_sp.sk job_storeproduct_sk
, lj.product_sk  job_gnm_product_sk
, frame_sp.description
, frame_sp.product_gnm_sk
, ltpp.supplier_cost tp_supplier_cost
, ltpp.actual_cost tp_actual_cost
, lgpp.supplier_cost gp_supplier_cost
, lgpp.actual_cost gp_actual_cost
, 'frame' prod_type
from spectaclejob sp 
left join order_history oh on oh.nk = substring(spectaclejobid,10, length(spectaclejobid))
left join last_job lj on lj.order_sk = oh.sk
inner join storeproduct frame_sp on frame_sp.sk = sp.framesk
left join product_gnm pg on frame_sp.product_gnm_sk = pg.sk
left join last_takeon_product_price  ltpp on ltpp.takeon_storeproduct_sk = frame_sp.sk
left join last_gnm_product_price lgpp on lgpp.product_gnm_sk = pg.sk
union all
select substring(spectaclejobid,10, length(spectaclejobid)) as pms_orderid
, oh.sk order_sk
, sp.invoicesk
, oh.nk gnm_order_id
, sp.framesk
, sp.right_lenssk
, sp.left_lenssk
, sp.storesk
, sp.pms_invoicenum
, lj.sk job_sk
, lj.nk job_id, oh.order_type
, lj.status
, left_sp.sk job_storeproduct_sk
, lj.product_sk  job_gnm_product_sk
, left_sp.description
, left_sp.product_gnm_sk
, ltpp.supplier_cost tp_supplier_cost
, ltpp.actual_cost tp_actual_cost
, lgpp.supplier_cost gp_supplier_cost
, lgpp.actual_cost gp_actual_cost
, 'leftlens' prod_type
from spectaclejob sp 
left join order_history oh on oh.nk = substring(spectaclejobid,10, length(spectaclejobid))
left join last_job lj on lj.order_sk = oh.sk
inner join storeproduct left_sp on left_sp.sk = sp.left_lenssk
left join product_gnm pg on left_sp.product_gnm_sk = pg.sk
left join last_takeon_product_price  ltpp on ltpp.takeon_storeproduct_sk = left_sp.sk
left join last_gnm_product_price lgpp on lgpp.product_gnm_sk = pg.sk
union all
select substring(spectaclejobid,10, length(spectaclejobid)) as pms_orderid
, oh.sk order_sk
, sp.invoicesk
, oh.nk gnm_order_id
, sp.framesk
, sp.right_lenssk
, sp.left_lenssk
, sp.storesk
, sp.pms_invoicenum
, lj.sk job_sk
, lj.nk job_id, oh.order_type
, lj.status
, right_sp.sk job_storeproduct_sk
, lj.product_sk job_gnm_product_sk
, right_sp.description
, right_sp.product_gnm_sk
, ltpp.supplier_cost tp_supplier_cost
, ltpp.actual_cost tp_actual_cost
, lgpp.supplier_cost gp_supplier_cost
, lgpp.actual_cost gp_actual_cost
, 'rightlens' prod_type
from spectaclejob sp 
left join order_history oh on oh.nk = substring(spectaclejobid,10, length(spectaclejobid))
left join last_job lj on lj.order_sk = oh.sk
inner join storeproduct right_sp on right_sp.sk = sp.right_lenssk
left join product_gnm pg on right_sp.product_gnm_sk = pg.sk
left join last_takeon_product_price  ltpp on ltpp.takeon_storeproduct_sk = right_sp.sk
left join last_gnm_product_price lgpp on lgpp.product_gnm_sk = pg.sk


-------------------------------------------------------------------------------------------------------
---- yellowfin reporting view ===  invoice + stockmovement + order + job
---- unified view
drop materialized view mview_invoiceitem_cogs_full;

create  materialized view mview_invoiceitem_cogs_full as 
SELECT 
	i.invoiceid
	,ii.invoicesk, 
	ii.pos, 
	sp.productid, 
	s.storeid, 
	ii.employeesk, 
	ii.serviceemployeesk, 
	ii.customersk, 
	ii.createddate, 
	ii.quantity, 
	ii.amount_incl_gst,
	ii.modified, 
	ii.amount_gst, 
	ii.discount_incl_gst, 
	ii.discount_gst, 
	ii.cost_incl_gst, 
	ii.cost_gst, 
	ii.description, 
	ii.payeesk, 
	ii.invoiceitemid, 
	ii.return_incl_gst, 
	ii.return_gst, 
	ii.bulkbillref, 
	ad.name
	,ssm.adjustmentdate,
	ssm.moving_quantity stockmoving_quantity,
	ssm.actual_cost pms_actual_cost, 
	ssm.actual_cost_value pmsactual_cost_value,
	ssm.moving_actual_cost_value cogs_moving_actual_cost_value, 
	ssm.moving_cost_per_unit cogs_moving_cost_per_unit, 
	io.pms_orderid, gnm_order_id
	,io.framesk
	,pms_invoicenum
	,job_id
	,order_type
	,status
	,tp_supplier_cost takeon_supplier_cost
	,tp_actual_cost takeon_actual_cost
	,gp_supplier_cost gnm_supplier_cost
	,gp_actual_cost gnm_actual_cost
FROM public.invoiceitem ii
	left join invoice i on i.sk = ii.invoicesk
	left join store s on s.sk = ii.storesk
	left join store_stockmovement ssm on ssm.invoice_sk = ii.invoicesk and ssm.takeon_storeproduct_sk = ii.productsk
	left join adjustment_type ad on ad.sk = ssm.adjustment_type_sk
	left join storeproduct sp on ii.productsk = sp.sk
	left join minvoice_orders_frame io on io.invoicesk = ii.invoicesk and io.job_storeproduct_sk = ii.productsk;
	
drop view view_invoiceitem_cogs_full;

create view view_invoiceitem_cogs_full as 
SELECT	* from mview_invoiceitem_cogs_full;

/*
	select count(*) from (
select c.* from view_invoiceitem_cogs_full c 
where gnm_order_id  is not null
)x;

select * from order_history;
*/

---- unified view
drop view view_invoiceitem_cogs;

create view view_invoiceitem_cogs as 
SELECT 
	ii.*
	,ad.name
	,ssm.adjustmentdate,
	ssm.moving_quantity stockmoving_quantity,
	ssm.moving_actual_cost_value cogs_moving_actual_cost_value, 
	ssm.moving_cost_per_unit cogs_moving_cost_per_unit, 
	io.pms_orderid, gnm_order_id
	,io.framesk
	,pms_invoicenum
	,job_id
	,order_type
	,status
	,tp_supplier_cost takeon_supplier_cost
	,tp_actual_cost takeon_actual_cost
	,gp_supplier_cost gnm_supplier_cost
	,gp_actual_cost gnm_actual_cost
FROM public.invoiceitem ii
	left join invoice i on i.sk = ii.invoicesk
	left join store s on s.sk = ii.storesk
	left join store_stockmovement ssm on ssm.invoice_sk = ii.invoicesk and ssm.takeon_storeproduct_sk = ii.productsk
	left join adjustment_type ad on ad.sk = ssm.adjustment_type_sk
	left join storeproduct sp on ii.productsk = sp.sk
	left join invoice_orders_frame io on io.invoicesk = ii.invoicesk and io.job_storeproduct_sk = ii.productsk;

/*
select * from view_invoiceitem_cogs 
where storesk = 13 and createddate >= '2017-10-31' and createddate < '2017-11-01'
and gnm_order_id is not null;


select ii.*  
FROM public.invoiceitem ii
	left join invoice i on i.sk = ii.invoicesk
	left join store s on s.sk = ii.storesk
where  ii.storesk = 13 and ii.createddate >= '2017-10-31' and ii.createddate < '2017-11-01';
*/

---- unified view
-- drop view view_invoiceitem_cogs;

drop materialized view view_invoiceitem_cogs_test;

create materialized view view_invoiceitem_cogs_test as 
SELECT 
	ii.*
	,ad.name
	,ssm.adjustmentdate
	,ssm.moving_quantity stockmoving_quantity
-- 	ssm.actual_cost pms_actual_cost, 
-- 	ssm.actual_cost_value pmsactual_cost_value,
--	ssm.moving_actual_cost_value cogs_moving_actual_cost_value, 
--	ssm.moving_cost_per_unit cogs_moving_cost_per_unit, 
--	io.pms_orderid, 
	,gnm_order_id
--	,io.framesk
--	,pms_invoicenum
	,job_id
	,order_type
	,status
--	,tp_supplier_cost takeon_supplier_cost
--	,tp_actual_cost takeon_actual_cost
--	,gp_supplier_cost gnm_supplier_cost
--	,gp_actual_cost gnm_actual_cost
		  ,coalesce( actual_cost_value, gp_supplier_cost ) * ssm.moving_quantity
          						as calc_actual_cost_inc_gst
          ,coalesce(case ssm.product_gnm_sk  when 0 then actual_cost_value
          						else moving_cost_per_unit end, gp_supplier_cost ) * ssm.moving_quantity
          						as calc_cost_inc_gst
          ,(coalesce(case ssm.product_gnm_sk  when 0 then actual_cost_value
          						else moving_cost_per_unit end, gp_supplier_cost ) * ssm.moving_quantity) / 10  as calc_gst	
FROM public.invoiceitem ii
	left join invoice i on i.sk = ii.invoicesk
	left join store s on s.sk = ii.storesk
	left join store_stockmovement ssm on ssm.invoice_sk = ii.invoicesk and ssm.takeon_storeproduct_sk = ii.productsk
	left join adjustment_type ad on ad.sk = ssm.adjustment_type_sk
	left join storeproduct sp on ii.productsk = sp.sk
	left join invoice_orders_frame io on io.invoicesk = ii.invoicesk  and io.job_storeproduct_sk = ii.productsk;

	
create index view_invoiceitem_cogs_test_idx on view_invoiceitem_cogs_test (invoicesk, createddate, modified, adjustmentdate);

/*
select * from view_invoiceitem_cogs_test 
where invoicesk <> 0  and createddate > '2017-10-01'
and calc_cost_inc_gst is not null and gnm_order_id is not null
*/

/*
set role scire;

drop materialized view public.view_invoiceitem_cogs;

create materialized view public.view_invoiceitem_cogs as 
SELECT 
	ii.*
	,ssm.adjusteddate
	,gnm_order_id
	,job_id
	,order_type
	,status
		  ,coalesce( actual_cost_value, gp_supplier_cost ) 
          						as calc_actual_cost_inc_gst
          ,coalesce( actual_cost_value, gp_supplier_cost ) / 10
          						as calc_actual_cost_gst
          ,coalesce(case ssm.product_gnm_sk  when 0 then actual_cost_value
          						else moving_cost_per_unit end, gp_supplier_cost ) 
          						as calc_cost_inc_gst
          ,coalesce(case ssm.product_gnm_sk  when 0 then actual_cost_value
          						else moving_cost_per_unit end, gp_supplier_cost )  / 10  as calc_gst	
FROM public.invoiceitem ii
	left join invoice i on i.sk = ii.invoicesk
	left join store s on s.sk = ii.storesk
	left join 
	(select ii.invoiceitemid, (select sk from  adjustment_price_history ssm 
					where  ssm.storeproduct_sk = ii.productsk and ii.createddate >= ssm.adjusteddate 
					order by ssm.adjusteddate_sk desc limit 1) as price_sk
	 from invoiceitem ii 
	) pk on pk.invoiceitemid = ii.invoiceitemid
	left join adjustment_price_history ssm on ssm.sk = pk.price_sk
	left join storeproduct sp on ii.productsk = sp.sk
	left join invoice_orders_frame io on io.invoicesk = ii.invoicesk  and io.job_storeproduct_sk = ii.productsk

	
create index public.view_invoiceitem_cogs_idx on view_invoiceitem_cogs (invoicesk, createddate, modified, adjustmentdate);
*/






