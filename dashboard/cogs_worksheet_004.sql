
----------- Create view that calcuates averages, save into a table of cogs averages, update end_date ranges for the table
set role scire;
SET search_path = public, pg_catalog;


-- View: public.invoice_orders_frame

-- 
drop view if exists  public.invoice_orders_frame_tst;

CREATE VIEW public.invoice_orders_frame_tst AS
 SELECT "substring"(sp.spectaclejobid::text, 10, length(sp.spectaclejobid::text)) AS pms_orderid,
    oh.sk AS order_sk,
    sp.invoicesk,
    oh.nk AS gnm_order_id,
    sp.framesk,
    sp.right_lenssk,
    sp.left_lenssk,
    sp.storesk,
    sp.pms_invoicenum,
    lj.sk AS job_sk,
    lj.nk AS job_id,
    oh.order_type,
    lj.status,
    stp.sk AS storeproduct_sk,
    lj.product_sk AS job_product_gnm_sk,
    stp.product_gnm_sk AS reconcile_product_gnm_sk,
    'frame'::text AS type
   FROM spectaclejob sp
     LEFT JOIN order_history oh ON oh.nk::text = "substring"(sp.spectaclejobid::text, 10, length(sp.spectaclejobid::text))
     LEFT JOIN last_job lj ON lj.order_sk = oh.sk
     LEFT JOIN storeproduct stp ON stp.sk = sp.framesk
  WHERE oh.sk IS NOT NULL AND lj.product_sk = stp.product_gnm_sk
UNION ALL
 SELECT "substring"(sp.spectaclejobid::text, 10, length(sp.spectaclejobid::text)) AS pms_orderid,
    oh.sk AS order_sk,
    sp.invoicesk,
    oh.nk AS gnm_order_id,
    sp.framesk,
    sp.right_lenssk,
    sp.left_lenssk,
    sp.storesk,
    sp.pms_invoicenum,
    lj.sk AS job_sk,
    lj.nk AS job_id,
    oh.order_type,
    lj.status,
    stp.sk AS storeproduct_sk,
    lj.product_sk AS job_product_gnm_sk,
    stp.product_gnm_sk AS reconcile_product_gnm_sk,
    'rightlens'::text AS type
   FROM spectaclejob sp
     LEFT JOIN order_history oh ON oh.nk::text = "substring"(sp.spectaclejobid::text, 10, length(sp.spectaclejobid::text))
     LEFT JOIN last_job lj ON lj.order_sk = oh.sk
     LEFT JOIN storeproduct stp ON stp.sk = sp.right_lenssk
  WHERE oh.sk IS NOT NULL AND lj.product_sk = stp.product_gnm_sk
UNION ALL
 SELECT "substring"(sp.spectaclejobid::text, 10, length(sp.spectaclejobid::text)) AS pms_orderid,
    oh.sk AS order_sk,
    sp.invoicesk,
    oh.nk AS gnm_order_id,
    sp.framesk,
    sp.right_lenssk,
    sp.left_lenssk,
    sp.storesk,
    sp.pms_invoicenum,
    lj.sk AS job_sk,
    lj.nk AS job_id,
    oh.order_type,
    lj.status,
    stp.sk AS storeproduct_sk,
    lj.product_sk AS job_product_gnm_sk,
    stp.product_gnm_sk AS reconcile_product_gnm_sk,
    'leftlens'::text AS type
   FROM spectaclejob sp
     LEFT JOIN order_history oh ON oh.nk::text = "substring"(sp.spectaclejobid::text, 10, length(sp.spectaclejobid::text))
     LEFT JOIN last_job lj ON lj.order_sk = oh.sk
     LEFT JOIN storeproduct stp ON stp.sk = sp.left_lenssk
  WHERE oh.sk IS NOT NULL AND lj.product_sk = stp.product_gnm_sk
  
  
  
drop view if exists public.view_invoiceitem_cogs_tst;

create view public.view_invoiceitem_cogs_tst as 
SELECT distinct
	ii.*
	,gnm_order_id
	,job_id
	,order_type
	,status

	,coalesce( ssm.actual_cost,gnmph.actual_cost, ii.cost_incl_gst ) as calc_actual_cost
    ,coalesce( ssm.actual_cost_gst, gnmph.actual_cost_gst,ii.cost_gst ) as calc_actual_cost_gst
    ,coalesce( ssm.average_cost, gnmph.actual_cost,ii.cost_incl_gst ) as calc_cost
    ,coalesce( ssm.average_cost_gst, gnmph.actual_cost_gst,ii.cost_gst ) as calc_cost_gst
     
FROM public.invoiceitem ii
	inner join invoice i on i.sk = ii.invoicesk
	inner join store s on s.sk = ii.storesk
	inner join storeproduct sp on ii.productsk = sp.sk
	
	left join product_gnm_average_cost_history ssm on ssm.storeproduct_sk = ii.productsk and ii.createddate >= ssm.adjustmentdate and ii.createddate < ssm.end_date
	left join invoice_orders_frame io on io.invoicesk = ii.invoicesk  and io.storeproduct_sk = ii.productsk
	
--	left join stock_price_history sph on sph.takeon_storeproduct_sk = sp.sk and ii.createddate >= sph.created and ii.createddate < sph.end_date 
	left join view_last_stock_history_price gnmph on  gnmph.product_gnm_sk = sp.product_gnm_sk and ii.createddate >= gnmph.created and ii.createddate < gnmph.end_date 
where  ii.createddate > '2017-11-01';


select * from view_invoiceitem_cogs_tst order by createddate desc limit 1000;


select * from view_invoiceitem_cogs order by createddate desc limit 1000;



-- drop materialized VIEW public.mview_invoiceitem_cogs_full cascade;
-- drop materialized view minvoice_orders_frame cascade;
-- drop materialized view last_gnm_product_price
-- drop materialized view last_takeon_product_price
-- drop materialized VIEW public.invoice_orders_frame cascade;
-- drop view vbyd_invoice cascade; drop view vbyd_invoiceitem cascade;
-- drop view invoice_orders_frame cascade;
-- drop view last_job
-- drop view store_stock_history_view
/*

drop view vbyd_job_history;
drop view vbyd_order_history;
drop view vbyd_store_stockmovement;
drop view_last_stock_history_price;
drop view view_product_gnm_average_cost_history;
drop view view_stockmovement;
drop view v_cogs_first_receipt
drop view vbyd_stock_price_history;

*/



drop materialized view public.view_invoiceitem_cogs;

create materialized view public.view_invoiceitem_cogs as 
SELECT distinct
	ii.*
	,ssm.adjustmentdate
	,gnm_order_id
	,job_id
	,order_type
	,status
	,coalesce( ssm.actual_cost, gp_supplier_cost,gnmph.actual_cost, sph.actual_cost ) as calc_actual_cost_inc_gst
    ,coalesce( ssm.actual_cost_gst, gp_supplier_cost, gnmph.actual_cost_gst,sph.actual_cost_gst ) as calc_actual_cost_gst
    ,coalesce( ssm.average_cost, gp_supplier_cost, gnmph.actual_cost,sph.actual_cost ) as calc_cost_inc_gst
    ,coalesce( ssm.average_cost_gst, gp_supplier_cost, gnmph.actual_cost_gst,sph.actual_cost_gst ) as calc_gst	
FROM public.invoiceitem ii
	left join invoice i on i.sk = ii.invoicesk
	left join store s on s.sk = ii.storesk
	left join 
	(select ii.invoiceitemid, (select sk from  product_gnm_average_cost_history ssm 
					where  ssm.storeproduct_sk = ii.productsk and ii.createddate >= ssm.adjustmentdate and ii.createddate < ssm.end_date 
					order by ssm.adjustmentdate desc limit 1) as average_cost_sk
	 from invoiceitem ii where  ii.createddate > '2017-08-01'
	) pk on pk.invoiceitemid = ii.invoiceitemid
	left join product_gnm_average_cost_history ssm on ssm.sk = pk.average_cost_sk
	left join storeproduct sp on ii.productsk = sp.sk
	left join invoice_orders_frame io on io.invoicesk = ii.invoicesk  and io.job_storeproduct_sk = ii.productsk
	left join stock_price_history sph on sph.takeon_storeproduct_sk = sp.sk and ii.createddate >= sph.created and ii.createddate < sph.end_date 
	left join stock_price_history gnmph on gnmph.product_gnm_sk = sp.product_gnm_sk and ii.createddate >= sph.created and ii.createddate < sph.end_date 
where  ii.createddate > '2017-08-01'
	
create index view_invoiceitem_cogs_idx on view_invoiceitem_cogs (invoicesk, createddate, modified);

-----------------------------------------------------------

select * from invoiceitem;
select * from product_gnm_average_cost_history;
select * from stock_price_history;
select * from view_invoiceitem_cogs where gnm_order_id is not null;
select * from stage.today_gnm_sunix_vframe;

/*
drop materialized view view_invoiceitem_cogs_test;

drop materialized view public.view_invoiceitem_cogs_full;
*/

drop table if exists invoice_item_prices;

create table invoice_item_prices (
	sk serial NOT NULL,
	
 	pms_stock_price_history_sk integer REFERENCES stock_price_history(sk),
 	gnm_stock_price_history_sk integer references stock_price_history(sk),
 	adjustment_price_history_sk integer references adjustment_price_history(sk),
 	
    invoice_sk integer references invoice(sk) not null,
    invoiceitemid varchar(60) not null,
    invoice_datetime timestamp without time zone,
    
    created timestamp without time zone DEFAULT now(),
    primary key(sk)
);

select * from invoiceitem ii
inner join storeproduct sp on sp.sk = ii.productsk
left join stock_price_history sph_ngm on sph_ngm.takeon_storeproduct_sk = ii.productsk and ii.createddate >= sph_ngm.created and ii.createddate < sph_ngm.created
-- left join stock_price_history sph_gm on sph_gm.product_gnm_sk = sp.product_gnm_sk and ii.createddate >= sph_gm.created and ii.createddate < sph_gm.created
where 
	sph_ngm.sk is not null and ii.createddate > '2017-11-25';

	
----------------------------------------------------------------------

select * from invoiceitem ii
inner join storeproduct sp on sp.sk = ii.productsk
left join stock_price_history sph_ngm on sph_ngm.takeon_storeproduct_sk = ii.productsk and ii.createddate >= sph_ngm.created 
where ii.createddate > '2017-11-25';

select * from view_invoiceitem_cogs limit 10; taking 5 secs



select inv.sk,inv.invoiceid, case when pdt.type = 'Lens Extras' then 'Lens' when pdt.type = 'Sunglasses' then 'Frame' 
else pdt.type end as product_type, itm.amount_incl_gst, itm.amount_gst, itm.discount_incl_gst, 
itm.discount_gst, itm.calc_cost, itm.calc_cost_gst, itm.calc_actual_cost, itm.calc_actual_cost_gst, 
itm.pos, itm.invoiceitemid,payee.name as payee, case when lower(payeetype) = 'patient rebate' 
then true else false end as is_rebate, bulkbillref
from view_invoiceitem_cogs itm
          inner join invoice inv on itm.invoicesk = inv.sk
          inner join payee on itm.payeesk = payee.sk
          inner join store on itm.storesk = store.sk
          left outer join storeproduct pdt on itm.productsk = pdt.sk
          where store.storeid = '2088-012' and inv.is_writeoff_invoice=false and coalesce(itm.modified,itm.createddate) > '2017-11-28' order by invoiceid;

select * from "store"


select calc_cost,calc_actual_cost, vc.*
from view_invoiceitem_cogs vc 
where storesk = 1
and createddate > '2017-11-28'



/*
 * 
	
create index view_invoiceitem_cogs_idx on view_invoiceitem_cogs (invoicesk, createddate, modified);

-----------------------------------------------------------

select * from invoiceitem;
select * from product_gnm_average_cost_history;
select * from stock_price_history;
select * from view_invoiceitem_cogs where gnm_order_id is not null;
select * from stage.today_gnm_sunix_vframe;


select * from invoice_orders_frame

select * from invoiceitem ii
inner join storeproduct sp on sp.sk = ii.productsk
left join stock_price_history sph_ngm on sph_ngm.takeon_storeproduct_sk = ii.productsk and ii.createddate >= sph_ngm.created and ii.createddate < sph_ngm.end_date
-- left join stock_price_history sph_gm on sph_gm.product_gnm_sk = sp.product_gnm_sk 	and ii.createddate >= sph_gm.created and ii.createddate < sph_gm.created
where 
	sph_ngm.sk is not null and 
ii.createddate > '2017-11-25';

	
----------------------------------------------------------------------

select * from invoiceitem ii
inner join storeproduct sp on sp.sk = ii.productsk
left join stock_price_history sph_ngm on sph_ngm.takeon_storeproduct_sk = ii.productsk and ii.createddate >= sph_ngm.created 
where ii.createddate > '2017-11-25';

select * from view_invoiceitem_cogs limit 10; taking 5 secs



select inv.sk,inv.invoiceid, case when pdt.type = 'Lens Extras' then 'Lens' when pdt.type = 'Sunglasses' then 'Frame' 
else pdt.type end as product_type, itm.amount_incl_gst, itm.amount_gst, itm.discount_incl_gst, 
itm.discount_gst, itm.calc_cost, itm.calc_cost_gst, itm.calc_actual_cost, itm.calc_actual_cost_gst, 
itm.pos, itm.invoiceitemid,payee.name as payee, case when lower(payeetype) = 'patient rebate' 
then true else false end as is_rebate, bulkbillref
from view_invoiceitem_cogs itm
          inner join invoice inv on itm.invoicesk = inv.sk
          inner join payee on itm.payeesk = payee.sk
          inner join store on itm.storesk = store.sk
          left outer join storeproduct pdt on itm.productsk = pdt.sk
          where store.storeid = '2088-012' and inv.is_writeoff_invoice=false and coalesce(itm.modified,itm.createddate) > '2017-11-28' order by invoiceid;

select * from "store"


select calc_cost,calc_actual_cost, vc.*
from view_invoiceitem_cogs vc 
where storesk = 1
and createddate > '2017-11-28'





*/

select * from 
(select product_gnm_sk, row_number() over (partition by sm.store_sk,sm.product_gnm_sk order by sm.created,sm.sk ) r 
from stock_price_history sm where sm.product_gnm_sk <> 0) 

create view view_last_stock_history_price as
select sph.* from stock_price_history sph inner join 
(select product_gnm_sk, max(sk) lastprice_sk
from stock_price_history sm where sm.product_gnm_sk <> 0 group by product_gnm_sk) x on x.lastprice_sk = sph.sk

SELECT distinct
	ii.*
	,gnm_order_id
	,job_id
	,order_type
	,status

	,coalesce( ssm.actual_cost,gnmph.actual_cost, ii.cost_incl_gst ) as calc_actual_cost
    ,coalesce( ssm.actual_cost_gst, gnmph.actual_cost_gst,ii.cost_gst ) as calc_actual_cost_gst
    ,coalesce( ssm.average_cost, gnmph.actual_cost,ii.cost_incl_gst ) as calc_cost
    ,coalesce( ssm.average_cost_gst, gnmph.actual_cost_gst,ii.cost_gst ) as calc_cost_gst
     
FROM public.invoiceitem ii
	inner join invoice i on i.sk = ii.invoicesk
	inner join store s on s.sk = ii.storesk
	inner join storeproduct sp on ii.productsk = sp.sk
	
	left join product_gnm_average_cost_history ssm on ssm.storeproduct_sk = ii.productsk and ii.createddate >= ssm.adjustmentdate and ii.createddate < ssm.end_date
	left join invoice_orders_frame io on io.invoicesk = ii.invoicesk  and io.storeproduct_sk = ii.productsk
	
--	left join stock_price_history sph on sph.takeon_storeproduct_sk = sp.sk and ii.createddate >= sph.created and ii.createddate < sph.end_date 
	left join view_last_stock_history_price gnmph on  gnmph.product_gnm_sk = sp.product_gnm_sk and ii.createddate >= gnmph.created and ii.createddate < gnmph.end_date 
where  ii.createddate > '2017-11-01' and sp.sk <> 0


select * from stock_price_history where product_gnm_sk in 
(select product_gnm_sk from stock_price_history
where product_gnm_sk <> 0 group by product_gnm_sk having count(*) > 10 order by count(*) desc limit 1);


update stock_price_history set product_gnm_sk = 0 where product_gnm_sk is null;

select * from stage.today_gnm_db_cost_history;

select * from view_invoiceitem_cogs_display where createddate > '2017-01-01' and
order_type is not null;

select order_type,count(*) c from view_invoiceitem_cogs_display where createddate > '2017-11-01' and
order_type is not null group by order_type



select inv.invoiceid, 
case when pdt.type = 'Lens Extras' then 'Lens' when pdt.type = 'Sunglasses' then 'Frame' else pdt.type end as product_type, 
itm.amount_incl_gst, itm.amount_gst, itm.discount_incl_gst, itm.discount_gst, itm.calc_cost, itm.calc_cost_gst, itm.calc_actual_cost, 
itm.calc_actual_cost_gst, itm.pos, itm.invoiceitemid,payee.name as payee, case when lower(payeetype) = 'patient rebate' then true else false end as is_rebate,
bulkbillref, order_type
from view_invoiceitem_cogs itm
          inner join invoice inv on itm.invoicesk = inv.sk
          inner join payee on itm.payeesk = payee.sk
          inner join store on itm.storesk = store.sk
          left outer join storeproduct pdt on itm.productsk = pdt.sk
where -- store.storeid = '2088-012' and 
          inv.is_writeoff_invoice=false and coalesce(itm.modified,itm.createddate) > '2017-11-01' and order_type is not null;


select inv.invoiceid, 
case when pdt.type = 'Lens Extras' then 'Lens' when pdt.type = 'Sunglasses' then 'Frame' else pdt.type end as product_type, 
itm.amount_incl_gst, itm.amount_gst, itm.discount_incl_gst, itm.discount_gst, itm.calc_cost, itm.calc_cost_gst, itm.calc_actual_cost, 
itm.calc_actual_cost_gst, itm.pos, itm.invoiceitemid,payee.name as payee, case when lower(payeetype) = 'patient rebate' then true else false end as is_rebate,
bulkbillref, order_type
from mview_invoiceitem_cogs itm
          inner join invoice inv on itm.invoicesk = inv.sk
          inner join payee on itm.payeesk = payee.sk
          inner join store on itm.storesk = store.sk
          left outer join storeproduct pdt on itm.productsk = pdt.sk
where -- store.storeid = '2088-012' and 
          inv.is_writeoff_invoice=false and coalesce(itm.modified,itm.createddate) > '2017-11-01' and order_type is not null;


select * from order_history where store_sk = 1 order by source_created desc
and source_created >= '2017-12-01'


-- commit;
-- rollback;
/*

select store_sk,takeon_storeproduct_sk,product_gnm_sk,created,end_date 
from stock_price_history sp 
where --sp.product_gnm_sk in (select product_gnm_sk from stock_price_history where end_date < '2030-01-01 00:00:00' and created = end_date) and 
--store_sk = 1 and 
product_gnm_sk <> 0
order by product_gnm_sk, created, end_date

--------------------------------------------------------------------------------------------------


select * from vstore_stockmovement where name = 'Receipt' and store_sk = 1 and product_gnm_sk <> 0;

select * from product_gnm_average_cost_history 
where product_gnm_sk in (select product_gnm_sk from vstore_stockmovement where name = 'Receipt' and store_sk = 1 and product_gnm_sk <> 0)
and store_sk = 1 order by product_gnm_sk,
*/



select sph.created,end_date,* from product_gnm pg 
LEFT JOIN stock_price_history sph on sph.product_gnm_sk = pg.sk
where productid = '00211104'
order by sph.created
;


select * from product_gnm pg 
LEFT JOIN product_gnm_average_cost_history sph on sph.product_gnm_sk = pg.sk
where productid = '00211104'
order by adjustmentdate
;


-- calculate date range from history
update public.stock_price_history set end_date = Z.end_date
from (select x.sk,x1.created end_date,x1.product_gnm_sk from 
(select sk,takeon_storeproduct_sk, product_gnm_sk,created, row_number() over (partition by product_gnm_sk order by sh.created,sh.sk) r
from stock_price_history sh where product_gnm_sk <> 0) x 
inner join
(select sk,takeon_storeproduct_sk, product_gnm_sk,created, row_number() over (partition by product_gnm_sk order by sh.created,sh.sk) r
from stock_price_history sh where product_gnm_sk <> 0) x1 on x.product_gnm_sk = x1.product_gnm_sk and x.r+1 = x1.r
order by x.product_gnm_sk) Z 
where Z.sk = stock_price_history.sk;


--- date ranges for product_gnm_average_cost_history
--begin;
update public.product_gnm_average_cost_history set end_date = Z.end_date
-- select * 
from 
(select x.sk,x1.sk,x.adjustmentdate, x1.adjustmentdate end_date,x1.product_gnm_sk ,x.r,x1.r
	from 
		(select sk,store_sk, product_gnm_sk,adjustmentdate, row_number() over (partition by product_gnm_sk,store_sk order by sh.sk) r
			from product_gnm_average_cost_history sh where product_gnm_sk <> 0 and product_gnm_sk = 6266) x 
		inner join (select sk,store_sk, product_gnm_sk,adjustmentdate, row_number() over (partition by product_gnm_sk,store_sk order by sh.sk) r
			from product_gnm_average_cost_history sh where product_gnm_sk <> 0 and product_gnm_sk = 6266) x1 
				on x.product_gnm_sk = x1.product_gnm_sk and x.r+1 = x1.r and x.store_sk = x1.store_sk
	order by x.product_gnm_sk) Z 

	order by Z.product_gnm_sk,end_date
--				where Z.sk = product_gnm_average_cost_history.sk;




select count(*) from (		
select * from invoiceitem where modified >= '2017-12-10'
) x;

select count(*) from (		
select distinct * from view_invoiceitem_cogs where modified >= '2017-12-10'
) v;

select count(*) from (		
select distinct * from vview_invoiceitem_cogs where modified >= '2017-12-10'
) v;

select * from view_invoiceitem_cogs where modified >= '2017-12-10' 
and invoiceitemid not in
(select invoiceitemid from invoiceitem where modified >= '2017-12-10' )


-----------------------------------------------------------------------------------
--- checking avg calc for product 6266

	select * from product_gnm_average_cost_history 
	where product_gnm_sk = 6266
	order by store_sk,adjustmentdate,sk asc;

	select * from stock_price_history 
	where product_gnm_sk = 6266
	order by sk asc;


	select t.adjustment_typeid, t.name,* from store_stockmovement sm left join adjustment_type t on t.sk = sm.adjustment_type_sk
	where product_gnm_sk = 6266 and store_sk in (select store_sk from product_gnm_average_cost_history)
order by store_sk,sm.sk asc;


select adjustment_typeid,count(*) from adjustment_type group by adjustment_typeid order by count(*) desc;
select name,count(*) from adjustment_type group by name order by count(*) desc


select * from stock_price_history sph left join 
(select product_gnm_sk, created, max(sk) sk, count(*) from stock_price_history 
where created = end_date group by product_gnm_sk, created having count(*) > 1) x on x.product_gnm_sk = sph.product_gnm_sk and x.created = sph.created
and x.sk <> sph.sk
where 
	sph.product_gnm_sk = 6266
order by sph.product_gnm_sk;


select * from store_stockmovement ssm 
left join (select * from stock_price_history sph )
left join stock_price_history  sph on ssm.takeon_storeproduct_sk = ssm.takeon_storeproduct_sk 
						and sph.created >= ssm.adjustmentdate and sph.end_date > ssm.adjustmentdate
where 
	sph.product_gnm_sk = 6266 
order by ssm.store_sk, ssm.takeon_storeproduct_sk, ssm.sk


-----------------------------------------------------------------------------------------------------------------------
-- 1. what are the issues ?

-- get a count of invoice items


select count(*) from (
	select * from invoice i where createddate >= '2017-12-20' and createddate <= '2017-12-20'
) x;

select count(*) from (
	select * from invoiceitem i where createddate >= '2017-12-20' and createddate <= '2017-12-20'
) x;


select count(*) from (
	select * from view_invoiceitem_cogs vc  where createddate >= '2017-12-20' and createddate <= '2017-12-20'
) y;


--------------------------------------------------------------------------------------------------


select count(*) from (
	select * from invoice i where createddate >= '2017-12-19' and createddate <= '2017-12-19'
) x;

select count(*) from (
	select * from invoiceitem i where createddate >= '2017-12-19' and createddate <= '2017-12-19'
) x;

select count(*) from (
	select * from view_invoiceitem_cogs vc  where createddate >= '2017-12-19' and createddate <= '2017-12-19'
) y;


select count(*) from invoiceitem where createddate >= '2017-12-19';


select * from adjustment_price_history



select * from product_gnm_average_cost_history order by store_sk,product_gnm_sk,adjustmentdate desc,sk  desc limit 100;




--- get me stock count from last reset, 


--- add a running total to product_gnm_average_cost_history to work out running average cost


alter table product_gnm_average_cost_history add column running_total integer;


select store_sk,product_gnm_sk,max(running_total) from product_gnm_average_cost_history
where store_sk = 1
group by store_sk,product_gnm_sk
order by max(running_total) desc,store_sk,product_gnm_sk;

select * from adjustment_type a 

insert into adjustment_type(adjustment_typeid,name,othername) values ('RESET','Stocktake Reset','Reset');



select * from product_gnm_average_cost_history where product_gnm_sk = 2969 order by store_sk,running_total asc

select * from vstore_stockmovement sm where product_gnm_sk = 2969 and store_sk =1 order by store_sk, adjustmentdate, sk;

--
--
-- 
insert into store_stockmovement (store_sk,nk,takeon_storeproduct_sk,adjustmentdate,
product_gnm_sk,adjustment_type_sk,stock_price_history_sk,adjustment,reason) 
values (1,'2088-012--91245x',	667496, '2017-08-08 16:00:00',2969,318,0,0,'')


--
-- get last reset
--  


	select
	seq.r,
		sm.sk,
		sm.adjustmentdate,
		1 quantity,
		1 running_total,
		sm.store_sk,
		sm.product_gnm_sk,
		sm.takeon_storeproduct_sk,
		sp.actual_cost::numeric(19,2),
		sp.actual_cost_gst::numeric(19,2),
		sp.actual_cost_gst::numeric(19,2),
		0,
		sp.actual_cost::numeric(19,2) as average_cost,
		sp.actual_cost_gst::numeric(19,2) as average_cost_gst,
		*
	from
		(
		select sm.sk,sm.store_sk,sm.product_gnm_sk,row_number() over (partition by sm.store_sk,sm.product_gnm_sk order by sm.adjustmentdate,sm.sk ) r 
			from store_stockmovement sm 
				left join (select takeon_storeproduct_sk,max(sm.adjustmentdate) adjustmentdate 
					from store_stockmovement sm inner join adjustment_type a on a.sk = sm.adjustment_type_sk
						where sm.product_gnm_sk <> 0 and a.Name in ('Stocktake Reset') 
						group by takeon_storeproduct_sk) sr on sr.takeon_storeproduct_sk = sm.takeon_storeproduct_sk
				inner join adjustment_type a on a.sk = sm.adjustment_type_sk
			where sm.product_gnm_sk <> 0  and ((sr.adjustmentdate is null) or (sr.adjustmentdate is not null and sm.adjustmentdate > sr.adjustmentdate))
					and a.Name in ('Receipt') 
		) seq
		inner join store_stockmovement sm on seq.sk = sm.sk
		inner join adjustment_type a on a.sk = sm.adjustment_type_sk
		left join  -- get the base price by date
			stock_price_history sp on sp.product_gnm_sk = sm.product_gnm_sk and sp.created <= sm.adjustmentdate and sp.end_date > sm.adjustmentdate
	where sm.product_gnm_sk <> 0 and a.Name in ('Receipt') and seq.r = 1 
	

---------




select takeon_storeproduct_sk,max(sm.sk) from 
store_stockmovement sm
inner join adjustment_type a on a.sk = sm.adjustment_type_sk
	where sm.product_gnm_sk <> 0 and a.Name in ('Stocktake Reset')
	group by takeon_storeproduct_sk


select sk,row_number() over (partition by sm.store_sk,sm.product_gnm_sk order by sm.adjustmentdate,sm.sk ) r 
			from store_stockmovement sm 
				left join (select takeon_storeproduct_sk,max(sm.adjustmentdate) adjustmentdate 
					from store_stockmovement sm inner join adjustment_type a on a.sk = sm.adjustment_type_sk
						where sm.product_gnm_sk <> 0 and a.Name in ('Stocktake Reset') 
						group by takeon_storeproduct_sk) sr on sr.takeon_storeproduct_sk = sm.takeon_storeproduct_sk and sm.adjustmentdate > sr.adjustmentdate
						

						
select * from  view_product_gnm_average_cost_history_reset where product_gnm_sk = 2969 and store_sk = 1
order by adjustmentdate



---------


---------

drop table stocktake cascade ;
drop table stocktake_item;

CREATE TABLE stocktake
(
    sk serial not null,
    nk text not null,
    store_sk bigint NOT NULL,
    planned_stocktake_start_date_time timestamp with time zone NOT NULL,
    planned_stocktake_end_date_time timestamp with time zone NOT NULL,
    actual_stocktake_start_date_time timestamp with time zone,
    actual_stocktake_end_date_time timestamp with time zone,
    created_user character varying(255) COLLATE pg_catalog."default" NOT NULL DEFAULT 'system'::character varying,
    created_date_time timestamp without time zone NOT NULL DEFAULT timezone('utc'::text, now()),
    modified_user character varying(255) COLLATE pg_catalog."default" NOT NULL DEFAULT 'system'::character varying,
    modified_date_time timestamp without time zone NOT NULL DEFAULT timezone('utc'::text, now()),
   	created timestamp without time zone NOT NULL DEFAULT timezone('utc'::text, now()),
    CONSTRAINT pk_stocktake PRIMARY KEY (sk),
    CONSTRAINT "fk_stocktake__store_id -> store" FOREIGN KEY (store_sk)
        REFERENCES store (sk) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);


CREATE TABLE stocktake_item
(
    sk serial not null,
    nk text not null,
    stocktake_sk integer NOT NULL,
    product_gnm_sk integer REFERENCES product_gnm(sk),
    store_product_sk integer references storeproduct,
    expected_qty integer NOT NULL,
    actual_qty integer NOT NULL,
    avg_cost_incl_gst numeric(19,2) NOT NULL, 
    
    avg_cost_gst numeric(19,2) NOT NULL,
    pms_user character varying(25) COLLATE pg_catalog."default" NOT NULL,
    last_purchased_date timestamp without time zone,
    created_user character varying(255) COLLATE pg_catalog."default" NOT NULL DEFAULT 'system'::character varying,
    created_date_time timestamp without time zone NOT NULL DEFAULT timezone('utc'::text, now()),
    modified_user character varying(255) COLLATE pg_catalog."default" NOT NULL DEFAULT 'system'::character varying,
    modified_date_time timestamp without time zone NOT NULL DEFAULT timezone('utc'::text, now()),
    created timestamp without time zone NOT NULL DEFAULT timezone('utc'::text, now()),
    CONSTRAINT pk_stocktake_item PRIMARY KEY (sk),
    CONSTRAINT fk_stocktake_item__stocktake_sk FOREIGN KEY (stocktake_sk)
        REFERENCES stocktake (sk) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);




---------
select * from stocktake_item



---------



select runid, configname, pipename, parameters,count(*) from stage.gnm_db_stocktake group by runid, configname, pipename, parameters


select runid, configname, pipename, parameters,count(*) from stage.gnm_db_stocktake group by runid, configname, pipename, parameters

select store,count(*) from stage.today_gnm_db_stocktake group by store


--------------------------------------------------------------
insert into stocktake (
	nk,
    store_sk,
    planned_stocktake_start_date_time,
    planned_stocktake_end_date_time,
    actual_stocktake_start_date_time,
    actual_stocktake_end_date_time,
    --created_user ,
    created_date_time ,
    ---modified_user,
    modified_date_time
    )  
select 
    tst.stocktake_id::integer, s.sk,
    tst.planned_stocktake_start_date_time::timestamp without time zone,
    tst.planned_stocktake_end_date_time::timestamp without time zone,
    actual_stocktake_start_date_time::timestamp without time zone,
    actual_stocktake_start_date_time::timestamp without time zone,
    created_date_time::timestamp without time zone,
    modified_date_time::timestamp without time zone
from stage.today_gnm_db_stocktake tst
-- inner join storeproduct sp  on sp.storeid = tst.store_id and sp.name = tst.pms_product_id and sp.storeid is not null
inner join store s on s.storeid = tst.store_id
left join  stocktake st on tst.stocktake_id,s.sk,tst.planned_stocktake_start_date_time,tst.planned_stocktake_end_date_time,actual_stocktake_start_date_time,
actual_stocktake_start_date_time
group by tst.stocktake_id,s.sk,tst.planned_stocktake_start_date_time,tst.planned_stocktake_end_date_time,actual_stocktake_start_date_time,
actual_stocktake_start_date_time,created_date_time,modified_date_time
	
select * from stocktake

--------------------------------------------------------------




    
---------------------------------------------------------------


select count(*) from (
select pms_product_id,sp.storeid,count(*) 
from stage.today_gnm_db_stocktake tst
inner join store s on s.storeid = tst.store_id
inner join storeproduct sp on sp.storeid = tst.store_id and sp.name = tst.pms_product_id and sp.storeid is not null
group by pms_product_id,sp.storeid
) x


drop index idx_storeproduct_name;

CREATE INDEX idx_storeproduct_name ON storeproduct USING hash (name);

select * from storeproduct
select * from stage.today_gnm_db_stocktake tst


select pms_product_id,*
from stage.today_gnm_db_stocktake tst
where pms_product_id =  'GNM001497'

select * from "store"

ome sag
select * from product_gnm pg

SELECT 
    waiting.locktype           AS waiting_locktype,
    waiting.relation::regclass AS waiting_table,
    waiting_stm.query          AS waiting_query,
    waiting.mode               AS waiting_mode,
    waiting.pid                AS waiting_pid,
    other.locktype             AS other_locktype,
    other.relation::regclass   AS other_table,
    other_stm.query            AS other_query,
    other.mode                 AS other_mode,
    other.pid                  AS other_pid,
    other.GRANTED              AS other_granted
FROM
    pg_catalog.pg_locks AS waiting
JOIN
    pg_catalog.pg_stat_activity AS waiting_stm
    ON (
        waiting_stm.pid = waiting.pid
    )
JOIN
    pg_catalog.pg_locks AS other
    ON (
        (
            waiting."database" = other."database"
        AND waiting.relation  = other.relation
        )
        OR waiting.transactionid = other.transactionid
    )
JOIN
    pg_catalog.pg_stat_activity AS other_stm
    ON (
        other_stm.pid = other.pid
    )
WHERE
    NOT waiting.GRANTED
AND
    waiting.pid <> other.pid
    
----------------------------------------------------------------------------------
    
    select "SUPPLIER","INVNO" from stage.today_gnm_sunix_vrestock
    
    
select * from pg_table 

SELECT *
FROM information_schema.columns 
WHERE table_schema = 'public' 
--AND table_name = 'blah' 
ORDER BY table_name, column_name ASC;


select table_catalog, table_schema, table_name, table_type ,*
from information_schema.tables 
where table_schema not in ('pg_catalog', 'information_schema');


select t.table_name, t.table_type, c.relname, c.relowner, u.usename
from information_schema.tables t
join pg_catalog.pg_class c on (t.table_name = c.relname)
join pg_catalog.pg_user u on (c.relowner = u.usesysid)
where t.table_schema='public';
    

drop view store_stock_history_view;



create view store_stock_history_view as
with recursive prior_total_adjustments as (

	select seq,store_sk,storeproduct_sk,product_gnm_sk,date_actual,dailytotal as cumulative_total, 0 as prior_total
	from daily_adjustments_sum where seq = 1
	
union all	
	select ds.seq,ds.store_sk,ds.storeproduct_sk,ds.product_gnm_sk,ds.date_actual, psm.cumulative_total + ds.dailytotal, psm.cumulative_total::integer as prior_total
	from prior_total_adjustments psm 
	inner join daily_adjustments_sum ds on 
		ds.store_sk = psm.store_sk and ds.storeproduct_sk = psm.storeproduct_sk and ds.product_gnm_sk = psm.product_gnm_sk
	where psm.seq + 1 = ds.seq
)
select seq,store_sk,storeproduct_sk,product_gnm_sk,date_actual,cumulative_total, prior_total from prior_total_adjustments order by date_actual;



--- update acquisition date totals    

insert into store_stock_history_adj (store_sk,storeproduct_sk,total,prior_total,source_staged,created,product_gnm_sk)
select store_sk, storeproduct_sk, cumulative_total,prior_total,date_actual,date_actual,product_gnm_sk , s.acquisition_date
from store_stock_history_view hv
inner join store s on s.sk = hv.store_sk
where hv.date_actual < s.acquisition_date


--- trac

;
select * from adjustment_type

--------------------------------------------------------------------------------------------------------------------
select * from stage.today_gnm_db_product

select stage.generate_view_for_table('gnm_db_product');


drop view stage.today_gnm_db_product;

select * from stage.gnm_db_product;




CREATE OR REPLACE FUNCTION stage.generate_view_for_table(_tname character varying)
 RETURNS void
 LANGUAGE plpgsql
AS $function$

declare
sql varchar;
begin

SELECT 'CREATE VIEW stage.today_' || _tname || ' AS SELECT ' || string_agg(pick || '->>''' || col || ''' as "' || col || '"',',') || ' FROM stage.get_latest_data_by_tablename(''' || _tname || ''')' 
as sel
FROM (
select distinct 'data' as pick, jsonb_object_keys("data") as col from stage.get_latest_data_by_tablename(_tname)
union all 
select distinct 'parameters' as pick, jsonb_object_keys("parameters") as col from stage.get_latest_data_by_tablename(_tname)
) T into sql;

execute sql;

end;

$function$;

CREATE OR REPLACE FUNCTION stage.get_latest_data_by_tablename(_tablename character)
 RETURNS TABLE(data jsonb, parameters jsonb, runid uuid, createddate timestamp without time zone)
 LANGUAGE plpgsql
AS $function$

declare
sql varchar;
begin
SELECT 'SELECT jsonb_array_elements("data") as data,parameters,T.runid,T.createddate FROM stage.' || _tablename || ' T where T.runid in (
select runid from (
select parameters->>''source'',runid,createddate,row_number() over (partition by parameters->>''source'' order by createddate desc) as r from stage.' || _tablename || ' where createddate >= (now()::date -7)) S where r = 1);' 
into sql;

return query execute sql;

end;

$function$;


SELECT jsonb_array_elements("data") as data,parameters,T.runid,T.createddate FROM stage.gnm_db_product T






SELECT runid, createddate, configname, pipename, parameters, "data"
FROM stage.gnm_optomate_v_stocktake_log;


SELECT "FRAME_BRAND", "ID", "OTHER_ITEMS", "USER_EDITED1", "CONT_LENS", "STOCK_TYPE", "SOLUTIONS", "DATE_EDITED", 
"PRE_COUNT_QOH", "USER_EDITED", "DATE_ADDED2", "STOCKTAKE_ID", "GM_PRODUCT_ID", "UNIT_COST_INC", "TIMESTMP", "POST_COUNT_QOH", 
"DATE_ADDED", "USER_ADDED", "DATE_ADDED1", "ID1", "BARCODE", "START_DATE_TIME", "TIMESTMP1", "STOCK_ID", "STOCK_TYPE1", 
"SPEC_LENS", "SUNGLASS", "DESCRIPTION", "SUPPLIER_IDENTIFIER", "ABORTED", "SPEC_FRAME", "UNIT_COST_EX", "DATE_EDITED1", "USER_ADDED1", 
"BARCODE1", "BRANCH_IDENTIFIER", "FINISH_DATE_TIME", "store", "source", "view", lookback_days
FROM stage.today_gnm_optomate_v_stocktake_log;




SELECT "FRAMENUM", "EXAVGCOST", "SCANLOC", "ENTRYORDER", "QTY", "BARCODE", "LOCATION", "RRP", "DIFF", "DESC", "QTYSET", "AVGCOST", "QTYINSTK", 
customer_frame_product_id, "store", filename, "source", "view", gnm_account_code
FROM stage.today_gnm_sunix_vfst;
----------------------------------------------------

select * from storeproduct sp inner join
(select "source" || '-PF-' || ltrim(rtrim("FRAMENUM")) prodid, "FRAMENUM" ,count(*) from stage.today_gnm_sunix_vfst group by "FRAMENUM",source ) x 
	on  (x.prodid = sp.productid) --or (x."FRAMENUM" = sp."description" )
where storeid = '2000-027' and "FRAMENUM" <> ''  and sp."description" <> '';

2000-027-PF-MCC000002 2000-027-PL-MCC000002

select * from storeproduct sp 
inner join stage.today_gnm_sunix_vfst st on st."DESC" = sp."name" 
where storeid = '2000-027';

select * from storeproduct sp 
inner join stage.today_gnm_sunix_vfst st on st."FRAMENUM" = sp."description" 
where storeid = '2000-027' and "FRAMENUM" <> ''  and sp."description" <> '';


select * from storeproduct where storeid = '2000-027';
select * from stage.today_gnm_sunix_vfst;

select * from "store"


overlay('Txxxxas' placing 'hom' from 2 for 4)



select max(stocktake_timestamp),filename from (
select to_timestamp(substring(filename,4,10) || ' ' || overlay( replace(substring(filename,15,11),'-',':')  placing ' ' from 9 for 1 ) , 'DD-MM-YYYY HH12:MI:SS AM') stocktake_timestamp,filename
 from stage.today_gnm_sunix_vfst group by filename) x
group by filename


select filename, count(*) from stage.today_gnm_sunix_vfst group by filename


select timestamp '04-12-2017 11:44:57 AM', to_timestamp( '04-12-2017 11:44:57 PM', 'DD-MM-YYYY HH12:MI:SS AM')



SELECT "FRAMENUM", "EXAVGCOST", "SCANLOC", "ENTRYORDER", "QTY", "BARCODE", "LOCATION", "RRP", "DIFF", "DESC", "QTYSET", "AVGCOST", "QTYINSTK", 
customer_frame_product_id, "store", filename, "source", "view", gnm_account_code
FROM stage.today_gnm_sunix_vfst;

select max(stocktake_timestamp) from (
select to_timestamp(substring(filename,4,10) || ' ' || overlay( replace(substring(filename,15,11),'-',':')  placing ' ' from 9 for 1 ) , 'DD-MM-YYYY HH12:MI:SS AM') stocktake_timestamp,filename
 from stage.today_gnm_sunix_vfst group by filename) x




INSERT INTO public.stocktake
( nk,
store_sk, 
planned_stocktake_start_date_time, 
planned_stocktake_end_date_time, 
actual_stocktake_start_date_time, 
actual_stocktake_end_date_time, 
created_user, 
created_date_time, 
modified_user, 
modified_date_time, 
created)
SELECT filename,s.sk,maxdate,maxdate,maxdate,maxdate,'system', now(), 'system', now(), now()
FROM stage.today_gnm_sunix_vfst st
inner join (select max(to_timestamp(substring(filename,4,10) || ' ' || overlay( replace(substring(filename,15,11),'-',':')  placing ' ' from 9 for 1 ) , 'DD-MM-YYYY HH12:MI:SS AM')) maxdate
 from stage.today_gnm_sunix_vfst ) x on x.maxdate = to_timestamp(substring(filename,4,10) || ' ' || overlay( replace(substring(filename,15,11),'-',':')  placing ' ' from 9 for 1 ) , 'DD-MM-YYYY HH12:MI:SS AM')
 inner join store s on st."source" = s.storeid
 left join stocktake stk on stk.nk = st.filename
where 
	stk.sk is null
group by st.filename, maxdate,s.sk

 
select * from stocktake order by sk desc;


--- add state to stocktake for trigger check
alter table stocktake add column state CHARACTER varying(50);

--- add diff column
alter table stocktake_item add column diff integer;


INSERT INTO public.stocktake_item
( 
	nk,
	stocktake_sk, 
	product_gnm_sk, 
	store_product_sk, 
	expected_qty, 
	actual_qty, 
	diff,
	avg_cost_incl_gst, 
	avg_cost_gst,
	pms_user,
	last_purchased_date, 
	created_user, 
	created_date_time, 
	modified_user, 
	modified_date_time, 
	created
)
-- VALUES(nextval('stocktake_item_sk_seq'::regclass), '', 0, 0, 0, 0, 0, 0, 0, '', '', 'system'::character varying,timezone('utc'::text, now()), 'system'::character varying, timezone('utc'::text, now()), timezone('utc'::text, now()));
select "FRAMENUM",st.sk,sp.product_gnm_sk,sp.sk,case when x."QTY" = '' then null else x."QTY"::integer end,x."QTYINSTK"::integer ,x."DIFF"::integer,
x."AVGCOST"::numeric,null,'system',null,'system',now(),'system',now(),now()
--select sp.sk,x.*
from storeproduct sp 
inner join stage.today_gnm_sunix_vfst x on  x."FRAMENUM" = sp."description" and x.source = sp.storeid
inner join stocktake st on st.nk = x.filename 
left join stocktake_item sti on sti.stocktake_sk = st.sk and sti.nk = "FRAMENUM"
where  "FRAMENUM" <> ''  and sp."description" <> '' and sti.sk is null;



select * from stocktake_item


select * from store_stockmovement where adjustment_type_sk = 318;


select * from adjustment_type where sk = 318;

select * from stock_price_history order by sk asc


insert into store_stockmovement 
(store_sk,
nk,
storeproduct_sk,
adjustmentdate,
product_gnm_sk,
adjustment_type_sk,
stock_price_history_sk,
adjustment,
reason) 
-- values (1,'2088-012--91245x',	667496, '2017-08-08 16:00:00',2969,318,0,0,'')
select s.store_sk, si.sk, si.store_product_sk, s.actual_stocktake_end_date_time,si.product_gnm_sk, a.sk, 
coalesce((select sk from  stock_price_history sph where sph.storeproduct_sk = si.store_product_sk 
	-- and sph.created >= s.actual_stocktake_end_date_time and sph.end_date < s.actual_stocktake_end_date_time 
	limit 1),0) as sph_sk, si.actual_qty, a.name
from stocktake_item si 
inner join stocktake s on s.sk = si.stocktake_sk
inner join adjustment_type a on a.name = 'Stocktake Reset'
where s.state = 'APPLY_STOCKTAKE'

-- left join stock_price_history sph on sph.created >= s.actual_stocktake_end_date_time and sph.end_date < s.actual_stocktake_end_date_time

select * from  stocktake_item si
left join stock_price_history sph  on si.store_product_sk = sph.storeproduct_sk
where
	sph.sk is null;

select count(*) from (
select * from stocktake_item) x


--------------------------------------------------------------------------------------------------------
--- adding consignment and glazing



SELECT id, job_id, "name", "type", value
FROM gnm.job_attribute where name = 'frame_glazing_action';


SELECT  j.*, value glazing_action FROM gnm.job j inner join gnm.job_attribute ja on ja.job_id = j.id where name = 'frame_glazing_action'


-- group by name, value

select runid,createddate::timestamp , configname ,pipename,parameters from stage.gnm_db_job order by createddate desc limit 10;
select runid,createddate::timestamp , configname ,pipename,parameters from stage.gnm_db_product order by createddate desc limit 10;

set role postgres;

set role scire;
SET search_path = public, stage, pg_catalog;



select now();

drop view stage.today_gnm_db_job

select stage.generate_view_for_table('gnm_db_job')

truncate table gnm_db_job;
truncate table stage.gnm_db_product;

select * from stage.today_gnm_db_product;


drop view stage.today_gnm_db_product;

select stage.generate_view_for_table('gnm_db_product');


alter table job_history add column    frame_glazing_action character varying(100);
alter table job_history add column       zeiss_glazing_action character varying(100);

insert into public.product_gnm
(productid, "type", 
datecreated, modified, 
"name", description, internal_sku, distributor,apn,brand_name,consignment)


SELECT product_id, p."type", 
created_date_time::TimeSTAMP without time zone, modified_date_time::TimeSTAMP without time zone,
p."name",  p.description, p.supplier_sku, 0, p.apn, p.brandname, p.consignment::boolean
FROM stage.today_gnm_db_product p
left join product_gnm pg on pg.productid = p.product_id
where  p.consignment::boolean = true

SELECT product_id, p."type", 
created_date_time::TimeSTAMP without time zone, modified_date_time::TimeSTAMP without time zone,
p."name",  p.description, p.supplier_sku, 0, p.apn, p.brandname, p.consignment::boolean
FROM stage.today_gnm_db_product p
left join product_gnm pg on pg.productid = p.product_id
where  p.consignment::boolean <> pg.consignment and pg.sk is not null



SELECT p.consignment::boolean, pg.consignment,product_id, p."type", 
created_date_time::TimeSTAMP without time zone, modified_date_time::TimeSTAMP without time zone,
p."name",  p.description, p.supplier_sku, 0, p.apn, p.brandname, p.consignment::boolean
FROM stage.today_gnm_db_product p
inner join product_gnm pg on pg.productid = p.product_id
where pg.consignment = true


update product_gnm set consignment = false;

update product_gnm pg set consignment = p.consignment::boolean
FROM stage.today_gnm_db_product p
where pg.productid = p.product_id and  p.consignment::boolean <> pg.consignment and pg.sk is not null


INSERT INTO public.job_history
(nk, order_id, 
order_sk, 
job_type, 
product_sk, supplier_sk, 
queue, status, 
frame_glazing_action,
zeiss_glazing_action,
source_created, source_modified)
select
js.job_id, js.order_id,
-- order sk
oh.sk,
js.job_type_id,
coalesce(p.sk,0) as productsk, coalesce(s.sk,0) as storesk,
js.queue, js.status,
js.glazing_action,
case when  glazing_action = '5' then '3' else glazing_action end,
js.created_date_time::TimeSTAMP without time zone, js.modified_date_time::TimeSTAMP without time zone
-- select *
from stage.today_gnm_db_job js 
inner join order_history oh on oh.nk = js.order_id
inner join public.supplier s on CAST(s.externalref as varchar(100) ) = js.supplier_id
inner join public.product_gnm p on js.product_id =   p.productid --CAST( p.productid as varchar(100))
left join public.job_history jh on jh.nk = js.job_id
where 
	jh.sk is null;

select count(*) from public.job_history

select created,source_created,* from job_history 
where order_id in (select order_id from job_history group by order_id having count(*) > 1 )
order by  created desc;

select created,source_created,* from order_history 

order by  created desc;


    select * from view_invoiceitem_cogs vc 
    where createddate >= (now() - interval '1  day' )::date and createddate <= (now()  )::date
	and vc.consignment = true;


    select * from view_invoiceitem_cogs vc 
    where vc.consignment = true and glazing_action = '2';


-- select * from adjustment_type order by sk desc
-- select * from stocktake order by sk desc

select * from stocktake s
inner join stocktake_item si on si.stocktake_sk = s.sk
where s.store_sk = 28 and s.sk = 30546


update stocktake set state =  'APPLY_STOCKTAKE' where sk =  30546;


select * from stocktake s where store_sk  = 1 order by sk desc;

select * from stocktake s
inner join stocktake_item si on si.stocktake_sk = s.sk
where s.state = 'APPLY_STOCKTAKE'

select * from adjustment_type order by sk desc


set role scire;
SET search_path = public, stage, pg_catalog;

begin;

insert into store_stockmovement 
(store_sk,
nk,
storeproduct_sk,
adjustmentdate,
product_gnm_sk,
adjustment_type_sk,
stock_price_history_sk,
adjustment,
reason) 
select s.store_sk, si.sk, si.store_product_sk, s.actual_stocktake_end_date_time,si.product_gnm_sk, a.sk, 
coalesce((select sk from  stock_price_history sph where sph.storeproduct_sk = si.store_product_sk 
	-- and sph.created >= s.actual_stocktake_end_date_time and sph.end_date < s.actual_stocktake_end_date_time 
	limit 1),0) as sph_sk, si.actual_qty, a.othername
from stocktake_item si 
inner join stocktake s on s.sk = si.stocktake_sk
inner join adjustment_type a on a.name = 'Stocktake Reset'
left join store_stockmovement sm on sm.store_sk = s.store_sk and sm.storeproduct_sk = si.store_product_sk and si.product_gnm_sk = sm.product_gnm_sk and sm.adjustment_type_sk = 318 and sm.nk::integer = si.sk
where s.state = 'APPLY_STOCKTAKE' and sm.sk is null;

insert into store_stockmovement 
(store_sk,
nk,
storeproduct_sk,
adjustmentdate,
product_gnm_sk,
adjustment_type_sk,
stock_price_history_sk,
adjustment,
reason) 
select s.store_sk, si.sk, si.store_product_sk, s.actual_stocktake_end_date_time,si.product_gnm_sk, a.sk, 
coalesce((select sk from  stock_price_history sph where sph.storeproduct_sk = si.store_product_sk 
	-- and sph.created >= s.actual_stocktake_end_date_time and sph.end_date < s.actual_stocktake_end_date_time 
	limit 1),0) as sph_sk, si.actual_qty, a.othername
from stocktake_item si 
inner join stocktake s on s.sk = si.stocktake_sk
inner join adjustment_type a on a.adjustment_typeid = 'STOCKTAKE_IN'
where s.state = 'APPLY_STOCKTAKE';

commit;



select s.store_sk, si.sk, si.store_product_sk, s.actual_stocktake_end_date_time,si.product_gnm_sk, a.sk, 
coalesce((select sk from  stock_price_history sph where sph.storeproduct_sk = si.store_product_sk 
	-- and sph.created >= s.actual_stocktake_end_date_time and sph.end_date < s.actual_stocktake_end_date_time 
	limit 1),0) as sph_sk, si.actual_qty, a.othername
-- select *
from stocktake_item si 
inner join stocktake s on s.sk = si.stocktake_sk
inner join adjustment_type a on a.name = 'Stocktake Reset'
left join store_stockmovement sm on sm.store_sk = s.store_sk and sm.storeproduct_sk = si.store_product_sk and si.product_gnm_sk = sm.product_gnm_sk and sm.adjustment_type_sk = 318 and sm.nk::integer = si.sk
--- select * from store_stockmovement sm
where sm.adjustment_type_sk = 318 and sm.sk is null


-- 

delete from  store_stockmovement sm where sm.sk = 79852




---------------
-- 1. what should the data look like
-- 2. what data does not conform to that expectation


select * from vstore_stockmovement sm where store_sk = 31 and storeproduct_sk = 699109 order by adjustmentdate;

select max(adjustmentdate),count(*) from vstore_stockmovement sm where product_gnm_sk is null

select * from vstore_stockmovement sm where product_gnm_sk is null


-- the reverse link on stock_price_history for takeon stock stockmovement is unreliable of stock_price_history changes
--


select storeproduct_sk, created, sk, store_sk, takeon_total, total, offcatalogue_reciepted_total, prior_total,*
from store_stock_history_adj where storeproduct_sk = 699109 
order by  sk
------------------------------------------
set role scire;
SET search_path = public, pg_catalog;

-------
--- backup  store_stock_history_adj
drop table backup_store_stock_history_adj;

select * into backup_store_stock_history_adj from store_stock_history_adj;

truncate table store_stock_history_adj;

insert into daily_adjustments_sum(store_sk, 
	storeproduct_sk,
	product_gnm_sk,
	date_actual,
	dailytotal,
	seq)
select sm.store_sk, 
	sm.storeproduct_sk, 
	coalesce(sm.product_gnm_sk,0) 
	product_gnm_sk, 
	dd.date_actual, 
	sum(adjustment) dailytotal, 
	row_number() over (partition by sm.store_sk,sm.storeproduct_sk order by dd.date_actual ) r 
from store_stockmovement sm
inner join d_date dd on dd.sk = sm.adjustmentdate_sk
left join daily_adjustments_sum da on da.store_sk = sm.store_sk and sm.storeproduct_sk = da.storeproduct_sk and dd.date_actual = da.date_actual
where sm.store_sk is not null and da.store_sk is null
group by sm.store_sk, sm.storeproduct_sk, coalesce(sm.product_gnm_sk,0), dd.date_actual;

--- persist cumulative daily totals
insert into store_stock_history_adj (store_sk,storeproduct_sk,takeon_total,total,prior_total,offcatalogue_reciepted_total, source_staged,created,product_gnm_sk)
select sshv.store_sk, sshv.storeproduct_sk,sshv.takeon_total, sshv.cumulative_total,sshv.prior_total,sshv.offcat_total,sshv.date_actual,sshv.date_actual,sshv.product_gnm_sk 
from store_stock_history_view sshv 
left join store_stock_history_adj ssha on ssha.storeproduct_sk = sshv.storeproduct_sk and ssha.store_sk = sshv.store_sk and ssha.source_staged = sshv.date_actual
and ssha.takeon_total = sshv.takeon_total and ssha.total = sshv.cumulative_total and ssha.prior_total = sshv.prior_total and  ssha.offcatalogue_reciepted_total = sshv.offcat_total
where ssha.sk is null

select * from daily_adjustments_sum;

-------------------------------------------------------------
--- view combining all costs
set role scire;
SET search_path = public, pg_catalog;

drop view store_stockmovement_costs;

create view store_stockmovement_costs as
select sm.sk,
coalesce((select sph.sk from stock_price_history sph 
	where sph.storeproduct_sk = sm.storeproduct_sk and sm.adjustmentdate >= sph.created and sm.adjustmentdate < sph.end_date order by sph.sk desc limit 1),0) as stock_price_history_sk,
coalesce((select pga.sk from product_gnm_average_cost_history pga 
	where pga.storeproduct_sk = sm.storeproduct_sk and sm.adjustmentdate >= pga.adjustmentdate and sm.adjustmentdate < pga.end_date order by pga.sk desc limit 1),0) as product_gnm_average_cost_history_sk

	from store_stockmovement sm 


select * from store_stockmovement_costs ssc
inner join stock_price_history sph on ssc.stock_price_history_sk = sph.sk
inner join product_gnm_average_cost_history pga on pga.sk = ssc.product_gnm_average_cost_history_sk

create materialized view store_stockmovement_costs as
select 
	ssc.store_stockmovement_sk, 
	pga.sk as product_gnm_average_cost_history_sk,
	pga.adjustmentdate as avg_adjustmentdate,
	pga.end_date as avg_end_date,
	pga.quantity as avg_quantity,
	pga.adjustment as avg_adjustment,
	pga.running_total as avg_running_total,
	pga.actual_cost as avg_actual_cost,
	pga.actual_cost_gst as avg_actual_cost_gst,
	pga.average_cost,
	pga.average_cost_gst,
	sph.sk as stock_price_history_sk,
	coalesce(sph.store_sk, pga.store_sk,  0) as store_sk,
	coalesce(sph.storeproduct_sk, pga.storeproduct_sk, 0) as storeproduct_sk,
	coalesce(sph.product_gnm_sk, pga.product_gnm_sk, 0) as product_gnm_sk,
	supplier_cost, 
	supplier_cost_gst, 
	sph.actual_cost as history_actual_cost, 
	sph.actual_cost_gst as history_actual_cost_gst, 
	supplier_rrp, 
	supplier_rrp_gst, 
	calculated_retail_price, 
	calculated_retail_price_gst, 
	override_retail_price, 
	override_retail_price_gst, 
	retail_price, 
	retail_price_gst, 
	sph.created as stock_price_history_created, 
	sph.end_date as stock_price_history_end_date
from (select sm.sk as store_stockmovement_sk,
coalesce((select sph.sk from stock_price_history sph 
	where sph.storeproduct_sk = sm.storeproduct_sk and sm.adjustmentdate >= sph.created and sm.adjustmentdate < sph.end_date order by sph.sk desc limit 1),0) as stock_price_history_sk,
coalesce((select pga.sk from product_gnm_average_cost_history pga 
	where pga.storeproduct_sk = sm.storeproduct_sk and sm.adjustmentdate >= pga.adjustmentdate and sm.adjustmentdate < pga.end_date order by pga.sk desc limit 1),0) as product_gnm_average_cost_history_sk
from store_stockmovement sm ) ssc
inner join stock_price_history sph on ssc.stock_price_history_sk = sph.sk
inner join product_gnm_average_cost_history pga on pga.sk = ssc.product_gnm_average_cost_history_sk
inner join store_stock_history_adj on 
where 
	pga.adjustmentdate > '2017-07-01'

	
	drop view store_stockmovement_costs;


create materialized view store_stockmovement_costs as
select 
	sm.sk as store_stockmovement_sk, 
	pga.sk as product_gnm_average_cost_history_sk,
	pga.adjustmentdate as avg_adjustmentdate,
	pga.end_date as avg_end_date,
	pga.quantity as avg_quantity,
	pga.adjustment as avg_adjustment,
	pga.running_total as avg_running_total,
	pga.actual_cost as avg_actual_cost,
	pga.actual_cost_gst as avg_actual_cost_gst,
	pga.average_cost,
	pga.average_cost_gst,
	sph.sk as stock_price_history_sk,
	coalesce(sph.store_sk, pga.store_sk,  0) as store_sk,
	coalesce(sph.storeproduct_sk, pga.storeproduct_sk, 0) as storeproduct_sk,
	coalesce(sph.product_gnm_sk, pga.product_gnm_sk, 0) as product_gnm_sk,
	supplier_cost, 
	supplier_cost_gst, 
	sph.actual_cost as history_actual_cost, 
	sph.actual_cost_gst as history_actual_cost_gst, 
	supplier_rrp, 
	supplier_rrp_gst, 
	calculated_retail_price, 
	calculated_retail_price_gst, 
	override_retail_price, 
	override_retail_price_gst, 
	retail_price, 
	retail_price_gst, 
	sph.created as stock_price_history_created, 
	sph.end_date as stock_price_history_end_date
from  store_stockmovement sm
left join stock_price_history sph on sph.storeproduct_sk = sm.storeproduct_sk and sm.adjustmentdate >= sph.created and sm.adjustmentdate < sph.end_date
left join product_gnm_average_cost_history pga on pga.storeproduct_sk = sm.storeproduct_sk and sm.adjustmentdate >= pga.adjustmentdate and sm.adjustmentdate < pga.end_date
left join store_stock_history_adj ssh on ssh.source_staged = sm.adjustmentdate
where sm.sk not in (select store_stockmovement_sk from test_stockmovements_jp)

create index on store_stockmovement_costs (store_stockmovement_sk);


--	pga.adjustmentdate > '2017-07-01'


select count(*) from (
select * from store_stockmovement_costs 
where product_gnm_average_cost_history_sk is not null) x

select count(*) from (
select * from store_stockmovement_costs 
where storeproduct_sk = 0) x

select count(*) from store_stockmovement

select store_stockmovement_sk,count(*) cnt into test_stockmovements_jp from store_stockmovement_costs group by store_stockmovement_sk having count(*) > 1

select * from test_stockmovements_jp 

select sm.* from store_stockmovement_costs  smc 
inner join store_stockmovement sm on sm.sk = smc.store_stockmovement_sk
where smc.storeproduct_sk = 0


/*
 drop from store_stockmovement
 
 actual_cost
 actual_cost_value
 moving_quantity
 moving_actual_cost_value
 moving_cost_per_unit
 
 adding 
 
 NOTE
 NOTE2
 
 
 ------------------------------------------------------------
 --- Alternately maintain -----------------------------------
 
  stock_price_history_sk
  product_gnm_average_cost_history_sk
 
 
 */



select * from product_gnm_average_cost_history where store_stockmovement_sk = 57221

select * from store_stockmovement_costs where store_stockmovement_sk = 57221

select * from store_stockmovement where sk in (select  store_stockmovement_sk from test_stockmovements_jp )


select * from stock_price_history where storeproduct_sk in (
select storeproduct_sk from  store_stockmovement where sk in (select  store_stockmovement_sk from test_stockmovements_jp )
)




select  from adjustment_type group by other


select * from store_stockmovement where adjustment_type_sk = 320


drop materialized view store_stockmovement_costs_value;

create materialized view store_stockmovement_costs_value as
select 
	sm.sk as store_stockmovement_sk, 
	pga.sk as product_gnm_average_cost_history_sk,
	pga.adjustmentdate as avg_adjustmentdate,
	pga.end_date as avg_end_date,
	pga.quantity as avg_quantity,
	pga.adjustment as avg_adjustment,
	pga.running_total as avg_running_total,
	pga.actual_cost as avg_actual_cost,
	pga.actual_cost_gst as avg_actual_cost_gst,
	pga.average_cost,
	pga.average_cost_gst,
	sph.sk as stock_price_history_sk,
	coalesce(sph.store_sk, pga.store_sk,  0) as store_sk,
	coalesce(sph.storeproduct_sk, pga.storeproduct_sk, 0) as storeproduct_sk,
	coalesce(sph.product_gnm_sk, pga.product_gnm_sk, 0) as product_gnm_sk,
	supplier_cost, 
	supplier_cost_gst, 
	sph.actual_cost as history_actual_cost, 
	sph.actual_cost_gst as history_actual_cost_gst, 
	supplier_rrp, 
	supplier_rrp_gst, 
	calculated_retail_price, 
	calculated_retail_price_gst, 
	override_retail_price, 
	override_retail_price_gst, 
	retail_price, 
	retail_price_gst, 
	sph.created as stock_price_history_created, 
	sph.end_date as stock_price_history_end_date,
	pga.average_cost * ssh.total as total_value,
	ssh.sk as store_stock_history_adj_sk
from  store_stockmovement sm
left join stock_price_history sph on sph.storeproduct_sk = sm.storeproduct_sk and sm.adjustmentdate >= sph.created and sm.adjustmentdate < sph.end_date
left join product_gnm_average_cost_history pga on pga.storeproduct_sk = sm.storeproduct_sk and sm.adjustmentdate >= pga.adjustmentdate and sm.adjustmentdate < pga.end_date
left join store_stock_history_adj ssh on ssh.source_staged = sm.adjustmentdate and sm.storeproduct_sk = ssh.storeproduct_sk
where sm.sk <> 0


select * from store_stockmovement_costs_value where total_value is  not null;


---------------
-- 1 daves queries
-- determine when something has been cancelled in what phase
-- 

select * from invoice_orders_frame

select status, count(*) from job_history group by status
select queue, count(*) from job_history group by queue

select 
 ROW_NUMBER () OVER (
 PARTITION BY order_sk,nk
 ORDER BY
 source_modified
 ),*
 from job_history
 where order_sk in (select j.order_sk from job_history j where queue = 'CANCELLED' ) 
 and order_sk in (select order_sk from job_history group by order_sk having count(*) > 2)


-----------------------------
-- gnm queries

 set role scire;
SET search_path = gnm,public, pg_catalog;

select ROW_NUMBER () OVER (
 PARTITION BY j.order_id,j.id,j.product_id
 ORDER BY
 qu.created_date_time
 ),
 j.id,j.order_id,j.product_id,j.supplier_id,j.status,qu.
from gnm.job j 
inner join job_queue jq on jq.job_id = j.id 
inner join queue_update qu on qu.id = jq.queue_id
where j.created_date_time > '2017-08-01' 

select count(*) from (
select 
 j.id,j.order_id,product_id,count(*)
from gnm.job j 
inner join job_queue jq on jq.job_id = j.id 
inner join queue_update qu on qu.id = jq.queue_id
where j.created_date_time > '2017-08-01'
group by j.order_id,j.id,product_id
having count(*) > 10
order by count(*) desc,order_id
) x

---- order with high number of job status updates > 400
select ROW_NUMBER () OVER (
 PARTITION BY j.order_id,j.id,j.product_id
 ORDER BY
 qu.created_date_time
 ),
 j.id,j.order_id,j.product_id,j.supplier_id,j.status,j.job_type_id,qu.*
from gnm.job j 
inner join job_queue jq on jq.job_id = j.id 
inner join queue_update qu on qu.id = jq.queue_id
where j.created_date_time > '2017-08-01'  and j.order_id = '1650625'

---- 
select * from gnm.order o
inner join store s on o.store_id = s.id
where order_id = '1650625'

select ROW_NUMBER () OVER (
 PARTITION BY j.order_id,j.id,j.product_id
 ORDER BY
 qu.created_date_time
 ),
 qu.queue,
 lag(qu.queue,1) OVER (
 PARTITION BY j.order_id,j.id,j.product_id
 ORDER BY
 qu.created_date_time
 ) prior_queue,
 j.id,j.order_id,j.product_id,j.supplier_id,j.status,j.queue,qu.*
from gnm.job j 
inner join job_queue jq on jq.job_id = j.id 
inner join queue_update qu on qu.id = jq.queue_id
where j.created_date_time > '2017-08-01' 


------------------------------------------
select status, count(*) from gnm.job group by status order by count(*) desc;
select queue, count(*) from gnm.job group by queue order by count(*) desc;

select * from
(select ROW_NUMBER () OVER (
 PARTITION BY j.order_id,j.id,j.product_id
 ORDER BY
 qu.created_date_time
 ),
 qu.queue,
 coalesce(lag(qu.queue,1) OVER (
 PARTITION BY j.order_id,j.id,j.product_id
 ORDER BY
 qu.created_date_time
 ),'start') as prior_queue,
 j.id job_id,j.order_id,j.product_id,j.supplier_id,j.status job_status,j.queue job_queue,qu.id queue_status_id, qu.comment, qu.created_date_time
from gnm.job j 
inner join job_queue jq on jq.job_id = j.id 
inner join queue_update qu on qu.id = jq.queue_id
where -- j.created_date_time > '2017-08-01' and 
	j.id in (select id from job where queue in ('CANCELLED'))) x
where x.queue <> x.prior_queue

SELECT  j.*, value glazing_action FROM gnm.job j inner join gnm.job_attribute ja on ja.job_id = j.id where name = 'frame_glazing_action'



--------------------------------------------------------------------------------------------
--- view combining all costs
set role scire;
SET search_path = public, pg_catalog;

drop materialized view store_stockmovement_costs_value;

create materialized view store_stockmovement_costs_value as
select 
	sm.sk as store_stockmovement_sk, 
	pga.sk as product_gnm_average_cost_history_sk,
	pga.adjustmentdate as avg_adjustmentdate,
	pga.end_date as avg_end_date,
	pga.quantity as avg_quantity,
	pga.adjustment as avg_adjustment,
	pga.running_total as avg_running_total,
	pga.actual_cost as avg_actual_cost,
	pga.actual_cost_gst as avg_actual_cost_gst,
	pga.average_cost,
	pga.average_cost_gst,
	sph.sk as stock_price_history_sk,
	coalesce(sph.store_sk, pga.store_sk,  0) as store_sk,
	coalesce(sph.storeproduct_sk, pga.storeproduct_sk, 0) as storeproduct_sk,
	coalesce(sph.product_gnm_sk, pga.product_gnm_sk, 0) as product_gnm_sk,
	supplier_cost, 
	supplier_cost_gst, 
	sph.actual_cost as history_actual_cost, 
	sph.actual_cost_gst as history_actual_cost_gst, 
	supplier_rrp, 
	supplier_rrp_gst, 
	calculated_retail_price, 
	calculated_retail_price_gst, 
	override_retail_price, 
	override_retail_price_gst, 
	retail_price, 
	retail_price_gst, 
	sph.created as stock_price_history_created, 
	sph.end_date as stock_price_history_end_date,
	x.total,
	pga.average_cost * x.total as total_value
from  store_stockmovement sm
inner join  (select sm.sk, (select ssh.total from store_stock_history_adj ssh 
				where sm.storeproduct_sk = ssh.storeproduct_sk and ssh.source_staged < sm.adjustmentdate order by created desc limit 1)
	as total from  store_stockmovement sm) x using (sk)
left join stock_price_history sph on sph.storeproduct_sk = sm.storeproduct_sk and sm.adjustmentdate >= sph.created and sm.adjustmentdate < sph.end_date
left join product_gnm_average_cost_history pga on pga.storeproduct_sk = sm.storeproduct_sk and sm.adjustmentdate >= pga.adjustmentdate and sm.adjustmentdate < pga.end_date
where sm.storeproduct_sk <> 0 


and x.total is not null and sm.product_gnm_sk <> 0


select source_staged,count(*) from store_stock_history_adj group by source_staged order  by source_staged desc
select * from store_stock_history_adj 

 select sm.*, (select ssh.total from store_stock_history_adj ssh where sm.storeproduct_sk = ssh.storeproduct_sk order by create desc limit 1) as total from  store_stockmovement sm







select storeproduct_sk, count(*) from stock_price_history sph
where storeproduct_sk <> 0
group by storeproduct_sk 
--having count(*) > 1
order by storeproduct_sk,count(*) desc

select count(*) from (
select * from stock_price_history where storeproduct_sk <> 0 
) x

select count(*) from (
select * from stock_price_history where product_gnm_sk <> 0 and product_gnm_sk is not null
) x

select count(*) from (
select * from stock_price_history where product_gnm_sk <> 0 and product_gnm_sk is  null
) x


select 
	sm.*, sph.*,pga.*,
	pga.average_cost * x.total as total_value
from  store_stockmovement sm
inner join  (select sm.sk, (select ssh.total from store_stock_history_adj ssh 
				where sm.storeproduct_sk = ssh.storeproduct_sk and ssh.source_staged < sm.adjustmentdate order by created desc limit 1)
	as total from  store_stockmovement sm) x using (sk)
left join stock_price_history sph on sph.storeproduct_sk = sm.storeproduct_sk and sm.adjustmentdate >= sph.created and sm.adjustmentdate < sph.end_date
left join product_gnm_average_cost_history pga on pga.storeproduct_sk = sm.storeproduct_sk and sm.adjustmentdate >= pga.adjustmentdate and sm.adjustmentdate < pga.end_date
where sm.sk <> 0 and x.total is not null

--------------------------------------------------------------------------------------------
--- view combining all costs
set role scire;
SET search_path = public, pg_catalog;

drop  view store_stockmovement_costs_value_jp;
-- drop materialized view store_stockmovement_costs_value_jp;

-- create materialized view store_stockmovement_costs_value_jp as
create view store_stockmovement_costs_value_jp as
select 
	sm.sk as store_stockmovement_sk, 
	sm.storeproduct_sk,
	pga.sk as product_gnm_average_cost_history_sk,
	pga.adjustmentdate as avg_adjustmentdate,
	pga.end_date as avg_end_date,
	pga.quantity as avg_quantity,
	pga.adjustment as avg_adjustment,
	pga.running_total as avg_running_total,
	pga.actual_cost as avg_actual_cost,
	pga.actual_cost_gst as avg_actual_cost_gst,
	pga.average_cost,
	pga.average_cost_gst,
	sph.sk as stock_price_history_sk,
	coalesce(sph.store_sk, pga.store_sk,  0) as store_sk,
	coalesce(sph.product_gnm_sk, pga.product_gnm_sk, 0) as product_gnm_sk,
	supplier_cost, 
	supplier_cost_gst, 
	sph.actual_cost as history_actual_cost, 
	sph.actual_cost_gst as history_actual_cost_gst, 
	supplier_rrp, 
	supplier_rrp_gst, 
	calculated_retail_price, 
	calculated_retail_price_gst, 
	override_retail_price, 
	override_retail_price_gst, 
	retail_price, 
	retail_price_gst, 
	sph.created as stock_price_history_created, 
	sph.end_date as stock_price_history_end_date,
	x.total,
	pga.average_cost * x.total as total_value
from  store_stockmovement sm
inner join  (select sm.sk, (select ssh.total from store_stock_history_adj ssh 
				where sm.storeproduct_sk = ssh.storeproduct_sk and ssh.source_staged < sm.adjustmentdate order by created desc limit 1)
	as total from  store_stockmovement sm) x using (sk)
left join stock_price_history sph on sph.storeproduct_sk = sm.storeproduct_sk and sm.adjustmentdate >= sph.created and sm.adjustmentdate < sph.end_date
left join product_gnm_average_cost_history pga on pga.storeproduct_sk = sm.storeproduct_sk and sm.adjustmentdate >= pga.adjustmentdate and sm.adjustmentdate < pga.end_date
where sm.storeproduct_sk <> 0 ;


--------------------------------------------------------------------------------------
--- validation

select sm.storeproduct_sk, count(*)
from  store_stockmovement sm
where sm.storeproduct_sk <> 0 
group by sm.storeproduct_sk
order by count(*) desc ;

select sm.storeproduct_sk, count(*)
from  store_stockmovement sm
inner join  (select sm.sk, (select ssh.total from store_stock_history_adj ssh 
				where sm.storeproduct_sk = ssh.storeproduct_sk and ssh.source_staged < sm.adjustmentdate order by created desc limit 1)
	as total from  store_stockmovement sm) x using (sk)
-- left join stock_price_history sph on sph.storeproduct_sk = sm.storeproduct_sk and sm.adjustmentdate >= sph.created and sm.adjustmentdate < sph.end_date
--left join product_gnm_average_cost_history pga on pga.storeproduct_sk = sm.storeproduct_sk and sm.adjustmentdate >= pga.adjustmentdate and sm.adjustmentdate < pga.end_date
where sm.storeproduct_sk <> 0 
group by sm.storeproduct_sk
order by count(*) desc ;

select sm.storeproduct_sk, count(*)
from  store_stockmovement sm
inner join  (select sm.sk, (select ssh.total from store_stock_history_adj ssh 
				where sm.storeproduct_sk = ssh.storeproduct_sk and ssh.source_staged < sm.adjustmentdate order by created desc limit 1)
	as total from  store_stockmovement sm) x using (sk)
left join stock_price_history sph on sph.storeproduct_sk = sm.storeproduct_sk and sm.adjustmentdate >= sph.created and sm.adjustmentdate < sph.end_date
--left join product_gnm_average_cost_history pga on pga.storeproduct_sk = sm.storeproduct_sk and sm.adjustmentdate >= pga.adjustmentdate and sm.adjustmentdate < pga.end_date
where sm.storeproduct_sk <> 0 
group by sm.storeproduct_sk
order by count(*) desc ;

select sm.storeproduct_sk, count(*)
from  store_stockmovement sm
inner join  (select sm.sk, (select ssh.total from store_stock_history_adj ssh 
				where sm.storeproduct_sk = ssh.storeproduct_sk and ssh.source_staged < sm.adjustmentdate order by created desc limit 1)
	as total from  store_stockmovement sm) x using (sk)
left join stock_price_history sph on sph.storeproduct_sk = sm.storeproduct_sk and sm.adjustmentdate >= sph.created and sm.adjustmentdate < sph.end_date
left join product_gnm_average_cost_history pga on pga.storeproduct_sk = sm.storeproduct_sk and sm.adjustmentdate >= pga.adjustmentdate and sm.adjustmentdate < pga.end_date
where sm.storeproduct_sk <> 0 
group by sm.storeproduct_sk
order by count(*) desc ;

select storeproduct_sk, count(*) from store_stockmovement_costs_value_jp group by storeproduct_sk order by count(*) desc ;
t
create view store_stockmovement_costs_value as
select 
	sm.sk as store_stockmovement_sk, 
	sm.storeproduct_sk,
	pga.sk as product_gnm_average_cost_history_sk,
	pga.adjustmentdate as avg_adjustmentdate,
	pga.end_date as avg_end_date,
	pga.quantity as avg_quantity,
	pga.adjustment as avg_adjustment,
	pga.running_total as avg_running_total,
	pga.actual_cost as avg_actual_cost,
	pga.actual_cost_gst as avg_actual_cost_gst,
	pga.average_cost,
	pga.average_cost_gst,
	sph.sk as stock_price_history_sk,
	coalesce(sph.store_sk, pga.store_sk,  0) as store_sk,
	coalesce(sph.product_gnm_sk, pga.product_gnm_sk, 0) as product_gnm_sk,
	supplier_cost, 
	supplier_cost_gst, 
	sph.actual_cost as history_actual_cost, 
	sph.actual_cost_gst as history_actual_cost_gst, 
	supplier_rrp, 
	supplier_rrp_gst, 
	calculated_retail_price, 
	calculated_retail_price_gst, 
	override_retail_price, 
	override_retail_price_gst, 
	retail_price, 
	retail_price_gst, 
	sph.created as stock_price_history_created, 
	sph.end_date as stock_price_history_end_date,
	x.total,
	pga.average_cost * x.total as total_value
from  store_stockmovement sm
inner join  (select sm.sk, (select ssh.total from store_stock_history_adj ssh 
				where sm.storeproduct_sk = ssh.storeproduct_sk and ssh.source_staged < sm.adjustmentdate order by created desc limit 1)
	as total from  store_stockmovement sm) x using (sk)
left join stock_price_history sph on sph.storeproduct_sk = sm.storeproduct_sk and sm.adjustmentdate >= sph.created and sm.adjustmentdate < sph.end_date
left join product_gnm_average_cost_history pga on pga.storeproduct_sk = sm.storeproduct_sk and sm.adjustmentdate >= pga.adjustmentdate and sm.adjustmentdate < pga.end_date
where sm.storeproduct_sk <> 0 ;




select store_sk,count(*)from stocktake group by store_sk;


-- add stocktake 


            SELECT 
            0 as store_sk, id as nk, 0 as storeproduct_sk, sp.sk as product_gnm_sk, 
            pch.supplier_cost::numeric, pch.supplier_cost_gst::numeric, 
            pch.actual_cost::numeric, pch.actual_cost_gst::numeric, 
            pch.supplier_rrp::numeric, pch.supplier_rrp_gst::numeric, pch.calculated_retail_price::numeric, pch.calculated_retail_price_gst::numeric, 
            0 as override_retail_price,0 as override_retail_price_gst,
            0 as retail_price,0 as retail_price_gst,
            pch.modified_date_time::TimeSTAMP without time zone as created
            FROM stage.today_gnm_db_cost_history pch 
            left join product_gnm sp on sp.productid = pch.product_id


---------------------------------------------------------------------------------------------------------------
  --- view combining all costs
set role postgres;
SET search_path = gnm,public, pg_catalog;
          
select x.* from
(select ROW_NUMBER () OVER (
 PARTITION BY j.order_id,j.id,j.product_id
 ORDER BY
 qu.created_date_time
 ),
 qu.queue,
 coalesce(lag(qu.queue,1) OVER (
 PARTITION BY j.order_id,j.id,j.product_id
 ORDER BY
 qu.created_date_time
 ),'start') as prior_queue,
 j.id job_id,j.order_id,j.product_id,j.supplier_id,j.status job_status,j.queue job_queue,qu.id queue_status_id, qu.comment, qu.created_date_time
from gnm.job j 
inner join job_queue jq on jq.job_id = j.id 
inner join queue_update qu on qu.id = jq.queue_id
where -- j.created_date_time > '2017-08-01' and 
	j.id in (select id from job where queue in ('CANCELLED'))) x
where x.queue <> x.prior_queue 


select x.*, value glazing_action from (select ROW_NUMBER () OVER ( PARTITION BY j.order_id,j.id,j.product_id ORDER BY qu.created_date_time ), qu.queue, coalesce(lag(qu.queue,1) OVER ( PARTITION BY j.order_id,j.id,j.product_id ORDER BY qu.created_date_time ),'start') as prior_queue, j.id job_id,j.order_id,j.product_id,j.supplier_id,j.status job_status,j.queue job_queue,qu.id queue_status_id, qu.comment, qu.created_date_time from gnm.job j inner join job_queue jq on jq.job_id = j.id  inner join queue_update qu on qu.id = jq.queue_id where j.id in (select id from job where queue in ('CANCELLED'))) x inner join job_attribute ja using(job_id) where x.queue <> x.prior_queue  and ja.name = 'frame_glazing_action'

SELECT  j.*, value glazing_action FROM gnm.job j inner join gnm.job_attribute ja on ja.job_id = j.id 
where name = 'frame_glazing_action'



truncate table daily_adjustments_sum;
truncate table job_history;
truncate table order_history;

truncate table store_stockmovement cascade;
truncate table product_gnm_average_cost_history;
truncate table product_gnm_average_cost_history_reset;

truncate table stocktake_item;
truncate table stocktake cascade;

truncate table stock_price_history cascade;

truncate table store_stock_history_adj;
truncate table store_stock_history;



select * from  daily_adjustments_sum;
select * from  job_history;
select * from  order_history;

select * from  store_stockmovement ;
select * from  product_gnm_average_cost_history;
select * from  product_gnm_average_cost_history_reset;

select * from  stocktake_item;
select * from  stocktake cascade;

select * from  stock_price_history cascade;

select * from  store_stock_history_adj;
select * from  store_stock_history;



--------------------------------------------------------------------------------------

INSERT INTO public.stock_price_history
(sk, store_sk, nk, storeproduct_sk, supplier_cost, supplier_cost_gst, actual_cost, actual_cost_gst, supplier_rrp, 
supplier_rrp_gst, calculated_retail_price, calculated_retail_price_gst, override_retail_price, override_retail_price_gst, 
retail_price, retail_price_gst, created, product_gnm_sk, end_date)
VALUES(nextval('stock_price_history_sk_seq'::regclass), 0, '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, now(), 0, '2030-01-01');

select * from stock_price_history where sk  = 0;


--------------------------------------------------------------------------------------




select count(*) from store_stockmovement;
select count(*) from daily_adjustments_sum;


alter table job_history add column cancel_type varchar(100);
alter table store_stockmovement add column stocktake_sk integer;
alter table store_stockmovement add column stocktake_item_sk integer;
alter table store_stockmovement add column batch_sk integer;

alter table store_stockmovement add constraint stocktake_sk_constraint foreign key (stocktake_sk) references stocktake(sk);

alter table store_stockmovement add constraint stocktake_item_sk_constraint foreign key (stocktake_item_sk) references stocktake_item(sk);

alter table store_stockmovement add constraint stocktake_item_sk_constraint foreign key (stocktake_item_sk) references stocktake_item(sk);


drop table if exists stockmovement_batch cascade;

CREATE TABLE stockmovement_batch (
    sk serial NOT NULL,
    store_sk integer references store(sk),
	nk text,
    created timestamp without time zone DEFAULT now(),
    primary key(sk)
);


----------------------------------------------------------------------------------------------------------------------------
--- update job view to include new fields

drop view stage.today_gnm_db_job;

select stage.generate_view_for_table('gnm_db_job');

select * from stage.today_gnm_db_job;

            select
            js.job_id as nk, js.order_id,
            -- order sk
            oh.sk as order_sk,
            js.job_type_id as job_type,
            js.prior_queue as cancel_type,
            coalesce(p.sk,0) as product_sk, coalesce(s.sk,0) as supplier_sk,
            js.queue, js.status,
            js.glazing_action as frame_glazing_action,
            (case when  glazing_action = '5' then '3' else glazing_action end) as zeiss_glazing_action,
            js.created_date_time::TimeSTAMP without time zone as source_created, 
            js.modified_date_time::TimeSTAMP without time zone as source_modified
            -- select *
            from stage.today_gnm_db_job js 
            inner join order_history oh on oh.nk = js.order_id
            inner join public.supplier s on CAST(s.externalref as varchar(100) ) = js.supplier_id
            inner join public.product_gnm p on js.product_id =   p.productid --CAST( p.productid as varchar(100))
            


select * from stocktake_item where last_purchased_date is not null


select * from stage.today_gnm_optomate_v_stocktake_log;
select * from stage.today_gnm_sunix_vfrstake;

select * from stage.today_gnm_sunix_vfst;

select * from stage.today_gnm_sunix_vrestock;


------------------------------------------------------------------------------------------------------------------------------------------

----------- 
set role scire;
SET search_path = public, pg_catalog;

INSERT INTO public.stocktake
( nk,
store_sk, 
planned_stocktake_start_date_time, 
planned_stocktake_end_date_time, 
actual_stocktake_start_date_time, 
actual_stocktake_end_date_time, 
created_user, 
created_date_time, 
modified_user, 
modified_date_time, 
created)
SELECT filename,s.sk,maxdate,maxdate,maxdate,maxdate,'system', now(), 'system', now(), now()
FROM stage.today_gnm_sunix_vfst st
inner join (select max(to_timestamp(substring(filename,4,10) || ' ' || overlay( replace(substring(filename,15,11),'-',':')  placing ' ' from 9 for 1 ) , 'DD-MM-YYYY HH12:MI:SS AM')) maxdate
 from stage.today_gnm_sunix_vfst ) x on x.maxdate = to_timestamp(substring(filename,4,10) || ' ' || overlay( replace(substring(filename,15,11),'-',':')  placing ' ' from 9 for 1 ) , 'DD-MM-YYYY HH12:MI:SS AM')
 inner join store s on st."source" = s.storeid
 left join stocktake stk on stk.nk = st.filename
where 
	stk.sk is null
group by st.filename, maxdate,s.sk;

INSERT INTO public.stocktake_item
( 
	nk,
	stocktake_sk, 
	product_gnm_sk, 
	store_product_sk, 
	expected_qty, 
	actual_qty, 
	diff,
	avg_cost_incl_gst, 
	avg_cost_gst,
	pms_user,
	last_purchased_date, 
	created_user, 
	created_date_time, 
	modified_user, 
	modified_date_time, 
	created
)
-- VALUES(nextval('stocktake_item_sk_seq'::regclass), '', 0, 0, 0, 0, 0, 0, 0, '', '', 'system'::character varying,timezone('utc'::text, now()), 'system'::character varying, timezone('utc'::text, now()), timezone('utc'::text, now()));
select "FRAMENUM",st.sk,sp.product_gnm_sk,sp.sk,case when x."QTY" = '' then null else x."QTY"::integer end,x."QTYINSTK"::integer ,x."DIFF"::integer,x."AVGCOST"::numeric,null,'system',null,'system',now(),'system',now(),now()
--select sp.sk,x.*
from storeproduct sp 
inner join stage.today_gnm_sunix_vfst x on  x."FRAMENUM" = sp."description" and x.source = sp.storeid
inner join stocktake st on st.nk = x.filename 
left join stocktake_item sti on sti.stocktake_sk = st.sk and sti.nk = "FRAMENUM"
where  "FRAMENUM" <> ''  and sp."description" <> '' and sti.sk is null;


select * from stage.today_gnm_sunix_vfst 

FST04-12-2017-11-44-57-AM.DBF	2000-027	2017-12-04 11:44:57
FST04-12-2017-12-01-05-AM.DBF	2000-027	2017-12-04 00:01:05

SELECT *
FROM stage.today_gnm_sunix_vfst st

select filename,source,max(to_timestamp(substring(filename,4,10) || ' ' || overlay( replace(substring(filename,15,11),'-',':')  placing ' ' from 9 for 1 ) , 'DD-MM-YYYY HH12:MI:SS AM')) maxdate
 from stage.today_gnm_sunix_vfst group by filename,source;


FST04-12-2017-11-44-57-AM.DBF	2000-027	2017-12-04 11:44:57
FST04-12-2017-12-01-05-AM.DBF	2000-027	2017-12-04 00:01:05










select to_timestamp(substring(filename,4,10) || ' ' || overlay( replace(substring(filename,15,11),'-',':')  placing ' ' from 9 for 1 ) , 'DD-MM-YYYY HH12:MI:SS AM'),* from stage.today_gnm_sunix_vfst where
'FST04-12-2017-11-44-57-AM.DBF' = filename




SELECT filename as nk,s.sk as store_sk,
            maxdate as planned_stocktake_start_date_time,maxdate as planned_stocktake_end_date_time,
            maxdate as actual_stocktake_start_date_time,maxdate as actual_stocktake_end_date_time,
            'system' as created_user, now() as created_date_time, 'system' as modified_user, 
            now() as modified_date_time, now() as created
FROM stage.today_gnm_sunix_vfst st
inner join (select max(to_timestamp(substring(filename,4,10) || ' ' || overlay( replace(substring(filename,15,11),'-',':')  placing ' ' from 9 for 1 ) , 'DD-MM-YYYY HH12:MI:SS AM')) maxdate
					,source 
from stage.today_gnm_sunix_vfst group by source ) x on 
   x.maxdate = to_timestamp(substring(filename,4,10) || ' ' || overlay( replace(substring(filename,15,11),'-',':')  placing ' ' from 9 for 1 ) , 'DD-MM-YYYY HH12:MI:SS AM') 
   	and x.source = st."source"
left join store s on st."source" = s.storeid
left join stocktake stk on stk.nk = st.filename
where 
	stk.sk is null
group by st.filename, maxdate,s.sk;


SELECT st.filename as nk,s.sk as store_sk,
            maxdate as planned_stocktake_start_date_time,maxdate as planned_stocktake_end_date_time,
            maxdate as actual_stocktake_start_date_time,maxdate as actual_stocktake_end_date_time,
            'system' as created_user, now() as created_date_time, 'system' as modified_user, 
            now() as modified_date_time, now() as created
FROM stage.today_gnm_sunix_vfst st
inner join (select max(to_timestamp(substring(replace(filename,'00','01'),4,10) || ' ' || overlay( replace(substring(replace(filename,'00','01'),15,11),'-',':')  placing ' ' from 9 for 1 ) ,
																																										'DD-MM-YYYY HH12:MI:SS AM')) maxdate
				, source
 from stage.today_gnm_sunix_vfst group by source) x on 
 	x.maxdate = to_timestamp(substring(replace(filename,'00','01'),4,10) || ' ' || overlay( replace(substring(replace(filename,'00','01'),15,11),'-',':')  placing ' ' from 9 for 1 ) , 'DD-MM-YYYY HH12:MI:SS AM') 
 		and x.source = st."source"
 left join store s on st."source" = s.storeid
 left join stocktake stk on stk.nk = st.filename
where 
	stk.sk  is not null
group by s.sk, maxdate, st.filename;


 
select filename,replace(filename,'00','01'),count(*) from stage.today_gnm_sunix_vfst  group by filename

select * from stocktake st inner join stocktake_item sti on sti.stocktake_sk = st.sk
left join (select sti.sk stocktake_item_sk, 
					(select  max(sm.sk) store_stockmovement_sk 
						from store_stockmovement sm where sm.adjustmentdate = max(sm.adjustmentdate) group by sm.storeproduct_sk order by sm.adjustmentdate_sk desc limit 1)

			from stocktake_item sti
				left join  vstore_stockmovement sm on sti.store_product_sk = sm.storeproduct_sk -- and sti.created_date_time <= sm.adjustmentdate
						where sm.name = 'Receipt') x on x.sk = sm.sk 


select * from adjustment_type

 (select sm.sk, sm.storeproduct_sk , row_number over ( partition by sm.storeproduct_sk order by sm.adjustmentdate desc limit 1 ) 

(select  max(sm.sk) store_stockmovement_sk 
						from store_stockmovement sm where sm.adjustmentdate = max(sm.adjustmentdate) group by sm.storeproduct_sk order by sm.adjustmentdate_sk desc limit 1)
 

-- 1. join stocktake item to the last stock reciept for that product / date is commented out just to get data

-- rollback
				commit;

begin ;
update stocktake_item  set last_purchased_date = sm.adjustmentdate
-- select sm.sk, sm.adjustmentdate
from 
(select sti.sk stocktake_item_sk,sm.storeproduct_sk,count(*), max(sm.sk) max_stockmovement
from stocktake_item sti
 left join  vstore_stockmovement sm on sti.store_product_sk = sm.storeproduct_sk -- and sti.created_date_time <= sm.adjustmentdate
where sm.name = 'Receipt' -- and sti.created_date_time <= sm.adjustmentdate
group by sti.sk,sm.storeproduct_sk) x
inner join store_stockmovement sm on sm.sk = x.max_stockmovement
where stocktake_item.sk = x.stocktake_item_sk


 select sm.sk, sm.adjustmentdate
from 
(select sti.sk stocktake_item_sk,sm.storeproduct_sk,count(*), max(sm.sk) max_stockmovement
from stocktake_item sti
 left join  vstore_stockmovement sm on sti.store_product_sk = sm.storeproduct_sk -- and sti.created_date_time <= sm.adjustmentdate
where sm.name = 'Receipt' -- and sti.created_date_time <= sm.adjustmentdate
group by sti.sk,sm.storeproduct_sk) x
inner join store_stockmovement sm on sm.sk = x.max_stockmovement
inner join store_stockmovement sm  stocktake_item.sk = x.stocktake_item_sk


select * from stocktake_item si where si.last_purchased_date is not null;


select * from stock_price_history sph 
	left join  stocktake_item si on si.store_product_sk = sph.storeproduct_sk
	left join  product_gnm_average_cost_history pga on pga.product_gnm_sk = sph.product_gnm_sk


update stock_price_history sph set supplier_cost = 
select pga.product_gnm_sk,coalesce(pga.average_cost,pga.actual_cost), coalesce(pga.average_cost_gst,pga.average_cost_gst) 
from stock_price_history sph 
--	left join  stocktake_item si on si.store_product_sk = sph.storeproduct_sk
	inner join  product_gnm_average_cost_history pga on pga.product_gnm_sk = sph.product_gnm_sk -- and  sph.store_sk = pga.store_sk
--	where sph.store_sk <> 0
	
	
-------------- update stocktake with average cost	
update stocktake_item sti set avg_cost_incl_gst = coalesce(pga.average_cost,pga.actual_cost), 
	avg_cost_gst = coalesce(pga.average_cost_gst,pga.average_cost_gst) 
from product_gnm_average_cost_history pga where pga.product_gnm_sk = sti.product_gnm_sk

--	left join  stocktake_item si on si.store_product_sk = sph.storeproduct_sk
	inner join  product_gnm_average_cost_history pga on pga.product_gnm_sk = sph.product_gnm_sk -- and  sph.store_sk = pga.store_sk
--	where sph.store_sk <> 0
	
	
--------------------------------------------------
select count(*) from stock_price_history

--- why do the stock take items


select * from daily_adjustments_sum
select * from "store"

-------------------------------------------------------------------------------

SELECT st.filename as nk,s.sk as store_sk,
            maxdate as planned_stocktake_start_date_time,maxdate as planned_stocktake_end_date_time,
            maxdate as actual_stocktake_start_date_time,maxdate as actual_stocktake_end_date_time,
            'system' as created_user, now() as created_date_time, 'system' as modified_user, 
            now() as modified_date_time, now() as created
FROM stage.today_gnm_sunix_vfst st
inner join (select max(to_timestamp(substring(replace(filename,'00','01'),4,10) || ' ' || overlay( replace(substring(replace(filename,'00','01'),15,11),'-',':')  placing ' ' from 9 for 1 ) ,
																																										'DD-MM-YYYY HH12:MI:SS AM')) maxdate
				, source
 from stage.today_gnm_sunix_vfst group by source) x on 
 	x.maxdate = to_timestamp(substring(replace(filename,'00','01'),4,10) || ' ' || overlay( replace(substring(replace(filename,'00','01'),15,11),'-',':')  placing ' ' from 9 for 1 ) , 'DD-MM-YYYY HH12:MI:SS AM') 
 		and x.source = st."source"
 inner join store s on st."source" = s.storeid
 left join stocktake stk on stk.nk = st.filename
where 
	stk.sk  is null
group by s.sk, maxdate, st.filename;




            SELECT st.filename as nk,s.sk as store_sk,
            maxdate as planned_stocktake_start_date_time,maxdate as planned_stocktake_end_date_time,
            maxdate as actual_stocktake_start_date_time,maxdate as actual_stocktake_end_date_time,
            'system' as created_user, now() as created_date_time, 'system' as modified_user, 
            now() as modified_date_time, now() as created
            FROM stage.today_gnm_sunix_vfst st
                            inner join (select max(to_timestamp(substring(replace(filename,'00','01'),4,10) || ' ' || overlay( replace                        (substring(replace(filename,'00','01'),15,11),'-',':')  placing ' ' from 9 for 1 ) ,
																																										'DD-MM-YYYY HH12:MI:SS AM')) maxdate, source
                                   from stage.today_gnm_sunix_vfst group by source) x on 
 	                          x.maxdate = to_timestamp(substring(replace(filename,'00','01'),4,10) || ' ' || overlay( replace(substring(replace(filename,'00','01'),15,11),'-',':')  placing ' ' from 9 for 1 ) , 'DD-MM-YYYY HH12:MI:SS AM') and x.source = st."source"
            inner join store s on st."source" = s.storeid
            left join stocktake stk on stk.nk = st.filename
            where 
	              stk.sk  is null
            group by s.sk, maxdate, st.filename;


select * from stocktake;

select * from stocktake_item;

-------------------------------------------------------------------------------------------------------------------------------------------------------------


select "FRAMENUM" as nk,
	st.sk as stocktake_sk,
	sp.product_gnm_sk as product_gnm_sk,
    sp.sk as store_product_sk,
    coalesce(case when x."QTY" = '' then null else x."QTY"::integer end,0) as actual_qty,
    x."QTYINSTK"::integer as expected_qty,
    x."DIFF"::integer as diff,
--    x."AVGCOST"::numeric as avg_cost_incl_gst,
     null as avg_cost_gst,'system' as pms_user,
     null as last_purchased_date,'system' as created_user,
     now() as created_date_time,
     'system' as modified_user,
     now() as modified_date_time,
     now() as created
            -- select sp.sk,x.*
from storeproduct sp 
            inner join stage.today_gnm_sunix_vfst x on  x."FRAMENUM" = sp."description" and x.source = sp.storeid
            inner join stocktake st on st.nk = x.filename 
            left join product_gnm_average_cost_history ssm on ssm.storeproduct_sk = sp.sk and ii.createddate >= ssm.adjustmentdate and ii.createddate < ssm.end_date
			left join view_last_stock_history_price gnmph on  gnmph.product_gnm_sk = sp.product_gnm_sk and ii.createddate >= gnmph.created and ii.createddate < gnmph.end_date 
 where  "FRAMENUM" <> ''  and sp."description" <> ''


------------------------------------------------------------------------
select "FRAMENUM" as nk,st.sk as stocktake_sk,sp.product_gnm_sk as product_gnm_sk,
            sp.sk as store_product_sk,
            coalesce(case when x."QTY" = '' then null else x."QTY"::integer end,0) as actual_qty,
            x."QTYINSTK"::integer as expected_qty,x."DIFF"::integer as diff,x."AVGCOST"::numeric as avg_cost_incl_gst,
            null as avg_cost_gst,'system' as pms_user,null as last_purchased_date,'system' as created_user,now() as created_date_time,
            'system' as modified_user,now() as modified_date_time,now() as created
            --select sp.sk,x.*
from storeproduct sp 
            inner join stage.today_gnm_sunix_vfst x on  x."FRAMENUM" = sp."description" and x.source = sp.storeid
            inner join stocktake st on st.nk = x.filename 
where  "FRAMENUM" <> ''  and sp."description" <> ''
 

select
	"FRAMENUM" as nk,
	st.sk as stocktake_sk,
	sp.product_gnm_sk as product_gnm_sk,
    sp.sk as store_product_sk,
    coalesce(case when x."QTY" = '' then null else x."QTY"::integer end,0) as actual_qty,
    x."QTYINSTK"::integer as expected_qty,
    x."DIFF"::integer as diff
	,null as avg_cost_incl_gst
	,null as avg_cost_gst
	,null as last_purchased_date
 --   ,coalesce( ssm.average_cost, ssm.actual_cost) as avg_cost_incl_gst
 --   ,coalesce( ssm.average_cost_gst, ssm.actual_cost_gst ) as avg_cost_gst
    ,'system' as pms_user
    ,'system' as created_user, now() as created_date_time
    ,'system' as modified_user, now() as modified_date_time,now() as created
FROM  storeproduct sp 
	inner join stage.today_gnm_sunix_vfst x on  x."FRAMENUM" = sp."description" and x.source = sp.storeid
    inner join stocktake st on st.nk = x.filename 	
--	left join product_gnm_average_cost_history ssm on ssm.storeproduct_sk = sp.sk and st.actual_stocktake_end_date_time >= ssm.adjustmentdate and st.actual_stocktake_end_date_time < ssm.end_date
--	left join view_last_stock_history_price gnmph on  gnmph.product_gnm_sk = sp.product_gnm_sk and st.actual_stocktake_end_date_time >= gnmph.created and st.actual_stocktake_end_date_time < gnmph.end_date 
 where  "FRAMENUM" <> ''  and sp."description" <> ''
 
 
 
------------------------------------------------------------------------

select 
	"FRAMENUM" as nk,
	st.sk as stocktake_sk,
	sp.product_gnm_sk as product_gnm_sk,
    sp.sk as store_product_sk,
    coalesce(case when x."QTY" = '' then null else x."QTY"::integer end,0) as actual_qty,
    x."QTYINSTK"::integer as expected_qty,x."DIFF"::integer as diff,
    x."AVGCOST"::numeric as avg_cost_incl_gst,
    null as avg_cost_gst,'system' as pms_user,
    null as last_purchased_date,'system' as created_user,
    now() as created_date_time,
    'system' as modified_user,
    now() as modified_date_time,
    now() as created
            --select sp.sk,x.*
from storeproduct sp 
            inner join stage.today_gnm_sunix_vfst x on  x."FRAMENUM" = sp."description" and x.source = sp.storeid
            inner join stocktake st on st.nk = x.filename 
            where  "FRAMENUM" <> ''  and sp."description" <> ''
 

            
            
INSERT INTO public.stocktake_item
( 
	nk,
	stocktake_sk, 
	product_gnm_sk, 
	store_product_sk, 
	expected_qty, 
	actual_qty, 
	diff,
	avg_cost_incl_gst, 
	avg_cost_gst,
	pms_user,
	last_purchased_date, 
	created_user, 
	created_date_time, 
	modified_user, 
	modified_date_time, 
	created
)            
select
	"FRAMENUM" as nk,
	st.sk as stocktake_sk,
	sp.product_gnm_sk as product_gnm_sk,
    sp.sk as store_product_sk,
    coalesce(case when x."QTY" = '' then null else x."QTY"::integer end,0) as actual_qty,
    x."QTYINSTK"::integer as expected_qty,
    x."DIFF"::integer as diff
	,null as avg_cost_incl_gst
	,null as avg_cost_gst
	,'system' as pms_user
	,null as last_purchased_date
    ,'system' as created_user
    ,now() as created_date_time
    ,'system' as modified_user, 
    now() as modified_date_time,now() as created
FROM  storeproduct sp 
	inner join stage.today_gnm_sunix_vfst x on  x."FRAMENUM" = sp."description" and x.source = sp.storeid
    inner join stocktake st on st.nk = x.filename 	
    left join stocktake_item sti on sti.stocktake_sk = st.sk and sp.sk = sti.store_product_sk 
 where  "FRAMENUM" <> ''  and sp."description" <> '' and sti.sk is null and sp.sk <>  1844640
 
 
select sti.stocktake_sk, sti.store_product_sk, count(*)
from  storeproduct sp 
	inner join stage.today_gnm_sunix_vfst x on  x."FRAMENUM" = sp."description" and x.source = sp.storeid
    inner join stocktake st on st.nk = x.filename 	
    left join stocktake_item sti on sti.stocktake_sk = st.sk and sp.sk = sti.store_product_sk 
 where  "FRAMENUM" <> ''  and sp."description" <> '' and sti.sk is null
 group by sti.stocktake_sk, sti.store_product_sk
 having count(*) > 1
 
 select sti.stocktake_sk, sti.store_product_sk, count(*)
from  stocktake_item sti 
 group by sti.stocktake_sk, sti.store_product_sk
 having count(*) > 1
 
 
 select * from stocktake st
 
select st.sk, sp.sk,*
from  storeproduct sp 
	inner join stage.today_gnm_sunix_vfst x on  x."FRAMENUM" = sp."description" and x.source = sp.storeid
    inner join stocktake st on st.nk = x.filename 	
    left join stocktake_item sti on sti.stocktake_sk = st.sk and sp.sk = sti.store_product_sk 
 where  "FRAMENUM" <> ''  and sp."description" <> '' and sti.sk is null and sp.sk =  1844640

 
 
 select count(*) from (
 select
	"FRAMENUM" as nk,
	st.sk as stocktake_sk,
	sp.product_gnm_sk as product_gnm_sk,
    sp.sk as store_product_sk,
    coalesce(case when x."QTY" = '' then null else x."QTY"::integer end,0) as actual_qty,
    x."QTYINSTK"::integer as expected_qty,
    x."DIFF"::integer as diff
	,null as avg_cost_incl_gst
	,null as avg_cost_gst
	,null as last_purchased_date
    ,'system' as pms_user
    ,'system' as created_user, 
    now() as created_date_time
    ,'system' as modified_user, 
    now() as modified_date_time,now() as created
FROM  storeproduct sp 
	inner join stage.today_gnm_sunix_vfst x on  x."FRAMENUM" = sp."description" and x.source = sp.storeid
    inner join stocktake st on st.nk = x.filename 	
    left join stocktake_item sti on sti.stocktake_sk = st.sk and sp.sk = sti.store_product_sk 
 where  "FRAMENUM" <> ''  and sp."description" <> '' and sti.sk is null and sp.sk <>  1844640 and st.nk = 'FST16-01-2018-05-04-46-PM.DBF'
 ) x;
 
 -- 1. import into stocktake-item
 -- 2. update for price
 -- 3. update for last retail - will require several passes
 
 select description,* from storeproduct sp where sk =  1844640
 
select nk, count(*) from stocktake group by nk;

select count(*) from stocktake_item

select * from stocktake_item


select * from store s where sk in (1,36,37)


select

	coalesce( ssm.average_cost, ssm.actual_cost) as avg_cost_incl_gst
	,coalesce( ssm.average_cost_gst, ssm.actual_cost_gst ) as avg_cost_gst
	,*

FROM  stocktake_item st
left join product_gnm_average_cost_history ssm on ssm.storeproduct_sk = st.store_product_sk and st.actual_stocktake_end_date_time >= ssm.adjustmentdate and st.actual_stocktake_end_date_time < ssm.end_date
left join view_last_stock_history_price gnmph on  gnmph.storeproduct_sk = st.store_product_sk and st.actual_stocktake_end_date_time >= gnmph.created and st.actual_stocktake_end_date_time < gnmph.end_date 


--- update average if exists
update  sti set avg_cost_incl_gst = 0
-- select *
from stocktake st 
	inner join stocktake_item sti on sti.stocktake_sk = st.sk
--	inner join product_gnm_average_cost_history pga on sti.store_product_sk = pga.storeproduct_sk  and st.actual_stocktake_end_date_time >= pga.adjustmentdate and st.actual_stocktake_end_date_time < pga.end_date
where 
	sti.avg_cost_incl_gst is null;



select count(*) from product_gnm_average_cost_history
select * from product_gnm_average_cost_history


select product_gnm_sk, count(* )  from product_gnm_average_cost_history group by product_gnm_sk;

select count(*) from 
(select product_gnm_sk, count(* )  from product_gnm_average_cost_history group by product_gnm_sk) x;

select count(*) from 

--------------------------------------------------------------------------------------------------------------------------------------------------

-- add new adjustment type
select * from adjustment_type a 

insert into adjustment_type(adjustment_typeid,name,othername) values ('STOCKTAKE_OUT','Stocktake Out','Stocktake Out');
select * from adjustment_type


set role scire;
SET search_path = public, stage, pg_catalog;

update stocktake set state = null;

----------------------------------------------------------------------------------
--- check for stocktake

select state,s.acquisition_date,st.actual_stocktake_start_date_time, * from stocktake st
inner join store s on s.sk = st.store_sk
order by st.actual_stocktake_start_date_time

---- set stocktakes to be processed
update stocktake set state = null;

update stocktake set state = 'APPLY_STOCKTAKE' 
where store_sk in (1,36,37)
update stocktake set state = 'APPLY_STOCKTAKE' 
where store_sk in (45)


----------------------------------------------------------------------------------
insert into store_stockmovement 
(store_sk,
nk,
storeproduct_sk,
adjustmentdate,
product_gnm_sk,
adjustment_type_sk,
stock_price_history_sk,
adjustment,
reason) 
select s.store_sk, si.sk, si.store_product_sk, 
s.actual_stocktake_end_date_time,
si.product_gnm_sk, 
a.sk, 
coalesce((select sk from  stock_price_history sph where sph.storeproduct_sk = si.store_product_sk 
	-- and sph.created >= s.actual_stocktake_end_date_time and sph.end_date < s.actual_stocktake_end_date_time 
	limit 1),0) as sph_sk, 
	si.expected_qty*-1, 
	a.othername
-- select *
from stocktake_item si 
inner join stocktake s on s.sk = si.stocktake_sk
inner join adjustment_type a on a.name = 'Stocktake Out'
left join store_stockmovement sm on sm.store_sk = s.store_sk and sm.storeproduct_sk = si.store_product_sk and si.product_gnm_sk = sm.product_gnm_sk and  sm.adjustment_type_sk = a.sk and sm.nk = si.sk::text
where s.state = 'APPLY_STOCKTAKE' and si.product_gnm_sk is not null
 and sm.sk is null;

insert into store_stockmovement 
(store_sk,
nk,
storeproduct_sk,
adjustmentdate,
product_gnm_sk,
adjustment_type_sk,
stock_price_history_sk,
adjustment,
reason) 
select s.store_sk, si.sk, si.store_product_sk, s.actual_stocktake_end_date_time,si.product_gnm_sk, a.sk, 
coalesce((select sk from  stock_price_history sph where sph.storeproduct_sk = si.store_product_sk 
	-- and sph.created >= s.actual_stocktake_end_date_time and sph.end_date < s.actual_stocktake_end_date_time 
	limit 1),0) as sph_sk, si.actual_qty, a.othername
-- select *
from stocktake_item si 
inner join stocktake s on s.sk = si.stocktake_sk
inner join adjustment_type a on a.name = 'Stocktake Reset'
left join store_stockmovement sm on sm.store_sk = s.store_sk and sm.storeproduct_sk = si.store_product_sk and si.product_gnm_sk = sm.product_gnm_sk and  sm.adjustment_type_sk = a.sk and sm.nk = si.sk::text
where s.state = 'APPLY_STOCKTAKE' and si.product_gnm_sk is not null
 and sm.sk is null;

insert into store_stockmovement 
(store_sk,
nk,
storeproduct_sk,
adjustmentdate,
product_gnm_sk,
adjustment_type_sk,
stock_price_history_sk,
adjustment,
reason) 
select s.store_sk, si.sk, si.store_product_sk, s.actual_stocktake_end_date_time,si.product_gnm_sk, a.sk, 
coalesce((select sk from  stock_price_history sph where sph.storeproduct_sk = si.store_product_sk 
	-- and sph.created >= s.actual_stocktake_end_date_time and sph.end_date < s.actual_stocktake_end_date_time 
	limit 1),0) as sph_sk, si.actual_qty, a.othername
-- select *
from stocktake_item si 
inner join stocktake s on s.sk = si.stocktake_sk
inner join adjustment_type a on a.adjustment_typeid = 'STOCKTAKE_IN'
left join store_stockmovement sm on sm.store_sk = s.store_sk and sm.storeproduct_sk = si.store_product_sk and si.product_gnm_sk = sm.product_gnm_sk and  sm.adjustment_type_sk = a.sk and sm.nk = si.sk::text
where s.state = 'APPLY_STOCKTAKE' and si.product_gnm_sk is not null
 and sm.sk is null;

--------------------------------------------------------
--- import store 0000000 stocktake

set role scire;
SET search_path = public, stage, pg_catalog;

select * from store;


select source,count(*) from today_gnm_sunix_vfst group by "source"

select "parameters"->>'source',count(*) from gnm_sunix_vfst group by "parameters"->>'source'


select count(*) from today_gnm_sunix_vfst where source = '0000-000'


select createddate,"parameters"->>'source'  from gnm_sunix_vfst order by createddate desc

SELECT jsonb_array_elements("data") as data,parameters,T.runid,T.createddate,
T."parameters"->>'source' 
FROM stage.gnm_sunix_vfst T where T.runid in (
select runid from (
select parameters->>'source',parameters->>'filename',runid,createddate,row_number() over (partition by parameters->>'source',parameters->>'filename' order by createddate desc) 
as r from stage.gnm_sunix_vfst where createddate >= (now()::date -7)) S where r = 1)
and T."parameters"->>'source' = '0000-000'
;



select parameters->>'source',parameters->>'filename',runid,createddate,row_number() over (partition by parameters->>'source',parameters->>'filename' order by createddate desc)
from stage.gnm_sunix_vfst 
where createddate >= (now()::date -7)

--------------------------


INSERT INTO public.stocktake
( nk,
store_sk, 
planned_stocktake_start_date_time, 
planned_stocktake_end_date_time, 
actual_stocktake_start_date_time, 
actual_stocktake_end_date_time, 
created_user, 
created_date_time, 
modified_user, 
modified_date_time, 
created)
SELECT st.filename as nk,s.sk as store_sk,
            maxdate as planned_stocktake_start_date_time,maxdate as planned_stocktake_end_date_time,
            maxdate as actual_stocktake_start_date_time,maxdate as actual_stocktake_end_date_time,
            'system' as created_user, now() as created_date_time, 'system' as modified_user, 
            now() as modified_date_time, now() as created
FROM stage.today_gnm_sunix_vfst st
inner join (select max(to_timestamp(substring(replace(filename,'00','01'),4,10) || ' ' || overlay( replace(substring(replace(filename,'00','01'),15,11),'-',':')  placing ' ' from 9 for 1 ) ,
																																										'DD-MM-YYYY HH12:MI:SS AM')) maxdate
				, source
 from stage.today_gnm_sunix_vfst group by source) x on 
 	x.maxdate = to_timestamp(substring(replace(filename,'00','01'),4,10) || ' ' || overlay( replace(substring(replace(filename,'00','01'),15,11),'-',':')  placing ' ' from 9 for 1 ) , 'DD-MM-YYYY HH12:MI:SS AM') 
 		and x.source = st."source"
 left join store s on st."source" = s.storeid
 left join stocktake stk on stk.nk = st.filename
where 
	stk.sk  is  null and s.sk is not null
group by s.sk, maxdate, st.filename;

-----------------------------------------------------------------------------------
--- sanity check 0000-000 stock take import
select count(*) from (
select * from stocktake st 
inner join stocktake_item sti on st.sk = sti.stocktake_sk
where st.store_sk = 45
) x

select * from stocktake_item sti 
inner join storeproduct sp on sp.sk = sti.store_product_sk
where sp.product_gnm_sk is null

-----------------------------------------------------------------------------------
1. stocktake batch
2. last known cost 
3. process stock take out, stock take information_schema

select * from store s

select reason,count(*) from store_stockmovement sm where store_sk = 45 group by reason
select * from store_stockmovement sm where store_sk = 45

--------------------------------------------------------------------------------------------

select sp.sk,sp.product_gnm_sk,* from stocktake_item si 
inner join stocktake st on st.sk = si.stocktake_sk
left join storeproduct sp on sp.sk = si.store_product_sk
where 
	st.store_sk = 45

select * from stock_price_history sph 
where sph.product_gnm_sk = 0 and sph.storeproduct_sk in 
(select sp.sk from stocktake_item si 
inner join stocktake st on st.sk = si.stocktake_sk
left join storeproduct sp on sp.sk = si.store_product_sk
where 
	st.store_sk = 45);
	


--------------------------------------------------------------------------------------------


select * from stock_price_history sph where product_gnm_sk = 0;

select * from stock_price_history sph where product_gnm_sk is null;


update stock_price_history set product_gnm_sk = 0 where  product_gnm_sk is null;

--------------------------------------------------------------------------------------------

-- cost GST

REFRESH MATERIALIZED view store_stockmovement_costs;

select count(*) from store_stockmovement_costs where store_sk = 45;

select sph.*,sm.* 
from  store_stockmovement sm
left join stock_price_history sph on sph.storeproduct_sk = sm.storeproduct_sk and sm.adjustmentdate >= sph.created and sm.adjustmentdate < sph.end_date
left join product_gnm_average_cost_history pga on pga.storeproduct_sk = sm.storeproduct_sk and sm.adjustmentdate >= pga.adjustmentdate and sm.adjustmentdate < pga.end_date
where sm.storeproduct_sk <> 0 and sm.store_sk = 45;





update sm set adjustment
--------------------------------------------------------------------------------------------


            select s.store_sk, ('SYS-' || si.sk) as nk, si.store_product_sk as storeproduct_sk, 
            s.actual_stocktake_end_date_time as adjustmentdate,
            d.sk as adjustmentdate_sk,
            si.product_gnm_sk, a.sk as adjustment_type_sk, 
            coalesce((select sk from  stock_price_history sph where sph.storeproduct_sk = si.store_product_sk 
                -- and sph.created >= s.actual_stocktake_end_date_time and sph.end_date < s.actual_stocktake_end_date_time 
                limit 1),0) as stock_price_history_sk, si.actual_qty as adjustment, a.othername as reason
            -- select *
            from stocktake_item si 
            inner join stocktake s on s.sk = si.stocktake_sk
            inner join adjustment_type a on a.name = 'Stocktake Reset'
            left outer join d_date d on (TO_CHAR((s.actual_stocktake_end_date_time::timestamp),'yyyymmdd')::INT = d.date_dim_id)
            where s.state = 'APPLY_STOCKTAKE' ;

select s.store_sk, si.sk, si.store_product_sk, 
s.actual_stocktake_end_date_time,
si.product_gnm_sk, 
a.sk, 
coalesce((select sk from  stock_price_history sph where sph.storeproduct_sk = si.store_product_sk 
	-- and sph.created >= s.actual_stocktake_end_date_time and sph.end_date < s.actual_stocktake_end_date_time 
	limit 1),0) as sph_sk, 
	si.expected_qty*-1, 
	a.othername
-- select *
from stocktake_item si 
inner join stocktake s on s.sk = si.stocktake_sk
inner join adjustment_type a on a.name = 'Stocktake Out'
left join store_stockmovement sm on sm.store_sk = s.store_sk and sm.storeproduct_sk = si.store_product_sk and si.product_gnm_sk = sm.product_gnm_sk and  sm.adjustment_type_sk = a.sk and sm.nk = si.sk::text
where s.state = 'APPLY_STOCKTAKE' and si.product_gnm_sk is not null
 and sm.sk is null;
            
            
            
            select s.store_sk, si.sk as nk, si.store_product_sk as storeproduct_sk, 
            s.actual_stocktake_end_date_time as adjustmentdate,
            d.sk as adjustmentdate_sk,
            si.product_gnm_sk, a.sk as adjustment_type_sk, 
            coalesce((select sk from  stock_price_history sph where sph.storeproduct_sk = si.store_product_sk 
                -- and sph.created >= s.actual_stocktake_end_date_time and sph.end_date < s.actual_stocktake_end_date_time 
                limit 1),0) as stock_price_history_sk, si.actual_qty as adjustment, a.othername as reason
            -- select *
            from stocktake_item si 
            inner join stocktake s on s.sk = si.stocktake_sk
            inner join adjustment_type a on a.adjustment_typeid = 'STOCKTAKE_IN'
            left outer join d_date d on (TO_CHAR((s.actual_stocktake_end_date_time::timestamp),'yyyymmdd')::INT = d.date_dim_id)
            where s.state = 'APPLY_STOCKTAKE'
            
            
--------------------------------------------------------------------------------------------


insert into store_stockmovement 
(store_sk,
nk,
storeproduct_sk,
adjustmentdate,
adjustmentdate_sk,
product_gnm_sk,
adjustment_type_sk,
stock_price_history_sk,
adjustment,
reason,
stocktake_sk,
stocktake_item_sk) 
select s.store_sk, 
('SYS-' || si.sk) as nk, 
si.store_product_sk, 
s.actual_stocktake_end_date_time,
d.sk as adjustmentdate_sk,
si.product_gnm_sk, 
a.sk, 
coalesce((select sk from  stock_price_history sph where sph.storeproduct_sk = si.store_product_sk 
	-- and sph.created >= s.actual_stocktake_end_date_time and sph.end_date < s.actual_stocktake_end_date_time 
	limit 1),0) as sph_sk, 
si.expected_qty*-1 as adjustment, 
a.othername,
si.stocktake_sk,
si.sk as stocktake_item_sk
-- select  sm.sk, sm.adjustment_type_sk, a.sk,  *
from stocktake_item si 
inner join stocktake s on s.sk = si.stocktake_sk
inner join adjustment_type a on a.name = 'Stocktake Out'
left join store_stockmovement sm on sm.adjustment_type_sk = a.sk and sm.stocktake_item_sk = si.sk 
left join d_date d on (TO_CHAR((s.actual_stocktake_end_date_time::timestamp),'yyyymmdd')::INT = d.date_dim_id)
where s.state = 'APPLY_STOCKTAKE' and si.product_gnm_sk is not null
 and sm.sk is null;

insert into store_stockmovement 
(store_sk,
nk,
storeproduct_sk,
adjustmentdate,
adjustmentdate_sk,
product_gnm_sk,
adjustment_type_sk,
stock_price_history_sk,
adjustment,
reason,
stocktake_sk,
stocktake_item_sk) 
select s.store_sk, ('SYS-' || si.sk) as nk, si.store_product_sk, s.actual_stocktake_end_date_time,
d.sk as adjustmentdate_sk,
si.product_gnm_sk, 
a.sk, 
coalesce((select sk from  stock_price_history sph where sph.storeproduct_sk = si.store_product_sk 
	-- and sph.created >= s.actual_stocktake_end_date_time and sph.end_date < s.actual_stocktake_end_date_time 
	limit 1),0) as sph_sk, 
si.actual_qty, 
a.othername,
si.stocktake_sk,
si.sk as stocktake_item_sk
-- select *
from stocktake_item si 
inner join stocktake s on s.sk = si.stocktake_sk
inner join adjustment_type a on a.name = 'Stocktake Reset'
left join store_stockmovement sm on sm.adjustment_type_sk = a.sk and sm.stocktake_item_sk = si.sk 
left outer join d_date d on (TO_CHAR((s.actual_stocktake_end_date_time::timestamp),'yyyymmdd')::INT = d.date_dim_id)
where s.state = 'APPLY_STOCKTAKE' and si.product_gnm_sk is not null
 and sm.sk is null;

insert into store_stockmovement 
(store_sk,
nk,
storeproduct_sk,
adjustmentdate,
adjustmentdate_sk,
product_gnm_sk,
adjustment_type_sk,
stock_price_history_sk,
adjustment,
reason,
stocktake_sk,
stocktake_item_sk) 
select s.store_sk,  
('SYS-' || si.sk) as nk, 
si.store_product_sk, 
s.actual_stocktake_end_date_time,
d.sk as adjustmentdate_sk,
si.product_gnm_sk, 
a.sk, 
coalesce((select sk from  stock_price_history sph where sph.storeproduct_sk = si.store_product_sk 
	-- and sph.created >= s.actual_stocktake_end_date_time and sph.end_date < s.actual_stocktake_end_date_time 
	limit 1),0) as sph_sk, 
	si.actual_qty, 
	a.othername,
si.stocktake_sk,
si.sk as stocktake_item_sk
-- select *
from stocktake_item si 
inner join stocktake s on s.sk = si.stocktake_sk
inner join adjustment_type a on a.adjustment_typeid = 'STOCKTAKE_IN'
left join store_stockmovement sm on sm.adjustment_type_sk = a.sk and sm.stocktake_item_sk = si.sk 
left outer join d_date d on (TO_CHAR((s.actual_stocktake_end_date_time::timestamp),'yyyymmdd')::INT = d.date_dim_id)
where s.state = 'APPLY_STOCKTAKE' and si.product_gnm_sk is not null
 and sm.sk is null;



----------------------------------------------------------
--- delete all the stockmovements -- rollback;
begin;
delete from store_stockmovement 
-- select * from store_stockmovement 
where reason in ('Stocktake Receipt','Reset','Stocktake Out')


commit;

-----------------------------------------------------------

--- check that storeproducts look ok
select * from storeproduct sp where sp.sk in (
select si.store_product_sk from stocktake s
inner join stocktake_item si on si.stocktake_sk = s.sk
where
	s.store_sk = 45)
;

--- check that stock price history is setup
select * from stock_price_history sp where sp.storeproduct_sk in (
select si.store_product_sk from stocktake s
inner join stocktake_item si on si.stocktake_sk = s.sk
where
	s.store_sk = 45)
;

--- check how man gnm products in stocktake
select * from stock_price_history sp where sp.product_gnm_sk in (
select si.product_gnm_sk from stocktake s
inner join stocktake_item si on si.stocktake_sk = s.sk
where
	s.store_sk = 45 and si.product_gnm_sk <> 0);

--- now impossible check for averages
select * from product_gnm_average_cost_history sp where sp.storeproduct_sk in (
select si.store_product_sk from stocktake s
inner join stocktake_item si on si.stocktake_sk = s.sk
where
	s.store_sk = 45 and si.product_gnm_sk <> 0);

-----------------------------------------------------------------------------------------------------------------
--- unravel the costing

set role scire;
SET search_path = public, pg_catalog;
	

-- drop materialized view store_stockmovement_costs;


create materialized view store_stockmovement_costs as
select 
	sm.sk as store_stockmovement_sk, 
	pga.sk as product_gnm_average_cost_history_sk,
	pga.adjustmentdate as avg_adjustmentdate,
	pga.end_date as avg_end_date,
	pga.quantity as avg_quantity,
	pga.adjustment as avg_adjustment,
	pga.running_total as avg_running_total,
	pga.actual_cost as avg_actual_cost,
	pga.actual_cost_gst as avg_actual_cost_gst,
	pga.average_cost,
	pga.average_cost_gst,
	sph.sk as stock_price_history_sk,
	coalesce(sph.store_sk, pga.store_sk,  0) as store_sk,
	coalesce(sph.storeproduct_sk, pga.storeproduct_sk, 0) as storeproduct_sk,
	coalesce(sph.product_gnm_sk, pga.product_gnm_sk, 0) as product_gnm_sk,
	supplier_cost, 
	supplier_cost_gst, 
	sph.actual_cost as history_actual_cost, 
	sph.actual_cost_gst as history_actual_cost_gst, 
	supplier_rrp, 
	supplier_rrp_gst, 
	calculated_retail_price, 
	calculated_retail_price_gst, 
	override_retail_price, 
	override_retail_price_gst, 
	retail_price, 
	retail_price_gst, 
	sph.created as stock_price_history_created, 
	sph.end_date as stock_price_history_end_date
from  store_stockmovement sm
left join stock_price_history sph on sph.storeproduct_sk = sm.storeproduct_sk -- and sm.adjustmentdate >= sph.created and sm.adjustmentdate < sph.end_date
left join product_gnm_average_cost_history pga on pga.storeproduct_sk = sm.storeproduct_sk and sm.adjustmentdate >= pga.adjustmentdate and sm.adjustmentdate < pga.end_date
where sm.storeproduct_sk <> 0 and sm.store_sk = 45 and sph.sk is not null;

create index on store_stockmovement_costs (store_stockmovement_sk);



REFRESH MATERIALIZED view store_stockmovement_costs;
-- cost GST

REFRESH MATERIALIZED view store_stockmovement_costs;

select count(*) from store_stockmovement_costs where store_sk = 45;

select stocktake_sk,store_product_sk,count(*) from (
select 
	si.stocktake_sk,
	si.sk as stocktake_item_sk, 
	si.store_product_sk,
	si.product_gnm_sk,
	pga.sk as product_gnm_average_cost_history_sk,
	pga.adjustmentdate as avg_adjustmentdate,
	pga.end_date as avg_end_date,
	pga.quantity as avg_quantity,
	pga.adjustment as avg_adjustment,
	pga.running_total as avg_running_total,
	pga.actual_cost as avg_actual_cost,
	pga.actual_cost_gst as avg_actual_cost_gst,
	pga.average_cost,
	pga.average_cost_gst,
	sph.sk as stock_price_history_sk,
	coalesce(sph.store_sk, pga.store_sk,  0) as store_sk,
	coalesce(sph.storeproduct_sk, pga.storeproduct_sk, 0) as storeproduct_sk,
	coalesce(sph.product_gnm_sk, pga.product_gnm_sk, 0) as product_gnm_sk,
	supplier_cost, 
	supplier_cost_gst, 
	sph.actual_cost as history_actual_cost, 
	sph.actual_cost_gst as history_actual_cost_gst, 
	supplier_rrp, 
	supplier_rrp_gst, 
	calculated_retail_price, 
	calculated_retail_price_gst, 
	override_retail_price, 
	override_retail_price_gst, 
	retail_price, 
	retail_price_gst, 
	sph.created as stock_price_history_created, 
	sph.end_date as stock_price_history_end_date
from	stocktake s inner join  
	stocktake_item si on si.stocktake_sk = s.sk
left join stock_price_history sph on sph.storeproduct_sk = si.store_product_sk  and s.actual_stocktake_end_date_time >= sph.created and s.actual_stocktake_end_date_time < sph.end_date
left join product_gnm_average_cost_history pga on pga.storeproduct_sk = si.store_product_sk and s.actual_stocktake_end_date_time  >= pga.adjustmentdate and s.actual_stocktake_end_date_time < pga.end_date
where si.store_product_sk  <> 0 and s.store_sk = 45 and sph.sk is not null
order by store_product_sk
) x 
group by stocktake_sk,store_product_sk
;


--------------- duplicates ???!
select stocktake_sk,store_product_sk,count(*)
from	stocktake s inner join  
	stocktake_item si on si.stocktake_sk = s.sk
	group by stocktake_sk,store_product_sk
order by store_product_sk


delete
-- select *
from stocktake_item si 
using 
(select si.sk stocktake_item_sk,stocktake_sk,store_product_sk, row_number()  over ( partition by stocktake_sk,store_product_sk,si.created_date_time ) r,*
from	stocktake s inner join  
	stocktake_item si on si.stocktake_sk = s.sk ) x
	where x.r = 2 and x.stocktake_item_sk = si.sk
	
	
---------------------------------------------------------------
-- set average cost for stocktake
-- rollback;
	
begin;
	update stocktake_item si set avg_cost_incl_gst = x.calc_cost, avg_cost_gst = x.calc_cost_gst
	-- select x.* 
	from 
	(select si.sk
    ,si.avg_cost_incl_gst,coalesce( pga.average_cost, sph.actual_cost,0 ) as calc_cost
    ,coalesce( pga.average_cost_gst, sph.actual_cost_gst,0 ) as calc_cost_gst
    
	from stocktake s inner join stocktake_item si on si.stocktake_sk = s.sk
	left join stock_price_history sph on sph.storeproduct_sk = si.store_product_sk  and s.actual_stocktake_end_date_time >= sph.created and s.actual_stocktake_end_date_time < sph.end_date
	left join product_gnm_average_cost_history pga on pga.storeproduct_sk = si.store_product_sk and s.actual_stocktake_end_date_time  >= pga.adjustmentdate and s.actual_stocktake_end_date_time < pga.end_date 
	where  si.store_product_sk  <> 0 ) x where  x.sk = si.sk;

commit;

select count(*) from stocktake_item


select * from stocktake_item si where avg_cost_gst is null

---------------------------------------------------------------
-- set average cost for stockmovement

-- dont need to just get from stockmovement


----- setting last purchased date for takeon stock
begin ;
update stocktake_item  set last_purchased_date = sm.adjustmentdate
-- select sm.sk, sm.adjustmentdate
from 
(select sti.sk stocktake_item_sk,sm.storeproduct_sk,count(*), max(sm.sk) max_stockmovement
from stocktake_item sti
 left join  vstore_stockmovement sm on sti.store_product_sk = sm.storeproduct_sk -- and sti.created_date_time <= sm.adjustmentdate
where sm.name = 'Receipt' -- and sti.created_date_time <= sm.adjustmentdate
group by sti.sk,sm.storeproduct_sk) x
inner join store_stockmovement sm on sm.sk = x.max_stockmovement
where stocktake_item.sk = x.stocktake_item_sk

select * from stocktake_item where last_purchased_date is not null;
commit;

----- setting last purchased date for orders


select * from store_stockmovement_costs where store_sk = 45;


---------- resolving optomate view issue

select * from stage.gnm_optomate_v_stocktake_log;
select * from stage.today_gnm_optomate_v_stocktake_log;

drop view stage.today_gnm_optomate_v_stocktake_log;

select stage.generate_view_for_table('gnm_optomate_v_stocktake_log');

select * from 

select * from stocktake s inner join stocktake_item si on si.stocktake_sk = s.sk where s.store_sk = 45;

----------- optomate stocktake-----------------------------------------------------------------------------------------
set role scire;
SET search_path = public, stage, pg_catalog;

INSERT INTO public.stocktake
( nk,
store_sk, 
planned_stocktake_start_date_time, 
planned_stocktake_end_date_time, 
actual_stocktake_start_date_time, 
actual_stocktake_end_date_time, 
created_user, 
created_date_time, 
modified_user, 
modified_date_time, 
created)
select os."STOCKTAKE_ID",s.sk,os."START_DATE_TIME"::timestamp ,os."FINISH_DATE_TIME"::timestamp,os."START_DATE_TIME"::timestamp,os."FINISH_DATE_TIME"::timestamp,os."USER_ADDED1",now(),os."USER_ADDED1", now(), now()
-- select *
FROM stage.today_gnm_optomate_v_stocktake_log os
 inner join store s on  s.store_nk = '0000-001' -- || '-'  || os."BRANCH_IDENTIFIER"
 left join stocktake stk on stk.nk = os."STOCKTAKE_ID"
where 
	stk.sk is null and source = '0000-000'
group by os."STOCKTAKE_ID",s.sk,os."START_DATE_TIME" ,os."FINISH_DATE_TIME",os."START_DATE_TIME",os."FINISH_DATE_TIME",os."USER_ADDED1"

select * from store s

----------------------------------------------------------------------------------


INSERT INTO public.stocktake_item
(
	nk,
	stocktake_sk, 
	product_gnm_sk, 
	store_product_sk, 
	expected_qty, 
	actual_qty, 
	diff,
	avg_cost_incl_gst, 
	avg_cost_gst,
	pms_user,
	last_purchased_date, 
	created_user, 
	created_date_time, 
	modified_user, 
	modified_date_time, 
	created
)
select 
	os."STOCKTAKE_ID" as nk,
	stk.sk as stocktake_sk,
	sp.product_gnm_sk,
	sp.sk as store_product_sk,
	os."PRE_COUNT_QOH"::integer as expected_qty,
	os."POST_COUNT_QOH"::integer as actual_qty,
	os."PRE_COUNT_QOH"::integer -  os."POST_COUNT_QOH"::integer as diff,
	null as avg_cost_incl_gst,
	null as avg_cost_gst,
	os."USER_ADDED" as pms_user,
	os."LAST_PURCHASED"::timestamp as last_purchased_date,
	os."USER_ADDED" as created_user,
	os."DATE_ADDED"::timestamp as created_date_time,
	os."USER_ADDED1"as modified_user,
	os."DATE_EDITED1"::timestamp as created_date_time,
	now()
	-- select  os."GM_PRODUCT_ID" ,sp.internal_sku ,s.storeid,sp.storeid ,* 
FROM stage.today_gnm_optomate_v_stocktake_log os
 inner join store s on  s.store_nk = '0000-001' -- || '-'  || os."BRANCH_IDENTIFIER"
inner join storeproduct sp on  
	os."GM_PRODUCT_ID" = sp.internal_sku and s.storeid = sp.storeid and internal_sku is not null and os."GM_PRODUCT_ID" is not null and internal_sku <> '' and os."GM_PRODUCT_ID" <> ''
 inner join stocktake stk on stk.nk = os."STOCKTAKE_ID"
where 
	-- stk.sk is null and 
	source = '0000-000' 

select * from storeproduct;
 select * FROM stage.today_gnm_optomate_v_stocktake_log os where "GM_PRODUCT_ID" is not null and "GM_PRODUCT_ID" <> '';

select * from storeproduct where storeid = '0000-001'

GM_PRODUCT_ID will match the internal_sku

select * from "store"

create index on stocktake (nk);

create index on storeproduct (storeid,internal_sku);

select * from stocktake

select * from stocktake_item si where stocktake_sk = 30565


delete from stocktake_item where stocktake_sk in (30561,30562,30563,30564)

delete from stocktake where sk in (30561,30562,30563,30564);

--------------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO public.stocktake
( nk,
store_sk, 
planned_stocktake_start_date_time, 
planned_stocktake_end_date_time, 
actual_stocktake_start_date_time, 
actual_stocktake_end_date_time, 
created_user, 
created_date_time, 
modified_user, 
modified_date_time, 
created)
select '0000-001' || '-' || os."STOCKTAKE_ID",s.sk,os."START_DATE_TIME"::timestamp ,os."FINISH_DATE_TIME"::timestamp,os."START_DATE_TIME"::timestamp,os."FINISH_DATE_TIME"::timestamp,os."USER_ADDED1",now(),os."USER_ADDED1", now(), now()
-- select *
FROM stage.today_gnm_optomate_v_stocktake_log os
 inner join store s on  s.store_nk = '0000-001'-- || '-'  || os."BRANCH_IDENTIFIER"
 left join stocktake stk on stk.nk ='0000-001' || '-' || os."STOCKTAKE_ID"
where 
	stk.sk is null
group by '0000-001' || '-' || os."STOCKTAKE_ID",s.sk,os."START_DATE_TIME" ,os."FINISH_DATE_TIME",os."START_DATE_TIME",os."FINISH_DATE_TIME",os."USER_ADDED1"



INSERT INTO public.stocktake_item
(
	nk,
	stocktake_sk, 
	product_gnm_sk, 
	store_product_sk, 
	expected_qty, 
	actual_qty, 
	diff,
	avg_cost_incl_gst, 
	avg_cost_gst,
	pms_user,
	last_purchased_date, 
	created_user, 
	created_date_time, 
	modified_user, 
	modified_date_time, 
	created
)
select 
	 '0000-001'|| '-' || os."ID" as nk,
	stk.sk as stocktake_sk,
	sp.product_gnm_sk,
	sp.sk as store_product_sk,
	os."PRE_COUNT_QOH"::integer as expected_qty,
	os."POST_COUNT_QOH"::integer as actual_qty,
	os."PRE_COUNT_QOH"::integer -  os."POST_COUNT_QOH"::integer as diff,
	null as avg_cost_incl_gst,
	null as avg_cost_gst,
	os."USER_ADDED" as pms_user,
	os."LAST_PURCHASED"::timestamp as last_purchased_date,
	os."USER_ADDED" as created_user,
	os."DATE_ADDED"::timestamp as created_date_time,
	os."USER_ADDED1"as modified_user,
	os."DATE_EDITED1"::timestamp as created_date_time,
	now()
	-- select  os."GM_PRODUCT_ID" ,sp.internal_sku ,s.storeid,sp.storeid ,* 
FROM stage.today_gnm_optomate_v_stocktake_log os
 inner join store s on  s.store_nk = os."source"
 inner join storeproduct sp on  
	os."GM_PRODUCT_ID" = sp.internal_sku and s.storeid = sp.storeid and internal_sku is not null and os."GM_PRODUCT_ID" is not null and internal_sku <> '' and os."GM_PRODUCT_ID" <> ''
 inner join stocktake stk on stk.nk = '0000-001'|| '-' || os."STOCKTAKE_ID"
 left join stocktake_item si on si.nk = '0000-001'|| '-' || os."ID" 
 where si.sk is null;


----------------------------------------------------------------------------------
--- insert stock items for NON GNM Products

INSERT INTO public.stocktake_item
(
	nk,
	stocktake_sk, 
	product_gnm_sk, 
	store_product_sk, 
	expected_qty, 
	actual_qty, 
	diff,
	avg_cost_incl_gst, 
	avg_cost_gst,
	pms_user,
	last_purchased_date, 
	created_user, 
	created_date_time, 
	modified_user, 
	modified_date_time, 
	created
)
select 
	 '0000-001'|| '-' || os."ID" as nk,
	stk.sk as stocktake_sk,
	sp.product_gnm_sk,
	sp.sk as store_product_sk,
	os."PRE_COUNT_QOH"::integer as expected_qty,
	os."POST_COUNT_QOH"::integer as actual_qty,
	os."PRE_COUNT_QOH"::integer -  os."POST_COUNT_QOH"::integer as diff,
	null as avg_cost_incl_gst,
	null as avg_cost_gst,
	os."USER_ADDED" as pms_user,
	os."LAST_PURCHASED"::timestamp as last_purchased_date,
	os."USER_ADDED" as created_user,
	os."DATE_ADDED"::timestamp as created_date_time,
	os."USER_ADDED1"as modified_user,
	os."DATE_EDITED1"::timestamp as created_date_time,
	now()
	-- select  os."GM_PRODUCT_ID" ,* 
FROM stage.today_gnm_optomate_v_stocktake_log os
 inner join store s on  s.store_nk = os."source"
 inner join stocktake stk on stk.nk = '0000-001'|| '-' || os."STOCKTAKE_ID"
 inner join storeproduct sp on  
	sp.productid = '0000-001' || '-PF-' || os."BARCODE1" and os."BARCODE1" is not null and os."BARCODE1"  <> '' 
 left join stocktake_item si on si.nk = '0000-001'|| '-' || os."ID" 
where  si.sk is null;



select * from stock_price_history sph where storeproduct_sk in
(select sk from storeproduct where storeid = '0000-001' and  product_gnm_sk <> 0)

----------------------------------------------------------------------------------------------------------------------

select * from 




--- reimport and process 

select * from store_stockmovement sm where sm.store_sk = 45

select
            0 as store_sk,
            0 as storeproduct_sk,
            coalesce(p.sk,0) as product_gnm_sk,
            coalesce(supplier_cost,0) as supplier_cost,
            coalesce(supplier_cost_gst,0) as supplier_cost_gst,
            coalesce(actual_cost,0) as actual_cost,
            coalesce(actual_cost_gst,0) as actual_cost_gst,
            coalesce(supplier_rrp,0) as supplier_rrp,
            coalesce(supplier_rrp_gst,0) as supplier_rrp_gst,
            coalesce(retail_price,0) as retail_price,
            coalesce(retail_price_gst,0) as retail_price_gst,
            ('GNM-' || id) as nk,
            effective_from_time as created
from public.product_price_history h
left outer join public.storeproduct p on (p.productid = h.product_id AND p.storeid = '0')


select count(*) from (
select sk from storeproduct sp 
	left join product_price_history pph on pph.product_id = sp.productid
where pph.id is null
) x;

select count(*) from (
select sp.sk from storeproduct sp 
	left join stock_price_history sph on sph.storeproduct_sk = sp.sk
where sph.sk is null
) x;


select * from storeproduct sp ;
select count(*) from product_price_history pph ;
select count(*) from stock_price_history pph ;
select * from storeproduct p;

           select vah.storeproduct_sk,
                vah.product_gnm_sk,
                vah.store_sk,
                vah.adjustmentdate,
                vah.sk as store_stockmovement_sk,
                vah.quantity,vah.running_total,
                vah.prior_avg_cost,vah.adjustment,
                vah.actual_cost,vah.actual_cost_gst,
                vah.average_cost,vah.average_cost_gst,
                '2030-01-01'::timestamp as end_date
            from view_product_gnm_average_cost_history vah 
            

  
            
select  row_number() over (partition by storeproduct_sk order by adjustmentdate) r,sk,store_sk,
storeproduct_sk,product_gnm_sk,adjustmentdate,adjustment_type_sk,"name",reason,adjustment
from vstore_stockmovement sm where
sm.storeproduct_sk in (
select storeproduct_sk from store_stockmovement sm
where adjustmentdate > '2017-11-01'
group by store_sk,storeproduct_sk
 having count(*) > 10
) and adjustment_type_sk not in (320,318,321)
order by store_sk, storeproduct_sk, r, adjustmentdate


select * from adjustment_type

select * from storeproduct where sk in (1845702);


select  row_number() over (partition by storeproduct_sk order by adjustmentdate) r,sk,store_sk,
storeproduct_sk,product_gnm_sk,adjustmentdate,adjustment_type_sk,"name",reason,adjustment,adjustment_typeid,comments
from vstore_stockmovement sm where
sm.storeproduct_sk in (
1845702
) and adjustment_type_sk not in (320,318,321)
order by store_sk, storeproduct_sk, r, adjustmentdate

select * from daily_adjustments_sum where storeproduct_sk = 1845702 order by date_actual

select * from store_stock_history_adj where storeproduct_sk = 1845702 order by created

select * from product_gnm_average_cost_history where storeproduct_sk = 1845702 order by created
select * from product_gnm_average_cost_history where storeproduct_sk = 1845702 order by created


----- make sure daily adjustments sum orders by date & time & sk, make sure timezone boundary is respected

 select sm.store_sk, 
            sm.storeproduct_sk, 
            coalesce(sm.product_gnm_sk,0)  as product_gnm_sk,
            date_trunc('day',sm.adjustmentdate), 
            sum(adjustment) dailytotal, 
            row_number() over (partition by sm.store_sk,sm.storeproduct_sk order by date_trunc('day',sm.adjustmentdate) ) as seq
from store_stockmovement sm
            inner join d_date dd on dd.sk = sm.adjustmentdate_sk
            where sm.store_sk is not null and storeproduct_sk = 1845702
            group by sm.store_sk, sm.storeproduct_sk, coalesce(sm.product_gnm_sk,0), date_trunc('day',sm.adjustmentdate);
 

select dd.date_actual,date_trunc('day',sm.adjustmentdate)   from store_stockmovement sm 
inner join store_stockmovement sm2 on sm.sk = sm2.sk
inner join d_date dd on dd.sk = sm2.adjustmentdate_sk
where
	date_trunc('day',sm.adjustmentdate) <> dd.date_actual

	
	select r."DATE", d.date_actual,coalesce("MODIFIED","CREATED","DATE")::timestamp as adjustmentdate
	-- from store_stockmovement sm 
	from
          stage.today_gnm_sunix_vrestock r
	          left outer join d_date d on (TO_CHAR((coalesce("MODIFIED","CREATED","DATE")::timestamp),'yyyymmdd')::INT = d.date_dim_id);



----------------------------------------------------------------------
--- correct stockmovements data
select d.sk, sm.adjustmentdate_sk, sm.adjustmentdate,d2.date_actual, d.*,sm.* 
from store_stockmovement sm 
inner join d_date d on  (TO_CHAR(adjustmentdate,'yyyymmdd')::INT = d.date_dim_id)
inner join d_date d2 on d2.sk = sm.adjustmentdate_sk
where 
 d.sk <> sm.adjustmentdate_sk;

update store_stockmovement sm set adjustmentdate_sk = d.sk
from  d_date d 
where 
(TO_CHAR(adjustmentdate,'yyyymmdd')::INT = d.date_dim_id) and d.sk <> sm.adjustmentdate_sk
 


          select "ID","NOTE","QTY_NEW","QTY_OLD","CODE",coalesce("MODIFIED","CREATED","DATE")::timestamp as adjustmentdate,"REASON","LOCATION",source,coalesce(s.sk,0) as store_sk,
          coalesce(tp.sk,0) as storeproduct_sk,
          coalesce(gp.sk,0) as product_gnm_sk,
          coalesce(j.invoicesk,0) as invoice_sk,
          coalesce(c.sk,0) as customer_sk,
          coalesce(
            (select sk from public.stock_price_history h
              where (h.product_gnm_sk = gp.sk AND gp.sk is not null) AND h.created <= (coalesce("MODIFIED","CREATED","DATE")::timestamp)
              order by created desc limit 1)
          ,
            (select sk from public.stock_price_history h
              where (h.storeproduct_sk = tp.sk) AND h.created <= (coalesce("MODIFIED","CREATED","DATE")::timestamp)
              order by created desc limit 1)
          ,0) as stock_price_history_sk,
          coalesce(d.sk,0) as adjustmentdate_sk,
          coalesce(a.sk,0) as adjustment_type_sk
          from
          stage.today_gnm_sunix_vrestock r
          left outer join public.store s on (s.store_nk = source)
          left outer join public.storeproduct tp on (tp.productid = (s.storeid || '-PF-' || "CODE"))
          left outer join public.product_gnm gp on (tp.internal_sku <> '' AND gp.productid = tp.internal_sku)
          left outer join public.spectaclejob j on (("REASON" = 'Sale' OR "REASON" = 'Cancel Sale') AND j.spectaclejobid = (s.storeid || '-' || substring("INVNO",5)))
          left outer join public.customer c on (c.customerid = (s.storeid || '-' || "REFNUM"))
          left outer join d_date d on (TO_CHAR((coalesce("MODIFIED","CREATED","DATE")::timestamp),'yyyymmdd')::INT = d.date_dim_id)
          left outer join adjustment_type a on (upper(r."REASON") = a.adjustment_typeid)
          where (coalesce("MODIFIED","CREATED","DATE")::timestamp) >= '2017-07-01' AND (coalesce("MODIFIED","CREATED","DATE")::timestamp) >= s.acquisition_date AND
          (coalesce("MODIFIED","CREATED","DATE")::timestamp) <= now() AND "STOCKTYPE" = 'F'

          
          
          
          select * from stocktake_item sti where sti.avg_cost_gst is null
          
          
          00011496
          
          
          
          select sph.* from 
          product_gnm pg left join 
          stock_price_history sph on sph.product_gnm_sk = pg.sk
          where pg.productid = '00011496'
   
          
          select * from 
          product_gnm pg
          -- left join stock_price_history sph on sph.product_gnm_sk = pg.sk
          left join storeproduct sp on sp.product_gnm_sk = pg.sk
          left join stock_price_history sph on sph.storeproduct_sk = sp.sk
          where pg.productid = '00011496'
    
          
          select * from stocktake_item sti 
          where sti.store_product_sk in
         (select sp.sk from 
          product_gnm pg 
          left join stock_price_history sph on sph.product_gnm_sk = pg.sk
          left join storeproduct sp on sp.product_gnm_sk = pg.sk
          where pg.productid = '00011496')

          ---------------------- must navigate to stockprice history     
select * from  storeproduct sp 
          left join stock_price_history sph on sph.storeproduct_sk = sp.sk
          left join product_gnm pg on pg.sk = sp.product_gnm_sk
          left join stock_price_history sph2 on sph2.product_gnm_sk = sp.product_gnm_sk
          where sp.product_gnm_sk <> 0 and sp.sk <> 0 and sph2.sk is not null
    

 select * from store_stockmovement sm          
          
 /*         
      1. stock price for all gnm products - done
      2. averages for all product_gnm - done, maybe bug miscounting by 1
      3. job_history - cancelled jobs
      4. job_history - job extras
*/
 
      
          
select * from product_gnm_average_cost_history where created <= NOW() AND end_date > NOW()
and storeproduct_sk in (select storeproduct_sk from stock_price_history where created <= NOW() AND end_date > NOW())

---- store product get last price & last average cost history

--CREATE OR REPLACE VIEW public.view_last_stock_history_price AS
 SELECT sph.sk,
    sph.store_sk,
    sph.nk,
    sph.storeproduct_sk,
    sph.supplier_cost,
    sph.supplier_cost_gst,
    sph.actual_cost,
    sph.actual_cost_gst,
    sph.supplier_rrp,
    sph.supplier_rrp_gst,
    sph.calculated_retail_price,
    sph.calculated_retail_price_gst,
    sph.override_retail_price,
    sph.override_retail_price_gst,
    sph.retail_price,
    sph.retail_price_gst,
    sph.created,
    sph.product_gnm_sk,
    sph.end_date
   FROM stock_price_history sph
     JOIN ( SELECT sm.product_gnm_sk,
            max(sm.sk) AS lastprice_sk
           FROM stock_price_history sm
          WHERE sm.product_gnm_sk <> 0
          GROUP BY sm.product_gnm_sk) x ON x.lastprice_sk = sph.sk;

CREATE OR REPLACE VIEW public.view_last_stock_history_price AS
 SELECT sph.sk,
    sph.store_sk,
    sph.nk,
    sph.storeproduct_sk,
    sph.supplier_cost,
    sph.supplier_cost_gst,
    sph.actual_cost,
    sph.actual_cost_gst,
    sph.supplier_rrp,
    sph.supplier_rrp_gst,
    sph.calculated_retail_price,
    sph.calculated_retail_price_gst,
    sph.override_retail_price,
    sph.override_retail_price_gst,
    sph.retail_price,
    sph.retail_price_gst,
    sph.created,
    sph.product_gnm_sk,
    sph.end_date
   FROM stock_price_history sph
     JOIN ( select sk as lastprice_sk, row_number()  over (partition by product_gnm_sk order by end_date desc) idx from stock_price_history  ) x ON x.lastprice_sk = sph.sk and idx = 1;    




------------------------------------------------------------------------------------------------------------------------------
----------------- view for store product that is a gnm product, stock_price_history and 


--------- non gnm product prices
 SELECT
 	sp.*,
 	sph.sk,
    sph.store_sk,
    sph.nk,
    sph.storeproduct_sk,
    sph.supplier_cost,
    sph.supplier_cost_gst,
    sph.actual_cost,
    sph.actual_cost_gst,
    sph.supplier_rrp,
    sph.supplier_rrp_gst,
    sph.calculated_retail_price,
    sph.calculated_retail_price_gst,
    sph.override_retail_price,
    sph.override_retail_price_gst,
    sph.retail_price,
    sph.retail_price_gst,
    sph.created,
    sph.product_gnm_sk,
    sph.end_date
   FROM storeproduct sp 
   left join stock_price_history sph on sp.sk = sph.storeproduct_sk
   inner join ( SELECT sm.storeproduct_sk,
            max(sm.end_date) AS maxdate
           FROM stock_price_history sm
          WHERE sm.storeproduct_sk <> 0
          GROUP BY sm.storeproduct_sk) x ON x.maxdate = sph.end_date and x.storeproduct_sk = sph.storeproduct_sk
	where sp.product_gnm_sk = 0
		limit 1000;

------- gnm product prices
 SELECT
 	sp.*,
 	sph.sk,
    sph.store_sk,
    sph.nk,
    sph.storeproduct_sk,
    sph.supplier_cost,
    sph.supplier_cost_gst,
    sph.actual_cost,
    sph.actual_cost_gst,
    sph.supplier_rrp,
    sph.supplier_rrp_gst,
    sph.calculated_retail_price,
    sph.calculated_retail_price_gst,
    sph.override_retail_price,
    sph.override_retail_price_gst,
    sph.retail_price,
    sph.retail_price_gst,
    sph.created,
    sph.product_gnm_sk,
    sph.end_date
   FROM storeproduct sp 
   left join stock_price_history sph on sp.sk = sph.storeproduct_sk
   inner join ( SELECT sm.storeproduct_sk,
            max(sm.end_date) AS maxdate
           FROM stock_price_history sm
          WHERE sm.storeproduct_sk <> 0
          GROUP BY sm.storeproduct_sk) x ON x.maxdate = sph.end_date and x.storeproduct_sk = sph.storeproduct_sk
	where sp.product_gnm_sk = 0
		limit 1000;


 SELECT 
 	sp.*,
 	sph.sk,
    sph.store_sk,
    sph.nk,
    sph.storeproduct_sk,
    sph.supplier_cost,
    sph.supplier_cost_gst,
    sph.actual_cost,
    sph.actual_cost_gst,
    sph.supplier_rrp,
    sph.supplier_rrp_gst,
    sph.calculated_retail_price,
    sph.calculated_retail_price_gst,
    sph.override_retail_price,
    sph.override_retail_price_gst,
    sph.retail_price,
    sph.retail_price_gst,
    sph.created,
    sph.product_gnm_sk,
    sph.end_date,
	pga.*
	--- select *
FROM storeproduct sp 
   left join stock_price_history sph on sp.sk = sph.storeproduct_sk
    left JOIN ( SELECT sm.product_gnm_sk,
            max(sm.end_date) AS maxdate
           FROM stock_price_history sm
          WHERE sm.product_gnm_sk <> 0
          GROUP BY sm.product_gnm_sk) x ON x.maxdate = sph.end_date and x.product_gnm_sk = sph.product_gnm_sk
	left join product_gnm_average_cost_history pga on pga.storeproduct_sk = sp.sk
     left join ( SELECT sm.storeproduct_sk,
            max(sm.end_date) AS maxdate
           FROM product_gnm_average_cost_history sm
          GROUP BY sm.storeproduct_sk) y ON y.maxdate = pga.end_date and y.storeproduct_sk = pga.storeproduct_sk
	where 
		sp.sk <> 0
limit 1000;

select count(*) from (
select *
FROM storeproduct sp 
inner join stock_price_history sph on sp.product_gnm_sk = sph.product_gnm_sk and sph.end_date = '2030-01-01 12:00:00'
left join product_gnm_average_cost_history pga on pga.storeproduct_sk = sp.sk and pga.end_date = '2030-01-01 12:00:00'
where 
		sp.product_gnm_sk <> 0  
) x

select count(*) from (
select *
FROM storeproduct sp 
inner join stock_price_history sph on sp.product_gnm_sk = sph.product_gnm_sk and sph.end_date = '2030-01-01 12:00:00'
left join product_gnm_average_cost_history pga on pga.storeproduct_sk = sp.sk and pga.end_date = '2030-01-01 12:00:00'
where 
		sp.product_gnm_sk <> 0  and pga.sk is not null
) x

select max(end_date) from stock_price_history

select count(*)  from (
 select store_sk,count(*) from product_gnm_average_cost_history group by store_sk order by count(*) desc
) x

select count(*)  from (
select * from vstore_stockmovement sm where store_sk = 1 and name = 'Receipt' and product_gnm_sk <> 0 order by sk asc
) z

select sm.product_gnm_sk, pga.average_cost,sm.moving_cost_per_unit,pga.actual_cost,  sm.actual_cost,sm.adjustmentdate,  * 
from vstore_stockmovement sm 
left join product_gnm_average_cost_history  pga on store_stockmovement_sk = sm.sk
where sm.store_sk = 1 and name = 'Receipt' and sm.product_gnm_sk <> 0 -- and pga.sk is null
order by sm.product_gnm_sk, sm.adjustmentdate asc
;

--------- daily adjustments arent being calculated

select * from stock_price_history where sk =  1528332

select * from daily_adjustments_sum da where product_gnm_sk = 84;

            select sm.store_sk, 
            sm.storeproduct_sk, 
            coalesce(sm.product_gnm_sk,0)  as product_gnm_sk,
            date_trunc('day',sm.adjustmentdate) as date_actual, 
            sum(adjustment) dailytotal, 
            row_number() over (partition by sm.store_sk,sm.storeproduct_sk order by date_trunc('day',sm.adjustmentdate) ) as seq
            from store_stockmovement sm
            where sm.store_sk is not null and product_gnm_sk = 84
            group by sm.store_sk, sm.storeproduct_sk, coalesce(sm.product_gnm_sk,0), date_trunc('day',sm.adjustmentdate);

select * from store_stockmovement sm where product_gnm_sk = 84 and store_sk = 1

select sm.product_gnm_sk, pga.average_cost,sm.moving_cost_per_unit,pga.actual_cost,  sm.actual_cost,sm.adjustmentdate,  * 
from vstore_stockmovement sm 
left join product_gnm_average_cost_history  pga on store_stockmovement_sk = sm.sk
where sm.store_sk = 1 and name = 'Receipt' and sm.product_gnm_sk <> 0 and  sm.product_gnm_sk = 84 -- and pga.sk is null
order by sm.product_gnm_sk, sm.adjustmentdate asc
;


select *
FROM storeproduct sp 
inner join stock_price_history sph on sp.product_gnm_sk = sph.product_gnm_sk and sph.end_date = '2030-01-01 12:00:00'
left join product_gnm_average_cost_history pga on pga.storeproduct_sk = sp.sk and pga.end_date = '2030-01-01 12:00:00'
where 
		sp.product_gnm_sk <> 0  and pga.sk is not null  and  sp.product_gnm_sk = 84
		
		
------------- averages for product
-- convirm view is fixed with a left join
		    select vah.storeproduct_sk,
                vah.product_gnm_sk,
                vah.store_sk,
                vah.adjustmentdate,
                vah.sk as store_stockmovement_sk,
                vah.quantity,vah.running_total,
                vah.prior_avg_cost,vah.adjustment,
                vah.actual_cost,vah.actual_cost_gst,
                vah.average_cost,vah.average_cost_gst,
                '2030-01-01'::timestamp as end_date
            from view_product_gnm_average_cost_history vah where  product_gnm_sk = 84

------------------------------------------------------------------------------------------------------------
--- investigate 
            
select sm.store_sk, sm.product_gnm_sk,sm.adjustmentdate,sm.adjustment, pga.average_cost,sm.moving_cost_per_unit,pga.actual_cost,  sm.actual_cost,  * 
from vstore_stockmovement sm 
left join product_gnm_average_cost_history  pga on store_stockmovement_sk = sm.sk
where sm.store_sk = 1 and sm.product_gnm_sk <> 0 and  sm.product_gnm_sk = 96  and name = 'Receipt' 
order by sm.store_sk,sm.product_gnm_sk, sm.adjustmentdate asc
;

-- gnm = 96

select *
FROM storeproduct sp 
inner join stock_price_history sph on sp.product_gnm_sk = sph.product_gnm_sk and sph.end_date = '2030-01-01 12:00:00'
left join product_gnm_average_cost_history pga on pga.storeproduct_sk = sp.sk and pga.end_date = '2030-01-01 12:00:00'
where 
		sp.product_gnm_sk <> 0  and  sp.product_gnm_sk = 96

		
		select * from storeproduct sp where product_gnm_sk = 96 and storeid ='2088-012'
		
		select * from "store"
		

select * from stock_price_history where product_gnm_sk =  96 order by created desc

---- return products with bad stock_price_history

select product_gnm_sk, mdate from 
(select product_gnm_sk, max(end_date) mdate from stock_price_history group by product_gnm_sk) x
where mdate <> '2030-01-01 12:00:00'

select * from product_gnm_average_cost_history where product_gnm_sk = 96;


select * from vstore_stockmovement sm 
where product_gnm_sk = 96
order by store_sk, adjustmentdate



select * from daily_adjustments_sum da where product_gnm_sk = 96;

select vah.storeproduct_sk,
                vah.product_gnm_sk,
                vah.store_sk,
                vah.adjustmentdate,
                vah.sk as store_stockmovement_sk,
                vah.quantity,vah.running_total,
                vah.prior_avg_cost,vah.adjustment,
                vah.actual_cost,vah.actual_cost_gst,
                vah.average_cost,vah.average_cost_gst,
                '2030-01-01'::timestamp as end_date
            from view_product_gnm_average_cost_history vah where  product_gnm_sk = 96;


           drop view if exists view_product_gnm_average_cost_history;

create or replace view view_product_gnm_average_cost_history as
with recursive cogs(
	sk,
	adjustmentdate,
	quantity,
	running_total,
	store_sk,
	product_gnm_sk,
	storeproduct_sk,
	actual_cost,
	actual_cost_gst,
	prior_avg_cost,
	adjustment,
	average_cost,
	average_cost_gst,
	next_sk,
	seq
) as(
	select
		sm.sk,
		sm.adjustmentdate,
		1 quantity,
		1 running_total,
		sm.store_sk,
		sm.product_gnm_sk,
		sm.storeproduct_sk,
		sp.actual_cost::numeric(19,2),
		sp.actual_cost_gst::numeric(19,2),
		sp.actual_cost_gst::numeric(19,2),
		0,
		sp.actual_cost::numeric(19,2) as average_cost,
		sp.actual_cost_gst::numeric(19,2) as average_cost_gst,
        seq2.sk as next_sk,
        seq2.r
	from
		(select sk,row_number() over (partition by sm.store_sk,sm.product_gnm_sk order by sm.adjustmentdate,sm.sk ) r from store_stockmovement sm ) seq
		inner join store_stockmovement sm on seq.sk = sm.sk
		inner join (select sk,sm.store_sk,sm.product_gnm_sk,row_number() over (partition by sm.store_sk,sm.product_gnm_sk order by sm.adjustmentdate,sm.sk ) r from store_stockmovement sm ) seq2
			on  seq.r+1 = seq2.r and seq2.product_gnm_sk = sm.product_gnm_sk and seq2.store_sk = sm.store_sk 
		inner join adjustment_type a on a.sk = sm.adjustment_type_sk
		left join  -- get the base price by date
			stock_price_history sp on sp.product_gnm_sk = sm.product_gnm_sk and sp.created <= sm.adjustmentdate and sp.end_date > sm.adjustmentdate
	where sm.product_gnm_sk <> 0 and a.Name in ('Receipt') and seq.r = 1
union all
	select
		sm.sk,
		sm.adjustmentdate,
		sm.adjustment as quantity,
		sm.adjustment + c.running_total as running_total,
		sm.store_sk,
		sm.product_gnm_sk,
		sm.storeproduct_sk,
		sp.actual_cost::numeric(19,2),
		sp.actual_cost_gst::numeric(19,2),
		c.average_cost,
		sm.adjustment,
		case when sm.adjustment + c.running_total <> 0
			then ((c.average_cost*c.running_total + sp.actual_cost*sm.adjustment) / (sm.adjustment + c.running_total))::numeric(19,2)
			else sp.actual_cost
		end
		as average_cost,
		case when sm.adjustment + c.running_total <> 0
			then ((c.average_cost_gst*c.running_total  + sp.actual_cost_gst*sm.adjustment) / (sm.adjustment + c.running_total))::numeric(19,2) 
			else sp.actual_cost_gst
		end
		as average_cost_gst,
		seq2.sk next_sk,
		seq2.r
	from cogs c 
	  	inner join store_stockmovement sm on sm.sk = c.next_sk
		inner join adjustment_type a on a.sk = sm.adjustment_type_sk
		left join stock_price_history sp on sp.product_gnm_sk = sm.product_gnm_sk and sp.created <= sm.adjustmentdate and sp.end_date > sm.adjustmentdate
		left join 
			(select sk,sm.store_sk,sm.product_gnm_sk,row_number() over (partition by sm.store_sk,sm.product_gnm_sk order by sm.adjustmentdate,sm.sk ) r 
			from store_stockmovement sm ) seq2 on  c.seq+1 = seq2.r and seq2.product_gnm_sk = sm.product_gnm_sk and seq2.store_sk = sm.store_sk 
		where sm.product_gnm_sk <> 0 and a.Name in ('Receipt') 
) select  * 
 from cogs;
 




select
		sm.sk,
		sm.adjustmentdate,
		1 quantity,
		1 running_total,
		sm.store_sk,
		sm.product_gnm_sk,
		sm.storeproduct_sk,
		sp.actual_cost::numeric(19,2),
		sp.actual_cost_gst::numeric(19,2),
		sp.actual_cost_gst::numeric(19,2),
		0,
		sp.actual_cost::numeric(19,2) as average_cost,
		sp.actual_cost_gst::numeric(19,2) as average_cost_gst,
        seq2.sk as next_sk,
        seq2.r
-- select * 
	from
		(select sk,row_number() over (partition by sm.store_sk,sm.product_gnm_sk order by sm.adjustmentdate,sm.sk ) r from vstore_stockmovement sm where Name in ('Receipt')) seq
		inner join store_stockmovement sm on seq.sk = sm.sk
		left join (select sk,sm.store_sk,sm.product_gnm_sk,row_number() over (partition by sm.store_sk,sm.product_gnm_sk order by sm.adjustmentdate,sm.sk ) r from store_stockmovement sm ) seq2
			on  seq.r+1 = seq2.r and seq2.product_gnm_sk = sm.product_gnm_sk and seq2.store_sk = sm.store_sk 
		inner join adjustment_type a on a.sk = sm.adjustment_type_sk
		left join  -- get the base price by date
			stock_price_history sp on sp.product_gnm_sk = sm.product_gnm_sk and sp.created <= sm.adjustmentdate and sp.end_date > sm.adjustmentdate
	where sm.product_gnm_sk <> 0 and a.Name in ('Receipt') and seq.r = 1 and sm.product_gnm_sk =96
	
	
select *
FROM storeproduct sp 
   left join stock_price_history sph on sp.sk = sph.storeproduct_sk
    left JOIN ( SELECT sm.product_gnm_sk,
            max(sm.end_date) AS maxdate
           FROM stock_price_history sm
          WHERE sm.product_gnm_sk <> 0
          GROUP BY sm.product_gnm_sk) x ON x.maxdate = sph.end_date and x.product_gnm_sk = sph.product_gnm_sk
	left join product_gnm_average_cost_history pga on pga.storeproduct_sk = sp.sk
     left join ( SELECT sm.storeproduct_sk,
            max(sm.end_date) AS maxdate
           FROM product_gnm_average_cost_history sm
          GROUP BY sm.storeproduct_sk) y ON y.maxdate = pga.end_date and y.storeproduct_sk = pga.storeproduct_sk
	where 
		sp.sk <> 0
limit 1000;

select *
FROM storeproduct sp 
inner join stock_price_history sph on sp.product_gnm_sk = sph.product_gnm_sk and sph.end_date = '2030-01-01 12:00:00'
left join product_gnm_average_cost_history pga on pga.storeproduct_sk = sp.sk and pga.end_date = '2030-01-01 12:00:00'
where 
		sp.product_gnm_sk <> 0  and pga.sk is not null
limit 1000;


------------------------------------------------------------------------------------------------------------
--- investigate 
 --- differences in calculation between mine & maurices           
select sm.store_sk, sm.product_gnm_sk,sm.adjustmentdate,sm.adjustment, pga.average_cost,sm.moving_cost_per_unit,pga.actual_cost,  sm.actual_cost,  * 
from vstore_stockmovement sm 
left join product_gnm_average_cost_history  pga on store_stockmovement_sk = sm.sk
where sm.store_sk = 1 and sm.product_gnm_sk <> 0 and name = 'Receipt' 
and average_cost <> moving_cost_per_unit
order by sm.store_sk,sm.product_gnm_sk, sm.adjustmentdate asc
;

--- matching calc between mine & maurice
select sm.store_sk, sm.product_gnm_sk,sm.adjustmentdate,sm.adjustment, pga.average_cost,sm.moving_cost_per_unit,pga.actual_cost,  sm.actual_cost,  * 
from vstore_stockmovement sm 
left join product_gnm_average_cost_history  pga on store_stockmovement_sk = sm.sk
where sm.store_sk = 1 and sm.product_gnm_sk <> 0 and name = 'Receipt' 
and average_cost = moving_cost_per_unit
order by sm.store_sk,sm.product_gnm_sk, sm.adjustmentdate asc
;

--- null average
select sm.store_sk, sm.product_gnm_sk,sm.adjustmentdate,sm.adjustment, pga.average_cost,sm.moving_cost_per_unit,pga.actual_cost,  sm.actual_cost,  * 
from vstore_stockmovement sm 
left join product_gnm_average_cost_history  pga on store_stockmovement_sk = sm.sk
where sm.store_sk = 1 and sm.product_gnm_sk <> 0 and name = 'Receipt' 
and average_cost is null
order by sm.store_sk,sm.product_gnm_sk, sm.adjustmentdate asc
;

------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------
--- investigate 
 --- differences in calculation between mine & maurices           
select sm.store_sk, sm.product_gnm_sk,sm.adjustmentdate,sm.adjustment, pga.average_cost,sm.moving_cost_per_unit,pga.actual_cost,  sm.actual_cost,  * 
from vstore_stockmovement sm 
left join product_gnm_average_cost_history  pga on store_stockmovement_sk = sm.sk
where  sm.product_gnm_sk <> 0 and name = 'Receipt' 
and average_cost <> moving_cost_per_unit
order by sm.store_sk,sm.product_gnm_sk, sm.adjustmentdate asc
;

--- matching calc between mine & maurice
select sm.store_sk, sm.product_gnm_sk,sm.adjustmentdate,sm.adjustment, pga.average_cost,sm.moving_cost_per_unit,pga.actual_cost,  sm.actual_cost,  * 
from vstore_stockmovement sm 
left join product_gnm_average_cost_history  pga on store_stockmovement_sk = sm.sk
where   sm.product_gnm_sk <> 0 and name = 'Receipt' 
and average_cost = moving_cost_per_unit
order by sm.store_sk,sm.product_gnm_sk, sm.adjustmentdate asc
;

--- null average
select sm.store_sk, sm.product_gnm_sk,sm.adjustmentdate,sm.adjustment, pga.average_cost,sm.moving_cost_per_unit,pga.actual_cost,  sm.actual_cost,  * 
from vstore_stockmovement sm 
left join product_gnm_average_cost_history  pga on store_stockmovement_sk = sm.sk
where sm.product_gnm_sk <> 0 and name = 'Receipt' 
and average_cost is null and moving_cost_per_unit is not null
order by sm.store_sk,sm.product_gnm_sk, sm.adjustmentdate asc
;

------------------------------------------------------------------------------------------------------------

--- 
select sph.actual_cost,sph.actual_cost_gst,sph.created,sph.end_date,pga.*
FROM storeproduct sp 
inner join stock_price_history sph on sp.product_gnm_sk = sph.product_gnm_sk and sph.end_date = '2030-01-01 12:00:00'
left join product_gnm_average_cost_history pga on pga.storeproduct_sk = sp.sk and pga.end_date = '2030-01-01 12:00:00'
where 
		sp.product_gnm_sk <> 0  and  sp.product_gnm_sk = 5 and storeid = '2088-012'


select sm.store_sk, sm.product_gnm_sk,sm.adjustmentdate,sm.adjustment,sph.actual_cost, pga.average_cost,sm.moving_cost_per_unit,pga.actual_cost,  sm.actual_cost,  * 
from vstore_stockmovement sm 
left join product_gnm_average_cost_history  pga on store_stockmovement_sk = sm.sk
left join stock_price_history sph on sm.product_gnm_sk = sph.product_gnm_sk and sph.end_date > sm.adjustmentdate and sph.created <= sm.adjustmentdate
where sm.store_sk = 1 and sm.product_gnm_sk <> 0 and name = 'Receipt' and sm.product_gnm_sk = 5
and average_cost is null
order by sm.store_sk,sm.product_gnm_sk, sm.adjustmentdate asc
;		
		
		----------------
		
		select * from vstore_stockmovement sm where name = 'Receipt' and sm.product_gnm_sk = 5
		
		select * from adjustment_type where name = 'Receipt'
		
		select * from adjustment_type
		select * from "store"
		
		
		
select c.*,* from 
	(select
		sm.sk,
		sm.adjustmentdate,
		1 quantity,
		1 running_total,
		sm.store_sk,
		sm.product_gnm_sk,
		sm.storeproduct_sk,
		sp.actual_cost::numeric(19,2),
		sp.actual_cost_gst::numeric(19,2),
		sp.actual_cost_gst::numeric(19,2),
		0,
		sp.actual_cost::numeric(19,2) as average_cost,
		sp.actual_cost_gst::numeric(19,2) as average_cost_gst,
        seq2.sk as next_sk,
        seq2.r as seq
	from
		(select sk,row_number() over (partition by sm.store_sk,sm.product_gnm_sk order by sm.adjustmentdate,sm.sk ) r from store_stockmovement sm where adjustment_type_sk in (1,7,8) ) seq
		inner join store_stockmovement sm on seq.sk = sm.sk
		left join (select sk,sm.store_sk,sm.product_gnm_sk,row_number() over (partition by sm.store_sk,sm.product_gnm_sk order by sm.adjustmentdate,sm.sk ) r from store_stockmovement sm ) seq2
			on  seq.r+1 = seq2.r and seq2.product_gnm_sk = sm.product_gnm_sk and seq2.store_sk = sm.store_sk 
		inner join adjustment_type a on a.sk = sm.adjustment_type_sk
		left join  -- get the base price by date
			stock_price_history sp on sp.product_gnm_sk = sm.product_gnm_sk and sp.created <= sm.adjustmentdate and sp.end_date > sm.adjustmentdate
	where sm.product_gnm_sk <> 0 and a.Name in ('Receipt') and seq.r = 1  ) c
	  	left join store_stockmovement sm on sm.sk = c.next_sk and sm.product_gnm_sk <> 0 
		left join adjustment_type a on a.sk = sm.adjustment_type_sk and a.Name in ('Receipt') 	
		left join stock_price_history sp on sp.product_gnm_sk = sm.product_gnm_sk and sp.created <= sm.adjustmentdate and sp.end_date > sm.adjustmentdate
		left join 
			(select sk,sm.store_sk,sm.product_gnm_sk,row_number() over (partition by sm.store_sk,sm.product_gnm_sk order by sm.adjustmentdate,sm.sk ) r 
			from store_stockmovement sm ) seq2 on  c.seq+1 = seq2.r and seq2.product_gnm_sk = sm.product_gnm_sk and seq2.store_sk = sm.store_sk 
	
		
		
		--------------
		-- are there any gnm products with stockmovements not processed with averages
		
			
select count(*) from (			
		select store_sk,product_gnm_sk,count(*) from  store_stockmovement  pga 
		where pga.product_gnm_sk not in (select product_gnm_sk from product_gnm_average_cost_history sm group by sm.product_gnm_sk) and product_gnm_sk <> 0
		group by store_sk,product_gnm_sk order by store_sk, product_gnm_sk
) x	;


select 		
select count(*) from (			
		select store_sk,product_gnm_sk,count(*) from  store_stockmovement  pga 
		where pga.product_gnm_sk not in (select product_gnm_sk from product_gnm_average_cost_history sm group by sm.product_gnm_sk) 
		group by store_sk,product_gnm_sk order by store_sk, product_gnm_sk
) x	
			
select count(*) from (			
		select store_sk,product_gnm_sk,count(*) from  store_stockmovement  pga 
		where pga.product_gnm_sk in (select product_gnm_sk from product_gnm_average_cost_history sm group by sm.product_gnm_sk) 
		group by store_sk,product_gnm_sk order by store_sk, product_gnm_sk
) x	;



select * from stocktake_item

select * from product_price_history where product_id ='00185927';


select * from product_gnm pg  where productid ='00185927';

select * from stock_price_history sp where product_gnm_sk = 20526



select * from product_gnm_average_cost_history where product_gnm_sk = 20526

select * from store_stockmovement sm where product_gnm_sk = 20526 

order by sk  asc

select * from adjustment_type
select * from stocktake
select * from stocktake_item

select * from 

update adjustment_type set name = 'Stocktake Receipt' where sk = 320;

delete 
from store_stockmovement ssm where ssm.store_sk in (45,46);

create index on store_stockmovement(store_sk);




            select s.store_sk, si.sk as nk, si.store_product_sk as storeproduct_sk, 
            s.actual_stocktake_end_date_time as adjustmentdate,
            d.sk as adjustmentdate_sk,
            si.product_gnm_sk, a.sk as adjustment_type_sk, 
            coalesce((select sk from  stock_price_history sph where sph.storeproduct_sk = si.store_product_sk 
                -- and sph.created >= s.actual_stocktake_end_date_time and sph.end_date < s.actual_stocktake_end_date_time 
                limit 1),0) as stock_price_history_sk, -1*si.expected_qty as adjustment, a.othername as reason
            -- select *
            from stocktake_item si 
            inner join stocktake s on s.sk = si.stocktake_sk
            inner join adjustment_type a on a.adjustment_typeid = 'STOCKTAKE_OUT'
            left outer join d_date d on (TO_CHAR((s.actual_stocktake_end_date_time::timestamp),'yyyymmdd')::INT = d.date_dim_id)
            where s.state = 'APPLY_STOCKTAKE'

SELECT * FROM dbo.SUNGLASS_BRANCH sb inner join dbo.SUNGLASS s on (sb.SUNGLASSID = s.ID)  where (sb.DATE_ADDED is not null) and (sb.DATE_EDITED > >= dateadd(day,"""${lookback_days}""",getdate()) OR s.DATE_EDITED > >= dateadd(day,"""${lookback_days}""",getdate()))


select * from store_stockmovement where store_sk = 45 order by created desc

select * from 


----------- Create view that calcuates averages, save into a table of cogs averages, update end_date ranges for the table
set role scire;
SET search_path = public, pg_catalog;

---------------------------------------------------------------------------
--- use a sequence table but using row number to generate it

-- set role postgres;
drop view if exists view_product_gnm_average_cost_history;

create or replace view view_product_gnm_average_cost_history as
with recursive cogs(
	sk,
	adjustmentdate,
	quantity,
	running_total,
	store_sk,
	product_gnm_sk,
	storeproduct_sk,
	actual_cost,
	actual_cost_gst,
	prior_avg_cost,
	adjustment,
	average_cost,
	average_cost_gst,
	next_sk,
	seq
) as(
	select
		sm.sk,
		sm.adjustmentdate,
		1 quantity,
		1 running_total,
		sm.store_sk,
		sm.product_gnm_sk,
		sm.storeproduct_sk,
		sp.actual_cost::numeric(19,2),
		sp.actual_cost_gst::numeric(19,2),
		sp.actual_cost_gst::numeric(19,2),
		0,
		sp.actual_cost::numeric(19,2) as average_cost,
		sp.actual_cost_gst::numeric(19,2) as average_cost_gst,
        seq2.sk as next_sk,
        seq2.r
	from
		(select sm.sk,row_number() over (partition by sm.store_sk,sm.product_gnm_sk order by sm.adjustmentdate,sm.sk ) r 
				from store_stockmovement sm  where sm.adjustment_type_sk in (select sk from adjustment_type a where a.Name in ('Receipt'))
				) seq
		inner join store_stockmovement sm on seq.sk = sm.sk and sm.product_gnm_sk <> 0
		left join (select sk,sm.store_sk,sm.product_gnm_sk,row_number() over (partition by sm.store_sk,sm.product_gnm_sk order by sm.adjustmentdate,sm.sk ) r 
			from store_stockmovement sm ) seq2 on  seq.r+1 = seq2.r and seq2.product_gnm_sk = sm.product_gnm_sk and seq2.store_sk = sm.store_sk 
		inner join adjustment_type a on a.sk = sm.adjustment_type_sk  and a.Name in ('Receipt')  
		left join  -- get the base price by date
			stock_price_history sp on sp.product_gnm_sk = sm.product_gnm_sk and sp.created <= sm.adjustmentdate and sp.end_date > sm.adjustmentdate
	where seq.r = 1
union all
	select
		sm.sk,
		sm.adjustmentdate,
		sm.adjustment as quantity,
		sm.adjustment + c.running_total as running_total,
		sm.store_sk,
		sm.product_gnm_sk,
		sm.storeproduct_sk,
		sp.actual_cost::numeric(19,2),
		sp.actual_cost_gst::numeric(19,2),
		c.average_cost,
		sm.adjustment,
		case when sm.adjustment + c.running_total <> 0
			then ((c.average_cost*c.running_total + sp.actual_cost*sm.adjustment) / (sm.adjustment + c.running_total))::numeric(19,2)
			else sp.actual_cost
		end
		as average_cost,
		case when sm.adjustment + c.running_total <> 0
			then ((c.average_cost_gst*c.running_total  + sp.actual_cost_gst*sm.adjustment) / (sm.adjustment + c.running_total))::numeric(19,2) 
			else sp.actual_cost_gst
		end
		as average_cost_gst,
		seq2.sk next_sk,
		seq2.r
	from cogs c 
	  	inner join store_stockmovement sm on sm.sk = c.next_sk and sm.product_gnm_sk <> 0 
		left join adjustment_type a on a.sk = sm.adjustment_type_sk and a.Name in ('Receipt') 	
		left join stock_price_history sp on sp.product_gnm_sk = sm.product_gnm_sk and sp.created <= sm.adjustmentdate and sp.end_date > sm.adjustmentdate
		left join 
			(select sk,sm.store_sk,sm.product_gnm_sk,row_number() over (partition by sm.store_sk,sm.product_gnm_sk order by sm.adjustmentdate,sm.sk ) r 
			from store_stockmovement sm ) seq2 on  c.seq+1 = seq2.r and seq2.product_gnm_sk = sm.product_gnm_sk and seq2.store_sk = sm.store_sk 
) select  * 
 from cogs;


select * from adjustment_type order by sk desc


		select sm.sk,row_number() over (partition by sm.store_sk,sm.product_gnm_sk order by sm.adjustmentdate,sm.sk ) r 
				from store_stockmovement sm  where sm.adjustment_type_sk in (select sk from adjustment_type a where a.Name in ('Receipt'))

		
		select sm.sk,row_number() over (partition by sm.store_sk,sm.product_gnm_sk order by sm.adjustmentdate,sm.sk ) r 
				from store_stockmovement sm  where sm.adjustment_type_sk in (select sk from adjustment_type a where a.Name in ('Stocktake Receipt'))

--- confirm all the stockmovements were created				
				select adjustment_type_sk,count(*) from  store_stockmovement sm where store_sk = 45 group by adjustment_type_sk
				
		            select s.store_sk, si.sk as nk, si.store_product_sk as storeproduct_sk, 
            s.actual_stocktake_end_date_time as adjustmentdate,
            d.sk as adjustmentdate_sk,
            si.product_gnm_sk, a.sk as adjustment_type_sk, 
            coalesce((select sk from  stock_price_history sph where sph.storeproduct_sk = si.store_product_sk 
                -- and sph.created >= s.actual_stocktake_end_date_time and sph.end_date < s.actual_stocktake_end_date_time 
                limit 1),0) as stock_price_history_sk, si.actual_qty as adjustment, a.othername as reason
            -- select *
            from stocktake_item si 
            inner join stocktake s on s.sk = si.stocktake_sk
            inner join adjustment_type a on a.adjustment_typeid = 'STOCKTAKE_IN'
            left outer join d_date d on (TO_CHAR((s.actual_stocktake_end_date_time::timestamp),'yyyymmdd')::INT = d.date_dim_id)
            where s.state = 'APPLY_STOCKTAKE'	
            
            
            
            select * from stocktake_item si
            inner join stocktake s on s.sk = si.stocktake_sk
            left join product_gnm_average_cost_history pga on pga.storeproduct_sk = si.store_product_sk
            where s.store_sk in (45,46) -- and si.store_product_sk
            and pga.sk is not null;


-----------------------------------------------------------------------------------
--- check a stocktake does not have any averages calculated for it
            
select store_product_sk,product_gnm_sk from stocktake_item si
     inner join stocktake s on s.sk = si.stocktake_sk
where
 si.store_product_sk not in (select storeproduct_sk from product_gnm_average_cost_history) and s.store_sk in (45,46)

 
 select * from adjustment_type order by sk desc;

 
 select * from stocktake_item si
 inner join store_stockmovement sm on si.sk = sm.stocktake_item_sk
	where sm.storeproduct_sk  not in (select storeproduct_sk from product_gnm_average_cost_history)
 
            select * from product_gnm_average_cost_history
            
            select * from view_product_gnm_average_cost_history pga where pga.store_sk in (45,46);
            
		select s.store_sk, ('SYS-STR-' || si.sk) as nk, si.store_product_sk as storeproduct_sk, 
            s.actual_stocktake_end_date_time as adjustmentdate,
            d.sk as adjustmentdate_sk,
            si.product_gnm_sk, a.sk as adjustment_type_sk, 
            coalesce((select sk from  stock_price_history sph where sph.storeproduct_sk = si.store_product_sk	limit 1),0) as stock_price_history_sk, 
            si.actual_qty as adjustment, 
            a.othername as reason,
            si.stocktake_sk, si.sk as stocktake_item_sk
            from stocktake_item si 
            inner join stocktake s on s.sk = si.stocktake_sk
            inner join adjustment_type a on a.name = 'Stocktake Reset'
            left outer join d_date d on (TO_CHAR((s.actual_stocktake_end_date_time::timestamp),'yyyymmdd')::INT = d.date_dim_id)
            where s.state = 'APPLY_STOCKTAKE' 


delete from product_gnm_average_cost_history where store_sk = 45;
select * from product_gnm_average_cost_history where store_sk = 45 and product_gnm_sk <> 0





-- 1. Recurse through events and apply them

-- 2. locate Event in time to start from
-- possible start points, Receipts or Stocktake In or Reset - starting values
-- 


--- initial query for calculating averages

--- initial query for calculating averages incrementally
--- 1. looks into averages table

select * from adjustment_type

select * from vstore_stockmovement where name = 'Stocktake Receipt' and product_gnm_sk <> 0

-- create some test data for store product = 2076915, product_gnm_sk  = 26095

select * from storeproduct where sk = 2076915

select * from store_stockmovement where storeproduct_sk = 2076915 order by adjustmentdate

select * from stock_price_history sph where storeproduct_sk = 2076915
select * from stock_price_history sph where product_gnm_sk = 26095 order by created

SELECT DATEADD(day, 1, GETDATE());

-----------------------------------------------------------------------------------------------
---- generate some receipt data

select * from stock_price_history sph where product_gnm_sk = 26095 and actual_cost is not null order by created

select store_sk, nk, storeproduct_sk, adjustmentdate, stock_price_history_sk, adjustment, reason,  comments, created, 
	adjustmentdate_sk, product_gnm_sk, adjustment_type_sk from vstore_stockmovement where name = 'Receipt' and product_gnm_sk <> 0

	delete from product_gnm_average_cost_history pga where storeproduct_sk = 2076915 ;
	delete from store_stockmovement where storeproduct_sk = 2076915 and adjustment_type_sk in (1,2)

---select floor(random()*(5-1+1))+1  from store_stock_history limit 100

	
set role scire;
SET search_path = public, pg_catalog;



-----
INSERT INTO public.store_stockmovement(
store_sk, nk, storeproduct_sk, adjustmentdate, stock_price_history_sk, adjustment, reason, comments, created, 
adjustmentdate_sk, product_gnm_sk, adjustment_type_sk)
select 45 as store_sk, sph.sk as nk, 2076915 as storeproduct_sk,created as adjustmentdate , 
sk as stock_price_history_sk, 3 as adjustment, 'test data jp', 'test data jp', now(), null, 26095, 1
from stock_price_history sph where product_gnm_sk = 26095 order by created;
	
-----	
INSERT INTO public.store_stockmovement(
store_sk, nk, storeproduct_sk, adjustmentdate, stock_price_history_sk, adjustment, reason, comments, created, 
adjustmentdate_sk, product_gnm_sk, adjustment_type_sk)
select 45 as store_sk, sph.sk as nk, 2076915 as storeproduct_sk,created + interval '5 day'  as adjustmentdate , 
sk as stock_price_history_sk, -2 as adjustment, 'test data jp', 'test data jp', now(), null, 26095, 2
from stock_price_history sph where product_gnm_sk = 26095 order by created;

----------------update the averages
-- make sure all product_gnm are not null
update store_stockmovement sm set product_gnm_sk = 0
where product_gnm_sk is null;

insert into product_gnm_average_cost_history (   
 	storeproduct_sk,
 	product_gnm_sk,
 	store_sk,
    adjustmentdate,
    store_stockmovement_sk,
 	quantity, running_total,
	prior_avg_cost, adjustment,

    actual_cost , actual_cost_gst,
    average_cost, average_cost_gst
    )
select vah.storeproduct_sk,vah.product_gnm_sk,
	vah.store_sk,
	vah.adjustmentdate,
	vah.sk,
	vah.quantity,vah.running_total,
	vah.prior_avg_cost,vah.adjustment,
	vah.actual_cost,vah.actual_cost_gst,
	vah.average_cost,vah.average_cost_gst
from view_product_gnm_average_cost_history vah 
left join product_gnm_average_cost_history ph on ph.store_stockmovement_sk = vah.sk
where 
	ph.store_stockmovement_sk is null and vah.adjustmentdate is not null;



---- check data was initialised
select * from product_gnm_average_cost_history pga where storeproduct_sk = 2076915 order by adjustmentdate

select store_sk, nk, storeproduct_sk, adjustmentdate, stock_price_history_sk, adjustment, reason,  comments, created, 
	adjustmentdate_sk, product_gnm_sk, adjustment_type_sk from vstore_stockmovement where storeproduct_sk = 2076915 order by adjustmentdate


	

----------------- init query
--- 1. Init is caused by first Receipt OR first Stocktake OR Avg Reset
--- for each product create init record <- populated from that point
--- each record is a last checkpoint that the recursive works forward from

	
--- what does an init look like
--- 1. Init cost, Process init cost, for product - stock_price_history, stage into stock_price_history_history
--- Stage a reset -- 

	select
		sm.sk,
		sm.adjustmentdate,
		sm.adjustment quantity,
		0 running_total,
		sm.store_sk,
		sm.product_gnm_sk,
		sm.storeproduct_sk,
		sp.actual_cost::numeric(19,2),
		sp.actual_cost_gst::numeric(19,2),
		sp.actual_cost_gst::numeric(19,2),
		0,
		sp.actual_cost::numeric(19,2) as average_cost,
		sp.actual_cost_gst::numeric(19,2) as average_cost_gst,
        seq2.sk as next_sk,
        seq2.r
	from
		(select sm.sk,row_number() over (partition by sm.store_sk,sm.product_gnm_sk order by sm.adjustmentdate,sm.sk ) r 
				from store_stockmovement sm  where sm.adjustment_type_sk in (select sk from adjustment_type a where a.Name in ('Receipt'))
				) seq
		inner join store_stockmovement sm on seq.sk = sm.sk and sm.product_gnm_sk <> 0
		left join (select sk,sm.store_sk,sm.product_gnm_sk,row_number() over (partition by sm.store_sk,sm.product_gnm_sk order by sm.adjustmentdate,sm.sk ) r 
			from store_stockmovement sm ) seq2 on  seq.r+1 = seq2.r and seq2.product_gnm_sk = sm.product_gnm_sk and seq2.store_sk = sm.store_sk 
		inner join adjustment_type a on a.sk = sm.adjustment_type_sk  and a.Name in ('Receipt')  
		left join  -- get the base price by date
			stock_price_history sp on sp.product_gnm_sk = sm.product_gnm_sk and sp.created <= sm.adjustmentdate and sp.end_date > sm.adjustmentdate
	where seq.r = 1 and sm.storeproduct_sk = 2076915

	
	
	
	--- when you encounter a cost adjustment add a new record for stock_price_history, which adjusts the price from the time of the adjustment to the future
	
	--- insert into stock_price_history new time range
	
	INSERT INTO public.stock_price_history(
	sk, store_sk, nk, storeproduct_sk, supplier_cost, supplier_cost_gst, actual_cost, actual_cost_gst, 
	supplier_rrp, supplier_rrp_gst, calculated_retail_price, calculated_retail_price_gst, override_retail_price, 
	override_retail_price_gst, retail_price, retail_price_gst, created, product_gnm_sk, end_date)
	VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);
	
	--- get last cost record as base line, Adjustments are 

drop function apply_cost_adjustment(in_storeproduct_sk int, 
	in_product_gnm_sk int, 
	in_actual_cost numeric(19,2), 
	in_actual_cost_gst numeric(19,2), 
	startperiod timestamp, 
	endperiod timestamp);

create function apply_cost_adjustment(in_storeproduct_sk int, 
	in_product_gnm_sk int, 
	in_actual_cost numeric(19,2), 
	in_actual_cost_gst numeric(19,2), 
	startperiod timestamp, 
	endperiod timestamp) returns integer as $$
	
	
	-- reset end date on existing records to be recalculated
	update stock_price_history sph set end_date = '2030-01-01' where storeproduct_sk = in_storeproduct_sk and product_gnm_sk = in_product_gnm_sk;
	
	-- insert new record
	INSERT INTO stock_price_history (
		store_sk, nk, storeproduct_sk, supplier_cost, supplier_cost_gst, actual_cost, actual_cost_gst, 
		supplier_rrp, supplier_rrp_gst, calculated_retail_price, calculated_retail_price_gst, override_retail_price, 
		override_retail_price_gst, retail_price, retail_price_gst, created, product_gnm_sk,end_date)
	select sph.store_sk, 'adjustment-' || now() || sph.nk,
		storeproduct_sk, supplier_cost, supplier_cost_gst, in_actual_cost, in_actual_cost_gst, 
		supplier_rrp, supplier_rrp_gst, calculated_retail_price, calculated_retail_price_gst, override_retail_price, 
		override_retail_price_gst, retail_price, retail_price_gst, startperiod, product_gnm_sk, endperiod
	from stock_price_history sph
	where sph.sk in (select max(sk) from stock_price_history 
						where storeproduct_sk = in_storeproduct_sk and product_gnm_sk = in_product_gnm_sk group by storeproduct_sk,product_gnm_sk ) 
		and storeproduct_sk = in_storeproduct_sk and product_gnm_sk = in_product_gnm_sk;

	--- Assumption 1. adjustment extends to max end date
	--- 2. Adjustment is AFTER created date for existing stock_cost_history records
	


--- date ranges for product_gnm_average_cost_history

	update public.stock_price_history set end_date = Z.end_date
	-- select * 
	from 
	(select x.sk,x1.created end_date,x1.product_gnm_sk 
	from (select sk,storeproduct_sk, product_gnm_sk,created, row_number() over (partition by storeproduct_sk,product_gnm_sk order by sh.created,sh.sk) r
				from stock_price_history sh where product_gnm_sk <> 0) x 
	inner join (select sk,storeproduct_sk, product_gnm_sk,created, row_number() over (partition by storeproduct_sk,product_gnm_sk order by sh.created,sh.sk) r
				from stock_price_history sh where product_gnm_sk <> 0) x1
					on x.product_gnm_sk = x1.product_gnm_sk and x.r+1 = x1.r and x.storeproduct_sk = x1.storeproduct_sk
					and x.product_gnm_sk = in_product_gnm_sk and x.storeproduct_sk = in_storeproduct_sk
	order by x.product_gnm_sk) Z 
	where Z.sk = stock_price_history.sk;

	--
	select 1;
$$ language sql;


select apply_cost_adjustment(0,26095,555.00,45.00,'2018-01-01','2030-01-01')


select * from stock_price_history sph where storeproduct_sk = 2076915;
select * from stock_price_history sph where product_gnm_sk = 26095 order by created;

--------------- 
-- create starting position record, which contains 


---- incremental tracking record used for processing stockmovements
drop table if exists store_stockmovement_checkpoint cascade;

CREATE TABLE store_stockmovement_checkpoint (
    sk serial NOT NULL,
    
 	storeproduct_sk integer REFERENCES storeproduct(sk),
 	product_gnm_sk integer references product_gnm(sk),
 	store_sk integer references store(sk),
 	
    adjustmentdate timestamp without time zone not null,
    end_date timestamp without time zone,
    	
    store_stockmovement_sk integer references store_stockmovement(sk),
 	quantity integer,
	prior_avg_cost numeric(19,2),
	adjustment integer,
	running_total integer,

    actual_cost numeric(19,2),
    actual_cost_gst numeric(19,2),
    average_cost numeric(19,2),
    average_cost_gst numeric(19,2),
    
    created timestamp without time zone DEFAULT now(),
    primary key(sk)
);

create index on store_stockmovement_checkpoint (storeproduct_sk,product_gnm_sk,adjustmentdate);

/*
	sk,
	adjustmentdate,
	quantity,
	running_total,
	store_sk,
	product_gnm_sk,
	storeproduct_sk,
	actual_cost,
	actual_cost_gst,
	prior_avg_cost,
	adjustment,
	average_cost,
	average_cost_gst,
	next_sk,
	seq
*/

select * from store_stockmovement_checkpoint smc ;

--- Starting checkpoint for adjustments
insert into store_stockmovement_checkpoint (   
    store_stockmovement_sk,
	adjustmentdate,
	quantity,
	running_total,
	store_sk,
 	product_gnm_sk,
 	storeproduct_sk,
    actual_cost , 
	prior_avg_cost, 
    actual_cost_gst,
	adjustment,
    average_cost, 
    average_cost_gst
    )
	select
		sm.sk,
		sm.adjustmentdate,
		sm.adjustment quantity,
		0 running_total,
		sm.store_sk,
		sm.product_gnm_sk,
		sm.storeproduct_sk,
		sp.actual_cost::numeric(19,2),
		sp.actual_cost::numeric(19,2) as prior_cost,
		sp.actual_cost_gst::numeric(19,2),
		0,
		sp.actual_cost::numeric(19,2) as average_cost,
		sp.actual_cost_gst::numeric(19,2) as average_cost_gst
	from 
		(select sm.sk,row_number() over (partition by sm.storeproduct_sk,sm.product_gnm_sk order by sm.adjustmentdate,sm.sk ) r ,
			lead (sm.sk,1) OVER (PARTITION BY sm.storeproduct_sk,product_gnm_sk ORDER by sm.adjustmentdate)	as next_sk
			from store_stockmovement sm  where sm.adjustment_type_sk in (select sk from adjustment_type a where a.Name in ('Receipt'))
				) seq
		inner join store_stockmovement sm on seq.sk = sm.sk and sm.product_gnm_sk <> 0
		inner join adjustment_type a on a.sk = sm.adjustment_type_sk  and a.Name in ('Receipt')  
		left join  -- get the base price by date
			stock_price_history sp on sp.product_gnm_sk = sm.product_gnm_sk and sp.created <= sm.adjustmentdate and sp.end_date > sm.adjustmentdate
		left join store_stockmovement_checkpoint smc on smc.storeproduct_sk = sm.storeproduct_sk
	where seq.r = 1 and smc.sk is null 	--- and sm.storeproduct_sk = 2076915
	
	


	select smc.sk,
			smc.adjustmentdate,
			smc.quantity,
			smc.running_total,
			smc.store_sk,
			smc.product_gnm_sk,
			smc.storeproduct_sk,
			smc.actual_cost,
			smc.actual_cost_gst,
			smc.prior_avg_cost,
			smc.adjustment,
			smc.average_cost,
			smc.average_cost_gst,
			lead (sm.sk,1) OVER (PARTITION BY sm.storeproduct_sk ORDER by sm.adjustmentdate,sm.sk),
			ROW_NUMBER () OVER (PARTITION BY sm.storeproduct_sk ORDER by sm.adjustmentdate,sm.sk),
			sm.*
	from 
		store_stockmovement_checkpoint smc 
		left join store_stockmovement sm on sm.storeproduct_sk = smc.storeproduct_sk and sm.adjustmentdate >= smc.adjustmentdate 
		where sm.storeproduct_sk = 2076915
		
	select * from store_stockmovement_checkpoint smc where storeproduct_sk = 2076915


select ROW_NUMBER () OVER (
 PARTITION BY j.order_id,j.id,j.product_id
 ORDER BY
 qu.created_date_time
 ),
 qu.queue,
 lag(qu.queue,1) OVER (
 PARTITION BY j.order_id,j.id,j.product_id
 ORDER BY
 qu.created_date_time
 ) prior_queue,
 j.id,j.order_id,j.product_id,j.supplier_id,j.status,j.queue,qu.*
from gnm.job j 
inner join job_queue jq on jq.job_id = j.id 
inner join queue_update qu on qu.id = jq.queue_id
where j.created_date_time > '2017-08-01' 

create function fn_set_initial_movement() returns integer as $$




	select 1;
$$ language sql;




----------- Create view that calcuates averages, save into a table of cogs averages, update end_date ranges for the table
set role scire;
SET search_path = public, pg_catalog;

---------------------------------------------------------------------------
--- use a sequence table but using row number to generate it

-- set role postgres;
drop view if exists view_product_gnm_average_cost_history_checkpoint;

create or replace view view_product_gnm_average_cost_history_checkpoint as
with recursive cogs(
	sk,
	adjustmentdate,
	quantity,
	running_total,
	store_sk,
	product_gnm_sk,
	storeproduct_sk,
	actual_cost,
	actual_cost_gst,
	prior_avg_cost,
	adjustment,
	average_cost,
	average_cost_gst,
	next_sk,
	seq
) as(
	select * from (select smc.store_stockmovement_sk as sk,
			smc.adjustmentdate,
			smc.quantity,
			smc.running_total,
			smc.store_sk,
			smc.product_gnm_sk,
			smc.storeproduct_sk,
			smc.actual_cost,
			smc.actual_cost_gst,
			smc.prior_avg_cost,
			smc.adjustment,
			smc.average_cost,
			smc.average_cost_gst,
			lead (sm.sk,1) OVER (PARTITION BY sm.storeproduct_sk ORDER by sm.adjustmentdate,sm.sk),
			ROW_NUMBER () OVER (PARTITION BY sm.storeproduct_sk ORDER by sm.adjustmentdate,sm.sk) r
	from 
		store_stockmovement_checkpoint smc 
		left join store_stockmovement sm on sm.storeproduct_sk = smc.storeproduct_sk and sm.adjustmentdate >= smc.adjustmentdate ) x
	where x.r = 1
union all
	select
		sm.sk,
		sm.adjustmentdate,
		sm.adjustment as quantity,
		sm.adjustment + c.running_total as running_total,
		sm.store_sk,
		sm.product_gnm_sk,
		sm.storeproduct_sk,
		sp.actual_cost::numeric(19,2),
		sp.actual_cost_gst::numeric(19,2),
		c.average_cost,
		sm.adjustment,
		coalesce( case when sm.adjustment + c.running_total <> 0
			then ((c.average_cost*c.running_total + sp.actual_cost*sm.adjustment) / (sm.adjustment + c.running_total))::numeric(19,2)
			else sp.actual_cost
		end, sp.actual_cost )
		as average_cost,
		coalesce(case when sm.adjustment + c.running_total <> 0
			then ((c.average_cost_gst*c.running_total  + sp.actual_cost_gst*sm.adjustment) / (sm.adjustment + c.running_total))::numeric(19,2) 
			else sp.actual_cost_gst
		end, sp.actual_cost_gst )
		as average_cost_gst,
		seq2.sk next_sk,
		seq2.r
	from cogs c 
	  	inner join store_stockmovement sm on sm.sk = c.next_sk and sm.product_gnm_sk <> 0 
		left join adjustment_type a on a.sk = sm.adjustment_type_sk and a.Name in ('Receipt') 	
		left join stock_price_history sp on sp.product_gnm_sk = sm.product_gnm_sk and sp.created <= sm.adjustmentdate and sp.end_date > sm.adjustmentdate
		left join 
			(select sk,sm.store_sk,sm.product_gnm_sk,row_number() over (partition by sm.store_sk,sm.product_gnm_sk order by sm.adjustmentdate,sm.sk ) r 
			from store_stockmovement sm ) seq2 on  c.seq+1 = seq2.r and seq2.product_gnm_sk = sm.product_gnm_sk and seq2.store_sk = sm.store_sk 
) select  * 
 from cogs;

select vah.storeproduct_sk,vah.product_gnm_sk,
	vah.store_sk,
	vah.adjustmentdate,
	vah.sk,
	vah.quantity,vah.running_total,
	vah.prior_avg_cost,vah.adjustment,
	vah.actual_cost,vah.actual_cost_gst,
	vah.average_cost,vah.average_cost_gst
from view_product_gnm_average_cost_history_checkpoint vah 
left join store_stockmovement_checkpoint ph on ph.store_stockmovement_sk = vah.sk
where 
ph.store_stockmovement_sk is null and vah.adjustmentdate is not null and
vah.storeproduct_sk = 2076915;


select vah.storeproduct_sk,vah.product_gnm_sk,
	vah.store_sk,
	vah.adjustmentdate,
	vah.sk,
	vah.quantity,vah.running_total,
	vah.prior_avg_cost,vah.adjustment,
	vah.actual_cost,vah.actual_cost_gst,
	vah.average_cost,vah.average_cost_gst
from view_product_gnm_average_cost_history_checkpoint vah 
where 
	--ph.store_stockmovement_sk is null and vah.adjustmentdate is not null and
storeproduct_sk = 2076915;



----------- save into a table of cogs averages, update end_date ranges for the table
set role scire;
SET search_path = public, pg_catalog;


-- make sure all product_gnm are not null
update store_stockmovement sm set product_gnm_sk = 0
where product_gnm_sk is null;

insert into product_gnm_average_cost_history (   
 	storeproduct_sk,
 	product_gnm_sk,
 	store_sk,
    adjustmentdate,
    store_stockmovement_sk,
 	quantity, running_total,
	prior_avg_cost, adjustment,

    actual_cost , actual_cost_gst,
    average_cost, average_cost_gst
    )
select 
	vah.storeproduct_sk,
	vah.product_gnm_sk,
	vah.store_sk,
	vah.adjustmentdate,
	vah.sk,
	vah.quantity,vah.running_total,
	vah.prior_avg_cost,vah.adjustment,
	vah.actual_cost,vah.actual_cost_gst,
	vah.average_cost,vah.average_cost_gst
from view_product_gnm_average_cost_history_checkpoint vah 
left join product_gnm_average_cost_history ph on ph.store_stockmovement_sk = vah.sk
where 
	ph.store_stockmovement_sk is null and vah.adjustmentdate is not null --and vah.sk not in (select sk from store_stockmovement);


// -------------------------------------------------------------------------------------------------------

select fn_set_init_stockmovement_preprocess();

-- select * from  store_stockmovement_checkpoint where storeproduct_sk = 2076915;


-- DROP INDEX public.idx_store_stockmovement_takeon_storeproduct_sk;
--- these need to go into migration scripts
CREATE INDEX  ON public.store_stockmovement USING btree(stocktake_item_sk);
CREATE INDEX  ON public.store_stockmovement_checkpoint USING btree(storeproduct_sk);
CREATE INDEX  ON public.store_stockmovement_checkpoint USING btree(store_sk,product_gnm_sk);
CREATE INDEX  ON public.store_stockmovement_checkpoint USING btree(store_sk,product_gnm_sk,adjustmentdate);
CREATE INDEX  ON public.store_stockmovement_checkpoint USING btree(storeproduct_sk,adjustmentdate);

SELECT locktype, relation::regclass,mode, transactionid AS tid,
virtualtransaction AS vtid,pid, granted
FROM pg_catalog.pg_locks l LEFT JOIN pg_catalog.pg_database db
ON db.oid=l.database WHERE (db.datname='gnm_scire' OR db.datname IS NULL)
AND NOT pid = pg_backend_pid();

	select * from (select smc.store_stockmovement_sk as sk,
			smc.adjustmentdate,
			smc.quantity,
			smc.running_total,
			smc.store_sk,
			smc.product_gnm_sk,
			smc.storeproduct_sk,
			smc.actual_cost,
			smc.actual_cost_gst,
			smc.prior_avg_cost,
			smc.adjustment,
			smc.average_cost,
			smc.average_cost_gst,
			lead (sm.sk,1) OVER (PARTITION BY sm.storeproduct_sk ORDER by sm.adjustmentdate,sm.sk) nextsk,
			ROW_NUMBER () OVER (PARTITION BY sm.storeproduct_sk ORDER by sm.adjustmentdate,sm.sk) r
	from 
		store_stockmovement_checkpoint smc 
		left join store_stockmovement sm on sm.storeproduct_sk = smc.storeproduct_sk and sm.adjustmentdate >= smc.adjustmentdate 
		inner join adjustment_type a on a.sk = sm.adjustment_type_sk 
		where a.Name in ('Receipt') ) x	where x.r = 1 and x.nextsk is null;
		


select * 
from stocktake_item si
inner join store_stockmovement sm on si.sk = sm.stocktake_item_sk
inner join storeproduct sp on sp.sk = sm.storeproduct_sk
--- left join stock_price_history sph on sph.storeproduct_sk = sp.sk and sph.created <= sm.adjustmentdate and sph.end_date > sm.adjustmentdate  and sp.product_gnm_sk = 0
left join stock_price_history sph2 on sph2.product_gnm_sk = sp.product_gnm_sk and sph2.created <= sm.adjustmentdate and sph2.end_date > sm.adjustmentdate 
where sm.storeproduct_sk  not in (select storeproduct_sk from store_stockmovement_checkpoint)
        and sm.adjustment_type_sk in (select sk from adjustment_type where name = 'Stocktake Receipt') and sp.product_gnm_sk <> 0
        
        
select * 
from store_stockmovement sm 
inner join adjustment_type a on a.sk = sm.adjustment_type_sk
left join stock_price_history sph2 on sph2.product_gnm_sk = sm.product_gnm_sk and sph2.created <= sm.adjustmentdate and sph2.end_date > sm.adjustmentdate 
left join store_stockmovement_checkpoint smc on smc.storeproduct_sk = sm.storeproduct_sk
where smc.storeproduct_sk  is null
        and a.name in ('Stocktake Receipt') and sm.product_gnm_sk <> 0

        
        
truncate table store_stockmovement_checkpoint;
truncate table product_gnm_average_cost_history;

select fn_set_init_stockmovement_preprocess();

select fn_process_cogs_averages();

select * from  store_stockmovement_checkpoint where storeproduct_sk = 2076915;
select * from  store_stockmovement_checkpoint where store_sk = 45;
select reason, count(*) from store_stockmovement where store_sk = 45 group by reason;

select * from product_gnm_average_cost_history where store_sk = 45;
select * from product_gnm_average_cost_history where store_sk = 45 and storeproduct_sk = 2076915;


select * from  store_stockmovement where storeproduct_sk = 2076915 --and sk = 1185985 
order by adjustmentdate,sk ;

---- populate checkpoint
select
		sm.sk,
		sm.adjustmentdate,
		sm.adjustment quantity,
		0 running_total,
		sm.store_sk,
		sm.product_gnm_sk,
		sm.storeproduct_sk,
		sp.actual_cost::numeric(19,2),
		sp.actual_cost::numeric(19,2) as prior_cost,
		sp.actual_cost_gst::numeric(19,2),
		0,
		sp.actual_cost::numeric(19,2) as average_cost,
		sp.actual_cost_gst::numeric(19,2) as average_cost_gst
	from 
		(select sm.sk,row_number() over (partition by sm.storeproduct_sk,sm.product_gnm_sk order by sm.adjustmentdate,sm.sk ) r ,
			lead (sm.sk,1) OVER (PARTITION BY sm.storeproduct_sk,product_gnm_sk ORDER by sm.adjustmentdate)	as next_sk
			from store_stockmovement sm  where sm.adjustment_type_sk in (select sk from adjustment_type a where a.Name in ('Receipt','Stocktake Receipt'))
				) seq
		inner join store_stockmovement sm on seq.sk = sm.sk and sm.product_gnm_sk <> 0
		inner join adjustment_type a on a.sk = sm.adjustment_type_sk  and a.Name in ('Receipt','Stocktake Receipt')  
		left join  -- get the base price by date
			stock_price_history sp on sp.product_gnm_sk = sm.product_gnm_sk and sp.created <= sm.adjustmentdate and sp.end_date > sm.adjustmentdate
		left join store_stockmovement_checkpoint smc on smc.storeproduct_sk = sm.storeproduct_sk
	where seq.r = 1 and smc.sk is null 	;

-- process using checkpoint
-- return first
	select * from (select smc.store_stockmovement_sk as sk,
			smc.adjustmentdate,
			smc.quantity,
			smc.running_total,
			smc.store_sk,
			smc.product_gnm_sk,
			smc.storeproduct_sk,
			smc.actual_cost,
			smc.actual_cost_gst,
			smc.prior_avg_cost,
			smc.adjustment,
			smc.average_cost,
			smc.average_cost_gst,
			lead (sm.sk,1) OVER (PARTITION BY sm.storeproduct_sk ORDER by sm.adjustmentdate,sm.sk),
			ROW_NUMBER () OVER (PARTITION BY sm.storeproduct_sk ORDER by sm.adjustmentdate,sm.sk) r
	from 
		store_stockmovement_checkpoint smc 
		left join store_stockmovement sm on sm.storeproduct_sk = smc.storeproduct_sk and sm.adjustmentdate >= smc.adjustmentdate 
		inner join adjustment_type a on a.sk = sm.adjustment_type_sk  
	where sm.storeproduct_sk = 2076915
		 ) x	where x.r = 1 and storeproduct_sk = 2076915;

--- return 
select * from stock_price_history where product_gnm_sk = 26095;
delete from stock_price_history where sk in (1934876,1934875)

select vah.storeproduct_sk,vah.product_gnm_sk,
	vah.store_sk,
	vah.adjustmentdate,
	vah.sk,
	vah.quantity,vah.running_total,
	vah.prior_avg_cost,vah.adjustment,
	vah.actual_cost,vah.actual_cost_gst,
	vah.average_cost,vah.average_cost_gst,
	*
from view_product_gnm_average_cost_history_checkpoint vah 
--left join product_gnm_average_cost_history ph on ph.store_stockmovement_sk = vah.sk
	left join store_stockmovement sm on sm.storeproduct_sk = vah.storeproduct_sk and vah.adjustmentdate = sm.adjustmentdate
where 
	  vah.storeproduct_sk = 2076915;


select * from 
	 (	select * from (select smc.store_stockmovement_sk as sk,
			smc.adjustmentdate,
			smc.quantity,
			smc.running_total,
			smc.store_sk,
			smc.product_gnm_sk,
			smc.storeproduct_sk,
			smc.actual_cost,
			smc.actual_cost_gst,
			smc.prior_avg_cost,
			smc.adjustment,
			smc.average_cost,
			smc.average_cost_gst,
			lead (sm.sk,1) OVER (PARTITION BY sm.storeproduct_sk ORDER by sm.adjustmentdate,sm.sk) next_sk,
			ROW_NUMBER () OVER (PARTITION BY sm.storeproduct_sk ORDER by sm.adjustmentdate,sm.sk) r
	from 
		store_stockmovement_checkpoint smc 
		left join store_stockmovement sm on sm.storeproduct_sk = smc.storeproduct_sk and sm.adjustmentdate >= smc.adjustmentdate 
		inner join adjustment_type a on a.sk = sm.adjustment_type_sk 
		 ) x	where x.r = 1 and storeproduct_sk = 2076915 ) c 
	  	inner join store_stockmovement sm on sm.sk = c.next_sk and sm.product_gnm_sk <> 0 
		inner join adjustment_type a on a.sk = sm.adjustment_type_sk and a.Name in ('Receipt') 	
		left join stock_price_history sp on sp.product_gnm_sk = sm.product_gnm_sk and sp.created <= sm.adjustmentdate and sp.end_date > sm.adjustmentdate
		
		
		select 			
			lead (sm.sk,1) OVER (PARTITION BY sm.storeproduct_sk ORDER by sm.adjustmentdate,sm.sk) next_sk,
			ROW_NUMBER () OVER (PARTITION BY sm.storeproduct_sk ORDER by sm.adjustmentdate,sm.sk) r,
		* from  store_stockmovement sm
		inner join adjustment_type a on a.sk = sm.adjustment_type_sk and a.Name in ('Receipt') 	
		left join stock_price_history sp on sp.product_gnm_sk = sm.product_gnm_sk and sp.created <= sm.adjustmentdate and sp.end_date > sm.adjustmentdate
		left join product_gnm_average_cost_history pga on pga.store_stockmovement_sk = sm.sk
		where sm.product_gnm_sk <> 0 and  sm.storeproduct_sk = 2076915;


----------- Create view that calcuates averages, save into a table of cogs averages, update end_date ranges for the table
set role scire;
SET search_path = public, pg_catalog;

-- set role postgres;
drop view if exists view_product_gnm_average_cost_history_checkpoint;

-- create or replace view view_product_gnm_average_cost_history_checkpoint as
with recursive cogs(
	sk,
	adjustmentdate,
	quantity,
	running_total,
	store_sk,
	product_gnm_sk,
	storeproduct_sk,
	actual_cost,
	actual_cost_gst,
	prior_avg_cost,
	adjustment,
	average_cost,
	average_cost_gst,
	next_sk,
	seq
) as(
	select * from (select smc.store_stockmovement_sk as sk,
			smc.adjustmentdate,
			smc.quantity,
			smc.running_total,
			smc.store_sk,
			smc.product_gnm_sk,
			smc.storeproduct_sk,
			smc.actual_cost,
			smc.actual_cost_gst,
			smc.prior_avg_cost,
			smc.adjustment,
			smc.average_cost,
			smc.average_cost_gst,
			lead (sm.sk,1) OVER (PARTITION BY sm.storeproduct_sk ORDER by sm.adjustmentdate,sm.sk),
			ROW_NUMBER () OVER (PARTITION BY sm.storeproduct_sk ORDER by sm.adjustmentdate,sm.sk) r
	from 
		store_stockmovement_checkpoint smc 
		left join store_stockmovement sm on sm.storeproduct_sk = smc.storeproduct_sk and sm.adjustmentdate >= smc.adjustmentdate 
		inner join adjustment_type a on a.sk = sm.adjustment_type_sk ) x	where x.r = 1
union all
	select
		sm.sk,
		sm.adjustmentdate,
		sm.adjustment as quantity,
		sm.adjustment + c.running_total as running_total,
		sm.store_sk,
		sm.product_gnm_sk,
		sm.storeproduct_sk,
		sp.actual_cost::numeric(19,2),
		sp.actual_cost_gst::numeric(19,2),
		c.average_cost,
		sm.adjustment,
		coalesce( case when sm.adjustment + c.running_total <> 0
			then ((c.average_cost*c.running_total + sp.actual_cost*sm.adjustment) / (sm.adjustment + c.running_total))::numeric(19,2)
			else sp.actual_cost
		end, sp.actual_cost )
		as average_cost,
		coalesce(case when sm.adjustment + c.running_total <> 0
			then ((c.average_cost_gst*c.running_total  + sp.actual_cost_gst*sm.adjustment) / (sm.adjustment + c.running_total))::numeric(19,2) 
			else sp.actual_cost_gst
		end, sp.actual_cost_gst )
		as average_cost_gst,
		seq2.sk next_sk,
		seq2.r  
	from cogs c 
	  	inner join store_stockmovement sm on sm.sk = c.next_sk 
		inner join adjustment_type a on a.sk = sm.adjustment_type_sk and a.Name in ('Receipt') 	
		left join stock_price_history sp on sp.product_gnm_sk = sm.product_gnm_sk and sp.created <= sm.adjustmentdate and sp.end_date > sm.adjustmentdate
		left join 
			(select sk,sm.storeproduct_sk,row_number() over (partition by sm.storeproduct_sk order by sm.adjustmentdate,sm.sk ) r 
			from store_stockmovement sm ) seq2 on  c.seq+1 = seq2.r and seq2.storeproduct_sk = sm.storeproduct_sk
	where  sm.product_gnm_sk <> 0 	
) select  * 
 from cogs where storeproduct_sk = 2076915 ;


select * from store_stockmovement_checkpoint where storeproduct_sk = 2076915 


select sk,sm.adjustmentdate,sm.storeproduct_sk,row_number() over (partition by sm.storeproduct_sk order by sm.adjustmentdate,sm.sk ) r 
from store_stockmovement sm  
where storeproduct_sk = 2076915 and sm.adjustment_type_sk = 1

			
			
				select * from (select smc.store_stockmovement_sk as sk,
			smc.adjustmentdate,
			smc.quantity,
			smc.running_total,
			smc.store_sk,
			smc.product_gnm_sk,
			smc.storeproduct_sk,
			smc.actual_cost,
			smc.actual_cost_gst,
			smc.prior_avg_cost,
			smc.adjustment,
			smc.average_cost,
			smc.average_cost_gst,
			lead (sm.sk,1) OVER (PARTITION BY sm.storeproduct_sk ORDER by sm.adjustmentdate,sm.sk),
			ROW_NUMBER () OVER (PARTITION BY sm.storeproduct_sk ORDER by sm.adjustmentdate,sm.sk) r
	from 
		store_stockmovement_checkpoint smc 
		left join store_stockmovement sm on sm.storeproduct_sk = smc.storeproduct_sk and sm.adjustmentdate >= smc.adjustmentdate 
		inner join adjustment_type a on a.sk = sm.adjustment_type_sk ) x	where x.r = 1 and storeproduct_sk = 2076915 ;
		
		
1185985
1,185,986


select * from store_stockmovement sm where sk in (1185985,1185986,)

select sm.store_stockmovement_sk as sk,
			smc.adjustmentdate,
			smc.quantity,
			smc.running_total,
			smc.store_sk,
			smc.product_gnm_sk,
			smc.storeproduct_sk,
			smc.actual_cost,
			smc.actual_cost_gst,
			smc.prior_avg_cost,
			smc.adjustment,
			smc.average_cost,
			smc.average_cost_gst,
			lead (sm.sk,1) OVER (PARTITION BY sm.storeproduct_sk ORDER by sm.adjustmentdate,sm.sk) next_sk,
			ROW_NUMBER () OVER (PARTITION BY sm.storeproduct_sk ORDER by sm.adjustmentdate,sm.sk) r
	from 
		store_stockmovement_checkpoint smc 
		left join store_stockmovement sm on sm.storeproduct_sk = smc.storeproduct_sk and sm.adjustmentdate >= smc.adjustmentdate 
		inner join adjustment_type a on a.sk = sm.adjustment_type_sk
		where sm.storeproduct_sk = 2076915 
		
			select * from (select smc.store_stockmovement_sk as sk,
			smc.adjustmentdate,
			smc.quantity,
			smc.running_total,
			smc.store_sk,
			smc.product_gnm_sk,
			smc.storeproduct_sk,
			smc.actual_cost,
			smc.actual_cost_gst,
			smc.prior_avg_cost,
			smc.adjustment,
			smc.average_cost,
			smc.average_cost_gst,
			lead (sm.sk,1) OVER (PARTITION BY sm.storeproduct_sk ORDER by sm.adjustmentdate,sm.sk),
			ROW_NUMBER () OVER (PARTITION BY sm.storeproduct_sk ORDER by sm.adjustmentdate,sm.sk) r
	from 
		store_stockmovement_checkpoint smc 
		left join store_stockmovement sm on sm.storeproduct_sk = smc.storeproduct_sk and sm.adjustmentdate >= smc.adjustmentdate 
		inner join adjustment_type a on a.sk = sm.adjustment_type_sk ) x	where x.r = 1 and storeproduct_sk = 2076915
		
		
		
		
		-- create or replace view view_product_gnm_average_cost_history_checkpoint as
with recursive cogs(
	sk,
	adjustmentdate,
	storeproduct_sk,
	next_sk
) as(
	select 	sk,
	adjustmentdate,
	storeproduct_sk,
	next_sk
	from (select smc.store_stockmovement_sk as sk,
			smc.adjustmentdate,
			smc.storeproduct_sk,
			lead (sm.sk,1) OVER (PARTITION BY sm.storeproduct_sk ORDER by sm.adjustmentdate,sm.sk) next_sk,
			row_number() over (PARTITION BY sm.storeproduct_sk ORDER by sm.adjustmentdate,sm.sk) r
	from 
		store_stockmovement_checkpoint smc 
		left join store_stockmovement sm on sm.storeproduct_sk = smc.storeproduct_sk and sm.adjustmentdate >= smc.adjustmentdate 
		inner join adjustment_type a on a.sk = sm.adjustment_type_sk ) x where x.r = 1 --and storeproduct_sk = 2076915 

union all
	select
		sm.sk,
		sm.adjustmentdate,
		sm.storeproduct_sk,
		seq.next_sk 
	from cogs c 
	  	inner join store_stockmovement sm on sm.sk = c.next_sk  	
--		inner join (select sm.sk, lead(sm.sk,1) over (partition by storeproduct_sk order by adjustmentdate,sm.sk ) next_sk 
--				from store_stockmovement sm inner join adjustment_type a on a.sk = sm.adjustment_type_sk and a.Name in ('Receipt') where adjustment_type_sk = 1  ) seq on seq.sk = sm.sk
		inner join (select sm.sk, lead(sm.sk,1) over (partition by storeproduct_sk order by adjustmentdate,sm.sk ) next_sk 
				from store_stockmovement sm inner join adjustment_type a on a.sk = sm.adjustment_type_sk and a.Name in ('Receipt') where sm.product_gnm_sk <> 0  ) seq on seq.sk = sm.sk
	where  sm.product_gnm_sk <> 0 	
) select  * 
 from cogs where storeproduct_sk = 2076915 ;


select * from view_product_gnm_average_cost_history_checkpoint where storeproduct_sk = 2076915 ;


	from cogs c 
	  	inner join store_stockmovement sm on sm.sk = c.next_sk 
		left join stock_price_history sp on sp.product_gnm_sk = sm.product_gnm_sk and sp.created <= sm.adjustmentdate and sp.end_date > sm.adjustmentdate
		inner join (select sm.sk, lead(sm.sk,1) over (partition by storeproduct_sk order by adjustmentdate,sm.sk ) next_sk 
				from store_stockmovement sm inner join adjustment_type a on a.sk = sm.adjustment_type_sk and a.Name in ('Receipt') where sm.product_gnm_sk <> 0  ) seq on seq.sk = sm.sk
				

select sk,sm.adjustmentdate,sm.storeproduct_sk,row_number() over (partition by sm.storeproduct_sk order by sm.adjustmentdate,sm.sk ) r ,product_gnm_sk
			from store_stockmovement sm
			 where storeproduct_sk = 2076915 and sm.adjustment_type_sk = 1;
 
select * from view_product_gnm_average_cost_history_checkpoint where storeproduct_sk = 2076915 ;

	select 	sk,
	adjustmentdate,
	quantity,
	running_total,
	store_sk,
	product_gnm_sk,
	storeproduct_sk,
	actual_cost,
	actual_cost_gst,
	prior_avg_cost,
	adjustment,
	average_cost,
	average_cost_gst,
	next_sk
	from (select smc.store_stockmovement_sk as sk,
			smc.adjustmentdate,
			smc.quantity,
			smc.running_total,
			smc.store_sk,
			smc.product_gnm_sk,
			smc.storeproduct_sk,
			smc.actual_cost,
			smc.actual_cost_gst,
			smc.prior_avg_cost,
			smc.adjustment,
			smc.average_cost,
			smc.average_cost_gst,
			lead (sm.sk,1) OVER (PARTITION BY sm.storeproduct_sk ORDER by sm.adjustmentdate,sm.sk) next_sk,
			row_number() over (PARTITION BY sm.storeproduct_sk ORDER by sm.adjustmentdate,sm.sk) r
	from 
		store_stockmovement_checkpoint smc 
		left join store_stockmovement sm on sm.storeproduct_sk = smc.storeproduct_sk and sm.adjustmentdate >= smc.adjustmentdate 
		inner join adjustment_type a on a.sk = sm.adjustment_type_sk ) x where x.r = 1 and   storeproduct_sk = 2076915 ;
		


select * from product_gnm where distributor <> 0;


insert into store_stockmovement_checkpoint (  
   store_stockmovement_sk,
    adjustmentdate,
    quantity,
    running_total,
    store_sk,
    product_gnm_sk,
    storeproduct_sk,
   actual_cost , 
    prior_avg_cost,
   actual_cost_gst,
    adjustment,
   average_cost,
   average_cost_gst
   )
    select
        sm.sk,
        sm.adjustmentdate,
        sm.adjustment quantity,
        0 running_total,
        sm.store_sk,
        sm.product_gnm_sk,
        sm.storeproduct_sk,
        sp.actual_cost::numeric(19,2),
        sp.actual_cost::numeric(19,2) as prior_cost,
        sp.actual_cost_gst::numeric(19,2),
        sm.adjustment,
        sp.actual_cost::numeric(19,2) as average_cost,
        sp.actual_cost_gst::numeric(19,2) as average_cost_gst
    from 
        (select sm.sk,row_number() over (partition by sm.storeproduct_sk,sm.product_gnm_sk order by sm.adjustmentdate,sm.sk ) r ,
            lead (sm.sk,1) OVER (PARTITION BY sm.storeproduct_sk,product_gnm_sk ORDER by sm.adjustmentdate)    as next_sk
            from store_stockmovement sm  where sm.adjustment_type_sk in (select sk from adjustment_type a where a.Name in ('Stocktake Receipt'))
                ) seq
        inner join store_stockmovement sm on seq.sk = sm.sk and sm.product_gnm_sk <> 0
        inner join adjustment_type a on a.sk = sm.adjustment_type_sk  and a.Name in ('Stocktake Receipt')  
        left join  -- get the base price by date
            stock_price_history sp on sp.product_gnm_sk = sm.product_gnm_sk and sp.created <= sm.adjustmentdate and sp.end_date > sm.adjustmentdate
        left join store_stockmovement_checkpoint smc on smc.storeproduct_sk = sm.storeproduct_sk and smc.adjustmentdate < sm.adjustmentdate
    where seq.r = 1 and smc.sk is null     ;


drop view if exists view_product_gnm_average_cost_history_checkpoint;

create or replace view view_product_gnm_average_cost_history_checkpoint as
with recursive cogs(
	sk,
	adjustmentdate,
	quantity,
	running_total,
	store_sk,
	product_gnm_sk,
	storeproduct_sk,
	actual_cost,
	actual_cost_gst,
	prior_avg_cost,
	adjustment,
	average_cost,
	average_cost_gst,
	next_sk
) as(
	select sk,
	adjustmentdate,
	quantity,
	running_total,
	store_sk,
	product_gnm_sk,
	storeproduct_sk,
	actual_cost,
	actual_cost_gst,
	prior_avg_cost,
	adjustment,
	average_cost,
	average_cost_gst,
	next_sk
	from (select
			smc.store_stockmovement_sk as sk,
			smc.adjustmentdate,
			smc.quantity,
			smc.running_total,
			smc.store_sk,
			smc.product_gnm_sk,
			smc.storeproduct_sk,
			smc.actual_cost,
			smc.actual_cost_gst,
			smc.prior_avg_cost,
			smc.adjustment,
			smc.average_cost,
			smc.average_cost_gst,
			lead (sm.sk,1) OVER (PARTITION BY sm.storeproduct_sk ORDER by sm.adjustmentdate,sm.sk) next_sk,
			row_number() over (PARTITION BY sm.storeproduct_sk ORDER by sm.adjustmentdate,sm.sk) r
	from 
		store_stockmovement_checkpoint smc 
		inner join (select max(sk) max_sk from store_stockmovement_checkpoint group by storeproduct_sk) smc2 on smc2.max_sk = smc.sk
		left join store_stockmovement sm on sm.storeproduct_sk = smc.storeproduct_sk and sm.adjustmentdate >= smc.adjustmentdate 
		inner join adjustment_type a on a.sk = sm.adjustment_type_sk where a.Name in ('Receipt') or (smc.store_stockmovement_sk = sm.sk)) x where x.r = 1 --and storeproduct_sk = 2076915 
union all
	select
		sm.sk,
		sm.adjustmentdate,
		sm.adjustment as quantity,
		sm.adjustment + c.running_total as running_total,
		sm.store_sk,
		sm.product_gnm_sk,
		sm.storeproduct_sk,
		sp.actual_cost::numeric(19,2),
		sp.actual_cost_gst::numeric(19,2),
		c.average_cost,
		sm.adjustment,
		coalesce( case when sm.adjustment + (c.adjustment + c.running_total) <> 0
			then ((c.average_cost*(c.adjustment + c.running_total) + sp.actual_cost*sm.adjustment) / (sm.adjustment + (c.adjustment + c.running_total)))::numeric(19,2)
			else sp.actual_cost
		end, sp.actual_cost )
		as average_cost,
		coalesce(case when sm.adjustment + (c.adjustment + c.running_total) <> 0
			then ((c.average_cost_gst*(c.adjustment + c.running_total)  + sp.actual_cost_gst*sm.adjustment) / (sm.adjustment + (c.adjustment + c.running_total)))::numeric(19,2) 
			else sp.actual_cost_gst
		end, sp.actual_cost_gst ) as average_cost_gst,
		seq.next_sk 
	from cogs c 
	  	inner join store_stockmovement sm on sm.sk = c.next_sk  	
	  	left join stock_price_history sp on sp.product_gnm_sk = sm.product_gnm_sk and sp.created <= sm.adjustmentdate and sp.end_date > sm.adjustmentdate
		inner join (select sm.sk, lead(sm.sk,1) over (partition by storeproduct_sk order by adjustmentdate,sm.sk ) next_sk 
				from store_stockmovement sm inner join adjustment_type a on a.sk = sm.adjustment_type_sk and a.Name in ('Receipt') where sm.product_gnm_sk <> 0  ) seq on seq.sk = sm.sk
	where  sm.product_gnm_sk <> 0 	
) select  *  from cogs



-----------------------------------------------------------

-- select fn_apply_cost_adjustment(2076915,26095,333.00,45.00,'2018-01-01','2030-01-01');
--  select * from store_stockmovement_checkpoint where storeproduct_sk = 2076915 order by sk ;

-- select * from stock_price_history sph where storeproduct_sk = 2076915;
-- select * from stock_price_history sph where product_gnm_sk = 26095 order by created;

select fn_apply_cost_adjustment(2076915,26095,555.00,45.00,'2018-01-01','2030-01-01');


alter table product_gnm drop column supplier_id;

alter table product_gnm add column supplier_sk integer;

select * from supplier;


           SELECT product_id as productid, p."type", 
            created_date_time::TimeSTAMP without time zone as datecreated, 
            modified_date_time::TimeSTAMP without time zone as modified,
            p."name",  p.description, p.supplier_sku as internal_sku, 
            0 as distributor, p.apn, p.brandname as brand_name, p.consignment::boolean, p.supplier_name, s.sk as supplier_sk
            FROM stage.today_gnm_db_product p
              inner join public.supplier s on s.name = p.supplier_name
              
         
              select * from product_gnm p;


-----------------------
--- conf files to replace functions
--- fn_set_init_stockmovement_preprocess
--- fn_process_cogs_averages

              select 
                vah.storeproduct_sk,
                vah.product_gnm_sk,
                vah.store_sk,
                vah.adjustmentdate,
                vah.sk as store_stockmovement_sk,
                vah.quantity,vah.running_total,
                vah.prior_avg_cost,vah.adjustment,
                vah.actual_cost,
                vah.actual_cost_gst,
                vah.average_cost,
                vah.average_cost_gst
              from view_product_gnm_average_cost_history_checkpoint vah 
              left join product_gnm_average_cost_history ph on ph.store_stockmovement_sk = vah.sk
              where 
                ph.store_stockmovement_sk is null and vah.adjustmentdate is not null
                
                
-------------------------------------------

                
select 
  stock_adjust_value_id,
  transact_ref, 
  stock_adjust_value_entry_id, 
  save.store_product_sk, 
  adjusted_average_cost_excl_gst,
  comment,
  current_average_cost_excl_gst, 
  current_average_cost_gst, 
  current_average_cost_incl_gst,
  modified_user,
  modified_date_time
from today_stock_adjust_value
                
CREATE tab
  sav.id as stock_adjust_value_id, sav.transact_ref, 
  save.id as stock_adjust_value_entry_id, save.store_product_sk, save.adjusted_average_cost_excl_gst, save.comment,
  save.current_average_cost_excl_gst, save.current_average_cost_gst, save.current_average_cost_incl_gst,
  sav.modified_user, sav.modified_date_time
  
                
                
select 
  sav.id as stock_adjust_value_id, sav.transact_ref, 
  save.id as stock_adjust_value_entry_id, save.store_product_sk, save.adjusted_average_cost_excl_gst, save.comment,
  save.current_average_cost_excl_gst, save.current_average_cost_gst, save.current_average_cost_incl_gst,
  sav.modified_user, sav.modified_date_time
from stock_adjust_value_entry save
join stock_adjust_value sav on sav.id = save.stock_adjust_value_id f
order by sav.modified_date_time, sav.id 
                
CREATE tab
  sav.id as stock_adjust_value_id, sav.transact_ref, 
  save.id as stock_adjust_value_entry_id, save.store_product_sk, save.adjusted_average_cost_excl_gst, save.comment,
  save.current_average_cost_excl_gst, save.current_average_cost_gst, save.current_average_cost_incl_gst,
  sav.modified_user, sav.modified_date_time

set role scire;
SET search_path = public, pg_catalog;


drop table stock_value_adjustments;

create table stock_value_adjustments (
	sk serial NOT NULL,
	stock_adjust_value_id bigint,
	nk varchar(150),
	transact_ref varchar(150),
	-- 
	stock_adjust_value_entry_id bigint,
	store_product_sk bigint,
	adjusted_average_cost_excl_gst numeric(19,2),
	total_stock_on_hand varchar(150),
	comment text,
	current_average_cost_excl_gst numeric(19,2),
	current_average_cost_gst numeric(19,2),
	current_average_cost_incl_gst numeric(19,2),
	modified_user varchar(150),
	modified_date_time  timestamp without time zone,
    
    created timestamp without time zone DEFAULT now(),
    primary key(sk)
);

alter table stock_value_adjustments add column transact_ref varchar(150);
alter table stock_value_adjustments add column total_stock_on_hand varchar(150);

select * from stock_value_adjustments;

alter table stock_value_adjustments add column store_sk bigint REFERENCES store(sk) ;

select * from "store"


---------------------------------------------------------------------
            select 
                sav.stock_adjust_value_id,
                sav.transact_ref as nk, 
                sav.stock_adjust_value_entry_id, 
                sav.store_product_sk, 
                sav.adjusted_average_cost_excl_gst,
                sav.comment,
                sav.current_average_cost_excl_gst, 
                sav.current_average_cost_gst, 
                sav.current_average_cost_incl_gst,
                sav.modified_user,
                sav.modified_date_time 
              from stage.today_gnm_db_stock_adjust_value  sav 
                left join stock_value_adjustments va on va.nk = sav.transact_ref   
              where	
              	va.sk is null;

            select 
*
              from stage.today_gnm_db_stock_adjust_value  sav 
                
fn_apply_cost_adjustment

select * from stage.gnm_db_stock_adjust_value;

select stage.generate_view_for_table('gnm_db_stock_adjust_value');

select * from  stock_value_adjustments sva 
left join storeproduct sp on sp.sk = sva.store_product_sk
left join product_gnm pg on pg.sk = sp.product_gnm_sk;

 fn_apply_cost_adjustment(in_storeproduct_sk int, 
	in_product_gnm_sk int, 
	in_actual_cost numeric(19,2), 
	in_actual_cost_gst numeric(19,2), 
	startperiod timestamp without time zone, 
	endperiod timestamp without time zone)

select fn_apply_cost_adjustment(2076915,26095,333.00,45.00,'2018-01-01','2030-01-01');


select fn_apply_cost_adjustment(sp.sk,pg.sk,sva.adjusted_average_cost_excl_gst,sva.adjusted_average_cost_excl_gst*0.1,
sva.modified_date_time,'2030-01-01')
from  stock_value_adjustments sva 
left join storeproduct sp on sp.sk = sva.store_product_sk
left join product_gnm pg on pg.sk = sp.product_gnm_sk;

select sp.sk,pg.sk,sva.adjusted_average_cost_excl_gst,sva.adjusted_average_cost_excl_gst*0.1,sva.modified_date_time,'2030-01-01'
from  stock_value_adjustments sva 
left join storeproduct sp on sp.sk = sva.store_product_sk
left join product_gnm pg on pg.sk = sp.product_gnm_sk;

select * from store_stockmovement_checkpoint spc where storeproduct_sk = 1847386;

select * from store_stockmovement_checkpoint;


              select 
                  ach.store_stockmovement_sk,
                  ach.storeproduct_sk,
                  ach.product_gnm_sk,
                  ach.store_sk,
                  ach.adjustmentdate,
                  ach.quantity,
                  ach.running_total,
                  ach.prior_avg_cost, 
                  ach.adjustment,
                  ach.actual_cost,
                  ach.actual_cost_gst,
                  ach.average_cost,
                  ach.average_cost_gst
              from product_gnm_average_cost_history ach 
              inner join     
                      (select storeproduct_sk,max(adjustmentdate) max_adjustmentdate from product_gnm_average_cost_history group by storeproduct_sk) x 
                        on x.storeproduct_sk = ach.storeproduct_sk and ach.adjustmentdate = x.max_adjustmentdate
              left join store_stockmovement_checkpoint smc on smc.store_stockmovement_sk = ach.store_stockmovement_sk 
              where smc.sk is null
              
      
              
              
                          select sk, coalesce(lead(adjustmentdate)
            over (partition by store_sk,product_gnm_sk order by sh.adjustmentdate,sh.sk),'2030-01-01'::timestamp) as end_date
            from store_stockmovement_checkpoint sh where product_gnm_sk <> 0
            
                      select 
                sav.stock_adjust_value_id,
                sav.stock_adjust_value_entry_id || '-nk-' || sav.transact_ref as nk, 
                sav.transact_ref,
                sav.stock_adjust_value_entry_id, 
                sav.store_product_sk, 
                sav.adjusted_average_cost_excl_gst,
                sav.comment,
                sav.current_average_cost_excl_gst, 
                sav.current_average_cost_gst, 
                sav.current_average_cost_incl_gst,
                sav.modified_user,
                sav.modified_date_time 
              from stage.today_gnm_db_stock_adjust_value  sav 
                left join stock_value_adjustments va on va.nk = sav.transact_ref   
              where	
              	va.sk is null              

              	select * from 
              	
              	
        select stage.generate_view_for_table ('gnm_db_stock_adjust_value') ;
        

select * from stock_value_adjustments;





---------------------------------------------------------------------------------------------------------------------------


              select 
                vah.storeproduct_sk,
                vah.product_gnm_sk,
                vah.store_sk,
                vah.adjustmentdate,
                vah.sk as store_stockmovement_sk,
                vah.quantity,vah.running_total,
                vah.prior_avg_cost,vah.adjustment,
                vah.actual_cost,
                vah.actual_cost_gst,
                vah.average_cost,
                vah.average_cost_gst
              from view_product_gnm_average_cost_history_checkpoint vah 
              left join product_gnm_average_cost_history ph on ph.store_stockmovement_sk = vah.sk
              where 
                ph.store_stockmovement_sk is null and vah.adjustmentdate is not null

select sph.*,sp.* from storeproduct sp
left join stock_price_history sph on sph.product_gnm_sk = sp.product_gnm_sk
where 
	sp.sk = 2079613 order by sph.created, sph.end_date;

select * from store_stockmovement where storeproduct_sk =  2079613 order by adjustmentdate, sk ;
select sum(adjustment) from store_stockmovement where adjustment_type_sk = 322 and storeproduct_sk =  2079613;
select * from product_gnm_average_cost_history where storeproduct_sk =  2079613 order by sk ;
select * from store_stockmovement_checkpoint  where storeproduct_sk =  2079613 order by adjustmentdate, sk ;

select * from stock_value_adjustments


1178406	3	87.99	3	0	87.99	8.00	87.99	8.00
1254739	1	87.99	1	1	110.00	10.00	93.49	8.50
1254742	2	93.49	2	3	110.00	10.00	101.75	9.25
1254748	2	101.75	2	5	110.00	10.00	104.11	9.46
1254754	2	104.11	2	7	110.00	10.00	105.42	9.58
1254768	2	105.42	2	9	110.00	10.00	106.25	9.66
1254772	1	106.25	1	10	110.00	10.00	106.56	9.69
1254775	2	106.56	2	12	110.00	10.00	107.09	9.74

3
1
2
2
2
2
1
2



select * from product_gnm_average_cost_history where storeproduct_sk =  2079613 order by sk ;
select * from store_stockmovement_checkpoint  where storeproduct_sk =  2079613 order by adjustmentdate, sk ;

	select
		sm.sk,
		sm.adjustmentdate,
		sm.adjustment quantity,
		0 running_total,
		sm.store_sk,
		sm.product_gnm_sk,
		sm.storeproduct_sk,
		sp.actual_cost::numeric(19,2),
		sp.actual_cost::numeric(19,2) as prior_cost,
		sp.actual_cost_gst::numeric(19,2),
		sm.adjustment,
		sp.actual_cost::numeric(19,2) as average_cost,
		sp.actual_cost_gst::numeric(19,2) as average_cost_gst
	from 
		(select sm.sk,row_number() over (partition by sm.storeproduct_sk,sm.product_gnm_sk order by sm.adjustmentdate,sm.sk ) r ,
			lead (sm.sk,1) OVER (PARTITION BY sm.storeproduct_sk,product_gnm_sk ORDER by sm.adjustmentdate)	as next_sk
			from store_stockmovement sm  where sm.adjustment_type_sk in (select sk from adjustment_type a where a.Name in ('Stocktake Receipt'))
				) seq
		inner join store_stockmovement sm on seq.sk = sm.sk and sm.product_gnm_sk <> 0
		inner join adjustment_type a on a.sk = sm.adjustment_type_sk  and a.Name in ('Stocktake Receipt')  
		left join  -- get the base price by date
			stock_price_history sp on sp.product_gnm_sk = sm.product_gnm_sk and sp.created <= sm.adjustmentdate and sp.end_date > sm.adjustmentdate
		left join store_stockmovement_checkpoint smc on smc.storeproduct_sk = sm.storeproduct_sk and smc.adjustmentdate < sm.adjustmentdate
	where seq.r = 1 and smc.sk is null 	and  sm.storeproduct_sk =  2079613 order by sm.adjustmentdate, sm.sk ;



delete from store_stockmovement_checkpoint where storeproduct_sk = 2079613;
delete from product_gnm_average_cost_history where storeproduct_sk = 2079613;


select vah.storeproduct_sk,vah.product_gnm_sk,
	vah.store_sk,
	vah.adjustmentdate,
	vah.sk,
	vah.quantity,vah.running_total,
	vah.prior_avg_cost,vah.adjustment,
	vah.actual_cost,vah.actual_cost_gst,
	vah.average_cost,vah.average_cost_gst
from view_product_gnm_average_cost_history_checkpoint vah 
left join product_gnm_average_cost_history ph on ph.store_stockmovement_sk = vah.sk
where vah.storeproduct_sk = 2079613;


	select
	x.r,
	sk,
	adjustmentdate,
	quantity,
	running_total,
	store_sk,
	product_gnm_sk,
	storeproduct_sk,
	actual_cost,
	actual_cost_gst,
	prior_avg_cost,
	adjustment,
	average_cost,
	average_cost_gst,
	next_sk
	from (select
			smc.store_stockmovement_sk as sk,
			smc.adjustmentdate,
			smc.quantity,
			smc.running_total,
			smc.store_sk,
			smc.product_gnm_sk,
			smc.storeproduct_sk,
			smc.actual_cost,
			smc.actual_cost_gst,
			smc.prior_avg_cost,
			smc.adjustment,
			smc.average_cost,
			smc.average_cost_gst,
			lead (sm.sk,1) OVER (PARTITION BY sm.storeproduct_sk ORDER by sm.adjustmentdate,sm.sk) next_sk,
			row_number() over (PARTITION BY sm.storeproduct_sk ORDER by sm.adjustmentdate,sm.sk) r
	from 
		store_stockmovement_checkpoint smc 
		inner join (select max(sk) max_sk from store_stockmovement_checkpoint group by storeproduct_sk) smc2 on smc2.max_sk = smc.sk
		left join store_stockmovement sm on sm.storeproduct_sk = smc.storeproduct_sk and sm.adjustmentdate >= smc.adjustmentdate 
		inner join adjustment_type a on a.sk = sm.adjustment_type_sk 
		where a.Name in ('Receipt') or (smc.store_stockmovement_sk = sm.sk)) x where -- x.r = 1 and 
		storeproduct_sk = 2076915 
	order by x.r
		
	
	
	
	
		------------- pickup the last average for processing
	insert into store_stockmovement_checkpoint (            
		    storeproduct_sk,
		    product_gnm_sk,
		    store_sk,
		   adjustmentdate,
		   store_stockmovement_sk,
		    quantity,
		    running_total,
		    prior_avg_cost, 
		    adjustment,
		   actual_cost ,
		   actual_cost_gst,
		   average_cost,
		   average_cost_gst)	
	select 
	    	ach.storeproduct_sk,
		    ach.product_gnm_sk,
		    ach.store_sk,
		   ach.adjustmentdate,
		   ach.store_stockmovement_sk,
		    ach.quantity,
		    ach.running_total,
		    ach.prior_avg_cost, 
		    ach.adjustment,
		   ach.actual_cost ,
		   ach.actual_cost_gst,
		   ach.average_cost,
		   ach.average_cost_gst
	from product_gnm_average_cost_history ach 
	inner join     
	        (select storeproduct_sk,max(adjustmentdate) max_adjustmentdate from product_gnm_average_cost_history group by storeproduct_sk) x 
	        	on x.storeproduct_sk = ach.storeproduct_sk and ach.adjustmentdate = x.max_adjustmentdate
	left join store_stockmovement_checkpoint smc on smc.store_stockmovement_sk = ach.store_stockmovement_sk 
	where smc.sk is null;

	
	
set role scire;
SET search_path = public, pg_catalog;

drop view if exists view_product_gnm_average_cost_history_checkpoint;

create or replace view view_product_gnm_average_cost_history_checkpoint as	
with recursive cogs(
	sk,
	adjustmentdate,
	quantity,
	running_total,
	store_sk,
	product_gnm_sk,
	storeproduct_sk,
	actual_cost,
	actual_cost_gst,
	prior_avg_cost,
	adjustment,
	average_cost,
	average_cost_gst,
	next_sk
) as(
	select sk,
	adjustmentdate,
	quantity,
	running_total,
	store_sk,
	product_gnm_sk,
	storeproduct_sk,
	actual_cost,
	actual_cost_gst,
	prior_avg_cost,
	adjustment,
	average_cost,
	average_cost_gst,
	next_sk
	from (select
			smc.store_stockmovement_sk as sk,
			smc.adjustmentdate,
			smc.quantity,
			smc.running_total,
			smc.store_sk,
			smc.product_gnm_sk,
			smc.storeproduct_sk,
			smc.actual_cost,
			smc.actual_cost_gst,
			smc.prior_avg_cost,
			smc.adjustment,
			smc.average_cost,
			smc.average_cost_gst,
			lead (sm.sk,1) OVER (PARTITION BY sm.storeproduct_sk ORDER by sm.adjustmentdate,sm.sk) next_sk,
			row_number() over (PARTITION BY sm.storeproduct_sk ORDER by sm.adjustmentdate,sm.sk) r
	from 
		store_stockmovement_checkpoint smc 
		inner join (select max(sk) max_sk from store_stockmovement_checkpoint group by storeproduct_sk) smc2 on smc2.max_sk = smc.sk
		left join store_stockmovement sm on sm.storeproduct_sk = smc.storeproduct_sk and sm.adjustmentdate >= smc.adjustmentdate 
		inner join adjustment_type a on a.sk = sm.adjustment_type_sk where a.Name in ('Receipt') or (smc.store_stockmovement_sk = sm.sk)) x where x.r = 1 
union all
	select
		sm.sk,
		sm.adjustmentdate,
		sm.adjustment as quantity,
		sm.adjustment + c.running_total as running_total,
		sm.store_sk,
		sm.product_gnm_sk,
		sm.storeproduct_sk,
		sp.actual_cost::numeric(19,2),
		sp.actual_cost_gst::numeric(19,2),
		c.average_cost,
		sm.adjustment,
		coalesce( case when sm.adjustment + (c.adjustment + c.running_total) <> 0
			then ((c.average_cost*(c.adjustment + c.running_total) + sp.actual_cost*sm.adjustment) / (sm.adjustment + (c.adjustment + c.running_total)))::numeric(19,2)
			else sp.actual_cost
		end, sp.actual_cost )
		as average_cost,
		coalesce(case when sm.adjustment + (c.adjustment + c.running_total) <> 0
			then ((c.average_cost_gst*(c.adjustment + c.running_total)  + sp.actual_cost_gst*sm.adjustment) / (sm.adjustment + (c.adjustment + c.running_total)))::numeric(19,2) 
			else sp.actual_cost_gst
		end, sp.actual_cost_gst ) as average_cost_gst,
		seq.next_sk 
	from cogs c 
	  	inner join store_stockmovement sm on sm.sk = c.next_sk  	
	  	left join stock_price_history sp on sp.product_gnm_sk = sm.product_gnm_sk and sp.created <= sm.adjustmentdate and sp.end_date > sm.adjustmentdate
		inner join (select sm.sk, lead(sm.sk,1) over (partition by storeproduct_sk order by adjustmentdate,sm.sk ) next_sk 
				from store_stockmovement sm inner join adjustment_type a on a.sk = sm.adjustment_type_sk and a.Name in ('Receipt') where sm.product_gnm_sk <> 0  ) seq on seq.sk = sm.sk
	where  sm.product_gnm_sk <> 0 	
) select  *  from cogs 

--where  storeproduct_sk = 2076915 


select * from store_stockmovement where storeproduct_sk =  2079613 order by adjustmentdate, sk ;
select sum(adjustment) from store_stockmovement where adjustment_type_sk = 322 and storeproduct_sk =  2079613;
select * from product_gnm_average_cost_history where storeproduct_sk =  2079613 order by sk ;
select * from store_stockmovement_checkpoint  where storeproduct_sk =  2079613 order by adjustmentdate, sk ;

------------------------------------
-- add column 
---select * from stock_value_adjustments
---- 
alter table store_stockmovement_checkpoint add column stock_value_adjustments_sk bigint, add constraint stva_sk_constraint foreign key (stock_value_adjustments_sk) references stock_value_adjustments(sk);

select * from stock_value_adjustments;

select * from stock_value_adjustments

select * from store_stockmovement_checkpoint


            select 
                sp.sk storeproduct_sk,
                sp.product_gnm_sk,
                s.sk as store_sk,
                sva.sk as stock_value_adjustments_sk,
                (sva.modified_date_time::date)::timestamp as adjustmentdate,
                '2030-01-01 12:00:00' as end_date,
                coalesce(smc.quantity,0) as quantity,
                coalesce(sva.total_stock_on_hand ,smc.running_total,0)  as running_total,
                coalesce(sva.current_average_cost_excl_gst, smc.prior_avg_cost) as prior_avg_cost,
                0 as adjustment,
                smc.actual_cost,
                smc.actual_cost_gst,
                sva.adjusted_average_cost_excl_gst as average_cost,
                sva.adjusted_average_cost_excl_gst*0.10 as average_cost_gst,
                smc.*
            -- select * 
            from stock_value_adjustments sva 
			left join storeproduct sp on sp.sk = sva.store_product_sk
			left join product_gnm pg on pg.sk = sp.product_gnm_sk
            inner join store s on s.storeid = sp.storeid
            left join store_stockmovement_checkpoint smc on smc.storeproduct_sk = sp.sk
            left join (select max(sk) maxsk 
                  from store_stockmovement_checkpoint group by storeproduct_sk) x on x.maxsk = smc.sk 
            --- check that not adding a duplicate stock value adjustment record
            where sp.sk <> 0 and (smc.stock_value_adjustments_sk is null or (smc.stock_value_adjustments_sk is not null 
            				and sva.sk not in (select stock_value_adjustments_sk from store_stockmovement_checkpoint 
													where stock_value_adjustments_sk is not null) ) )and pg.sk is not null;
            
select * from store_stockmovement_checkpoint where stock_value_adjustments_sk is not null;



	
	select fn_apply_cost_adjustment(sp.sk,pg.sk,sva.adjusted_average_cost_excl_gst,sva.adjusted_average_cost_excl_gst*0.1,
sva.modified_date_time,'2030-01-01')
from  stock_value_adjustments sva 
left join storeproduct sp on sp.sk = sva.store_product_sk
left join product_gnm pg on pg.sk = sp.product_gnm_sk;

select sp.sk,pg.sk,sva.adjusted_average_cost_excl_gst,sva.adjusted_average_cost_excl_gst*0.1,sva.modified_date_time,'2030-01-01'
from  stock_value_adjustments sva 
left join storeproduct sp on sp.sk = sva.store_product_sk
left join product_gnm pg on pg.sk = sp.product_gnm_sk;


--- 
1. stocktake
2. value adjust - entry 


Applying store_stockmovement_checkpoint

1. checkpoint - sk 1 date 15/ 10:00

2. pickups sk 1 - sk 2 date 15/11:59


Apply cogs

query max() checkpoint order by date, sk



              select
                sm.sk as store_stockmovement_sk,
                sm.adjustmentdate,
                sm.adjustment quantity,
                sm.adjustment running_total,
                sm.store_sk,
                sm.product_gnm_sk,
                sm.storeproduct_sk,
                sp.actual_cost::numeric(19,2) as actual_cost,
                sp.actual_cost::numeric(19,2) as prior_avg_cost,
                sp.actual_cost_gst::numeric(19,2) as actual_cost_gst,
                sm.adjustment,
                sp.actual_cost::numeric(19,2) as average_cost,
                sp.actual_cost_gst::numeric(19,2) as average_cost_gst
              from 
                (select sm.sk,row_number() over (partition by sm.storeproduct_sk,sm.product_gnm_sk order by sm.adjustmentdate,sm.sk ) r ,
                  lead (sm.sk,1) OVER (PARTITION BY sm.storeproduct_sk,product_gnm_sk ORDER by sm.adjustmentdate)	as next_sk
                  from store_stockmovement sm  where sm.adjustment_type_sk in (select sk from adjustment_type a where a.Name in ('Stocktake Receipt'))
                    ) seq
                inner join store_stockmovement sm on seq.sk = sm.sk and sm.product_gnm_sk <> 0
                inner join adjustment_type a on a.sk = sm.adjustment_type_sk  and a.Name in ('Stocktake Receipt')  
                left join  -- get the base price by date
                  stock_price_history sp on sp.product_gnm_sk = sm.product_gnm_sk and sp.created <= sm.adjustmentdate and sp.end_date > sm.adjustmentdate
                left join store_stockmovement_checkpoint smc on smc.storeproduct_sk = sm.storeproduct_sk and smc.adjustmentdate < sm.adjustmentdate
                left join stock_value_adjustments sva on sva.store_product_sk = sm.storeproduct_sk and sva.modified_date_time::date = sm.adjustmentdate::date
              where seq.r = 1 and smc.sk is null and sva.sk is null;

              
              

            select 
                sp.sk storeproduct_sk,
                sp.product_gnm_sk,
                s.sk as store_sk,
                sva.sk as stock_value_adjustments_sk,
                (sva.modified_date_time::date)::timestamp as adjustmentdate,
                '2030-01-01 12:00:00' as end_date,
                coalesce(smc.quantity,0) as quantity,
                coalesce(sva.total_stock_on_hand::integer ,smc.running_total::integer,0)  as running_total,
                coalesce(sva.current_average_cost_excl_gst, smc.prior_avg_cost) as prior_avg_cost,
                0 as adjustment,
                smc.actual_cost,
                smc.actual_cost_gst,
                sva.adjusted_average_cost_excl_gst as average_cost,
                sva.adjusted_average_cost_excl_gst*0.10 as average_cost_gst
            from stock_value_adjustments sva 
            left join storeproduct sp on sp.sk = sva.store_product_sk
            left join product_gnm pg on pg.sk = sp.product_gnm_sk
            inner join store s on s.storeid = sp.storeid
            left join store_stockmovement_checkpoint smc on smc.storeproduct_sk = sp.sk
            left join (select max(sk) maxsk 
                  from store_stockmovement_checkpoint group by storeproduct_sk) x on x.maxsk = smc.sk 
            --- check that not adding a duplicate stock value adjustment record
            where sp.sk <> 0 and (smc.stock_value_adjustments_sk is null or (smc.stock_value_adjustments_sk is not null 
            				and smc.stock_value_adjustments_sk <> sva.sk))
            				

           
            				
    select sum(cnt) /12   from    (select storesk,extract(month from createddate) m , count(*) cnt from invoice 
           where extract(year from createddate) =2017 and storesk  in (1)
           group by storesk, extract(month from createddate) order by count(*) desc) x
           
           
           avg 1,300
           
           
   select        130 * 6
           
   select * from spectaclejob
   
               				
    select sum(cnt) /12   from    (select storesk,extract(month from orderdate) m , count(*) cnt from spectaclejob 
           where extract(year from orderdate) =2017 and storesk  in (1)
           group by storesk, extract(month from orderdate) order by count(*) desc) x
   
   
----- stock take pms systems

--- stock price history for takeon 
--- stock price history for off catalog
--- stock price history adjustments for takeon & off catalog

select * from stock       

select 	x.sk,
		x1.created end_date,
		x1.product_gnm_sk from 
(select sk,storeproduct_sk, product_gnm_sk,created, row_number() over (partition by product_gnm_sk order by sh.created,sh.sk) r
from stock_price_history sh where product_gnm_sk <> 0) x 
inner join
(select sk,storeproduct_sk, product_gnm_sk,created, row_number() over (partition by product_gnm_sk order by sh.created,sh.sk) r
from stock_price_history sh where product_gnm_sk <> 0) x1 on x.product_gnm_sk = x1.product_gnm_sk and x.r+1 = x1.r
order by x.product_gnm_sk



              select x.sk,
                  x1.created end_date
              from 
              (select sk,storeproduct_sk, product_gnm_sk,created, row_number() over (partition by product_gnm_sk order by sh.created,sh.sk) r
              from stock_price_history sh where product_gnm_sk <> 0) x 
              inner join
              (select sk,storeproduct_sk, product_gnm_sk,created, row_number() over (partition by product_gnm_sk order by sh.created,sh.sk) r
              from stock_price_history sh where product_gnm_sk <> 0) x1 on x.product_gnm_sk = x1.product_gnm_sk and x.r+1 = x1.r
              order by x.product_gnm_sk
              
              
daily_gnm_to_scire_sync_01.conf
gnm_product_cost_history.conf
gnm_takeon_stock_cost_optomate.conf
gnm_takeon_stock_cost_sunix.conf


            select
            (s.storeid || '-PF-' || "FRAMENUM" || '-M-' || coalesce("MODIFIED",'') ) as nk,
            coalesce(s.sk,0) as store_sk,
            s.acquisition_date,
            coalesce(p.sk,0) as storeproduct_sk,
            "LISTPRICE",
            "EXLISTPR",
            "COSTPRICE",
            "EXCOSTPR",
            "RRP",
            s.storeid,
            "FRAMENUM",
            '2030-01-01'::timestamp as end_date,f.*
            from stage.today_gnm_sunix_vframe f
            inner join public.store s on (s.store_nk = f.source)
            left outer join public.storeproduct p on (p.productid = (s.storeid || '-PF-' || "FRAMENUM"))
            where "SUPPLIER" <> 'GNM' and f."MODIFIED" is not null
			order by f."MODIFIED" desc
			
			
			
			select
            (s.storeid || '-PF-' || "FRAMENUM" || '-M-' || coalesce("MODIFIED",'') ) as nk,
            coalesce(s.sk,0) as store_sk,
            s.acquisition_date,
            case when (s.acquisition_date > f."MODIFIED"::DATE and f."MODIFIED" is not null) or (f."MODIFIED" is null) then s.acquisition_date else f."MODIFIED"::DATE end as created,
            coalesce(p.sk,0) as storeproduct_sk,
            "LISTPRICE",
            "EXLISTPR",
            "COSTPRICE",
            "EXCOSTPR",
            "RRP",
            s.storeid,
            "FRAMENUM",
            '2030-01-01'::timestamp as end_date
            from stage.today_gnm_sunix_vframe f
            inner join public.store s on (s.store_nk = f.source)
            left outer join public.storeproduct p on (p.productid = (s.storeid || '-PF-' || "FRAMENUM"))
            where "SUPPLIER" <> 'GNM' and f."MODIFIED" is not null
            order by f."MODIFIED" desc
            
			
			
          SELECT 
           (s.storeid || '-PF-' || fb."BARCODE" || '-M-' || coalesce(fb."DATE_EDITED",'') ) as nk,
           case when (s.acquisition_date >fb."DATE_EDITED"::DATE and fb."DATE_EDITED" is not null) or (fb."DATE_EDITED" is null) 
           	then s.acquisition_date else fb."DATE_EDITED"::DATE end as created,                  
          "SELLINC","LISTPRICEEX", "COSTINC",
          "BRANCH_IDENTIFIER", "COSTEX", "RRPINC",
          coalesce(s.sk,0) as store_sk,
          fb.source,"BARCODE",
          coalesce(p.sk,0) as storeproduct_sk,
          coalesce(s.acquisition_date,'2000-01-01') as acquisition_date,
          s.storeid,
          '2030-01-01'::timestamp as end_date
          FROM stage.today_gnm_optomate_v_specframe_branch fb
          inner join public.store s on ( (fb.source || '-' || "BRANCH_IDENTIFIER") = s.store_nk)
          inner join public.storeproduct p on (p.productid = (s.storeid || '-PF-' || fb."BARCODE"))
          where fb."SUPPLIER_IDENTIFIER"<>'GNM'
          and not exists (
            select * FROM stage.today_gnm_optomate_v_specframe_branch fbi
            where fbi."BRANCH_IDENTIFIER" = fb."BRANCH_IDENTIFIER" AND fbi."BARCODE" = fb."BARCODE" AND fbi."ID" > fb."ID"
          )
          UNION ALL
          SELECT 
           (s.storeid || '-PF-' || fb."BARCODE" || '-M-' || coalesce(fb."DATE_EDITED",'') ) as nk,
           case when (s.acquisition_date >fb."DATE_EDITED"::DATE and fb."DATE_EDITED" is not null) or (fb."DATE_EDITED" is null) 
           	then s.acquisition_date else fb."DATE_EDITED"::DATE end as created,    
          "SELLINC","LISTPRICEEX", "COSTINC",
          "BRANCH_IDENTIFIER", "COSTEX", "RRPINC",
          coalesce(s.sk,0) as store_sk,
          fb.source,"BARCODE",
          coalesce(p.sk,0) as storeproduct_sk,
          coalesce(s.acquisition_date,'2000-01-01') as acquisition_date,
          s.storeid,
          '2030-01-01'::timestamp as end_date
          FROM stage.today_gnm_optomate_v_sunglass_branch fb
          inner join public.store s on ( (fb.source || '-' || "BRANCH_IDENTIFIER") = s.store_nk)
          inner join public.storeproduct p on (p.productid = (s.storeid || '-PF-' || fb."BARCODE"))
          where fb."SUPPLIER_IDENTIFIER"<>'GNM'
          and not exists (
            select * FROM stage.today_gnm_optomate_v_sunglass_branch fbi
            where fbi."BRANCH_IDENTIFIER" = fb."BRANCH_IDENTIFIER" AND fbi."BARCODE" = fb."BARCODE" AND fbi."ID" > fb."ID"
          )
  
          
          select * from  stage.today_gnm_optomate_v_sunglass_branch fb
          
	
                      select 
                sav.stock_adjust_value_id,
                sav.transact_ref || '-' || sav.stock_adjust_value_entry_id as nk, 
                sav.transact_ref,
                sav.stock_adjust_value_entry_id, 
                sav.store_product_sk, 
                sav.adjusted_average_cost_excl_gst,
                sav.total_stock_on_hand,
                sav.comment,
                sav.current_average_cost_excl_gst, 
                sav.current_average_cost_gst, 
                sav.current_average_cost_incl_gst,
                s.sk as store_sk,
                sav.modified_user,
                sav.modified_date_time 
              from stage.today_gnm_db_stock_adjust_value  sav 
                left join public.store s on (s.storeid = sav.store_nk)
          
          
   		INSERT INTO stock_price_history (
			store_sk, nk, storeproduct_sk, supplier_cost, supplier_cost_gst, actual_cost, actual_cost_gst, 
			supplier_rrp, supplier_rrp_gst, 
			calculated_retail_price, 
			calculated_retail_price_gst, 
			override_retail_price, 
			override_retail_price_gst, 
			retail_price, 
			retail_price_gst, 
			created, 
			product_gnm_sk,
			end_date)
	   select sph.store_sk, 'adjustment-' || now() || sph.nk,
			storeproduct_sk, supplier_cost, supplier_cost_gst, 
			in_actual_cost, in_actual_cost_gst, 
			supplier_rrp, supplier_rrp_gst, calculated_retail_price, 
			calculated_retail_price_gst, override_retail_price, 
			override_retail_price_gst, retail_price, 
			retail_price_gst, startperiod, product_gnm_sk, endperiod
		from stock_price_history sph
		where sph.sk in (select max(sk) from stock_price_history where storeproduct_sk = in_storeproduct_sk 
						group by storeproduct_sk ) and storeproduct_sk = in_storeproduct_sk and product_gnm_sk = in_product_gnm_sk;

		--- WILL NEED TO HANDLE different business logic assumptiosn 
		--- Assumption 1. adjustment extends to max end date
		--- 2. Adjustment is AFTER created date for existing stock_cost_history records and applies to records only AFTER the adjustment
	
		update public.stock_price_history set end_date = Z.end_date
		-- select * 
		from 
		(select x.sk,x1.created end_date,x1.product_gnm_sk 
		from (select sk,storeproduct_sk, product_gnm_sk,created, row_number() over (partition by storeproduct_sk,product_gnm_sk order by sh.created,sh.sk) r
					from stock_price_history sh where
						x.product_gnm_sk = in_product_gnm_sk and in_storeproduct_sk = storeproduct_sk) x 
		inner join (select sk,storeproduct_sk, product_gnm_sk,created, row_number() over (partition by storeproduct_sk,product_gnm_sk order by sh.created,sh.sk) r
					from stock_price_history sh where in_storeproduct_sk = storeproduct_sk) x1
						on x.product_gnm_sk = x1.product_gnm_sk and x.r+1 = x1.r 
						and x.product_gnm_sk = in_product_gnm_sk and x.storeproduct_sk = in_storeproduct_sk
		order by x.product_gnm_sk) Z 
		where Z.sk = stock_price_history.sk; 
		

            select 
                sp.sk storeproduct_sk,
                sp.product_gnm_sk,
                s.sk as store_sk,
                sva.sk as stock_value_adjustments_sk,
                (sva.modified_date_time::date)::timestamp as adjustmentdate,
                '2030-01-01 12:00:00' as end_date,
                coalesce(smc.quantity,0) as quantity,
                coalesce(sva.total_stock_on_hand::integer ,smc.running_total::integer,0)  as running_total,
                coalesce(sva.current_average_cost_excl_gst, smc.prior_avg_cost) as prior_avg_cost,
                0 as adjustment,
                smc.actual_cost,
                smc.actual_cost_gst,
                sva.adjusted_average_cost_excl_gst as average_cost,
                sva.adjusted_average_cost_excl_gst*0.10 as average_cost_gst
            from stock_value_adjustments sva 
            left join storeproduct sp on sp.sk = sva.store_product_sk
            left join product_gnm pg on pg.sk = sp.product_gnm_sk
            inner join store s on s.storeid = sp.storeid
            left join store_stockmovement_checkpoint smc on smc.storeproduct_sk = sp.sk
            left join (select max(sk) maxsk 
                  from store_stockmovement_checkpoint group by storeproduct_sk) x on x.maxsk = smc.sk 
            --- check that not adding a duplicate stock value adjustment record
            where sp.sk <> 0 and (smc.stock_value_adjustments_sk is null or (smc.stock_value_adjustments_sk is not null 
            				and smc.stock_value_adjustments_sk <> sva.sk))
            				
            				
            select 
                sp.sk storeproduct_sk,
                sp.product_gnm_sk,
                s.sk as store_sk,
                sva.sk as stock_value_adjustments_sk,
                (sva.modified_date_time::date)::timestamp as adjustmentdate,
                '2030-01-01 12:00:00' as end_date,
                coalesce(smc.quantity,0) as quantity,
                coalesce(sva.total_stock_on_hand::integer ,smc.running_total::integer,0)  as running_total,
                coalesce(sva.current_average_cost_excl_gst, smc.prior_avg_cost) as prior_avg_cost,
                0 as adjustment,
                smc.actual_cost,
                smc.actual_cost_gst,
                sva.adjusted_average_cost_excl_gst as average_cost,
                sva.adjusted_average_cost_excl_gst*0.10 as average_cost_gst
            from stock_value_adjustments sva 
            left join storeproduct sp on sp.sk = sva.store_product_sk
            left join product_gnm pg on pg.sk = sp.product_gnm_sk
            inner join store s on s.storeid = sp.storeid
            left join store_stockmovement_checkpoint smc on smc.storeproduct_sk = sp.sk
            left join (select max(sk) maxsk 
                  from store_stockmovement_checkpoint group by storeproduct_sk) x on x.maxsk = smc.sk 
            --- check that not adding a duplicate stock value adjustment record
            where sp.sk <> 0 and (smc.stock_value_adjustments_sk is null or (smc.stock_value_adjustments_sk is not null 
            				and smc.stock_value_adjustments_sk <> sva.sk))
 ---------------------------------------------------------------------------------
 
                sva.sk as stock_value_adjustments_sk,
                (sva.modified_date_time::date)::timestamp as adjustmentdate,
                '2030-01-01 12:00:00' as end_date,
                coalesce(smc.quantity,0) as quantity,
                coalesce(sva.total_stock_on_hand::integer ,smc.running_total::integer,0)  as running_total,
                coalesce(sva.current_average_cost_excl_gst, smc.prior_avg_cost) as prior_avg_cost,
                0 as adjustment,
                smc.actual_cost,
                smc.actual_cost_gst,
                sva.adjusted_average_cost_excl_gst as average_cost,
                sva.adjusted_average_cost_excl_gst*0.10 as average_cost_gst
                
       		sph.store_sk, 

			storeproduct_sk, 
			supplier_cost, 
			supplier_cost_gst, 
			sva. as actual_cost , 
			in_actual_cost_gst, 
			supplier_rrp, 
			supplier_rrp_gst, 
			calculated_retail_price, 
			calculated_retail_price_gst, 
			override_retail_price, 
			override_retail_price_gst, 
			retail_price, 
			retail_price_gst, 
			startperiod, 
			product_gnm_sk, 
			endperiod
            				
  ---------------------------------------------------------------------------------
           				
            				
                --- what to do next?
		INSERT INTO stock_price_history (
			store_sk, 
			nk, 
			storeproduct_sk, 
			supplier_cost, 
			supplier_cost_gst, 
			actual_cost, 
			actual_cost_gst, 
			supplier_rrp, supplier_rrp_gst, 
			calculated_retail_price, 
			calculated_retail_price_gst, 
			override_retail_price, 
			override_retail_price_gst, 
			retail_price, 
			retail_price_gst, 
			created, 
			product_gnm_sk,
			end_date)                
       select 
		s.sk as store_sk,
		'adjustment-' || sva.created ||  sva.nk as nk,
        sp.sk storeproduct_sk,
        sph.supplier_cost,
        sph.supplier_cost_gst,
        sva.adjusted_average_cost_excl_gst as actual_cost,
        sva.adjusted_average_cost_excl_gst*0.10 as actual_cost_gst,
		sph.supplier_rrp, 
		sph.supplier_rrp_gst, 
		sph.calculated_retail_price, 
		sph.calculated_retail_price_gst, 
		sph.override_retail_price, 
		sph.override_retail_price_gst, 
		sph.retail_price, 
		sph.retail_price_gst, 
       	(sva.modified_date_time::date)::timestamp as created,
        sp.product_gnm_sk,
        '2030-01-01' as end_date
		from stock_value_adjustments sva 
            left join storeproduct sp on sp.sk = sva.store_product_sk
            left join product_gnm pg on pg.sk = sp.product_gnm_sk
            inner join store s on s.storeid = sp.storeid			
			left join stock_price_history sph on sva.store_product_sk = sph.storeproduct_sk
			inner join (select storeproduct_sk, max(sk) max_stock_price_history_sk from stock_price_history group by storeproduct_sk ) msp on msp.max_stock_price_history_sk = sph.sk
		where sph.product_gnm_sk = 0 and ('adjustment-' || sva.created ||  sva.nk ) not in (select nk from stock_price_history)

     
		delete from stock_price_history  where nk like 'adjustment%' and product_gnm_sk = 0
		
		
		select * from stock_price_history where nk like 'adjustment%'
		
		select 
              s.sk as store_sk,
              'adjustment-' || sva.created ||  sva.nk as nk,
                  sp.sk storeproduct_sk,
                  sph.supplier_cost,
                  sph.supplier_cost_gst,
                  sva.adjusted_average_cost_excl_gst as actual_cost,
                  sva.adjusted_average_cost_excl_gst*0.10 as actual_cost_gst,
              sph.supplier_rrp, 
              sph.supplier_rrp_gst, 
              sph.calculated_retail_price, 
              sph.calculated_retail_price_gst, 
              sph.override_retail_price, 
              sph.override_retail_price_gst, 
              sph.retail_price, 
              sph.retail_price_gst, 
                  (sva.modified_date_time::date)::timestamp as created,
                  sp.product_gnm_sk,
                  '2030-01-01' as end_date
              from stock_value_adjustments sva 
                      left join storeproduct sp on sp.sk = sva.store_product_sk
                      left join product_gnm pg on pg.sk = sp.product_gnm_sk
                      inner join store s on s.storeid = sp.storeid			
                left join stock_price_history sph on sva.store_product_sk = sph.storeproduct_sk
                inner join (select storeproduct_sk, max(sk) max_stock_price_history_sk from stock_price_history group by storeproduct_sk ) msp on msp.max_stock_price_history_sk = sph.sk
              where sph.product_gnm_sk = 0 and ('adjustment-' || sva.created ||  sva.nk ) not in (select nk from stock_price_history)

		
		
		WITH RECURSIVE cogs(sk, adjustmentdate, quantity, running_total, store_sk, product_gnm_sk, storeproduct_sk, actual_cost, actual_cost_gst, prior_avg_cost, adjustment, average_cost, average_cost_gst, next_sk) AS (
         SELECT x.sk,
            x.adjustmentdate,
            x.quantity,
            x.running_total,
            x.store_sk,
            x.product_gnm_sk,
            x.storeproduct_sk,
            x.actual_cost,
            x.actual_cost_gst,
            x.prior_avg_cost,
            x.adjustment,
            x.average_cost,
            x.average_cost_gst,
            x.next_sk
           FROM ( SELECT smc.store_stockmovement_sk AS sk,
                    smc.adjustmentdate,
                    smc.quantity,
                    smc.running_total,
                    smc.store_sk,
                    smc.product_gnm_sk,
                    smc.storeproduct_sk,
                    smc.actual_cost,
                    smc.actual_cost_gst,
                    smc.prior_avg_cost,
                    smc.adjustment,
                    smc.average_cost,
                    smc.average_cost_gst,
                    lead(sm.sk, 1) OVER (PARTITION BY sm.storeproduct_sk ORDER BY sm.adjustmentdate, sm.sk) AS next_sk,
                    row_number() OVER (PARTITION BY sm.storeproduct_sk ORDER BY sm.adjustmentdate, sm.sk) AS r
                   FROM store_stockmovement_checkpoint smc
                     JOIN ( SELECT max(store_stockmovement_checkpoint.sk) AS max_sk
                           FROM store_stockmovement_checkpoint
                          GROUP BY store_stockmovement_checkpoint.storeproduct_sk) smc2 ON smc2.max_sk = smc.sk
                     LEFT JOIN store_stockmovement sm ON sm.storeproduct_sk = smc.storeproduct_sk AND sm.adjustmentdate >= smc.adjustmentdate 
                     JOIN adjustment_type a ON a.sk = sm.adjustment_type_sk
                  WHERE a.name::text = 'Receipt'::text OR smc.store_stockmovement_sk = sm.sk OR (a.name::text = 'Stocktake Receipt' and smc.store_stockmovement_sk is null)) x
          WHERE x.r = 1 
        UNION ALL
         SELECT sm.sk,
            sm.adjustmentdate,
            sm.adjustment AS quantity,
            sm.adjustment + c.running_total AS running_total,
            sm.store_sk,
            sm.product_gnm_sk,
            sm.storeproduct_sk,
            sp.actual_cost,
            sp.actual_cost_gst,
            c.average_cost,
            sm.adjustment,
            COALESCE(
                CASE
                    WHEN (sm.adjustment + c.running_total) <> 0 THEN ((c.average_cost * c.running_total::numeric + sp.actual_cost * sm.adjustment::numeric) / (sm.adjustment + c.running_total)::numeric)::numeric(19,2)
                    ELSE sp.actual_cost
                END, sp.actual_cost) AS average_cost,
            COALESCE(
                CASE
                    WHEN (sm.adjustment + c.running_total) <> 0 THEN ((c.average_cost_gst * c.running_total::numeric + sp.actual_cost_gst * sm.adjustment::numeric) / (sm.adjustment + c.running_total)::numeric)::numeric(19,2)
                    ELSE sp.actual_cost_gst
                END, sp.actual_cost_gst) AS average_cost_gst,
            seq.next_sk
           FROM cogs c
             JOIN store_stockmovement sm ON sm.sk = c.next_sk
             LEFT JOIN stock_price_history sp ON sp.product_gnm_sk = sm.product_gnm_sk AND sp.created <= sm.adjustmentdate AND sp.end_date > sm.adjustmentdate
             JOIN ( SELECT sm_1.sk,
                    lead(sm_1.sk, 1) OVER (PARTITION BY sm_1.storeproduct_sk ORDER BY sm_1.adjustmentdate, sm_1.sk) AS next_sk
                   FROM store_stockmovement sm_1
                     JOIN adjustment_type a ON a.sk = sm_1.adjustment_type_sk AND a.name::text = 'Receipt'::text
                  WHERE sm_1.product_gnm_sk <> 0) seq ON seq.sk = sm.sk
          WHERE sm.product_gnm_sk <> 0
        )
 SELECT cogs.sk,
    cogs.adjustmentdate,
    cogs.quantity,
    cogs.running_total,
    cogs.store_sk,
    cogs.product_gnm_sk,
    cogs.storeproduct_sk,
    cogs.actual_cost,
    cogs.actual_cost_gst,
    cogs.prior_avg_cost,
    cogs.adjustment,
    cogs.average_cost,
    cogs.average_cost_gst,
    cogs.next_sk
   FROM cogs
   
   // ----------------------------------------------------------------------------------------------------------
   
                 select x.sk,
                  x1.created end_date
              from 
              (select sk,storeproduct_sk, product_gnm_sk,created, row_number() over (partition by product_gnm_sk order by sh.created,sh.sk) r
              from stock_price_history sh) x 
              inner join
              (select sk,storeproduct_sk, product_gnm_sk,created, row_number() over (partition by product_gnm_sk order by sh.created,sh.sk) r
              from stock_price_history sh ) x1 on x.product_gnm_sk = x1.product_gnm_sk and x.r+1 = x1.r
              order by x.product_gnm_sk
   
              
   -- choose store
   -- choose product
   -- choose gnmproduct
   
              
              
   -- create a test case & process the data, actual sequence
   -- 1. gnm product  / non gnm product- no movements
   
              select * from stock_value_adjustments
              
              select * from stock_price_history sph inner join
             ( select storeproduct_sk, count(*) from stock_price_history sph where storeproduct_sk <> 0 group by storeproduct_sk having count(*) > 1 ) x  on x.storeproduct_sk = sph.storeproduct_sk
             order by sk
             
             select x.sk, x1.created end_date
              from 
          	  (select storeproduct_sk, product_gnm_sk, count(*) from stock_price_history where end_date = '2030-01-01' group by storeproduct_sk,product_gnm_sk  having count(*) > 1 ) y 
          	  inner join
              (select sk,storeproduct_sk, product_gnm_sk,created, row_number() over (partition by product_gnm_sk order by sh.created,sh.sk) r
              from stock_price_history sh ) x on x.storeproduct_sk = y.storeproduct_sk and x.product_gnm_sk = y.product_gnm_sk
              inner join
              (select sk,storeproduct_sk, product_gnm_sk,created, row_number() over (partition by product_gnm_sk order by sh.created,sh.sk) r
              from stock_price_history sh ) x1 on x.product_gnm_sk = x1.product_gnm_sk and x.r+1 = x1.r
              order by x.product_gnm_sk

              
              select x.sk, sh2.created end_date
              from 
          	  (select storeproduct_sk, product_gnm_sk, count(*) from stock_price_history where end_date = '2030-01-01' group by storeproduct_sk,product_gnm_sk  having count(*) > 1 ) y 
          	  inner join
              (select sk,storeproduct_sk, product_gnm_sk,created, lead (sh.sk,1) over (partition by storeproduct_sk,product_gnm_sk order by sh.created,sh.sk) next_sk
              from stock_price_history sh ) x on x.product_gnm_sk = y.product_gnm_sk and x.storeproduct_sk = y.storeproduct_sk
              inner join 
              	stock_price_history sh2 on sh2.sk = x.next_sk
              order by x.product_gnm_sk
                       
          select * from stock_price_history where storeproduct_sk = 2200069 order by sk
              
          update stock_price_history set end_date = '2030-01-01' where storeproduct_sk = 2200069

          select storeproduct_sk, product_gnm_sk, count(*) from stock_price_history -- where end_date = '2030-01-01'
          where product_gnm_sk in (select product_gnm_sk from stock_value_adjustments)
          group by storeproduct_sk,product_gnm_sk  having count(*) > 1
          order by product_gnm_sk desc


              select x.sk, x1.created end_date
              from (select sk,storeproduct_sk, product_gnm_sk,created, row_number() over (partition by product_gnm_sk order by sh.created,sh.sk) r from stock_price_history sh) x 
              inner join (select sk,storeproduct_sk, product_gnm_sk,created, row_number() over (partition by product_gnm_sk order by sh.created,sh.sk) r from stock_price_history sh ) x1 on x.product_gnm_sk = x1.product_gnm_sk and x.r+1 = x1.r
              order by x.product_gnm_sk
               
             
   -- 2. stocktake in 
   -- 3. receipt
   -- 4. value adjustment
   
      
   -- create a test case & process the data, sequence applied
   -- 1. value adjustment
   -- 2. stocktake in 
   -- 3. receipt, adjusted

    -- create a test case & process the data, actual sequence
   -- 0 gnm product - no movements
   -- 1. value adjustment
   -- 2. stocktake in 
   -- 3. receipt, adjusted  
   
              
              select * from stock_value_adjustments 
              
              select * from stocktake_item where store_product_sk = 2200069
              
                 select * from stocktake_item where store_product_sk in (select store_product_sk from stock_value_adjustments) order by created_date_time
               
select * from stock_price_history where storeproduct_sk in (select store_product_sk from stock_value_adjustments) order by created ;

   select * from store_stockmovement where product_gnm_sk =0 and  storeproduct_sk in (select store_product_sk from stock_value_adjustments) order by storeproduct_sk, adjustmentdate, sk
   
      select * from store_stockmovement_checkpoint where storeproduct_sk in (select store_product_sk from stock_value_adjustments) order by storeproduct_sk, adjustmentdate, sk
      
      select * from store_stockmovement where storeproduct_sk = 2200069 order by sk
      
      select * from store_stockmovement sm inner join store_stockmovement_costs smc on sm.sk = smc.store_stockmovement_sk where smc.storeproduct_sk = 2200069 order by adjustmentdate, sk
      
      select * from stock_price_history where storeproduct_sk = 2200069 order by sk
      
      select * from v_store_stock_movement_cost where storeproduct_sk = 2200069     

2018-02-01 00:00:00
2018-02-13 10:39:17
      
2017-04-20 00:00:00	0	2017-04-20 00:00:00
2018-02-07 00:00:00	0	2018-02-07 00:00:00
2018-02-07 00:00:00	0	2030-01-01 00:00:00
271.70	24.70
240.00	24.00
240.00	24.00

rollback;

 begin ;
delete from stock_price_history where sk = 1939812;


      select * from stock_price_history where storeproduct_sk = 2200069 order by sk
      

          update stock_price_history set end_date = '2030-01-01' where storeproduct_sk = 2200069

             select x.sk, sh2.created end_date
              from
               (select storeproduct_sk, product_gnm_sk, count(*) from stock_price_history where end_date = '2030-01-01' 
                  group by storeproduct_sk,product_gnm_sk  having count(*) > 1 ) y
                  inner join
              (select sk,storeproduct_sk, product_gnm_sk,created, lead (sh.sk,1) over 
              			(partition by storeproduct_sk,product_gnm_sk order by sh.created,sh.sk) next_sk
              from stock_price_history sh ) x on x.product_gnm_sk = y.product_gnm_sk and x.storeproduct_sk = y.storeproduct_sk
              inner join
                stock_price_history sh2 on sh2.sk = x.next_sk
                where x.storeproduct_sk = 2200069    
              order by x.product_gnm_sk
              
              
              


     orderid 1117444, jobid 56450
      