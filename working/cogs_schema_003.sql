--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.2
-- Dumped by pg_dump version 9.6.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

--
-- Name: fn_create_viewbydate(text, text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION fn_create_viewbydate(tablename text, datecolumnname text) RETURNS void
    LANGUAGE plpgsql
    AS $_$
begin
	EXECUTE fn_create_viewbydate_string($1, $2);
end
$_$;


--
-- Name: fn_create_viewbydate(text, text, text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION fn_create_viewbydate(tablename text, datecolumnname text, keycolumn text) RETURNS void
    LANGUAGE plpgsql
    AS $_$
begin
	EXECUTE fn_create_viewbydate_string($1, $2,$3);
end
$_$;


--
-- Name: fn_create_viewbydate_string(text, text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION fn_create_viewbydate_string(tablename text, datecolumnname text) RETURNS text
    LANGUAGE sql
    AS $_$
select 'CREATE OR REPLACE VIEW vbyd_' || $1 || ' as select date_part(''year'',sm.' || $2 || ')as year,date_part(''doy'',sm.' 
|| $2 || ') as doy,date_part(''month'',sm.' || $2 || ' )as month,date_part(''dow'',sm.' || $2 || ') as dow,date_part(''day'',sm.'|| $2 ||') as day, sk 
from ' || $1 || ' sm 
group by date_part(''year'',sm.' || $2 || '),date_part(''doy'',sm.' || $2 || '),date_part(''month'',sm.' || $2 || '),date_part(''dow'',sm.' || $2 || '),date_part(''day'',sm.' || $2 || '), sk' as view_string $_$;


--
-- Name: fn_create_viewbydate_string(text, text, text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION fn_create_viewbydate_string(tablename text, datecolumnname text, keycolumn text) RETURNS text
    LANGUAGE sql
    AS $_$
select 'CREATE OR REPLACE VIEW vbyd_' || $1 || ' as select date_part(''year'',sm.' || $2 || ')as year,date_part(''doy'',sm.' 
|| $2 || ') as doy,date_part(''month'',sm.' || $2 || ' )as month,date_part(''dow'',sm.' || $2 || ') as dow,date_part(''day'',sm.'|| $2 ||') as day, ' || $3 || '  
from ' || $1 || ' sm 
group by date_part(''year'',sm.' || $2 || '),date_part(''doy'',sm.' || $2 || '),date_part(''month'',sm.' || $2 || '),date_part(''dow'',sm.' || $2 || '),date_part(''day'',sm.' || $2 || '),' || $3 as view_string $_$;


--
-- Name: fn_create_viewbydate_string2(regclass, regclass); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION fn_create_viewbydate_string2(tablename regclass, datecolumnname regclass) RETURNS text
    LANGUAGE sql
    AS $_$
select 'CREATE OR REPLACE VIEW vbyd_' || $1 || ' as select date_part(''year'',sm.' || $2 || ')as year,date_part(''doy'',sm.' 
|| $2 || ') as doy,date_part(''month'',sm.' || $2 || ')as month2,date_part(''dow'',sm.' || $2 || ') as dow,date_part(''day'',sm.' || $2 || ') as day, sk from ' || $1 || 
' sm group by date_part(''year'',sm.' || $2 || '),date_part(''doy'',sm.' || $2 || '),date_part(''month'',sm.' || $2 || '),date_part(''dow'',sm.' 
|| $2 || '),date_part(''day'',sm.' || $2 || '),sk' as view_string $_$;


--
-- Name: fn_create_viewbydate_string2(character varying, character varying); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION fn_create_viewbydate_string2(tablename character varying, datecolumnname character varying) RETURNS character varying
    LANGUAGE sql
    AS $_$
select 'CREATE OR REPLACE VIEW vbyd_' || $1 || ' as select date_part(''year'',sm.' || $2 || ')as year,date_part(''doy'',sm.' 
|| $2 || ') as doy,date_part(''month'',sm.' || $2 || ' )as month,date_part(''dow'',sm.' || $2 || ') as dow,date_part(''day'',sm.'|| $2 ||') as day, sk 
from ' || $1 || ' sm 
group by date_part(''year'',sm.' || $2 || '),date_part(''doy'',sm.' || $2 || '),date_part(''month'',sm.' || $2 || '),date_part(''dow'',sm.' || $2 || '),date_part(''day'',sm.' || $2 || '),sk' as view_string $_$;


--
-- Name: within_dow_range(timestamp without time zone, timestamp without time zone); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION within_dow_range(d1 timestamp without time zone, d2 timestamp without time zone) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
declare
	td1 timestamp;
	td2 timestamp;
begin	
	if date_part('doy',d1) = date_part('doy',d2) and date_part('year',d1) = date_part('year',d2) then
		return true;
	elsif d2 >= (d1 - interval '1 days') and d2 <= (d1 + interval '1 day') then
		return true;
	elsif date_part('dow',d1) = 1 and d2 >= (d1 - interval '3 days') and d2 <= (d1 + interval '1 day') then
		return true;
	elsif date_part('dow',d1) = 5 and d2 >= (d1 - interval '1 days') and d2 <= (d1 + interval '3 day') then
		return true;
	elsif date_part('dow',d1) = 6 and d2 >= (d1 - interval '1 days') and d2 <= (d1 + interval '2 day') then
		return true;
	else
		return false;
	end if;
end;
$$;


--
-- Name: within_dow_range(timestamp without time zone, timestamp without time zone, integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION within_dow_range(d1 timestamp without time zone, d2 timestamp without time zone, bounds integer) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
begin	
	if d2 >= (d1 - interval '1d'* bounds) and d2 <= (d1 + interval '1d'* bounds) then
		return true;
	else
		return within_dow_range(d1,d2);
	end if;
end;
$$;


SET default_with_oids = false;

--
-- Name: adjustment_type; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE adjustment_type (
    sk integer NOT NULL,
    adjustment_typeid character varying(30) NOT NULL,
    name character varying(100) DEFAULT 'Other'::character varying NOT NULL,
    othername character varying(100) NOT NULL
);


--
-- Name: adjustment_type_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE adjustment_type_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: adjustment_type_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE adjustment_type_sk_seq OWNED BY adjustment_type.sk;


--
-- Name: appointment; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE appointment (
    sk integer NOT NULL,
    appointmentid character varying(100),
    storesk integer NOT NULL,
    createddate timestamp without time zone NOT NULL,
    booking_startdate timestamp without time zone NOT NULL,
    booking_enddate timestamp without time zone NOT NULL,
    employeesk integer NOT NULL,
    customersk integer NOT NULL,
    statussk integer NOT NULL,
    appointment_typesk integer NOT NULL,
    mins integer,
    modified timestamp without time zone,
    prospect_firstname character varying(50),
    prospect_lastname character varying(50),
    prospect_title character varying(10),
    prospect_email character varying(150),
    prospect_homephone character varying(150),
    prospect_mobile character varying(150),
    prospect_workphone character varying(150),
    isdeleted boolean DEFAULT false NOT NULL
);


--
-- Name: appointment_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE appointment_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: appointment_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE appointment_sk_seq OWNED BY appointment.sk;


--
-- Name: appointment_type; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE appointment_type (
    sk integer NOT NULL,
    appointment_typeid character varying(30) NOT NULL,
    name character varying(100) NOT NULL
);


--
-- Name: appointment_type_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE appointment_type_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: appointment_type_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE appointment_type_sk_seq OWNED BY appointment_type.sk;


--
-- Name: invoice_orders; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE invoice_orders (
    sk integer NOT NULL,
    store_sk integer NOT NULL,
    invoice_sk integer NOT NULL,
    order_sk integer NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: invoiceitem_old; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE invoiceitem_old (
    invoicesk integer NOT NULL,
    pos integer NOT NULL,
    productsk integer NOT NULL,
    storesk integer NOT NULL,
    employeesk integer NOT NULL,
    serviceemployeesk integer NOT NULL,
    customersk integer NOT NULL,
    createddate timestamp without time zone NOT NULL,
    quantity integer NOT NULL,
    amount_incl_gst numeric(19,4),
    modified timestamp without time zone,
    amount_gst numeric(19,4),
    discount_incl_gst numeric(19,4),
    discount_gst numeric(19,4),
    cost_incl_gst numeric(19,4),
    cost_gst numeric(19,4),
    description character varying(500),
    payeesk integer,
    invoiceitemid character varying(60) DEFAULT ''::character varying NOT NULL,
    return_incl_gst numeric(19,4),
    return_gst numeric(19,4),
    bulkbillref character varying(100)
);


--
-- Name: order_history; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE order_history (
    sk integer NOT NULL,
    store_sk integer NOT NULL,
    customer_sk integer NOT NULL,
    source_id character varying(100),
    order_type character varying(100) NOT NULL,
    queue character varying(100),
    status character varying(100),
    source_created timestamp without time zone NOT NULL,
    source_modified timestamp without time zone NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: stock_price_history; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE stock_price_history (
    sk integer NOT NULL,
    store_sk integer NOT NULL,
    source_id character varying(100) NOT NULL,
    takeon_storeproduct_sk integer NOT NULL,
    supplier_cost numeric(19,2),
    supplier_cost_gst numeric(19,2),
    actual_cost numeric(19,2),
    actual_cost_gst numeric(19,2),
    supplier_rrp numeric(19,2),
    supplier_rrp_gst numeric(19,2),
    calculated_retail_price numeric(19,2),
    calculated_retail_price_gst numeric(19,2),
    override_retail_price numeric(19,2),
    override_retail_price_gst numeric(19,2),
    retail_price numeric(19,2),
    retail_price_gst numeric(19,2),
    created timestamp without time zone DEFAULT now() NOT NULL,
    product_gnm_sk integer
);


--
-- Name: store_stockmovement; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE store_stockmovement (
    sk integer NOT NULL,
    store_sk integer NOT NULL,
    source_id character varying(100) NOT NULL,
    takeon_storeproduct_sk integer NOT NULL,
    adjustmentdate timestamp without time zone,
    stock_price_history_sk integer NOT NULL,
    adjustment integer NOT NULL,
    adjustmenttype character varying(20) NOT NULL,
    reason character varying(20) NOT NULL,
    invoice_sk integer,
    customer_sk integer,
    avg_cost_price numeric(19,2),
    shipping_ref character varying(100),
    shipping_operator character varying(100),
    comments character varying(500),
    employee_sk integer,
    created timestamp without time zone DEFAULT now() NOT NULL,
    adjustmentdate_sk integer,
    product_gnm_sk integer,
    adjustment_type_sk integer DEFAULT 0 NOT NULL,
    actual_cost numeric(19,2),
    actual_cost_value numeric(19,2),
    moving_quantity numeric(19,2),
    moving_actual_cost_value numeric(19,2),
    moving_cost_per_unit numeric(19,2)
);


--
-- Name: storeproduct; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE storeproduct (
    sk integer NOT NULL,
    storeid character varying(12),
    productid character varying(500),
    type character varying(100),
    datecreated timestamp without time zone,
    name character varying(500),
    description character varying(500),
    retailprice numeric(19,4),
    wholesaleprice numeric(19,4),
    modified timestamp without time zone,
    last_purchase timestamp without time zone,
    last_sale timestamp without time zone,
    quantity integer,
    frame_model character varying(100) DEFAULT ''::character varying,
    frame_colour character varying(100) DEFAULT ''::character varying,
    frame_brand character varying(100) DEFAULT ''::character varying,
    frame_temple numeric(8,2) DEFAULT 0,
    frame_bridge numeric(8,2) DEFAULT 0,
    frame_eye_size numeric(8,2) DEFAULT 0,
    frame_depth numeric(8,2) DEFAULT 0,
    frame_material character varying(100) DEFAULT ''::character varying,
    lens_type character varying(100) DEFAULT ''::character varying,
    lens_refractive_index numeric(19,4) DEFAULT 0,
    lens_stock_grind character varying(10) DEFAULT ''::character varying,
    lens_size numeric(4,2) DEFAULT 0,
    lens_segment_size numeric(4,2) DEFAULT 0,
    internal_sku character varying(100),
    distributor character varying(100),
    product_gnm_sk integer
);


--
-- Name: cogs_sales; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW cogs_sales AS
 SELECT ii.invoicesk,
    ii.pos,
    ii.productsk,
    ii.storesk,
    ii.employeesk,
    ii.serviceemployeesk,
    ii.customersk,
    ii.createddate,
    ii.quantity,
    ii.amount_incl_gst,
    ii.modified,
    ii.amount_gst,
    ii.discount_incl_gst,
    ii.discount_gst,
    ii.cost_incl_gst,
    ii.cost_gst,
    ii.description,
    ii.payeesk,
    ii.invoiceitemid,
    ii.return_incl_gst,
    ii.return_gst,
    ii.bulkbillref,
    ( SELECT ph.actual_cost
           FROM stock_price_history ph
          WHERE ((ph.takeon_storeproduct_sk = ii.productsk) AND (ph.created <= ii.createddate))
          ORDER BY ph.created DESC
         LIMIT 1) AS takeon_current_cost,
    ( SELECT ph.actual_cost
           FROM stock_price_history ph
          WHERE ((ph.product_gnm_sk = tp.product_gnm_sk) AND (ph.created <= ii.createddate))
          ORDER BY ph.created DESC
         LIMIT 1) AS gnm_current_cost,
    ( SELECT m.actual_cost
           FROM store_stockmovement m
          WHERE ((m.invoice_sk = ii.invoicesk) AND (m.takeon_storeproduct_sk = ii.productsk))
         LIMIT 1) AS average_cost,
    ( SELECT oh.order_type
           FROM (invoice_orders io
             JOIN order_history oh ON ((io.order_sk = oh.sk)))
          WHERE (io.invoice_sk = ii.invoicesk)
          ORDER BY oh.source_modified DESC
         LIMIT 1) AS order_type
   FROM (invoiceitem_old ii
     JOIN storeproduct tp ON ((ii.productsk = tp.sk)))
  ORDER BY ii.createddate DESC;


--
-- Name: consult; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE consult (
    sk integer NOT NULL,
    consultid character varying(100),
    consultdate timestamp without time zone NOT NULL,
    storesk integer NOT NULL,
    employeesk integer NOT NULL,
    customersk integer NOT NULL,
    modified timestamp without time zone NOT NULL
);


--
-- Name: consult_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE consult_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: consult_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE consult_sk_seq OWNED BY consult.sk;


--
-- Name: consult_type; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE consult_type (
    sk integer NOT NULL,
    consult_typeid character varying(30) NOT NULL,
    description character varying(100) NOT NULL
);


--
-- Name: consult_type_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE consult_type_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: consult_type_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE consult_type_sk_seq OWNED BY consult_type.sk;


--
-- Name: consultitem; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE consultitem (
    sk integer NOT NULL,
    consultsk integer NOT NULL,
    consult_typesk integer NOT NULL
);


--
-- Name: consultitem_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE consultitem_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: consultitem_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE consultitem_sk_seq OWNED BY consultitem.sk;


--
-- Name: customer; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE customer (
    sk integer NOT NULL,
    createddate timestamp without time zone NOT NULL,
    customerid character varying(500),
    firstname character varying(50) NOT NULL,
    lastname character varying(50),
    middlename character varying(50),
    title character varying(10) NOT NULL,
    gender character varying(10) NOT NULL,
    birthdate date,
    preferredname character varying(50),
    preferredcontactmethod character varying(50),
    email character varying(150) NOT NULL,
    emailcreated timestamp without time zone DEFAULT ('now'::text)::date NOT NULL,
    phonecountryid integer,
    phonecreated timestamp without time zone DEFAULT ('now'::text)::date NOT NULL,
    address1 character varying(250),
    address2 character varying(250),
    address3 character varying(250),
    state character varying(60),
    suburb character varying(60),
    postcode integer,
    firstvisit timestamp without time zone,
    lastvisit timestamp without time zone,
    lastconsult timestamp without time zone,
    lastrecall timestamp without time zone,
    nextrecall timestamp without time zone,
    lastrecalldesc character varying(250),
    nextrecalldesc character varying(250),
    lastoptomseen integer,
    storesk integer DEFAULT 0 NOT NULL,
    healthfund character varying(50),
    modified timestamp without time zone,
    optout_recall boolean DEFAULT false NOT NULL,
    optout_marketing boolean DEFAULT false NOT NULL,
    deceased boolean DEFAULT false NOT NULL,
    homephone character varying(150),
    mobile character varying(150),
    workphone character varying(150),
    isaddrvalid boolean
);


--
-- Name: customer_ext; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE customer_ext (
    sk integer NOT NULL,
    customersk integer NOT NULL,
    s_recall character varying(1),
    s_cmsms character varying(1),
    s_cmmobile character varying(1),
    s_cmwphone character varying(1),
    s_cmhphone character varying(1),
    s_cmemail character varying(1),
    s_cmletter character varying(1),
    s_cmfno boolean,
    s_cmnoemail boolean,
    s_apnotif integer,
    s_apreminder integer,
    o_pref_recallint character varying(20),
    o_pref_recallsub character varying(20),
    o_pref_ordercoll character varying(20),
    o_pref_appointments character varying(20),
    o_pref_marketing character varying(20),
    o_pref_telephone character varying(20),
    o_badaddress boolean,
    o_badphone boolean,
    o_bademail boolean,
    o_noemail boolean,
    modified timestamp without time zone,
    o_inactive boolean,
    o_inactive_reason character varying(255)
);


--
-- Name: customer_ext_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE customer_ext_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: customer_ext_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE customer_ext_sk_seq OWNED BY customer_ext.sk;


--
-- Name: customer_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE customer_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: customer_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE customer_sk_seq OWNED BY customer.sk;


--
-- Name: d_date; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE d_date (
    sk integer NOT NULL,
    date_dim_id integer NOT NULL,
    date_actual date NOT NULL,
    epoch bigint NOT NULL,
    day_suffix character varying(4) NOT NULL,
    day_name character varying(9) NOT NULL,
    day_of_week integer NOT NULL,
    day_of_month integer NOT NULL,
    day_of_quarter integer NOT NULL,
    day_of_year integer NOT NULL,
    week_of_month integer NOT NULL,
    week_of_year integer NOT NULL,
    week_of_year_iso character(10) NOT NULL,
    month_actual integer NOT NULL,
    month_name character varying(9) NOT NULL,
    month_name_abbreviated character(3) NOT NULL,
    year_month_name character varying(8) NOT NULL,
    quarter_actual integer NOT NULL,
    quarter_name character varying(9) NOT NULL,
    year_calendar integer NOT NULL,
    year_calendar_name character varying(7) NOT NULL,
    year_financial integer NOT NULL,
    year_financial_name character varying(7) NOT NULL,
    first_day_of_week date NOT NULL,
    last_day_of_week date NOT NULL,
    first_day_of_month date NOT NULL,
    last_day_of_month date NOT NULL,
    first_day_of_quarter date NOT NULL,
    last_day_of_quarter date NOT NULL,
    first_day_of_year date NOT NULL,
    last_day_of_year date NOT NULL,
    yyyymm character(6) NOT NULL,
    yyyyq character(5) NOT NULL,
    weekend_indr boolean NOT NULL
);


--
-- Name: d_date_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE d_date_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: d_date_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE d_date_sk_seq OWNED BY d_date.sk;


--
-- Name: dailysnapshot_cost; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE dailysnapshot_cost (
    createddate_sk integer,
    today date,
    cost numeric
);


--
-- Name: dailysnapshot_cost_stores; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE dailysnapshot_cost_stores (
    storeid character varying(12),
    today date,
    cost numeric
);


--
-- Name: dailysnapshot_revenue; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE dailysnapshot_revenue (
    createddate_sk integer,
    today date,
    revenue numeric
);


--
-- Name: dailysnapshot_revenue_stores; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE dailysnapshot_revenue_stores (
    storeid character varying(12),
    today date,
    revenue numeric
);


--
-- Name: discountcode; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE discountcode (
    sk integer NOT NULL,
    storeid character varying(12) DEFAULT 'All'::character varying NOT NULL,
    discountitemcode character varying(100) NOT NULL,
    description character varying(150),
    valid_from timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone,
    valid_to timestamp without time zone DEFAULT '2099-12-31 00:00:00'::timestamp without time zone
);


--
-- Name: discountcode_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE discountcode_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: discountcode_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE discountcode_sk_seq OWNED BY discountcode.sk;


--
-- Name: employee; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE employee (
    sk integer NOT NULL,
    createddate timestamp without time zone NOT NULL,
    employeeid character varying(500),
    employeetype character varying(50),
    employmentstartdate date,
    employmentenddate date,
    firstname character varying(50) NOT NULL,
    lastname character varying(50),
    middlename character varying(50),
    title character varying(10) NOT NULL,
    gender character varying(10) NOT NULL,
    birthdate date,
    preferredname character varying(50),
    preferredcontactmethod character varying(10),
    email character varying(150) NOT NULL,
    emailcreated timestamp without time zone DEFAULT ('now'::text)::date NOT NULL,
    phonecountryid integer,
    phonenumber character varying(150),
    phonerawnumber character varying(150),
    phonemobile boolean NOT NULL,
    phonesmsenabled boolean NOT NULL,
    phonefax boolean NOT NULL,
    phonelandline boolean NOT NULL,
    phonecreated timestamp without time zone DEFAULT ('now'::text)::date NOT NULL,
    storesk integer DEFAULT 0 NOT NULL
);


--
-- Name: employee_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE employee_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: employee_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE employee_sk_seq OWNED BY employee.sk;


--
-- Name: expense; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE expense (
    sk integer NOT NULL,
    expenseid character varying(100),
    storesk integer NOT NULL,
    createddate timestamp without time zone NOT NULL,
    employeesk integer NOT NULL,
    expense_typesk integer NOT NULL,
    description character varying(100),
    amount_incl_gst numeric(19,4),
    amount_gst numeric(19,4),
    modified timestamp without time zone
);


--
-- Name: expense_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE expense_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: expense_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE expense_sk_seq OWNED BY expense.sk;


--
-- Name: expense_type; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE expense_type (
    sk integer NOT NULL,
    expense_typeid character varying(30) NOT NULL,
    name character varying(100) NOT NULL
);


--
-- Name: expense_type_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE expense_type_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: expense_type_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE expense_type_sk_seq OWNED BY expense_type.sk;


--
-- Name: gnm_invoice_orders; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE gnm_invoice_orders (
    orderid character varying(100),
    customer_sk integer,
    order_date timestamp without time zone,
    jobid character varying(100),
    job_date timestamp without time zone,
    product_sk integer,
    supplier_sk integer,
    productid character varying(500),
    type character varying(100)
);


--
-- Name: inventory_snapshot; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE inventory_snapshot (
    product_gnm_sk integer,
    store_sk integer,
    date_sk integer,
    accumalitive_adjustment bigint,
    accumalitive_actual_value numeric,
    total_receipt bigint,
    total_sale bigint,
    total_return bigint,
    total_writeoff bigint,
    total_theft bigint,
    total_receipt_actual_value numeric,
    total_sale_actual_value numeric,
    total_return_actual_value numeric,
    total_writeoff_actual_value numeric,
    total_theft_actual_value numeric,
    takeon_storeproduct_sk integer
);


--
-- Name: invoice; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE invoice (
    sk integer NOT NULL,
    invoiceid character varying(100),
    createddate timestamp without time zone NOT NULL,
    storesk integer NOT NULL,
    employeesk integer NOT NULL,
    customersk integer NOT NULL,
    amount_incl_gst numeric(19,4),
    modified timestamp without time zone,
    amount_gst numeric(19,4),
    reversalref character varying(100),
    rebateref character varying(100)
);


--
-- Name: invoice_dailysnapshot; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE invoice_dailysnapshot (
    createddate_sk integer,
    today date,
    revenue numeric
);


--
-- Name: invoice_dailysnapshot_all; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE invoice_dailysnapshot_all (
    createddate_sk integer,
    today date,
    today_revenue numeric,
    yesterday_revenue numeric,
    today_revenue_py numeric,
    yesterday_revenue_py numeric,
    day_lastweek_revenue numeric,
    day_lastweek_revenue_py numeric
);


--
-- Name: invoice_order_job; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE invoice_order_job (
    sk integer NOT NULL,
    store_sk integer NOT NULL,
    invoice_sk integer NOT NULL,
    order_id_nk character varying(100),
    job_id_nk character varying(100),
    created timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: invoice_order_job_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE invoice_order_job_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: invoice_order_job_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE invoice_order_job_sk_seq OWNED BY invoice_order_job.sk;


--
-- Name: invoice_orders_reconcile; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE invoice_orders_reconcile (
    pmsorderid text,
    invoicesk integer,
    framesk integer,
    right_lenssk integer,
    left_lenssk integer,
    storesk integer,
    pms_invoicenum character varying(100),
    orderid character varying(100),
    customer_sk integer,
    order_date timestamp without time zone,
    jobid character varying(100),
    job_date timestamp without time zone,
    product_sk integer,
    supplier_sk integer,
    productid character varying(500),
    type character varying(100)
);


--
-- Name: invoice_orders_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE invoice_orders_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: invoice_orders_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE invoice_orders_sk_seq OWNED BY invoice_orders.sk;


--
-- Name: invoice_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE invoice_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: invoice_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE invoice_sk_seq OWNED BY invoice.sk;


--
-- Name: invoiceitem; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE invoiceitem (
    invoicesk integer,
    pos integer,
    productsk integer,
    storesk integer,
    employeesk integer,
    serviceemployeesk integer,
    customersk integer,
    createddate timestamp without time zone,
    quantity integer,
    amount_incl_gst numeric(19,4),
    modified timestamp without time zone,
    amount_gst numeric(19,4),
    discount_incl_gst numeric(19,4),
    discount_gst numeric(19,4),
    cost_incl_gst numeric(19,4),
    cost_gst numeric(19,4),
    description character varying(500),
    payeesk integer,
    invoiceitemid character varying(60),
    return_incl_gst numeric(19,4),
    return_gst numeric(19,4),
    bulkbillref character varying(100),
    createddate_sk integer,
    pms_cost numeric(19,2),
    gnm_current_cost numeric(19,2),
    takeon_avg_cost numeric(19,2),
    order_type character varying(90),
    order_id character varying(90)
);


--
-- Name: job_history; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE job_history (
    sk integer NOT NULL,
    source_id character varying(100),
    order_id character varying(100) NOT NULL,
    order_sk integer NOT NULL,
    job_type character varying(100) NOT NULL,
    product_sk integer NOT NULL,
    supplier_sk integer NOT NULL,
    queue character varying(100),
    status character varying(100),
    source_created timestamp without time zone NOT NULL,
    source_modified timestamp without time zone NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: job_history_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE job_history_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: job_history_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE job_history_sk_seq OWNED BY job_history.sk;


--
-- Name: job_process_status; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE job_process_status (
    sk integer NOT NULL,
    jobname character varying(255) NOT NULL,
    status character varying(10) NOT NULL,
    description character varying(255),
    lastexecution timestamp without time zone NOT NULL
);


--
-- Name: job_process_status_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE job_process_status_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: job_process_status_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE job_process_status_sk_seq OWNED BY job_process_status.sk;


--
-- Name: last_job; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW last_job AS
 SELECT jh.sk,
    jh.source_id,
    jh.order_id,
    jh.order_sk,
    jh.job_type,
    jh.product_sk,
    jh.supplier_sk,
    jh.queue,
    jh.status,
    jh.source_created,
    jh.source_modified,
    jh.created
   FROM (job_history jh
     LEFT JOIN ( SELECT jh_1.source_id,
            max(jh_1.sk) AS max_sk,
            max(jh_1.source_modified) AS max_date,
            count(*) AS count
           FROM job_history jh_1
          GROUP BY jh_1.source_id) x ON ((x.max_sk = jh.sk)));


--
-- Name: order_history_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE order_history_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: order_history_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE order_history_sk_seq OWNED BY order_history.sk;


--
-- Name: payee; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE payee (
    sk integer NOT NULL,
    storeid character varying(12),
    payeeid character varying(100),
    name character varying(200),
    payeetype character varying(150),
    chainid character varying(100) DEFAULT ''::character varying NOT NULL
);


--
-- Name: payee_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE payee_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: payee_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE payee_sk_seq OWNED BY payee.sk;


--
-- Name: payment; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE payment (
    sk integer NOT NULL,
    paymentid character varying(100),
    createddate timestamp without time zone NOT NULL,
    amount_incl_gst numeric(19,4),
    modified timestamp without time zone,
    amount_gst numeric(19,4),
    storeid character varying(100) DEFAULT ''::character varying,
    payeeid character varying(100) DEFAULT ''::character varying,
    employeeid character varying(100) DEFAULT ''::character varying,
    customerid character varying(100) DEFAULT ''::character varying,
    payment_typeid character varying(100) DEFAULT ''::character varying
);


--
-- Name: payment_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE payment_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: payment_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE payment_sk_seq OWNED BY payment.sk;


--
-- Name: payment_type; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE payment_type (
    sk integer NOT NULL,
    payment_typeid character varying(30) NOT NULL,
    name character varying(100) NOT NULL
);


--
-- Name: payment_type_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE payment_type_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: payment_type_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE payment_type_sk_seq OWNED BY payment_type.sk;


--
-- Name: paymentitem; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE paymentitem (
    storesk integer NOT NULL,
    paymentsk integer NOT NULL,
    invoicesk integer NOT NULL,
    customersk integer NOT NULL,
    payment_typesk integer NOT NULL,
    payment_employeesk integer NOT NULL,
    invoice_employeesk integer NOT NULL,
    payment_createddate timestamp without time zone NOT NULL,
    invoice_createddate timestamp without time zone NOT NULL,
    payment_amount_incl_gst numeric(19,4),
    invoice_amount_incl_gst numeric(19,4),
    modified timestamp without time zone,
    payment_amount_gst numeric(19,4),
    paymentitemid character varying(60) DEFAULT ''::character varying NOT NULL,
    payeesk integer
);


--
-- Name: pms_invoice_orders; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE pms_invoice_orders (
    orderid text,
    invoicesk integer,
    framesk integer,
    right_lenssk integer,
    left_lenssk integer,
    storesk integer,
    pms_invoicenum character varying(100)
);


--
-- Name: prod_gnm; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE prod_gnm (
    storeid character varying(12),
    description character varying(500),
    product_id character varying(255),
    count bigint
);


--
-- Name: prod_multi_store; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE prod_multi_store (
    pname text,
    count bigint
);


--
-- Name: product_gnm; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE product_gnm (
    sk integer NOT NULL,
    productid character varying(500),
    type character varying(100),
    datecreated timestamp without time zone,
    name character varying(500),
    description character varying(500),
    retailprice numeric(19,4),
    wholesaleprice numeric(19,4),
    modified timestamp without time zone,
    last_purchase timestamp without time zone,
    last_sale timestamp without time zone,
    quantity integer,
    frame_model character varying(100) DEFAULT ''::character varying,
    frame_colour character varying(100) DEFAULT ''::character varying,
    frame_brand character varying(100) DEFAULT ''::character varying,
    frame_temple numeric(8,2) DEFAULT 0,
    frame_bridge numeric(8,2) DEFAULT 0,
    frame_eye_size numeric(8,2) DEFAULT 0,
    frame_depth numeric(8,2) DEFAULT 0,
    frame_material character varying(100) DEFAULT ''::character varying,
    lens_type character varying(100) DEFAULT ''::character varying,
    lens_refractive_index numeric(19,4) DEFAULT 0,
    lens_stock_grind character varying(10) DEFAULT ''::character varying,
    lens_size numeric(4,2) DEFAULT 0,
    lens_segment_size numeric(4,2) DEFAULT 0,
    internal_sku character varying(100),
    distributor character varying(100),
    created timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: product_gnm_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE product_gnm_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: product_gnm_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE product_gnm_sk_seq OWNED BY product_gnm.sk;


--
-- Name: product_price_history; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE product_price_history (
    id bigint NOT NULL,
    product_id character varying(8) NOT NULL,
    supplier_cost numeric(19,2),
    supplier_cost_gst numeric(19,2),
    actual_cost numeric(19,2),
    actual_cost_gst numeric(19,2),
    supplier_rrp numeric(19,2),
    supplier_rrp_gst numeric(19,2),
    calculated_retail_price numeric(19,2),
    calculated_retail_price_gst numeric(19,2),
    override_retail_price numeric(19,2),
    override_retail_price_gst numeric(19,2),
    retail_price numeric(19,2),
    retail_price_gst numeric(19,2),
    effective_from_time timestamp without time zone NOT NULL,
    effective_to_time timestamp without time zone
);


--
-- Name: product_price_history_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE product_price_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: product_price_history_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE product_price_history_id_seq OWNED BY product_price_history.id;


--
-- Name: schema_version; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE schema_version (
    installed_rank integer NOT NULL,
    version character varying(50),
    description character varying(200) NOT NULL,
    type character varying(20) NOT NULL,
    script character varying(1000) NOT NULL,
    checksum integer,
    installed_by character varying(100) NOT NULL,
    installed_on timestamp without time zone DEFAULT now() NOT NULL,
    execution_time integer NOT NULL,
    success boolean NOT NULL
);


--
-- Name: spectaclejob; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE spectaclejob (
    sk integer NOT NULL,
    invoicesk integer,
    framesk integer,
    right_lenssk integer,
    left_lenssk integer,
    storesk integer NOT NULL,
    createddate timestamp without time zone NOT NULL,
    orderdate timestamp without time zone,
    receiveddate timestamp without time zone,
    pickupdate timestamp without time zone,
    modified timestamp without time zone NOT NULL,
    spectaclejobid character varying(60) DEFAULT ''::character varying NOT NULL,
    pms_invoicenum character varying(100)
);


--
-- Name: status; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE status (
    sk integer NOT NULL,
    statusid character varying(30) NOT NULL,
    name character varying(100) NOT NULL
);


--
-- Name: status_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE status_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: status_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE status_sk_seq OWNED BY status.sk;


--
-- Name: stock_price_history_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE stock_price_history_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: stock_price_history_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE stock_price_history_sk_seq OWNED BY stock_price_history.sk;


--
-- Name: store; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE store (
    sk integer NOT NULL,
    storeid character varying(12) NOT NULL,
    name character varying(100) NOT NULL,
    chainid character varying(100) DEFAULT ''::character varying NOT NULL,
    isactive boolean DEFAULT false NOT NULL,
    store_nk character varying(60),
    acquisition_date timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL
);


--
-- Name: store_orders_period; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE store_orders_period (
    store_sk integer,
    name character varying(100),
    order_type character varying(100),
    first_order_date timestamp without time zone,
    last_order_date timestamp without time zone,
    total bigint
);


--
-- Name: store_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE store_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: store_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE store_sk_seq OWNED BY store.sk;


--
-- Name: store_stock_history; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE store_stock_history (
    sk integer NOT NULL,
    store_sk integer NOT NULL,
    source_id character varying(100) NOT NULL,
    takeon_storeproduct_sk integer NOT NULL,
    storeproduct_sk integer NOT NULL,
    total integer NOT NULL,
    prior_total integer NOT NULL,
    reason character varying(20) NOT NULL,
    comments character varying(500),
    employee_sk integer,
    created timestamp without time zone DEFAULT now()
);


--
-- Name: store_stock_history_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE store_stock_history_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: store_stock_history_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE store_stock_history_sk_seq OWNED BY store_stock_history.sk;


--
-- Name: store_stockmovement_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE store_stockmovement_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: store_stockmovement_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE store_stockmovement_sk_seq OWNED BY store_stockmovement.sk;


--
-- Name: storeproduct_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE storeproduct_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: storeproduct_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE storeproduct_sk_seq OWNED BY storeproduct.sk;


--
-- Name: supplier_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE supplier_sk_seq
    START WITH 60
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: supplier; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE supplier (
    sk integer DEFAULT nextval('supplier_sk_seq'::regclass) NOT NULL,
    externalref bigint NOT NULL,
    supplierid character varying(255) NOT NULL,
    name character varying(255) NOT NULL
);


--
-- Name: supplierproduct_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE supplierproduct_sk_seq
    START WITH 500
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: supplierproduct; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE supplierproduct (
    sk integer DEFAULT nextval('supplierproduct_sk_seq'::regclass) NOT NULL,
    suppliersku character varying(255) NOT NULL,
    suppliersk integer NOT NULL,
    frame_model character varying(255),
    frame_colour character varying(100),
    internal_sku character varying(100),
    frame_brand character varying(100)
);


--
-- Name: t1; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW t1 AS
 SELECT d.sk,
    d.date_dim_id,
    d.date_actual,
    d.epoch,
    d.day_suffix,
    d.day_name,
    d.day_of_week,
    d.day_of_month,
    d.day_of_quarter,
    d.day_of_year,
    d.week_of_month,
    d.week_of_year,
    d.week_of_year_iso,
    d.month_actual,
    d.month_name,
    d.month_name_abbreviated,
    d.year_month_name,
    d.quarter_actual,
    d.quarter_name,
    d.year_calendar,
    d.year_calendar_name,
    d.year_financial,
    d.year_financial_name,
    d.first_day_of_week,
    d.last_day_of_week,
    d.first_day_of_month,
    d.last_day_of_month,
    d.first_day_of_quarter,
    d.last_day_of_quarter,
    d.first_day_of_year,
    d.last_day_of_year,
    d.yyyymm,
    d.yyyyq,
    d.weekend_indr,
    m.adjustment AS measure
   FROM (store_stockmovement m
     JOIN d_date d ON ((m.adjustmentdate_sk = d.sk)));


--
-- Name: takeon_product; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE takeon_product (
    sk integer NOT NULL,
    store_sk integer NOT NULL,
    supplier_sk integer NOT NULL,
    source_id character varying(100) NOT NULL,
    supplier_sku character varying(100) NOT NULL,
    name character varying(500) NOT NULL,
    description character varying(500) NOT NULL,
    model character varying(500) NOT NULL,
    brand character varying(500) NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: takeon_product_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE takeon_product_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: takeon_product_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE takeon_product_sk_seq OWNED BY takeon_product.sk;


--
-- Name: test; Type: MATERIALIZED VIEW; Schema: public; Owner: -
--

CREATE MATERIALIZED VIEW test AS
 SELECT tt.sk,
    tt.today,
    tt.revenue
   FROM ( SELECT d.sk,
            d.date_actual AS today,
            sum(i.amount_gst) AS revenue
           FROM (invoiceitem i
             JOIN d_date d ON ((i.createddate_sk = d.sk)))
          GROUP BY d.sk, d.date_actual) tt
  WITH NO DATA;


--
-- Name: tmp_storeprod1; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE tmp_storeprod1 (
    sk integer,
    pname text,
    productid character varying(500),
    storeid character varying(12),
    name character varying(500),
    description character varying(500),
    internal_sku character varying(100)
);


--
-- Name: unmatched_orders; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE unmatched_orders (
    sk integer,
    store_sk integer,
    customer_sk integer,
    source_id character varying(100),
    order_type character varying(100),
    queue character varying(100),
    status character varying(100),
    source_created timestamp without time zone,
    source_modified timestamp without time zone,
    created timestamp without time zone
);


--
-- Name: v_cogs_first_receipt; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW v_cogs_first_receipt AS
 SELECT y.sk,
    y.adjustmentdate,
    y.product_gnm_sk,
    y.adjustment,
    y.actual_cost,
    ((y.adjustment)::numeric * y.actual_cost) AS actual_cost_value,
    ((y.adjustment)::numeric * y.actual_cost) AS moving_actual_cost_value,
    y.actual_cost AS moving_cost_per_unit,
    ( SELECT im.sk
           FROM store_stockmovement im
          WHERE ((im.product_gnm_sk = y.product_gnm_sk) AND (im.adjustmentdate >= y.adjustmentdate) AND (im.sk <> y.sk))
          ORDER BY im.adjustmentdate, im.sk
         LIMIT 1) AS n1
   FROM ( SELECT t.sk,
            t.store_sk,
            t.source_id,
            t.takeon_storeproduct_sk,
            t.adjustmentdate,
            t.stock_price_history_sk,
            t.adjustment,
            t.adjustmenttype,
            t.reason,
            t.invoice_sk,
            t.customer_sk,
            t.avg_cost_price,
            t.shipping_ref,
            t.shipping_operator,
            t.comments,
            t.employee_sk,
            t.created,
            t.adjustmentdate_sk,
            t.product_gnm_sk,
            t.adjustment_type_sk,
            t.actual_cost,
            row_number() OVER (PARTITION BY t.product_gnm_sk ORDER BY t.adjustmentdate, t.sk) AS r
           FROM ( SELECT m.sk,
                    m.store_sk,
                    m.source_id,
                    m.takeon_storeproduct_sk,
                    m.adjustmentdate,
                    m.stock_price_history_sk,
                    m.adjustment,
                    m.adjustmenttype,
                    m.reason,
                    m.invoice_sk,
                    m.customer_sk,
                    m.avg_cost_price,
                    m.shipping_ref,
                    m.shipping_operator,
                    m.comments,
                    m.employee_sk,
                    m.created,
                    m.adjustmentdate_sk,
                    m.product_gnm_sk,
                    m.adjustment_type_sk,
                    ( SELECT h.actual_cost
                           FROM stock_price_history h
                          WHERE ((h.product_gnm_sk = m.product_gnm_sk) AND (h.created <= m.adjustmentdate))
                          ORDER BY h.created DESC
                         LIMIT 1) AS actual_cost
                   FROM (store_stockmovement m
                     JOIN adjustment_type a ON (((m.adjustment_type_sk = a.sk) AND ((a.adjustment_typeid)::text = 'RESTOCK'::text))))
                  WHERE (m.product_gnm_sk <> 0)) t
          WHERE (t.actual_cost IS NOT NULL)) y
  WHERE (y.r = 1);


--
-- Name: vbyd_invoice; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW vbyd_invoice AS
 SELECT date_part('year'::text, sm.createddate) AS year,
    date_part('doy'::text, sm.createddate) AS doy,
    date_part('month'::text, sm.createddate) AS month,
    date_part('dow'::text, sm.createddate) AS dow,
    date_part('day'::text, sm.createddate) AS day,
    sm.sk
   FROM invoice sm
  GROUP BY (date_part('year'::text, sm.createddate)), (date_part('doy'::text, sm.createddate)), (date_part('month'::text, sm.createddate)), (date_part('dow'::text, sm.createddate)), (date_part('day'::text, sm.createddate)), sm.sk;


--
-- Name: vbyd_invoiceitem; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW vbyd_invoiceitem AS
 SELECT date_part('year'::text, sm.createddate) AS year,
    date_part('doy'::text, sm.createddate) AS doy,
    date_part('month'::text, sm.createddate) AS month,
    date_part('dow'::text, sm.createddate) AS dow,
    date_part('day'::text, sm.createddate) AS day,
    sm.invoicesk
   FROM invoiceitem_old sm
  GROUP BY (date_part('year'::text, sm.createddate)), (date_part('doy'::text, sm.createddate)), (date_part('month'::text, sm.createddate)), (date_part('dow'::text, sm.createddate)), (date_part('day'::text, sm.createddate)), sm.invoicesk;


--
-- Name: vbyd_job_history; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW vbyd_job_history AS
 SELECT date_part('year'::text, sm.source_created) AS year,
    date_part('doy'::text, sm.source_created) AS doy,
    date_part('month'::text, sm.source_created) AS month,
    date_part('dow'::text, sm.source_created) AS dow,
    date_part('day'::text, sm.source_created) AS day,
    sm.sk
   FROM job_history sm
  GROUP BY (date_part('year'::text, sm.source_created)), (date_part('doy'::text, sm.source_created)), (date_part('month'::text, sm.source_created)), (date_part('dow'::text, sm.source_created)), (date_part('day'::text, sm.source_created)), sm.sk;


--
-- Name: vbyd_order_history; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW vbyd_order_history AS
 SELECT date_part('year'::text, sm.source_created) AS year,
    date_part('doy'::text, sm.source_created) AS doy,
    date_part('month'::text, sm.source_created) AS month,
    date_part('dow'::text, sm.source_created) AS dow,
    date_part('day'::text, sm.source_created) AS day,
    sm.sk
   FROM order_history sm
  GROUP BY (date_part('year'::text, sm.source_created)), (date_part('doy'::text, sm.source_created)), (date_part('month'::text, sm.source_created)), (date_part('dow'::text, sm.source_created)), (date_part('day'::text, sm.source_created)), sm.sk;


--
-- Name: vbyd_stock_price_history; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW vbyd_stock_price_history AS
 SELECT date_part('year'::text, sm.created) AS year,
    date_part('doy'::text, sm.created) AS doy,
    date_part('month'::text, sm.created) AS month,
    date_part('dow'::text, sm.created) AS dow,
    date_part('day'::text, sm.created) AS day,
    sm.sk
   FROM stock_price_history sm
  GROUP BY (date_part('year'::text, sm.created)), (date_part('doy'::text, sm.created)), (date_part('month'::text, sm.created)), (date_part('dow'::text, sm.created)), (date_part('day'::text, sm.created)), sm.sk;


--
-- Name: vbyd_store_stockmovement; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW vbyd_store_stockmovement AS
 SELECT date_part('year'::text, sm.adjustmentdate) AS year,
    date_part('doy'::text, sm.adjustmentdate) AS doy,
    date_part('month'::text, sm.adjustmentdate) AS month,
    date_part('dow'::text, sm.adjustmentdate) AS dow,
    date_part('day'::text, sm.adjustmentdate) AS day,
    sm.sk
   FROM store_stockmovement sm
  GROUP BY (date_part('year'::text, sm.adjustmentdate)), (date_part('doy'::text, sm.adjustmentdate)), (date_part('month'::text, sm.adjustmentdate)), (date_part('dow'::text, sm.adjustmentdate)), (date_part('day'::text, sm.adjustmentdate)), sm.sk;


--
-- Name: viewsmbyperiod; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW viewsmbyperiod AS
 SELECT date_part('year'::text, sm.adjustmentdate) AS year,
    date_part('doy'::text, sm.adjustmentdate) AS doy,
    date_part('month'::text, sm.adjustmentdate) AS month,
    date_part('dow'::text, sm.adjustmentdate) AS dow,
    date_part('day'::text, sm.adjustmentdate) AS day,
    sm.sk
   FROM store_stockmovement sm
  GROUP BY (date_part('year'::text, sm.adjustmentdate)), (date_part('doy'::text, sm.adjustmentdate)), (date_part('month'::text, sm.adjustmentdate)), (date_part('dow'::text, sm.adjustmentdate)), (date_part('day'::text, sm.adjustmentdate)), sm.sk;


--
-- Name: adjustment_type sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY adjustment_type ALTER COLUMN sk SET DEFAULT nextval('adjustment_type_sk_seq'::regclass);


--
-- Name: appointment sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY appointment ALTER COLUMN sk SET DEFAULT nextval('appointment_sk_seq'::regclass);


--
-- Name: appointment_type sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY appointment_type ALTER COLUMN sk SET DEFAULT nextval('appointment_type_sk_seq'::regclass);


--
-- Name: consult sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY consult ALTER COLUMN sk SET DEFAULT nextval('consult_sk_seq'::regclass);


--
-- Name: consult_type sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY consult_type ALTER COLUMN sk SET DEFAULT nextval('consult_type_sk_seq'::regclass);


--
-- Name: consultitem sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY consultitem ALTER COLUMN sk SET DEFAULT nextval('consultitem_sk_seq'::regclass);


--
-- Name: customer sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY customer ALTER COLUMN sk SET DEFAULT nextval('customer_sk_seq'::regclass);


--
-- Name: customer_ext sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY customer_ext ALTER COLUMN sk SET DEFAULT nextval('customer_ext_sk_seq'::regclass);


--
-- Name: d_date sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY d_date ALTER COLUMN sk SET DEFAULT nextval('d_date_sk_seq'::regclass);


--
-- Name: discountcode sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY discountcode ALTER COLUMN sk SET DEFAULT nextval('discountcode_sk_seq'::regclass);


--
-- Name: employee sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY employee ALTER COLUMN sk SET DEFAULT nextval('employee_sk_seq'::regclass);


--
-- Name: expense sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY expense ALTER COLUMN sk SET DEFAULT nextval('expense_sk_seq'::regclass);


--
-- Name: expense_type sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY expense_type ALTER COLUMN sk SET DEFAULT nextval('expense_type_sk_seq'::regclass);


--
-- Name: invoice sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY invoice ALTER COLUMN sk SET DEFAULT nextval('invoice_sk_seq'::regclass);


--
-- Name: invoice_order_job sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY invoice_order_job ALTER COLUMN sk SET DEFAULT nextval('invoice_order_job_sk_seq'::regclass);


--
-- Name: invoice_orders sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY invoice_orders ALTER COLUMN sk SET DEFAULT nextval('invoice_orders_sk_seq'::regclass);


--
-- Name: job_history sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY job_history ALTER COLUMN sk SET DEFAULT nextval('job_history_sk_seq'::regclass);


--
-- Name: job_process_status sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY job_process_status ALTER COLUMN sk SET DEFAULT nextval('job_process_status_sk_seq'::regclass);


--
-- Name: order_history sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY order_history ALTER COLUMN sk SET DEFAULT nextval('order_history_sk_seq'::regclass);


--
-- Name: payee sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY payee ALTER COLUMN sk SET DEFAULT nextval('payee_sk_seq'::regclass);


--
-- Name: payment sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY payment ALTER COLUMN sk SET DEFAULT nextval('payment_sk_seq'::regclass);


--
-- Name: payment_type sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY payment_type ALTER COLUMN sk SET DEFAULT nextval('payment_type_sk_seq'::regclass);


--
-- Name: product_gnm sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY product_gnm ALTER COLUMN sk SET DEFAULT nextval('product_gnm_sk_seq'::regclass);


--
-- Name: product_price_history id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY product_price_history ALTER COLUMN id SET DEFAULT nextval('product_price_history_id_seq'::regclass);


--
-- Name: status sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY status ALTER COLUMN sk SET DEFAULT nextval('status_sk_seq'::regclass);


--
-- Name: stock_price_history sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY stock_price_history ALTER COLUMN sk SET DEFAULT nextval('stock_price_history_sk_seq'::regclass);


--
-- Name: store sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY store ALTER COLUMN sk SET DEFAULT nextval('store_sk_seq'::regclass);


--
-- Name: store_stock_history sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stock_history ALTER COLUMN sk SET DEFAULT nextval('store_stock_history_sk_seq'::regclass);


--
-- Name: store_stockmovement sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stockmovement ALTER COLUMN sk SET DEFAULT nextval('store_stockmovement_sk_seq'::regclass);


--
-- Name: storeproduct sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY storeproduct ALTER COLUMN sk SET DEFAULT nextval('storeproduct_sk_seq'::regclass);


--
-- Name: takeon_product sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY takeon_product ALTER COLUMN sk SET DEFAULT nextval('takeon_product_sk_seq'::regclass);


--
-- Name: adjustment_type adjustment_type_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY adjustment_type
    ADD CONSTRAINT adjustment_type_pkey PRIMARY KEY (sk);


--
-- Name: appointment appointment_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY appointment
    ADD CONSTRAINT appointment_pkey PRIMARY KEY (sk);


--
-- Name: appointment_type appointment_type_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY appointment_type
    ADD CONSTRAINT appointment_type_pkey PRIMARY KEY (sk);


--
-- Name: consult_type constult_type_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY consult_type
    ADD CONSTRAINT constult_type_pkey PRIMARY KEY (sk);


--
-- Name: consult consult_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY consult
    ADD CONSTRAINT consult_pkey PRIMARY KEY (sk);


--
-- Name: consultitem consultitem_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY consultitem
    ADD CONSTRAINT consultitem_pkey PRIMARY KEY (sk);


--
-- Name: customer_ext customer_ext_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY customer_ext
    ADD CONSTRAINT customer_ext_pkey PRIMARY KEY (sk);


--
-- Name: customer customer_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY customer
    ADD CONSTRAINT customer_pkey PRIMARY KEY (sk);


--
-- Name: expense expense_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY expense
    ADD CONSTRAINT expense_pkey PRIMARY KEY (sk);


--
-- Name: expense_type expense_type_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY expense_type
    ADD CONSTRAINT expense_type_pkey PRIMARY KEY (sk);


--
-- Name: supplier externalref_ukey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY supplier
    ADD CONSTRAINT externalref_ukey UNIQUE (externalref);


--
-- Name: invoice_order_job invoice_order_job_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY invoice_order_job
    ADD CONSTRAINT invoice_order_job_pkey PRIMARY KEY (sk);


--
-- Name: invoice_orders invoice_orders_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY invoice_orders
    ADD CONSTRAINT invoice_orders_pkey PRIMARY KEY (sk);


--
-- Name: invoice invoice_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY invoice
    ADD CONSTRAINT invoice_pkey PRIMARY KEY (sk);


--
-- Name: invoiceitem_old invoiceitem_skey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY invoiceitem_old
    ADD CONSTRAINT invoiceitem_skey PRIMARY KEY (invoiceitemid);


--
-- Name: job_history job_history_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY job_history
    ADD CONSTRAINT job_history_pkey PRIMARY KEY (sk);


--
-- Name: order_history order_history_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY order_history
    ADD CONSTRAINT order_history_pkey PRIMARY KEY (sk);


--
-- Name: payee payee_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY payee
    ADD CONSTRAINT payee_pkey PRIMARY KEY (sk);


--
-- Name: payment payment_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY payment
    ADD CONSTRAINT payment_pkey PRIMARY KEY (sk);


--
-- Name: payment_type payment_type_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY payment_type
    ADD CONSTRAINT payment_type_pkey PRIMARY KEY (sk);


--
-- Name: paymentitem paymentitem_skey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY paymentitem
    ADD CONSTRAINT paymentitem_skey PRIMARY KEY (paymentitemid);


--
-- Name: d_date pk_d_date; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY d_date
    ADD CONSTRAINT pk_d_date PRIMARY KEY (sk);


--
-- Name: employee pk_employee; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY employee
    ADD CONSTRAINT pk_employee PRIMARY KEY (sk);


--
-- Name: product_gnm product_gnm_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY product_gnm
    ADD CONSTRAINT product_gnm_pkey PRIMARY KEY (sk);


--
-- Name: schema_version schema_version_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY schema_version
    ADD CONSTRAINT schema_version_pk PRIMARY KEY (installed_rank);


--
-- Name: status status_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY status
    ADD CONSTRAINT status_pkey PRIMARY KEY (sk);


--
-- Name: stock_price_history stock_price_history_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY stock_price_history
    ADD CONSTRAINT stock_price_history_pkey PRIMARY KEY (sk);


--
-- Name: store store_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store
    ADD CONSTRAINT store_pkey PRIMARY KEY (sk);


--
-- Name: store_stock_history store_stock_history_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stock_history
    ADD CONSTRAINT store_stock_history_pkey PRIMARY KEY (sk);


--
-- Name: store_stockmovement store_stockmovement_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stockmovement
    ADD CONSTRAINT store_stockmovement_pkey PRIMARY KEY (sk);


--
-- Name: storeproduct storeproduct_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY storeproduct
    ADD CONSTRAINT storeproduct_pkey PRIMARY KEY (sk);


--
-- Name: supplier supplier_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY supplier
    ADD CONSTRAINT supplier_pkey PRIMARY KEY (sk);


--
-- Name: supplierproduct supplierproduct_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY supplierproduct
    ADD CONSTRAINT supplierproduct_pkey PRIMARY KEY (sk);


--
-- Name: takeon_product takeon_product_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY takeon_product
    ADD CONSTRAINT takeon_product_pkey PRIMARY KEY (sk);


--
-- Name: appointment_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX appointment_nkey ON appointment USING btree (appointmentid);


--
-- Name: appointment_storesk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX appointment_storesk ON appointment USING hash (storesk);


--
-- Name: appointment_type_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX appointment_type_nkey ON appointment_type USING btree (appointment_typeid);


--
-- Name: consult_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX consult_nkey ON consult USING btree (consultid);


--
-- Name: consult_type_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX consult_type_nkey ON consult_type USING btree (consult_typeid);


--
-- Name: customer_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX customer_nkey ON customer USING btree (customerid);


--
-- Name: employee_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX employee_nkey ON employee USING btree (employeeid);


--
-- Name: expense_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX expense_nkey ON expense USING btree (expenseid);


--
-- Name: expense_type_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX expense_type_nkey ON expense_type USING btree (expense_typeid);


--
-- Name: fki_supplierproduct_supplier_fkey; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX fki_supplierproduct_supplier_fkey ON supplierproduct USING btree (suppliersk);


--
-- Name: idx_adjustment_type_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX idx_adjustment_type_id ON adjustment_type USING btree (adjustment_typeid);


--
-- Name: idx_adjustment_type_name; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_adjustment_type_name ON adjustment_type USING btree (name);


--
-- Name: idx_d_date_dateactual; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_d_date_dateactual ON d_date USING btree (date_actual);


--
-- Name: idx_d_date_day_of_year; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_d_date_day_of_year ON d_date USING btree (day_of_year);


--
-- Name: idx_d_date_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX idx_d_date_id ON d_date USING btree (date_dim_id);


--
-- Name: idx_d_date_year_calendar; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_d_date_year_calendar ON d_date USING btree (year_calendar);


--
-- Name: idx_invoiceitemwithdate_createddate; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_invoiceitemwithdate_createddate ON invoiceitem USING btree (createddate);


--
-- Name: idx_invoiceitemwithdate_createddate_sk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_invoiceitemwithdate_createddate_sk ON invoiceitem USING btree (createddate_sk);


--
-- Name: idx_invoiceitemwithdate_customer_sk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_invoiceitemwithdate_customer_sk ON invoiceitem USING btree (customersk);


--
-- Name: idx_invoiceitemwithdate_employee_sk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_invoiceitemwithdate_employee_sk ON invoiceitem USING btree (employeesk);


--
-- Name: idx_invoiceitemwithdate_invoice_sk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_invoiceitemwithdate_invoice_sk ON invoiceitem USING btree (invoicesk);


--
-- Name: idx_invoiceitemwithdate_payee_sk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_invoiceitemwithdate_payee_sk ON invoiceitem USING btree (payeesk);


--
-- Name: idx_invoiceitemwithdate_product_sk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_invoiceitemwithdate_product_sk ON invoiceitem USING btree (productsk);


--
-- Name: idx_invoiceitemwithdate_serviceemployee_sk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_invoiceitemwithdate_serviceemployee_sk ON invoiceitem USING btree (serviceemployeesk);


--
-- Name: idx_invoiceitemwithdate_store_sk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_invoiceitemwithdate_store_sk ON invoiceitem USING btree (storesk);


--
-- Name: idx_model; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_model ON supplierproduct USING btree (frame_model);


--
-- Name: idx_order_history_sourceid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_order_history_sourceid ON order_history USING btree (source_id);


--
-- Name: idx_orderid_order_history; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_orderid_order_history ON order_history USING btree (source_id);


--
-- Name: idx_orderid_pms_invoice_orders; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_orderid_pms_invoice_orders ON pms_invoice_orders USING btree (orderid);


--
-- Name: idx_stock_price_history_gnmproduct_date; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_stock_price_history_gnmproduct_date ON stock_price_history USING btree (product_gnm_sk, created DESC);


--
-- Name: idx_stock_price_history_source_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX idx_stock_price_history_source_id ON stock_price_history USING btree (source_id);


--
-- Name: idx_stock_price_history_takeon_product_date; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_stock_price_history_takeon_product_date ON stock_price_history USING btree (takeon_storeproduct_sk, created DESC);


--
-- Name: idx_store_nk; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX idx_store_nk ON store USING btree (store_nk);


--
-- Name: idx_store_stock_history_storeproduct_date; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_store_stock_history_storeproduct_date ON store_stock_history USING btree (storeproduct_sk, created DESC);


--
-- Name: idx_store_stock_history_takeon_product_date; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_store_stock_history_takeon_product_date ON store_stock_history USING btree (takeon_storeproduct_sk, created DESC);


--
-- Name: idx_store_stockmovement_adjustmentdate; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_store_stockmovement_adjustmentdate ON store_stockmovement USING btree (product_gnm_sk, adjustmentdate);


--
-- Name: idx_store_stockmovement_adjustmentdate2; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_store_stockmovement_adjustmentdate2 ON store_stockmovement USING btree (takeon_storeproduct_sk, adjustmentdate);


--
-- Name: idx_store_stockmovement_adjustmentdate_sk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_store_stockmovement_adjustmentdate_sk ON store_stockmovement USING btree (adjustmentdate_sk);


--
-- Name: idx_store_stockmovement_adjustmenttype_sk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_store_stockmovement_adjustmenttype_sk ON store_stockmovement USING btree (adjustment_type_sk);


--
-- Name: idx_store_stockmovement_invoice_sk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_store_stockmovement_invoice_sk ON store_stockmovement USING btree (invoice_sk);


--
-- Name: idx_store_stockmovement_source_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX idx_store_stockmovement_source_id ON store_stockmovement USING btree (source_id);


--
-- Name: idx_store_stockmovement_store_sk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_store_stockmovement_store_sk ON store_stockmovement USING btree (store_sk);


--
-- Name: idx_store_stockmovement_takeon_storeproduct_sk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_store_stockmovement_takeon_storeproduct_sk ON store_stockmovement USING btree (takeon_storeproduct_sk);


--
-- Name: idx_suppliersku; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_suppliersku ON supplierproduct USING btree (suppliersku);


--
-- Name: invoice_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX invoice_nkey ON invoice USING btree (invoiceid);


--
-- Name: invoice_storesk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX invoice_storesk ON invoice USING hash (storesk);


--
-- Name: invoiceitem_createddate_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX invoiceitem_createddate_idx ON invoiceitem_old USING btree (createddate);


--
-- Name: invoiceitem_storesk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX invoiceitem_storesk ON invoiceitem_old USING hash (storesk);


--
-- Name: payee_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX payee_nkey ON payee USING btree (payeeid);


--
-- Name: payment_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX payment_nkey ON payment USING btree (paymentid);


--
-- Name: payment_storeid_ix; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX payment_storeid_ix ON payment USING btree (storeid);


--
-- Name: payment_type_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX payment_type_nkey ON payment_type USING btree (payment_typeid);


--
-- Name: paymentitem_storesk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX paymentitem_storesk ON paymentitem USING hash (storesk);


--
-- Name: productid_on_product; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX productid_on_product ON storeproduct USING btree (productid);


--
-- Name: productid_on_product_gnm; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX productid_on_product_gnm ON product_gnm USING btree (productid);


--
-- Name: schema_version_s_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX schema_version_s_idx ON schema_version USING btree (success);


--
-- Name: status_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX status_nkey ON status USING btree (statusid);


--
-- Name: store_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX store_nkey ON store USING btree (storeid);


--
-- Name: storeproduct_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX storeproduct_nkey ON storeproduct USING btree (productid);


--
-- Name: invoice_order_job invoice_order_job_invoice_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY invoice_order_job
    ADD CONSTRAINT invoice_order_job_invoice_sk_fkey FOREIGN KEY (invoice_sk) REFERENCES invoice(sk);


--
-- Name: invoice_order_job invoice_order_job_store_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY invoice_order_job
    ADD CONSTRAINT invoice_order_job_store_sk_fkey FOREIGN KEY (store_sk) REFERENCES store(sk);


--
-- Name: invoice_orders invoice_orders_invoice_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY invoice_orders
    ADD CONSTRAINT invoice_orders_invoice_sk_fkey FOREIGN KEY (invoice_sk) REFERENCES invoice(sk);


--
-- Name: invoice_orders invoice_orders_order_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY invoice_orders
    ADD CONSTRAINT invoice_orders_order_sk_fkey FOREIGN KEY (order_sk) REFERENCES order_history(sk);


--
-- Name: invoice_orders invoice_orders_store_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY invoice_orders
    ADD CONSTRAINT invoice_orders_store_sk_fkey FOREIGN KEY (store_sk) REFERENCES store(sk);


--
-- Name: job_history job_history_product_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY job_history
    ADD CONSTRAINT job_history_product_sk_fkey FOREIGN KEY (product_sk) REFERENCES product_gnm(sk);


--
-- Name: job_history job_history_supplier_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY job_history
    ADD CONSTRAINT job_history_supplier_sk_fkey FOREIGN KEY (supplier_sk) REFERENCES supplier(sk);


--
-- Name: order_history order_history_customer_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY order_history
    ADD CONSTRAINT order_history_customer_sk_fkey FOREIGN KEY (customer_sk) REFERENCES customer(sk);


--
-- Name: order_history order_history_store_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY order_history
    ADD CONSTRAINT order_history_store_sk_fkey FOREIGN KEY (store_sk) REFERENCES store(sk);


--
-- Name: stock_price_history stock_price_history_store_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY stock_price_history
    ADD CONSTRAINT stock_price_history_store_sk_fkey FOREIGN KEY (store_sk) REFERENCES store(sk);


--
-- Name: stock_price_history stock_price_history_takeon_storeproduct_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY stock_price_history
    ADD CONSTRAINT stock_price_history_takeon_storeproduct_sk_fkey FOREIGN KEY (takeon_storeproduct_sk) REFERENCES storeproduct(sk);


--
-- Name: store_stock_history store_stock_history_employee_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stock_history
    ADD CONSTRAINT store_stock_history_employee_sk_fkey FOREIGN KEY (employee_sk) REFERENCES employee(sk);


--
-- Name: store_stock_history store_stock_history_store_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stock_history
    ADD CONSTRAINT store_stock_history_store_sk_fkey FOREIGN KEY (store_sk) REFERENCES store(sk);


--
-- Name: store_stock_history store_stock_history_storeproduct_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stock_history
    ADD CONSTRAINT store_stock_history_storeproduct_sk_fkey FOREIGN KEY (storeproduct_sk) REFERENCES storeproduct(sk);


--
-- Name: store_stock_history store_stock_history_takeon_storeproduct_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stock_history
    ADD CONSTRAINT store_stock_history_takeon_storeproduct_sk_fkey FOREIGN KEY (takeon_storeproduct_sk) REFERENCES storeproduct(sk);


--
-- Name: store_stockmovement store_stockmovement_customer_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stockmovement
    ADD CONSTRAINT store_stockmovement_customer_sk_fkey FOREIGN KEY (customer_sk) REFERENCES customer(sk);


--
-- Name: store_stockmovement store_stockmovement_employee_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stockmovement
    ADD CONSTRAINT store_stockmovement_employee_sk_fkey FOREIGN KEY (employee_sk) REFERENCES employee(sk);


--
-- Name: store_stockmovement store_stockmovement_invoice_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stockmovement
    ADD CONSTRAINT store_stockmovement_invoice_sk_fkey FOREIGN KEY (invoice_sk) REFERENCES invoice(sk);


--
-- Name: store_stockmovement store_stockmovement_stock_price_history_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stockmovement
    ADD CONSTRAINT store_stockmovement_stock_price_history_sk_fkey FOREIGN KEY (stock_price_history_sk) REFERENCES stock_price_history(sk);


--
-- Name: store_stockmovement store_stockmovement_store_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stockmovement
    ADD CONSTRAINT store_stockmovement_store_sk_fkey FOREIGN KEY (store_sk) REFERENCES store(sk);


--
-- Name: store_stockmovement store_stockmovement_takeon_storeproduct_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stockmovement
    ADD CONSTRAINT store_stockmovement_takeon_storeproduct_sk_fkey FOREIGN KEY (takeon_storeproduct_sk) REFERENCES storeproduct(sk);


--
-- Name: supplierproduct supplierproduct_supplier_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY supplierproduct
    ADD CONSTRAINT supplierproduct_supplier_fkey FOREIGN KEY (suppliersk) REFERENCES supplier(sk);


--
-- Name: takeon_product takeon_product_store_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY takeon_product
    ADD CONSTRAINT takeon_product_store_sk_fkey FOREIGN KEY (store_sk) REFERENCES store(sk);


--
-- Name: takeon_product takeon_product_supplier_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY takeon_product
    ADD CONSTRAINT takeon_product_supplier_sk_fkey FOREIGN KEY (supplier_sk) REFERENCES supplier(sk);


--
-- PostgreSQL database dump complete
--

