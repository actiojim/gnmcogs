
 SET search_path = stage;
 
 drop table if exists job_stage;
 
create table job_stage
(
    sk serial NOT NULL,
	run_id character varying(100),
	
    -- natural source key job_id
    job_id character varying(100),
    
    -- source data order id
    order_id character varying(100),
    customer_id character varying(100),
    store_id character varying(100),
    product_id character varying(100),
    supplier_id character varying(100),
    quantity character varying(100),
    
    -- job type frame or lens
    job_type character varying(100),
    queue character varying(100),
    status character varying(100),
    source_created character varying(100),
    source_modified character varying(100),
    
    -- created date
    created timestamp without time zone NOT null default now(),
    primary key (sk)
);

insert into stage.job_stage (
run_id,
order_id,
customer_id,
store_id,
job_id,
queue,
status,
quantity,
source_created,
source_modified,
job_type,
product_id,
supplier_id)
select  
j.runid,
o.orderid,
o.store_id,
c.customerId, 
j.f->>'id' as jobid
 ,f->'attributes'->>'queue' as queue
 ,f->'attributes'->>'status' as status
 ,f->'attributes'->>'quantity' as quantity
 ,f->'attributes'->>'createdDateTime' as created
 ,f->'attributes'->>'modifiedDateTime' as modified
 ,f->'relationships'->'jobType'->'data'->>'id' as jobtype
 ,f->'relationships'->'product'->'data'->>'id' as productid
 ,f->'relationships'->'supplier'->'data'->>'id' as supplierid 
from  
  (select runid, jsonb_array_elements(jsonb_array_elements(data)->'data') as f from stage.gnm_hub_orders 
--  	where runid in (select runid from stage.gnm_hub_orders where createddate >= '2017-08-20 18:47:46')) j 
  inner join 
	(select runid,f->>'id' as orderid,f->'relationships'->'customer'->'data'->>'id'as store_id, f->'relationships'->'store'->'data'->>'id'as store_id from 
		(select runid,jsonb_array_elements(jsonb_array_elements(y.data)->'included') as f 
			from stage.gnm_hub_orders y 
			where runid in (select runid from stage.gnm_hub_orders where createddate >= '2017-08-20 18:47:46') ) x where f->>'type' = 'order') o on f->'relationships'->'order'->'data'->>'id' = o.orderid 
  inner join 
	(select runid,f->>'id' as id,f->'attributes'->>'customerId' as customerId from 
			(select runid,jsonb_array_elements(jsonb_array_elements(y.data)->'included' ) as f 
					from stage.gnm_hub_orders y 
					where runid in (select runid from stage.gnm_hub_orders where createddate >= '2017-08-20 18:47:46') ) x
		where f->>'type' = 'orderCustomer') c on o.customer_id = c.id
  
 where j.f->>'id'  not in (select job_id from job_stage)




SET search_path = gnm_scire_0816;

select * from stage.job_stage js inner join invoice i on i.



select * from gnm.product 


select * from storeproduct


INSERT INTO gnm_scire_0816.storeproduct
(sk, storeid, productid, "type", datecreated, modified, "name", description, internal_sku, distributor,

retailprice, 
wholesaleprice, 
last_purchase, 
last_sale, 
quantity, 

frame_model, frame_colour, frame_brand, frame_temple, 
frame_bridge, frame_eye_size, frame_depth, frame_material, lens_type, lens_refractive_index, 
lens_stock_grind, lens_size, lens_segment_size, )
VALUES(nextval('storeproduct_sk_seq'::regclass), '', '', '', '', '', '', 0, 0, '', '', '', 0, ''::character varying, ''::character varying, ''::character varying, 0, 0, 0, 0, ''::character varying, ''::character varying, 0, ''::character varying, 0, 0, '', '');


SELECT 0, product_id, "type", created_date_time, , modified_date_time, "name",  description, supplier_sku, supplier_id,

discontinued, supplier_rrp, supplier_rrp_gst, 
supplier_cost, supplier_cost_gst, created_user, modified_user,  apn,
image_id,  brand_id, price_policy_id, actual_cost, actual_cost_gst, calculated_retail_price, 
calculated_retail_price_gst, override_cost, override_cost_gst, price_change_reason, price_error

FROM gnm.product;

select * from "store"

select * from gnm_scire_0816.storeproduct

INSERT INTO gnm_scire_0816.storeproduct
(storeid, productid, "type", datecreated, modified, "name", description, internal_sku )
SELECT 0, product_id, "type", created_date_time, modified_date_time, "name",  description, supplier_sku
FROM gnm.product 
-- where product_id not in (select productid from gnm_scire_0816.storeproduct )

select * from gnm.product
where product_id not in (select productid from gnm_scire_0816.storeproduct )


CREATE UNIQUE INDEX productid_on_product ON gnm_scire_0816.storeproduct (productid);


select * from storeproduct where storeid = '0'

 SET search_path = gnm_scire_0816;
 

INSERT INTO gnm_scire_0816.stock_price_history
(store_sk, source_id, takeon_storeproduct_sk, storeproduct_sk, 
supplier_cost, supplier_cost_gst, 
actual_cost, actual_cost_gst, 
supplier_rrp, supplier_rrp_gst, calculated_retail_price, calculated_retail_price_gst, 
override_retail_price, override_retail_price_gst, 
retail_price, retail_price_gst, 
created)
-- VALUES(nextval('stock_price_history_sk_seq'::regclass), 0, '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, now());
SELECT 
0, id, 0, sp.sk, 
supplier_cost, supplier_cost_gst, 
actual_cost, actual_cost_gst, 
supplier_rrp, supplier_rrp_gst, calculated_retail_price, calculated_retail_price_gst, 
0,0,
0,0,
modified_date_time
FROM gnm.product_cost_history pch inner join gnm_scire_0816.storeproduct sp on sp.productid = pch.product_id



select * from storeproduct sp inner join stock_price_history sph on sph.storeproduct_sk = sp.sk


select * from order_history


select * from stage.job_stage



