--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.3
-- Dumped by pg_dump version 9.6.3

-- Started on 2017-09-26 06:10:13

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

SET default_with_oids = false;

--
-- TOC entry 316 (class 1259 OID 55919)
-- Name: appointment; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE appointment (
    sk integer NOT NULL,
    appointmentid character varying(100),
    storesk integer NOT NULL,
    createddate timestamp without time zone NOT NULL,
    booking_startdate timestamp without time zone NOT NULL,
    booking_enddate timestamp without time zone NOT NULL,
    employeesk integer NOT NULL,
    customersk integer NOT NULL,
    statussk integer NOT NULL,
    appointment_typesk integer NOT NULL,
    mins integer,
    modified timestamp without time zone,
    prospect_firstname character varying(50),
    prospect_lastname character varying(50),
    prospect_title character varying(10),
    prospect_email character varying(150),
    prospect_homephone character varying(150),
    prospect_mobile character varying(150),
    prospect_workphone character varying(150),
    isdeleted boolean DEFAULT false NOT NULL
);


--
-- TOC entry 317 (class 1259 OID 55926)
-- Name: appointment_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE appointment_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2947 (class 0 OID 0)
-- Dependencies: 317
-- Name: appointment_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE appointment_sk_seq OWNED BY appointment.sk;


--
-- TOC entry 318 (class 1259 OID 55928)
-- Name: appointment_type; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE appointment_type (
    sk integer NOT NULL,
    appointment_typeid character varying(30) NOT NULL,
    name character varying(100) NOT NULL
);


--
-- TOC entry 319 (class 1259 OID 55931)
-- Name: appointment_type_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE appointment_type_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2948 (class 0 OID 0)
-- Dependencies: 319
-- Name: appointment_type_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE appointment_type_sk_seq OWNED BY appointment_type.sk;


--
-- TOC entry 320 (class 1259 OID 55933)
-- Name: consult; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE consult (
    sk integer NOT NULL,
    consultid character varying(100),
    consultdate timestamp without time zone NOT NULL,
    storesk integer NOT NULL,
    employeesk integer NOT NULL,
    customersk integer NOT NULL,
    modified timestamp without time zone NOT NULL
);


--
-- TOC entry 321 (class 1259 OID 55936)
-- Name: consult_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE consult_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2949 (class 0 OID 0)
-- Dependencies: 321
-- Name: consult_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE consult_sk_seq OWNED BY consult.sk;


--
-- TOC entry 322 (class 1259 OID 55938)
-- Name: consult_type; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE consult_type (
    sk integer NOT NULL,
    consult_typeid character varying(30) NOT NULL,
    description character varying(100) NOT NULL
);


--
-- TOC entry 323 (class 1259 OID 55941)
-- Name: consult_type_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE consult_type_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2950 (class 0 OID 0)
-- Dependencies: 323
-- Name: consult_type_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE consult_type_sk_seq OWNED BY consult_type.sk;


--
-- TOC entry 324 (class 1259 OID 55943)
-- Name: consultitem; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE consultitem (
    sk integer NOT NULL,
    consultsk integer NOT NULL,
    consult_typesk integer NOT NULL
);


--
-- TOC entry 325 (class 1259 OID 55946)
-- Name: consultitem_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE consultitem_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2951 (class 0 OID 0)
-- Dependencies: 325
-- Name: consultitem_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE consultitem_sk_seq OWNED BY consultitem.sk;


--
-- TOC entry 326 (class 1259 OID 55948)
-- Name: customer; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE customer (
    sk integer NOT NULL,
    createddate timestamp without time zone NOT NULL,
    customerid character varying(500),
    firstname character varying(50) NOT NULL,
    lastname character varying(50),
    middlename character varying(50),
    title character varying(10) NOT NULL,
    gender character varying(10) NOT NULL,
    birthdate date,
    preferredname character varying(50),
    preferredcontactmethod character varying(50),
    email character varying(150) NOT NULL,
    emailcreated timestamp without time zone DEFAULT ('now'::text)::date NOT NULL,
    phonecountryid integer,
    phonecreated timestamp without time zone DEFAULT ('now'::text)::date NOT NULL,
    address1 character varying(250),
    address2 character varying(250),
    address3 character varying(250),
    state character varying(60),
    suburb character varying(60),
    postcode integer,
    firstvisit timestamp without time zone,
    lastvisit timestamp without time zone,
    lastconsult timestamp without time zone,
    lastrecall timestamp without time zone,
    nextrecall timestamp without time zone,
    lastrecalldesc character varying(250),
    nextrecalldesc character varying(250),
    lastoptomseen integer,
    storesk integer DEFAULT 0 NOT NULL,
    healthfund character varying(50),
    modified timestamp without time zone,
    optout_recall boolean DEFAULT false NOT NULL,
    optout_marketing boolean DEFAULT false NOT NULL,
    deceased boolean DEFAULT false NOT NULL,
    homephone character varying(150),
    mobile character varying(150),
    workphone character varying(150),
    isaddrvalid boolean
);


--
-- TOC entry 327 (class 1259 OID 55960)
-- Name: customer_ext; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE customer_ext (
    sk integer NOT NULL,
    customersk integer NOT NULL,
    s_recall character varying(1),
    s_cmsms character varying(1),
    s_cmmobile character varying(1),
    s_cmwphone character varying(1),
    s_cmhphone character varying(1),
    s_cmemail character varying(1),
    s_cmletter character varying(1),
    s_cmfno boolean,
    s_cmnoemail boolean,
    s_apnotif integer,
    s_apreminder integer,
    o_pref_recallint character varying(20),
    o_pref_recallsub character varying(20),
    o_pref_ordercoll character varying(20),
    o_pref_appointments character varying(20),
    o_pref_marketing character varying(20),
    o_pref_telephone character varying(20),
    o_badaddress boolean,
    o_badphone boolean,
    o_bademail boolean,
    o_noemail boolean,
    modified timestamp without time zone,
    o_inactive boolean,
    o_inactive_reason character varying(255)
);


--
-- TOC entry 328 (class 1259 OID 55963)
-- Name: customer_ext_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE customer_ext_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2952 (class 0 OID 0)
-- Dependencies: 328
-- Name: customer_ext_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE customer_ext_sk_seq OWNED BY customer_ext.sk;


--
-- TOC entry 329 (class 1259 OID 55965)
-- Name: customer_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE customer_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2953 (class 0 OID 0)
-- Dependencies: 329
-- Name: customer_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE customer_sk_seq OWNED BY customer.sk;


--
-- TOC entry 330 (class 1259 OID 55967)
-- Name: discountcode; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE discountcode (
    sk integer NOT NULL,
    storeid character varying(12) DEFAULT 'All'::character varying NOT NULL,
    discountitemcode character varying(100) NOT NULL,
    description character varying(150),
    valid_from timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone,
    valid_to timestamp without time zone DEFAULT '2099-12-31 00:00:00'::timestamp without time zone
);


--
-- TOC entry 331 (class 1259 OID 55973)
-- Name: discountcode_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE discountcode_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2954 (class 0 OID 0)
-- Dependencies: 331
-- Name: discountcode_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE discountcode_sk_seq OWNED BY discountcode.sk;


--
-- TOC entry 332 (class 1259 OID 55975)
-- Name: employee; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE employee (
    sk integer NOT NULL,
    createddate timestamp without time zone NOT NULL,
    employeeid character varying(500),
    employeetype character varying(50),
    employmentstartdate date,
    employmentenddate date,
    firstname character varying(50) NOT NULL,
    lastname character varying(50),
    middlename character varying(50),
    title character varying(10) NOT NULL,
    gender character varying(10) NOT NULL,
    birthdate date,
    preferredname character varying(50),
    preferredcontactmethod character varying(10),
    email character varying(150) NOT NULL,
    emailcreated timestamp without time zone DEFAULT ('now'::text)::date NOT NULL,
    phonecountryid integer,
    phonenumber character varying(150),
    phonerawnumber character varying(150),
    phonemobile boolean NOT NULL,
    phonesmsenabled boolean NOT NULL,
    phonefax boolean NOT NULL,
    phonelandline boolean NOT NULL,
    phonecreated timestamp without time zone DEFAULT ('now'::text)::date NOT NULL,
    storesk integer DEFAULT 0 NOT NULL
);


--
-- TOC entry 333 (class 1259 OID 55984)
-- Name: employee_providerno; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE employee_providerno (
    sk integer NOT NULL,
    employeesk integer NOT NULL,
    storesk integer NOT NULL,
    providerno character varying(30),
    modified timestamp without time zone DEFAULT timezone('Australia/Sydney'::text, now()) NOT NULL
);


--
-- TOC entry 334 (class 1259 OID 55988)
-- Name: employee_providerno_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE employee_providerno_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2955 (class 0 OID 0)
-- Dependencies: 334
-- Name: employee_providerno_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE employee_providerno_sk_seq OWNED BY employee_providerno.sk;


--
-- TOC entry 335 (class 1259 OID 55990)
-- Name: employee_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE employee_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2956 (class 0 OID 0)
-- Dependencies: 335
-- Name: employee_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE employee_sk_seq OWNED BY employee.sk;


--
-- TOC entry 336 (class 1259 OID 55992)
-- Name: expense; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE expense (
    sk integer NOT NULL,
    expenseid character varying(100),
    storesk integer NOT NULL,
    createddate timestamp without time zone NOT NULL,
    employeesk integer NOT NULL,
    expense_typesk integer NOT NULL,
    description character varying(100),
    amount_incl_gst numeric(19,4),
    amount_gst numeric(19,4),
    modified timestamp without time zone
);


--
-- TOC entry 337 (class 1259 OID 55995)
-- Name: expense_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE expense_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2957 (class 0 OID 0)
-- Dependencies: 337
-- Name: expense_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE expense_sk_seq OWNED BY expense.sk;


--
-- TOC entry 338 (class 1259 OID 55997)
-- Name: expense_type; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE expense_type (
    sk integer NOT NULL,
    expense_typeid character varying(30) NOT NULL,
    name character varying(100) NOT NULL
);


--
-- TOC entry 339 (class 1259 OID 56000)
-- Name: expense_type_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE expense_type_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2958 (class 0 OID 0)
-- Dependencies: 339
-- Name: expense_type_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE expense_type_sk_seq OWNED BY expense_type.sk;


--
-- TOC entry 340 (class 1259 OID 56002)
-- Name: invoice; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE invoice (
    sk integer NOT NULL,
    invoiceid character varying(100),
    createddate timestamp without time zone NOT NULL,
    storesk integer NOT NULL,
    employeesk integer NOT NULL,
    customersk integer NOT NULL,
    amount_incl_gst numeric(19,4),
    modified timestamp without time zone,
    amount_gst numeric(19,4),
    reversalref character varying(100),
    rebateref character varying(100)
);


--
-- TOC entry 341 (class 1259 OID 56005)
-- Name: invoice_orders; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE invoice_orders (
    sk integer NOT NULL,
    store_sk integer NOT NULL,
    invoice_sk integer NOT NULL,
    order_sk integer NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL
);


--
-- TOC entry 342 (class 1259 OID 56009)
-- Name: invoice_orders_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE invoice_orders_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2959 (class 0 OID 0)
-- Dependencies: 342
-- Name: invoice_orders_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE invoice_orders_sk_seq OWNED BY invoice_orders.sk;


--
-- TOC entry 343 (class 1259 OID 56011)
-- Name: invoice_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE invoice_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2960 (class 0 OID 0)
-- Dependencies: 343
-- Name: invoice_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE invoice_sk_seq OWNED BY invoice.sk;


--
-- TOC entry 344 (class 1259 OID 56013)
-- Name: invoiceitem; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE invoiceitem (
    invoicesk integer NOT NULL,
    pos integer NOT NULL,
    productsk integer NOT NULL,
    storesk integer NOT NULL,
    employeesk integer NOT NULL,
    serviceemployeesk integer NOT NULL,
    customersk integer NOT NULL,
    createddate timestamp without time zone NOT NULL,
    quantity integer NOT NULL,
    amount_incl_gst numeric(19,4),
    modified timestamp without time zone,
    amount_gst numeric(19,4),
    discount_incl_gst numeric(19,4),
    discount_gst numeric(19,4),
    cost_incl_gst numeric(19,4),
    cost_gst numeric(19,4),
    description character varying(500),
    payeesk integer,
    invoiceitemid character varying(60) DEFAULT ''::character varying NOT NULL,
    return_incl_gst numeric(19,4),
    return_gst numeric(19,4),
    bulkbillref character varying(100)
);


--
-- TOC entry 345 (class 1259 OID 56020)
-- Name: job_process_status; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE job_process_status (
    sk integer NOT NULL,
    jobname character varying(255) NOT NULL,
    status character varying(10) NOT NULL,
    description character varying(255),
    lastexecution timestamp without time zone NOT NULL
);


--
-- TOC entry 346 (class 1259 OID 56026)
-- Name: job_process_status_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE job_process_status_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2961 (class 0 OID 0)
-- Dependencies: 346
-- Name: job_process_status_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE job_process_status_sk_seq OWNED BY job_process_status.sk;


--
-- TOC entry 347 (class 1259 OID 56028)
-- Name: order_history; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE order_history (
    sk integer NOT NULL,
    store_sk integer NOT NULL,
    customer_sk integer NOT NULL,
    source_id character varying(100),
    order_type character varying(100) NOT NULL,
    queue character varying(100),
    status character varying(100),
    source_created character varying(100),
    source_modified character varying(100),
    created timestamp without time zone DEFAULT now() NOT NULL
);


--
-- TOC entry 348 (class 1259 OID 56035)
-- Name: order_history_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE order_history_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2962 (class 0 OID 0)
-- Dependencies: 348
-- Name: order_history_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE order_history_sk_seq OWNED BY order_history.sk;


--
-- TOC entry 349 (class 1259 OID 56037)
-- Name: order_job_history; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE order_job_history (
    sk integer NOT NULL,
    source_id character varying(100),
    order_id integer NOT NULL,
    order_sk integer NOT NULL,
    job_type character varying(100) NOT NULL,
    queue character varying(100),
    status character varying(100),
    source_created character varying(100),
    source_modified character varying(100),
    created timestamp without time zone DEFAULT now() NOT NULL
);


--
-- TOC entry 350 (class 1259 OID 56044)
-- Name: order_job_history_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE order_job_history_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2963 (class 0 OID 0)
-- Dependencies: 350
-- Name: order_job_history_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE order_job_history_sk_seq OWNED BY order_job_history.sk;


--
-- TOC entry 351 (class 1259 OID 56046)
-- Name: payee; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE payee (
    sk integer NOT NULL,
    storeid character varying(12),
    payeeid character varying(100),
    name character varying(200),
    payeetype character varying(150),
    chainid character varying(100) DEFAULT ''::character varying NOT NULL
);


--
-- TOC entry 352 (class 1259 OID 56053)
-- Name: payee_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE payee_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2964 (class 0 OID 0)
-- Dependencies: 352
-- Name: payee_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE payee_sk_seq OWNED BY payee.sk;


--
-- TOC entry 353 (class 1259 OID 56055)
-- Name: payment; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE payment (
    sk integer NOT NULL,
    paymentid character varying(100),
    createddate timestamp without time zone NOT NULL,
    amount_incl_gst numeric(19,4),
    modified timestamp without time zone,
    amount_gst numeric(19,4),
    storeid character varying(100) DEFAULT ''::character varying,
    payeeid character varying(100) DEFAULT ''::character varying,
    employeeid character varying(100) DEFAULT ''::character varying,
    customerid character varying(100) DEFAULT ''::character varying,
    payment_typeid character varying(100) DEFAULT ''::character varying
);


--
-- TOC entry 354 (class 1259 OID 56066)
-- Name: payment_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE payment_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2965 (class 0 OID 0)
-- Dependencies: 354
-- Name: payment_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE payment_sk_seq OWNED BY payment.sk;


--
-- TOC entry 355 (class 1259 OID 56068)
-- Name: payment_type; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE payment_type (
    sk integer NOT NULL,
    payment_typeid character varying(30) NOT NULL,
    name character varying(100) NOT NULL
);


--
-- TOC entry 356 (class 1259 OID 56071)
-- Name: payment_type_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE payment_type_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2966 (class 0 OID 0)
-- Dependencies: 356
-- Name: payment_type_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE payment_type_sk_seq OWNED BY payment_type.sk;


--
-- TOC entry 357 (class 1259 OID 56073)
-- Name: paymentitem; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE paymentitem (
    storesk integer NOT NULL,
    paymentsk integer NOT NULL,
    invoicesk integer NOT NULL,
    customersk integer NOT NULL,
    payment_typesk integer NOT NULL,
    payment_employeesk integer NOT NULL,
    invoice_employeesk integer NOT NULL,
    payment_createddate timestamp without time zone NOT NULL,
    invoice_createddate timestamp without time zone NOT NULL,
    payment_amount_incl_gst numeric(19,4),
    invoice_amount_incl_gst numeric(19,4),
    modified timestamp without time zone,
    payment_amount_gst numeric(19,4),
    paymentitemid character varying(60) DEFAULT ''::character varying NOT NULL,
    payeesk integer
);


--
-- TOC entry 358 (class 1259 OID 56077)
-- Name: product_price_history; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE product_price_history (
    id bigint NOT NULL,
    product_id character varying(8) NOT NULL,
    supplier_cost numeric(19,2),
    supplier_cost_gst numeric(19,2),
    actual_cost numeric(19,2),
    actual_cost_gst numeric(19,2),
    supplier_rrp numeric(19,2),
    supplier_rrp_gst numeric(19,2),
    calculated_retail_price numeric(19,2),
    calculated_retail_price_gst numeric(19,2),
    override_retail_price numeric(19,2),
    override_retail_price_gst numeric(19,2),
    retail_price numeric(19,2),
    retail_price_gst numeric(19,2),
    effective_from_time timestamp without time zone NOT NULL,
    effective_to_time timestamp without time zone
);


--
-- TOC entry 359 (class 1259 OID 56080)
-- Name: product_price_history_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE product_price_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2967 (class 0 OID 0)
-- Dependencies: 359
-- Name: product_price_history_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE product_price_history_id_seq OWNED BY product_price_history.id;


--
-- TOC entry 360 (class 1259 OID 56082)
-- Name: schema_version; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE schema_version (
    installed_rank integer NOT NULL,
    version character varying(50),
    description character varying(200) NOT NULL,
    type character varying(20) NOT NULL,
    script character varying(1000) NOT NULL,
    checksum integer,
    installed_by character varying(100) NOT NULL,
    installed_on timestamp without time zone DEFAULT now() NOT NULL,
    execution_time integer NOT NULL,
    success boolean NOT NULL
);


--
-- TOC entry 361 (class 1259 OID 56089)
-- Name: spectaclejob; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE spectaclejob (
    sk integer NOT NULL,
    invoicesk integer,
    framesk integer,
    right_lenssk integer,
    left_lenssk integer,
    storesk integer NOT NULL,
    createddate timestamp without time zone NOT NULL,
    orderdate timestamp without time zone,
    receiveddate timestamp without time zone,
    pickupdate timestamp without time zone,
    modified timestamp without time zone NOT NULL,
    spectaclejobid character varying(60) DEFAULT ''::character varying NOT NULL,
    pms_invoicenum character varying(100)
);


--
-- TOC entry 362 (class 1259 OID 56093)
-- Name: spectaclejob_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE spectaclejob_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2968 (class 0 OID 0)
-- Dependencies: 362
-- Name: spectaclejob_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE spectaclejob_sk_seq OWNED BY spectaclejob.sk;


--
-- TOC entry 363 (class 1259 OID 56095)
-- Name: status; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE status (
    sk integer NOT NULL,
    statusid character varying(30) NOT NULL,
    name character varying(100) NOT NULL
);


--
-- TOC entry 364 (class 1259 OID 56098)
-- Name: status_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE status_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2969 (class 0 OID 0)
-- Dependencies: 364
-- Name: status_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE status_sk_seq OWNED BY status.sk;


--
-- TOC entry 365 (class 1259 OID 56100)
-- Name: stock_price_history; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE stock_price_history (
    sk integer NOT NULL,
    store_sk integer NOT NULL,
    source_id character varying(100) NOT NULL,
    takeon_storeproduct_sk integer NOT NULL,
    storeproduct_sk integer NOT NULL,
    supplier_cost numeric(19,2),
    supplier_cost_gst numeric(19,2),
    actual_cost numeric(19,2),
    actual_cost_gst numeric(19,2),
    supplier_rrp numeric(19,2),
    supplier_rrp_gst numeric(19,2),
    calculated_retail_price numeric(19,2),
    calculated_retail_price_gst numeric(19,2),
    override_retail_price numeric(19,2),
    override_retail_price_gst numeric(19,2),
    retail_price numeric(19,2),
    retail_price_gst numeric(19,2),
    avg_cost_price numeric(19,2),
    created timestamp without time zone DEFAULT now() NOT NULL
);


--
-- TOC entry 366 (class 1259 OID 56104)
-- Name: stock_price_history_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE stock_price_history_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2970 (class 0 OID 0)
-- Dependencies: 366
-- Name: stock_price_history_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE stock_price_history_sk_seq OWNED BY stock_price_history.sk;


--
-- TOC entry 367 (class 1259 OID 56106)
-- Name: store; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE store (
    sk integer NOT NULL,
    storeid character varying(12) NOT NULL,
    name character varying(100) NOT NULL,
    chainid character varying(100) DEFAULT ''::character varying NOT NULL,
    isactive boolean DEFAULT false NOT NULL,
    pms character varying(20)
);


--
-- TOC entry 368 (class 1259 OID 56111)
-- Name: store_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE store_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2971 (class 0 OID 0)
-- Dependencies: 368
-- Name: store_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE store_sk_seq OWNED BY store.sk;


--
-- TOC entry 369 (class 1259 OID 56113)
-- Name: store_stock_history; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE store_stock_history (
    sk integer NOT NULL,
    store_sk integer NOT NULL,
    source_id character varying(100) NOT NULL,
    takeon_storeproduct_sk integer NOT NULL,
    storeproduct_sk integer NOT NULL,
    total integer NOT NULL,
    prior_total integer NOT NULL,
    reason character varying(20) NOT NULL,
    comments character varying(500),
    employee_sk integer,
    created timestamp without time zone DEFAULT now()
);


--
-- TOC entry 370 (class 1259 OID 56120)
-- Name: store_stock_history_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE store_stock_history_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2972 (class 0 OID 0)
-- Dependencies: 370
-- Name: store_stock_history_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE store_stock_history_sk_seq OWNED BY store_stock_history.sk;


--
-- TOC entry 371 (class 1259 OID 56122)
-- Name: store_stockmovement; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE store_stockmovement (
    sk integer NOT NULL,
    store_sk integer NOT NULL,
    source_id character varying(100) NOT NULL,
    takeon_storeproduct_sk integer NOT NULL,
    storeproduct_sk integer NOT NULL,
    adjustmentdate timestamp without time zone,
    stock_price_history_sk integer NOT NULL,
    adjustment integer NOT NULL,
    adjustmenttype character varying(20) NOT NULL,
    reason character varying(20) NOT NULL,
    invoice_sk integer,
    customer_sk integer,
    shipping_ref character varying(100),
    shipping_operator character varying(100),
    comments character varying(500),
    employee_sk integer,
    created timestamp without time zone DEFAULT now() NOT NULL
);


--
-- TOC entry 372 (class 1259 OID 56129)
-- Name: store_stockmovement_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE store_stockmovement_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2973 (class 0 OID 0)
-- Dependencies: 372
-- Name: store_stockmovement_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE store_stockmovement_sk_seq OWNED BY store_stockmovement.sk;


--
-- TOC entry 373 (class 1259 OID 56131)
-- Name: storeproduct; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE storeproduct (
    sk integer NOT NULL,
    storeid character varying(12),
    productid character varying(500),
    type character varying(100),
    datecreated timestamp without time zone,
    name character varying(500),
    description character varying(500),
    retailprice numeric(19,4),
    wholesaleprice numeric(19,4),
    modified timestamp without time zone,
    last_purchase timestamp without time zone,
    last_sale timestamp without time zone,
    quantity integer,
    frame_model character varying(100) DEFAULT ''::character varying,
    frame_colour character varying(100) DEFAULT ''::character varying,
    frame_brand character varying(100) DEFAULT ''::character varying,
    frame_temple numeric(8,2) DEFAULT 0,
    frame_bridge numeric(8,2) DEFAULT 0,
    frame_eye_size numeric(8,2) DEFAULT 0,
    frame_depth numeric(8,2) DEFAULT 0,
    frame_material character varying(100) DEFAULT ''::character varying,
    lens_type character varying(100) DEFAULT ''::character varying,
    lens_refractive_index numeric(19,4) DEFAULT 0,
    lens_stock_grind character varying(10) DEFAULT ''::character varying,
    lens_size numeric(4,2) DEFAULT 0,
    lens_segment_size numeric(4,2) DEFAULT 0,
    internal_sku character varying(100),
    distributor character varying(100)
);


--
-- TOC entry 374 (class 1259 OID 56150)
-- Name: storeproduct_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE storeproduct_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2974 (class 0 OID 0)
-- Dependencies: 374
-- Name: storeproduct_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE storeproduct_sk_seq OWNED BY storeproduct.sk;


--
-- TOC entry 375 (class 1259 OID 56152)
-- Name: supplier_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE supplier_sk_seq
    START WITH 60
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 376 (class 1259 OID 56154)
-- Name: supplier; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE supplier (
    sk integer DEFAULT nextval('supplier_sk_seq'::regclass) NOT NULL,
    externalref bigint NOT NULL,
    supplierid character varying(255) NOT NULL,
    name character varying(255) NOT NULL
);


--
-- TOC entry 377 (class 1259 OID 56161)
-- Name: supplierproduct_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE supplierproduct_sk_seq
    START WITH 500
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 378 (class 1259 OID 56163)
-- Name: supplierproduct; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE supplierproduct (
    sk integer DEFAULT nextval('supplierproduct_sk_seq'::regclass) NOT NULL,
    suppliersku character varying(255) NOT NULL,
    suppliersk integer NOT NULL,
    frame_model character varying(255),
    frame_colour character varying(100),
    internal_sku character varying(100),
    frame_brand character varying(100)
);


--
-- TOC entry 379 (class 1259 OID 56170)
-- Name: takeon_product; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE takeon_product (
    sk integer NOT NULL,
    store_sk integer NOT NULL,
    supplier_sk integer NOT NULL,
    source_id character varying(100) NOT NULL,
    supplier_sku character varying(100) NOT NULL,
    name character varying(500) NOT NULL,
    description character varying(500) NOT NULL,
    model character varying(500) NOT NULL,
    brand character varying(500) NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL
);


--
-- TOC entry 380 (class 1259 OID 56177)
-- Name: takeon_product_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE takeon_product_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2975 (class 0 OID 0)
-- Dependencies: 380
-- Name: takeon_product_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE takeon_product_sk_seq OWNED BY takeon_product.sk;


--
-- TOC entry 2642 (class 2604 OID 56179)
-- Name: appointment sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY appointment ALTER COLUMN sk SET DEFAULT nextval('appointment_sk_seq'::regclass);


--
-- TOC entry 2644 (class 2604 OID 56180)
-- Name: appointment_type sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY appointment_type ALTER COLUMN sk SET DEFAULT nextval('appointment_type_sk_seq'::regclass);


--
-- TOC entry 2645 (class 2604 OID 56181)
-- Name: consult sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY consult ALTER COLUMN sk SET DEFAULT nextval('consult_sk_seq'::regclass);


--
-- TOC entry 2646 (class 2604 OID 56182)
-- Name: consult_type sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY consult_type ALTER COLUMN sk SET DEFAULT nextval('consult_type_sk_seq'::regclass);


--
-- TOC entry 2647 (class 2604 OID 56183)
-- Name: consultitem sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY consultitem ALTER COLUMN sk SET DEFAULT nextval('consultitem_sk_seq'::regclass);


--
-- TOC entry 2654 (class 2604 OID 56184)
-- Name: customer sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY customer ALTER COLUMN sk SET DEFAULT nextval('customer_sk_seq'::regclass);


--
-- TOC entry 2655 (class 2604 OID 56185)
-- Name: customer_ext sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY customer_ext ALTER COLUMN sk SET DEFAULT nextval('customer_ext_sk_seq'::regclass);


--
-- TOC entry 2656 (class 2604 OID 56186)
-- Name: discountcode sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY discountcode ALTER COLUMN sk SET DEFAULT nextval('discountcode_sk_seq'::regclass);


--
-- TOC entry 2660 (class 2604 OID 56187)
-- Name: employee sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY employee ALTER COLUMN sk SET DEFAULT nextval('employee_sk_seq'::regclass);


--
-- TOC entry 2664 (class 2604 OID 56188)
-- Name: employee_providerno sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY employee_providerno ALTER COLUMN sk SET DEFAULT nextval('employee_providerno_sk_seq'::regclass);


--
-- TOC entry 2666 (class 2604 OID 56189)
-- Name: expense sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY expense ALTER COLUMN sk SET DEFAULT nextval('expense_sk_seq'::regclass);


--
-- TOC entry 2667 (class 2604 OID 56190)
-- Name: expense_type sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY expense_type ALTER COLUMN sk SET DEFAULT nextval('expense_type_sk_seq'::regclass);


--
-- TOC entry 2668 (class 2604 OID 56191)
-- Name: invoice sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY invoice ALTER COLUMN sk SET DEFAULT nextval('invoice_sk_seq'::regclass);


--
-- TOC entry 2669 (class 2604 OID 56192)
-- Name: invoice_orders sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY invoice_orders ALTER COLUMN sk SET DEFAULT nextval('invoice_orders_sk_seq'::regclass);


--
-- TOC entry 2672 (class 2604 OID 56193)
-- Name: job_process_status sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY job_process_status ALTER COLUMN sk SET DEFAULT nextval('job_process_status_sk_seq'::regclass);


--
-- TOC entry 2673 (class 2604 OID 56194)
-- Name: order_history sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY order_history ALTER COLUMN sk SET DEFAULT nextval('order_history_sk_seq'::regclass);


--
-- TOC entry 2675 (class 2604 OID 56195)
-- Name: order_job_history sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY order_job_history ALTER COLUMN sk SET DEFAULT nextval('order_job_history_sk_seq'::regclass);


--
-- TOC entry 2677 (class 2604 OID 56196)
-- Name: payee sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY payee ALTER COLUMN sk SET DEFAULT nextval('payee_sk_seq'::regclass);


--
-- TOC entry 2684 (class 2604 OID 56197)
-- Name: payment sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY payment ALTER COLUMN sk SET DEFAULT nextval('payment_sk_seq'::regclass);


--
-- TOC entry 2685 (class 2604 OID 56198)
-- Name: payment_type sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY payment_type ALTER COLUMN sk SET DEFAULT nextval('payment_type_sk_seq'::regclass);


--
-- TOC entry 2687 (class 2604 OID 56199)
-- Name: product_price_history id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY product_price_history ALTER COLUMN id SET DEFAULT nextval('product_price_history_id_seq'::regclass);


--
-- TOC entry 2689 (class 2604 OID 56200)
-- Name: spectaclejob sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY spectaclejob ALTER COLUMN sk SET DEFAULT nextval('spectaclejob_sk_seq'::regclass);


--
-- TOC entry 2691 (class 2604 OID 56201)
-- Name: status sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY status ALTER COLUMN sk SET DEFAULT nextval('status_sk_seq'::regclass);


--
-- TOC entry 2692 (class 2604 OID 56202)
-- Name: stock_price_history sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY stock_price_history ALTER COLUMN sk SET DEFAULT nextval('stock_price_history_sk_seq'::regclass);


--
-- TOC entry 2694 (class 2604 OID 56203)
-- Name: store sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY store ALTER COLUMN sk SET DEFAULT nextval('store_sk_seq'::regclass);


--
-- TOC entry 2697 (class 2604 OID 56204)
-- Name: store_stock_history sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stock_history ALTER COLUMN sk SET DEFAULT nextval('store_stock_history_sk_seq'::regclass);


--
-- TOC entry 2699 (class 2604 OID 56205)
-- Name: store_stockmovement sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stockmovement ALTER COLUMN sk SET DEFAULT nextval('store_stockmovement_sk_seq'::regclass);


--
-- TOC entry 2714 (class 2604 OID 56206)
-- Name: storeproduct sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY storeproduct ALTER COLUMN sk SET DEFAULT nextval('storeproduct_sk_seq'::regclass);


--
-- TOC entry 2717 (class 2604 OID 56207)
-- Name: takeon_product sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY takeon_product ALTER COLUMN sk SET DEFAULT nextval('takeon_product_sk_seq'::regclass);


--
-- TOC entry 2721 (class 2606 OID 56209)
-- Name: appointment appointment_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY appointment
    ADD CONSTRAINT appointment_pkey PRIMARY KEY (sk);


--
-- TOC entry 2725 (class 2606 OID 56212)
-- Name: appointment_type appointment_type_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY appointment_type
    ADD CONSTRAINT appointment_type_pkey PRIMARY KEY (sk);


--
-- TOC entry 2730 (class 2606 OID 56214)
-- Name: consult_type constult_type_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY consult_type
    ADD CONSTRAINT constult_type_pkey PRIMARY KEY (sk);


--
-- TOC entry 2728 (class 2606 OID 56216)
-- Name: consult consult_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY consult
    ADD CONSTRAINT consult_pkey PRIMARY KEY (sk);


--
-- TOC entry 2733 (class 2606 OID 56219)
-- Name: consultitem consultitem_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY consultitem
    ADD CONSTRAINT consultitem_pkey PRIMARY KEY (sk);


--
-- TOC entry 2738 (class 2606 OID 56221)
-- Name: customer_ext customer_ext_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY customer_ext
    ADD CONSTRAINT customer_ext_pkey PRIMARY KEY (sk);


--
-- TOC entry 2736 (class 2606 OID 56223)
-- Name: customer customer_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY customer
    ADD CONSTRAINT customer_pkey PRIMARY KEY (sk);


--
-- TOC entry 2744 (class 2606 OID 56225)
-- Name: expense expense_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY expense
    ADD CONSTRAINT expense_pkey PRIMARY KEY (sk);


--
-- TOC entry 2747 (class 2606 OID 56227)
-- Name: expense_type expense_type_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY expense_type
    ADD CONSTRAINT expense_type_pkey PRIMARY KEY (sk);


--
-- TOC entry 2795 (class 2606 OID 56229)
-- Name: supplier externalref_ukey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY supplier
    ADD CONSTRAINT externalref_ukey UNIQUE (externalref);


--
-- TOC entry 2753 (class 2606 OID 56231)
-- Name: invoice_orders invoice_orders_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY invoice_orders
    ADD CONSTRAINT invoice_orders_pkey PRIMARY KEY (sk);


--
-- TOC entry 2750 (class 2606 OID 56233)
-- Name: invoice invoice_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY invoice
    ADD CONSTRAINT invoice_pkey PRIMARY KEY (sk);


--
-- TOC entry 2755 (class 2606 OID 56235)
-- Name: invoiceitem invoiceitem_skey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY invoiceitem
    ADD CONSTRAINT invoiceitem_skey PRIMARY KEY (invoiceitemid);


--
-- TOC entry 2758 (class 2606 OID 56244)
-- Name: order_history order_history_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY order_history
    ADD CONSTRAINT order_history_pkey PRIMARY KEY (sk);


--
-- TOC entry 2760 (class 2606 OID 56246)
-- Name: order_job_history order_job_history_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY order_job_history
    ADD CONSTRAINT order_job_history_pkey PRIMARY KEY (sk);


--
-- TOC entry 2763 (class 2606 OID 56248)
-- Name: payee payee_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY payee
    ADD CONSTRAINT payee_pkey PRIMARY KEY (sk);


--
-- TOC entry 2766 (class 2606 OID 56250)
-- Name: payment payment_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY payment
    ADD CONSTRAINT payment_pkey PRIMARY KEY (sk);


--
-- TOC entry 2770 (class 2606 OID 56252)
-- Name: payment_type payment_type_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY payment_type
    ADD CONSTRAINT payment_type_pkey PRIMARY KEY (sk);


--
-- TOC entry 2772 (class 2606 OID 56254)
-- Name: paymentitem paymentitem_skey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY paymentitem
    ADD CONSTRAINT paymentitem_skey PRIMARY KEY (paymentitemid);


--
-- TOC entry 2741 (class 2606 OID 56258)
-- Name: employee pk_employee; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY employee
    ADD CONSTRAINT pk_employee PRIMARY KEY (sk);


--
-- TOC entry 2775 (class 2606 OID 56260)
-- Name: schema_version schema_version_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY schema_version
    ADD CONSTRAINT schema_version_pk PRIMARY KEY (installed_rank);


--
-- TOC entry 2778 (class 2606 OID 56262)
-- Name: spectaclejob spectaclejob_skey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY spectaclejob
    ADD CONSTRAINT spectaclejob_skey PRIMARY KEY (spectaclejobid);


--
-- TOC entry 2781 (class 2606 OID 56264)
-- Name: status status_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY status
    ADD CONSTRAINT status_pkey PRIMARY KEY (sk);


--
-- TOC entry 2783 (class 2606 OID 56266)
-- Name: stock_price_history stock_price_history_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY stock_price_history
    ADD CONSTRAINT stock_price_history_pkey PRIMARY KEY (sk);


--
-- TOC entry 2786 (class 2606 OID 56268)
-- Name: store store_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store
    ADD CONSTRAINT store_pkey PRIMARY KEY (sk);


--
-- TOC entry 2788 (class 2606 OID 56270)
-- Name: store_stock_history store_stock_history_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stock_history
    ADD CONSTRAINT store_stock_history_pkey PRIMARY KEY (sk);


--
-- TOC entry 2790 (class 2606 OID 56272)
-- Name: store_stockmovement store_stockmovement_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stockmovement
    ADD CONSTRAINT store_stockmovement_pkey PRIMARY KEY (sk);


--
-- TOC entry 2793 (class 2606 OID 56274)
-- Name: storeproduct storeproduct_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY storeproduct
    ADD CONSTRAINT storeproduct_pkey PRIMARY KEY (sk);


--
-- TOC entry 2797 (class 2606 OID 56276)
-- Name: supplier supplier_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY supplier
    ADD CONSTRAINT supplier_pkey PRIMARY KEY (sk);


--
-- TOC entry 2802 (class 2606 OID 56278)
-- Name: supplierproduct supplierproduct_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY supplierproduct
    ADD CONSTRAINT supplierproduct_pkey PRIMARY KEY (sk);


--
-- TOC entry 2804 (class 2606 OID 56280)
-- Name: takeon_product takeon_product_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY takeon_product
    ADD CONSTRAINT takeon_product_pkey PRIMARY KEY (sk);


--
-- TOC entry 2719 (class 1259 OID 56281)
-- Name: appointment_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX appointment_nkey ON appointment USING btree (appointmentid);


--
-- TOC entry 2722 (class 1259 OID 56282)
-- Name: appointment_storesk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX appointment_storesk ON appointment USING hash (storesk);


--
-- TOC entry 2723 (class 1259 OID 56291)
-- Name: appointment_type_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX appointment_type_nkey ON appointment_type USING btree (appointment_typeid);


--
-- TOC entry 2726 (class 1259 OID 56292)
-- Name: consult_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX consult_nkey ON consult USING btree (consultid);


--
-- TOC entry 2731 (class 1259 OID 56293)
-- Name: consult_type_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX consult_type_nkey ON consult_type USING btree (consult_typeid);


--
-- TOC entry 2734 (class 1259 OID 56294)
-- Name: customer_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX customer_nkey ON customer USING btree (customerid);


--
-- TOC entry 2739 (class 1259 OID 56295)
-- Name: employee_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX employee_nkey ON employee USING btree (employeeid);


--
-- TOC entry 2742 (class 1259 OID 56296)
-- Name: expense_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX expense_nkey ON expense USING btree (expenseid);


--
-- TOC entry 2745 (class 1259 OID 56297)
-- Name: expense_type_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX expense_type_nkey ON expense_type USING btree (expense_typeid);


--
-- TOC entry 2798 (class 1259 OID 56298)
-- Name: fki_supplierproduct_supplier_fkey; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX fki_supplierproduct_supplier_fkey ON supplierproduct USING btree (suppliersk);


--
-- TOC entry 2799 (class 1259 OID 56299)
-- Name: idx_model; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_model ON supplierproduct USING btree (frame_model);


--
-- TOC entry 2800 (class 1259 OID 56302)
-- Name: idx_suppliersku; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_suppliersku ON supplierproduct USING btree (suppliersku);


--
-- TOC entry 2748 (class 1259 OID 56303)
-- Name: invoice_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX invoice_nkey ON invoice USING btree (invoiceid);


--
-- TOC entry 2751 (class 1259 OID 56305)
-- Name: invoice_storesk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX invoice_storesk ON invoice USING hash (storesk);


--
-- TOC entry 2756 (class 1259 OID 56309)
-- Name: invoiceitem_storesk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX invoiceitem_storesk ON invoiceitem USING hash (storesk);


--
-- TOC entry 2761 (class 1259 OID 56315)
-- Name: payee_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX payee_nkey ON payee USING btree (payeeid);


--
-- TOC entry 2764 (class 1259 OID 56316)
-- Name: payment_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX payment_nkey ON payment USING btree (paymentid);


--
-- TOC entry 2767 (class 1259 OID 56317)
-- Name: payment_storeid_ix; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX payment_storeid_ix ON payment USING btree (storeid);


--
-- TOC entry 2768 (class 1259 OID 56318)
-- Name: payment_type_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX payment_type_nkey ON payment_type USING btree (payment_typeid);


--
-- TOC entry 2773 (class 1259 OID 56319)
-- Name: paymentitem_storesk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX paymentitem_storesk ON paymentitem USING hash (storesk);


--
-- TOC entry 2776 (class 1259 OID 56320)
-- Name: schema_version_s_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX schema_version_s_idx ON schema_version USING btree (success);


--
-- TOC entry 2779 (class 1259 OID 56321)
-- Name: status_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX status_nkey ON status USING btree (statusid);


--
-- TOC entry 2784 (class 1259 OID 56322)
-- Name: store_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX store_nkey ON store USING btree (storeid);


--
-- TOC entry 2791 (class 1259 OID 56323)
-- Name: storeproduct_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX storeproduct_nkey ON storeproduct USING btree (productid);


--
-- TOC entry 2805 (class 2606 OID 56324)
-- Name: invoice_orders invoice_orders_invoice_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY invoice_orders
    ADD CONSTRAINT invoice_orders_invoice_sk_fkey FOREIGN KEY (invoice_sk) REFERENCES invoice(sk);


--
-- TOC entry 2806 (class 2606 OID 56329)
-- Name: invoice_orders invoice_orders_order_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY invoice_orders
    ADD CONSTRAINT invoice_orders_order_sk_fkey FOREIGN KEY (order_sk) REFERENCES order_history(sk);


--
-- TOC entry 2807 (class 2606 OID 56334)
-- Name: invoice_orders invoice_orders_store_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY invoice_orders
    ADD CONSTRAINT invoice_orders_store_sk_fkey FOREIGN KEY (store_sk) REFERENCES store(sk);


--
-- TOC entry 2808 (class 2606 OID 56339)
-- Name: order_history order_history_customer_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY order_history
    ADD CONSTRAINT order_history_customer_sk_fkey FOREIGN KEY (customer_sk) REFERENCES customer(sk);


--
-- TOC entry 2809 (class 2606 OID 56344)
-- Name: order_history order_history_store_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY order_history
    ADD CONSTRAINT order_history_store_sk_fkey FOREIGN KEY (store_sk) REFERENCES store(sk);


--
-- TOC entry 2810 (class 2606 OID 56349)
-- Name: stock_price_history stock_price_history_store_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY stock_price_history
    ADD CONSTRAINT stock_price_history_store_sk_fkey FOREIGN KEY (store_sk) REFERENCES store(sk);


--
-- TOC entry 2811 (class 2606 OID 56354)
-- Name: stock_price_history stock_price_history_storeproduct_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY stock_price_history
    ADD CONSTRAINT stock_price_history_storeproduct_sk_fkey FOREIGN KEY (storeproduct_sk) REFERENCES storeproduct(sk);


--
-- TOC entry 2812 (class 2606 OID 56359)
-- Name: stock_price_history stock_price_history_takeon_storeproduct_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY stock_price_history
    ADD CONSTRAINT stock_price_history_takeon_storeproduct_sk_fkey FOREIGN KEY (takeon_storeproduct_sk) REFERENCES storeproduct(sk);


--
-- TOC entry 2813 (class 2606 OID 56364)
-- Name: store_stock_history store_stock_history_employee_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stock_history
    ADD CONSTRAINT store_stock_history_employee_sk_fkey FOREIGN KEY (employee_sk) REFERENCES employee(sk);


--
-- TOC entry 2814 (class 2606 OID 56369)
-- Name: store_stock_history store_stock_history_store_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stock_history
    ADD CONSTRAINT store_stock_history_store_sk_fkey FOREIGN KEY (store_sk) REFERENCES store(sk);


--
-- TOC entry 2815 (class 2606 OID 56374)
-- Name: store_stock_history store_stock_history_storeproduct_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stock_history
    ADD CONSTRAINT store_stock_history_storeproduct_sk_fkey FOREIGN KEY (storeproduct_sk) REFERENCES storeproduct(sk);


--
-- TOC entry 2816 (class 2606 OID 56379)
-- Name: store_stock_history store_stock_history_takeon_storeproduct_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stock_history
    ADD CONSTRAINT store_stock_history_takeon_storeproduct_sk_fkey FOREIGN KEY (takeon_storeproduct_sk) REFERENCES storeproduct(sk);


--
-- TOC entry 2817 (class 2606 OID 56384)
-- Name: store_stockmovement store_stockmovement_customer_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stockmovement
    ADD CONSTRAINT store_stockmovement_customer_sk_fkey FOREIGN KEY (customer_sk) REFERENCES customer(sk);


--
-- TOC entry 2818 (class 2606 OID 56389)
-- Name: store_stockmovement store_stockmovement_employee_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stockmovement
    ADD CONSTRAINT store_stockmovement_employee_sk_fkey FOREIGN KEY (employee_sk) REFERENCES employee(sk);


--
-- TOC entry 2819 (class 2606 OID 56394)
-- Name: store_stockmovement store_stockmovement_invoice_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stockmovement
    ADD CONSTRAINT store_stockmovement_invoice_sk_fkey FOREIGN KEY (invoice_sk) REFERENCES invoice(sk);


--
-- TOC entry 2820 (class 2606 OID 56399)
-- Name: store_stockmovement store_stockmovement_stock_price_history_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stockmovement
    ADD CONSTRAINT store_stockmovement_stock_price_history_sk_fkey FOREIGN KEY (stock_price_history_sk) REFERENCES stock_price_history(sk);


--
-- TOC entry 2821 (class 2606 OID 56404)
-- Name: store_stockmovement store_stockmovement_store_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stockmovement
    ADD CONSTRAINT store_stockmovement_store_sk_fkey FOREIGN KEY (store_sk) REFERENCES store(sk);


--
-- TOC entry 2822 (class 2606 OID 56409)
-- Name: store_stockmovement store_stockmovement_storeproduct_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stockmovement
    ADD CONSTRAINT store_stockmovement_storeproduct_sk_fkey FOREIGN KEY (storeproduct_sk) REFERENCES storeproduct(sk);


--
-- TOC entry 2823 (class 2606 OID 56414)
-- Name: store_stockmovement store_stockmovement_takeon_storeproduct_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stockmovement
    ADD CONSTRAINT store_stockmovement_takeon_storeproduct_sk_fkey FOREIGN KEY (takeon_storeproduct_sk) REFERENCES storeproduct(sk);


--
-- TOC entry 2824 (class 2606 OID 56419)
-- Name: supplierproduct supplierproduct_supplier_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY supplierproduct
    ADD CONSTRAINT supplierproduct_supplier_fkey FOREIGN KEY (suppliersk) REFERENCES supplier(sk);


--
-- TOC entry 2825 (class 2606 OID 56424)
-- Name: takeon_product takeon_product_store_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY takeon_product
    ADD CONSTRAINT takeon_product_store_sk_fkey FOREIGN KEY (store_sk) REFERENCES store(sk);


--
-- TOC entry 2826 (class 2606 OID 56429)
-- Name: takeon_product takeon_product_supplier_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY takeon_product
    ADD CONSTRAINT takeon_product_supplier_sk_fkey FOREIGN KEY (supplier_sk) REFERENCES supplier(sk);


-- Completed on 2017-09-26 06:10:23

--
-- PostgreSQL database dump complete
--

