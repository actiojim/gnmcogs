SET search_path = stage;
 
drop table if exists order_stage;
 
create table order_stage
(
    sk serial NOT NULL,
	run_id character varying(100),
	   
    -- source data order id
    order_id character varying(100),
    
    cust_firstname character varying(100),
    cust_lastname character varying(100),
    cust_midname character varying(100),
    cust_phone character varying(100),
    store_id character varying(100),
    
    cust_sk int,
    
    
    order_type character varying(100),
    
    queue character varying(100),
    status character varying(100),
    
    source_created character varying(100),
    source_modified character varying(100),
    
    -- created date
    created timestamp without time zone NOT null default now(),
    primary key (sk)
);


-- 1. customer entries that are missing from customer table
INSERT INTO public.customer ( storesk, firstname, lastname, homephone, createddate, title, gender, email)
select  distinct  s.sk, os.cust_firstname, os.cust_lastname, os.cust_phone, now(), '', '', ''
from stage.order_stage os 
left join public.customer c on c.firstname = os.cust_firstname and c.lastname = os.cust_lastname  
-- and (os.cust_phone = c.homephone or os.cust_phone = c.mobile or os.cust_phone = c.workphone)
left join public.store s on s.storeid = os.store_id -- and c.storesk = s.sk
where
 c.sk is null;
 
 
-- stage orders
INSERT INTO order_stage
(run_id, order_id, 
cust_firstname, cust_lastname, cust_phone, 
cust_sk, store_id, 
order_type, 
queue, status, 
source_created, source_modified)
SELECT null , o.order_id, 
oc.first_name, oc.last_name, oc.telephone, 
c.sk, s.store_id, 
o.order_type_id,
o.queue, o.status, 
o.created_date_time, o.modified_date_time
FROM gnm."order" o 
inner join gnm.order_customer oc on oc.id = o.customer_id
inner join gnm."store" s on s.id = o.store_id
left outer join public.store ps on ps.storeid = s.store_id
left join public.customer c on c.firstname = oc.first_name and c.lastname = oc.last_name and c.storesk = ps.sk

select count(*) from (
SELECT null , o.order_id, 
oc.first_name, oc.last_name, oc.telephone, 
c.sk, s.store_id, 
o.order_type_id,
o.queue, o.status, 
o.created_date_time, o.modified_date_time
FROM gnm."order" o 
inner join gnm.order_customer oc on oc.id = o.customer_id
inner join gnm."store" s on s.id = o.store_id
left join public.store ps on ps.storeid = s.store_id
left join public.customer c on c.firstname = oc.first_name and c.lastname = oc.last_name and c.storesk = ps.sk) x

-- find customer
/*
select count(*) from (
select  distinct os.store_id, s.sk, os.cust_firstname, os.cust_lastname, os.cust_phone
from stage.order_stage os 
left join public.customer c on c.firstname = os.cust_firstname and c.lastname = os.cust_lastname  
-- and (os.cust_phone = c.homephone or os.cust_phone = c.mobile or os.cust_phone = c.workphone)
left join public.store s on s.storeid = os.store_id -- and c.storesk = s.sk
where
 c.sk is  null) x

select * from public.customer

*/


	
 /*

select * from gnm.store
select * from gnm."order"
select * from gnm.job

*/

----------------------------------------------------------------------------------------------------------------------

SET search_path = stage;
 
drop table if exists job_stage;
 
create table job_stage
(
    sk serial NOT NULL,
	run_id character varying(100),
	
    -- natural source key job_id
    job_id character varying(100),
    
    -- source data order id
    order_id character varying(100),
    product_id character varying(100),
    supplier_id character varying(100),
    quantity character varying(100),
    
    -- job type frame or lens
    job_type character varying(100),
    queue character varying(100),
    status character varying(100),
    source_created character varying(100),
    source_modified character varying(100),
    
    -- created date
    created timestamp without time zone NOT null default now(),
    primary key (sk)
);

--
-- Generate job stage
-- 
/*
insert into stage.job_stage (
	run_id,
	order_id,
	customer_id,
	store_id,
	job_id,
	queue,
	status,
	quantity,
	source_created,
	source_modified,
	job_type,
	product_id,
	supplier_id)
select  
	j.runid,
	o.orderid,
	o.store_id,
	c.customerId, 
	j.f->>'id' as jobid
	 ,f->'attributes'->>'queue' as queue
	 ,f->'attributes'->>'status' as status
	 ,f->'attributes'->>'quantity' as quantity
	 ,f->'attributes'->>'createdDateTime' as created
	 ,f->'attributes'->>'modifiedDateTime' as modified
	 ,f->'relationships'->'jobType'->'data'->>'id' as jobtype
	 ,f->'relationships'->'product'->'data'->>'id' as productid
	 ,f->'relationships'->'supplier'->'data'->>'id' as supplierid 
from  
  (select runid, jsonb_array_elements(jsonb_array_elements(data)->'data') as f from stage.gnm_hub_orders 
 	where runid in (select runid from stage.gnm_hub_orders where createddate >= '2017-08-20 18:47:46')) j 
  inner join 
	(select runid,f->>'id' as orderid,f->'relationships'->'customer'->'data'->>'id'as customer_id, f->'relationships'->'store'->'data'->>'id'as store_id from 
		(select runid,jsonb_array_elements(jsonb_array_elements(y.data)->'included') as f 
			from stage.gnm_hub_orders y 
			where runid in (select runid from stage.gnm_hub_orders where createddate >= '2017-08-20 18:47:46') ) x where f->>'type' = 'order') o on f->'relationships'->'order'->'data'->>'id' = o.orderid 
  inner join 
	(select runid,f->>'id' as id,f->'attributes'->>'customerId' as customerId from 
			(select runid,jsonb_array_elements(jsonb_array_elements(y.data)->'included' ) as f 
					from stage.gnm_hub_orders y 
					where runid in (select runid from stage.gnm_hub_orders where createddate >= '2017-08-20 18:47:46') ) x
		where f->>'type' = 'orderCustomer') c on o.customer_id = c.id
  
 where j.f->>'id'  not in (select job_id from job_stage)
*/

insert into stage.job_stage (
	run_id,
	order_id,
	job_id,
	queue,
	status,
	quantity,
	source_created,
	source_modified,
	job_type,
	product_id,
	supplier_id)
select  
	null,
	j.order_id,
	j.id,
	j.queue,
	j.status,
	j.quantity,
	j.created_date_time,
	j.modified_date_time,
	j.job_type_id,
	j.product_id,
	j.supplier_id
from gnm.job j ;

-- ------------------------------------------------------------------------------------------


SET search_path = public;
CREATE UNIQUE INDEX productid_on_product ON public.storeproduct (productid);

--- Populate store product from 

INSERT INTO storeproduct
(storeid, productid, "type", 
datecreated, modified, 
"name", description, internal_sku, distributor)
SELECT 0, product_id, "type", 
created_date_time, modified_date_time,
p."name",  description, supplier_sku, s.sk
FROM gnm.product p 
inner join public.supplier s on s.externalref = p.supplier_id

/*
SELECT id, supplier_id, "name", email, fax, telephone, contact_person_id, created_user, created_date_time, modified_user, modified_date_time
FROM gnm.supplier;

select * from public.storeproduct where storeid = '0'
select * from public.supplier
*/

------------------------------------------------------------------------------------------------------------------------------------------

SET search_path = public;

INSERT INTO stock_price_history
(store_sk, source_id, takeon_storeproduct_sk, storeproduct_sk, 
supplier_cost, supplier_cost_gst, 
actual_cost, actual_cost_gst, 
supplier_rrp, supplier_rrp_gst, calculated_retail_price, calculated_retail_price_gst, 
override_retail_price, override_retail_price_gst, 
retail_price, retail_price_gst, 
created)
-- VALUES(nextval('stock_price_history_sk_seq'::regclass), 0, '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, now());
SELECT 
0, id, 0, sp.sk, 
supplier_cost, supplier_cost_gst, 
actual_cost, actual_cost_gst, 
supplier_rrp, supplier_rrp_gst, calculated_retail_price, calculated_retail_price_gst, 
0,0,
0,0,
modified_date_time
FROM gnm.product_cost_history pch inner join storeproduct sp on sp.productid = pch.product_id

-----------------------------------------------------------------------------------------------------------------------------------------
/*
select * from "store"

select * from gnm_scire_0816.storeproduct

INSERT INTO gnm_scire_0816.storeproduct
(storeid, productid, "type", datecreated, modified, "name", description, internal_sku )
SELECT 0, product_id, "type", created_date_time, modified_date_time, "name",  description, supplier_sku
FROM gnm.product 
-- where product_id not in (select productid from gnm_scire_0816.storeproduct )

select * from gnm.product
where product_id not in (select productid from gnm_scire_0816.storeproduct )

select * from gnm_scire_0816.storeproduct sp inner join gnm_scire_0816.stock_price_history sph on sph.storeproduct_sk = sp.sk

*/
--------------------------------------------------------------------------------------------------------------------------------------------

SET search_path = public;

-- stage order history
drop table if exists order_history cascade;
drop table if exists order_job_history cascade;
drop table if exists invoice_orders cascade;

--
-- history record is immutable a new record is created for each data change
--
create table order_history
(
    sk serial NOT NULL,
    store_sk int NOT NULL REFERENCES store(sk),
    customer_sk int NOT NULL REFERENCES customer(sk),
    
    -- natural source key
    source_id character varying(100),
    
    order_type character varying(100) NOT NULL,
    queue character varying(100),
    status character varying(100),

    source_created character varying(100),
    source_modified character varying(100),

    -- created date
    created timestamp without time zone NOT NULL default now(),
    primary key (sk)
);

-- 
-- history record is immutable a new record is created for each data change
--
create table order_job_history
(
    sk serial NOT NULL,  
    -- natural source key job_id
    source_id character varying(100),
    
    -- source data order id
    order_id int NOT NULL,
    order_sk int not null,
    
    -- job type frame or lens
    job_type character varying(100) NOT NULL,
    queue character varying(100),
    status character varying(100),

    source_created character varying(100),
    source_modified character varying(100),

    -- created date
    created timestamp without time zone NOT NULL default now(),
    primary key (sk)
);

-- Bind Table between invoice and orders
create table invoice_orders
(
    sk serial NOT NULL,
    store_sk int NOT NULL REFERENCES store(sk),
    
    invoice_sk int NOT NULL REFERENCES invoice(sk),
    order_sk int NOT NULL REFERENCES order_history(sk),

    -- created date
    created timestamp without time zone NOT NULL default now(),
    primary key (sk)
)

--- -----------------------------------------------------------------------------------------------------------------------------

SET search_path = public;

/*
select count(*) from (select  order_id from stage.job_stage js group by js.order_id) x;
select count(*) from (select  order_id from gnm.order o group by o.order_id) x;
select * from order_history
select * from stage.order_stage
select * from "store"
SELECT sk, storeid, "name", chainid, isactive FROM "store";

select * from order_history

select * from order_job_history
select * from stage.job_stage



*/

-- process the orders
INSERT INTO order_history
(store_sk, 
customer_sk,
source_id, 
order_type, 
queue, status, 
source_created, source_modified)
SELECT 
case when s.sk is null then 0 else s.sk end, 
case when c.sk is null then 0 else c.sk end,
order_id, 
case when os.order_type is null then 'UNKNOWN' else os.order_type end, 
os.queue, os.status, 
os.source_created, os.source_modified
,oh.*
FROM stage.order_stage os 
left join "store" s on s.storeid = os.store_id
left join customer c on c.firstname = os.cust_firstname and c.lastname = os.cust_lastname and c.storesk = s.sk
left join order_history oh on oh.source_id = os.order_id
where 
	oh.source_id is null
	
-- 
-- history record is immutable a new record is created for each data change
--

	drop table public.job_history
	
create table public.job_history
(
    sk serial NOT NULL,  
    -- natural source key job_id
    source_id character varying(100) not NULL,
    
    -- source data order id
    order_id character varying(100) NOT NULL,
    order_sk int not null,
    
    -- job type frame or lens
    job_type character varying(100) NOT NULL,
    
    product_sk int not null references storeproduct(sk),
    supplier_sk int not null references supplier(sk),
    
    queue character varying(100),
    status character varying(100),

    source_created character varying(100),
    source_modified character varying(100),

    -- created date
    created timestamp without time zone NOT NULL default now(),
    primary key (sk)
);


-- select * from public.storeproduct
-- select * from gnm.product
-- select * from stage.job_stage
-- select * from public.supplier

INSERT INTO public.job_history
(source_id, order_id, 
order_sk, 
job_type, 
product_sk, supplier_sk, 
queue, status, 
source_created, source_modified)
select
js.job_id,
js.order_id,
-- order sk
oh.sk,
p.sk, s.sk, 

js.job_type, js.queue, js.status,
js.source_created, js.source_modified
-- select *
from stage.job_stage js 
inner join order_history oh on oh.source_id = js.order_id
inner join supplier s on CAST(s.externalref as varchar(100) ) = js.supplier_id
inner join public.storeproduct p on js.product_id =   p.productid --CAST( p.productid as varchar(100))
where 
	s.sk is not null and  p.sk is not null


-- what we believe is shaped by our ignorance, our beliefs expand to fill the space of our ignorances





-- update store 
select * from public.store_stockmovement

select * from public.invoice i left join public.stock_stockmovement sm on sm.invoice_sk isk = i.sk

---------------------------------------------------------------------------------------------------------------------------------

SET search_path = public;
drop function fn_create_viewbydate_string(tablename text, datecolumnname text);

create or replace function fn_create_viewbydate_string(tablename text, datecolumnname text) returns text as $$
select 'CREATE OR REPLACE VIEW vbyd_' || $1 || ' as select date_part(''year'',sm.' || $2 || ')as year,date_part(''doy'',sm.' 
|| $2 || ') as doy,date_part(''month'',sm.' || $2 || ' )as month,date_part(''dow'',sm.' || $2 || ') as dow,date_part(''day'',sm.'|| $2 ||') as day, sk 
from ' || $1 || ' sm 
group by date_part(''year'',sm.' || $2 || '),date_part(''doy'',sm.' || $2 || '),date_part(''month'',sm.' || $2 || '),date_part(''dow'',sm.' || $2 || '),date_part(''day'',sm.' || $2 || '),sk' as view_string $$
LANGUAGE sql;

drop function fn_create_viewbydate(tablename text, datecolumnname text) ;
create or replace function fn_create_viewbydate(tablename text, datecolumnname text) 
returns void as $func$
begin
	EXECUTE fn_create_viewbydate_string($1, $2);
end
$func$ 
LANGUAGE plpgsql;

select public.fn_create_viewbydate_string('stock_price_history', 'created');


select fn_create_viewbydate('store_stockmovement', 'adjustmentdate');
select fn_create_viewbydate('stock_price_history', 'created');
select fn_create_viewbydate('invoice','createddate');


select * from vbyd_stock_price_history where year = 2017;
select * from vbyd_store_stockmovement where year = 2017;
select * from vybd_invoice where year = 2017 and month = 7;






-- 


