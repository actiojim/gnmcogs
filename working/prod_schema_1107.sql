--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.3
-- Dumped by pg_dump version 10.0

SET statement_timeout = 0;
SET lock_timeout = 0;
-- SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: stage; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA stage;


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = stage, pg_catalog;

--
-- Name: generate_view_for_table(character varying); Type: FUNCTION; Schema: stage; Owner: -
--

CREATE FUNCTION generate_view_for_table(_tname character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$

declare
sql varchar;
begin

SELECT 'CREATE VIEW stage.today_' || _tname || ' AS SELECT ' || string_agg(pick || '->>''' || col || ''' as "' || col || '"',',') || ',runid as actio_runid,createddate as actio_createddate FROM stage.get_latest_data_by_tablename(''' || _tname || ''')'  as sel
FROM (
select distinct 'data' as pick, jsonb_object_keys("data") as col from stage.get_latest_data_by_tablename(_tname)
union all
select distinct 'parameters' as pick, jsonb_object_keys("parameters") as col from stage.get_latest_data_by_tablename(_tname)
) T into sql;

execute sql;

end;

$$;


--
-- Name: get_latest_data_by_tablename(character); Type: FUNCTION; Schema: stage; Owner: -
--

CREATE FUNCTION get_latest_data_by_tablename(_tablename character) RETURNS TABLE(data jsonb, parameters jsonb, runid uuid, createddate timestamp without time zone)
    LANGUAGE plpgsql
    AS $$

declare
sql varchar;
begin
SELECT 'SELECT jsonb_array_elements("data") as data,parameters,T.runid,T.createddate FROM stage.' || _tablename || ' T where T.runid in (
select runid from (
select parameters->>''actio_source'',runid,createddate,row_number() over (partition by parameters->>''actio_source'' order by createddate desc) as r from stage.' || _tablename || ') S where r = 1);' 
into sql;

return query execute sql;

end;

$$;


SET search_path = public, pg_catalog;

SET default_with_oids = false;

--
-- Name: appointment; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE appointment (
    sk integer NOT NULL,
    appointmentid character varying(100),
    storesk integer NOT NULL,
    createddate timestamp without time zone NOT NULL,
    booking_startdate timestamp without time zone NOT NULL,
    booking_enddate timestamp without time zone NOT NULL,
    employeesk integer NOT NULL,
    customersk integer NOT NULL,
    statussk integer NOT NULL,
    appointment_typesk integer NOT NULL,
    mins integer,
    modified timestamp without time zone,
    prospect_firstname character varying(50),
    prospect_lastname character varying(50),
    prospect_title character varying(10),
    prospect_email character varying(150),
    prospect_homephone character varying(150),
    prospect_mobile character varying(150),
    prospect_workphone character varying(150),
    isdeleted boolean DEFAULT false NOT NULL
);


--
-- Name: appointment_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE appointment_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: appointment_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE appointment_sk_seq OWNED BY appointment.sk;


--
-- Name: appointment_type; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE appointment_type (
    sk integer NOT NULL,
    appointment_typeid character varying(30) NOT NULL,
    name character varying(100) NOT NULL
);


--
-- Name: appointment_type_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE appointment_type_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: appointment_type_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE appointment_type_sk_seq OWNED BY appointment_type.sk;


--
-- Name: consult; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE consult (
    sk integer NOT NULL,
    consultid character varying(100),
    consultdate timestamp without time zone NOT NULL,
    storesk integer NOT NULL,
    employeesk integer NOT NULL,
    customersk integer NOT NULL,
    modified timestamp without time zone NOT NULL
);


--
-- Name: consult_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE consult_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: consult_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE consult_sk_seq OWNED BY consult.sk;


--
-- Name: consult_type; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE consult_type (
    sk integer NOT NULL,
    consult_typeid character varying(30) NOT NULL,
    description character varying(100) NOT NULL
);


--
-- Name: consult_type_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE consult_type_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: consult_type_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE consult_type_sk_seq OWNED BY consult_type.sk;


--
-- Name: consultitem; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE consultitem (
    sk integer NOT NULL,
    consultsk integer NOT NULL,
    consult_typesk integer NOT NULL
);


--
-- Name: consultitem_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE consultitem_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: consultitem_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE consultitem_sk_seq OWNED BY consultitem.sk;


--
-- Name: customer; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE customer (
    sk integer NOT NULL,
    createddate timestamp without time zone NOT NULL,
    customerid character varying(500),
    firstname character varying(50) NOT NULL,
    lastname character varying(50),
    middlename character varying(50),
    title character varying(10) NOT NULL,
    gender character varying(10) NOT NULL,
    birthdate date,
    preferredname character varying(50),
    preferredcontactmethod character varying(50),
    email character varying(150) NOT NULL,
    emailcreated timestamp without time zone DEFAULT ('now'::text)::date NOT NULL,
    phonecountryid integer,
    phonecreated timestamp without time zone DEFAULT ('now'::text)::date NOT NULL,
    address1 character varying(250),
    address2 character varying(250),
    address3 character varying(250),
    state character varying(60),
    suburb character varying(60),
    postcode integer,
    firstvisit timestamp without time zone,
    lastvisit timestamp without time zone,
    lastconsult timestamp without time zone,
    lastrecall timestamp without time zone,
    nextrecall timestamp without time zone,
    lastrecalldesc character varying(250),
    nextrecalldesc character varying(250),
    lastoptomseen integer,
    storesk integer DEFAULT 0 NOT NULL,
    healthfund character varying(50),
    modified timestamp without time zone,
    optout_recall boolean DEFAULT false NOT NULL,
    optout_marketing boolean DEFAULT false NOT NULL,
    deceased boolean DEFAULT false NOT NULL,
    homephone character varying(150),
    mobile character varying(150),
    workphone character varying(150),
    isaddrvalid boolean
);


--
-- Name: customer_ext; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE customer_ext (
    sk integer NOT NULL,
    customersk integer NOT NULL,
    s_recall character varying(1),
    s_cmsms character varying(1),
    s_cmmobile character varying(1),
    s_cmwphone character varying(1),
    s_cmhphone character varying(1),
    s_cmemail character varying(1),
    s_cmletter character varying(1),
    s_cmfno boolean,
    s_cmnoemail boolean,
    s_apnotif integer,
    s_apreminder integer,
    o_pref_recallint character varying(20),
    o_pref_recallsub character varying(20),
    o_pref_ordercoll character varying(20),
    o_pref_appointments character varying(20),
    o_pref_marketing character varying(20),
    o_pref_telephone character varying(20),
    o_badaddress boolean,
    o_badphone boolean,
    o_bademail boolean,
    o_noemail boolean,
    modified timestamp without time zone,
    o_inactive boolean,
    o_inactive_reason character varying(255)
);


--
-- Name: customer_ext_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE customer_ext_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: customer_ext_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE customer_ext_sk_seq OWNED BY customer_ext.sk;


--
-- Name: customer_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE customer_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: customer_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE customer_sk_seq OWNED BY customer.sk;


--
-- Name: discountcode; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE discountcode (
    sk integer NOT NULL,
    storeid character varying(12) DEFAULT 'All'::character varying NOT NULL,
    discountitemcode character varying(100) NOT NULL,
    description character varying(150),
    valid_from timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone,
    valid_to timestamp without time zone DEFAULT '2099-12-31 00:00:00'::timestamp without time zone
);


--
-- Name: discountcode_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE discountcode_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: discountcode_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE discountcode_sk_seq OWNED BY discountcode.sk;


--
-- Name: employee; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE employee (
    sk integer NOT NULL,
    createddate timestamp without time zone NOT NULL,
    employeeid character varying(500),
    employeetype character varying(50),
    employmentstartdate date,
    employmentenddate date,
    firstname character varying(50) NOT NULL,
    lastname character varying(50),
    middlename character varying(50),
    title character varying(10) NOT NULL,
    gender character varying(10) NOT NULL,
    birthdate date,
    preferredname character varying(50),
    preferredcontactmethod character varying(10),
    email character varying(150) NOT NULL,
    emailcreated timestamp without time zone DEFAULT ('now'::text)::date NOT NULL,
    phonecountryid integer,
    phonenumber character varying(150),
    phonerawnumber character varying(150),
    phonemobile boolean NOT NULL,
    phonesmsenabled boolean NOT NULL,
    phonefax boolean NOT NULL,
    phonelandline boolean NOT NULL,
    phonecreated timestamp without time zone DEFAULT ('now'::text)::date NOT NULL,
    storesk integer DEFAULT 0 NOT NULL
);


--
-- Name: employee_providerno; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE employee_providerno (
    sk integer NOT NULL,
    employeesk integer NOT NULL,
    storesk integer NOT NULL,
    providerno character varying(30),
    modified timestamp without time zone DEFAULT timezone('Australia/Sydney'::text, now()) NOT NULL
);


--
-- Name: employee_providerno_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE employee_providerno_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: employee_providerno_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE employee_providerno_sk_seq OWNED BY employee_providerno.sk;


--
-- Name: employee_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE employee_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: employee_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE employee_sk_seq OWNED BY employee.sk;


--
-- Name: expense; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE expense (
    sk integer NOT NULL,
    expenseid character varying(100),
    storesk integer NOT NULL,
    createddate timestamp without time zone NOT NULL,
    employeesk integer NOT NULL,
    expense_typesk integer NOT NULL,
    description character varying(100),
    amount_incl_gst numeric(19,4),
    amount_gst numeric(19,4),
    modified timestamp without time zone
);


--
-- Name: expense_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE expense_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: expense_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE expense_sk_seq OWNED BY expense.sk;


--
-- Name: expense_type; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE expense_type (
    sk integer NOT NULL,
    expense_typeid character varying(30) NOT NULL,
    name character varying(100) NOT NULL
);


--
-- Name: expense_type_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE expense_type_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: expense_type_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE expense_type_sk_seq OWNED BY expense_type.sk;


--
-- Name: invoice; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE invoice (
    sk integer NOT NULL,
    invoiceid character varying(100),
    createddate timestamp without time zone NOT NULL,
    storesk integer NOT NULL,
    employeesk integer NOT NULL,
    customersk integer NOT NULL,
    amount_incl_gst numeric(19,4),
    modified timestamp without time zone,
    amount_gst numeric(19,4),
    reversalref character varying(100),
    rebateref character varying(100),
    is_writeoff_invoice boolean DEFAULT false,
    writeoffref character varying(100)
);


--
-- Name: invoice_orders; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE invoice_orders (
    sk integer NOT NULL,
    store_sk integer NOT NULL,
    invoice_sk integer NOT NULL,
    order_sk integer NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: invoice_orders_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE invoice_orders_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: invoice_orders_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE invoice_orders_sk_seq OWNED BY invoice_orders.sk;


--
-- Name: invoice_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE invoice_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: invoice_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE invoice_sk_seq OWNED BY invoice.sk;


--
-- Name: invoiceitem; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE invoiceitem (
    invoicesk integer NOT NULL,
    pos integer NOT NULL,
    productsk integer NOT NULL,
    storesk integer NOT NULL,
    employeesk integer NOT NULL,
    serviceemployeesk integer NOT NULL,
    customersk integer NOT NULL,
    createddate timestamp without time zone NOT NULL,
    quantity integer NOT NULL,
    amount_incl_gst numeric(19,4),
    modified timestamp without time zone,
    amount_gst numeric(19,4),
    discount_incl_gst numeric(19,4),
    discount_gst numeric(19,4),
    cost_incl_gst numeric(19,4),
    cost_gst numeric(19,4),
    description character varying(500),
    payeesk integer,
    invoiceitemid character varying(60) DEFAULT ''::character varying NOT NULL,
    return_incl_gst numeric(19,4),
    return_gst numeric(19,4),
    bulkbillref character varying(100)
);


--
-- Name: job_process_status; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE job_process_status (
    sk integer NOT NULL,
    jobname character varying(255) NOT NULL,
    status character varying(10) NOT NULL,
    description character varying(255),
    lastexecution timestamp without time zone NOT NULL
);


--
-- Name: job_process_status_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE job_process_status_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: job_process_status_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE job_process_status_sk_seq OWNED BY job_process_status.sk;


--
-- Name: order_history; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE order_history (
    sk integer NOT NULL,
    store_sk integer NOT NULL,
    customer_sk integer NOT NULL,
    source_id character varying(100),
    order_type character varying(100) NOT NULL,
    queue character varying(100),
    status character varying(100),
    source_created character varying(100),
    source_modified character varying(100),
    created timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: order_history_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE order_history_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: order_history_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE order_history_sk_seq OWNED BY order_history.sk;


--
-- Name: order_job_history; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE order_job_history (
    sk integer NOT NULL,
    source_id character varying(100),
    order_id integer NOT NULL,
    order_sk integer NOT NULL,
    job_type character varying(100) NOT NULL,
    queue character varying(100),
    status character varying(100),
    source_created character varying(100),
    source_modified character varying(100),
    created timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: order_job_history_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE order_job_history_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: order_job_history_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE order_job_history_sk_seq OWNED BY order_job_history.sk;


--
-- Name: payee; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE payee (
    sk integer NOT NULL,
    storeid character varying(12),
    payeeid character varying(100),
    name character varying(200),
    payeetype character varying(150),
    chainid character varying(100) DEFAULT ''::character varying NOT NULL
);


--
-- Name: payee_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE payee_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: payee_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE payee_sk_seq OWNED BY payee.sk;


--
-- Name: payment; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE payment (
    sk integer NOT NULL,
    paymentid character varying(100),
    createddate timestamp without time zone NOT NULL,
    amount_incl_gst numeric(19,4),
    modified timestamp without time zone,
    amount_gst numeric(19,4),
    storeid character varying(100) DEFAULT ''::character varying,
    payeeid character varying(100) DEFAULT ''::character varying,
    employeeid character varying(100) DEFAULT ''::character varying,
    customerid character varying(100) DEFAULT ''::character varying,
    payment_typeid character varying(100) DEFAULT ''::character varying
);


--
-- Name: payment_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE payment_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: payment_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE payment_sk_seq OWNED BY payment.sk;


--
-- Name: payment_type; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE payment_type (
    sk integer NOT NULL,
    payment_typeid character varying(30) NOT NULL,
    name character varying(100) NOT NULL
);


--
-- Name: payment_type_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE payment_type_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: payment_type_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE payment_type_sk_seq OWNED BY payment_type.sk;


--
-- Name: paymentitem; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE paymentitem (
    storesk integer NOT NULL,
    paymentsk integer NOT NULL,
    invoicesk integer NOT NULL,
    customersk integer NOT NULL,
    payment_typesk integer NOT NULL,
    payment_employeesk integer NOT NULL,
    invoice_employeesk integer NOT NULL,
    payment_createddate timestamp without time zone NOT NULL,
    invoice_createddate timestamp without time zone NOT NULL,
    payment_amount_incl_gst numeric(19,4),
    invoice_amount_incl_gst numeric(19,4),
    modified timestamp without time zone,
    payment_amount_gst numeric(19,4),
    paymentitemid character varying(60) DEFAULT ''::character varying NOT NULL,
    payeesk integer,
    rebateref character varying(30)
);


--
-- Name: product_price_history; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE product_price_history (
    id bigint NOT NULL,
    product_id character varying(8) NOT NULL,
    supplier_cost numeric(19,2),
    supplier_cost_gst numeric(19,2),
    actual_cost numeric(19,2),
    actual_cost_gst numeric(19,2),
    supplier_rrp numeric(19,2),
    supplier_rrp_gst numeric(19,2),
    calculated_retail_price numeric(19,2),
    calculated_retail_price_gst numeric(19,2),
    override_retail_price numeric(19,2),
    override_retail_price_gst numeric(19,2),
    retail_price numeric(19,2),
    retail_price_gst numeric(19,2),
    effective_from_time timestamp without time zone NOT NULL,
    effective_to_time timestamp without time zone
);


--
-- Name: product_price_history_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE product_price_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: product_price_history_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE product_price_history_id_seq OWNED BY product_price_history.id;


--
-- Name: schema_version; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE schema_version (
    installed_rank integer NOT NULL,
    version character varying(50),
    description character varying(200) NOT NULL,
    type character varying(20) NOT NULL,
    script character varying(1000) NOT NULL,
    checksum integer,
    installed_by character varying(100) NOT NULL,
    installed_on timestamp without time zone DEFAULT now() NOT NULL,
    execution_time integer NOT NULL,
    success boolean NOT NULL
);


--
-- Name: spectaclejob; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE spectaclejob (
    sk integer NOT NULL,
    invoicesk integer,
    framesk integer,
    right_lenssk integer,
    left_lenssk integer,
    storesk integer NOT NULL,
    createddate timestamp without time zone NOT NULL,
    orderdate timestamp without time zone,
    receiveddate timestamp without time zone,
    pickupdate timestamp without time zone,
    modified timestamp without time zone NOT NULL,
    spectaclejobid character varying(60) DEFAULT ''::character varying NOT NULL,
    pms_invoicenum character varying(100)
);


--
-- Name: spectaclejob_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE spectaclejob_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: spectaclejob_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE spectaclejob_sk_seq OWNED BY spectaclejob.sk;


--
-- Name: status; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE status (
    sk integer NOT NULL,
    statusid character varying(30) NOT NULL,
    name character varying(100) NOT NULL
);


--
-- Name: status_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE status_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: status_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE status_sk_seq OWNED BY status.sk;


--
-- Name: stock_price_history; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE stock_price_history (
    sk integer NOT NULL,
    store_sk integer NOT NULL,
    source_id character varying(100) NOT NULL,
    takeon_storeproduct_sk integer NOT NULL,
    storeproduct_sk integer NOT NULL,
    supplier_cost numeric(19,2),
    supplier_cost_gst numeric(19,2),
    actual_cost numeric(19,2),
    actual_cost_gst numeric(19,2),
    supplier_rrp numeric(19,2),
    supplier_rrp_gst numeric(19,2),
    calculated_retail_price numeric(19,2),
    calculated_retail_price_gst numeric(19,2),
    override_retail_price numeric(19,2),
    override_retail_price_gst numeric(19,2),
    retail_price numeric(19,2),
    retail_price_gst numeric(19,2),
    avg_cost_price numeric(19,2),
    created timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: stock_price_history_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE stock_price_history_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: stock_price_history_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE stock_price_history_sk_seq OWNED BY stock_price_history.sk;


--
-- Name: store; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE store (
    sk integer NOT NULL,
    storeid character varying(12) NOT NULL,
    name character varying(100) NOT NULL,
    chainid character varying(100) DEFAULT ''::character varying NOT NULL,
    isactive boolean DEFAULT false NOT NULL,
    pms character varying(20),
    store_nk character varying(60),
    acquisition_date timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL
);


--
-- Name: store_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE store_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: store_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE store_sk_seq OWNED BY store.sk;


--
-- Name: store_stock_history; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE store_stock_history (
    sk integer NOT NULL,
    store_sk integer NOT NULL,
    source_id character varying(100) NOT NULL,
    takeon_storeproduct_sk integer NOT NULL,
    storeproduct_sk integer NOT NULL,
    total integer NOT NULL,
    prior_total integer NOT NULL,
    reason character varying(20) NOT NULL,
    comments character varying(500),
    employee_sk integer,
    created timestamp without time zone DEFAULT now()
);


--
-- Name: store_stock_history_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE store_stock_history_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: store_stock_history_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE store_stock_history_sk_seq OWNED BY store_stock_history.sk;


--
-- Name: store_stockmovement; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE store_stockmovement (
    sk integer NOT NULL,
    store_sk integer NOT NULL,
    source_id character varying(100) NOT NULL,
    takeon_storeproduct_sk integer NOT NULL,
    storeproduct_sk integer NOT NULL,
    adjustmentdate timestamp without time zone,
    stock_price_history_sk integer NOT NULL,
    adjustment integer NOT NULL,
    adjustmenttype character varying(20) NOT NULL,
    reason character varying(20) NOT NULL,
    invoice_sk integer,
    customer_sk integer,
    shipping_ref character varying(100),
    shipping_operator character varying(100),
    comments character varying(500),
    employee_sk integer,
    created timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: store_stockmovement_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE store_stockmovement_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: store_stockmovement_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE store_stockmovement_sk_seq OWNED BY store_stockmovement.sk;


--
-- Name: storeproduct; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE storeproduct (
    sk integer NOT NULL,
    storeid character varying(12),
    productid character varying(500),
    type character varying(100),
    datecreated timestamp without time zone,
    name character varying(500),
    description character varying(500),
    retailprice numeric(19,4),
    wholesaleprice numeric(19,4),
    modified timestamp without time zone,
    last_purchase timestamp without time zone,
    last_sale timestamp without time zone,
    quantity integer,
    frame_model character varying(100) DEFAULT ''::character varying,
    frame_colour character varying(100) DEFAULT ''::character varying,
    frame_brand character varying(100) DEFAULT ''::character varying,
    frame_temple numeric(8,2) DEFAULT 0,
    frame_bridge numeric(8,2) DEFAULT 0,
    frame_eye_size numeric(8,2) DEFAULT 0,
    frame_depth numeric(8,2) DEFAULT 0,
    frame_material character varying(100) DEFAULT ''::character varying,
    lens_type character varying(100) DEFAULT ''::character varying,
    lens_refractive_index numeric(19,4) DEFAULT 0,
    lens_stock_grind character varying(10) DEFAULT ''::character varying,
    lens_size numeric(4,2) DEFAULT 0,
    lens_segment_size numeric(4,2) DEFAULT 0,
    internal_sku character varying(100),
    distributor character varying(100)
);


--
-- Name: storeproduct_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE storeproduct_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: storeproduct_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE storeproduct_sk_seq OWNED BY storeproduct.sk;


--
-- Name: supplier_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE supplier_sk_seq
    START WITH 60
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: supplier; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE supplier (
    sk integer DEFAULT nextval('supplier_sk_seq'::regclass) NOT NULL,
    externalref bigint NOT NULL,
    supplierid character varying(255) NOT NULL,
    name character varying(255) NOT NULL
);


--
-- Name: supplierproduct_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE supplierproduct_sk_seq
    START WITH 500
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: supplierproduct; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE supplierproduct (
    sk integer DEFAULT nextval('supplierproduct_sk_seq'::regclass) NOT NULL,
    suppliersku character varying(255) NOT NULL,
    suppliersk integer NOT NULL,
    frame_model character varying(255),
    frame_colour character varying(100),
    internal_sku character varying(100),
    frame_brand character varying(100)
);


--
-- Name: takeon_product; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE takeon_product (
    sk integer NOT NULL,
    store_sk integer NOT NULL,
    supplier_sk integer NOT NULL,
    source_id character varying(100) NOT NULL,
    supplier_sku character varying(100) NOT NULL,
    name character varying(500) NOT NULL,
    description character varying(500) NOT NULL,
    model character varying(500) NOT NULL,
    brand character varying(500) NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: takeon_product_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE takeon_product_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: takeon_product_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE takeon_product_sk_seq OWNED BY takeon_product.sk;


SET search_path = stage, pg_catalog;

--
-- Name: sunix_vconspec; Type: TABLE; Schema: stage; Owner: -
--

CREATE TABLE sunix_vconspec (
    runid uuid,
    createddate timestamp without time zone DEFAULT now(),
    configname character varying(100),
    pipename character varying(100),
    parameters jsonb,
    data jsonb
);


--
-- Name: sunix_vdoctor; Type: TABLE; Schema: stage; Owner: -
--

CREATE TABLE sunix_vdoctor (
    runid uuid,
    createddate timestamp without time zone DEFAULT now(),
    configname character varying(100),
    pipename character varying(100),
    parameters jsonb,
    data jsonb
);


--
-- Name: sunix_vpatient; Type: TABLE; Schema: stage; Owner: -
--

CREATE TABLE sunix_vpatient (
    runid uuid,
    createddate timestamp without time zone DEFAULT now(),
    configname character varying(100),
    pipename character varying(100),
    parameters jsonb,
    data jsonb
);


--
-- Name: today_sunix_vconspec; Type: VIEW; Schema: stage; Owner: -
--

CREATE VIEW today_sunix_vconspec AS
 SELECT (get_latest_data_by_tablename.data ->> 'L_IOPA'::text) AS "L_IOPA",
    (get_latest_data_by_tablename.data ->> 'R_SPHERE6'::text) AS "R_SPHERE6",
    (get_latest_data_by_tablename.data ->> 'R_VA6'::text) AS "R_VA6",
    (get_latest_data_by_tablename.data ->> 'L_NVA5'::text) AS "L_NVA5",
    (get_latest_data_by_tablename.data ->> 'CRTUSER'::text) AS "CRTUSER",
    (get_latest_data_by_tablename.data ->> 'RECALLTYPE'::text) AS "RECALLTYPE",
    (get_latest_data_by_tablename.data ->> 'L_VA1'::text) AS "L_VA1",
    (get_latest_data_by_tablename.data ->> 'L_AXIS2'::text) AS "L_AXIS2",
    (get_latest_data_by_tablename.data ->> 'RX_TYPE2'::text) AS "RX_TYPE2",
    (get_latest_data_by_tablename.data ->> 'NOTE6'::text) AS "NOTE6",
    (get_latest_data_by_tablename.data ->> 'ALLERGY'::text) AS "ALLERGY",
    (get_latest_data_by_tablename.data ->> 'SYMP3'::text) AS "SYMP3",
    (get_latest_data_by_tablename.data ->> 'PD'::text) AS "PD",
    (get_latest_data_by_tablename.data ->> 'OPTOM'::text) AS "OPTOM",
    (get_latest_data_by_tablename.data ->> 'CREATED'::text) AS "CREATED",
    (get_latest_data_by_tablename.data ->> 'RECORDED'::text) AS "RECORDED",
    (get_latest_data_by_tablename.data ->> 'NOTE5'::text) AS "NOTE5",
    (get_latest_data_by_tablename.data ->> 'R_VA4'::text) AS "R_VA4",
    (get_latest_data_by_tablename.data ->> 'L_AXIS6'::text) AS "L_AXIS6",
    (get_latest_data_by_tablename.data ->> 'R_CYL4'::text) AS "R_CYL4",
    (get_latest_data_by_tablename.data ->> 'ACNTLLENS'::text) AS "ACNTLLENS",
    (get_latest_data_by_tablename.data ->> 'R_CYL2'::text) AS "R_CYL2",
    (get_latest_data_by_tablename.data ->> 'R_VPRISM5'::text) AS "R_VPRISM5",
    (get_latest_data_by_tablename.data ->> 'L_CYL6'::text) AS "L_CYL6",
    (get_latest_data_by_tablename.data ->> 'L_VA5'::text) AS "L_VA5",
    (get_latest_data_by_tablename.data ->> 'L_CYL2'::text) AS "L_CYL2",
    (get_latest_data_by_tablename.data ->> 'CONV'::text) AS "CONV",
    (get_latest_data_by_tablename.data ->> 'HEAD2'::text) AS "HEAD2",
    (get_latest_data_by_tablename.data ->> 'DISTAID'::text) AS "DISTAID",
    (get_latest_data_by_tablename.data ->> 'RX_TYPE3'::text) AS "RX_TYPE3",
    (get_latest_data_by_tablename.data ->> 'L_NVA2'::text) AS "L_NVA2",
    (get_latest_data_by_tablename.data ->> 'L_VPRISM5'::text) AS "L_VPRISM5",
    (get_latest_data_by_tablename.data ->> 'HEAD9'::text) AS "HEAD9",
    (get_latest_data_by_tablename.data ->> 'HEAD1'::text) AS "HEAD1",
    (get_latest_data_by_tablename.data ->> 'L_CYL5'::text) AS "L_CYL5",
    (get_latest_data_by_tablename.data ->> 'R_SPHERE5'::text) AS "R_SPHERE5",
    (get_latest_data_by_tablename.data ->> 'DRAW1'::text) AS "DRAW1",
    (get_latest_data_by_tablename.data ->> 'B_MINUS'::text) AS "B_MINUS",
    (get_latest_data_by_tablename.data ->> 'R_CYL1'::text) AS "R_CYL1",
    (get_latest_data_by_tablename.data ->> 'DISTBUP'::text) AS "DISTBUP",
    (get_latest_data_by_tablename.data ->> 'RPD'::text) AS "RPD",
    (get_latest_data_by_tablename.data ->> 'XFER'::text) AS "XFER",
    (get_latest_data_by_tablename.data ->> 'BIOM2'::text) AS "BIOM2",
    (get_latest_data_by_tablename.data ->> 'LOGSTR'::text) AS "LOGSTR",
    (get_latest_data_by_tablename.data ->> 'OU_UNVA'::text) AS "OU_UNVA",
    (get_latest_data_by_tablename.data ->> 'ACCOM_OU'::text) AS "ACCOM_OU",
    (get_latest_data_by_tablename.data ->> 'IOP_TIME'::text) AS "IOP_TIME",
    (get_latest_data_by_tablename.data ->> 'L_SPHERE3'::text) AS "L_SPHERE3",
    (get_latest_data_by_tablename.data ->> 'RXNOTE6'::text) AS "RXNOTE6",
    (get_latest_data_by_tablename.data ->> 'L_INT4'::text) AS "L_INT4",
    (get_latest_data_by_tablename.data ->> 'PUPIL4'::text) AS "PUPIL4",
    (get_latest_data_by_tablename.data ->> 'R_CYL5'::text) AS "R_CYL5",
    (get_latest_data_by_tablename.data ->> 'RPACHYMTY'::text) AS "RPACHYMTY",
    (get_latest_data_by_tablename.data ->> 'L_UNVA'::text) AS "L_UNVA",
    (get_latest_data_by_tablename.data ->> 'DISTUNAID'::text) AS "DISTUNAID",
    (get_latest_data_by_tablename.data ->> 'LPD'::text) AS "LPD",
    (get_latest_data_by_tablename.data ->> 'L_UVA'::text) AS "L_UVA",
    (get_latest_data_by_tablename.data ->> 'SYMPTOM1'::text) AS "SYMPTOM1",
    (get_latest_data_by_tablename.data ->> 'DRAW'::text) AS "DRAW",
    (get_latest_data_by_tablename.data ->> 'SYMP4'::text) AS "SYMP4",
    (get_latest_data_by_tablename.data ->> 'R_VA5'::text) AS "R_VA5",
    (get_latest_data_by_tablename.data ->> 'DISPPAGE'::text) AS "DISPPAGE",
    (get_latest_data_by_tablename.data ->> 'R_INT6'::text) AS "R_INT6",
    (get_latest_data_by_tablename.data ->> 'L_VA4'::text) AS "L_VA4",
    (get_latest_data_by_tablename.data ->> 'R_AXIS4'::text) AS "R_AXIS4",
    (get_latest_data_by_tablename.data ->> 'UD_PHOIRA'::text) AS "UD_PHOIRA",
    (get_latest_data_by_tablename.data ->> 'B_PLUS'::text) AS "B_PLUS",
    (get_latest_data_by_tablename.data ->> 'UN_VPHORIA'::text) AS "UN_VPHORIA",
    (get_latest_data_by_tablename.data ->> 'R_SPHERE1'::text) AS "R_SPHERE1",
    (get_latest_data_by_tablename.data ->> 'AN_NRC'::text) AS "AN_NRC",
    (get_latest_data_by_tablename.data ->> 'L_PRISM5'::text) AS "L_PRISM5",
    (get_latest_data_by_tablename.data ->> 'PUPIL'::text) AS "PUPIL",
    (get_latest_data_by_tablename.data ->> 'R2'::text) AS "R2",
    (get_latest_data_by_tablename.data ->> 'RX_TYPE1'::text) AS "RX_TYPE1",
    (get_latest_data_by_tablename.data ->> 'LOCATION1'::text) AS "LOCATION1",
    (get_latest_data_by_tablename.data ->> 'AD_HPHORIA'::text) AS "AD_HPHORIA",
    (get_latest_data_by_tablename.data ->> 'BIOM1'::text) AS "BIOM1",
    (get_latest_data_by_tablename.data ->> 'L_NVA4'::text) AS "L_NVA4",
    (get_latest_data_by_tablename.data ->> 'AN_PHOIRA'::text) AS "AN_PHOIRA",
    (get_latest_data_by_tablename.data ->> 'R_NVA2'::text) AS "R_NVA2",
    (get_latest_data_by_tablename.data ->> 'DIST'::text) AS "DIST",
    (get_latest_data_by_tablename.data ->> 'MODIFIED0'::text) AS "MODIFIED0",
    (get_latest_data_by_tablename.data ->> 'MEDICATION'::text) AS "MEDICATION",
    (get_latest_data_by_tablename.data ->> 'UN_HPHORIA'::text) AS "UN_HPHORIA",
    (get_latest_data_by_tablename.data ->> 'SYMP1'::text) AS "SYMP1",
    (get_latest_data_by_tablename.data ->> 'R_ADD6'::text) AS "R_ADD6",
    (get_latest_data_by_tablename.data ->> 'PHVAL'::text) AS "PHVAL",
    (get_latest_data_by_tablename.data ->> 'OU_UVA'::text) AS "OU_UVA",
    (get_latest_data_by_tablename.data ->> 'FEXCLUDE'::text) AS "FEXCLUDE",
    (get_latest_data_by_tablename.data ->> 'OPH2'::text) AS "OPH2",
    (get_latest_data_by_tablename.data ->> 'L_ADD3'::text) AS "L_ADD3",
    (get_latest_data_by_tablename.data ->> 'L_ADD6'::text) AS "L_ADD6",
    (get_latest_data_by_tablename.data ->> 'ACCOM_LE'::text) AS "ACCOM_LE",
    (get_latest_data_by_tablename.data ->> 'OPH1'::text) AS "OPH1",
    (get_latest_data_by_tablename.data ->> 'AN_VPHORIA'::text) AS "AN_VPHORIA",
    (get_latest_data_by_tablename.data ->> 'BIOM4'::text) AS "BIOM4",
    (get_latest_data_by_tablename.data ->> 'HEAD4'::text) AS "HEAD4",
    (get_latest_data_by_tablename.data ->> 'HEAD3'::text) AS "HEAD3",
    (get_latest_data_by_tablename.data ->> 'AD_NRC'::text) AS "AD_NRC",
    (get_latest_data_by_tablename.data ->> 'PHVAR'::text) AS "PHVAR",
    (get_latest_data_by_tablename.data ->> 'L_CYL3'::text) AS "L_CYL3",
    (get_latest_data_by_tablename.data ->> 'L_INT6'::text) AS "L_INT6",
    (get_latest_data_by_tablename.data ->> 'L_AXIS4'::text) AS "L_AXIS4",
    (get_latest_data_by_tablename.data ->> 'CONDATE'::text) AS "CONDATE",
    (get_latest_data_by_tablename.data ->> 'L_AXIS5'::text) AS "L_AXIS5",
    (get_latest_data_by_tablename.data ->> 'DRAWING'::text) AS "DRAWING",
    (get_latest_data_by_tablename.data ->> 'L_AXIS3'::text) AS "L_AXIS3",
    (get_latest_data_by_tablename.data ->> 'L_ADD4'::text) AS "L_ADD4",
    (get_latest_data_by_tablename.data ->> 'R_INT1'::text) AS "R_INT1",
    (get_latest_data_by_tablename.data ->> 'RXNOTE3'::text) AS "RXNOTE3",
    (get_latest_data_by_tablename.data ->> 'R_AXIS3'::text) AS "R_AXIS3",
    (get_latest_data_by_tablename.data ->> 'SYMPTOM2'::text) AS "SYMPTOM2",
    (get_latest_data_by_tablename.data ->> 'ACCOM'::text) AS "ACCOM",
    (get_latest_data_by_tablename.data ->> 'R_ADD2'::text) AS "R_ADD2",
    (get_latest_data_by_tablename.data ->> 'R_CYL6'::text) AS "R_CYL6",
    (get_latest_data_by_tablename.data ->> 'R_AXIS6'::text) AS "R_AXIS6",
    (get_latest_data_by_tablename.data ->> 'L_PRISM1'::text) AS "L_PRISM1",
    (get_latest_data_by_tablename.data ->> 'COLOUR_VI'::text) AS "COLOUR_VI",
    (get_latest_data_by_tablename.data ->> 'R_NVA3'::text) AS "R_NVA3",
    (get_latest_data_by_tablename.data ->> 'R_INT5'::text) AS "R_INT5",
    (get_latest_data_by_tablename.data ->> 'R_VA1'::text) AS "R_VA1",
    (get_latest_data_by_tablename.data ->> 'BILLTYPE'::text) AS "BILLTYPE",
    (get_latest_data_by_tablename.data ->> 'R_UNVA'::text) AS "R_UNVA",
    (get_latest_data_by_tablename.data ->> 'R_AXIS2'::text) AS "R_AXIS2",
    (get_latest_data_by_tablename.data ->> 'DISTBDN'::text) AS "DISTBDN",
    (get_latest_data_by_tablename.data ->> 'ANUFXCYL'::text) AS "ANUFXCYL",
    (get_latest_data_by_tablename.data ->> 'L_IOP'::text) AS "L_IOP",
    (get_latest_data_by_tablename.data ->> 'REFNUM'::text) AS "REFNUM",
    (get_latest_data_by_tablename.data ->> 'HEAD7'::text) AS "HEAD7",
    (get_latest_data_by_tablename.data ->> 'R1'::text) AS "R1",
    (get_latest_data_by_tablename.data ->> 'NOTE9'::text) AS "NOTE9",
    (get_latest_data_by_tablename.data ->> 'AD_PHOIRA'::text) AS "AD_PHOIRA",
    (get_latest_data_by_tablename.data ->> 'R_SPHERE4'::text) AS "R_SPHERE4",
    (get_latest_data_by_tablename.data ->> 'L_CYL1'::text) AS "L_CYL1",
    (get_latest_data_by_tablename.data ->> 'RX_TYPE4'::text) AS "RX_TYPE4",
    (get_latest_data_by_tablename.data ->> 'R_SPHERE2'::text) AS "R_SPHERE2",
    (get_latest_data_by_tablename.data ->> 'REFNUM0'::text) AS "REFNUM0",
    (get_latest_data_by_tablename.data ->> 'L_VA2'::text) AS "L_VA2",
    (get_latest_data_by_tablename.data ->> 'X_CYL'::text) AS "X_CYL",
    (get_latest_data_by_tablename.data ->> 'L_NVA1'::text) AS "L_NVA1",
    (get_latest_data_by_tablename.data ->> 'HIST2'::text) AS "HIST2",
    (get_latest_data_by_tablename.data ->> 'R_UVA'::text) AS "R_UVA",
    (get_latest_data_by_tablename.data ->> 'L_INT5'::text) AS "L_INT5",
    (get_latest_data_by_tablename.data ->> 'MEMO18'::text) AS "MEMO18",
    (get_latest_data_by_tablename.data ->> 'L_CYL4'::text) AS "L_CYL4",
    (get_latest_data_by_tablename.data ->> 'RX_TYPE5'::text) AS "RX_TYPE5",
    (get_latest_data_by_tablename.data ->> 'RX_TYPE6'::text) AS "RX_TYPE6",
    (get_latest_data_by_tablename.data ->> 'NOTE3'::text) AS "NOTE3",
    (get_latest_data_by_tablename.data ->> 'DRAWFLAG'::text) AS "DRAWFLAG",
    (get_latest_data_by_tablename.data ->> 'MODGIVEN'::text) AS "MODGIVEN",
    (get_latest_data_by_tablename.data ->> 'R_ADD3'::text) AS "R_ADD3",
    (get_latest_data_by_tablename.data ->> 'R_CYL3'::text) AS "R_CYL3",
    (get_latest_data_by_tablename.data ->> 'PUPIL3'::text) AS "PUPIL3",
    (get_latest_data_by_tablename.data ->> 'LOCATION'::text) AS "LOCATION",
    (get_latest_data_by_tablename.data ->> 'PUPILNOTE'::text) AS "PUPILNOTE",
    (get_latest_data_by_tablename.data ->> 'CONREFNUM'::text) AS "CONREFNUM",
    (get_latest_data_by_tablename.data ->> 'COVERN'::text) AS "COVERN",
    (get_latest_data_by_tablename.data ->> 'L_INT3'::text) AS "L_INT3",
    (get_latest_data_by_tablename.data ->> 'R_NVA4'::text) AS "R_NVA4",
    (get_latest_data_by_tablename.data ->> 'R_AXIS5'::text) AS "R_AXIS5",
    (get_latest_data_by_tablename.data ->> 'LPACHYMTY'::text) AS "LPACHYMTY",
    (get_latest_data_by_tablename.data ->> 'BIOM3'::text) AS "BIOM3",
    (get_latest_data_by_tablename.data ->> 'NOTE1'::text) AS "NOTE1",
    (get_latest_data_by_tablename.data ->> 'RXNOTE4'::text) AS "RXNOTE4",
    (get_latest_data_by_tablename.data ->> 'R_INT4'::text) AS "R_INT4",
    (get_latest_data_by_tablename.data ->> 'L_VA6'::text) AS "L_VA6",
    (get_latest_data_by_tablename.data ->> 'R_INT3'::text) AS "R_INT3",
    (get_latest_data_by_tablename.data ->> 'NEXTVISIT'::text) AS "NEXTVISIT",
    (get_latest_data_by_tablename.data ->> 'R_ADD4'::text) AS "R_ADD4",
    (get_latest_data_by_tablename.data ->> 'MODUSER'::text) AS "MODUSER",
    (get_latest_data_by_tablename.data ->> 'L_SPHERE1'::text) AS "L_SPHERE1",
    (get_latest_data_by_tablename.data ->> 'R_SPHERE3'::text) AS "R_SPHERE3",
    (get_latest_data_by_tablename.data ->> 'LOCKID'::text) AS "LOCKID",
    (get_latest_data_by_tablename.data ->> 'MODKEY'::text) AS "MODKEY",
    (get_latest_data_by_tablename.data ->> 'OPHTH2'::text) AS "OPHTH2",
    (get_latest_data_by_tablename.data ->> 'AN_HPHORIA'::text) AS "AN_HPHORIA",
    (get_latest_data_by_tablename.data ->> 'R_IOPA'::text) AS "R_IOPA",
    (get_latest_data_by_tablename.data ->> 'PIC2'::text) AS "PIC2",
    (get_latest_data_by_tablename.data ->> 'L_SPHERE4'::text) AS "L_SPHERE4",
    (get_latest_data_by_tablename.data ->> 'OPH4'::text) AS "OPH4",
    (get_latest_data_by_tablename.data ->> 'L_NVA3'::text) AS "L_NVA3",
    (get_latest_data_by_tablename.data ->> 'AN_PRC'::text) AS "AN_PRC",
    (get_latest_data_by_tablename.data ->> 'VISUAL_F'::text) AS "VISUAL_F",
    (get_latest_data_by_tablename.data ->> 'L_INT2'::text) AS "L_INT2",
    (get_latest_data_by_tablename.data ->> 'R_AXIS1'::text) AS "R_AXIS1",
    (get_latest_data_by_tablename.data ->> 'HEAD5'::text) AS "HEAD5",
    (get_latest_data_by_tablename.data ->> 'FIXDISP'::text) AS "FIXDISP",
    (get_latest_data_by_tablename.data ->> 'OPHTH1'::text) AS "OPHTH1",
    (get_latest_data_by_tablename.data ->> 'PUPIL1'::text) AS "PUPIL1",
    (get_latest_data_by_tablename.data ->> 'L_SPHERE2'::text) AS "L_SPHERE2",
    (get_latest_data_by_tablename.data ->> 'NOTE8'::text) AS "NOTE8",
    (get_latest_data_by_tablename.data ->> 'PUPIL2'::text) AS "PUPIL2",
    (get_latest_data_by_tablename.data ->> 'RXNOTE1'::text) AS "RXNOTE1",
    (get_latest_data_by_tablename.data ->> 'UD_VPHORIA'::text) AS "UD_VPHORIA",
    (get_latest_data_by_tablename.data ->> 'NOTE2'::text) AS "NOTE2",
    (get_latest_data_by_tablename.data ->> 'NOTE7'::text) AS "NOTE7",
    (get_latest_data_by_tablename.data ->> 'RXNOTE2'::text) AS "RXNOTE2",
    (get_latest_data_by_tablename.data ->> 'R_VA2'::text) AS "R_VA2",
    (get_latest_data_by_tablename.data ->> 'L_ADD5'::text) AS "L_ADD5",
    (get_latest_data_by_tablename.data ->> 'R_ADD1'::text) AS "R_ADD1",
    (get_latest_data_by_tablename.data ->> 'R_PRISM5'::text) AS "R_PRISM5",
    (get_latest_data_by_tablename.data ->> 'R_IOP'::text) AS "R_IOP",
    (get_latest_data_by_tablename.data ->> 'L_VPRISM1'::text) AS "L_VPRISM1",
    (get_latest_data_by_tablename.data ->> 'DELFLAG'::text) AS "DELFLAG",
    (get_latest_data_by_tablename.data ->> 'STEREOPSIS'::text) AS "STEREOPSIS",
    (get_latest_data_by_tablename.data ->> 'NPC'::text) AS "NPC",
    (get_latest_data_by_tablename.data ->> 'NOTE4'::text) AS "NOTE4",
    (get_latest_data_by_tablename.data ->> 'EOM'::text) AS "EOM",
    (get_latest_data_by_tablename.data ->> 'UN_PHOIRA'::text) AS "UN_PHOIRA",
    (get_latest_data_by_tablename.data ->> 'BIOM'::text) AS "BIOM",
    (get_latest_data_by_tablename.data ->> 'L_SPHERE6'::text) AS "L_SPHERE6",
    (get_latest_data_by_tablename.data ->> 'AD_VPHORIA'::text) AS "AD_VPHORIA",
    (get_latest_data_by_tablename.data ->> 'UD_HPHORIA'::text) AS "UD_HPHORIA",
    (get_latest_data_by_tablename.data ->> 'PNOTES'::text) AS "PNOTES",
    (get_latest_data_by_tablename.data ->> 'L_VA3'::text) AS "L_VA3",
    (get_latest_data_by_tablename.data ->> 'R_NVA5'::text) AS "R_NVA5",
    (get_latest_data_by_tablename.data ->> 'L_INT1'::text) AS "L_INT1",
    (get_latest_data_by_tablename.data ->> 'COVERD'::text) AS "COVERD",
    (get_latest_data_by_tablename.data ->> 'L_NVA6'::text) AS "L_NVA6",
    (get_latest_data_by_tablename.data ->> 'HEAD8'::text) AS "HEAD8",
    (get_latest_data_by_tablename.data ->> 'L_ADD1'::text) AS "L_ADD1",
    (get_latest_data_by_tablename.data ->> 'L_ADD2'::text) AS "L_ADD2",
    (get_latest_data_by_tablename.data ->> 'R_INT2'::text) AS "R_INT2",
    (get_latest_data_by_tablename.data ->> 'R_NVA1'::text) AS "R_NVA1",
    (get_latest_data_by_tablename.data ->> 'RECALLMTH'::text) AS "RECALLMTH",
    (get_latest_data_by_tablename.data ->> 'R_VPRISM1'::text) AS "R_VPRISM1",
    (get_latest_data_by_tablename.data ->> 'R_NVA6'::text) AS "R_NVA6",
    (get_latest_data_by_tablename.data ->> 'MODIFIED'::text) AS "MODIFIED",
    (get_latest_data_by_tablename.data ->> 'L_SPHERE5'::text) AS "L_SPHERE5",
    (get_latest_data_by_tablename.data ->> 'IOPMETHOD'::text) AS "IOPMETHOD",
    (get_latest_data_by_tablename.data ->> 'OPH3'::text) AS "OPH3",
    (get_latest_data_by_tablename.data ->> 'AD_PRC'::text) AS "AD_PRC",
    (get_latest_data_by_tablename.data ->> 'ANFXCYL'::text) AS "ANFXCYL",
    (get_latest_data_by_tablename.data ->> 'L_AXIS1'::text) AS "L_AXIS1",
    (get_latest_data_by_tablename.data ->> 'SYMP2'::text) AS "SYMP2",
    (get_latest_data_by_tablename.data ->> 'DOWNLOAD'::text) AS "DOWNLOAD",
    (get_latest_data_by_tablename.data ->> 'R_PRISM1'::text) AS "R_PRISM1",
    (get_latest_data_by_tablename.data ->> 'CONTYPE'::text) AS "CONTYPE",
    (get_latest_data_by_tablename.data ->> 'RCPERIOD'::text) AS "RCPERIOD",
    (get_latest_data_by_tablename.data ->> 'BILLNO'::text) AS "BILLNO",
    (get_latest_data_by_tablename.data ->> 'HIST1'::text) AS "HIST1",
    (get_latest_data_by_tablename.data ->> 'RDATE'::text) AS "RDATE",
    (get_latest_data_by_tablename.data ->> 'R_ADD5'::text) AS "R_ADD5",
    (get_latest_data_by_tablename.data ->> 'RXNOTE'::text) AS "RXNOTE",
    (get_latest_data_by_tablename.data ->> 'HEAD6'::text) AS "HEAD6",
    (get_latest_data_by_tablename.data ->> 'R_VA3'::text) AS "R_VA3",
    (get_latest_data_by_tablename.data ->> 'PATIENT'::text) AS "PATIENT",
    (get_latest_data_by_tablename.parameters ->> 'actio_source'::text) AS actio_source,
    (get_latest_data_by_tablename.parameters ->> 'view_name'::text) AS view_name,
    (get_latest_data_by_tablename.parameters ->> 'ext'::text) AS ext,
    (get_latest_data_by_tablename.parameters ->> 'actio_system'::text) AS actio_system,
    get_latest_data_by_tablename.runid AS actio_runid,
    get_latest_data_by_tablename.createddate AS actio_createddate
   FROM get_latest_data_by_tablename('sunix_VCONSPEC'::bpchar) get_latest_data_by_tablename(data, parameters, runid, createddate);


--
-- Name: today_sunix_vdoctor; Type: VIEW; Schema: stage; Owner: -
--

CREATE VIEW today_sunix_vdoctor AS
 SELECT (get_latest_data_by_tablename.data ->> 'NOTE'::text) AS "NOTE",
    (get_latest_data_by_tablename.data ->> 'PKEY'::text) AS "PKEY",
    (get_latest_data_by_tablename.data ->> 'XFER'::text) AS "XFER",
    (get_latest_data_by_tablename.data ->> 'LOGSTR'::text) AS "LOGSTR",
    (get_latest_data_by_tablename.data ->> 'QUALIF'::text) AS "QUALIF",
    (get_latest_data_by_tablename.data ->> 'HOLSIGN'::text) AS "HOLSIGN",
    (get_latest_data_by_tablename.data ->> 'TPRESNO'::text) AS "TPRESNO",
    (get_latest_data_by_tablename.data ->> 'ACN'::text) AS "ACN",
    (get_latest_data_by_tablename.data ->> 'CODE2'::text) AS "CODE2",
    (get_latest_data_by_tablename.data ->> 'ADDR0'::text) AS "ADDR0",
    (get_latest_data_by_tablename.data ->> 'PROVIDER'::text) AS "PROVIDER",
    (get_latest_data_by_tablename.data ->> 'GNAME'::text) AS "GNAME",
    (get_latest_data_by_tablename.data ->> 'EMAIL'::text) AS "EMAIL",
    (get_latest_data_by_tablename.data ->> 'TITLE'::text) AS "TITLE",
    (get_latest_data_by_tablename.data ->> 'ADDR1'::text) AS "ADDR1",
    (get_latest_data_by_tablename.data ->> 'COLOUR'::text) AS "COLOUR",
    (get_latest_data_by_tablename.data ->> 'APPOINT'::text) AS "APPOINT",
    (get_latest_data_by_tablename.data ->> 'CODE'::text) AS "CODE",
    (get_latest_data_by_tablename.data ->> 'MEDICARE'::text) AS "MEDICARE",
    (get_latest_data_by_tablename.data ->> 'POSTCODE'::text) AS "POSTCODE",
    (get_latest_data_by_tablename.data ->> 'PHONE'::text) AS "PHONE",
    (get_latest_data_by_tablename.data ->> 'LOCATION'::text) AS "LOCATION",
    (get_latest_data_by_tablename.data ->> 'SNAME'::text) AS "SNAME",
    (get_latest_data_by_tablename.data ->> 'PAYPROV'::text) AS "PAYPROV",
    (get_latest_data_by_tablename.data ->> 'APPEND'::text) AS "APPEND",
    (get_latest_data_by_tablename.data ->> 'PASSPHRASE'::text) AS "PASSPHRASE",
    (get_latest_data_by_tablename.data ->> 'GROUP'::text) AS "GROUP",
    (get_latest_data_by_tablename.data ->> 'MODKEY'::text) AS "MODKEY",
    (get_latest_data_by_tablename.data ->> 'APPSTART'::text) AS "APPSTART",
    (get_latest_data_by_tablename.data ->> 'PHONE2'::text) AS "PHONE2",
    (get_latest_data_by_tablename.data ->> 'FAX'::text) AS "FAX",
    (get_latest_data_by_tablename.data ->> 'PRACCODE'::text) AS "PRACCODE",
    (get_latest_data_by_tablename.data ->> 'EMPLOYEEID'::text) AS "EMPLOYEEID",
    (get_latest_data_by_tablename.data ->> 'DELFLAG'::text) AS "DELFLAG",
    (get_latest_data_by_tablename.data ->> 'COMPANY'::text) AS "COMPANY",
    (get_latest_data_by_tablename.data ->> 'ADDR2'::text) AS "ADDR2",
    (get_latest_data_by_tablename.parameters ->> 'actio_source'::text) AS actio_source,
    (get_latest_data_by_tablename.parameters ->> 'view_name'::text) AS view_name,
    (get_latest_data_by_tablename.parameters ->> 'ext'::text) AS ext,
    (get_latest_data_by_tablename.parameters ->> 'actio_system'::text) AS actio_system,
    get_latest_data_by_tablename.runid AS actio_runid,
    get_latest_data_by_tablename.createddate AS actio_createddate
   FROM get_latest_data_by_tablename('sunix_VDOCTOR'::bpchar) get_latest_data_by_tablename(data, parameters, runid, createddate);


--
-- Name: today_sunix_vpatient; Type: VIEW; Schema: stage; Owner: -
--

CREATE VIEW today_sunix_vpatient AS
 SELECT (get_latest_data_by_tablename.data ->> 'MEDICORD'::text) AS "MEDICORD",
    (get_latest_data_by_tablename.data ->> 'ALLERGY'::text) AS "ALLERGY",
    (get_latest_data_by_tablename.data ->> 'OPTOM'::text) AS "OPTOM",
    (get_latest_data_by_tablename.data ->> 'CREATED'::text) AS "CREATED",
    (get_latest_data_by_tablename.data ->> 'RECORDED'::text) AS "RECORDED",
    (get_latest_data_by_tablename.data ->> 'EMAILDATE'::text) AS "EMAILDATE",
    (get_latest_data_by_tablename.data ->> 'RECALLSENT'::text) AS "RECALLSENT",
    (get_latest_data_by_tablename.data ->> 'HFUND'::text) AS "HFUND",
    (get_latest_data_by_tablename.data ->> 'NOTE'::text) AS "NOTE",
    (get_latest_data_by_tablename.data ->> 'XFER'::text) AS "XFER",
    (get_latest_data_by_tablename.data ->> 'LOGSTR'::text) AS "LOGSTR",
    (get_latest_data_by_tablename.data ->> 'CLCNT'::text) AS "CLCNT",
    (get_latest_data_by_tablename.data ->> 'CONBAL'::text) AS "CONBAL",
    (get_latest_data_by_tablename.data ->> 'DISPBAL'::text) AS "DISPBAL",
    (get_latest_data_by_tablename.data ->> 'CMFNO'::text) AS "CMFNO",
    (get_latest_data_by_tablename.data ->> 'ADDR3'::text) AS "ADDR3",
    (get_latest_data_by_tablename.data ->> 'PRACNO'::text) AS "PRACNO",
    (get_latest_data_by_tablename.data ->> 'BIRTHDAY'::text) AS "BIRTHDAY",
    (get_latest_data_by_tablename.data ->> 'FIRSTVISIT'::text) AS "FIRSTVISIT",
    (get_latest_data_by_tablename.data ->> 'HPHONE'::text) AS "HPHONE",
    (get_latest_data_by_tablename.data ->> 'FLYBUYSBC'::text) AS "FLYBUYSBC",
    (get_latest_data_by_tablename.data ->> 'CMHPHONE'::text) AS "CMHPHONE",
    (get_latest_data_by_tablename.data ->> 'CMMOBILE'::text) AS "CMMOBILE",
    (get_latest_data_by_tablename.data ->> 'REFRESH'::text) AS "REFRESH",
    (get_latest_data_by_tablename.data ->> 'FCLREPLACE'::text) AS "FCLREPLACE",
    (get_latest_data_by_tablename.data ->> 'CMSMS'::text) AS "CMSMS",
    (get_latest_data_by_tablename.data ->> 'LASTLOC'::text) AS "LASTLOC",
    (get_latest_data_by_tablename.data ->> 'SPCNT'::text) AS "SPCNT",
    (get_latest_data_by_tablename.data ->> 'SURNAME'::text) AS "SURNAME",
    (get_latest_data_by_tablename.data ->> 'OUTSTDDATE'::text) AS "OUTSTDDATE",
    (get_latest_data_by_tablename.data ->> 'CMLETTER'::text) AS "CMLETTER",
    (get_latest_data_by_tablename.data ->> 'LOCATION1'::text) AS "LOCATION1",
    (get_latest_data_by_tablename.data ->> 'POSTNAME'::text) AS "POSTNAME",
    (get_latest_data_by_tablename.data ->> 'ADDRFLAG'::text) AS "ADDRFLAG",
    (get_latest_data_by_tablename.data ->> 'PHONE3'::text) AS "PHONE3",
    (get_latest_data_by_tablename.data ->> 'POSTADDR3'::text) AS "POSTADDR3",
    (get_latest_data_by_tablename.data ->> 'OTHER4'::text) AS "OTHER4",
    (get_latest_data_by_tablename.data ->> 'RECALL'::text) AS "RECALL",
    (get_latest_data_by_tablename.data ->> 'FHISTORY'::text) AS "FHISTORY",
    (get_latest_data_by_tablename.data ->> 'OTHER2'::text) AS "OTHER2",
    (get_latest_data_by_tablename.data ->> 'FFRAPPRO'::text) AS "FFRAPPRO",
    (get_latest_data_by_tablename.data ->> 'REFER1'::text) AS "REFER1",
    (get_latest_data_by_tablename.data ->> 'CMEMAIL'::text) AS "CMEMAIL",
    (get_latest_data_by_tablename.data ->> 'EMAIL'::text) AS "EMAIL",
    (get_latest_data_by_tablename.data ->> 'TITLE'::text) AS "TITLE",
    (get_latest_data_by_tablename.data ->> 'MEDICARE0'::text) AS "MEDICARE0",
    (get_latest_data_by_tablename.data ->> 'COUNTRY'::text) AS "COUNTRY",
    (get_latest_data_by_tablename.data ->> 'ADDR1'::text) AS "ADDR1",
    (get_latest_data_by_tablename.data ->> 'REFNUM'::text) AS "REFNUM",
    (get_latest_data_by_tablename.data ->> 'STATE'::text) AS "STATE",
    (get_latest_data_by_tablename.data ->> 'HOBBY'::text) AS "HOBBY",
    (get_latest_data_by_tablename.data ->> 'REFNUM0'::text) AS "REFNUM0",
    (get_latest_data_by_tablename.data ->> 'POSTPCODE'::text) AS "POSTPCODE",
    (get_latest_data_by_tablename.data ->> 'MEDICARE'::text) AS "MEDICARE",
    (get_latest_data_by_tablename.data ->> 'DOCTOR'::text) AS "DOCTOR",
    (get_latest_data_by_tablename.data ->> 'OTHER3'::text) AS "OTHER3",
    (get_latest_data_by_tablename.data ->> 'OPHTH'::text) AS "OPHTH",
    (get_latest_data_by_tablename.data ->> 'POSTCODE'::text) AS "POSTCODE",
    (get_latest_data_by_tablename.data ->> 'BALANCE'::text) AS "BALANCE",
    (get_latest_data_by_tablename.data ->> 'NUMTYPE'::text) AS "NUMTYPE",
    (get_latest_data_by_tablename.data ->> 'POSTSTATE'::text) AS "POSTSTATE",
    (get_latest_data_by_tablename.data ->> 'CMWPHONE'::text) AS "CMWPHONE",
    (get_latest_data_by_tablename.data ->> 'FLYBUYS'::text) AS "FLYBUYS",
    (get_latest_data_by_tablename.data ->> 'CRTWS'::text) AS "CRTWS",
    (get_latest_data_by_tablename.data ->> 'LOCATION'::text) AS "LOCATION",
    (get_latest_data_by_tablename.data ->> 'FUNDSTR'::text) AS "FUNDSTR",
    (get_latest_data_by_tablename.data ->> 'POSTADDR1'::text) AS "POSTADDR1",
    (get_latest_data_by_tablename.data ->> 'PATLOC'::text) AS "PATLOC",
    (get_latest_data_by_tablename.data ->> 'CMNO'::text) AS "CMNO",
    (get_latest_data_by_tablename.data ->> 'KNOWNAS'::text) AS "KNOWNAS",
    (get_latest_data_by_tablename.data ->> 'FTODO'::text) AS "FTODO",
    (get_latest_data_by_tablename.data ->> 'NOTE1'::text) AS "NOTE1",
    (get_latest_data_by_tablename.data ->> 'MOBILE'::text) AS "MOBILE",
    (get_latest_data_by_tablename.data ->> 'CSCEXPIRY'::text) AS "CSCEXPIRY",
    (get_latest_data_by_tablename.data ->> 'FRELATION'::text) AS "FRELATION",
    (get_latest_data_by_tablename.data ->> 'OCCUPT'::text) AS "OCCUPT",
    (get_latest_data_by_tablename.data ->> 'MODKEY'::text) AS "MODKEY",
    (get_latest_data_by_tablename.data ->> 'FOUTSTAND'::text) AS "FOUTSTAND",
    (get_latest_data_by_tablename.data ->> 'LASTRECALL'::text) AS "LASTRECALL",
    (get_latest_data_by_tablename.data ->> 'CMNOEMAIL'::text) AS "CMNOEMAIL",
    (get_latest_data_by_tablename.data ->> 'LOCENTRY'::text) AS "LOCENTRY",
    (get_latest_data_by_tablename.data ->> 'FAX'::text) AS "FAX",
    (get_latest_data_by_tablename.data ->> 'OUTSTLET'::text) AS "OUTSTLET",
    (get_latest_data_by_tablename.data ->> 'FCODE'::text) AS "FCODE",
    (get_latest_data_by_tablename.data ->> 'ACCCODE'::text) AS "ACCCODE",
    (get_latest_data_by_tablename.data ->> 'CSC'::text) AS "CSC",
    (get_latest_data_by_tablename.data ->> 'LASTVISIT'::text) AS "LASTVISIT",
    (get_latest_data_by_tablename.data ->> 'OUTSTAND'::text) AS "OUTSTAND",
    (get_latest_data_by_tablename.data ->> 'LASTRCLET'::text) AS "LASTRCLET",
    (get_latest_data_by_tablename.data ->> 'NEXTRCLET'::text) AS "NEXTRCLET",
    (get_latest_data_by_tablename.data ->> 'CAHCARDEXP'::text) AS "CAHCARDEXP",
    (get_latest_data_by_tablename.data ->> 'IMPFROM'::text) AS "IMPFROM",
    (get_latest_data_by_tablename.data ->> 'DELFLAG'::text) AS "DELFLAG",
    (get_latest_data_by_tablename.data ->> 'GIVEN_NAME'::text) AS "GIVEN_NAME",
    (get_latest_data_by_tablename.data ->> 'REFER2'::text) AS "REFER2",
    (get_latest_data_by_tablename.data ->> 'REFDATE'::text) AS "REFDATE",
    (get_latest_data_by_tablename.data ->> 'REFNUMOLD'::text) AS "REFNUMOLD",
    (get_latest_data_by_tablename.data ->> 'FEXTREF'::text) AS "FEXTREF",
    (get_latest_data_by_tablename.data ->> 'FORNAME'::text) AS "FORNAME",
    (get_latest_data_by_tablename.data ->> 'NEXTRECALL'::text) AS "NEXTRECALL",
    (get_latest_data_by_tablename.data ->> 'MODIFIED'::text) AS "MODIFIED",
    (get_latest_data_by_tablename.data ->> 'FPOSTADDR'::text) AS "FPOSTADDR",
    (get_latest_data_by_tablename.data ->> 'MODWS'::text) AS "MODWS",
    (get_latest_data_by_tablename.data ->> 'WPHONE'::text) AS "WPHONE",
    (get_latest_data_by_tablename.data ->> 'SEX'::text) AS "SEX",
    (get_latest_data_by_tablename.data ->> 'RCPERIOD'::text) AS "RCPERIOD",
    (get_latest_data_by_tablename.data ->> 'POSTADDR2'::text) AS "POSTADDR2",
    (get_latest_data_by_tablename.data ->> 'MEDICEXP'::text) AS "MEDICEXP",
    (get_latest_data_by_tablename.data ->> 'NHI'::text) AS "NHI",
    (get_latest_data_by_tablename.data ->> 'OTHER1'::text) AS "OTHER1",
    (get_latest_data_by_tablename.data ->> 'ADDR2'::text) AS "ADDR2",
    (get_latest_data_by_tablename.parameters ->> 'actio_source'::text) AS actio_source,
    (get_latest_data_by_tablename.parameters ->> 'view_name'::text) AS view_name,
    (get_latest_data_by_tablename.parameters ->> 'ext'::text) AS ext,
    (get_latest_data_by_tablename.parameters ->> 'actio_system'::text) AS actio_system,
    get_latest_data_by_tablename.runid AS actio_runid,
    get_latest_data_by_tablename.createddate AS actio_createddate
   FROM get_latest_data_by_tablename('sunix_VPATIENT'::bpchar) get_latest_data_by_tablename(data, parameters, runid, createddate);


SET search_path = public, pg_catalog;

--
-- Name: appointment sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY appointment ALTER COLUMN sk SET DEFAULT nextval('appointment_sk_seq'::regclass);


--
-- Name: appointment_type sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY appointment_type ALTER COLUMN sk SET DEFAULT nextval('appointment_type_sk_seq'::regclass);


--
-- Name: consult sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY consult ALTER COLUMN sk SET DEFAULT nextval('consult_sk_seq'::regclass);


--
-- Name: consult_type sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY consult_type ALTER COLUMN sk SET DEFAULT nextval('consult_type_sk_seq'::regclass);


--
-- Name: consultitem sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY consultitem ALTER COLUMN sk SET DEFAULT nextval('consultitem_sk_seq'::regclass);


--
-- Name: customer sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY customer ALTER COLUMN sk SET DEFAULT nextval('customer_sk_seq'::regclass);


--
-- Name: customer_ext sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY customer_ext ALTER COLUMN sk SET DEFAULT nextval('customer_ext_sk_seq'::regclass);


--
-- Name: discountcode sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY discountcode ALTER COLUMN sk SET DEFAULT nextval('discountcode_sk_seq'::regclass);


--
-- Name: employee sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY employee ALTER COLUMN sk SET DEFAULT nextval('employee_sk_seq'::regclass);


--
-- Name: employee_providerno sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY employee_providerno ALTER COLUMN sk SET DEFAULT nextval('employee_providerno_sk_seq'::regclass);


--
-- Name: expense sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY expense ALTER COLUMN sk SET DEFAULT nextval('expense_sk_seq'::regclass);


--
-- Name: expense_type sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY expense_type ALTER COLUMN sk SET DEFAULT nextval('expense_type_sk_seq'::regclass);


--
-- Name: invoice sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY invoice ALTER COLUMN sk SET DEFAULT nextval('invoice_sk_seq'::regclass);


--
-- Name: invoice_orders sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY invoice_orders ALTER COLUMN sk SET DEFAULT nextval('invoice_orders_sk_seq'::regclass);


--
-- Name: job_process_status sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY job_process_status ALTER COLUMN sk SET DEFAULT nextval('job_process_status_sk_seq'::regclass);


--
-- Name: order_history sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY order_history ALTER COLUMN sk SET DEFAULT nextval('order_history_sk_seq'::regclass);


--
-- Name: order_job_history sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY order_job_history ALTER COLUMN sk SET DEFAULT nextval('order_job_history_sk_seq'::regclass);


--
-- Name: payee sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY payee ALTER COLUMN sk SET DEFAULT nextval('payee_sk_seq'::regclass);


--
-- Name: payment sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY payment ALTER COLUMN sk SET DEFAULT nextval('payment_sk_seq'::regclass);


--
-- Name: payment_type sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY payment_type ALTER COLUMN sk SET DEFAULT nextval('payment_type_sk_seq'::regclass);


--
-- Name: product_price_history id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY product_price_history ALTER COLUMN id SET DEFAULT nextval('product_price_history_id_seq'::regclass);


--
-- Name: spectaclejob sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY spectaclejob ALTER COLUMN sk SET DEFAULT nextval('spectaclejob_sk_seq'::regclass);


--
-- Name: status sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY status ALTER COLUMN sk SET DEFAULT nextval('status_sk_seq'::regclass);


--
-- Name: stock_price_history sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY stock_price_history ALTER COLUMN sk SET DEFAULT nextval('stock_price_history_sk_seq'::regclass);


--
-- Name: store sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY store ALTER COLUMN sk SET DEFAULT nextval('store_sk_seq'::regclass);


--
-- Name: store_stock_history sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stock_history ALTER COLUMN sk SET DEFAULT nextval('store_stock_history_sk_seq'::regclass);


--
-- Name: store_stockmovement sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stockmovement ALTER COLUMN sk SET DEFAULT nextval('store_stockmovement_sk_seq'::regclass);


--
-- Name: storeproduct sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY storeproduct ALTER COLUMN sk SET DEFAULT nextval('storeproduct_sk_seq'::regclass);


--
-- Name: takeon_product sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY takeon_product ALTER COLUMN sk SET DEFAULT nextval('takeon_product_sk_seq'::regclass);


--
-- Name: appointment appointment_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY appointment
    ADD CONSTRAINT appointment_pkey PRIMARY KEY (sk);


--
-- Name: appointment_type appointment_type_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY appointment_type
    ADD CONSTRAINT appointment_type_pkey PRIMARY KEY (sk);


--
-- Name: consult_type constult_type_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY consult_type
    ADD CONSTRAINT constult_type_pkey PRIMARY KEY (sk);


--
-- Name: consult consult_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY consult
    ADD CONSTRAINT consult_pkey PRIMARY KEY (sk);


--
-- Name: consultitem consultitem_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY consultitem
    ADD CONSTRAINT consultitem_pkey PRIMARY KEY (sk);


--
-- Name: customer_ext customer_ext_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY customer_ext
    ADD CONSTRAINT customer_ext_pkey PRIMARY KEY (sk);


--
-- Name: customer customer_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY customer
    ADD CONSTRAINT customer_pkey PRIMARY KEY (sk);


--
-- Name: expense expense_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY expense
    ADD CONSTRAINT expense_pkey PRIMARY KEY (sk);


--
-- Name: expense_type expense_type_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY expense_type
    ADD CONSTRAINT expense_type_pkey PRIMARY KEY (sk);


--
-- Name: supplier externalref_ukey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY supplier
    ADD CONSTRAINT externalref_ukey UNIQUE (externalref);


--
-- Name: invoice_orders invoice_orders_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY invoice_orders
    ADD CONSTRAINT invoice_orders_pkey PRIMARY KEY (sk);


--
-- Name: invoice invoice_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY invoice
    ADD CONSTRAINT invoice_pkey PRIMARY KEY (sk);


--
-- Name: invoiceitem invoiceitem_skey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY invoiceitem
    ADD CONSTRAINT invoiceitem_skey PRIMARY KEY (invoiceitemid);


--
-- Name: order_history order_history_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY order_history
    ADD CONSTRAINT order_history_pkey PRIMARY KEY (sk);


--
-- Name: order_job_history order_job_history_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY order_job_history
    ADD CONSTRAINT order_job_history_pkey PRIMARY KEY (sk);


--
-- Name: payee payee_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY payee
    ADD CONSTRAINT payee_pkey PRIMARY KEY (sk);


--
-- Name: payment payment_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY payment
    ADD CONSTRAINT payment_pkey PRIMARY KEY (sk);


--
-- Name: payment_type payment_type_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY payment_type
    ADD CONSTRAINT payment_type_pkey PRIMARY KEY (sk);


--
-- Name: paymentitem paymentitem_skey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY paymentitem
    ADD CONSTRAINT paymentitem_skey PRIMARY KEY (paymentitemid);


--
-- Name: employee pk_employee; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY employee
    ADD CONSTRAINT pk_employee PRIMARY KEY (sk);


--
-- Name: schema_version schema_version_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY schema_version
    ADD CONSTRAINT schema_version_pk PRIMARY KEY (installed_rank);


--
-- Name: spectaclejob spectaclejob_skey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY spectaclejob
    ADD CONSTRAINT spectaclejob_skey PRIMARY KEY (spectaclejobid);


--
-- Name: status status_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY status
    ADD CONSTRAINT status_pkey PRIMARY KEY (sk);


--
-- Name: stock_price_history stock_price_history_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY stock_price_history
    ADD CONSTRAINT stock_price_history_pkey PRIMARY KEY (sk);


--
-- Name: store store_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store
    ADD CONSTRAINT store_pkey PRIMARY KEY (sk);


--
-- Name: store_stock_history store_stock_history_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stock_history
    ADD CONSTRAINT store_stock_history_pkey PRIMARY KEY (sk);


--
-- Name: store_stockmovement store_stockmovement_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stockmovement
    ADD CONSTRAINT store_stockmovement_pkey PRIMARY KEY (sk);


--
-- Name: storeproduct storeproduct_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY storeproduct
    ADD CONSTRAINT storeproduct_pkey PRIMARY KEY (sk);


--
-- Name: supplier supplier_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY supplier
    ADD CONSTRAINT supplier_pkey PRIMARY KEY (sk);


--
-- Name: supplierproduct supplierproduct_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY supplierproduct
    ADD CONSTRAINT supplierproduct_pkey PRIMARY KEY (sk);


--
-- Name: takeon_product takeon_product_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY takeon_product
    ADD CONSTRAINT takeon_product_pkey PRIMARY KEY (sk);


--
-- Name: appointment_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX appointment_nkey ON appointment USING btree (appointmentid);


--
-- Name: appointment_storesk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX appointment_storesk ON appointment USING hash (storesk);


--
-- Name: appointment_type_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX appointment_type_nkey ON appointment_type USING btree (appointment_typeid);


--
-- Name: consult_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX consult_nkey ON consult USING btree (consultid);


--
-- Name: consult_type_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX consult_type_nkey ON consult_type USING btree (consult_typeid);


--
-- Name: customer_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX customer_nkey ON customer USING btree (customerid);


--
-- Name: employee_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX employee_nkey ON employee USING btree (employeeid);


--
-- Name: expense_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX expense_nkey ON expense USING btree (expenseid);


--
-- Name: expense_type_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX expense_type_nkey ON expense_type USING btree (expense_typeid);


--
-- Name: fki_supplierproduct_supplier_fkey; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX fki_supplierproduct_supplier_fkey ON supplierproduct USING btree (suppliersk);


--
-- Name: idx_model; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_model ON supplierproduct USING btree (frame_model);


--
-- Name: idx_store_nk; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX idx_store_nk ON store USING btree (store_nk);


--
-- Name: idx_suppliersku; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_suppliersku ON supplierproduct USING btree (suppliersku);


--
-- Name: invoice_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX invoice_nkey ON invoice USING btree (invoiceid);


--
-- Name: invoice_storesk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX invoice_storesk ON invoice USING hash (storesk);


--
-- Name: invoiceitem_storesk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX invoiceitem_storesk ON invoiceitem USING hash (storesk);


--
-- Name: payee_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX payee_nkey ON payee USING btree (payeeid);


--
-- Name: payment_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX payment_nkey ON payment USING btree (paymentid);


--
-- Name: payment_storeid_ix; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX payment_storeid_ix ON payment USING btree (storeid);


--
-- Name: payment_type_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX payment_type_nkey ON payment_type USING btree (payment_typeid);


--
-- Name: paymentitem_storesk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX paymentitem_storesk ON paymentitem USING hash (storesk);


--
-- Name: schema_version_s_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX schema_version_s_idx ON schema_version USING btree (success);


--
-- Name: status_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX status_nkey ON status USING btree (statusid);


--
-- Name: store_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX store_nkey ON store USING btree (storeid);


--
-- Name: storeproduct_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX storeproduct_nkey ON storeproduct USING btree (productid);


--
-- Name: invoice_orders invoice_orders_invoice_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY invoice_orders
    ADD CONSTRAINT invoice_orders_invoice_sk_fkey FOREIGN KEY (invoice_sk) REFERENCES invoice(sk);


--
-- Name: invoice_orders invoice_orders_order_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY invoice_orders
    ADD CONSTRAINT invoice_orders_order_sk_fkey FOREIGN KEY (order_sk) REFERENCES order_history(sk);


--
-- Name: invoice_orders invoice_orders_store_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY invoice_orders
    ADD CONSTRAINT invoice_orders_store_sk_fkey FOREIGN KEY (store_sk) REFERENCES store(sk);


--
-- Name: order_history order_history_customer_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY order_history
    ADD CONSTRAINT order_history_customer_sk_fkey FOREIGN KEY (customer_sk) REFERENCES customer(sk);


--
-- Name: order_history order_history_store_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY order_history
    ADD CONSTRAINT order_history_store_sk_fkey FOREIGN KEY (store_sk) REFERENCES store(sk);


--
-- Name: stock_price_history stock_price_history_store_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY stock_price_history
    ADD CONSTRAINT stock_price_history_store_sk_fkey FOREIGN KEY (store_sk) REFERENCES store(sk);


--
-- Name: stock_price_history stock_price_history_storeproduct_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY stock_price_history
    ADD CONSTRAINT stock_price_history_storeproduct_sk_fkey FOREIGN KEY (storeproduct_sk) REFERENCES storeproduct(sk);


--
-- Name: stock_price_history stock_price_history_takeon_storeproduct_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY stock_price_history
    ADD CONSTRAINT stock_price_history_takeon_storeproduct_sk_fkey FOREIGN KEY (takeon_storeproduct_sk) REFERENCES storeproduct(sk);


--
-- Name: store_stock_history store_stock_history_employee_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stock_history
    ADD CONSTRAINT store_stock_history_employee_sk_fkey FOREIGN KEY (employee_sk) REFERENCES employee(sk);


--
-- Name: store_stock_history store_stock_history_store_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stock_history
    ADD CONSTRAINT store_stock_history_store_sk_fkey FOREIGN KEY (store_sk) REFERENCES store(sk);


--
-- Name: store_stock_history store_stock_history_storeproduct_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stock_history
    ADD CONSTRAINT store_stock_history_storeproduct_sk_fkey FOREIGN KEY (storeproduct_sk) REFERENCES storeproduct(sk);


--
-- Name: store_stock_history store_stock_history_takeon_storeproduct_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stock_history
    ADD CONSTRAINT store_stock_history_takeon_storeproduct_sk_fkey FOREIGN KEY (takeon_storeproduct_sk) REFERENCES storeproduct(sk);


--
-- Name: store_stockmovement store_stockmovement_customer_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stockmovement
    ADD CONSTRAINT store_stockmovement_customer_sk_fkey FOREIGN KEY (customer_sk) REFERENCES customer(sk);


--
-- Name: store_stockmovement store_stockmovement_employee_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stockmovement
    ADD CONSTRAINT store_stockmovement_employee_sk_fkey FOREIGN KEY (employee_sk) REFERENCES employee(sk);


--
-- Name: store_stockmovement store_stockmovement_invoice_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stockmovement
    ADD CONSTRAINT store_stockmovement_invoice_sk_fkey FOREIGN KEY (invoice_sk) REFERENCES invoice(sk);


--
-- Name: store_stockmovement store_stockmovement_stock_price_history_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stockmovement
    ADD CONSTRAINT store_stockmovement_stock_price_history_sk_fkey FOREIGN KEY (stock_price_history_sk) REFERENCES stock_price_history(sk);


--
-- Name: store_stockmovement store_stockmovement_store_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stockmovement
    ADD CONSTRAINT store_stockmovement_store_sk_fkey FOREIGN KEY (store_sk) REFERENCES store(sk);


--
-- Name: store_stockmovement store_stockmovement_storeproduct_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stockmovement
    ADD CONSTRAINT store_stockmovement_storeproduct_sk_fkey FOREIGN KEY (storeproduct_sk) REFERENCES storeproduct(sk);


--
-- Name: store_stockmovement store_stockmovement_takeon_storeproduct_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stockmovement
    ADD CONSTRAINT store_stockmovement_takeon_storeproduct_sk_fkey FOREIGN KEY (takeon_storeproduct_sk) REFERENCES storeproduct(sk);


--
-- Name: supplierproduct supplierproduct_supplier_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY supplierproduct
    ADD CONSTRAINT supplierproduct_supplier_fkey FOREIGN KEY (suppliersk) REFERENCES supplier(sk);


--
-- Name: takeon_product takeon_product_store_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY takeon_product
    ADD CONSTRAINT takeon_product_store_sk_fkey FOREIGN KEY (store_sk) REFERENCES store(sk);


--
-- Name: takeon_product takeon_product_supplier_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY takeon_product
    ADD CONSTRAINT takeon_product_supplier_sk_fkey FOREIGN KEY (supplier_sk) REFERENCES supplier(sk);


--
-- PostgreSQL database dump complete
--

