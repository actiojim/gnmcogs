
--- rollup of adjustmenttype
select s.storeid,at.name,x.adjustmenttype,total from public.store s inner join 
(select store_sk,adjustment_type_sk,adjustmenttype,count(*) as total from store_stockmovement sm 
group by sm.store_sk,adjustment_type_sk,adjustmenttype) x 
on x.store_sk = s.sk
inner join adjustment_type at on at.sk = x.adjustment_type_sk
order by storeid,at.name,x.adjustmenttype

