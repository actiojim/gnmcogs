
INSERT INTO public.supplier(
            sk, externalref, supplierid, name)
    VALUES (0, 0, '', '');

INSERT INTO public.takeon_product(
            sk, store_sk, supplier_sk, source_id, supplier_sku, name, description, 
            model, brand,created)
    VALUES (-1, 0, 0, '', '', '', '', 
            '', '', now());

INSERT INTO public.takeon_product(
        sk, store_sk, supplier_sk, source_id, supplier_sku, name, description, 
        model, brand,created)
VALUES (0, 0, 0, '', '', '', '', 
        '', '', now());

INSERT INTO public.supplierproduct(
        sk, suppliersku, suppliersk, frame_model, frame_colour, internal_sku, 
        frame_brand)
VALUES (0, '', 0, '', '', '', 
        '');

INSERT INTO public.storeproduct(
    sk, storeid, productid, type, datecreated, name, description, 
    retailprice, wholesaleprice, modified, last_purchase, last_sale, 
    quantity, frame_model, frame_colour, frame_brand, frame_temple, 
    frame_bridge, frame_eye_size, frame_depth, frame_material, lens_type, 
    lens_refractive_index, lens_stock_grind, lens_size, lens_segment_size, 
    internal_sku, distributor)
VALUES (0, '', '', '', now(), '', '', 
    0, 0, now(), now(), now(), 
    0, '', '', '', 0, 
    0, 0, 0, '', '', 
    0, '', 0, 0, 
    '', '');
    
INSERT INTO public.storeproduct(
    sk, storeid, productid, type, datecreated, name, description, 
    retailprice, wholesaleprice, modified, last_purchase, last_sale, 
    quantity, frame_model, frame_colour, frame_brand, frame_temple, 
    frame_bridge, frame_eye_size, frame_depth, frame_material, lens_type, 
    lens_refractive_index, lens_stock_grind, lens_size, lens_segment_size, 
    internal_sku, distributor)
VALUES (-1, 'ZZZ', 'ZZZ', '', now(), '', '', 
    0, 0, now(), now(), now(), 
    0, '', '', '', 0, 
    0, 0, 0, '', '', 
    0, '', 0, 0, 
    '', '');
    
INSERT INTO public.stock_price_history(
            sk, store_sk, 
            source_id, 
           takeon_storeproduct_sk, 
 --           storeproduct_sk, 
            supplier_cost, supplier_cost_gst, actual_cost, 
            actual_cost_gst, supplier_rrp, supplier_rrp_gst, calculated_retail_price, 
            calculated_retail_price_gst, override_retail_price, override_retail_price_gst, 
            retail_price, retail_price_gst, created)
    VALUES (0, 0, '', 0, 
 --           0, 
            null, null, null, 
            null, null, null, null, 
            null, null, null, 
            null, null, now());
            
INSERT INTO public.invoice(
            sk, invoiceid, createddate, storesk, employeesk, customersk, 
            amount_incl_gst, modified, amount_gst, reversalref, rebateref)
    VALUES (0, '', now(), 0, 0, 0, 
            null, now(), null, '', '');
    
            
INSERT INTO public.customer(
        sk, createddate, customerid, firstname, lastname, middlename, 
        title, gender, birthdate, preferredname, preferredcontactmethod, 
        email, emailcreated, phonecountryid, phonecreated, address1, 
        address2, address3, state, suburb, postcode, firstvisit, lastvisit, 
        lastconsult, lastrecall, nextrecall, lastrecalldesc, nextrecalldesc, 
        lastoptomseen, storesk, healthfund, modified, optout_recall, 
        optout_marketing, deceased, homephone, mobile, workphone)
VALUES (0, now(), '', '', '', '', 
        '', '', now(), '', '', 
        '', now(), 0, now(), '', 
        '', '', '', '', 0, now(), now(), 
        now(), now(), now(), '', '', 
        0, 0, '', now(), false, 
        false, false, '', '', '');

INSERT INTO public.employee(
            sk, createddate, employeeid, employeetype, employmentstartdate, 
            employmentenddate, firstname, lastname, middlename, title, gender, 
            birthdate, preferredname, preferredcontactmethod, email, emailcreated, 
            phonecountryid, phonenumber, phonerawnumber, phonemobile, phonesmsenabled, 
            phonefax, phonelandline, phonecreated, storesk)
    VALUES (0, now(), '', '', now(), 
            now(), '', '', '', '', '', 
            now(), '', '', '', now(), 
            0, '', '', false, false, 
            false, false, now(), 0);
            
--------------------------------------------------------------------------------------------------------
--------

select * from employee;
select * from customer order by sk;
select * from stock_price_history order by sk;
select * from storeproduct order by sk;
select * from takeon_product order by sk;
select * from supplier order by sk;

select count(*) from store_stockmovement 





        