
SET search_path = gnm_scire;

drop table  if exists store_stockmovement CASCADE;
drop table  if exists store_stock_history CASCADE;
drop table  if exists invoice_orders CASCADE;
drop table  if exists stock_price_history CASCADE;
drop table  if exists takeon_product CASCADE;
drop table  if exists order_job_history CASCADE;
drop table  if exists order_history CASCADE;

CREATE TABLE takeon_product (

    sk serial NOT NULL,
    store_sk int NOT NULL REFERENCES store(sk),
    supplier_sk int NOT NULL REFERENCES supplier(sk),
    
    -- key used by source system for this record
    source_id character varying(100) NOT NULL,
    supplier_sku character varying(100) NOT NULL,
    name character varying(500) NOT NULL,
    description character varying(500) NOT NULL,
    model character varying(500) NOT NULL,
    brand character varying(500) NOT NULL,
    created timestamp without time zone NOT NULL,

    primary key (sk)
);

CREATE TABLE stock_price_history (
    sk serial NOT NULL,
    store_sk int NOT NULL REFERENCES store(sk),
    source_id character varying(100) NOT NULL,
    
    -- there can be different products that are being tracked in the stock history
    takeon_product_sk int NOT NULL REFERENCES takeon_product(sk),
    supplierproduct_sk int NOT NULL REFERENCES supplierproduct(sk),
    storeproduct_sk int NOT NULL REFERENCES storeproduct(sk),

    supplier_cost numeric(19, 2),
    supplier_cost_gst numeric(19, 2),
    actual_cost numeric(19, 2),
    actual_cost_gst numeric(19, 2),
    supplier_rrp numeric(19, 2),
    supplier_rrp_gst numeric(19, 2),
    calculated_retail_price numeric(19, 2),
    calculated_retail_price_gst numeric(19, 2),
    override_retail_price numeric(19, 2),
    override_retail_price_gst numeric(19, 2),
    retail_price numeric(19, 2),
    retail_price_gst numeric(19, 2),
    
    created timestamp without time zone NOT NULL,
    primary key (sk)
);

----------------------------------------------------------------------------------------
--- STORAGE

CREATE TABLE store_stock_history
(
	sk serial NOT NULL,
    store_sk int NOT NULL REFERENCES store(sk),
    source_id character varying(100) NOT NULL,
    
    takeon_product_sk int NOT NULL REFERENCES takeon_product(sk),
    supplierproduct_sk int NOT NULL REFERENCES supplierproduct(sk),
    storeproduct_sk int NOT NULL REFERENCES storeproduct(sk),

    -- the new calculated total for stock
    total int NOT NULL,
    -- the previous calculated total for stock
    prior_total int NOT NULL,
    -- calculation reason
    reason character varying(20) NOT NULL,
    -- comments, free form
    comments character varying(500),
	-- employee ref
    employee_sk int references employee(sk),
    -- date of stock calculation
    created timestamp without time zone,

    primary key (sk)
);


CREATE TABLE store_stockmovement
(
	sk serial NOT NULL,
    store_sk int NOT NULL REFERENCES store(sk),
   	source_id character varying(100) NOT NULL,

    -- there can be different products that are being tracked in the stock history
    takeon_product_sk int NOT NULL REFERENCES takeon_product(sk),
    supplierproduct_sk int NOT NULL REFERENCES supplierproduct(sk),
    storeproduct_sk int NOT NULL REFERENCES storeproduct(sk),

    -- date of stock adjustment
    adjustmentDate timestamp without time zone,
    -- price record at time of purchase for product
    stock_price_history_sk int not null references stock_price_history(sk),
    -- adjustment amount (positive/negative)
    adjustment int not null,
    -- adjustment type (incoming/outgoing)
    adjustmentType character varying(20) NOT NULL,
    -- adjustment reason (warranty/purchase/pickup/order/floor stock purchase)
    reason character varying(20) NOT NULL,
	-- invoice ref
   	invoice_sk int references invoice(sk),
    -- customer ref
    customer_sk int references customer(sk),

    ---------------------------------------------------------------------------
    -- shipping number
    shipping_ref character varying(100),
    -- shipping operator ref
    -- shipping_operator_sk int references shipping_operator(sk),
    shipping_operator character varying(100),
    -- comments, free form
    comments character varying(500),
	-- employee ref
    employee_sk int references employee(sk),
    -- created date
    created timestamp without time zone NOT NULL,

    primary key (sk)
);

--
-- history record is immutable a new record is created for each data change
--
create table order_history
(
    sk serial NOT NULL,
    store_sk int NOT NULL REFERENCES store(sk),
    
    -- natural source key
    source_id character varying(100),
    
    order_type character varying(100) NOT NULL,
    queue character varying(100),
    status character varying(100),
    
    -- created date
    created timestamp without time zone NOT NULL,
    primary key (sk)
);

-- 
-- history record is immutable a new record is created for each data change
--
create table order_job_history
(
    sk serial NOT NULL,
    store_sk int NOT NULL REFERENCES store(sk),
    order_sk int NOT NULL REFERENCES order_history(sk),
    -- natural source key
    source_id character varying(100),
    
    -- job type frame or lens
    job_type character varying(100) NOT NULL,
    queue character varying(100),
    status character varying(100),    
    -- created date
    created timestamp without time zone NOT NULL,
    primary key (sk)
);

-- Bind Table between invoice and orders
create table invoice_orders
(
    sk serial NOT NULL,
    store_sk int NOT NULL REFERENCES store(sk),
    
    invoice_sk int NOT NULL REFERENCES invoice(sk),
    order_sk int NOT NULL REFERENCES orders(sk),

    -- created date
    created timestamp without time zone NOT NULL,
    primary key (sk)
)




