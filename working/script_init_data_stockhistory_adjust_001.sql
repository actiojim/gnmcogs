
-- select * from public.store_stock_history limit 100;

-- view 1 

-- select * from invoiceitem
-- select * from store_stock_history

-- alter table store_stock_history add column product_gnm_sk varchar(60);

alter table public.store add column nk varying(20) ;
alter table public.store add column store_nk varying(20) ;


-- begin;
-- update public.store set nk = store_nk

-- rollback;


-- ALTER TABLE ONLY store_stockmovement
--    ADD CONSTRAINT store_stockmovement_product_gnm_sk_fkey FOREIGN KEY (product_gnm_sk) REFERENCES product_gnm(sk);

    --- View for stock level since last update
    
    
--    store_stock_history -> storeproduct_takeon
--    store_stock_history -> product_gnm
    
--SELECT sk, store_sk, nk, takeon_storeproduct_sk, product_gnm_sk, total, prior_total, employee_sk, source_staged, created
--FROM public.store_stock_history;




create view view_store_stock_history
select * from
(
select ssh.sk, 
	store_sk, 
	nk, 
	takeon_storeproduct_sk, 
	ssh.product_gnm_sk, 
	total, prior_total, 
	employee_sk, 
	source_staged, 
	ssh.created
from  public.store_stock_history ssh 
	inner join  ( select nk as last_nk, store_sk as last_store_sk, takeon_storeproduct_sk as last_takeon_storeproduct_sk,max(source_staged) as last_source_staged 
   	from store_stock_history ssh group by nk,store_sk,takeon_storeproduct_sk ) ls 
   		on ls.last_store_sk = ssh.store_sk and ls.last_takeon_storeproduct_sk = ssh.takeon_storeproduct_sk and ssh.source_staged = ls.last_source_staged
   	left join storeproduct sp on sp.sk = ssh.takeon_storeproduct_sk
union all
select ssh.sk, 
	store_sk, 
	nk, 
	takeon_storeproduct_sk, 
	ssh.product_gnm_sk, 
	total, 
	prior_total, 
	employee_sk, 
	source_staged, 
	ssh.created
from  public.store_stock_history ssh 
	inner join ( select nk as last_nk, store_sk as last_store_sk, product_gnm_sk as last_product_gnm_sk,max(source_staged) as last_source_staged 
   	from store_stock_history ssh group by nk,store_sk,product_gnm_sk ) ls 
   		on ls.last_store_sk = ssh.store_sk and ls.last_product_gnm_sk = ssh.product_gnm_sk and ssh.source_staged = ls.last_source_staged
   	left join product_gnm pg on pg.sk = ssh.product_gnm_sk
) st
left join "store" s on s.sk = st.store_sk



    --- view for david
 
drop table store_stock_history;



SET search_path = public, pg_catalog;

CREATE TABLE store_stock_history (
    sk integer NOT NULL,
    store_sk integer NOT NULL,
    nk character varying(100) NOT NULL,
    takeon_storeproduct_sk integer,
    product_gnm_sk integer, 
    total integer NOT NULL,
    prior_total integer NOT NULL,
    employee_sk integer,
    source_staged timestamp without time zone NOT NULL,
    created timestamp without time zone DEFAULT now()
);


ALTER TABLE ONLY store_stock_history
    ADD CONSTRAINT store_stock_history_product_gnm_sk_fkey FOREIGN KEY (product_gnm_sk) REFERENCES product_gnm(sk);
  
--
-- Name: store_stock_history_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE store_stock_history_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: store_stock_history_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE store_stock_history_sk_seq OWNED BY store_stock_history.sk;

select * from store_stock_history

-- colour code instead of colour description

--- color description needs to be replaced with colour code, 
--- product

set search path = public, catalog;

select * from store_stockmovements

SELECT sk, store_sk, source_id, takeon_storeproduct_sk, adjustmentdate, stock_price_history_sk, adjustment, adjustmenttype, reason, invoice_sk, 
customer_sk, avg_cost_price, shipping_ref, shipping_operator, comments, employee_sk, created, adjustmentdate_sk, product_gnm_sk, 
adjustment_type_sk, actual_cost, actual_cost_value, moving_quantity, moving_actual_cost_value, moving_cost_per_unit
FROM public.store_stockmovement;


select store_sk, takeon_storeproduct_sk, product_gnm_sk, source_id, sum(adjustment) 
from store_stockmovement 
-- where product_gnm_sk is not null and product_gnm_sk != 0
group by store_sk, takeon_storeproduct_sk, product_gnm_sk,  source_id order by sum(adjustment) desc;

SELECT sk, store_sk, nk, takeon_storeproduct_sk, product_gnm_sk, total, prior_total, employee_sk, source_staged, created
FROM public.store_stock_history_adj;

-------------------------------------------------------------------------
--- initial
insert into public.store_stock_history_adj (store_sk,takeon_storeproduct_sk,product_gnm_sk,total,prior_total,source_staged)
select store_sk, takeon_storeproduct_sk, product_gnm_sk, sum(adjustment), -1, current_timestamp
from store_stockmovement 
where store_sk is not null  --product_gnm_sk is not null and product_gnm_sk != 0
group by store_sk, takeon_storeproduct_sk, product_gnm_sk,  source_id order by sum(adjustment) desc;


select store_sk, takeon_storeproduct_sk, product_gnm_sk, sum(adjustment), -1, current_timestamp
from store_stockmovement ssm 
inner join 
where store_sk is not null  --product_gnm_sk is not null and product_gnm_sk != 0
group by store_sk, takeon_storeproduct_sk, product_gnm_sk,  source_id order by sum(adjustment) desc;


          WITH RECURSIVE cogs(sk,adjustmentdate,store_sk,product_gnm_sk,adjustment,actual_cost,actual_cost_value,moving_quantity,moving_actual_cost_value,moving_cost_per_unit,next_sk) AS (

          select sk,
          adjustmentdate,
          store_sk,
          product_gnm_sk,
          adjustment,
          actual_cost::numeric(19,2),
          (adjustment * actual_cost)::numeric as actual_cost_value,
          adjustment as moving_quantity,
          (adjustment * actual_cost)::numeric as moving_actual_cost_value,
          actual_cost::numeric(19,2) as moving_cost_per_unit,
          (select im.sk from public.store_stockmovement im
          where im.product_gnm_sk = Y.product_gnm_sk and im.store_sk = Y.store_sk and
          ((im.adjustmentdate = Y.adjustmentdate and im.sk > Y.sk) or im.adjustmentdate > Y.adjustmentdate)
          order by adjustmentdate,im.sk limit 1
          )
          from (
          select sk,adjustmentdate,store_sk,product_gnm_sk,adjustment,gnm_actual_cost as actual_cost,
          row_number() over (partition by store_sk,product_gnm_sk order by adjustmentdate,sk ) r from (
          select m.sk,m.adjustmentdate,m.store_sk,m.product_gnm_sk,m.adjustment,

          (select actual_cost from public.stock_price_history h
                        where (h.product_gnm_sk = m.product_gnm_sk) AND h.created <= m.adjustmentdate
                        order by created desc limit 1) as gnm_actual_cost
          from public.store_stockmovement m
          inner join public.adjustment_type a on (m.adjustment_type_sk = a.sk and a.name = 'Receipt')
          where product_gnm_sk <> 0
          ) T where T.gnm_actual_cost is not null
          ) Y where r = 1


          UNION ALL

          select sk,adjustmentdate,store_sk,product_gnm_sk,adjustment,
          actual_cost::numeric(19,2),
          (adjustment * actual_cost)::numeric(19,2),
          prev_moving_quantity + adjustment,
          (prev_moving_actual_cost_value + (adjustment * actual_cost))::numeric(19,2),
          (CASE WHEN is_receipt = true AND (prev_moving_quantity + adjustment) <> 0 THEN
          (prev_moving_actual_cost_value + (adjustment * actual_cost)) / (prev_moving_quantity + adjustment)
           ELSE actual_cost END)::numeric(19,2),
          next_sk
          from (
          select m.sk,
          m.adjustmentdate,
          m.store_sk,
          m.product_gnm_sk,
          m.adjustment,
          c.moving_cost_per_unit as prev_moving_cost_per_unit,
          c.moving_quantity as prev_moving_quantity,
          c.moving_actual_cost_value as prev_moving_actual_cost_value,
          (CASE WHEN name = 'Receipt' THEN
            (select actual_cost from public.stock_price_history h
               where (h.product_gnm_sk = m.product_gnm_sk) AND h.created <= m.adjustmentdate
               AND actual_cost is not null
               order by created desc limit 1)
           ELSE c.moving_cost_per_unit END) as actual_cost,
          (CASE WHEN name = 'Receipt' THEN true else false END) as is_receipt,
          (select im.sk from public.store_stockmovement im
          where im.product_gnm_sk = m.product_gnm_sk and  im.store_sk = m.store_sk and
          ((im.adjustmentdate = m.adjustmentdate and im.sk > m.sk) or im.adjustmentdate > m.adjustmentdate)
          order by im.adjustmentdate,im.sk limit 1
          ) as next_sk
          from cogs c, public.store_stockmovement m
          inner join public.adjustment_type a on (m.adjustment_type_sk = a.sk)
          where c.next_sk = m.sk and c.next_sk is not null
          ) T

          )

          select sk,store_sk,actual_cost,actual_cost_value,moving_quantity,moving_actual_cost_value,moving_cost_per_unit from cogs
          
          
          

