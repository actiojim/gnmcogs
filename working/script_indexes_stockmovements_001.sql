

SET search_path = public, pg_catalog;

begin;


--
-- Name: adjustment_type sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY adjustment_type ALTER COLUMN sk SET DEFAULT nextval('adjustment_type_sk_seq'::regclass);


--
-- Name: invoice_order_job sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY invoice_order_job ALTER COLUMN sk SET DEFAULT nextval('invoice_order_job_sk_seq'::regclass);


--
-- Name: invoice_orders sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY invoice_orders ALTER COLUMN sk SET DEFAULT nextval('invoice_orders_sk_seq'::regclass);


--
-- Name: job_history sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY job_history ALTER COLUMN sk SET DEFAULT nextval('job_history_sk_seq'::regclass);


--
-- Name: order_history sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY order_history ALTER COLUMN sk SET DEFAULT nextval('order_history_sk_seq'::regclass);


--
-- Name: stock_price_history sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY stock_price_history ALTER COLUMN sk SET DEFAULT nextval('stock_price_history_sk_seq'::regclass);


--
-- Name: store_stock_history sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stock_history ALTER COLUMN sk SET DEFAULT nextval('store_stock_history_sk_seq'::regclass);


--
-- Name: store_stockmovement sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stockmovement ALTER COLUMN sk SET DEFAULT nextval('store_stockmovement_sk_seq'::regclass);


--
-- Name: takeon_product sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY takeon_product ALTER COLUMN sk SET DEFAULT nextval('takeon_product_sk_seq'::regclass);


--
-- Name: adjustment_type adjustment_type_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY adjustment_type
    ADD CONSTRAINT adjustment_type_pkey PRIMARY KEY (sk);


--
-- Name: invoice_order_job invoice_order_job_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY invoice_order_job
    ADD CONSTRAINT invoice_order_job_pkey PRIMARY KEY (sk);


--
-- Name: invoice_orders invoice_orders_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY invoice_orders
    ADD CONSTRAINT invoice_orders_pkey PRIMARY KEY (sk);


--
-- Name: job_history job_history_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY job_history
    ADD CONSTRAINT job_history_pkey PRIMARY KEY (sk);


--
-- Name: order_history order_history_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY order_history
    ADD CONSTRAINT order_history_pkey PRIMARY KEY (sk);


--
-- Name: d_date pk_d_date; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY d_date
    ADD CONSTRAINT pk_d_date PRIMARY KEY (sk);



--
-- Name: stock_price_history stock_price_history_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY stock_price_history
    ADD CONSTRAINT stock_price_history_pkey PRIMARY KEY (sk);



--
-- Name: store_stock_history store_stock_history_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stock_history
    ADD CONSTRAINT store_stock_history_pkey PRIMARY KEY (sk);


--
-- Name: store_stockmovement store_stockmovement_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stockmovement
    ADD CONSTRAINT store_stockmovement_pkey PRIMARY KEY (sk);



--
-- Name: takeon_product takeon_product_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY takeon_product
    ADD CONSTRAINT takeon_product_pkey PRIMARY KEY (sk);


--
-- Name: idx_adjustment_type_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX idx_adjustment_type_id ON adjustment_type USING btree (adjustment_typeid);


--
-- Name: idx_adjustment_type_name; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_adjustment_type_name ON adjustment_type USING btree (name);


--
-- Name: idx_d_date_dateactual; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_d_date_dateactual ON d_date USING btree (date_actual);


--
-- Name: idx_d_date_day_of_year; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_d_date_day_of_year ON d_date USING btree (day_of_year);


--
-- Name: idx_d_date_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX idx_d_date_id ON d_date USING btree (date_dim_id);


--
-- Name: idx_d_date_year_calendar; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_d_date_year_calendar ON d_date USING btree (year_calendar);


--
-- Name: idx_invoiceitemwithdate_createddate; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_invoiceitemwithdate_createddate ON invoiceitem USING btree (createddate);


--
-- Name: idx_invoiceitemwithdate_createddate_sk; Type: INDEX; Schema: public; Owner: -
--

-- CREATE INDEX idx_invoiceitemwithdate_createddate_sk ON invoiceitem USING btree (createddate_sk);


--
-- Name: idx_invoiceitemwithdate_customer_sk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_invoiceitemwithdate_customer_sk ON invoiceitem USING btree (customersk);


--
-- Name: idx_invoiceitemwithdate_employee_sk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_invoiceitemwithdate_employee_sk ON invoiceitem USING btree (employeesk);


--
-- Name: idx_invoiceitemwithdate_invoice_sk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_invoiceitemwithdate_invoice_sk ON invoiceitem USING btree (invoicesk);


--
-- Name: idx_invoiceitemwithdate_payee_sk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_invoiceitemwithdate_payee_sk ON invoiceitem USING btree (payeesk);


--
-- Name: idx_invoiceitemwithdate_product_sk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_invoiceitemwithdate_product_sk ON invoiceitem USING btree (productsk);


--
-- Name: idx_invoiceitemwithdate_serviceemployee_sk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_invoiceitemwithdate_serviceemployee_sk ON invoiceitem USING btree (serviceemployeesk);


--
-- Name: idx_invoiceitemwithdate_store_sk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_invoiceitemwithdate_store_sk ON invoiceitem USING btree (storesk);


--
-- Name: idx_model; Type: INDEX; Schema: public; Owner: -
--

-- CREATE INDEX idx_model ON supplierproduct USING btree (frame_model);


--
-- Name: idx_order_history_sourceid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_order_history_sourceid ON order_history USING btree (source_id);


--
-- Name: idx_orderid_order_history; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_orderid_order_history ON order_history USING btree (source_id);


--
-- Name: idx_orderid_pms_invoice_orders; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_orderid_pms_invoice_orders ON pms_invoice_orders USING btree (orderid);


--
-- Name: idx_stock_price_history_gnmproduct_date; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_stock_price_history_gnmproduct_date ON stock_price_history USING btree (product_gnm_sk, created DESC);


--
-- Name: idx_stock_price_history_source_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX idx_stock_price_history_source_id ON stock_price_history USING btree (source_id);


--
-- Name: idx_stock_price_history_takeon_product_date; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_stock_price_history_takeon_product_date ON stock_price_history USING btree (takeon_storeproduct_sk, created DESC);


--
-- Name: idx_store_nk; Type: INDEX; Schema: public; Owner: -
--

-- CREATE UNIQUE INDEX idx_store_nk ON store USING btree (store_nk);


--
-- Name: idx_store_stock_history_storeproduct_date; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_store_stock_history_storeproduct_date ON store_stock_history USING btree (storeproduct_sk, created DESC);


--
-- Name: idx_store_stock_history_takeon_product_date; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_store_stock_history_takeon_product_date ON store_stock_history USING btree (takeon_storeproduct_sk, created DESC);


--
-- Name: idx_store_stockmovement_adjustmentdate; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_store_stockmovement_adjustmentdate ON store_stockmovement USING btree (product_gnm_sk, adjustmentdate);


--
-- Name: idx_store_stockmovement_adjustmentdate2; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_store_stockmovement_adjustmentdate2 ON store_stockmovement USING btree (takeon_storeproduct_sk, adjustmentdate);


--
-- Name: idx_store_stockmovement_adjustmentdate_sk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_store_stockmovement_adjustmentdate_sk ON store_stockmovement USING btree (adjustmentdate_sk);


--
-- Name: idx_store_stockmovement_adjustmenttype_sk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_store_stockmovement_adjustmenttype_sk ON store_stockmovement USING btree (adjustment_type_sk);


--
-- Name: idx_store_stockmovement_invoice_sk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_store_stockmovement_invoice_sk ON store_stockmovement USING btree (invoice_sk);


--
-- Name: idx_store_stockmovement_source_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX idx_store_stockmovement_source_id ON store_stockmovement USING btree (source_id);


--
-- Name: idx_store_stockmovement_store_sk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_store_stockmovement_store_sk ON store_stockmovement USING btree (store_sk);


--
-- Name: idx_store_stockmovement_takeon_storeproduct_sk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_store_stockmovement_takeon_storeproduct_sk ON store_stockmovement USING btree (takeon_storeproduct_sk);



--
-- Name: invoiceitem_createddate_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX invoiceitem_createddate_idx ON invoiceitem_old USING btree (createddate);



--
-- Name: productid_on_product; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX productid_on_product ON storeproduct USING btree (productid);


--
-- Name: productid_on_product_gnm; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX productid_on_product_gnm ON product_gnm USING btree (productid);

--
-- Name: invoice_order_job invoice_order_job_invoice_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY invoice_order_job
    ADD CONSTRAINT invoice_order_job_invoice_sk_fkey FOREIGN KEY (invoice_sk) REFERENCES invoice(sk);


--
-- Name: invoice_order_job invoice_order_job_store_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY invoice_order_job
    ADD CONSTRAINT invoice_order_job_store_sk_fkey FOREIGN KEY (store_sk) REFERENCES store(sk);


--
-- Name: invoice_orders invoice_orders_invoice_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY invoice_orders
    ADD CONSTRAINT invoice_orders_invoice_sk_fkey FOREIGN KEY (invoice_sk) REFERENCES invoice(sk);


--
-- Name: invoice_orders invoice_orders_order_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY invoice_orders
    ADD CONSTRAINT invoice_orders_order_sk_fkey FOREIGN KEY (order_sk) REFERENCES order_history(sk);


--
-- Name: invoice_orders invoice_orders_store_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY invoice_orders
    ADD CONSTRAINT invoice_orders_store_sk_fkey FOREIGN KEY (store_sk) REFERENCES store(sk);


--
-- Name: job_history job_history_product_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY job_history
    ADD CONSTRAINT job_history_product_sk_fkey FOREIGN KEY (product_sk) REFERENCES product_gnm(sk);


--
-- Name: job_history job_history_supplier_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY job_history
    ADD CONSTRAINT job_history_supplier_sk_fkey FOREIGN KEY (supplier_sk) REFERENCES supplier(sk);


--
-- Name: order_history order_history_customer_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY order_history
    ADD CONSTRAINT order_history_customer_sk_fkey FOREIGN KEY (customer_sk) REFERENCES customer(sk);


--
-- Name: order_history order_history_store_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY order_history
    ADD CONSTRAINT order_history_store_sk_fkey FOREIGN KEY (store_sk) REFERENCES store(sk);


--
-- Name: stock_price_history stock_price_history_store_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY stock_price_history
    ADD CONSTRAINT stock_price_history_store_sk_fkey FOREIGN KEY (store_sk) REFERENCES store(sk);


--
-- Name: stock_price_history stock_price_history_takeon_storeproduct_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY stock_price_history
    ADD CONSTRAINT stock_price_history_takeon_storeproduct_sk_fkey FOREIGN KEY (takeon_storeproduct_sk) REFERENCES storeproduct(sk);


--
-- Name: store_stock_history store_stock_history_employee_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stock_history
    ADD CONSTRAINT store_stock_history_employee_sk_fkey FOREIGN KEY (employee_sk) REFERENCES employee(sk);


--
-- Name: store_stock_history store_stock_history_store_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stock_history
    ADD CONSTRAINT store_stock_history_store_sk_fkey FOREIGN KEY (store_sk) REFERENCES store(sk);


--
-- Name: store_stock_history store_stock_history_storeproduct_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stock_history
    ADD CONSTRAINT store_stock_history_product_gnm_sk_fkey FOREIGN KEY (product_gnm_sk) REFERENCES product_gnm(sk);


--
-- Name: store_stock_history store_stock_history_takeon_storeproduct_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stock_history
    ADD CONSTRAINT store_stock_history_takeon_storeproduct_sk_fkey FOREIGN KEY (takeon_storeproduct_sk) REFERENCES storeproduct(sk);


--
-- Name: store_stockmovement store_stockmovement_customer_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stockmovement
    ADD CONSTRAINT store_stockmovement_customer_sk_fkey FOREIGN KEY (customer_sk) REFERENCES customer(sk);


--
-- Name: store_stockmovement store_stockmovement_employee_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stockmovement
    ADD CONSTRAINT store_stockmovement_employee_sk_fkey FOREIGN KEY (employee_sk) REFERENCES employee(sk);


--
-- Name: store_stockmovement store_stockmovement_invoice_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stockmovement
    ADD CONSTRAINT store_stockmovement_invoice_sk_fkey FOREIGN KEY (invoice_sk) REFERENCES invoice(sk);


--
-- Name: store_stockmovement store_stockmovement_stock_price_history_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stockmovement
    ADD CONSTRAINT store_stockmovement_stock_price_history_sk_fkey FOREIGN KEY (stock_price_history_sk) REFERENCES stock_price_history(sk);


--
-- Name: store_stockmovement store_stockmovement_store_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stockmovement
    ADD CONSTRAINT store_stockmovement_store_sk_fkey FOREIGN KEY (store_sk) REFERENCES store(sk);


--
-- Name: store_stockmovement store_stockmovement_takeon_storeproduct_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stockmovement
    ADD CONSTRAINT store_stockmovement_takeon_storeproduct_sk_fkey FOREIGN KEY (takeon_storeproduct_sk) REFERENCES storeproduct(sk);


--
-- Name: supplierproduct supplierproduct_supplier_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY supplierproduct
    ADD CONSTRAINT supplierproduct_supplier_fkey FOREIGN KEY (suppliersk) REFERENCES supplier(sk);


--
-- Name: takeon_product takeon_product_store_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY takeon_product
    ADD CONSTRAINT takeon_product_store_sk_fkey FOREIGN KEY (store_sk) REFERENCES store(sk);


--
-- Name: takeon_product takeon_product_supplier_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY takeon_product
    ADD CONSTRAINT takeon_product_supplier_sk_fkey FOREIGN KEY (supplier_sk) REFERENCES supplier(sk);



