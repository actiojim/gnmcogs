drop function within_dow_range( d1 timestamp, d2 timestamp);

create or replace function within_dow_range ( d1 timestamp, d2 timestamp) returns boolean
as $$
declare
	td1 timestamp;
	td2 timestamp;
begin	
	if date_part('doy',d1) = date_part('doy',d2) and date_part('year',d1) = date_part('year',d2) then
		return true;
	elsif d2 >= (d1 - interval '1 days') and d2 <= (d1 + interval '1 day') then
		return true;
	elsif date_part('dow',d1) = 1 and d2 >= (d1 - interval '3 days') and d2 <= (d1 + interval '1 day') then
		return true;
	elsif date_part('dow',d1) = 5 and d2 >= (d1 - interval '1 days') and d2 <= (d1 + interval '3 day') then
		return true;
	elsif date_part('dow',d1) = 6 and d2 >= (d1 - interval '1 days') and d2 <= (d1 + interval '2 day') then
		return true;
	else
		return false;
	end if;
end;
$$ LANGUAGE plpgsql;

create or replace function within_dow_range ( d1 timestamp, d2 timestamp, bounds integer) returns boolean
as $$
begin	
	if d2 >= (d1 - interval '1d'* bounds) and d2 <= (d1 + interval '1d'* bounds) then
		return true;
	else
		return within_dow_range(d1,d2);
	end if;
end;
$$ LANGUAGE plpgsql;
