

SET search_path = public, pg_catalog;


drop table store_stock_history_adj;



CREATE TABLE store_stock_history_adj (
    sk  serial primary key NOT NULL,
    store_sk integer NOT NULL,
    nk character varying(100),
    takeon_storeproduct_sk integer,
    product_gnm_sk integer, 
    total integer NOT NULL,
    prior_total integer NOT NULL,
    employee_sk integer,
    source_staged timestamp without time zone NOT NULL,
    created timestamp without time zone DEFAULT now()
);


ALTER TABLE ONLY store_stock_history_adj
    ADD CONSTRAINT store_stock_history_adj_product_gnm_sk_fkey FOREIGN KEY (product_gnm_sk) REFERENCES product_gnm(sk);
  
    
alter table store_stock_history_adj drop constraint store_stock_history_adj_product_gnm_sk_fkey;

--
-- Name: store_stock_history_adj_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

drop view view_store_stock_history_takeon;
drop view view_store_stock_history_gnm;


create or replace view view_store_stock_history_takeon as
select st.*, s.name as storename ,s.chainid,s.store_nk from
(
select 
	store_sk, 
	takeon_storeproduct_sk, 
	ssh.product_gnm_sk, 
	total, prior_total, 
	employee_sk, 
	source_staged, 
	ssh.created,
	sp.productid, sp.type,sp.name,sp.description,sp.frame_model,sp.frame_colour,sp.frame_brand
from  public.store_stock_history_adj ssh 
	inner join  ( select nk as last_nk, store_sk as last_store_sk, takeon_storeproduct_sk as last_takeon_storeproduct_sk,max(source_staged) as last_source_staged 
   	from store_stock_history_adj ssh group by nk,store_sk,takeon_storeproduct_sk ) ls 
   		on ls.last_store_sk = ssh.store_sk and ls.last_takeon_storeproduct_sk = ssh.takeon_storeproduct_sk and ssh.source_staged = ls.last_source_staged
   	left join storeproduct sp on sp.sk = ssh.takeon_storeproduct_sk
) st
left join "store" s on s.sk = st.store_sk


create or replace view view_store_stock_history_gnm as
select st.*, s.name as storename ,s.chainid,s.store_nk from
(
select  
	store_sk, 
	takeon_storeproduct_sk, 
	ssh.product_gnm_sk, 
	total, 
	prior_total, 
	employee_sk, 
	source_staged, 
	ssh.created,
	pg.productid, pg.type,pg.name,pg.description,pg.frame_model,pg.frame_colour,pg.frame_brand
from  public.store_stock_history_adj ssh 
	inner join ( select nk as last_nk, store_sk as last_store_sk, product_gnm_sk as last_product_gnm_sk,max(source_staged) as last_source_staged 
   	from store_stock_history_adj ssh group by nk,store_sk,product_gnm_sk ) ls 
   		on ls.last_store_sk = ssh.store_sk and ls.last_product_gnm_sk = ssh.product_gnm_sk and ssh.source_staged = ls.last_source_staged
   	left join product_gnm pg on pg.sk = ssh.product_gnm_sk
) st
left join "store" s on s.sk = st.store_sk

select * from storeproduct



select storeid,name,chainid, * from public."store"

-- join storeid -- storeid,name,chainid,nk
-- join productid -- 

select * from public.storeproduct sp


select * from public.store_stock_history

select * from scire.public.view_store_stock_history where takeon_storeproduct_sk <> 0



select * from temp_store_stockmovement

select * from product_gnm where sk = 0






alter database scire owner to gnm;


select * from view_store_stock_history where takeon_storeproduct_sk is not null and takeon_storeproduct_sk <> 0 and name is not null


select * from storeproduct

select * from product_gnm 


select * from store_stockmovement


-------------------------------------------------------------------------
--- initial 

delete from public.store_stock_history_adj;


insert into public.store_stock_history_adj (store_sk,takeon_storeproduct_sk,product_gnm_sk,total,prior_total,source_staged)
select store_sk, takeon_storeproduct_sk, product_gnm_sk, sum(adjustment), -1, current_timestamp
from store_stockmovement 
where store_sk is not null  --product_gnm_sk is not null and product_gnm_sk != 0
group by store_sk, takeon_storeproduct_sk, product_gnm_sk,  source_id order by sum(adjustment) desc;


select store_sk, takeon_storeproduct_sk, product_gnm_sk, sum(adjustment), -1, current_timestamp
from store_stockmovement ssm 
inner join 
where store_sk is not null  --product_gnm_sk is not null and product_gnm_sk != 0
group by store_sk, takeon_storeproduct_sk, product_gnm_sk,  source_id order by sum(adjustment) desc;


select * from store_stock_history_adj where product_gnm_sk is not null 
