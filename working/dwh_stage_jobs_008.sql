SET search_path = stage;
 
drop table if exists order_stage;
 
create table order_stage
(
    sk serial NOT NULL,
	run_id character varying(100),
	   
    -- source data order id
    order_id character varying(100),
    
    cust_firstname character varying(100),
    cust_lastname character varying(100),
    cust_midname character varying(100),
    cust_phone character varying(100),
    store_id character varying(100),
    
    cust_sk int,
    order_type character varying(100),
    
    queue character varying(100),
    status character varying(100),
    
    source_created character varying(100),
    source_modified character varying(100),
    
    -- created date
    created timestamp without time zone NOT null default now(),
    primary key (sk)
);


-- 1. customer entries that are missing from customer table
INSERT INTO public.customer ( storesk, firstname, lastname, homephone, createddate, title, gender, email)
select  distinct  s.sk, os.cust_firstname, os.cust_lastname, os.cust_phone, now(), '', '', ''
from stage.order_stage os 
left join public.customer c on c.firstname = os.cust_firstname and c.lastname = os.cust_lastname  
-- and (os.cust_phone = c.homephone or os.cust_phone = c.mobile or os.cust_phone = c.workphone)
left join public.store s on s.storeid = os.store_id -- and c.storesk = s.sk
where
 c.sk is null;
 
 
-- stage orders
INSERT INTO order_stage
(run_id, order_id, 
cust_firstname, cust_lastname, cust_phone, 
cust_sk, store_id, 
order_type, 
queue, status, 
source_created, source_modified)
SELECT null , o.order_id, 
oc.first_name, oc.last_name, oc.telephone, 
c.sk, s.store_id, 
o.order_type_id,
o.queue, o.status, 
o.created_date_time, o.modified_date_time
FROM gnm."order" o 
inner join gnm.order_customer oc on oc.id = o.customer_id
inner join gnm."store" s on s.id = o.store_id
left outer join public.store ps on ps.storeid = s.store_id
left join public.customer c on c.firstname = oc.first_name and c.lastname = oc.last_name and c.storesk = ps.sk

select count(*) from (
SELECT null , o.order_id, 
oc.first_name, oc.last_name, oc.telephone, 
c.sk, s.store_id, 
o.order_type_id,
o.queue, o.status, 
o.created_date_time, o.modified_date_time
FROM gnm."order" o 
inner join gnm.order_customer oc on oc.id = o.customer_id
inner join gnm."store" s on s.id = o.store_id
left join public.store ps on ps.storeid = s.store_id
left join public.customer c on c.firstname = oc.first_name and c.lastname = oc.last_name and c.storesk = ps.sk) x

-- find customer
/*
select count(*) from (
select  distinct os.store_id, s.sk, os.cust_firstname, os.cust_lastname, os.cust_phone
from stage.order_stage os 
left join public.customer c on c.firstname = os.cust_firstname and c.lastname = os.cust_lastname  
-- and (os.cust_phone = c.homephone or os.cust_phone = c.mobile or os.cust_phone = c.workphone)
left join public.store s on s.storeid = os.store_id -- and c.storesk = s.sk
where
 c.sk is  null) x

select * from public.customer

*/


	
 /*

select * from gnm.store
select * from gnm."order"
select * from gnm.job

*/

----------------------------------------------------------------------------------------------------------------------

SET search_path = stage;
 
drop table if exists job_stage;
 
create table job_stage
(
    sk serial NOT NULL,
	run_id character varying(100),
	
    -- natural source key job_id
    job_id character varying(100),
    
    -- source data order id
    order_id character varying(100),
    product_id character varying(100),
    supplier_id character varying(100),
    quantity character varying(100),
    
    -- job type frame or lens
    job_type character varying(100),
    queue character varying(100),
    status character varying(100),
    source_created character varying(100),
    source_modified character varying(100),
    
    -- created date
    created timestamp without time zone NOT null default now(),
    primary key (sk)
);

--
-- Generate job stage
-- 
/*
insert into stage.job_stage (
	run_id,
	order_id,
	customer_id,
	store_id,
	job_id,
	queue,
	status,
	quantity,
	source_created,
	source_modified,
	job_type,
	product_id,
	supplier_id)
select  
	j.runid,
	o.orderid,
	o.store_id,
	c.customerId, 
	j.f,>'id' as jobid
	 ,f,'attributes',>'queue' as queue
	 ,f,'attributes',>'status' as status
	 ,f,'attributes',>'quantity' as quantity
	 ,f,'attributes',>'createdDateTime' as created
	 ,f,'attributes',>'modifiedDateTime' as modified
	 ,f,'relationships','jobType','data',>'id' as jobtype
	 ,f,'relationships','product','data',>'id' as productid
	 ,f,'relationships','supplier','data',>'id' as supplierid 
from  
  (select runid, jsonb_array_elements(jsonb_array_elements(data),'data') as f from stage.gnm_hub_orders 
 	where runid in (select runid from stage.gnm_hub_orders where createddate >= '2017-08-20 18:47:46')) j 
  inner join 
	(select runid,f,>'id' as orderid,f,'relationships','customer','data',>'id'as customer_id, f,'relationships','store','data',>'id'as store_id from 
		(select runid,jsonb_array_elements(jsonb_array_elements(y.data),'included') as f 
			from stage.gnm_hub_orders y 
			where runid in (select runid from stage.gnm_hub_orders where createddate >= '2017-08-20 18:47:46') ) x where f,>'type' = 'order') o on f,'relationships','order','data',>'id' = o.orderid 
  inner join 
	(select runid,f,>'id' as id,f,'attributes',>'customerId' as customerId from 
			(select runid,jsonb_array_elements(jsonb_array_elements(y.data),'included' ) as f 
					from stage.gnm_hub_orders y 
					where runid in (select runid from stage.gnm_hub_orders where createddate >= '2017-08-20 18:47:46') ) x
		where f,>'type' = 'orderCustomer') c on o.customer_id = c.id
  
 where j.f,>'id'  not in (select job_id from job_stage)
*/

insert into stage.job_stage (
	run_id,
	order_id,
	job_id,
	queue,
	status,
	quantity,
	source_created,
	source_modified,
	job_type,
	product_id,
	supplier_id)
select  
	null,
	j.order_id,
	j.id,
	j.queue,
	j.status,
	j.quantity,
	j.created_date_time,
	j.modified_date_time,
	j.job_type_id,
	j.product_id,
	j.supplier_id
from gnm.job j ;

-- ------------------------------------------------------------------------------------------
select * from product_gnm;

SET search_path = public;
CREATE UNIQUE INDEX productid_on_product ON public.storeproduct (productid);

--- Populate store product from 

--- create table
drop table public.product_gnm;

CREATE TABLE public.product_gnm
(
    sk serial NOT NULL,
    productid character varying(500) COLLATE pg_catalog."default",
    type character varying(100) COLLATE pg_catalog."default",
    datecreated timestamp without time zone,
    name character varying(500) COLLATE pg_catalog."default",
    description character varying(500) COLLATE pg_catalog."default",
    retailprice numeric(19, 4),
    wholesaleprice numeric(19, 4),
    modified timestamp without time zone,
    last_purchase timestamp without time zone,
    last_sale timestamp without time zone,
    quantity integer,
    frame_model character varying(100) COLLATE pg_catalog."default" DEFAULT ''::character varying,
    frame_colour character varying(100) COLLATE pg_catalog."default" DEFAULT ''::character varying,
    frame_brand character varying(100) COLLATE pg_catalog."default" DEFAULT ''::character varying,
    frame_temple numeric(8, 2) DEFAULT 0,
    frame_bridge numeric(8, 2) DEFAULT 0,
    frame_eye_size numeric(8, 2) DEFAULT 0,
    frame_depth numeric(8, 2) DEFAULT 0,
    frame_material character varying(100) COLLATE pg_catalog."default" DEFAULT ''::character varying,
    lens_type character varying(100) COLLATE pg_catalog."default" DEFAULT ''::character varying,
    lens_refractive_index numeric(19, 4) DEFAULT 0,
    lens_stock_grind character varying(10) COLLATE pg_catalog."default" DEFAULT ''::character varying,
    lens_size numeric(4, 2) DEFAULT 0,
    lens_segment_size numeric(4, 2) DEFAULT 0,
    internal_sku character varying(100) COLLATE pg_catalog."default",
    distributor character varying(100) COLLATE pg_catalog."default",
    created timestamp without time zone NOT NULL default now(),
     PRIMARY KEY (sk)
)

--
-- Name: product_gnm_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE product_gnm_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: product_gnm_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE product_gnm_sk_seq OWNED BY product_gnm.sk;


-- INSERT INTO storeproduct
insert into public.product_gnm
(productid, "type", 
datecreated, modified, 
"name", description, internal_sku, distributor)
SELECT product_id, "type", 
created_date_time, modified_date_time,
p."name",  description, supplier_sku, s.sk
FROM gnm.product p 
inner join public.supplier s on s.externalref = p.supplier_id
-- left join 


select * from public.product_gnm

/*
SELECT id, supplier_id, "name", email, fax, telephone, contact_person_id, created_user, created_date_time, modified_user, modified_date_time
FROM gnm.supplier;

select * from public.storeproduct where storeid = '0'
select * from public.supplier
*/

------------------------------------------------------------------------------------------------------------------------------------------

SET search_path = public;

-- select * from stock_price_history


INSERT INTO stock_price_history
(store_sk, source_id, takeon_storeproduct_sk, product_gnm_sk, 
supplier_cost, supplier_cost_gst, 
actual_cost, actual_cost_gst, 
supplier_rrp, supplier_rrp_gst, calculated_retail_price, calculated_retail_price_gst, 
override_retail_price, override_retail_price_gst, 
retail_price, retail_price_gst, 
created)
-- VALUES(nextval('stock_price_history_sk_seq'::regclass), 0, '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, now());
SELECT 
0, id, 0, sp.sk, 
supplier_cost, supplier_cost_gst, 
actual_cost, actual_cost_gst, 
supplier_rrp, supplier_rrp_gst, calculated_retail_price, calculated_retail_price_gst, 
0,0,
0,0,
modified_date_time
FROM gnm.product_cost_history pch left join product_gnm sp on sp.productid = pch.product_id


-----------------------------------------------------------------------------------------------------------------------------------------
/*
select * from "store"

select * from gnm_scire_0816.storeproduct

INSERT INTO gnm_scire_0816.storeproduct
(storeid, productid, "type", datecreated, modified, "name", description, internal_sku )
SELECT 0, product_id, "type", created_date_time, modified_date_time, "name",  description, supplier_sku
FROM gnm.product 
-- where product_id not in (select productid from gnm_scire_0816.storeproduct )

select * from gnm.product
where product_id not in (select productid from gnm_scire_0816.storeproduct )

select * from gnm_scire_0816.storeproduct sp inner join gnm_scire_0816.stock_price_history sph on sph.storeproduct_sk = sp.sk

*/
--------------------------------------------------------------------------------------------------------------------------------------------

SET search_path = public;

-- stage order history
drop table if exists order_history cascade;
drop table if exists job_history cascade;
drop table if exists invoice_orders cascade;

--
-- history record is immutable a new record is created for each data change
--
create table order_history
(
    sk serial NOT NULL,
    store_sk int NOT NULL REFERENCES store(sk),
    customer_sk int NOT NULL REFERENCES customer(sk),
    
    -- natural source key
    source_id character varying(100),
    
    order_type character varying(100) NOT NULL,
    queue character varying(100),
    status character varying(100),

    source_created timestamp not null,
    source_modified timestamp not null,

    -- created date
    created timestamp without time zone NOT NULL default now(),
    primary key (sk)
);


-- 
-- history record is immutable a new record is created for each data change
--
create table job_history
(
    sk serial NOT NULL,  
    -- natural source key job_id
    source_id character varying(100),
    
    -- source data order id
    order_id character varying(100) NOT NULL,
    order_sk int not null,
    
    -- job type frame or lens
    job_type character varying(100) NOT NULL,
    
    product_sk int not null references product_gnm(sk),
    supplier_sk int not null references supplier(sk),
    
    queue character varying(100),
    status character varying(100),


    source_created timestamp not null,
    source_modified timestamp not null,

    -- created date
    created timestamp without time zone NOT NULL default now(),
    primary key (sk)
);


-- drop table public.job_history;


-- Bind Table between invoice and orders
create table invoice_orders
(
    sk serial NOT NULL,
    store_sk int NOT NULL REFERENCES store(sk),
    
    invoice_sk int NOT NULL REFERENCES invoice(sk),
    order_sk int NOT NULL REFERENCES order_history(sk), 

    -- created date
    created timestamp without time zone NOT NULL default now(),
    primary key (sk)
)

--- -----------------------------------------------------------------------------------------------------------------------------

SET search_path = public;

/*
select count(*) from (select  order_id from stage.job_stage js group by js.order_id) x;
select count(*) from (select  order_id from gnm.order o group by o.order_id) x;
select * from order_history
select * from stage.order_stage
select * from "store"
SELECT sk, storeid, "name", chainid, isactive FROM "store";

select * from order_history

select * from order_job_history
select * from stage.job_stage
*/

-- process the orders
INSERT INTO order_history
(store_sk, 
customer_sk,
source_id, 
order_type, 
queue, status, 
source_created, source_modified)
SELECT 
coalesce(s.sk, 0), 
coalesce(c.sk, 0),
order_id, 
coalesce(os.order_type,'UNKNOWN'), 
os.queue, os.status, 
-- to_timestamp(os.source_created,'YYYY-MM-DD HH24:MI:SS'), to_timestamp(os.source_modified,'YYYY-MM-DD HH24:MI:SS')
os.source_created::TimeSTAMP with time zone, os.source_modified::TimeSTAMP with time zone
FROM stage.order_stage os 
left join "store" s on s.storeid = os.store_id
left join customer c on c.firstname = os.cust_firstname and c.lastname = os.cust_lastname and c.storesk = s.sk
left join order_history oh on oh.source_id = os.order_id
where 
	oh.source_id is null

	
-- 
-- history record is immutable a new record is created for each data change
--


-- select * from public.storeproduct
-- select * from gnm.product
-- select * from stage.job_stage
-- select * from public.supplier
-- select * from public.job_history

INSERT INTO public.job_history
(source_id, order_id, 
order_sk, 
job_type, 
product_sk, supplier_sk, 
queue, status, 
source_created, source_modified)
select
js.job_id, js.order_id,
-- order sk
oh.sk,
js.job_type,
coalesce(p.sk,0) as productsk, coalesce(s.sk,0) as storesk,
js.queue, js.status,
js.source_created::TimeSTAMP with time zone, js.source_modified::TimeSTAMP with time zone
-- select *
from stage.job_stage js 
inner join order_history oh on oh.source_id = js.order_id
inner join public.supplier s on CAST(s.externalref as varchar(100) ) = js.supplier_id
inner join public.product_gnm p on js.product_id =   p.productid --CAST( p.productid as varchar(100))
left join public.job_history jh on jh.source_id = js.job_id
where 
	jh.sk is null
	
-- where
--	s.sk is not null and  p.sk is not null


-- what we believe is shaped by our ignorance, our beliefs expand to fill the space of our ignorance
-- 

-- update store 
select * from public.store_stockmovement
select * from public.invoice i left join public.stock_stockmovement sm on sm.invoice_sk isk = i.sk

---------------------------------------------------------------------------------------------------------------------------------

SET search_path = public;
drop function fn_create_viewbydate_string(tablename text, datecolumnname text);

create or replace function fn_create_viewbydate_string(tablename text, datecolumnname text) returns text as $$
select 'CREATE OR REPLACE VIEW vbyd_' || $1 || ' as select date_part(''year'',sm.' || $2 || ')as year,date_part(''doy'',sm.' 
|| $2 || ') as doy,date_part(''month'',sm.' || $2 || ' )as month,date_part(''dow'',sm.' || $2 || ') as dow,date_part(''day'',sm.'|| $2 ||') as day, sk 
from ' || $1 || ' sm 
group by date_part(''year'',sm.' || $2 || '),date_part(''doy'',sm.' || $2 || '),date_part(''month'',sm.' || $2 || '),date_part(''dow'',sm.' || $2 || '),date_part(''day'',sm.' || $2 || '), sk' as view_string $$
LANGUAGE sql;

drop function fn_create_viewbydate(tablename text, datecolumnname text) ;
create or replace function fn_create_viewbydate(tablename text, datecolumnname text) 
returns void as $func$
begin
	EXECUTE fn_create_viewbydate_string($1, $2);
end
$func$ 
LANGUAGE plpgsql;


drop function fn_create_viewbydate_string(tablename text, datecolumnname text, keycolumn text);

create or replace function fn_create_viewbydate_string(tablename text, datecolumnname text, keycolumn text) returns text as $$
select 'CREATE OR REPLACE VIEW vbyd_' || $1 || ' as select date_part(''year'',sm.' || $2 || ')as year,date_part(''doy'',sm.' 
|| $2 || ') as doy,date_part(''month'',sm.' || $2 || ' )as month,date_part(''dow'',sm.' || $2 || ') as dow,date_part(''day'',sm.'|| $2 ||') as day, ' || $3 || '  
from ' || $1 || ' sm 
group by date_part(''year'',sm.' || $2 || '),date_part(''doy'',sm.' || $2 || '),date_part(''month'',sm.' || $2 || '),date_part(''dow'',sm.' || $2 || '),date_part(''day'',sm.' || $2 || '),' || $3 as view_string $$
LANGUAGE sql;

drop function fn_create_viewbydate(tablename text, datecolumnname text, keycolumn text) ;
create or replace function fn_create_viewbydate(tablename text, datecolumnname text, keycolumn text) 
returns void as $func$
begin
	EXECUTE fn_create_viewbydate_string($1, $2,$3);
end
$func$ 
LANGUAGE plpgsql;

select public.fn_create_viewbydate_string('stock_price_history', 'created');


select fn_create_viewbydate('store_stockmovement', 'adjustmentdate');
select fn_create_viewbydate('stock_price_history', 'created');
select fn_create_viewbydate('invoice','createddate');
select fn_create_viewbydate_string('invoiceitem','createddate','invoicesk')
select fn_create_viewbydate('order_history','source_created');
select fn_create_viewbydate('job_history','source_created');



CREATE OR REPLACE VIEW vbyd_job_history as select date_part('year',sm.source_created)as year,
date_part('doy',sm.source_created) as doy,date_part('month',sm.source_created )as month,date_part('dow',sm.source_created) as dow,date_part('day',sm.source_created) as day, sk 
from public.job_history sm 
group by date_part('year',sm.source_created),date_part('doy',sm.source_created),date_part('month',sm.source_created),date_part('dow',sm.source_created),date_part('day',sm.source_created), sk

select * from job_history


CREATE OR REPLACE VIEW vbyd_invoiceitem as select date_part('year',sm.createddate)as year,date_part('doy',sm.createddate) as doy,date_part('month',sm.createddate )as month,date_part('dow',sm.createddate) as dow,date_part('day',sm.createddate) as day, invoicesk  
from invoiceitem sm 
group by date_part('year',sm.createddate),date_part('doy',sm.createddate),date_part('month',sm.createddate),date_part('dow',sm.createddate),date_part('day',sm.createddate),invoicesk

select * from vbyd_stock_price_history where year = 2017;
select * from vbyd_store_stockmovement where year = 2017;
select * from vbyd_invoice where year = 2017 and month = 7;
select * from vbyd_invoiceitem
select * from vbyd_order_history 
select * from vbyd_job_history


select * from invoiceitem
select * from order_history

select * from public.order_history oh 
inner join public.job_history jh on jh.order_sk = oh.sk
left join invoiceitem ii on ii.customersk = oh.customer_sk and ii.productsk = jh.product_sk
where ii.invoicesk is not null


select order_id,count(*) from public.job_history group by order_id order by count(*) desc

-- steps 

/*
 * stock_movements
 * orders
 * job_history - view of get last state
 * 
 * join invoice to order - by day(+- 1,2 or Monday/Friday) time dimension may need day of week then, storeproductid (storeid,productid), storeid, customerid
 * 
 */

delete from public.storeproduct where storeid = '0'

select * from public.storeproduct where storeid = '0000-000' and productid in (select product_id from gnm.product)

begin;
update public.storeproduct set storeid = '0000-000' where storeid = '0' and productid in (select product_id from gnm.product)
-- commit;

begin;
update public.storeproduct set description="name","name" = description where storeid = '0'
-- commit;
-- rollback;

begin ;
-- ref to history first
delete from public.stock_price_history where storeproduct_sk in (select sk from public.storeproduct where storeid = '0000-000')
delete from public.storeproduct where storeid = '0000-000'
rollback;

select * from public.storeproduct where storeid = '0000-000'
select * from public.storeproduct where storeid <> '0000-000'

select * from gnm.product p 
inner join 
(select substring(productid,10, length(productid)) as pname,productid from public.storeproduct where storeid != '0') x on x.pname = p."name"

select x.storeid, x.description,p.product_id,count(*) into prod_gnm from gnm.product p 
inner join 
 public.storeproduct x on x.description = p."name" group by x.storeid, x.description,p.product_id

 
--- number of gnm products at a store
select * from public.store s
inner join 
(select storeid, count(*) from prod_gnm pg group by storeid) x  on s.storeid = x.storeid

select *  from prod_gnm pg inner join "store" s on s.storeid = pg.storeid

select * from gnm.product

--------------------------------------------------------------------------------------------


select * from public."store"
begin;
insert into public.store(sk,storeid,name,chainid,isactive,store_nk,acquisition_date)
values (-1,'0000-000','gnm','gnm',true,'gnm','2015-01-01')
-- commit

select * from public.invoiceitem

--- clear out incorrect products

select substring(productid,10, length(productid)) as pname,productid,storeid,"name",description,internal_sku from public.storeproduct where storeid != '0'

select x.pname,count(*) into prod_multi_store from
(select substring(productid,10, length(productid)) as pname,productid from public.storeproduct where storeid != '0') x
group by x.pname having count(*) > 1 order by count(*) desc


select count(*) from prod_multi_store

MERGE INTO public.storeproduct AS tgt
USING SOURCE_TABLE AS src
ON (tgt.sk=src.sk)
WHEN MATCHED
THEN UPDATE SET
tgt.tableoid=src.tableoid, tgt.cmax=src.cmax, tgt.xmax=src.xmax, tgt.cmin=src.cmin, tgt.xmin=src.xmin, tgt.ctid=src.ctid, tgt.storeid=src.storeid, tgt.productid=src.productid, tgt."type"=src."type", tgt.datecreated=src.datecreated, tgt."name"=src."name", tgt.description=src.description, tgt.retailprice=src.retailprice, tgt.wholesaleprice=src.wholesaleprice, tgt.modified=src.modified, tgt.last_purchase=src.last_purchase, tgt.last_sale=src.last_sale, tgt.quantity=src.quantity, tgt.frame_model=src.frame_model, tgt.frame_colour=src.frame_colour, tgt.frame_brand=src.frame_brand, tgt.frame_temple=src.frame_temple, tgt.frame_bridge=src.frame_bridge, tgt.frame_eye_size=src.frame_eye_size, tgt.frame_depth=src.frame_depth, tgt.frame_material=src.frame_material, tgt.lens_type=src.lens_type, tgt.lens_refractive_index=src.lens_refractive_index, tgt.lens_stock_grind=src.lens_stock_grind, tgt.lens_size=src.lens_size, tgt.lens_segment_size=src.lens_segment_size, tgt.internal_sku=src.internal_sku, tgt.distributor=src.distributor
WHEN NOT MATCHED
THEN INSERT (tableoid, cmax, xmax, cmin, xmin, ctid, sk, storeid, productid, "type", datecreated, "name", description, retailprice, wholesaleprice, modified, last_purchase, last_sale, quantity, frame_model, frame_colour, frame_brand, frame_temple, frame_bridge, frame_eye_size, frame_depth, frame_material, lens_type, lens_refractive_index, lens_stock_grind, lens_size, lens_segment_size, internal_sku, distributor)
VALUES (src.tableoid, src.cmax, src.xmax, src.cmin, src.xmin, src.ctid, src.sk, src.storeid, src.productid, src."type", src.datecreated, src."name", src.description, src.retailprice, src.wholesaleprice, src.modified, src.last_purchase, src.last_sale, src.quantity, src.frame_model, src.frame_colour, src.frame_brand, src.frame_temple, src.frame_bridge, src.frame_eye_size, src.frame_depth, src.frame_material, src.lens_type, src.lens_refractive_index, src.lens_stock_grind, src.lens_size, src.lens_segment_size, src.internal_sku, src.distributor);


-- product_gnm, storeproduct (storeid,productid)


select queue, count(*) from gnm.job group by queue


---------------------------------------------------------------------------------------------------
---- Add reference back to GNM product

ALTER TABLE public.storeproduct ADD COLUMN product_gnm_sk int;

select count(*) from 
(select sp.storeid,count(*) from public.product_gnm pg right join public.storeproduct sp on pg.name = sp.description 
where
	pg.sk is null group by sp.storeid) x

select * from public.storeproduct sp where storeid = '7000-013'

drop table tmp_storeprod1;

select sk,substring(productid,13, length(productid)) as pname,productid,storeid,"name",description,internal_sku 
into tmp_storeprod1
from public.storeproduct 


select sp.internal_sku,pg.* from storeproduct sp left join  product_gnm pg on pg.productid = sp.internal_sku where pg.sk is not null

select count(*) from storeproduct sp left join  product_gnm pg on pg.productid = sp.internal_sku where pg.sk is not null
select count(*) from storeproduct sp left join  product_gnm pg on pg.productid = sp.internal_sku where pg.sk is  null



select count(*) from 
(select distinct * from tmp_storeprod1 sp1 left join public.product_gnm pg on pg.name = sp1.pname
where pg.sk is not null) x

select count(*) from public.product_gnm pg right join public.storeproduct sp on pg.name = sp.description  sp1 

// ------------------------------------------------------

begin; 
update public.storeproduct as sp set product_gnm_sk = x.gnmsk from
(select sp.sk, pg.sk as gnmsk from storeproduct sp left join  product_gnm pg on pg.productid = sp.internal_sku where pg.sk is not null) x 
where x.sk = sp.sk

commit;


select count(*) from  public.storeproduct sp  where product_gnm_sk is not null

-- rollback;
select storeid,count(*) from storeproduct sp where product_gnm_sk is not null group by storeid


/*
 * stock_movements
 * orders
 * job_history - view of get last state
 * 
 * join invoice to order - by day(+- 1,2 or Monday/Friday) time dimension may need day of week then, storeproductid (storeid,productid), storeid, customerid
 * 
 */

select count(*) from store_stockmovement where adjustmenttype like 'Receipt'


select count(*) from store_stockmovement where adjustmenttype like 'Receipt'

select count(*)

select * from adjustment_type

update store_stockmovement set adjustment_type_sk = 305 where adjustmenttype like 'Receipt'


drop table temp_stock_price_history;

create temporary table temp_stock_price_history("store_sk" numeric,"takeon_storeproduct_sk" numeric,"product_gnm_sk" numeric,"supplier_cost" numeric,
"supplier_cost_gst" numeric,"actual_cost" numeric,"actual_cost_gst" numeric,"supplier_rrp" numeric,"supplier_rrp_gst" numeric,"retail_price" numeric,
"retail_price_gst" numeric,"source_id" varchar,"created" timestamp without time zone default (now() at time zone 'utc'));

insert into temp_stock_price_history("store_sk","takeon_storeproduct_sk","product_gnm_sk",
"supplier_cost","supplier_cost_gst","actual_cost","actual_cost_gst","supplier_rrp","supplier_rrp_gst","retail_price","retail_price_gst","source_id","created") 
values (0,0,0,0,0,85,8.5,0,0,217,0,'-PF-MAK000009',now())
,(0,0,0,0,0,150,15,0,0,363,0,'-PF-TAM000005',''),(0,0,0,0,0,65,6.5,0,0,174,0,'-PF-MAK000014',''),
(0,0,0,0,0,85,8.5,0,0,231,0,'-PF-APL000003',''),(0,0,0,0,0,105,10.5,0,0,261,0,'-PF-MAK000011',''),(0,0,0,0,0,65,6.5,0,0,174,0,'-PF-MAK000017',''),
(0,0,0,0,0,245,24.5,0,0,570,0,'-PF-EYR000001',''),(0,0,0,0,0,150,15,0,0,352,0,'-PF-TAM000027',''),(0,0,0,0,0,145,14.5,0,0,341,0,'-PF-TAM000016',''),
(0,0,0,0,0,90,9,0,0,231,0,'-PF-LUX000002',''),(0,0,0,0,0,70,7,0,0,0,0,'-PF-MAK000022',''),
(23,1904587,0,0,0,121.5,12.15,0,0,275,0,'2112-028-PF-LU0004907','1900-01-01 12:00:00'),(0,0,0,0,0,110,11,0,0,289,0,'-PF-MYO000003',''),
(0,0,0,0,0,80,8,0,0,209,0,'-PF-MAT000005',''),(0,0,0,0,0,65,6.5,0,0,174,0,'-PF-MAK000006',''),(0,0,0,0,0,208,20.8,0,0,480,0,'-PF-EYM000003',''),
(0,0,0,0,0,125,12.5,0,0,308,0,'-PF-HEA000001',''),(0,0,0,0,0,135,13.5,0,0,319,0,'-PF-TAM000011',''),(0,0,0,0,0,135,13.5,0,0,319,0,'-PF-TAM000010',''),
(0,0,0,0,0,120,12,0,0,317,0,'-PF-MAK000025',''),(0,0,0,0,0,163,16.3,0,0,392,0,'-PF-TAM000022',''),(0,0,0,0,0,75,7.5,0,0,198,0,'-PF-WEL000004',''),
(0,0,0,0,0,175,17.5,0,0,414,0,'-PF-APL000002',''),(0,0,0,0,0,75,7.5,0,0,187,0,'-PF-MAK000003',''),(0,0,0,0,0,119,11.9,0,0,295,0,'-PF-OAK000001',''),
(0,0,0,0,0,220,22,0,0,517,0,'-PF-APL000001',''),(0,0,0,0,0,65,6.5,0,0,174,0,'-PF-MAK000016',''),(0,0,0,0,0,150,15,0,0,352,0,'-PF-TAM000026',''),
(0,0,0,0,0,228,22.8,0,0,535,0,'-PF-EYM000001',''),(0,0,0,0,0,150,15,0,0,352,0,'-PF-TAM000009',''),(0,0,0,0,0,60,6,0,0,165,0,'-PF-MAT000001',''),
(0,0,0,0,0,150,15,0,0,363,0,'-PF-TAM000004',''),(0,0,0,0,0,60,6,0,0,176,0,'-PF-WEL000003',''),(0,0,0,0,0,140,14,0,0,330,0,'-PF-TAM000023',''),
(0,0,0,0,0,208,20.8,0,0,480,0,'-PF-EYM000002',''),(0,0,0,0,0,150,15,0,0,363,0,'-PF-TAM000012',''),
(23,1851775,0,0,0,146.76,14.68,0,0,380,0,'2112-028-PF-LU0004906','1900-01-01 12:00:00'),(0,0,0,0,0,155,15.5,0,0,374,0,'-PF-TAM000001',''),
(0,0,0,0,0,65,6.5,0,0,174,0,'-PF-MAK000005',''),(0,0,0,0,0,252,25.2,0,0,588,0,'-PF-EYM000006',''),(0,0,0,0,0,140,14,0,0,330,0,'-PF-TAM000015',''),
(0,0,0,0,0,92,9.2,0,0,239,0,'-PF-MAT000004',''),(0,0,0,0,0,90,9,0,0,119,0,'-PF-LXL000001',''),(0,0,0,0,0,155,15.5,0,0,374,0,'-PF-EYR000003',''),
(0,0,0,0,0,60,6,0,0,165,0,'-PF-MAT000002',''),(0,0,0,0,0,110,11,0,0,281,0,'-PF-MAK000026',''),(0,0,0,0,0,140,14,0,0,341,0,'-PF-HEA000007',''),
(0,0,0,0,0,208,20.8,0,0,480,0,'-PF-EYM000005',''),(0,0,0,0,0,95,9.5,0,0,248,0,'-PF-MAK000015',''),(0,0,0,0,0,155,15.5,0,0,363,0,'-PF-TAM000019',''),
(23,1849101,0,0,0,133.29,13.33,0,0,330,0,'2112-028-PF-LU0004905','1900-01-01 12:00:00'),(0,0,0,0,0,135,13.5,0,0,319,0,'-PF-TAM000007',''),
(0,0,0,0,0,155,15.5,0,0,363,0,'-PF-TAM000014',''),(0,0,0,0,0,99,9.9,0,0,256,0,'-PF-MAK000008',''),(0,0,0,0,0,145,14.5,0,0,341,0,'-PF-TAM000025',''),
(0,0,0,0,0,110,11,0,0,275,0,'-PF-HEA000003',''),(0,0,0,0,0,135,13.5,0,0,319,0,'-PF-TAM000008',''),(0,0,0,0,0,55,5.5,0,0,155,0,'-PF-LAM000001',''),
(0,0,0,0,0,110,11,0,0,281,0,'-PF-MYO000001',''),(0,0,0,0,0,105,10.5,0,0,286,0,'-PF-MAK000023',''),(0,0,0,0,0,70,7,0,0,187,0,'-PF-MAK000012',''),(0,0,0,0,0,120,12,0,0,298,0,'-PF-TAM000003',''),(0,0,0,0,0,135,13.5,0,0,319,0,'-PF-TAM000018',''),(0,0,0,0,0,120,12,0,0,298,0,'-PF-TAM000002',''),(0,0,0,0,0,155,15.5,0,0,374,0,'-PF-TAM000029',''),(0,0,0,0,0,75,7.5,0,0,187,0,'-PF-MAK000001',''),(0,0,0,0,0,90,9,0,0,99,0,'-PF-MAT000007',''),(0,0,0,0,0,140,14,0,0,341,0,'-PF-PRI000002',''),(0,0,0,0,0,163,16.3,0,0,392,0,'-PF-TAM000024',''),(0,0,0,0,0,75,7.5,0,0,176,0,'-PF-WEL000002',''),(0,0,0,0,0,75,7.5,0,0,196,0,'-PF-MAK000020',''),(0,0,0,0,0,150,15,0,0,368,0,'-PF-PRI000001',''),(0,0,0,0,0,144.55,14.45,0,0,330,0,'-PF-TAM000013',''),(0,0,0,0,0,185,18.5,0,0,440,0,'-PF-MAK000019',''),(0,0,0,0,0,75,7.5,0,0,198,0,'-PF-000000001',''),(0,0,0,0,0,145,14.5,0,0,341,0,'-PF-TAM000020',''),(0,0,0,0,0,85,8.5,0,0,220,0,'-PF-MAT000003',''),(0,0,0,0,0,135,13.5,0,0,330,0,'-PF-HEA000006',''),(0,0,0,0,0,140,14,0,0,330,0,'-PF-TAM000021',''),(23,1849052,0,0,0,187,18.7,0,0,300,0,'2112-028-PF-LU0004904','1900-01-01 12:00:00'),(0,0,0,0,0,175,17.5,0,0,418,0,'-PF-MAK000024',''),(0,0,0,0,0,65,6.5,0,0,174,0,'-PF-MAK000013',''),(0,0,0,0,0,65,6.5,0,0,174,0,'-PF-MAK000018',''),(0,0,0,0,0,85,8.5,0,0,219,0,'-PF-MAT000006',''),(0,0,0,0,0,140,14,0,0,341,0,'-PF-PRI000004',''),(0,0,0,0,0,176,17.6,0,0,420,0,'-PF-LUX000001',''),(0,0,0,0,0,155,15.5,0,0,374,0,'-PF-TAM000028',''),(0,0,0,0,0,225,22.5,0,0,540,0,'-PF-EYR000002',''),(0,0,0,0,0,160,16,0,0,385,0,'-PF-TAM000017',''),(0,0,0,0,0,65,6.5,0,0,174,0,'-PF-MAK000021',''),(0,0,0,0,0,115,11.5,0,0,286,0,'-PF-PRI000003',''),(0,0,0,0,0,75,7.5,0,0,176,0,'-PF-WEL000001',''),(0,0,0,0,0,135,13.5,0,0,336,0,'-PF-HEA000002',''),(0,0,0,0,0,115,11.5,0,0,297,0,'-PF-MYO000002',''),(0,0,0,0,0,95,9.5,0,0,240,0,'-PF-MAK000007',''),(0,0,0,0,0,135,13.5,0,0,319,0,'-PF-TAM000006',''),
(0,0,0,0,0,208,20.8,0,0,480,0,'-PF-EYM000004',''),(0,0,0,0,0,85,8.5,0,0,217,0,'-PF-MAK000010',''),(0,0,0,0,0,125,12.5,0,0,308,0,'-PF-HEA000005',''),
(0,0,0,0,0,75,7.5,0,0,187,0,'-PF-MAK000002','');

insert into public.stock_price_history("store_sk","takeon_storeproduct_sk","product_gnm_sk","supplier_cost","supplier_cost_gst","actual_cost",
"actual_cost_gst","supplier_rrp","supplier_rrp_gst","retail_price","retail_price_gst","source_id","created")
select "store_sk","takeon_storeproduct_sk",
"product_gnm_sk","supplier_cost","supplier_cost_gst","actual_cost","actual_cost_gst","supplier_rrp","supplier_rrp_gst","retail_price","retail_price_gst",
"source_id",to_timestamp("created") from temp_stock_price_history where not exists (select 1 from public.stock_price_history 
where public.stock_price_history.source_id = temp_stock_price_history.source_id);
update public.stock_price_history as td set "store_sk" = ts."store_sk","takeon_storeproduct_sk" = ts."takeon_storeproduct_sk","product_gnm_sk" = ts."product_gnm_sk",
"supplier_cost" = ts."supplier_cost","supplier_cost_gst" = ts."supplier_cost_gst","actual_cost" = ts."actual_cost","actual_cost_gst" = ts."actual_cost_gst",
"supplier_rrp" = ts."supplier_rrp","supplier_rrp_gst" = ts."supplier_rrp_gst","retail_price" = ts."retail_price","retail_price_gst" = ts."retail_price_gst",
"source_id" = ts."source_id","created" = to_timestamp(ts."created") from temp_stock_price_history ts 
where td.source_id = ts.source_id AND (td."store_sk" <> ts."store_sk" OR (td."store_sk" is null AND ts."store_sk" is not null) 
OR (td."store_sk" is not null AND ts."store_sk" is null) OR td."takeon_storeproduct_sk" <> ts."takeon_storeproduct_sk" OR 
(td."takeon_storeproduct_sk" is null AND ts."takeon_storeproduct_sk" is not null) OR (td."takeon_storeproduct_sk" is not null AND ts."takeon_storeproduct_sk" is null) 
OR td."product_gnm_sk" <> ts."product_gnm_sk" OR (td."product_gnm_sk" is null AND ts."product_gnm_sk" is not null) OR (td."product_gnm_sk" is not null AND ts."product_gnm_sk" is null) OR td."supplier_cost" <> ts."supplier_cost" OR (td."supplier_cost" is null AND ts."supplier_cost" is not null) OR (td."supplier_cost" is not null AND ts."supplier_cost" is null) OR td."supplier_cost_gst" <> ts."supplier_cost_gst" OR (td."supplier_cost_gst" is null AND ts."supplier_cost_gst" is not null) OR (td."supplier_cost_gst" is not null AND ts."supplier_cost_gst" is null) OR td."actual_cost" <> ts."actual_cost" OR (td."actual_cost" is null AND ts."actual_cost" is not null) OR (td."actual_cost" is not null AND ts."actual_cost" is null) OR td."actual_cost_gst" <> ts."actual_cost_gst" OR (td."actual_cost_gst" is null AND ts."actual_cost_gst" is not null) OR (td."actual_cost_gst" is not null AND ts."actual_cost_gst" is null) OR td."supplier_rrp" <> ts."supplier_rrp" OR (td."supplier_rrp" is null AND ts."supplier_rrp" is not null) OR (td."supplier_rrp" is not null AND ts."supplier_rrp" is null) OR td."supplier_rrp_gst" <> ts."supplier_rrp_gst" OR (td."supplier_rrp_gst" is null AND ts."supplier_rrp_gst" is not null) OR (td."supplier_rrp_gst" is not null AND ts."supplier_rrp_gst" is null) OR td."retail_price" <> ts."retail_price" OR (td."retail_price" is null AND ts."retail_price" is not null) OR (td."retail_price" is not null AND ts."retail_price" is null) OR td."retail_price_gst" <> ts."retail_price_gst" OR (td."retail_price_gst" is null AND ts."retail_price_gst" is not null) OR (td."retail_price_gst" is not null AND ts."retail_price_gst" is null) OR td."source_id" <> ts."source_id" OR (td."source_id" is null AND ts."source_id" is not null) OR (td."source_id" is not null AND ts."source_id" is null) OR td."created" <> ts."created" OR 
(td."created" is null AND ts."created" is not null) OR (td."created" is not null AND ts."created" is null))



select count(*) from stock_price_history sp 
where takeon_storeproduct_sk <> 0
limit 100

alter table stock_price_history rename column "created_orig" to "created";

alter table stock_price_history add column "created" varchar(100);


update stock_price_history set created = created_orig




insert into public.stock_price_history(
"store_sk",
"takeon_storeproduct_sk",
"product_gnm_sk",
"supplier_cost","supplier_cost_gst","actual_cost",
"actual_cost_gst","supplier_rrp","supplier_rrp_gst",
"retail_price","retail_price_gst",
"source_id","created")


      term = """("record",
                  this.store_sk,
                  this.takeon_storeproduct_sk,
                  "product_gnm_sk" , 0,
                  "supplier_cost" , 0,
                  "supplier_cost_gst" , 0,
                  "actual_cost" , numeric(this.EXLISTPR),
                  "actual_cost_gst" , (numeric(this.LISTPRICE) - numeric(this.EXLISTPR)),
                  "supplier_rrp" , 0,
                  "supplier_rrp_gst" , 0,
                  "retail_price" , numeric(this.RRP),
                  "retail_price_gst" , 0,
                  "source_id" , (this.storeid + "-PF-" + this.FRAMENUM),
                  "created" , this.acquisition_date
                )"""

drop table temp_stock_p


rice_history;
                  
                  
create  table temp_stock_price_history("store_sk" decimal,"takeon_storeproduct_sk" decimal,"product_gnm_sk" decimal,"supplier_cost" decimal,
"supplier_cost_gst" decimal,"actual_cost" decimal,"actual_cost_gst" decimal,"supplier_rrp" decimal,"supplier_rrp_gst" decimal,"retail_price" decimal,
"retail_price_gst" decimal,"source_id" varchar,"created" timestamp without time zone default (now() at time zone 'utc'));


insert into temp_stock_price_history(
	"store_sk",
	"takeon_storeproduct_sk",
	"product_gnm_sk",
	
	"supplier_cost",
	"supplier_cost_gst",
	"actual_cost",
	"actual_cost_gst",
	"supplier_rrp",
	"supplier_rrp_gst",
	"retail_price",
	"retail_price_gst",
	
	"source_id",
	"created") 
select
   coalesce(s.sk,0) as store_sk,
   coalesce(p.sk,0) as takeon_storeproduct_sk,
   p.product_gnm_sk ,
   
   0 as supplier_cost,
   0 as supplier_cost_gst,
           to_number("LISTPRICE",'999999D9') as actual_cost,
            to_number("EXLISTPR",'999999D9') as actual_cost_gst ,
            
            to_number("COSTPRICE" ,'999999D9')as supplier_rrp,
            to_number("EXCOSTPR" ,'999999D9')as supplier_rrp_gst,
            
            to_number("RRP",'999999D9') as retail_price,
            to_number("RRP" ,'999999D9') as retail_price_gst,
            
            s.storeid || '-PF-' || "FRAMENUM" as source_id,
            s.acquisition_date as created
into 
	temp_ssh
from stage.today_gnm_sunix_vframe f
       left outer join public.store s on (s.store_nk = f.source)
       left outer join public.storeproduct p on (p.productid = (s.storeid || '-PF-' || "FRAMENUM"))
where "SUPPLIER" <> 'GNM'
            
            
select
   coalesce(s.sk,0) as store_sk,
   coalesce(p.sk,0) as takeon_storeproduct_sk,
   p.product_gnm_sk ,
   
   0 as supplier_cost,
   0 as supplier_cost_gst,
"LISTPRICE" as actual_cost,
"EXLISTPR" as actual_cost_gst , 
"COSTPRICE" as supplier_rrp,
"EXCOSTPR" as supplier_rrp_gst,
            
"RRP" as retail_price,
"RRP" as retail_price_gst,
            
            s.storeid || '-PF-' || "FRAMENUM" as source_id,
            s.acquisition_date as created
into 
	temp_ssh
from stage.today_gnm_sunix_vframe f
       left outer join public.store s on (s.store_nk = f.source)
       left outer join public.storeproduct p on (p.productid = (s.storeid || '-PF-' || "FRAMENUM"))
where "SUPPLIER" <> 'GNM'


select actual_cost,,actual_cost ~ '^[0-9\.]+$' from temp_ssh where (actual_cost ~ '^[0-9\.]+$') = true

select count(*) from store_stockmovement

select count(*) from store_stockmovement where adjustment_type_sk <> 0


select sk,
          adjustmentdate,
          store_sk,
          product_gnm_sk,
          adjustment,
          actual_cost::numeric(19,2),
          (adjustment * actual_cost)::numeric as actual_cost_value,
          adjustment as moving_quantity,
          (adjustment * actual_cost)::numeric as moving_actual_cost_value,
          actual_cost::numeric(19,2) as moving_cost_per_unit,
          (select im.sk from public.store_stockmovement im
          where im.product_gnm_sk = Y.product_gnm_sk and im.store_sk = Y.store_sk and
          ((im.adjustmentdate = Y.adjustmentdate and im.sk > Y.sk) or im.adjustmentdate > Y.adjustmentdate)
          order by adjustmentdate,im.sk limit 1
          )
          from (
          select sk,adjustmentdate,store_sk,product_gnm_sk,adjustment,gnm_actual_cost as actual_cost,
          row_number() over (partition by store_sk,product_gnm_sk order by adjustmentdate,sk ) r from (
          select m.sk,m.adjustmentdate,m.store_sk,m.product_gnm_sk,m.adjustment,

          (select actual_cost from public.stock_price_history h
                        where (h.product_gnm_sk = m.product_gnm_sk) AND h.created <= m.adjustmentdate
                        order by created desc limit 1) as gnm_actual_cost
          from public.store_stockmovement m
          --inner join public.adjustment_type a on (m.adjustment_type_sk = a.sk and a.name = 'Receipt')
          where product_gnm_sk <> 0
          ) T where T.gnm_actual_cost is not null
          ) Y where r = 1

  
select adjustmenttype,adjustment_type_sk,count(*) from public.store_stockmovement group by adjustmenttype,adjustment_type_sk order by count(*) desc

select sk,adjustmentdate,store_sk,product_gnm_sk,adjustment,gnm_actual_cost as actual_cost,
          row_number() over (partition by store_sk,product_gnm_sk order by adjustmentdate,sk ) r from (
          select m.sk,m.adjustmentdate,m.store_sk,m.product_gnm_sk,m.adjustment,

          (select actual_cost from public.stock_price_history h
                        where (h.product_gnm_sk = m.product_gnm_sk) AND h.created <= m.adjustmentdate
                        order by created desc limit 1) as gnm_actual_cost
          from public.store_stockmovement m
          --inner join public.adjustment_type a on (m.adjustment_type_sk = a.sk and a.name = 'Receipt')
          where product_gnm_sk <> 0) x

          select * from adjustment_type
  
          begin ;
          update store_stockmovement set adjustment_type_sk = 1 where adjustment_type_sk = 99
          
          
          commit;
          
/*
 * 
 *           
          1. fix the adjustment type -- 
          2. confirm the average calculation
          3. script the remaining pieces using datapipes
               
 */
          
insert into adjustment_type values (adjustment_typeid,name,othername) values ("Restock", "Receipt", "Receipt")
insert into adjustment_type values (adjustment_typeid,name,othername) values ("RESTOCK" , "Receipt", "Receipt")
insert into adjustment_type values (adjustment_typeid,name,othername) values ("RE" , "Return", "Return")
insert into adjustment_type values (adjustment_typeid,name,othername) values ("CO" , "Return","Return")
insert into adjustment_type values (adjustment_typeid,name,othername) values ("ST" , "Stocktake","Stocktake")
insert into adjustment_type values (adjustment_typeid,name,othername) values ("CREDIT" , "Return","Return")
insert into adjustment_type values (adjustment_typeid,name,othername) values ("REC" , "Return","Return")
insert into adjustment_type values (adjustment_typeid,name,othername) values ("RTS" , "Return","Return")
insert into adjustment_type values (adjustment_typeid,name,othername) values ("Sale Cancel" , "Sale","Sale")
insert into adjustment_type values (adjustment_typeid,name,othername) values ("Cancel Sale" , "Sale","Sale")
insert into adjustment_type values (adjustment_typeid,name,othername) values ("Sale - CO:DE" , "Sale", "Sale")
insert into adjustment_type values (adjustment_typeid,name,othername) values ("Sale - SA:DS" , "Sale","Sale")
insert into adjustment_type values (adjustment_typeid,name,othername) values ("Sale - PO:DS" , "Sale","Sale")
insert into adjustment_type values (adjustment_typeid,name,othername) values ("Sale Cancel - PO:DS" , "Sale","Sale")
insert into adjustment_type values (adjustment_typeid,name,othername) values ("Sale Cancel - SA:DS" , "Sale", "Sale")
insert into adjustment_type values (adjustment_typeid,name,othername) values ("CASH SALE" , "Sale","Sale")
insert into adjustment_type values (adjustment_typeid,name,othername) values ("CITY" , "Transferout","Transferout")
insert into adjustment_type values (adjustment_typeid,name,othername) values ("BA" , "Transferout","Transferout")
insert into adjustment_type values (adjustment_typeid,name,othername) values ("TLIVO" , "Transferout", "Transferout")
insert into adjustment_type values (adjustment_typeid,name,othername) values ("TO" , "Transferout", "Transferout")
insert into adjustment_type values (adjustment_typeid,name,othername) values ("TOP" , "Transferout","Transferout")
insert into adjustment_type values (adjustment_typeid,name,othername) values ("TRANS" , "Transferout","Transferout")
insert into adjustment_type values (adjustment_typeid,name,othername) values ("TTR" , "Transferout", "Transferout")
insert into adjustment_type values (adjustment_typeid,name,othername) values ("PDB" , "Transferin","Transferin")
insert into adjustment_type values (adjustment_typeid,name,othername) values ("TFLIV" , "Transferin", "Transferin")
insert into adjustment_type values (adjustment_typeid,name,othername) values ("TI" , "Transferin","Transferin")
insert into adjustment_type values (adjustment_typeid,name,othername) values ("TOPB" , "Transferin","Transferin")
insert into adjustment_type values (adjustment_typeid,name,othername) values ("TH" , "Theft", "Theft")
insert into adjustment_type values (adjustment_typeid,name,othername) values ("WO" , "Writeoff","Writeoff")


truncate store_stockmovement;
truncate adjustment_type;


alter table store_stockmovement drop column adjustmenttype;

 drop view v_cogs_first_receipt;
 
CREATE VIEW v_cogs_first_receipt as

 SELECT y.sk,
    y.adjustmentdate,
    y.product_gnm_sk,
    y.adjustment,
    y.actual_cost,
    ((y.adjustment)::numeric * y.actual_cost) AS actual_cost_value,
    ((y.adjustment)::numeric * y.actual_cost) AS moving_actual_cost_value,
    y.actual_cost AS moving_cost_per_unit,
    ( SELECT im.sk
           FROM store_stockmovement im
          WHERE ((im.product_gnm_sk = y.product_gnm_sk) AND (im.adjustmentdate >= y.adjustmentdate) AND (im.sk <> y.sk))
          ORDER BY im.adjustmentdate, im.sk
         LIMIT 1) AS n1
   FROM ( SELECT t.sk,
            t.store_sk,
            t.source_id,
            t.takeon_storeproduct_sk,
            t.adjustmentdate,
            t.stock_price_history_sk,
            t.adjustment,
            t.reason,
            t.invoice_sk,
            t.customer_sk,
            t.avg_cost_price,
            t.shipping_ref,
            t.shipping_operator,
            t.comments,
            t.employee_sk,
            t.created,
            t.adjustmentdate_sk,
            t.product_gnm_sk,
            t.adjustment_type_sk,
            t.actual_cost,
            row_number() OVER (PARTITION BY t.product_gnm_sk ORDER BY t.adjustmentdate, t.sk) AS r
           FROM ( SELECT m.sk,
                    m.store_sk,
                    m.source_id,
                    m.takeon_storeproduct_sk,
                    m.adjustmentdate,
                    m.stock_price_history_sk,
                    m.adjustment,
                    m.reason,
                    m.invoice_sk,
                    m.customer_sk,
                    m.avg_cost_price,
                    m.shipping_ref,
                    m.shipping_operator,
                    m.comments,
                    m.employee_sk,
                    m.created,
                    m.adjustmentdate_sk,
                    m.product_gnm_sk,
                    m.adjustment_type_sk,
                    ( SELECT h.actual_cost
                           FROM stock_price_history h
                          WHERE ((h.product_gnm_sk = m.product_gnm_sk) AND (h.created <= m.adjustmentdate))
                          ORDER BY h.created DESC
                         LIMIT 1) AS actual_cost
                   FROM (store_stockmovement m
                     JOIN adjustment_type a ON (((m.adjustment_type_sk = a.sk) AND ((a.adjustment_typeid)::text = 'RESTOCK'::text))))
                  WHERE (m.product_gnm_sk <> 0)) t
          WHERE (t.actual_cost IS NOT NULL)) y
  WHERE (y.r = 1);


select * from store_stockmovement ssm inner join adjustment_type at on at.sk = ssm.adjustment_type_sk


  
select adjustment_type_sk,count(*) from public.store_stockmovement group by adjustmenttype,adjustment_type_sk order by count(*) desc

select * from adjustment_type



select * from  stage.today_gnm_sunix_vfrstake




---- Normalise the Events by topology
-- current topology/dimensions
-- dimbucket == sk, year, doy, store, product

-- dimbucket, event, store_stockmenvet_sk, adjustmenttype
---dimbucket, event, invoice, pos
---dimbucket, event, orders + ordertype + jobs + job status

-- Matched events / Matched Events
--- source bucket for event,dimbucket, matching event invoice, matching event order, matching event stockmovment

-- UnMatched events
--- events in bucket not in Matched events


---- Create Dim Bucket
--- year + doy + gnm_product_sk + takeon_storeproduct + store_sk

date_part('year'::text, sm.source_created) AS year,
    date_part('doy'::text, sm.source_created) AS doy,
    date_part('month'::text, sm.source_created) AS month,
    date_part('dow'::text, sm.source_created) AS dow,
    date_part('day'::text, sm.source_created) AS day,
    sm.sk
   FROM order_history sm
  GROUP BY (date_part('year'::text, sm.source_created)), (date_part('doy'::text, sm.source_created)), (date_part('month'::text, sm.source_created)), (date_part('dow'::text, sm.source_created)), (date_part('day'::text, sm.source_created)), sm.sk;

create table daily_store_bucket (
	sk serial primary key not null,
	"year" integer not null,
	"doy"  integer not null,
	store_sk integer not null references store(sk),
	product_gnm_sk integer not null references product_gnm(sk),
	storeproduct_sk integer not null references storeproduct(sk)
);

-- enforce an uniquenss index

-- year + day + store + storeproduct + product_gnm + sk

--- Create From InvoicesItems
insert into daily_store_bucket ("year","doy",store_sk,product_gnm_sk,storeproduct_sk)
select (date_part('year'::text, sm.createddate)), (date_part('doy'::text, sm.createddate)), storesk, coalesce(sp.product_gnm_sk,0), productsk 
from invoiceitem sm 
inner join storeproduct sp on sp.sk = sm.productsk
left join daily_store_bucket dsb on dsb.year = (date_part('year'::text, sm.createddate)) and doy= (date_part('doy'::text, sm.createddate))

group by (date_part('year'::text, sm.createddate)), (date_part('doy'::text, sm.createddate)), storesk, productsk, sp.product_gnm_sk

-- Create From Stockmovements
insert into daily_store_bucket ("year","doy",store_sk,product_gnm_sk,storeproduct_sk)
select (date_part('year'::text, sm.createddate)), (date_part('doy'::text, sm.createddate)), storesk, productsk 
from store_stockmovement sm
group by (date_part('year'::text, sm.createddate)), (date_part('doy'::text, sm.createddate)), storesk, productsk

-- Create From Orders
insert into daily_store_bucket ("year","doy",store_sk,product_gnm_sk,storeproduct_sk)
select (date_part('year'::text, sm.source_created)), (date_part('doy'::text, sm.source_created)), oh.storesk, sm.productsk 
from job_history sm inner join order_history oh on sm.order_sk = oh.sk
group by (date_part('year'::text, sm.source_created)), (date_part('doy'::text, sm.source_created)), oh.storesk, sm.productsk

-- go back and update product_gnm for storeproducts where possible

-- create invoice bucket
create table 
daily_invoice_bucket (
	sk not null,
	daily_store_bucket_sk not null,
	invoice_sk ,
	pos
)

-- create stockmovement bucket
create table 
daily_stockmovement_bucket (
	sk not null,
	daily_store_bucket_sk not null,
	store_stockmovement_sk
)

-- create order bucket
create table 
daily_job_bucket (
	sk not null,
	daily_store_bucket_sk not null,
	job_sk
)


---- Create Matched event bucket
create table 
daily_matched_bucket (
	sk not null,
	invoice_daily_store_bucket_sk not null,
	job_daily_store_bucket_sk not null,
	stockmove_daily_store_bucket_sk not null,
	invoice_sk ,
	pos,
	job_sk,
	store_stockmovement_sk,
	confidence,
)

-- Unmatched View

select * from daily_invoice_bucket  dib
left join daily_matched_bucket dmb on where
union all
select * from daily_invoice_bucket  dib
left join daily_matched_bucket dmb on where 
union all
select * from daily_invoice_bucket  dib
left join daily_matched_bucket dmb on where

begin;
update adjustment_type as at2 set name = 'Sale'
from adjustment_type at1 
where at1.name = 'Sale' and upper(at1.adjustment_typeid) = at2.adjustment_typeid	

-- rollback;
commit;

select * from adjustment_type

truncate table adjustment_type;

begin;
update adjustment_type as at1 set name = 'Sale'
where at1.name <> 'Sale' and upper(at1.adjustment_typeid) like '%SALE%';


commit;

select adjustment_typeid, count(*) from adjustment_type group by adjustment_typeid;

select * from adjustment_type where upper(adjustment_typeid) like '%SALE%';


select * from stock_price_history where takeon_storeproduct_sk <> 0

select * from product_gnm

select * from storeproduct

select * from stock_price_history sph left join product_gnm pg on pg.sk = sph.product_gnm_sk
where pg.sk is null


SELECT 
0, id, 0, sp.sk, 
pch.supplier_cost, pch.supplier_cost_gst, 
pch.actual_cost, pch.actual_cost_gst, 
pch.supplier_rrp, pch.supplier_rrp_gst, pch.calculated_retail_price, pch.calculated_retail_price_gst, 
0,0,
0,0,
pch.modified_date_time
FROM gnm.product_cost_history pch 
left join product_gnm sp on sp.productid = pch.product_id
left join stock_price_history sph on sph.created = pch.modified_date_time
where sph.created is null;

select
            0 as store_sk,
            0 as takeon_storeproduct_sk,
            coalesce(p.sk,0) as product_gnm_sk,
            coalesce(supplier_cost,0) as supplier_cost,
            coalesce(supplier_cost_gst,0) as supplier_cost_gst,
            coalesce(actual_cost,0) as actual_cost,
            coalesce(actual_cost_gst,0) as actual_cost_gst,
            coalesce(supplier_rrp,0) as supplier_rrp,
            coalesce(supplier_rrp_gst,0) as supplier_rrp_gst,
            coalesce(retail_price,0) as retail_price,
            coalesce(retail_price_gst,0) as retail_price_gst,
            ('GNM-' || id) as source_id,
            effective_from_time as created
            from public.product_price_history h
            left outer join public.storeproduct p
            on (p.productid = h.product_id AND p.storeid = '0')
            
            
            
            select
            coalesce(s.sk,0) as store_sk,
            s.acquisition_date,
            coalesce(p.sk,0) as takeon_storeproduct_sk,
            "LISTPRICE",
            "EXLISTPR",
            "COSTPRICE",
            "EXCOSTPR",
            "RRP",
            s.storeid,
            "FRAMENUM"
            from stage.today_gnm_sunix_vframe f
            inner join public.store s on (s.store_nk = f.source)
            left outer join public.storeproduct p on (p.productid = (s.storeid || '-PF-' || "FRAMENUM"))
            where "SUPPLIER" <> 'GNM'
            
            
            
          select s.sk as store_sk,
          i.id,
          coalesce(tp.sk,0) as takeon_storeproduct_sk,
          coalesce(gp.sk,0) as product_gnm_sk,
          actual_qty,
          d.sk as adjustmentdate_sk,
          d.date_actual,
          a.sk as adjustment_type_sk
          from gnm.stocktake_item i
          inner join gnm.stocktake t on (i.stocktake_id = t.id)
          inner join gnm.store ts on (t.store_id = ts.id)
          left outer join public.store s on (s.store_nk = ts.store_id)
          inner join public.storeproduct tp on (tp.productid = (s.storeid || '-PF-' || pms_product_id))
          left outer join public.product_gnm gp on (tp.internal_sku <> '' AND gp.productid = tp.internal_sku)
          left outer join d_date d on (d.date_actual = '2017-06-30')
          inner join adjustment_type a on ('RECEIPT' = a.adjustment_typeid)
          
          
          --- less to do 
                      
           select "ID","NOTE","QTY_NEW","QTY_OLD","CODE","DATE","REASON","LOCATION",source,coalesce(s.sk,0) as store_sk,
          coalesce(tp.sk,0) as takeon_storeproduct_sk,
          coalesce(gp.sk,0) as product_gnm_sk,
          coalesce(j.invoicesk,0) as invoice_sk,
          coalesce(c.sk,0) as customer_sk,
          coalesce(
            (select sk from public.stock_price_history h
              where (h.product_gnm_sk = gp.sk AND gp.sk is not null) AND h.created <= ("DATE"::timestamp)
              order by created desc limit 1)
          ,
            (select sk from public.stock_price_history h
              where (h.takeon_storeproduct_sk = tp.sk) AND h.created <= ("DATE"::timestamp)
              order by created desc limit 1)
          ,0) as stock_price_history_sk,
          coalesce(d.sk,0) as adjustmentdate_sk,
          coalesce(a.sk,0) as adjustment_type_sk
          from
          stage.today_gnm_sunix_vrestock r
          left outer join public.store s on (s.store_nk = source)
          left outer join public.storeproduct tp on (tp.productid = (s.storeid || '-PF-' || "CODE"))
          left outer join public.product_gnm gp on (tp.internal_sku <> '' AND gp.productid = tp.internal_sku)
          left outer join public.spectaclejob j on (("REASON" = 'Sale' OR "REASON" = 'Cancel Sale') AND j.spectaclejobid = (s.storeid || '-' || substring("INVNO",5)))
          left outer join public.customer c on (c.customerid = (s.storeid || '-' || "REFNUM"))
          left outer join d_date d on (TO_CHAR((r."DATE"::timestamp),'yyyymmdd')::INT = d.date_dim_id)
          left outer join adjustment_type a on (upper(r."REASON") = a.adjustment_typeid)
          where ("DATE"::timestamp) >= '2017-07-01' AND ("DATE"::timestamp) >= s.acquisition_date AND
          ("DATE"::timestamp) <= now() AND "STOCKTYPE" = 'F'
          
          
           

select count(*) from store_stockmovement;






insert into prod_common(prod_ref_sk,product_gnm_sk,storeproduct_sk)

select pr.sk,pg.sk,sp.sk,pg.*,sp.*
-- sp.internal_sku,pg.productid,upper(sp.frame_brand),upper(sp.frame_model),upper(sp.frame_colour),
-- upper(pg.frame_brand),upper(pg.frame_model),upper(pg.frame_colour), pg.*
from storeproduct sp 
inner join product_gnm pg on pg.productid = sp.internal_sku 
-- left join prod_ref pr on pr.brand = upper(sp.frame_brand) and pr.model = upper(sp.frame_model) and pr.colour = upper(sp.frame_colour)

select pg.sk,sp.sk,pg.*,sp.*
-- sp.internal_sku,pg.productid,upper(sp.frame_brand),upper(sp.frame_model),upper(sp.frame_colour),
-- upper(pg.frame_brand),upper(pg.frame_model),upper(pg.frame_colour), pg.*
from storeproduct sp 
inner join product_gnm pg on pg.productid = sp.internal_sku 
-- left join prod_ref pr on pr.brand = upper(sp.frame_brand) and pr.model = upper(sp.frame_model) and pr.colour = upper(sp.frame_colour)









