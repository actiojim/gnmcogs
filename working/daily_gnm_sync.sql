SET search_path = stage;
 
drop table if exists order_stage;
 
create table order_stage
(
    sk serial NOT NULL,
	run_id character varying(100),
	   
    -- source data order id
    order_id character varying(100),
    
    cust_firstname character varying(100),
    cust_lastname character varying(100),
    cust_midname character varying(100),
    cust_phone character varying(100),
    store_id character varying(100),
    
    cust_sk int,
    order_type character varying(100),
    
    queue character varying(100),
    status character varying(100),
    
    source_created character varying(100),
    source_modified character varying(100),
    
    -- created date
    created timestamp without time zone NOT null default now(),
    primary key (sk)
);

-- 1. customer entries that are missing from customer table
INSERT INTO public.customer ( storesk, firstname, lastname, homephone, createddate, title, gender, email)
select  distinct  s.sk, os.cust_firstname, os.cust_lastname, os.cust_phone, now(), '', '', ''
from stage.order_stage os 
left join public.customer c on c.firstname = os.cust_firstname and c.lastname = os.cust_lastname  
-- and (os.cust_phone = c.homephone or os.cust_phone = c.mobile or os.cust_phone = c.workphone)
left join public.store s on s.storeid = os.store_id -- and c.storesk = s.sk
where
 c.sk is null;
 
 
-- stage orders
INSERT INTO order_stage
(run_id, order_id, 
cust_firstname, cust_lastname, cust_phone, 
cust_sk, store_id, 
order_type, 
queue, status, 
source_created, source_modified)
SELECT null , o.order_id, 
oc.first_name, oc.last_name, oc.telephone, 
c.sk, s.store_id, 
o.order_type_id,
o.queue, o.status, 
o.created_date_time, o.modified_date_time
FROM gnm."order" o 
inner join gnm.order_customer oc on oc.id = o.customer_id
inner join gnm."store" s on s.id = o.store_id
left outer join public.store ps on ps.storeid = s.store_id
left join public.customer c on c.firstname = oc.first_name and c.lastname = oc.last_name and c.storesk = ps.sk;


drop table if exists job_stage;
 
create table job_stage
(
    sk serial NOT NULL,
	run_id character varying(100),
	
    -- natural source key job_id
    job_id character varying(100),
    
    -- source data order id
    order_id character varying(100),
    product_id character varying(100),
    supplier_id character varying(100),
    quantity character varying(100),
    
    -- job type frame or lens
    job_type character varying(100),
    queue character varying(100),
    status character varying(100),
    source_created character varying(100),
    source_modified character varying(100),
    
    -- created date
    created timestamp without time zone NOT null default now(),
    primary key (sk)
);

insert into stage.job_stage (
	run_id,
	order_id,
	job_id,
	queue,
	status,
	quantity,
	source_created,
	source_modified,
	job_type,
	product_id,
	supplier_id)
select  
	null,
	j.order_id,
	j.id,
	j.queue,
	j.status,
	j.quantity,
	j.created_date_time,
	j.modified_date_time,
	j.job_type_id,
	j.product_id,
	j.supplier_id
from gnm.job j ;
