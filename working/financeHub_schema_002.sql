--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.3
-- Dumped by pg_dump version 9.6.3

-- Started on 2017-09-25 12:16:39

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

SET default_with_oids = false;

--
-- TOC entry 182 (class 1259 OID 50250)
-- Name: appointment; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE appointment (
    sk integer NOT NULL,
    appointmentid character varying(100),
    storesk integer NOT NULL,
    createddate timestamp without time zone NOT NULL,
    booking_startdate timestamp without time zone NOT NULL,
    booking_enddate timestamp without time zone NOT NULL,
    employeesk integer NOT NULL,
    customersk integer NOT NULL,
    statussk integer NOT NULL,
    appointment_typesk integer NOT NULL,
    mins integer,
    modified timestamp without time zone,
    prospect_firstname character varying(50),
    prospect_lastname character varying(50),
    prospect_title character varying(10),
    prospect_email character varying(150),
    prospect_homephone character varying(150),
    prospect_mobile character varying(150),
    prospect_workphone character varying(150),
    isdeleted boolean DEFAULT false NOT NULL
);


--
-- TOC entry 183 (class 1259 OID 50257)
-- Name: appointment_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE appointment_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2852 (class 0 OID 0)
-- Dependencies: 183
-- Name: appointment_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE appointment_sk_seq OWNED BY appointment.sk;


--
-- TOC entry 184 (class 1259 OID 50259)
-- Name: appointment_type; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE appointment_type (
    sk integer NOT NULL,
    appointment_typeid character varying(30) NOT NULL,
    name character varying(100) NOT NULL
);


--
-- TOC entry 185 (class 1259 OID 50262)
-- Name: appointment_type_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE appointment_type_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2853 (class 0 OID 0)
-- Dependencies: 185
-- Name: appointment_type_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE appointment_type_sk_seq OWNED BY appointment_type.sk;


--
-- TOC entry 186 (class 1259 OID 50264)
-- Name: consult; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE consult (
    sk integer NOT NULL,
    consultid character varying(100),
    consultdate timestamp without time zone NOT NULL,
    storesk integer NOT NULL,
    employeesk integer NOT NULL,
    customersk integer NOT NULL,
    modified timestamp without time zone NOT NULL
);


--
-- TOC entry 187 (class 1259 OID 50267)
-- Name: consult_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE consult_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2854 (class 0 OID 0)
-- Dependencies: 187
-- Name: consult_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE consult_sk_seq OWNED BY consult.sk;


--
-- TOC entry 188 (class 1259 OID 50269)
-- Name: consult_type; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE consult_type (
    sk integer NOT NULL,
    consult_typeid character varying(30) NOT NULL,
    description character varying(100) NOT NULL
);


--
-- TOC entry 189 (class 1259 OID 50272)
-- Name: consult_type_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE consult_type_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2855 (class 0 OID 0)
-- Dependencies: 189
-- Name: consult_type_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE consult_type_sk_seq OWNED BY consult_type.sk;


--
-- TOC entry 190 (class 1259 OID 50274)
-- Name: consultitem; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE consultitem (
    sk integer NOT NULL,
    consultsk integer NOT NULL,
    consult_typesk integer NOT NULL
);


--
-- TOC entry 191 (class 1259 OID 50277)
-- Name: consultitem_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE consultitem_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2856 (class 0 OID 0)
-- Dependencies: 191
-- Name: consultitem_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE consultitem_sk_seq OWNED BY consultitem.sk;


--
-- TOC entry 192 (class 1259 OID 50279)
-- Name: customer; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE customer (
    sk integer NOT NULL,
    createddate timestamp without time zone NOT NULL,
    customerid character varying(500),
    firstname character varying(50) NOT NULL,
    lastname character varying(50),
    middlename character varying(50),
    title character varying(10) NOT NULL,
    gender character varying(10) NOT NULL,
    birthdate date,
    preferredname character varying(50),
    preferredcontactmethod character varying(50),
    email character varying(150) NOT NULL,
    emailcreated timestamp without time zone DEFAULT ('now'::text)::date NOT NULL,
    phonecountryid integer,
    phonecreated timestamp without time zone DEFAULT ('now'::text)::date NOT NULL,
    address1 character varying(250),
    address2 character varying(250),
    address3 character varying(250),
    state character varying(60),
    suburb character varying(60),
    postcode integer,
    firstvisit timestamp without time zone,
    lastvisit timestamp without time zone,
    lastconsult timestamp without time zone,
    lastrecall timestamp without time zone,
    nextrecall timestamp without time zone,
    lastrecalldesc character varying(250),
    nextrecalldesc character varying(250),
    lastoptomseen integer,
    storesk integer DEFAULT 0 NOT NULL,
    healthfund character varying(50),
    modified timestamp without time zone,
    optout_recall boolean DEFAULT false NOT NULL,
    optout_marketing boolean DEFAULT false NOT NULL,
    deceased boolean DEFAULT false NOT NULL,
    homephone character varying(150),
    mobile character varying(150),
    workphone character varying(150),
    isaddrvalid boolean
);


--
-- TOC entry 193 (class 1259 OID 50291)
-- Name: customer_ext; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE customer_ext (
    sk integer NOT NULL,
    customersk integer NOT NULL,
    s_recall character varying(1),
    s_cmsms character varying(1),
    s_cmmobile character varying(1),
    s_cmwphone character varying(1),
    s_cmhphone character varying(1),
    s_cmemail character varying(1),
    s_cmletter character varying(1),
    s_cmfno boolean,
    s_cmnoemail boolean,
    s_apnotif integer,
    s_apreminder integer,
    o_pref_recallint character varying(20),
    o_pref_recallsub character varying(20),
    o_pref_ordercoll character varying(20),
    o_pref_appointments character varying(20),
    o_pref_marketing character varying(20),
    o_pref_telephone character varying(20),
    o_badaddress boolean,
    o_badphone boolean,
    o_bademail boolean,
    o_noemail boolean,
    modified timestamp without time zone,
    o_inactive boolean,
    o_inactive_reason character varying(255)
);


--
-- TOC entry 194 (class 1259 OID 50294)
-- Name: customer_ext_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE customer_ext_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2857 (class 0 OID 0)
-- Dependencies: 194
-- Name: customer_ext_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE customer_ext_sk_seq OWNED BY customer_ext.sk;


--
-- TOC entry 195 (class 1259 OID 50296)
-- Name: customer_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE customer_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2858 (class 0 OID 0)
-- Dependencies: 195
-- Name: customer_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE customer_sk_seq OWNED BY customer.sk;


--
-- TOC entry 196 (class 1259 OID 50298)
-- Name: discountcode; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE discountcode (
    sk integer NOT NULL,
    storeid character varying(12) DEFAULT 'All'::character varying NOT NULL,
    discountitemcode character varying(100) NOT NULL,
    description character varying(150),
    valid_from timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone,
    valid_to timestamp without time zone DEFAULT '2099-12-31 00:00:00'::timestamp without time zone
);


--
-- TOC entry 197 (class 1259 OID 50304)
-- Name: discountcode_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE discountcode_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2859 (class 0 OID 0)
-- Dependencies: 197
-- Name: discountcode_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE discountcode_sk_seq OWNED BY discountcode.sk;


--
-- TOC entry 198 (class 1259 OID 50306)
-- Name: employee; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE employee (
    sk integer NOT NULL,
    createddate timestamp without time zone NOT NULL,
    employeeid character varying(500),
    employeetype character varying(50),
    employmentstartdate date,
    employmentenddate date,
    firstname character varying(50) NOT NULL,
    lastname character varying(50),
    middlename character varying(50),
    title character varying(10) NOT NULL,
    gender character varying(10) NOT NULL,
    birthdate date,
    preferredname character varying(50),
    preferredcontactmethod character varying(10),
    email character varying(150) NOT NULL,
    emailcreated timestamp without time zone DEFAULT ('now'::text)::date NOT NULL,
    phonecountryid integer,
    phonenumber character varying(150),
    phonerawnumber character varying(150),
    phonemobile boolean NOT NULL,
    phonesmsenabled boolean NOT NULL,
    phonefax boolean NOT NULL,
    phonelandline boolean NOT NULL,
    phonecreated timestamp without time zone DEFAULT ('now'::text)::date NOT NULL,
    storesk integer DEFAULT 0 NOT NULL
);


--
-- TOC entry 199 (class 1259 OID 50315)
-- Name: employee_providerno; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE employee_providerno (
    sk integer NOT NULL,
    employeesk integer NOT NULL,
    storesk integer NOT NULL,
    providerno character varying(30),
    modified timestamp without time zone DEFAULT timezone('Australia/Sydney'::text, now()) NOT NULL
);


--
-- TOC entry 200 (class 1259 OID 50319)
-- Name: employee_providerno_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE employee_providerno_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2860 (class 0 OID 0)
-- Dependencies: 200
-- Name: employee_providerno_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE employee_providerno_sk_seq OWNED BY employee_providerno.sk;


--
-- TOC entry 201 (class 1259 OID 50321)
-- Name: employee_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE employee_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2861 (class 0 OID 0)
-- Dependencies: 201
-- Name: employee_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE employee_sk_seq OWNED BY employee.sk;


--
-- TOC entry 202 (class 1259 OID 50323)
-- Name: expense; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE expense (
    sk integer NOT NULL,
    expenseid character varying(100),
    storesk integer NOT NULL,
    createddate timestamp without time zone NOT NULL,
    employeesk integer NOT NULL,
    expense_typesk integer NOT NULL,
    description character varying(100),
    amount_incl_gst numeric(19,4),
    amount_gst numeric(19,4),
    modified timestamp without time zone
);


--
-- TOC entry 203 (class 1259 OID 50326)
-- Name: expense_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE expense_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2862 (class 0 OID 0)
-- Dependencies: 203
-- Name: expense_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE expense_sk_seq OWNED BY expense.sk;


--
-- TOC entry 204 (class 1259 OID 50328)
-- Name: expense_type; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE expense_type (
    sk integer NOT NULL,
    expense_typeid character varying(30) NOT NULL,
    name character varying(100) NOT NULL
);


--
-- TOC entry 205 (class 1259 OID 50331)
-- Name: expense_type_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE expense_type_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2863 (class 0 OID 0)
-- Dependencies: 205
-- Name: expense_type_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE expense_type_sk_seq OWNED BY expense_type.sk;


--
-- TOC entry 206 (class 1259 OID 50333)
-- Name: invoice; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE invoice (
    sk integer NOT NULL,
    invoiceid character varying(100),
    createddate timestamp without time zone NOT NULL,
    storesk integer NOT NULL,
    employeesk integer NOT NULL,
    customersk integer NOT NULL,
    amount_incl_gst numeric(19,4),
    modified timestamp without time zone,
    amount_gst numeric(19,4),
    reversalref character varying(100),
    rebateref character varying(100)
);


--
-- TOC entry 207 (class 1259 OID 50342)
-- Name: invoice_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE invoice_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2864 (class 0 OID 0)
-- Dependencies: 207
-- Name: invoice_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE invoice_sk_seq OWNED BY invoice.sk;


--
-- TOC entry 208 (class 1259 OID 50344)
-- Name: invoiceitem; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE invoiceitem (
    invoicesk integer NOT NULL,
    pos integer NOT NULL,
    productsk integer NOT NULL,
    storesk integer NOT NULL,
    employeesk integer NOT NULL,
    serviceemployeesk integer NOT NULL,
    customersk integer NOT NULL,
    createddate timestamp without time zone NOT NULL,
    quantity integer NOT NULL,
    amount_incl_gst numeric(19,4),
    modified timestamp without time zone,
    amount_gst numeric(19,4),
    discount_incl_gst numeric(19,4),
    discount_gst numeric(19,4),
    cost_incl_gst numeric(19,4),
    cost_gst numeric(19,4),
    description character varying(500),
    payeesk integer,
    invoiceitemid character varying(60) DEFAULT ''::character varying NOT NULL,
    return_incl_gst numeric(19,4),
    return_gst numeric(19,4),
    bulkbillref character varying(100)
);


--
-- TOC entry 209 (class 1259 OID 50351)
-- Name: job_process_status; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE job_process_status (
    sk integer NOT NULL,
    jobname character varying(255) NOT NULL,
    status character varying(10) NOT NULL,
    description character varying(255),
    lastexecution timestamp without time zone NOT NULL
);


--
-- TOC entry 210 (class 1259 OID 50357)
-- Name: job_process_status_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE job_process_status_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2865 (class 0 OID 0)
-- Dependencies: 210
-- Name: job_process_status_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE job_process_status_sk_seq OWNED BY job_process_status.sk;


--
-- TOC entry 211 (class 1259 OID 50377)
-- Name: payee; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE payee (
    sk integer NOT NULL,
    storeid character varying(12),
    payeeid character varying(100),
    name character varying(200),
    payeetype character varying(150),
    chainid character varying(100) DEFAULT ''::character varying NOT NULL
);


--
-- TOC entry 212 (class 1259 OID 50384)
-- Name: payee_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE payee_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2866 (class 0 OID 0)
-- Dependencies: 212
-- Name: payee_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE payee_sk_seq OWNED BY payee.sk;


--
-- TOC entry 213 (class 1259 OID 50386)
-- Name: payment; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE payment (
    sk integer NOT NULL,
    paymentid character varying(100),
    createddate timestamp without time zone NOT NULL,
    amount_incl_gst numeric(19,4),
    modified timestamp without time zone,
    amount_gst numeric(19,4),
    storeid character varying(100) DEFAULT ''::character varying,
    payeeid character varying(100) DEFAULT ''::character varying,
    employeeid character varying(100) DEFAULT ''::character varying,
    customerid character varying(100) DEFAULT ''::character varying,
    payment_typeid character varying(100) DEFAULT ''::character varying
);


--
-- TOC entry 214 (class 1259 OID 50397)
-- Name: payment_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE payment_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2867 (class 0 OID 0)
-- Dependencies: 214
-- Name: payment_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE payment_sk_seq OWNED BY payment.sk;


--
-- TOC entry 215 (class 1259 OID 50399)
-- Name: payment_type; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE payment_type (
    sk integer NOT NULL,
    payment_typeid character varying(30) NOT NULL,
    name character varying(100) NOT NULL
);


--
-- TOC entry 216 (class 1259 OID 50402)
-- Name: payment_type_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE payment_type_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2868 (class 0 OID 0)
-- Dependencies: 216
-- Name: payment_type_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE payment_type_sk_seq OWNED BY payment_type.sk;


--
-- TOC entry 217 (class 1259 OID 50404)
-- Name: paymentitem; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE paymentitem (
    storesk integer NOT NULL,
    paymentsk integer NOT NULL,
    invoicesk integer NOT NULL,
    customersk integer NOT NULL,
    payment_typesk integer NOT NULL,
    payment_employeesk integer NOT NULL,
    invoice_employeesk integer NOT NULL,
    payment_createddate timestamp without time zone NOT NULL,
    invoice_createddate timestamp without time zone NOT NULL,
    payment_amount_incl_gst numeric(19,4),
    invoice_amount_incl_gst numeric(19,4),
    modified timestamp without time zone,
    payment_amount_gst numeric(19,4),
    paymentitemid character varying(60) DEFAULT ''::character varying NOT NULL,
    payeesk integer
);


--
-- TOC entry 218 (class 1259 OID 50408)
-- Name: product_price_history; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE product_price_history (
    id bigint NOT NULL,
    product_id character varying(8) NOT NULL,
    supplier_cost numeric(19,2),
    supplier_cost_gst numeric(19,2),
    actual_cost numeric(19,2),
    actual_cost_gst numeric(19,2),
    supplier_rrp numeric(19,2),
    supplier_rrp_gst numeric(19,2),
    calculated_retail_price numeric(19,2),
    calculated_retail_price_gst numeric(19,2),
    override_retail_price numeric(19,2),
    override_retail_price_gst numeric(19,2),
    retail_price numeric(19,2),
    retail_price_gst numeric(19,2),
    effective_from_time timestamp without time zone NOT NULL,
    effective_to_time timestamp without time zone
);


--
-- TOC entry 219 (class 1259 OID 50411)
-- Name: product_price_history_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE product_price_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2869 (class 0 OID 0)
-- Dependencies: 219
-- Name: product_price_history_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE product_price_history_id_seq OWNED BY product_price_history.id;


--
-- TOC entry 220 (class 1259 OID 50413)
-- Name: schema_version; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE schema_version (
    installed_rank integer NOT NULL,
    version character varying(50),
    description character varying(200) NOT NULL,
    type character varying(20) NOT NULL,
    script character varying(1000) NOT NULL,
    checksum integer,
    installed_by character varying(100) NOT NULL,
    installed_on timestamp without time zone DEFAULT now() NOT NULL,
    execution_time integer NOT NULL,
    success boolean NOT NULL
);


--
-- TOC entry 221 (class 1259 OID 50420)
-- Name: spectaclejob; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE spectaclejob (
    sk integer NOT NULL,
    invoicesk integer,
    framesk integer,
    right_lenssk integer,
    left_lenssk integer,
    storesk integer NOT NULL,
    createddate timestamp without time zone NOT NULL,
    orderdate timestamp without time zone,
    receiveddate timestamp without time zone,
    pickupdate timestamp without time zone,
    modified timestamp without time zone NOT NULL,
    spectaclejobid character varying(60) DEFAULT ''::character varying NOT NULL,
    pms_invoicenum character varying(100)
);


--
-- TOC entry 222 (class 1259 OID 50424)
-- Name: spectaclejob_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE spectaclejob_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2870 (class 0 OID 0)
-- Dependencies: 222
-- Name: spectaclejob_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE spectaclejob_sk_seq OWNED BY spectaclejob.sk;


--
-- TOC entry 223 (class 1259 OID 50426)
-- Name: status; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE status (
    sk integer NOT NULL,
    statusid character varying(30) NOT NULL,
    name character varying(100) NOT NULL
);


--
-- TOC entry 224 (class 1259 OID 50429)
-- Name: status_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE status_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2871 (class 0 OID 0)
-- Dependencies: 224
-- Name: status_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE status_sk_seq OWNED BY status.sk;


--
-- TOC entry 225 (class 1259 OID 50437)
-- Name: store; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE store (
    sk integer NOT NULL,
    storeid character varying(12) NOT NULL,
    name character varying(100) NOT NULL,
    chainid character varying(100) DEFAULT ''::character varying NOT NULL,
    isactive boolean DEFAULT false NOT NULL,
    pms character varying(20)
);


--
-- TOC entry 226 (class 1259 OID 50442)
-- Name: store_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE store_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2872 (class 0 OID 0)
-- Dependencies: 226
-- Name: store_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE store_sk_seq OWNED BY store.sk;


--
-- TOC entry 227 (class 1259 OID 50462)
-- Name: storeproduct; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE storeproduct (
    sk integer NOT NULL,
    storeid character varying(12),
    productid character varying(500),
    type character varying(100),
    datecreated timestamp without time zone,
    name character varying(500),
    description character varying(500),
    retailprice numeric(19,4),
    wholesaleprice numeric(19,4),
    modified timestamp without time zone,
    last_purchase timestamp without time zone,
    last_sale timestamp without time zone,
    quantity integer,
    frame_model character varying(100) DEFAULT ''::character varying,
    frame_colour character varying(100) DEFAULT ''::character varying,
    frame_brand character varying(100) DEFAULT ''::character varying,
    frame_temple numeric(8,2) DEFAULT 0,
    frame_bridge numeric(8,2) DEFAULT 0,
    frame_eye_size numeric(8,2) DEFAULT 0,
    frame_depth numeric(8,2) DEFAULT 0,
    frame_material character varying(100) DEFAULT ''::character varying,
    lens_type character varying(100) DEFAULT ''::character varying,
    lens_refractive_index numeric(19,4) DEFAULT 0,
    lens_stock_grind character varying(10) DEFAULT ''::character varying,
    lens_size numeric(4,2) DEFAULT 0,
    lens_segment_size numeric(4,2) DEFAULT 0,
    internal_sku character varying(100),
    distributor character varying(100)
);


--
-- TOC entry 228 (class 1259 OID 50481)
-- Name: storeproduct_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE storeproduct_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2873 (class 0 OID 0)
-- Dependencies: 228
-- Name: storeproduct_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE storeproduct_sk_seq OWNED BY storeproduct.sk;


--
-- TOC entry 229 (class 1259 OID 50483)
-- Name: supplier_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE supplier_sk_seq
    START WITH 60
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 230 (class 1259 OID 50485)
-- Name: supplier; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE supplier (
    sk integer DEFAULT nextval('supplier_sk_seq'::regclass) NOT NULL,
    externalref bigint NOT NULL,
    supplierid character varying(255) NOT NULL,
    name character varying(255) NOT NULL
);


--
-- TOC entry 231 (class 1259 OID 50492)
-- Name: supplierproduct_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE supplierproduct_sk_seq
    START WITH 500
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 232 (class 1259 OID 50494)
-- Name: supplierproduct; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE supplierproduct (
    sk integer DEFAULT nextval('supplierproduct_sk_seq'::regclass) NOT NULL,
    suppliersku character varying(255) NOT NULL,
    suppliersk integer NOT NULL,
    frame_model character varying(255),
    frame_colour character varying(100),
    internal_sku character varying(100),
    frame_brand character varying(100)
);


--
-- TOC entry 2595 (class 2604 OID 50510)
-- Name: appointment sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY appointment ALTER COLUMN sk SET DEFAULT nextval('appointment_sk_seq'::regclass);


--
-- TOC entry 2597 (class 2604 OID 50511)
-- Name: appointment_type sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY appointment_type ALTER COLUMN sk SET DEFAULT nextval('appointment_type_sk_seq'::regclass);


--
-- TOC entry 2598 (class 2604 OID 50512)
-- Name: consult sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY consult ALTER COLUMN sk SET DEFAULT nextval('consult_sk_seq'::regclass);


--
-- TOC entry 2599 (class 2604 OID 50513)
-- Name: consult_type sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY consult_type ALTER COLUMN sk SET DEFAULT nextval('consult_type_sk_seq'::regclass);


--
-- TOC entry 2600 (class 2604 OID 50514)
-- Name: consultitem sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY consultitem ALTER COLUMN sk SET DEFAULT nextval('consultitem_sk_seq'::regclass);


--
-- TOC entry 2607 (class 2604 OID 50515)
-- Name: customer sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY customer ALTER COLUMN sk SET DEFAULT nextval('customer_sk_seq'::regclass);


--
-- TOC entry 2608 (class 2604 OID 50516)
-- Name: customer_ext sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY customer_ext ALTER COLUMN sk SET DEFAULT nextval('customer_ext_sk_seq'::regclass);


--
-- TOC entry 2612 (class 2604 OID 50517)
-- Name: discountcode sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY discountcode ALTER COLUMN sk SET DEFAULT nextval('discountcode_sk_seq'::regclass);


--
-- TOC entry 2616 (class 2604 OID 50518)
-- Name: employee sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY employee ALTER COLUMN sk SET DEFAULT nextval('employee_sk_seq'::regclass);


--
-- TOC entry 2617 (class 2604 OID 50519)
-- Name: employee_providerno sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY employee_providerno ALTER COLUMN sk SET DEFAULT nextval('employee_providerno_sk_seq'::regclass);


--
-- TOC entry 2619 (class 2604 OID 50520)
-- Name: expense sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY expense ALTER COLUMN sk SET DEFAULT nextval('expense_sk_seq'::regclass);


--
-- TOC entry 2620 (class 2604 OID 50521)
-- Name: expense_type sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY expense_type ALTER COLUMN sk SET DEFAULT nextval('expense_type_sk_seq'::regclass);


--
-- TOC entry 2621 (class 2604 OID 50522)
-- Name: invoice sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY invoice ALTER COLUMN sk SET DEFAULT nextval('invoice_sk_seq'::regclass);


--
-- TOC entry 2623 (class 2604 OID 50524)
-- Name: job_process_status sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY job_process_status ALTER COLUMN sk SET DEFAULT nextval('job_process_status_sk_seq'::regclass);


--
-- TOC entry 2624 (class 2604 OID 50527)
-- Name: payee sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY payee ALTER COLUMN sk SET DEFAULT nextval('payee_sk_seq'::regclass);


--
-- TOC entry 2631 (class 2604 OID 50528)
-- Name: payment sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY payment ALTER COLUMN sk SET DEFAULT nextval('payment_sk_seq'::regclass);


--
-- TOC entry 2632 (class 2604 OID 50529)
-- Name: payment_type sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY payment_type ALTER COLUMN sk SET DEFAULT nextval('payment_type_sk_seq'::regclass);


--
-- TOC entry 2634 (class 2604 OID 50530)
-- Name: product_price_history id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY product_price_history ALTER COLUMN id SET DEFAULT nextval('product_price_history_id_seq'::regclass);


--
-- TOC entry 2636 (class 2604 OID 50531)
-- Name: spectaclejob sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY spectaclejob ALTER COLUMN sk SET DEFAULT nextval('spectaclejob_sk_seq'::regclass);


--
-- TOC entry 2638 (class 2604 OID 50532)
-- Name: status sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY status ALTER COLUMN sk SET DEFAULT nextval('status_sk_seq'::regclass);


--
-- TOC entry 2641 (class 2604 OID 50534)
-- Name: store sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY store ALTER COLUMN sk SET DEFAULT nextval('store_sk_seq'::regclass);


--
-- TOC entry 2655 (class 2604 OID 50537)
-- Name: storeproduct sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY storeproduct ALTER COLUMN sk SET DEFAULT nextval('storeproduct_sk_seq'::regclass);


--
-- TOC entry 2660 (class 2606 OID 50546)
-- Name: appointment appointment_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY appointment
    ADD CONSTRAINT appointment_pkey PRIMARY KEY (sk);


--
-- TOC entry 2664 (class 2606 OID 50548)
-- Name: appointment_type appointment_type_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY appointment_type
    ADD CONSTRAINT appointment_type_pkey PRIMARY KEY (sk);


--
-- TOC entry 2669 (class 2606 OID 50550)
-- Name: consult_type constult_type_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY consult_type
    ADD CONSTRAINT constult_type_pkey PRIMARY KEY (sk);


--
-- TOC entry 2667 (class 2606 OID 50552)
-- Name: consult consult_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY consult
    ADD CONSTRAINT consult_pkey PRIMARY KEY (sk);


--
-- TOC entry 2672 (class 2606 OID 50554)
-- Name: consultitem consultitem_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY consultitem
    ADD CONSTRAINT consultitem_pkey PRIMARY KEY (sk);


--
-- TOC entry 2677 (class 2606 OID 50556)
-- Name: customer_ext customer_ext_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY customer_ext
    ADD CONSTRAINT customer_ext_pkey PRIMARY KEY (sk);


--
-- TOC entry 2675 (class 2606 OID 50559)
-- Name: customer customer_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY customer
    ADD CONSTRAINT customer_pkey PRIMARY KEY (sk);


--
-- TOC entry 2683 (class 2606 OID 50561)
-- Name: expense expense_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY expense
    ADD CONSTRAINT expense_pkey PRIMARY KEY (sk);


--
-- TOC entry 2686 (class 2606 OID 50563)
-- Name: expense_type expense_type_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY expense_type
    ADD CONSTRAINT expense_type_pkey PRIMARY KEY (sk);


--
-- TOC entry 2722 (class 2606 OID 50565)
-- Name: supplier externalref_ukey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY supplier
    ADD CONSTRAINT externalref_ukey UNIQUE (externalref);


--
-- TOC entry 2689 (class 2606 OID 50569)
-- Name: invoice invoice_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY invoice
    ADD CONSTRAINT invoice_pkey PRIMARY KEY (sk);


--
-- TOC entry 2692 (class 2606 OID 50575)
-- Name: invoiceitem invoiceitem_skey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY invoiceitem
    ADD CONSTRAINT invoiceitem_skey PRIMARY KEY (invoiceitemid);


--
-- TOC entry 2696 (class 2606 OID 50583)
-- Name: payee payee_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY payee
    ADD CONSTRAINT payee_pkey PRIMARY KEY (sk);


--
-- TOC entry 2699 (class 2606 OID 50585)
-- Name: payment payment_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY payment
    ADD CONSTRAINT payment_pkey PRIMARY KEY (sk);


--
-- TOC entry 2703 (class 2606 OID 50587)
-- Name: payment_type payment_type_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY payment_type
    ADD CONSTRAINT payment_type_pkey PRIMARY KEY (sk);


--
-- TOC entry 2705 (class 2606 OID 50589)
-- Name: paymentitem paymentitem_skey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY paymentitem
    ADD CONSTRAINT paymentitem_skey PRIMARY KEY (paymentitemid);


--
-- TOC entry 2680 (class 2606 OID 50599)
-- Name: employee pk_employee; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY employee
    ADD CONSTRAINT pk_employee PRIMARY KEY (sk);


--
-- TOC entry 2708 (class 2606 OID 50601)
-- Name: schema_version schema_version_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY schema_version
    ADD CONSTRAINT schema_version_pk PRIMARY KEY (installed_rank);


--
-- TOC entry 2711 (class 2606 OID 50603)
-- Name: spectaclejob spectaclejob_skey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY spectaclejob
    ADD CONSTRAINT spectaclejob_skey PRIMARY KEY (spectaclejobid);


--
-- TOC entry 2714 (class 2606 OID 50605)
-- Name: status status_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY status
    ADD CONSTRAINT status_pkey PRIMARY KEY (sk);


--
-- TOC entry 2717 (class 2606 OID 50609)
-- Name: store store_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store
    ADD CONSTRAINT store_pkey PRIMARY KEY (sk);


--
-- TOC entry 2720 (class 2606 OID 50615)
-- Name: storeproduct storeproduct_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY storeproduct
    ADD CONSTRAINT storeproduct_pkey PRIMARY KEY (sk);


--
-- TOC entry 2724 (class 2606 OID 50617)
-- Name: supplier supplier_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY supplier
    ADD CONSTRAINT supplier_pkey PRIMARY KEY (sk);


--
-- TOC entry 2729 (class 2606 OID 50619)
-- Name: supplierproduct supplierproduct_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY supplierproduct
    ADD CONSTRAINT supplierproduct_pkey PRIMARY KEY (sk);


--
-- TOC entry 2658 (class 1259 OID 50622)
-- Name: appointment_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX appointment_nkey ON appointment USING btree (appointmentid);


--
-- TOC entry 2661 (class 1259 OID 50623)
-- Name: appointment_storesk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX appointment_storesk ON appointment USING hash (storesk);


--
-- TOC entry 2662 (class 1259 OID 50624)
-- Name: appointment_type_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX appointment_type_nkey ON appointment_type USING btree (appointment_typeid);


--
-- TOC entry 2665 (class 1259 OID 50625)
-- Name: consult_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX consult_nkey ON consult USING btree (consultid);


--
-- TOC entry 2670 (class 1259 OID 50626)
-- Name: consult_type_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX consult_type_nkey ON consult_type USING btree (consult_typeid);


--
-- TOC entry 2673 (class 1259 OID 50627)
-- Name: customer_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX customer_nkey ON customer USING btree (customerid);


--
-- TOC entry 2678 (class 1259 OID 50630)
-- Name: employee_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX employee_nkey ON employee USING btree (employeeid);


--
-- TOC entry 2681 (class 1259 OID 50631)
-- Name: expense_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX expense_nkey ON expense USING btree (expenseid);


--
-- TOC entry 2684 (class 1259 OID 50632)
-- Name: expense_type_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX expense_type_nkey ON expense_type USING btree (expense_typeid);


--
-- TOC entry 2725 (class 1259 OID 50633)
-- Name: fki_supplierproduct_supplier_fkey; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX fki_supplierproduct_supplier_fkey ON supplierproduct USING btree (suppliersk);


--
-- TOC entry 2726 (class 1259 OID 50634)
-- Name: idx_model; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_model ON supplierproduct USING btree (frame_model);


--
-- TOC entry 2727 (class 1259 OID 50635)
-- Name: idx_suppliersku; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_suppliersku ON supplierproduct USING btree (suppliersku);


--
-- TOC entry 2687 (class 1259 OID 50636)
-- Name: invoice_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX invoice_nkey ON invoice USING btree (invoiceid);


--
-- TOC entry 2690 (class 1259 OID 50637)
-- Name: invoice_storesk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX invoice_storesk ON invoice USING hash (storesk);


--
-- TOC entry 2693 (class 1259 OID 50641)
-- Name: invoiceitem_storesk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX invoiceitem_storesk ON invoiceitem USING hash (storesk);


--
-- TOC entry 2694 (class 1259 OID 50648)
-- Name: payee_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX payee_nkey ON payee USING btree (payeeid);


--
-- TOC entry 2697 (class 1259 OID 50649)
-- Name: payment_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX payment_nkey ON payment USING btree (paymentid);


--
-- TOC entry 2700 (class 1259 OID 50650)
-- Name: payment_storeid_ix; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX payment_storeid_ix ON payment USING btree (storeid);


--
-- TOC entry 2701 (class 1259 OID 50651)
-- Name: payment_type_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX payment_type_nkey ON payment_type USING btree (payment_typeid);


--
-- TOC entry 2706 (class 1259 OID 50652)
-- Name: paymentitem_storesk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX paymentitem_storesk ON paymentitem USING hash (storesk);


--
-- TOC entry 2709 (class 1259 OID 50653)
-- Name: schema_version_s_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX schema_version_s_idx ON schema_version USING btree (success);


--
-- TOC entry 2712 (class 1259 OID 50654)
-- Name: status_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX status_nkey ON status USING btree (statusid);


--
-- TOC entry 2715 (class 1259 OID 50655)
-- Name: store_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX store_nkey ON store USING btree (storeid);


--
-- TOC entry 2718 (class 1259 OID 50656)
-- Name: storeproduct_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX storeproduct_nkey ON storeproduct USING btree (productid);


--
-- TOC entry 2730 (class 2606 OID 50752)
-- Name: supplierproduct supplierproduct_supplier_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY supplierproduct
    ADD CONSTRAINT supplierproduct_supplier_fkey FOREIGN KEY (suppliersk) REFERENCES supplier(sk);


-- Completed on 2017-09-25 12:16:41

--
-- PostgreSQL database dump complete
--

