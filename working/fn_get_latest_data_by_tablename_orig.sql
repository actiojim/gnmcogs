-- FUNCTION: stage.get_latest_data_by_tablename(character)

-- DROP FUNCTION stage.get_latest_data_by_tablename(character);

CREATE OR REPLACE FUNCTION stage.get_latest_data_by_tablename(
	_tablename character)
    RETURNS TABLE(data jsonb, parameters jsonb) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$

declare
sql varchar;
begin
SELECT 'SELECT jsonb_array_elements("data") as data,parameters FROM stage.' || _tablename || ' T where T.runid in (
select runid from (
select parameters->>''source'',runid,createddate,row_number() over (partition by parameters->>''source'' order by createddate desc) as r from stage.' || _tablename || ') S where r = 1);' 
into sql;

return query execute sql;

end;

$BODY$;

ALTER FUNCTION stage.get_latest_data_by_tablename(character)
    OWNER TO postgres;