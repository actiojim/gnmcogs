
-- Table: public.stock_price_history

-- DROP TABLE public.stock_price_history;

CREATE TABLE public.stock_price_history
(
    sk integer NOT NULL DEFAULT nextval('stock_price_history_sk_seq'::regclass),
    store_sk integer NOT NULL,
    source_id character varying(100) COLLATE pg_catalog."default" NOT NULL,
    
    takeon_storeproduct_sk integer NOT NULL,
    storeproduct_sk integer NOT NULL,
    
    supplier_cost numeric(19, 2),
    supplier_cost_gst numeric(19, 2),
    actual_cost numeric(19, 2),
    actual_cost_gst numeric(19, 2),
    supplier_rrp numeric(19, 2),
    supplier_rrp_gst numeric(19, 2),
    calculated_retail_price numeric(19, 2),
    calculated_retail_price_gst numeric(19, 2),
    override_retail_price numeric(19, 2),
    override_retail_price_gst numeric(19, 2),
    retail_price numeric(19, 2),
    retail_price_gst numeric(19, 2),
    created timestamp without time zone NOT NULL DEFAULT now(),
    CONSTRAINT stock_price_history_pkey PRIMARY KEY (sk),
    CONSTRAINT stock_price_history_store_sk_fkey FOREIGN KEY (store_sk)
        REFERENCES public.store (sk) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT stock_price_history_storeproduct_sk_fkey FOREIGN KEY (storeproduct_sk)
        REFERENCES public.storeproduct (sk) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT stock_price_history_takeon_storeproduct_sk_fkey FOREIGN KEY (takeon_storeproduct_sk)
        REFERENCES public.storeproduct (sk) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.stock_price_history
    OWNER to postgres;

-- Index: idx_stock_price_history_source_id
    
    
   begin; rollback;
   
   
   
   
   --- drop constraint to storeproduct sk
   
    alter table public.stock_price_history drop constraint stock_price_history_storeproduct_sk_fkey;
    
    -- add  column
    
    alter table  public.stock_price_history add column product_gnm_sk int ;
    
    alter table public.stock_price_history drop column storeproduct_sk;
    
    --- migrate existing relationship
    
    select count(*) from public.stock_price_history sph 
    inner join storeproduct sp on sp.sk = sph.storeproduct_sk
    inner join product_gnm pg on pg.sk = sp.product_gnm_sk
    where storeproduct_sk <> 0
    
    
    select * from storeproduct sp on sp.sk = sph.storeproduct_sk
    inner join product_gnm pg on pg.sk = sp.product_gnm_sk
    where storeproduct_sk <> 0
    
    
    select * from product_gnm pg
    
    
    update public.stock_price_history as sph set product_gnm_sk = sp.product_gnm_sk
    from storeproduct sp
    where sp.
    inner join  product_gnm pg on pg.sk = sp.product_gnm_sk
    
    
    select * from storeproduct sp inner join stock_price_history sph on sph.storeproduct_sk = sp.sk
    where sp.product_gnm_sk is not null
    
   	select * from public.stock_price_history sph inner join gnm.product_price_history  pph on pph.id::text = sph.source_id
     
    
    
    SET search_path = public;
    
begin;
INSERT INTO stock_price_history
(store_sk, source_id, takeon_storeproduct_sk, storeproduct_sk,
supplier_cost, supplier_cost_gst, 
actual_cost, actual_cost_gst, 
supplier_rrp, supplier_rrp_gst, calculated_retail_price, calculated_retail_price_gst, 
override_retail_price, override_retail_price_gst, 
retail_price, retail_price_gst, 
created,
product_gnm_sk)
SELECT
0, pch.id, 0, 0,
supplier_cost, supplier_cost_gst, 
actual_cost, actual_cost_gst, 
supplier_rrp, supplier_rrp_gst, calculated_retail_price, calculated_retail_price_gst, 
0,0,
0,0,
modified_date_time,
 pg.sk
-- select count(*)
FROM gnm.product_cost_history pch inner join public.product_gnm pg on pg.productid = pch.product_id

commit;
    
-- rollback;


select count(*) from stock_price_history where product_gnm_sk is not null
select * from stock_price_history where product_gnm_sk is not null


select count(*) from store_stockmovement where product_gnm_sk is not null and product_gnm_sk <> 0

select * from product_gnm

    alter table  public.store_stockmovement add column product_gnm_sk int ;

        
    update public.store_stockmovement as sph set product_gnm_sk = sp.product_gnm_sk
    
    select *
    from public.store_stockmovement sm 
	inner join product_gnm pg on pg.sk = sm.product_gnm_sk
	where stock_price_history_sk <> 0
	

	
	select sp.*
	from public.store_stockmovement sm 
	inner join public.storeproduct sp on sp.sk = sm.storeproduct_sk and sm.storeproduct_sk <> 0
	where sm.product_gnm_sk is not null	
	
	
	
	CREATE UNIQUE INDEX productid_on_product_gnm ON public.product_gnm (productid);

	

select adjustmenttype,count(*) from store_stockmovement where product_gnm_sk is not null and product_gnm_sk <> 0
group by adjustmenttype order  by count(*) desc


select * from public.store


select frame_brand, count(*) from storeproduct 
where frame_brand is not null
group by frame_brand

select frame_brand, count(*) from product_gnm 
where frame_brand is not null
group by frame_brand


update public.product_gnm as pg set frame_brand = b.name 
-- select * 
from gnm.brand b inner join gnm.product p on p.brand_id = b.id
where pg.productid = p.product_id

-- inner join public.product_gnm pg on pg.productid = p.product_id

select sp.product_gnm_sk ,count(*)
from public.storeproduct sp where  sp.product_gnm_sk is not null
group by sp.product_gnm_sk order by count(*) desc


select supplier_cost,sm.* from store_stockmovement sm 
inner join public.stock_price_history sph on sph.sk = sm.stock_price_history_sk
where sph.supplier_cost is not null and sph.supplier_cost > 0

update store_stockmovement as sm set avg_cost_price = supplier_cost
from public.stock_price_history sph
where sph.sk = sm.stock_price_history_sk and sph.supplier_cost is not null and sph.supplier_cost > 0

--- Drop Field

    -- add  column
    select * from public.store_stockmovement
    alter table  public.store_stockmovement drop column storeproduct_sk;
   
    
    --- products with multiple costs
 
    select pg.name,pg.sk,sph.sk,sph.actual_cost from public.product_gnm pg 
    left join public.stock_price_history sph on sph.product_gnm_sk =  pg.sk
    group by pg.name,pg.sk,sph.sk,sph.actual_cost
    
    -- number of 
    select pg.name,pg.sk,count(*) from public.product_gnm pg 
    inner join public.stock_price_history sph on sph.product_gnm_sk =  pg.sk
    inner join public.store_stockmovement sm on sm.product_gnm_sk = pg.sk
    inner join public.stock_price_history sph2 on sph2.sk = sm.stock_price_history_sk
    -- where sph2.actual_cost = sph.actual_cost
    group by pg.name,pg.sk 
    having count(*) > 1
    order by count(*) desc
    
    
    select pg.name,pg.sk,sph.actual_cost,sph2.actual_cost,sm.* from public.product_gnm pg 
    inner join public.stock_price_history sph on sph.product_gnm_sk =  pg.sk
    inner join public.store_stockmovement sm on sm.product_gnm_sk = pg.sk
    inner join public.stock_price_history sph2 on sph2.sk = sm.stock_price_history_sk
    where adjustment > 0 and adjustmenttype = 'Receipt'
    
/*    group by pg.name,pg.sk 
    having count(*) > 1
    order by count(*) desc */
    
    -- 
    select pg.name,pg.sk,count(*),sph.sk,sph.actual_cost from public.product_gnm pg 
    left join public.stock_price_history sph on sph.product_gnm_sk =  pg.sk
    group by pg.name,pg.sk,sph.sk,sph.actual_cost
    order by count(*) desc
    
    
    
    
    
    
