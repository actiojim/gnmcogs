-- SET search_path = public;


--
-- Name: fn_create_viewbydate(text, text); Type: FUNCTION; Schema: public; Owner: -
--

create or replace  FUNCTION fn_create_viewbydate(tablename text, datecolumnname text) RETURNS void
    LANGUAGE plpgsql
    AS $_$
begin
	EXECUTE fn_create_viewbydate_string($1, $2);
end
$_$;


--
-- Name: fn_create_viewbydate(text, text, text); Type: FUNCTION; Schema: public; Owner: -
--

create or replace  FUNCTION fn_create_viewbydate(tablename text, datecolumnname text, keycolumn text) RETURNS void
    LANGUAGE plpgsql
    AS $_$
begin
	EXECUTE fn_create_viewbydate_string($1, $2,$3);
end
$_$;


--
-- Name: fn_create_viewbydate_string(text, text); Type: FUNCTION; Schema: public; Owner: -
--

create or replace  FUNCTION fn_create_viewbydate_string(tablename text, datecolumnname text) RETURNS text
    LANGUAGE sql
    AS $_$
select 'CREATE OR REPLACE VIEW vbyd_' || $1 || ' as select date_part(''year'',sm.' || $2 || ')as year,date_part(''doy'',sm.' 
|| $2 || ') as doy,date_part(''month'',sm.' || $2 || ' )as month,date_part(''dow'',sm.' || $2 || ') as dow,date_part(''day'',sm.'|| $2 ||') as day, sk 
from ' || $1 || ' sm 
group by date_part(''year'',sm.' || $2 || '),date_part(''doy'',sm.' || $2 || '),date_part(''month'',sm.' || $2 || '),date_part(''dow'',sm.' || $2 || '),date_part(''day'',sm.' || $2 || '), sk' as view_string $_$;


--
-- Name: fn_create_viewbydate_string(text, text, text); Type: FUNCTION; Schema: public; Owner: -
--

create or replace  FUNCTION fn_create_viewbydate_string(tablename text, datecolumnname text, keycolumn text) RETURNS text
    LANGUAGE sql
    AS $_$
select 'CREATE OR REPLACE VIEW vbyd_' || $1 || ' as select date_part(''year'',sm.' || $2 || ')as year,date_part(''doy'',sm.' 
|| $2 || ') as doy,date_part(''month'',sm.' || $2 || ' )as month,date_part(''dow'',sm.' || $2 || ') as dow,date_part(''day'',sm.'|| $2 ||') as day, ' || $3 || '  
from ' || $1 || ' sm 
group by date_part(''year'',sm.' || $2 || '),date_part(''doy'',sm.' || $2 || '),date_part(''month'',sm.' || $2 || '),date_part(''dow'',sm.' || $2 || '),date_part(''day'',sm.' || $2 || '),' || $3 as view_string $_$;


--
-- Name: fn_create_viewbydate_string2(regclass, regclass); Type: FUNCTION; Schema: public; Owner: -
--

create or replace  FUNCTION fn_create_viewbydate_string2(tablename regclass, datecolumnname regclass) RETURNS text
    LANGUAGE sql
    AS $_$
select 'CREATE OR REPLACE VIEW vbyd_' || $1 || ' as select date_part(''year'',sm.' || $2 || ')as year,date_part(''doy'',sm.' 
|| $2 || ') as doy,date_part(''month'',sm.' || $2 || ')as month2,date_part(''dow'',sm.' || $2 || ') as dow,date_part(''day'',sm.' || $2 || ') as day, sk from ' || $1 || 
' sm group by date_part(''year'',sm.' || $2 || '),date_part(''doy'',sm.' || $2 || '),date_part(''month'',sm.' || $2 || '),date_part(''dow'',sm.' 
|| $2 || '),date_part(''day'',sm.' || $2 || '),sk' as view_string $_$;


--
-- Name: fn_create_viewbydate_string2(character varying, character varying); Type: FUNCTION; Schema: public; Owner: -
--

create or replace  FUNCTION fn_create_viewbydate_string2(tablename character varying, datecolumnname character varying) RETURNS character varying
    LANGUAGE sql
    AS $_$
select 'CREATE OR REPLACE VIEW vbyd_' || $1 || ' as select date_part(''year'',sm.' || $2 || ')as year,date_part(''doy'',sm.' 
|| $2 || ') as doy,date_part(''month'',sm.' || $2 || ' )as month,date_part(''dow'',sm.' || $2 || ') as dow,date_part(''day'',sm.'|| $2 ||') as day, sk 
from ' || $1 || ' sm 
group by date_part(''year'',sm.' || $2 || '),date_part(''doy'',sm.' || $2 || '),date_part(''month'',sm.' || $2 || '),date_part(''dow'',sm.' || $2 || '),date_part(''day'',sm.' || $2 || '),sk' 
as view_string $_$;


--
-- Name: within_dow_range(timestamp without time zone, timestamp without time zone); Type: FUNCTION; Schema: public; Owner: -
--

create or replace  FUNCTION within_dow_range(d1 timestamp without time zone, d2 timestamp without time zone) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
declare
	td1 timestamp;
	td2 timestamp;
begin	
	if date_part('doy',d1) = date_part('doy',d2) and date_part('year',d1) = date_part('year',d2) then
		return true;
	elsif d2 >= (d1 - interval '1 days') and d2 <= (d1 + interval '1 day') then
		return true;
	elsif date_part('dow',d1) = 1 and d2 >= (d1 - interval '3 days') and d2 <= (d1 + interval '1 day') then
		return true;
	elsif date_part('dow',d1) = 5 and d2 >= (d1 - interval '1 days') and d2 <= (d1 + interval '3 day') then
		return true;
	elsif date_part('dow',d1) = 6 and d2 >= (d1 - interval '1 days') and d2 <= (d1 + interval '2 day') then
		return true;
	else
		return false;
	end if;
end;
$$;


--
-- Name: within_dow_range(timestamp without time zone, timestamp without time zone, integer); Type: FUNCTION; Schema: public; Owner: -
--

create or replace  FUNCTION within_dow_range(d1 timestamp without time zone, d2 timestamp without time zone, bounds integer) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
begin	
	if d2 >= (d1 - interval '1d'* bounds) and d2 <= (d1 + interval '1d'* bounds) then
		return true;
	else
		return within_dow_range(d1,d2);
	end if;
end;
$$;

