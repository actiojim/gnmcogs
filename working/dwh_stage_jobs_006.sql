SET search_path = stage;
 
drop table if exists order_stage;
 
create table order_stage
(
    sk serial NOT NULL,
	run_id character varying(100),
	   
    -- source data order id
    order_id character varying(100),
    
    cust_firstname character varying(100),
    cust_lastname character varying(100),
    cust_midname character varying(100),
    cust_phone character varying(100),
    store_id character varying(100),
    
    cust_sk int,
    order_type character varying(100),
    
    queue character varying(100),
    status character varying(100),
    
    source_created character varying(100),
    source_modified character varying(100),
    
    -- created date
    created timestamp without time zone NOT null default now(),
    primary key (sk)
);


-- 1. customer entries that are missing from customer table
INSERT INTO public.customer ( storesk, firstname, lastname, homephone, createddate, title, gender, email)
select  distinct  s.sk, os.cust_firstname, os.cust_lastname, os.cust_phone, now(), '', '', ''
from stage.order_stage os 
left join public.customer c on c.firstname = os.cust_firstname and c.lastname = os.cust_lastname  
-- and (os.cust_phone = c.homephone or os.cust_phone = c.mobile or os.cust_phone = c.workphone)
left join public.store s on s.storeid = os.store_id -- and c.storesk = s.sk
where
 c.sk is null;
 
 
-- stage orders
INSERT INTO order_stage
(run_id, order_id, 
cust_firstname, cust_lastname, cust_phone, 
cust_sk, store_id, 
order_type, 
queue, status, 
source_created, source_modified)
SELECT null , o.order_id, 
oc.first_name, oc.last_name, oc.telephone, 
c.sk, s.store_id, 
o.order_type_id,
o.queue, o.status, 
o.created_date_time, o.modified_date_time
FROM gnm."order" o 
inner join gnm.order_customer oc on oc.id = o.customer_id
inner join gnm."store" s on s.id = o.store_id
left outer join public.store ps on ps.storeid = s.store_id
left join public.customer c on c.firstname = oc.first_name and c.lastname = oc.last_name and c.storesk = ps.sk

select count(*) from (
SELECT null , o.order_id, 
oc.first_name, oc.last_name, oc.telephone, 
c.sk, s.store_id, 
o.order_type_id,
o.queue, o.status, 
o.created_date_time, o.modified_date_time
FROM gnm."order" o 
inner join gnm.order_customer oc on oc.id = o.customer_id
inner join gnm."store" s on s.id = o.store_id
left join public.store ps on ps.storeid = s.store_id
left join public.customer c on c.firstname = oc.first_name and c.lastname = oc.last_name and c.storesk = ps.sk) x

-- find customer
/*
select count(*) from (
select  distinct os.store_id, s.sk, os.cust_firstname, os.cust_lastname, os.cust_phone
from stage.order_stage os 
left join public.customer c on c.firstname = os.cust_firstname and c.lastname = os.cust_lastname  
-- and (os.cust_phone = c.homephone or os.cust_phone = c.mobile or os.cust_phone = c.workphone)
left join public.store s on s.storeid = os.store_id -- and c.storesk = s.sk
where
 c.sk is  null) x

select * from public.customer

*/


	
 /*

select * from gnm.store
select * from gnm."order"
select * from gnm.job

*/

----------------------------------------------------------------------------------------------------------------------

SET search_path = stage;
 
drop table if exists job_stage;
 
create table job_stage
(
    sk serial NOT NULL,
	run_id character varying(100),
	
    -- natural source key job_id
    job_id character varying(100),
    
    -- source data order id
    order_id character varying(100),
    product_id character varying(100),
    supplier_id character varying(100),
    quantity character varying(100),
    
    -- job type frame or lens
    job_type character varying(100),
    queue character varying(100),
    status character varying(100),
    source_created character varying(100),
    source_modified character varying(100),
    
    -- created date
    created timestamp without time zone NOT null default now(),
    primary key (sk)
);

--
-- Generate job stage
-- 
/*
insert into stage.job_stage (
	run_id,
	order_id,
	customer_id,
	store_id,
	job_id,
	queue,
	status,
	quantity,
	source_created,
	source_modified,
	job_type,
	product_id,
	supplier_id)
select  
	j.runid,
	o.orderid,
	o.store_id,
	c.customerId, 
	j.f->>'id' as jobid
	 ,f->'attributes'->>'queue' as queue
	 ,f->'attributes'->>'status' as status
	 ,f->'attributes'->>'quantity' as quantity
	 ,f->'attributes'->>'createdDateTime' as created
	 ,f->'attributes'->>'modifiedDateTime' as modified
	 ,f->'relationships'->'jobType'->'data'->>'id' as jobtype
	 ,f->'relationships'->'product'->'data'->>'id' as productid
	 ,f->'relationships'->'supplier'->'data'->>'id' as supplierid 
from  
  (select runid, jsonb_array_elements(jsonb_array_elements(data)->'data') as f from stage.gnm_hub_orders 
 	where runid in (select runid from stage.gnm_hub_orders where createddate >= '2017-08-20 18:47:46')) j 
  inner join 
	(select runid,f->>'id' as orderid,f->'relationships'->'customer'->'data'->>'id'as customer_id, f->'relationships'->'store'->'data'->>'id'as store_id from 
		(select runid,jsonb_array_elements(jsonb_array_elements(y.data)->'included') as f 
			from stage.gnm_hub_orders y 
			where runid in (select runid from stage.gnm_hub_orders where createddate >= '2017-08-20 18:47:46') ) x where f->>'type' = 'order') o on f->'relationships'->'order'->'data'->>'id' = o.orderid 
  inner join 
	(select runid,f->>'id' as id,f->'attributes'->>'customerId' as customerId from 
			(select runid,jsonb_array_elements(jsonb_array_elements(y.data)->'included' ) as f 
					from stage.gnm_hub_orders y 
					where runid in (select runid from stage.gnm_hub_orders where createddate >= '2017-08-20 18:47:46') ) x
		where f->>'type' = 'orderCustomer') c on o.customer_id = c.id
  
 where j.f->>'id'  not in (select job_id from job_stage)
*/

insert into stage.job_stage (
	run_id,
	order_id,
	job_id,
	queue,
	status,
	quantity,
	source_created,
	source_modified,
	job_type,
	product_id,
	supplier_id)
select  
	null,
	j.order_id,
	j.id,
	j.queue,
	j.status,
	j.quantity,
	j.created_date_time,
	j.modified_date_time,
	j.job_type_id,
	j.product_id,
	j.supplier_id
from gnm.job j ;

-- ------------------------------------------------------------------------------------------


SET search_path = public;
CREATE UNIQUE INDEX productid_on_product ON public.storeproduct (productid);

--- Populate store product from 

--- create table
drop table public.product_gnm;

CREATE TABLE public.product_gnm
(
    sk serial NOT NULL,
    productid character varying(500) COLLATE pg_catalog."default",
    type character varying(100) COLLATE pg_catalog."default",
    datecreated timestamp without time zone,
    name character varying(500) COLLATE pg_catalog."default",
    description character varying(500) COLLATE pg_catalog."default",
    retailprice numeric(19, 4),
    wholesaleprice numeric(19, 4),
    modified timestamp without time zone,
    last_purchase timestamp without time zone,
    last_sale timestamp without time zone,
    quantity integer,
    frame_model character varying(100) COLLATE pg_catalog."default" DEFAULT ''::character varying,
    frame_colour character varying(100) COLLATE pg_catalog."default" DEFAULT ''::character varying,
    frame_brand character varying(100) COLLATE pg_catalog."default" DEFAULT ''::character varying,
    frame_temple numeric(8, 2) DEFAULT 0,
    frame_bridge numeric(8, 2) DEFAULT 0,
    frame_eye_size numeric(8, 2) DEFAULT 0,
    frame_depth numeric(8, 2) DEFAULT 0,
    frame_material character varying(100) COLLATE pg_catalog."default" DEFAULT ''::character varying,
    lens_type character varying(100) COLLATE pg_catalog."default" DEFAULT ''::character varying,
    lens_refractive_index numeric(19, 4) DEFAULT 0,
    lens_stock_grind character varying(10) COLLATE pg_catalog."default" DEFAULT ''::character varying,
    lens_size numeric(4, 2) DEFAULT 0,
    lens_segment_size numeric(4, 2) DEFAULT 0,
    internal_sku character varying(100) COLLATE pg_catalog."default",
    distributor character varying(100) COLLATE pg_catalog."default",
    created timestamp without time zone NOT NULL default now(),
     PRIMARY KEY (sk)
)




-- INSERT INTO storeproduct
insert into public.product_gnm
(productid, "type", 
datecreated, modified, 
"name", description, internal_sku, distributor)
SELECT product_id, "type", 
created_date_time, modified_date_time,
p."name",  description, supplier_sku, s.sk
FROM gnm.product p 
inner join public.supplier s on s.externalref = p.supplier_id

/*
SELECT id, supplier_id, "name", email, fax, telephone, contact_person_id, created_user, created_date_time, modified_user, modified_date_time
FROM gnm.supplier;

select * from public.storeproduct where storeid = '0'
select * from public.supplier
*/

------------------------------------------------------------------------------------------------------------------------------------------

SET search_path = public;

INSERT INTO stock_price_history
(store_sk, source_id, takeon_storeproduct_sk, storeproduct_sk, 
supplier_cost, supplier_cost_gst, 
actual_cost, actual_cost_gst, 
supplier_rrp, supplier_rrp_gst, calculated_retail_price, calculated_retail_price_gst, 
override_retail_price, override_retail_price_gst, 
retail_price, retail_price_gst, 
created)
-- VALUES(nextval('stock_price_history_sk_seq'::regclass), 0, '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, now());
SELECT 
0, id, 0, sp.sk, 
supplier_cost, supplier_cost_gst, 
actual_cost, actual_cost_gst, 
supplier_rrp, supplier_rrp_gst, calculated_retail_price, calculated_retail_price_gst, 
0,0,
0,0,
modified_date_time
FROM gnm.product_cost_history pch inner join storeproduct sp on sp.productid = pch.product_id


-----------------------------------------------------------------------------------------------------------------------------------------
/*
select * from "store"

select * from gnm_scire_0816.storeproduct

INSERT INTO gnm_scire_0816.storeproduct
(storeid, productid, "type", datecreated, modified, "name", description, internal_sku )
SELECT 0, product_id, "type", created_date_time, modified_date_time, "name",  description, supplier_sku
FROM gnm.product 
-- where product_id not in (select productid from gnm_scire_0816.storeproduct )

select * from gnm.product
where product_id not in (select productid from gnm_scire_0816.storeproduct )

select * from gnm_scire_0816.storeproduct sp inner join gnm_scire_0816.stock_price_history sph on sph.storeproduct_sk = sp.sk

*/
--------------------------------------------------------------------------------------------------------------------------------------------

SET search_path = public;

-- stage order history
drop table if exists order_history cascade;
drop table if exists job_history cascade;
drop table if exists invoice_orders cascade;

--
-- history record is immutable a new record is created for each data change
--
create table order_history
(
    sk serial NOT NULL,
    store_sk int NOT NULL REFERENCES store(sk),
    customer_sk int NOT NULL REFERENCES customer(sk),
    
    -- natural source key
    source_id character varying(100),
    
    order_type character varying(100) NOT NULL,
    queue character varying(100),
    status character varying(100),

    source_created timestamp not null,
    source_modified timestamp not null,

    -- created date
    created timestamp without time zone NOT NULL default now(),
    primary key (sk)
);


-- 
-- history record is immutable a new record is created for each data change
--
create table job_history
(
    sk serial NOT NULL,  
    -- natural source key job_id
    source_id character varying(100),
    
    -- source data order id
    order_id character varying(100) NOT NULL,
    order_sk int not null,
    
    -- job type frame or lens
    job_type character varying(100) NOT NULL,
    
    product_sk int not null references product_gnm(sk),
    supplier_sk int not null references supplier(sk),
    
    queue character varying(100),
    status character varying(100),


    source_created timestamp not null,
    source_modified timestamp not null,

    -- created date
    created timestamp without time zone NOT NULL default now(),
    primary key (sk)
);


-- drop table public.job_history;


-- Bind Table between invoice and orders
create table invoice_orders
(
    sk serial NOT NULL,
    store_sk int NOT NULL REFERENCES store(sk),
    
    invoice_sk int NOT NULL REFERENCES invoice(sk),
    order_sk int NOT NULL REFERENCES order_history(sk), 

    -- created date
    created timestamp without time zone NOT NULL default now(),
    primary key (sk)
)

--- -----------------------------------------------------------------------------------------------------------------------------

SET search_path = public;

/*
select count(*) from (select  order_id from stage.job_stage js group by js.order_id) x;
select count(*) from (select  order_id from gnm.order o group by o.order_id) x;
select * from order_history
select * from stage.order_stage
select * from "store"
SELECT sk, storeid, "name", chainid, isactive FROM "store";

select * from order_history

select * from order_job_history
select * from stage.job_stage
*/

-- process the orders
INSERT INTO order_history
(store_sk, 
customer_sk,
source_id, 
order_type, 
queue, status, 
source_created, source_modified)
SELECT 
coalesce(s.sk, 0), 
coalesce(c.sk, 0),
order_id, 
coalesce(os.order_type,'UNKNOWN'), 
os.queue, os.status, 
-- to_timestamp(os.source_created,'YYYY-MM-DD HH24:MI:SS'), to_timestamp(os.source_modified,'YYYY-MM-DD HH24:MI:SS')
os.source_created::TimeSTAMP with time zone, os.source_modified::TimeSTAMP with time zone
FROM stage.order_stage os 
left join "store" s on s.storeid = os.store_id
left join customer c on c.firstname = os.cust_firstname and c.lastname = os.cust_lastname and c.storesk = s.sk
left join order_history oh on oh.source_id = os.order_id
where 
	oh.source_id is null

	
-- 
-- history record is immutable a new record is created for each data change
--


-- select * from public.storeproduct
-- select * from gnm.product
-- select * from stage.job_stage
-- select * from public.supplier
-- select * from public.job_history

INSERT INTO public.job_history
(source_id, order_id, 
order_sk, 
job_type, 
product_sk, supplier_sk, 
queue, status, 
source_created, source_modified)
select
js.job_id, js.order_id,
-- order sk
oh.sk,
js.job_type,
coalesce(p.sk,0) as productsk, coalesce(s.sk,0) as storesk,
js.queue, js.status,
js.source_created::TimeSTAMP with time zone, js.source_modified::TimeSTAMP with time zone
-- select *
from stage.job_stage js 
inner join order_history oh on oh.source_id = js.order_id
inner join public.supplier s on CAST(s.externalref as varchar(100) ) = js.supplier_id
inner join public.product_gnm p on js.product_id =   p.productid --CAST( p.productid as varchar(100))
left join public.job_history jh on jh.source_id = js.job_id
where 
	jh.sk is null
	
-- where
--	s.sk is not null and  p.sk is not null


-- what we believe is shaped by our ignorance, our beliefs expand to fill the space of our ignorance
-- 

-- update store 
select * from public.store_stockmovement
select * from public.invoice i left join public.stock_stockmovement sm on sm.invoice_sk isk = i.sk

---------------------------------------------------------------------------------------------------------------------------------

SET search_path = public;
drop function fn_create_viewbydate_string(tablename text, datecolumnname text);

create or replace function fn_create_viewbydate_string(tablename text, datecolumnname text) returns text as $$
select 'CREATE OR REPLACE VIEW vbyd_' || $1 || ' as select date_part(''year'',sm.' || $2 || ')as year,date_part(''doy'',sm.' 
|| $2 || ') as doy,date_part(''month'',sm.' || $2 || ' )as month,date_part(''dow'',sm.' || $2 || ') as dow,date_part(''day'',sm.'|| $2 ||') as day, sk 
from ' || $1 || ' sm 
group by date_part(''year'',sm.' || $2 || '),date_part(''doy'',sm.' || $2 || '),date_part(''month'',sm.' || $2 || '),date_part(''dow'',sm.' || $2 || '),date_part(''day'',sm.' || $2 || '), sk' as view_string $$
LANGUAGE sql;

drop function fn_create_viewbydate(tablename text, datecolumnname text) ;
create or replace function fn_create_viewbydate(tablename text, datecolumnname text) 
returns void as $func$
begin
	EXECUTE fn_create_viewbydate_string($1, $2);
end
$func$ 
LANGUAGE plpgsql;


drop function fn_create_viewbydate_string(tablename text, datecolumnname text, keycolumn text);

create or replace function fn_create_viewbydate_string(tablename text, datecolumnname text, keycolumn text) returns text as $$
select 'CREATE OR REPLACE VIEW vbyd_' || $1 || ' as select date_part(''year'',sm.' || $2 || ')as year,date_part(''doy'',sm.' 
|| $2 || ') as doy,date_part(''month'',sm.' || $2 || ' )as month,date_part(''dow'',sm.' || $2 || ') as dow,date_part(''day'',sm.'|| $2 ||') as day, ' || $3 || '  
from ' || $1 || ' sm 
group by date_part(''year'',sm.' || $2 || '),date_part(''doy'',sm.' || $2 || '),date_part(''month'',sm.' || $2 || '),date_part(''dow'',sm.' || $2 || '),date_part(''day'',sm.' || $2 || '),' || $3 as view_string $$
LANGUAGE sql;

drop function fn_create_viewbydate(tablename text, datecolumnname text, keycolumn text) ;
create or replace function fn_create_viewbydate(tablename text, datecolumnname text, keycolumn text) 
returns void as $func$
begin
	EXECUTE fn_create_viewbydate_string($1, $2,$3);
end
$func$ 
LANGUAGE plpgsql;

select public.fn_create_viewbydate_string('stock_price_history', 'created');


select fn_create_viewbydate('store_stockmovement', 'adjustmentdate');
select fn_create_viewbydate('stock_price_history', 'created');
select fn_create_viewbydate('invoice','createddate');
select fn_create_viewbydate_string('invoiceitem','createddate','invoicesk')
select fn_create_viewbydate('order_history','source_created');
select fn_create_viewbydate('job_history','source_created');



CREATE OR REPLACE VIEW vbyd_job_history as select date_part('year',sm.source_created)as year,
date_part('doy',sm.source_created) as doy,date_part('month',sm.source_created )as month,date_part('dow',sm.source_created) as dow,date_part('day',sm.source_created) as day, sk 
from public.job_history sm 
group by date_part('year',sm.source_created),date_part('doy',sm.source_created),date_part('month',sm.source_created),date_part('dow',sm.source_created),date_part('day',sm.source_created), sk

select * from job_history


CREATE OR REPLACE VIEW vbyd_invoiceitem as select date_part('year',sm.createddate)as year,date_part('doy',sm.createddate) as doy,date_part('month',sm.createddate )as month,date_part('dow',sm.createddate) as dow,date_part('day',sm.createddate) as day, invoicesk  
from invoiceitem sm 
group by date_part('year',sm.createddate),date_part('doy',sm.createddate),date_part('month',sm.createddate),date_part('dow',sm.createddate),date_part('day',sm.createddate),invoicesk

select * from vbyd_stock_price_history where year = 2017;
select * from vbyd_store_stockmovement where year = 2017;
select * from vbyd_invoice where year = 2017 and month = 7;
select * from vbyd_invoiceitem
select * from vbyd_order_history 
select * from vbyd_job_history


select * from invoiceitem
select * from order_history

select * from public.order_history oh 
inner join public.job_history jh on jh.order_sk = oh.sk
left join invoiceitem ii on ii.customersk = oh.customer_sk and ii.productsk = jh.product_sk
where ii.invoicesk is not null


select order_id,count(*) from public.job_history group by order_id order by count(*) desc

-- steps 

/*
 * stock_movements
 * orders
 * job_history - view of get last state
 * 
 * join invoice to order - by day(+- 1,2 or Monday/Friday) time dimension may need day of week then, storeproductid (storeid,productid), storeid, customerid
 * 
 */

delete from public.storeproduct where storeid = '0'

select * from public.storeproduct where storeid = '0000-000' and productid in (select product_id from gnm.product)

begin;
update public.storeproduct set storeid = '0000-000' where storeid = '0' and productid in (select product_id from gnm.product)
-- commit;

begin;
update public.storeproduct set description="name","name" = description where storeid = '0'
-- commit;
-- rollback;

begin ;
-- ref to history first
delete from public.stock_price_history where storeproduct_sk in (select sk from public.storeproduct where storeid = '0000-000')
delete from public.storeproduct where storeid = '0000-000'
rollback;

select * from public.storeproduct where storeid = '0000-000'
select * from public.storeproduct where storeid <> '0000-000'

select * from gnm.product p 
inner join 
(select substring(productid,10, length(productid)) as pname,productid from public.storeproduct where storeid != '0') x on x.pname = p."name"

select x.storeid, x.description,p.product_id,count(*) into prod_gnm from gnm.product p 
inner join 
 public.storeproduct x on x.description = p."name" group by x.storeid, x.description,p.product_id

 
--- number of gnm products at a store
select * from public.store s
inner join 
(select storeid, count(*) from prod_gnm pg group by storeid) x  on s.storeid = x.storeid

select *  from prod_gnm pg inner join "store" s on s.storeid = pg.storeid

select * from gnm.product

--------------------------------------------------------------------------------------------


select * from public."store"
begin;
insert into public.store(sk,storeid,name,chainid,isactive,store_nk,acquisition_date)
values (-1,'0000-000','gnm','gnm',true,'gnm','2015-01-01')
-- commit

select * from public.invoiceitem

--- clear out incorrect products

select substring(productid,10, length(productid)) as pname,productid,storeid,"name",description,internal_sku from public.storeproduct where storeid != '0'

select x.pname,count(*) into prod_multi_store from
(select substring(productid,10, length(productid)) as pname,productid from public.storeproduct where storeid != '0') x
group by x.pname having count(*) > 1 order by count(*) desc


select count(*) from prod_multi_store

MERGE INTO public.storeproduct AS tgt
USING SOURCE_TABLE AS src
ON (tgt.sk=src.sk)
WHEN MATCHED
THEN UPDATE SET
tgt.tableoid=src.tableoid, tgt.cmax=src.cmax, tgt.xmax=src.xmax, tgt.cmin=src.cmin, tgt.xmin=src.xmin, tgt.ctid=src.ctid, tgt.storeid=src.storeid, tgt.productid=src.productid, tgt."type"=src."type", tgt.datecreated=src.datecreated, tgt."name"=src."name", tgt.description=src.description, tgt.retailprice=src.retailprice, tgt.wholesaleprice=src.wholesaleprice, tgt.modified=src.modified, tgt.last_purchase=src.last_purchase, tgt.last_sale=src.last_sale, tgt.quantity=src.quantity, tgt.frame_model=src.frame_model, tgt.frame_colour=src.frame_colour, tgt.frame_brand=src.frame_brand, tgt.frame_temple=src.frame_temple, tgt.frame_bridge=src.frame_bridge, tgt.frame_eye_size=src.frame_eye_size, tgt.frame_depth=src.frame_depth, tgt.frame_material=src.frame_material, tgt.lens_type=src.lens_type, tgt.lens_refractive_index=src.lens_refractive_index, tgt.lens_stock_grind=src.lens_stock_grind, tgt.lens_size=src.lens_size, tgt.lens_segment_size=src.lens_segment_size, tgt.internal_sku=src.internal_sku, tgt.distributor=src.distributor
WHEN NOT MATCHED
THEN INSERT (tableoid, cmax, xmax, cmin, xmin, ctid, sk, storeid, productid, "type", datecreated, "name", description, retailprice, wholesaleprice, modified, last_purchase, last_sale, quantity, frame_model, frame_colour, frame_brand, frame_temple, frame_bridge, frame_eye_size, frame_depth, frame_material, lens_type, lens_refractive_index, lens_stock_grind, lens_size, lens_segment_size, internal_sku, distributor)
VALUES (src.tableoid, src.cmax, src.xmax, src.cmin, src.xmin, src.ctid, src.sk, src.storeid, src.productid, src."type", src.datecreated, src."name", src.description, src.retailprice, src.wholesaleprice, src.modified, src.last_purchase, src.last_sale, src.quantity, src.frame_model, src.frame_colour, src.frame_brand, src.frame_temple, src.frame_bridge, src.frame_eye_size, src.frame_depth, src.frame_material, src.lens_type, src.lens_refractive_index, src.lens_stock_grind, src.lens_size, src.lens_segment_size, src.internal_sku, src.distributor);


-- product_gnm, storeproduct (storeid,productid)


select queue, count(*) from gnm.job group by queue


---------------------------------------------------------------------------------------------------
---- Add reference back to GNM product

ALTER TABLE public.storeproduct ADD COLUMN product_gnm_sk int;

select count(*) from 
(select sp.storeid,count(*) from public.product_gnm pg right join public.storeproduct sp on pg.name = sp.description 
where
	pg.sk is null group by sp.storeid) x

select * from public.storeproduct sp where storeid = '7000-013'

drop table tmp_storeprod1;

select sk,substring(productid,13, length(productid)) as pname,productid,storeid,"name",description,internal_sku 
into tmp_storeprod1
from public.storeproduct 


select sp.internal_sku,pg.* from storeproduct sp left join  product_gnm pg on pg.productid = sp.internal_sku where pg.sk is not null

select count(*) from storeproduct sp left join  product_gnm pg on pg.productid = sp.internal_sku where pg.sk is not null
select count(*) from storeproduct sp left join  product_gnm pg on pg.productid = sp.internal_sku where pg.sk is  null



select count(*) from 
(select distinct * from tmp_storeprod1 sp1 left join public.product_gnm pg on pg.name = sp1.pname
where pg.sk is not null) x

select count(*) from public.product_gnm pg right join public.storeproduct sp on pg.name = sp.description  sp1 


begin; 
update public.storeproduct as sp set product_gnm_sk = x.gnmsk from
(select sp.sk, pg.sk as gnmsk from storeproduct sp left join  product_gnm pg on pg.productid = sp.internal_sku where pg.sk is not null) x 
where x.sk = sp.sk

commit;


select count(*) from  public.storeproduct sp  where product_gnm_sk is not null

-- rollback;
select storeid,count(*) from storeproduct sp where product_gnm_sk is not null group by storeid


/*
 * stock_movements
 * orders
 * job_history - view of get last state
 * 
 * join invoice to order - by day(+- 1,2 or Monday/Friday) time dimension may need day of week then, storeproductid (storeid,productid), storeid, customerid
 * 
 */

----- Join the invoices to the Order

select * from public.product_gnm

select * from public.invoiceitem ii 
inner join storeproduct sp on sp.sk = ii.productsk


select count(*) from public.order_history oh 
inner join public.job_history jh on jh.order_sk = oh.sk
--inner join public.product_gnm pg on pg.sk = jh.product_sk
inner join public.storeproduct sp on sp.product_gnm_sk = jh.product_sk
left join invoiceitem ii on ii.customersk = oh.customer_sk and ii.productsk = sp.sk

where ii.invoicesk is not null


select count(*) 
from public.order_history oh 
	inner join public.job_history jh on jh.order_sk = oh.sk
	inner join vbyd_job_history vjh on vjh.sk = jh.sk
	inner join public.storeproduct sp on sp.product_gnm_sk = jh.product_sk
	left join (select ii.*,year,day,month,dow,doy,day from  invoiceitem ii
	inner join vbyd_invoiceitem vii on vii.invoicesk = ii.invoicesk) ii on ii.customersk = oh.customer_sk and ii.productsk = sp.sk  and ii.storesk = oh.store_sk
where 
	ii."year" = vjh.year and ii.doy = vjh.doy and vjh.year = 2017
	
------------------------------------------------------------------------------
select ii.doy,vjh.doy,ii.dow,vjh.dow,ii.invoicesk,vjh.sk
from public.order_history oh 
	inner join public.job_history jh on jh.order_sk = oh.sk
	inner join vbyd_job_history vjh on vjh.sk = jh.sk
	inner join public.storeproduct sp on sp.product_gnm_sk = jh.product_sk
	left join (select ii.*,year,day,month,dow,doy,day from  invoiceitem ii
	inner join vbyd_invoiceitem vii on vii.invoicesk = ii.invoicesk) ii on ii.customersk = oh.customer_sk and ii.productsk = sp.sk and ii.storesk = oh.store_sk
where 
	ii."year" = vjh.year and ii.doy = vjh.doy and vjh.year = 2017
	
	
----------------------------------------------------------------------------
	
select ii.doy,vjh.doy,ii.dow,vjh.dow,ii.invoicesk,vjh.sk
from public.order_history oh 
	inner join public.job_history jh on jh.order_sk = oh.sk
	inner join vbyd_job_history vjh on vjh.sk = jh.sk
	inner join public.storeproduct sp on sp.product_gnm_sk = jh.product_sk
	left join (select ii.*,year,day,month,dow,doy from  invoiceitem ii
	inner join vbyd_invoiceitem vii on vii.invoicesk = ii.invoicesk) ii on ii.customersk = oh.customer_sk and ii.productsk = sp.sk and ii.storesk = oh.store_sk
where 
	ii."year" = vjh.year and ii.doy <> vjh.doy and vjh.year = 2017
--------------------------------------------------------------------------------
-- populate invoice_orders

insert into invoice_orders(store_sk,invoice_sk,order_sk) 
select oh.store_sk,ii.invoicesk,oh.sk
from public.order_history oh 
	inner join public.job_history jh on jh.order_sk = oh.sk
	inner join vbyd_job_history vjh on vjh.sk = jh.sk
	inner join public.storeproduct sp on sp.product_gnm_sk = jh.product_sk
	left join (select ii.*,year,day,month,dow,doy from  invoiceitem ii
	inner join vbyd_invoiceitem vii on vii.invoicesk = ii.invoicesk) ii on ii.customersk = oh.customer_sk and ii.productsk = sp.sk and ii.storesk = oh.store_sk
where 
	ii."year" = vjh.year and ii.doy <> vjh.doy and vjh.year = 2017
	
-- how many invoice orders
select * from invoice_orders
select count(*)	from (select  store_sk,invoice_sk,order_sk, count(*) from invoice_orders group by store_sk,invoice_sk,order_sk) x
	



-----------------------------------------------------------------------------------
(select * from  invoiceitem ii
inner join vbyd_invoiceitem vii on vii.invoicesk = ii.invoicesk) ii

select count(*) from public.order_history

select * from public.invoiceitem ii 
left join public.storeproduct sp on sp.sk = ii.productsk

select count(*) from public.invoiceitem ii 
left join public.storeproduct sp on sp.sk = ii.productsk
left join public.job_history jh on jh.product_sk = sp.product_gnm_sk
where 
	sp.sk is not null and jh.sk is not null 
	

-- vbyd_invoiceitem

	
select * from public.order_history oh 
inner join job_history jh on jh.order_sk = oh.sk


-- 
select order_type,job_type, count(*) from public.order_history oh 
inner join job_history jh on jh.order_sk = oh.sk
inner join 
group by oh.order_type,jh.job_type order by count(*) desc

select job_type, count(*) from public.job_history oh group by oh.job_type order by count(*) desc;

select * from store_stockmovement sm 

--------------------------------------------------------------------------------------------------------
-- investigate missing 
-- 
select count(*) from store_stockmovement sm where invoice_sk <> 0



--- calculate price average -----

-- formula

-- product_cost
select count(*) from store_stockmovement where adjustment > 0 and product_gnm_sk > 0

-- stock movements by optomertrist

-- non gnm stock
select vss.year, vss.month, vss.doy,s.name,count(*) from store_stockmovement ss 
inner join vbyd_store_stockmovement vss on vss.sk = ss.sk
inner join public.store s on s.sk = ss.store_sk
where adjustment > 0 and product_gnm_sk = 0 and vss.year = 2017
group by vss.year, vss.month, vss.doy, s.name
order by vss.year, vss.month desc, vss.doy, s.name

--  gnm stock
select vss.year, vss.month, vss.doy,s.name,count(*) from store_stockmovement ss 
inner join vbyd_store_stockmovement vss on vss.sk = ss.sk
inner join public.store s on s.sk = ss.store_sk
where adjustment > 0 and product_gnm_sk > 0 and vss.year = 2017
group by vss.year, vss.month, vss.doy, s.name
order by vss.year, vss.month desc, vss.doy, s.name


select * from product_gnm pg left join stock_price_history sph on sph.


select * from public.store_stockmovement sm where sm.stock_price_history_sk

select * from public.store_stockmovement sm where sm.stock_price_history_sk

select frame_brand, count(*) from product_gnm pg group by pg.frame_brand


-- umatched orders
select count(*) from order_history oh left join invoice_orders io on io.order_sk = oh.sk
where io.sk is  null

select count(*) 

-- 
select count(*) from invoice ii left join invoice_orders io on io.invoice_sk = ii.sk
where io.sk is  null

select * from order_history

SET search_path = public;

--- valid orders range by store
select s.sk as store_sk ,s.name ,order_type, min(source_created) as first_order_date,max(source_created) as last_order_date,count(*) as total 
into store_orders_period
from order_history oh 
	inner join "store" s on s.sk = oh.store_sk
	where date_part('year',source_created) >= 2017
group by s.sk,name,order_type order by name,min(source_created),s.name asc


select sum(total) from store_orders_period

select case when 

create or replace function within_dow_range ( d1 timestamp, d2 timestamp) returns boolean
as $$
begin
	select d1, d2;
	
end;
$$ LANGUAGE plpgsql;

drop function within_dow_range( d1 timestamp, d2 timestamp);

create or replace function within_dow_range ( d1 timestamp, d2 timestamp) 
returns setof record
as $$
begin
	select d1 as date1, d2 as date2;
end;
$$ LANGUAGE plpgsql;


select date1,date2 from within_dow_range ('2017-06-07 12:00:00','2017-06-07 12:00:00') as x(date1::timestamp, date2::timestamp)


	
	