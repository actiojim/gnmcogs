--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.3
-- Dumped by pg_dump version 10.0

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: gnm; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA gnm;


--
-- Name: stage; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA stage;


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = gnm, pg_catalog;

--
-- Name: add_ranged_product_to_catalogues(character varying, character varying, character varying, timestamp without time zone); Type: FUNCTION; Schema: gnm; Owner: -
--

CREATE FUNCTION add_ranged_product_to_catalogues(p_product_id character varying, p_price_change_reason character varying, p_modified_user character varying, p_modified_date_time timestamp without time zone) RETURNS integer
    LANGUAGE plpgsql
    AS $$
-- Function to add newly ranged products to catalogues. A product becomes ranged the first time it is assigned a
-- status other than 'Initial'.
DECLARE
  product_row product%ROWTYPE;
  product_state_row product_state%ROWTYPE;
  rows_inserted INTEGER := 0;

BEGIN

  FOR product_row IN
    SELECT * FROM product WHERE id = p_product_id
  LOOP
    SELECT * INTO product_state_row FROM product_state WHERE id = product_row.current_state_id;

    -- If the product has been ranged (assigned a non-initial state)...
    IF product_state_row.state_type <> 'Initial' AND
      NOT EXISTS(SELECT id FROM product_state WHERE state_type <> 'Initial' AND product_id = p_product_id AND id <> product_state_row.id) THEN

      -- Ensure the product has been added to all catalogues which are linked to ranges (catalogue sources) that include
      -- this product
      -- Unique constraint on product_price will prevent duplicate row insertions
      -- Unique constraint errors will be ignored with the ON CONFLICT DO NOTHING clause
      INSERT INTO product_price (product_coll_id, product_id, price_change_reason, modified_user, modified_date_time, created_user, created_date_time)
      SELECT catalogue_attr.product_coll_id, range_mem.product_id, p_price_change_reason, p_modified_user, p_modified_date_time, p_modified_user, p_modified_date_time 
      FROM product_coll_all_member range_mem
      JOIN product_coll range ON range.coll_type = 'CatalogueSource' AND range_mem.product_coll_id = range.id
      JOIN product_coll_attribute catalogue_attr ON catalogue_attr.name = 'catalogueSourceId' AND catalogue_attr.value::BIGINT = range.id
      WHERE range_mem.product_id = p_product_id
      ON CONFLICT DO NOTHING;

      GET DIAGNOSTICS rows_inserted = ROW_COUNT;

    END IF;

  END LOOP;

  RETURN rows_inserted;

END;
$$;


--
-- Name: add_selector_products_to_catalogues(bigint, character varying, character varying, timestamp without time zone); Type: FUNCTION; Schema: gnm; Owner: -
--

CREATE FUNCTION add_selector_products_to_catalogues(selector_id bigint, price_change_reason character varying, p_modified_user character varying, p_modified_date_time timestamp without time zone) RETURNS void
    LANGUAGE plpgsql
    AS $$
-- Function to add products to catalogues where those products are in a selector collection and do not already
-- exist in catalogues based on the range (catalogue source collection) containing the selector
BEGIN
  INSERT INTO product_price (product_coll_id, product_id, price_change_reason, modified_user, modified_date_time, created_user, created_date_time)
  SELECT catalogue_attr.product_coll_id, product_id, price_change_reason, p_modified_user, p_modified_date_time, p_modified_user, p_modified_date_time 
  FROM product_coll_all_member selector_mem
  JOIN product_coll_tree pct ON pct.child_id = selector_mem.product_coll_id
  JOIN product_coll_attribute catalogue_attr ON catalogue_attr.value::BIGINT = pct.parent_id AND catalogue_attr.name = 'catalogueSourceId'
  WHERE selector_mem.product_coll_id = selector_id
  AND product_id NOT IN (SELECT product_id FROM product_price WHERE product_coll_id = catalogue_attr.product_coll_id AND product_id = selector_mem.product_id);

END;
$$;


--
-- Name: brand_tier_propagate(); Type: FUNCTION; Schema: gnm; Owner: -
--

CREATE FUNCTION brand_tier_propagate() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
-- Function to force price recalculation when the brand tier attribute of a brand has changed
DECLARE
  change_reason CHARACTER VARYING(255) := NULL;
BEGIN
  IF COALESCE(NEW.brand_tier_id,-1) != COALESCE(OLD.brand_tier_id,-1) THEN
    change_reason := CASE
      WHEN NEW.brand_tier_id IS NULL THEN 'Brand has been unassigned from a brand tier'
      WHEN OLD.brand_tier_id IS NULL THEN 'Brand has been assigned to a brand tier'
      ELSE 'Brand has been assigned to a different brand tier'
      END;

    UPDATE product
    SET price_change_reason = change_reason,
        modified_user = NEW.modified_user,
        modified_date_time = NEW.modified_date_time
    WHERE brand_id = NEW.id;
  END IF;

  RETURN NEW;
END;
$$;


--
-- Name: calculate_product_retail_prices(); Type: FUNCTION; Schema: gnm; Owner: -
--

CREATE FUNCTION calculate_product_retail_prices() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
-- Function to calculate retail prices for a product
DECLARE
  gst_rate DECIMAL;
  product_row product%ROWTYPE;
  price_policy_row price_policy%ROWTYPE;
  retail_price DECIMAL := NULL;
  pricing_changed BOOLEAN;
  price_error VARCHAR(255) := NULL;

BEGIN

  -- RAISE NOTICE 'calculate_product_retail_prices called with ID %', NEW.id;

  -- Get the product for which retail price is to be calculated
  SELECT * INTO product_row FROM product
  WHERE id = NEW.product_id;

  <<block>>
  BEGIN
    -- If there has been an error with the product supplier prices, then don't calculate the retail price
    IF product_row.actual_cost IS NULL THEN
      price_error := product_row.price_error;
      EXIT block;
    END IF;

    -- RAISE NOTICE 'product_row.actual_cost = %', product_row.actual_cost;

    -- Get the price_policy applicable to this product
    SELECT * INTO price_policy_row FROM price_policy
    WHERE id = product_row.price_policy_id;

    -- Margin price policy
    --
    IF price_policy_row.type = 'margin' THEN

      -- Override pricing by using a different margin percentage
      IF NEW.price_override_method = 'overrideMarginPercent' THEN
        IF NEW.override_margin_percent IS NULL THEN
          price_error := 'Override margin percent is missing';
          EXIT block;
        END IF;

        -- Margin percentage will be applied to the calculated actual cost
        NEW.override_retail_price := product_row.actual_cost / (1 - NEW.override_margin_percent / 100);

      -- Override pricing by simply specifying the retail price
      ELSEIF NEW.price_override_method = 'overrideRetailPrice' THEN
        IF NEW.override_retail_price IS NULL THEN
          price_error := 'Override retail price is missing';
          EXIT block;
        END IF;

        -- Calculate the margin percentage use for the override
        NEW.override_margin_percent := 100 * (NEW.override_retail_price - product_row.actual_cost) / NEW.override_retail_price;

      ELSEIF NEW.price_override_method IS NOT NULL THEN
        price_error := 'Price override method is invalid';
        EXIT block;
      END IF;

      retail_price := CASE WHEN NEW.override_retail_price IS NOT NULL THEN NEW.override_retail_price ELSE product_row.supplier_rrp END;

      -- Not applicable to margin pricing, so ensure it is NULL
      NEW.override_markup_factor := NULL;

    --  Markup price policy
    --
    ELSEIF price_policy_row.type = 'markup' THEN
      -- Override pricing by using a different markup factor
      IF NEW.price_override_method = 'overrideMarkupFactor' THEN
        IF NEW.override_markup_factor IS NULL THEN
          price_error := 'Override markup factor is missing ';
          EXIT block;
        END IF;

        -- Apply the override markup factor
        NEW.override_retail_price := product_row.actual_cost * NEW.override_markup_factor;

      -- Override pricing by simply specifying the retail price
      ELSEIF NEW.price_override_method = 'overrideRetailPrice' THEN
        IF NEW.override_retail_price IS NULL THEN
          price_error := 'Override retail price is missing';
          EXIT block;
        END IF;

        -- Calculate the markup factor used for the override
        NEW.override_markup_factor := NEW.override_retail_price / product_row.actual_cost;

      ELSE
        IF NEW.price_override_method IS NOT NULL THEN
          price_error := 'Price override method is invalid';
          EXIT block;
        END IF;
      END IF;

      retail_price := CASE WHEN NEW.override_retail_price IS NOT NULL THEN NEW.override_retail_price ELSE product_row.calculated_retail_price END;

      -- Not applicable to markup pricing, so ensure it is NULL
      NEW.override_margin_percent := NULL;

    --  Actual price policy
    --
    ELSEIF price_policy_row.type = 'actual' THEN
      -- Simply use the override retail price
      retail_price := NEW.override_retail_price;

      -- Calculate the margin, but prevent division errors
      IF NEW.override_retail_price IS NOT NULL AND NEW.override_retail_price <> 0 THEN
        NEW.override_margin_percent := 100 * (NEW.override_retail_price - product_row.actual_cost) / NEW.override_retail_price;
      ELSE
        NEW.override_margin_percent := NULL;
      END IF;

      -- Not applicable to margin pricing, so ensure it is NULL
      NEW.override_markup_factor := NULL;

    END IF;

    IF retail_price IS NULL THEN
      price_error := 'Unable to determine retail price';
      EXIT block;
    END IF;

    -- Round the retail price if rounding is enabled, but not if the price is being overridden
    IF price_policy_row.enable_rounding AND NEW.override_retail_price IS NULL THEN
      SELECT round_retail_price(retail_price) INTO retail_price;
    END IF;

    -- Finally set the retail price. Price will be rounded to match the data type of the column.
    NEW.retail_price := retail_price;

    -- Set GST rate based on whether GST applies to the product cost
    gst_rate := CASE WHEN product_row.actual_cost_gst = 0 THEN 0 ELSE 0.1000 END;

    -- RAISE NOTICE 'gst_rate = %', gst_rate;

    -- Assign the gst amounts. Amounts will be rounded to match the column data type.
    NEW.override_retail_price_gst := CASE WHEN NEW.override_retail_price IS NULL THEN NULL ELSE NEW.override_retail_price * gst_rate / (1 + gst_rate) END;
    NEW.retail_price_gst := retail_price * gst_rate / (1 + gst_rate);

    -- RAISE NOTICE 'gst_rate = %, NEW.override_retail_price_gst = %, NEW.retail_price_gst = %', gst_rate, NEW.override_retail_price_gst, NEW.retail_price_gst;

  END;

  -- If an error was detected do not generate retail prices
  NEW.price_error := price_error;
  IF price_error IS NOT NULL THEN
    NEW.retail_price := NULL;
    NEW.retail_price_gst := NULL;
    -- RAISE NOTICE 'price_error = %', price_error;
  END IF;

  -- Determine whether pricing has changed
  pricing_changed := TG_OP = 'INSERT' OR product_pricing_changed(OLD, NEW);

  -- RAISE NOTICE 'pricing_changed = %', pricing_changed;

  -- If pricing has changed, then ensure a reason is given
  IF pricing_changed THEN
    IF NEW.price_change_reason IS NULL THEN
      IF TG_OP = 'INSERT' THEN
        NEW.price_change_reason := 'Product price inserted';
      ELSEIF TG_OP = 'UPDATE' THEN
        NEW.price_change_reason := 'Product price updated';
      END IF;
    END IF;

    RETURN NEW;

  ELSE
    -- If pricing has not changed, return the old row so that duplicate history rows are not inserted
    RETURN OLD;

  END IF;
END;
$$;


--
-- Name: calculate_supplier_product_prices(); Type: FUNCTION; Schema: gnm; Owner: -
--

CREATE FUNCTION calculate_supplier_product_prices() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
-- Function to product cost using the applicable price policy
DECLARE
  new_product product%ROWTYPE;
  old_product product%ROWTYPE;
  price_policy_row price_policy%ROWTYPE;
  gst_rate DECIMAL;
  actual_cost DECIMAL := NULL;
  calculated_retail_price DECIMAL := NULL;
  preserve_prices BOOLEAN;
  field_changed BOOLEAN;
  price_error VARCHAR(255) := NULL;

BEGIN

  <<block>>
  BEGIN

    -- Lookup applicable active policy. The lookup order is:
    -- product, brand, brand_tier, supplier

    -- Check if there is a price policy specifically for this product
    SELECT * INTO price_policy_row FROM price_policy
    WHERE supplier_id = NEW.supplier_id AND product_id = NEW.id AND active_id = 0;

    IF NOT FOUND AND NEW.brand_id IS NOT NULL THEN

      -- Check if there is a price policy for the product brand
      SELECT * INTO price_policy_row FROM price_policy
      WHERE supplier_id = NEW.supplier_id AND brand_id = NEW.brand_id AND active_id = 0;

      IF NOT FOUND THEN

        -- Check if there is a price policy for any brand tier containing this product brand
        SELECT * INTO price_policy_row FROM price_policy
        WHERE supplier_id = NEW.supplier_id AND brand_tier_id = (SELECT brand_tier_id FROM brand WHERE id = NEW.brand_id) AND active_id = 0;

      END IF;

    END IF;

    IF NOT FOUND THEN
      -- If a price policy has still not been found, then use the default supplier pricing policy
      SELECT * INTO price_policy_row FROM price_policy
      WHERE supplier_id = NEW.supplier_id
      AND brand_tier_id IS NULL
      AND brand_id IS NULL
      AND product_id IS NULL
      AND active_id = 0;

    END IF;

    -- Unable to find a price_policy for this product!
    IF price_policy_row IS NULL THEN
      price_error := 'Unable to find a price policy for this product';
      EXIT block;
    END IF;

    NEW.price_policy_id := price_policy_row.id;

    -- Margin price policy
    --
    IF price_policy_row.type = 'margin' THEN

      -- supplier_rrp must be provided for margin type pricing
      IF NEW.supplier_rrp IS NULL OR NEW.supplier_rrp = 0 THEN
        price_error := 'Supplier RRP is required for Margin price policy';
        EXIT block;
      ELSEIF NEW.supplier_rrp_gst IS NULL THEN
        price_error := 'Supplier RRP GST is required for Margin price policy';
        EXIT block;
      ELSEIF price_policy_row.margin_percent IS NULL THEN
        price_error := 'No margin percentage specified for Margin price policy';
        EXIT block;
      END IF;

      -- GST is either 10% or 0%
      gst_rate := CASE WHEN NEW.supplier_rrp_gst = 0 THEN 0 ELSE 0.1000 END;

      actual_cost := NEW.supplier_rrp * (1 - price_policy_row.margin_percent / 100);

      calculated_retail_price := null;

    -- Markup price policy
    --
    ELSEIF price_policy_row.type = 'markup' THEN
      -- supplier_cost must be provided for markup type pricing
      IF NEW.supplier_cost IS NULL OR NEW.supplier_cost = 0 THEN
        price_error := 'Supplier Cost is required for Markup price policy';
        EXIT block;
      ELSEIF NEW.supplier_cost_gst IS NULL THEN
        price_error := 'Supplier Cost GST is required for Markup price policy';
        EXIT block;
      ELSEIF price_policy_row.markup_factor IS NULL THEN
        price_error := 'No markup factor specified for Markup price policy';
        EXIT block;
      END IF;

      -- GST is either 10% or 0%
      gst_rate := CASE WHEN NEW.supplier_cost_gst = 0 THEN 0 ELSE 0.1000 END;

      IF price_policy_row.cost_discount_percent IS NOT NULL THEN
        actual_cost := NEW.supplier_cost * (1 - price_policy_row.cost_discount_percent / 100);
      ELSE
        actual_cost := NEW.supplier_cost;
      END IF;

      calculated_retail_price := actual_cost * price_policy_row.markup_factor;

    -- Actual price policy
    --
    ELSEIF price_policy_row.type = 'actual' THEN
        -- Check whether to preserve prices when the price policy is being changed to 'actual'.
      IF TG_OP = 'UPDATE' THEN
        new_product := NEW;
        old_product := OLD;
        SELECT preserve_prices_for_actual_price_policy(old_product, new_product, TRUE) INTO preserve_prices;
        IF preserve_prices THEN
          -- Preserve pricing by copying actual_cost to override_cost. It will then copied back to actual_cost meaning
          -- that pricing will be unchanged even though the pricing policy has been changed.
          --RAISE NOTICE 'preserving prices: set override_cost=%', NEW.actual_cost;
          NEW.override_cost := NEW.actual_cost;
          NEW.override_cost_gst := NEW.actual_cost_gst;
        END IF;
      END IF;

      -- override_cost must be provided for actual type pricing
      IF NEW.override_cost IS NULL OR NEW.override_cost = 0 THEN
        price_error := 'Override Cost is required for Actual price policy';
        EXIT block;
      END IF;
      IF NEW.override_cost_gst IS NULL THEN
        price_error := 'Override Cost GST is required for Actual price policy';
        EXIT block;
      END IF;

      -- GST is either 10% or 0%
      gst_rate := CASE WHEN NEW.override_cost_gst = 0 THEN 0 ELSE 0.1000 END;

      actual_cost := NEW.override_cost;

      calculated_retail_price := null;

    END IF;

    -- Assign all the calculated prices. Prices will be rounded to match the data type of the columns.
    NEW.actual_cost := actual_cost;
    NEW.calculated_retail_price := calculated_retail_price;

    -- Assign all the gst amounts. Amounts will be rounded to match the column data type.
    NEW.actual_cost_gst := CASE WHEN actual_cost IS NULL THEN NULL ELSE actual_cost * gst_rate / (1 + gst_rate) END;
    NEW.calculated_retail_price_gst := CASE WHEN calculated_retail_price IS NULL THEN NULL ELSE calculated_retail_price * gst_rate / (1 + gst_rate) END;

  END;

  -- If an error was detected wipe all calculated price fields
  IF price_error IS NOT NULL THEN
    NEW.actual_cost := NULL;
    NEW.actual_cost_gst := NULL;
    NEW.calculated_retail_price := NULL;
    NEW.calculated_retail_price_gst := NULL;
    NEW.price_error := price_error;
  END IF;

  -- See if pricing has changed. Include GST amounts just in case they also changed.
  -- Use COALESCE as comparisons involving NULL values result in undefined values
  field_changed := TG_OP = 'INSERT' OR
                   COALESCE(NEW.price_error,'') != COALESCE(OLD.price_error,'') OR
                   COALESCE(NEW.actual_cost,-1) != COALESCE(OLD.actual_cost,-1) OR
                   COALESCE(NEW.actual_cost_gst,-1) != COALESCE(OLD.actual_cost_gst,-1) OR
                   COALESCE(NEW.calculated_retail_price,-1) != COALESCE(OLD.calculated_retail_price,-1) OR
                   COALESCE(NEW.calculated_retail_price_gst,-1) != COALESCE(OLD.calculated_retail_price_gst,-1) OR
                   COALESCE(NEW.supplier_rrp,-1) != COALESCE(OLD.supplier_rrp,-1) OR
                   COALESCE(NEW.supplier_rrp_gst,-1) != COALESCE(OLD.supplier_rrp_gst,-1) OR
                   COALESCE(NEW.supplier_cost,-1) != COALESCE(OLD.supplier_cost,-1) OR
                   COALESCE(NEW.supplier_cost_gst,-1) != COALESCE(OLD.supplier_cost_gst,-1) OR
                   COALESCE(NEW.override_cost,-1) != COALESCE(OLD.override_cost,-1) OR
                   COALESCE(NEW.override_cost_gst,-1) != COALESCE(OLD.override_cost_gst,-1) OR
                   COALESCE(NEW.price_policy_id,-1) != COALESCE(OLD.price_policy_id,-1);

  -- If pricing has changed, then ensure a reason is given
  IF field_changed THEN

    IF NEW.price_change_reason IS NULL THEN
      IF TG_OP = 'INSERT' THEN
        NEW.price_change_reason := 'Product inserted';
      ELSEIF TG_OP = 'UPDATE' THEN
        NEW.price_change_reason := 'Product updated';
      END IF;
    END IF;

    NEW.price_error := price_error;

  ELSE
    -- If pricing has not changed, do not update the reason
    NEW.price_change_reason := OLD.price_change_reason;

  END IF;

  RETURN NEW;
END;
$$;


--
-- Name: drop_all_table_policies(character varying); Type: FUNCTION; Schema: gnm; Owner: -
--

CREATE FUNCTION drop_all_table_policies(table_name character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
-- Function to drop all row level security policies that have beene created for a table
DECLARE
  policy_name VARCHAR;
BEGIN
  FOR policy_name IN SELECT polname FROM pg_policy WHERE polrelid = (SELECT oid FROM pg_class WHERE relname = table_name)
  LOOP
      EXECUTE 'DROP POLICY ' || policy_name || ' ON "' || table_name || '"';
  END LOOP;
END;
$$;


--
-- Name: dropoldroles(); Type: FUNCTION; Schema: gnm; Owner: -
--

CREATE FUNCTION dropoldroles() RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE
  rolename VARCHAR;
BEGIN
  FOR rolename IN SELECT rolname FROM pg_roles WHERE rolname LIKE 'user%' OR rolname IN (
    'admin_role', 'finance_role', 'operations_role', 'store_role', 'supplier_role'
  )
  LOOP
    EXECUTE 'DROP ROLE ' || rolename;
  END LOOP;
END;
$$;


--
-- Name: execute_product_coll_all_member(bigint); Type: FUNCTION; Schema: gnm; Owner: -
--

CREATE FUNCTION execute_product_coll_all_member(product_coll_all_id bigint) RETURNS TABLE(product_coll_id bigint, product_id character varying)
    LANGUAGE plpgsql
    AS $$
-- Function to return IDs of all products in a specified collections, regardless of whether those collections are dynamic
-- or static or the table in which those members might be stored.
-- It need to be updated as new collection types are created.
DECLARE
  product_coll_row product_coll%ROWTYPE;

BEGIN
  SELECT * INTO product_coll_row FROM product_coll WHERE id = product_coll_all_id;
  -- RAISE NOTICE 'product_coll_row id = %', product_coll_row.id;

  IF FOUND THEN
    -- RAISE NOTICE 'Collection found with product_coll_id %', product_coll_all_id;
    CASE product_coll_row.coll_type
      -- Collections stored in the product_price table
      WHEN 'Catalogue' THEN
        -- RAISE NOTICE 'fetch Catalogue collection members for product_coll_id %', product_coll_all_id;
        RETURN QUERY SELECT product_coll_all_id, pp.product_id FROM product_price pp WHERE pp.product_coll_id = product_coll_all_id;
      
      -- Hierarchical collections with child collections stored in the product_coll_tree
      WHEN 'CatalogueSource' THEN
        -- RAISE NOTICE 'fetch CatalogueSource collection members for product_coll_id %', product_coll_all_id;
        RETURN QUERY SELECT product_coll_all_id, pcam.product_id FROM product_coll_all_member pcam WHERE pcam.product_coll_id IN (SELECT child_id FROM product_coll_tree WHERE parent_id = product_coll_all_id);

      -- Collections stored in the product_state_request table
      WHEN 'ProductStateRequestBundle' THEN
        -- RAISE NOTICE 'fetch ProductStateRequestBundle collection members for product_coll_id %', product_coll_all_id;
        RETURN QUERY SELECT product_coll_all_id, psr.product_id FROM product_state_request psr WHERE psr.product_coll_id = product_coll_all_id;
      
      -- Collections dynamically generated from a filter
      WHEN 'DynamicFilter' THEN 
        -- RAISE NOTICE 'fetch DynamicFilter collection members for product_coll_id %', product_coll_all_id;      
        RETURN QUERY SELECT product_coll_all_id, pcfm.product_id FROM product_coll_filter_member pcfm WHERE pcfm.product_coll_id = product_coll_all_id;
      
      -- Assume all other collections are stored in the product_coll_member table
      ELSE 
	-- RAISE NOTICE 'fetch other collection members for product_coll_id %', product_coll_all_id;      
        RETURN QUERY SELECT product_coll_all_id, pcm.product_id FROM product_coll_member pcm WHERE pcm.product_coll_id = product_coll_all_id;
    END CASE;
  ELSE  
    -- RAISE NOTICE 'Collection NOT found with product_coll_id %', product_coll_all_id;
    -- Return empty result set
    RETURN QUERY SELECT product_coll_all_id, id FROM product WHERE FALSE;
          
  END IF;

  RETURN;
END;
$$;


--
-- Name: execute_product_coll_filter(bigint); Type: FUNCTION; Schema: gnm; Owner: -
--

CREATE FUNCTION execute_product_coll_filter(product_coll_filter_id bigint) RETURNS TABLE(product_coll_id bigint, product_id character varying)
    LANGUAGE plpgsql
    AS $_$
-- Return all the IDs of products that satisfy the given filter
DECLARE
  product_coll_filter_row product_coll_filter%ROWTYPE;

BEGIN
  SELECT * INTO product_coll_filter_row FROM product_coll_filter WHERE id = product_coll_filter_id;

  IF FOUND THEN
	  RETURN QUERY EXECUTE 'SELECT ' || product_coll_filter_id || '::BIGINT, * FROM ' || product_coll_filter_row.coll_filter || '($1)'
	    USING product_coll_filter_row.coll_filter_parameter_json;
  ELSE
    -- Return empty result set
    RETURN QUERY SELECT product_coll_filter_id, id FROM product WHERE FALSE;
  END IF;

  RETURN;
END;
$_$;


--
-- Name: fixjobids(); Type: FUNCTION; Schema: gnm; Owner: -
--

CREATE FUNCTION fixjobids() RETURNS void
    LANGUAGE plpgsql
    AS $_$
DECLARE
  maxJobId BIGINT;
  newJobIdInt BIGINT;
  jobIdRhs text;
  newJobId text;
  jobRec record;
BEGIN
  SELECT last_value + increment_by INTO maxJobid FROM static_id_seq;
  raise notice 'static_id_seq next value is %', maxJobid;

  EXECUTE 'ALTER SEQUENCE job_id_sequence RESTART WITH ' || maxJobid;

  select last_value + increment_by into newJobIdInt from static_id_seq;
  LOOP
    jobIdRhs := ltrim(to_char(newJobIdInt,'99999'));
    newJobId := to_char(newJobIdInt,'0000000009');

    select * into jobRec from job where id ~ ( '.*' || jobIdRhs || '$') and id !~ ('0{7}.*' || jobIdRhs) order by created_date_time desc;
    if found then
      raise notice 'Replace job ID % with %', jobRec.id, newJobId;
      update order_info set name = replace(name,jobRec.id,newJobId) where id in (select id from order_info where name like '%.' || jobRec.id || '.%');
      update job set id = newJobId, job_id = newJobId where id = jobRec.id;
    end if;
    newJobIdInt := newJobIdInt - 1;
    EXIT WHEN newJobIdInt = 0;
  END LOOP;
END;
$_$;


--
-- Name: get_product_ids_for_product_type(jsonb); Type: FUNCTION; Schema: gnm; Owner: -
--

CREATE FUNCTION get_product_ids_for_product_type(parameter_json jsonb) RETURNS TABLE(product_id character varying)
    LANGUAGE plpgsql
    AS $$
-- Return all the product IDs for a given product type
DECLARE
  filter_product_types VARCHAR[];

BEGIN
  filter_product_types = ARRAY(SELECT jsonb_array_elements_text(parameter_json -> 'product_types'));

  RETURN QUERY
  SELECT p.id FROM product p, product_state ps
  WHERE p.type = ANY(filter_product_types)
  AND ps.id = p.current_state_id
  AND ps.state_type != 'Initial';

  RETURN;
END;
$$;


--
-- Name: get_product_ids_for_supplier_id(jsonb); Type: FUNCTION; Schema: gnm; Owner: -
--

CREATE FUNCTION get_product_ids_for_supplier_id(parameter_json jsonb) RETURNS TABLE(product_id character varying)
    LANGUAGE plpgsql
    AS $$
-- Return all the product IDs for a given supplier ID
DECLARE
  filter_supplier_id BIGINT;

BEGIN
  filter_supplier_id = parameter_json ->> 'supplier_id';

  RETURN QUERY
  SELECT p.id FROM product p, product_state ps
  WHERE p.supplier_id = filter_supplier_id
  AND ps.id = p.current_state_id
  AND ps.state_type != 'Initial';

  RETURN;
END;
$$;


--
-- Name: get_product_ids_for_supplier_id_product_type(jsonb); Type: FUNCTION; Schema: gnm; Owner: -
--

CREATE FUNCTION get_product_ids_for_supplier_id_product_type(parameter_json jsonb) RETURNS TABLE(product_id character varying)
    LANGUAGE plpgsql
    AS $$
-- Return all the product IDs for a given supplier ID and product type
DECLARE
  filter_supplier_id BIGINT;
  filter_product_types VARCHAR[];

BEGIN
  filter_supplier_id = parameter_json ->> 'supplier_id';
  filter_product_types = ARRAY(SELECT jsonb_array_elements_text(parameter_json -> 'product_types'));

  RETURN QUERY
  SELECT p.id FROM product p, product_state ps
  WHERE p.supplier_id = filter_supplier_id
  AND p.type = ANY(filter_product_types)
  AND ps.id = p.current_state_id
  AND ps.state_type != 'Initial';

  RETURN;
END;
$$;


--
-- Name: insert_missing_frame_selectors(); Type: FUNCTION; Schema: gnm; Owner: -
--

CREATE FUNCTION insert_missing_frame_selectors() RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE
  supplier_id BIGINT;
  supplier_code VARCHAR;
  supplier_name VARCHAR;
  product_type VARCHAR;
  product_type_plural VARCHAR;
  product_coll_id BIGINT;
  json_text VARCHAR;
  
-- Add 'selectors' missing from the AllFrameAndSundryProducts catalogue source collection
--
BEGIN
  FOR supplier_id, supplier_code, supplier_name, product_type IN
    SELECT DISTINCT s.id, s.supplier_id, s.name, p.type
    FROM product p, supplier s
    WHERE s.id = p.supplier_id
    AND s.id IN (
      SELECT DISTINCT p.supplier_id FROM product p
      WHERE type = 'frame'
      AND p.supplier_id NOT IN (
        SELECT t.supplier_id FROM ( 
          SELECT DISTINCT (coll_filter_parameter_json-> 'supplier_id' )::VARCHAR::BIGINT AS supplier_id 
          FROM product_coll_filter pcf 
          WHERE pcf.id IN (SELECT child_id FROM product_coll_tree WHERE parent_id = (SELECT id FROM product_coll WHERE name = 'AllFrameAndSundryProducts'))
        ) t 
        WHERE t.supplier_id is NOT NULL
      )
    )
  LOOP
    product_type_plural = product_type || (CASE right(product_type, 1) WHEN 's' THEN  'es' ELSE 's' END);

    -- Start the collection
    INSERT INTO product_coll (name, description, coll_type) VALUES
      (UPPER(supplier_code || '-' || product_type), supplier_name || ' ' || product_type_plural, 'MaterialisedFilter');

    product_coll_id = LASTVAL();

    -- Add the collection filter
    json_text = '{"supplier_id": ' || supplier_id || ', "product_type": "' || product_type || '"}';
    RAISE NOTICE '%', json_text;
    INSERT INTO product_coll_filter (id, coll_filter, coll_filter_parameter_json) VALUES
      (product_coll_id, 'get_product_ids_for_supplier_id_product_type', json_text::jsonb);

    -- Make this filter a child of the catalogue source collection
    INSERT INTO product_coll_tree (parent_id, child_id) VALUES
      ((SELECT id FROM product_coll WHERE name = 'AllFrameAndSundryProducts'), product_coll_id);

  END LOOP;
END;
$$;


SET default_with_oids = false;

--
-- Name: product; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE product (
    id character varying(255) NOT NULL,
    product_id character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    type character varying(255) NOT NULL,
    apn character varying(255),
    description character varying(255),
    current_state_id bigint,
    supplier_rrp numeric(19,2),
    supplier_rrp_gst numeric(19,2),
    supplier_cost numeric(19,2),
    supplier_cost_gst numeric(19,2),
    created_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    created_date_time timestamp with time zone DEFAULT now() NOT NULL,
    modified_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    modified_date_time timestamp with time zone DEFAULT now() NOT NULL,
    supplier_id bigint NOT NULL,
    image_id bigint,
    supplier_sku character varying(255) NOT NULL,
    brand_id bigint,
    price_policy_id bigint,
    actual_cost numeric(19,2),
    actual_cost_gst numeric(19,2),
    calculated_retail_price numeric(19,2),
    calculated_retail_price_gst numeric(19,2),
    override_cost numeric(19,2),
    override_cost_gst numeric(19,2),
    price_change_reason character varying(255),
    price_error character varying(255),
    consignment boolean DEFAULT false,
    rating integer DEFAULT 10,
    pending_state_request_id bigint
);


--
-- Name: preserve_prices_for_actual_price_policy(product, product); Type: FUNCTION; Schema: gnm; Owner: -
--

CREATE FUNCTION preserve_prices_for_actual_price_policy(old_product product, new_product product) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
-- For determining whether prices should be preserved when changing to the 'actual' price policy
DECLARE
  new_price_policy_type VARCHAR;

BEGIN
  --RAISE NOTICE 'preserve_prices_for_actual_price_policy: old_product = %, new_product = %', old_product, new_product;
  
  -- Only consider whether prices should be preserved if the product is being updated
  IF old_product.price_policy_id IS NOT NULL AND new_product.price_policy_id IS NOT NULL THEN
    SELECT type INTO new_price_policy_type FROM price_policy WHERE id = new_product.price_policy_id;
    -- Only preserve prices if the new price policy type is 'actual' and the price policy type was
    -- previously not 'actual'. This can be inferred from the fact of there having been an actual_cost but
    -- no override_cost.
    IF new_price_policy_type = 'actual'
       AND old_product.override_cost IS NULL
       AND old_product.actual_cost IS NOT NULL THEN
      --RAISE NOTICE 'preserve_prices_for_actual_price_policy return TRUE';
      RETURN TRUE;
    END IF;
  END IF;

  -- Don't try to preserve prices
  --RAISE NOTICE 'preserve_prices_for_actual_price_policy return FALSE';
  RETURN FALSE;
END;
$$;


--
-- Name: preserve_prices_for_actual_price_policy(product, product, boolean); Type: FUNCTION; Schema: gnm; Owner: -
--

CREATE FUNCTION preserve_prices_for_actual_price_policy(old_product product, new_product product, before_update boolean) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
-- For determining whether prices should be preserved when changing to the 'actual' price policy
DECLARE
  new_price_policy_type VARCHAR;

BEGIN
  --RAISE NOTICE 'preserve_prices_for_actual_price_policy: old_product = %, new_product = %', old_product, new_product;

  -- Only consider whether prices should be preserved if the product is being updated and there is a cost/price to preserve
  IF old_product.price_policy_id IS NOT NULL AND new_product.price_policy_id IS NOT NULL AND old_product.actual_cost IS NOT NULL THEN
    SELECT type INTO new_price_policy_type FROM price_policy WHERE id = new_product.price_policy_id;
    -- Only consider preserving prices if the price policy type is 'actual'. If the previous actual cost did not match
    -- the override_cost then it can also be deduced that the policy type has changed, in which case we may want to
    -- preserve prices.
    IF new_price_policy_type = 'actual' AND COALESCE(old_product.override_cost, -1) != old_product.actual_cost THEN
      IF before_update THEN
        -- Only proceed with preserving actual cost if the override_cost is not being updated as part of the change to
        -- 'actual' pricing
        IF COALESCE(old_product.override_cost, -1) = COALESCE(new_product.override_cost, -1) THEN
          --RAISE NOTICE 'preserve_prices_for_actual_price_policy return TRUE';
          RETURN TRUE;
        END IF;
      ELSE
        -- If the trigger that updates product costs has already run, then only proceed with preserving catalogue
        -- prices if actual_cost was preserved
        IF COALESCE(new_product.override_cost, -1) = COALESCE(new_product.actual_cost, -1) THEN
          --RAISE NOTICE 'preserve_prices_for_actual_price_policy return TRUE';
          RETURN TRUE;
        END IF;
      END IF;
    END IF;
  END IF;

  -- Don't try to preserve prices
  --RAISE NOTICE 'preserve_prices_for_actual_price_policy return FALSE';
  RETURN FALSE;
END;
$$;


--
-- Name: price_policy_propagate(); Type: FUNCTION; Schema: gnm; Owner: -
--

CREATE FUNCTION price_policy_propagate() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
-- Function to force price recalculation when a price policy is created,
-- updated or deleted
DECLARE
  oldScope VARCHAR;
  newScope VARCHAR;
  action VARCHAR;
  change_reason VARCHAR := NULL;
  change_user VARCHAR := NULL;
  change_date_time timestamp with time zone := NULL;
BEGIN
  -- Build the price_change_reason
  oldScope := CASE
    WHEN TG_OP IN ('UPDATE', 'DELETE') THEN price_policy_scope(OLD)
    ELSE NULL
    END;
  newScope := CASE
    WHEN TG_OP IN ('UPDATE', 'INSERT') IS NOT NULL THEN price_policy_scope(NEW)
    ELSE NULL
    END;

  IF TG_OP IN ('UPDATE', 'INSERT') AND newScope IS NULL THEN
    RAISE EXCEPTION SQLSTATE '22000' USING
      MESSAGE = 'Price policy error',
      DETAIL = 'No supplier, brand tier, brand or product has been chosen for this policy';
  END IF;

  action := CASE
    WHEN TG_OP = 'UPDATE' THEN 'updated'
    WHEN TG_OP = 'DELETE' THEN 'deleted'
    WHEN TG_OP = 'INSERT' THEN 'inserted'
    ELSE NULL
    END;

  change_reason := COALESCE(newScope, oldScope) || ' price policy ' || action;

  -- Add more info to the change reason for updates
  IF TG_OP = 'UPDATE' THEN
    IF OLD.active_id = 0 AND NEW.active_id > 0 THEN
      change_reason := change_reason || ', deactivated';
    END IF;
    IF OLD.active_id > 0 AND NEW.active_id = 0 THEN
      change_reason := change_reason || ', activated';
    END IF;
    IF oldScope != newScope THEN
      change_reason := change_reason || ', scope changed from ' || oldScope;
    END IF;
  END IF;

  IF TG_OP IN ('UPDATE', 'INSERT') THEN
    change_user := NEW.modified_user;
    change_date_time := NEW.modified_date_time;
  ELSE
    -- Unfortunately, when a policy is being deleted we do not have the current G&M username, so the only option is to
    -- use the postgresql user/role name.
    change_user := current_user;
    change_date_time := current_timestamp;
  END IF;

  -- Force price calculation for products that this policy previously applied to
  IF TG_OP IN ('UPDATE', 'DELETE') THEN
    UPDATE product
    SET price_change_reason = change_reason,
        modified_user = change_user,
        modified_date_time = change_date_time
    WHERE price_policy_id = OLD.id;
  END IF;

  -- Force price calculation for products that this policy now applies to.
  -- With updates, it is possible that the scope has changed (e.g. change of brand, or change from product to brand
  -- scope) so need to determine the applicable products again
  IF TG_OP IN ('UPDATE', 'INSERT') THEN
    -- Policy applies to a single product
    IF NEW.product_id IS NOT NULL THEN
      -- For updates, only force calculation if policy now applies to a new product
      IF TG_OP = 'INSERT' OR NEW.active_id != OLD.active_id OR NEW.product_id != COALESCE(OLD.product_id,'') THEN
        UPDATE product
        SET price_change_reason = change_reason,
            modified_user = change_user,
            modified_date_time = change_date_time
        WHERE id = NEW.product_id;
      END IF;

    -- Policy applies to all products having the specified brand
    ELSEIF NEW.brand_id IS NOT NULL THEN
      -- For updates, only force calculation if policy now applies to a new brand
      IF TG_OP = 'INSERT' OR NEW.active_id != OLD.active_id OR NEW.brand_id != COALESCE(OLD.brand_id,-1) THEN
        UPDATE product
        SET price_change_reason = change_reason,
            modified_user = change_user,
            modified_date_time = change_date_time
        WHERE brand_id = NEW.brand_id;
      END IF;

    -- Policy applies to all products having brands in the specified brand tier
    ELSEIF NEW.brand_tier_id IS NOT NULL THEN
      -- For updates, only force calculation if policy now applies to a new brand tier
      IF TG_OP = 'INSERT' OR NEW.active_id != OLD.active_id OR NEW.brand_tier_id != COALESCE(OLD.brand_tier_id,-1) THEN
        UPDATE product
        SET price_change_reason = change_reason,
            modified_user = change_user,
            modified_date_time = change_date_time
        WHERE brand_id IN (SELECT id FROM brand WHERE brand_tier_id = NEW.brand_tier_id);
      END IF;

    -- Policy applies to all products for the specified supplier
    -- In practice there should be no pricing calculations triggered by this step because:
    -- * when a supplier policy is first created, there should be no existing products
    -- * a policy cannot be changed to a supplier scope
    -- Nevertheless, keep this step for completeness
    ELSEIF NEW.supplier_id IS NOT NULL THEN
      -- For updates, only force calculation if policy now applies to a new supplier
      IF TG_OP = 'INSERT' OR NEW.active_id != OLD.active_id OR NEW.supplier_id != COALESCE(OLD.supplier_id,-1) THEN
        UPDATE product
        SET price_change_reason = change_reason,
            modified_user = change_user,
            modified_date_time = change_date_time
        WHERE supplier_id = NEW.supplier_id;
      END IF;
    END IF;
  END IF;

  RETURN NEW;
END;
$$;


--
-- Name: price_policy; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE price_policy (
    id bigint NOT NULL,
    supplier_id bigint NOT NULL,
    name character varying(100) NOT NULL,
    type character varying(30) NOT NULL,
    brand_tier_id bigint,
    brand_id bigint,
    product_id character varying(8),
    active_id bigint NOT NULL,
    cost_discount_percent numeric(5,2),
    markup_factor numeric(5,2),
    margin_percent numeric(5,2),
    enable_rounding boolean NOT NULL,
    created_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    created_date_time timestamp with time zone DEFAULT now() NOT NULL,
    modified_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    modified_date_time timestamp with time zone DEFAULT now() NOT NULL,
    CONSTRAINT chk1_price_policy CHECK (((active_id = 0) OR (active_id = id))),
    CONSTRAINT chk2_price_policy CHECK ((((
CASE (brand_tier_id IS NULL)
    WHEN true THEN 0
    ELSE 1
END +
CASE (brand_id IS NULL)
    WHEN true THEN 0
    ELSE 1
END) +
CASE (product_id IS NULL)
    WHEN true THEN 0
    ELSE 1
END) <= 1))
);


--
-- Name: price_policy_scope(price_policy); Type: FUNCTION; Schema: gnm; Owner: -
--

CREATE FUNCTION price_policy_scope(price_policy_row price_policy) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
-- Function to force price recalculation when a price policy is created,
-- updated or deleted
DECLARE
  scope VARCHAR;
BEGIN
  scope := CASE
    WHEN price_policy_row.product_id IS NOT NULL THEN 'Product'
    WHEN price_policy_row.brand_id IS NOT NULL THEN 'Brand'
    WHEN price_policy_row.brand_tier_id IS NOT NULL THEN 'Brand tier'
    WHEN price_policy_row.supplier_id IS NOT NULL THEN 'Supplier'
    ELSE NULL
    END;
  RETURN scope;
END;
$$;


--
-- Name: product_coll_member_delete(); Type: FUNCTION; Schema: gnm; Owner: -
--

CREATE FUNCTION product_coll_member_delete() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
-- Update the total members in a product collection whenever a new member is deleted
BEGIN
  UPDATE product_coll
    SET total_members = COALESCE(total_members, 0) - 1,
        members_modified_date_time = NOW() AT TIME ZONE 'utc'
    WHERE id = OLD.product_coll_id;
  RETURN NULL;
END;
$$;


--
-- Name: product_coll_member_insert(); Type: FUNCTION; Schema: gnm; Owner: -
--

CREATE FUNCTION product_coll_member_insert() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
-- Update the total members in a product collection whenever a new member is inserted
BEGIN
  UPDATE product_coll
    SET total_members = COALESCE(total_members, 0) + 1,
        -- Match timestamp to when member was inserted
        members_modified_date_time = NEW.created_date_time
    WHERE id = NEW.product_coll_id;
  RETURN NULL;
END;
$$;


--
-- Name: product_coll_tree_delete(); Type: FUNCTION; Schema: gnm; Owner: -
--

CREATE FUNCTION product_coll_tree_delete() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
-- Update the total members in a parent product collection whenever a child collection is removed from a parent
DECLARE
  total_members_diff BIGINT;

BEGIN
  SELECT COALESCE(total_members, 0) INTO total_members_diff FROM product_coll WHERE id = OLD.child_id;
  IF total_members_diff != 0 THEN
    UPDATE product_coll
      SET total_members = COALESCE(total_members, 0) - total_members_diff,
          members_modified_date_time = NOW() AT TIME ZONE 'utc'
      WHERE id = OLD.parent_id;
  END IF;  
  RETURN NULL;
END;
$$;


--
-- Name: product_coll_tree_insert(); Type: FUNCTION; Schema: gnm; Owner: -
--

CREATE FUNCTION product_coll_tree_insert() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
-- Update the total members in a parent product collection whenever a child collection is added to a parent
DECLARE
  total_members_diff BIGINT;

BEGIN
  SELECT COALESCE(total_members, 0) INTO total_members_diff FROM product_coll WHERE id = NEW.child_id;
  IF total_members_diff != 0 THEN
    UPDATE product_coll
      SET total_members = COALESCE(total_members, 0) + total_members_diff,
          members_modified_date_time = NOW() AT TIME ZONE 'utc'
      WHERE id = NEW.parent_id;
  END IF;
  RETURN NULL;
END;
$$;


--
-- Name: product_coll_update(); Type: FUNCTION; Schema: gnm; Owner: -
--

CREATE FUNCTION product_coll_update() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
-- Update the total members in a parent product collection whenever a child collection is updated
DECLARE
  total_members_diff BIGINT;

BEGIN
  total_members_diff = COALESCE(NEW.total_members, 0) - COALESCE(OLD.total_members, 0);
  UPDATE product_coll
    SET total_members = COALESCE(total_members, 0) + total_members_diff,
        -- Only update members_modified_date_time if members of child collection have actually changed
        members_modified_date_time = GREATEST(members_modified_date_time, NEW.members_modified_date_time)
    WHERE id IN (SELECT parent_id FROM product_coll_tree WHERE child_id = NEW.id);
  RETURN NULL;
END;
$$;


--
-- Name: product_price_delete(); Type: FUNCTION; Schema: gnm; Owner: -
--

CREATE FUNCTION product_price_delete() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
-- Update the total members in a product collection whenever a new member is deleted
BEGIN
  UPDATE product_coll
    SET total_members = COALESCE(total_members, 0) - 1,
        members_modified_date_time = NOW() AT TIME ZONE 'utc'
    WHERE id = OLD.product_coll_id;
  RETURN NULL;
END;
$$;


--
-- Name: product_price_insert(); Type: FUNCTION; Schema: gnm; Owner: -
--

CREATE FUNCTION product_price_insert() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
-- Update the total members in a product collection whenever a new member is inserted
BEGIN
  UPDATE product_coll
    SET total_members = COALESCE(total_members, 0) + 1,
        -- Match timestamp to when member was inserted
        members_modified_date_time = NEW.created_date_time
    WHERE id = NEW.product_coll_id;
  RETURN NULL;
END;
$$;


--
-- Name: product_price; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE product_price (
    id bigint NOT NULL,
    product_coll_id bigint NOT NULL,
    product_id character varying(255) NOT NULL,
    override_retail_price numeric(19,2),
    override_retail_price_gst numeric(19,2),
    override_markup_factor numeric(5,2),
    override_margin_percent numeric(5,2),
    price_override_method character varying(30),
    retail_price numeric(19,2),
    retail_price_gst numeric(19,2),
    price_change_reason character varying(255),
    price_error character varying(255),
    created_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    created_date_time timestamp without time zone DEFAULT timezone('utc'::text, now()) NOT NULL,
    modified_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    modified_date_time timestamp without time zone DEFAULT timezone('utc'::text, now()) NOT NULL
);


--
-- Name: product_pricing_changed(product_price, product_price); Type: FUNCTION; Schema: gnm; Owner: -
--

CREATE FUNCTION product_pricing_changed(old_product_price product_price, new_product_price product_price) RETURNS boolean
    LANGUAGE plpgsql
    AS $_$
-- Determines whether retail pricing for a product has actually changed
DECLARE
  pricing_changed BOOLEAN;

BEGIN
  -- Use COALESCE as comparisons involving NULL values result in undefined values
  pricing_changed := COALESCE(new_product_price.price_error,'') != COALESCE(old_product_price.price_error,'') OR
                     COALESCE(new_product_price.retail_price,-1) != COALESCE(old_product_price.retail_price,-1) OR
                     COALESCE(new_product_price.retail_price_gst,-1) != COALESCE(old_product_price.retail_price_gst,-1) OR
                     COALESCE(new_product_price.override_retail_price,-1) != COALESCE(old_product_price.override_retail_price,-1) OR
                     COALESCE(new_product_price.override_retail_price_gst,-1) != COALESCE(old_product_price.override_retail_price_gst,-1) OR
                     COALESCE(new_product_price.override_margin_percent,-1) != COALESCE(old_product_price.override_margin_percent,-1) OR
                     COALESCE(new_product_price.override_markup_factor,-1) != COALESCE(old_product_price.override_markup_factor,-1) OR
                     COALESCE(new_product_price.price_override_method,'X') != COALESCE(old_product_price.price_override_method,'X');

  -- Determine whether the change is only to the price_change_reason
  IF NOT pricing_changed AND COALESCE(new_product_price.price_change_reason,'X') != COALESCE(old_product_price.price_change_reason,'X') THEN
    -- Treat changes to price_change_reason as a pricing change, except where that change is only due to an import. Changes resulting from an
    -- import can be identified from the price_change_reason starting 3-4 upper case characters and ending with the
    -- word "import", e.g. "XLSX product import".
    IF new_product_price.price_change_reason IS NOT NULL AND new_product_price.price_change_reason !~ '^[[:upper:]]{3,4}[[:space:]].*import$' THEN
      pricing_changed := TRUE;
    END IF;
  END IF;

  RETURN pricing_changed;
END;
$_$;


--
-- Name: product_state_request_delete(); Type: FUNCTION; Schema: gnm; Owner: -
--

CREATE FUNCTION product_state_request_delete() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
-- Update the total members in a product collection whenever a new member is deleted
BEGIN
  UPDATE product_coll
    SET total_members = COALESCE(total_members, 0) - 1,
        members_modified_date_time = NOW() AT TIME ZONE 'utc'
    WHERE id = OLD.product_coll_id;
  RETURN NULL;
END;
$$;


--
-- Name: product_state_request_insert(); Type: FUNCTION; Schema: gnm; Owner: -
--

CREATE FUNCTION product_state_request_insert() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
-- Update the total members in a product collection whenever a new member is inserted
BEGIN
  UPDATE product_coll
    SET total_members = COALESCE(total_members, 0) + 1,
        -- Match timestamp to when member was inserted
        members_modified_date_time = NEW.created_date_time
    WHERE id = NEW.product_coll_id;
  RETURN NULL;
END;
$$;


--
-- Name: remove_product_assortment_product_duplicate_rows(integer); Type: FUNCTION; Schema: gnm; Owner: -
--

CREATE FUNCTION remove_product_assortment_product_duplicate_rows(maxremoveduplicates integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
-- For removing rows duplicate rows from the product_assortment_product table, i.e. where a product appears multiple
-- times in an assortment
DECLARE
  pap_cursor CURSOR FOR SELECT assortment_id, product_id FROM product_assortment_product ORDER BY assortment_id, product_id FOR UPDATE;
  new_row product_assortment_product%ROWTYPE;
  old_row product_assortment_product%ROWTYPE;
  isDuplicate BOOLEAN := false;
  cntRemoveDuplicates INTEGER := 0;

BEGIN
  RAISE NOTICE 'Begin removing duplicate rows from product_assortment_product';

  OPEN pap_cursor;

  LOOP
    FETCH pap_cursor INTO new_row;
    EXIT WHEN NOT FOUND;

    IF old_row.product_id IS NOT NULL THEN
	    isDuplicate := new_row.assortment_id = old_row.assortment_id AND new_row.product_id = old_row.product_id;

      IF isDuplicate THEN
        -- Delete the duplicate
        --RAISE NOTICE 'Delete duplicate row: %', new_row;
	      DELETE FROM product_assortment_product WHERE CURRENT OF pap_cursor;

	      cntRemoveDuplicates := cntRemoveDuplicates + 1;

	      EXIT WHEN cntRemoveDuplicates >= maxRemoveDuplicates;
	  
        old_row := new_row;
      ELSE
        old_row := new_row;
      END IF;

    ELSE
      old_row := new_row;    
    END IF;

  END LOOP;

  CLOSE pap_cursor;

  RAISE NOTICE 'Total duplicate rows removed from product_assortment_product: %', cntRemoveDuplicates;

  RETURN cntRemoveDuplicates;
END;
$$;


--
-- Name: remove_product_price_history_duplicate_rows(integer); Type: FUNCTION; Schema: gnm; Owner: -
--

CREATE FUNCTION remove_product_price_history_duplicate_rows(maxremoveduplicates integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
-- For removing rows that have been unnecessarily created in the product_price_history table every time a change was
-- made to the product table
DECLARE
  pph_cursor CURSOR FOR SELECT * FROM product_price_history ORDER BY product_coll_id, product_id, id FOR UPDATE;
  new_row product_price_history%ROWTYPE;
  old_row product_price_history%ROWTYPE;
  isMatchingProductPrice BOOLEAN := false;
  isDuplicate BOOLEAN := false;
  cntRemoveDuplicates INTEGER := 0;

BEGIN
  RAISE NOTICE 'Begin removing duplicate rows from product_price_history';

  OPEN pph_cursor;

  LOOP
    FETCH pph_cursor INTO new_row;
    EXIT WHEN NOT FOUND;

    IF old_row.product_id IS NOT NULL THEN
      isMatchingProductPrice := COALESCE(new_row.product_coll_id,-1) = COALESCE(old_row.product_coll_id,-1) AND
                                COALESCE(new_row.product_id,'') = COALESCE(old_row.product_id,'');
      -- Check for duplicate if this row is for the same product and catalogue                       
      IF isMatchingProductPrice THEN
	isDuplicate := COALESCE(new_row.override_retail_price,-1) = COALESCE(old_row.override_retail_price,-1) AND
                       COALESCE(new_row.override_retail_price_gst,-1) = COALESCE(old_row.override_retail_price_gst,-1) AND
                       COALESCE(new_row.override_markup_factor,-1) = COALESCE(old_row.override_markup_factor,-1) AND
                       COALESCE(new_row.override_margin_percent,-1) = COALESCE(old_row.override_margin_percent,-1) AND
                       COALESCE(new_row.price_override_method,'') = COALESCE(old_row.price_override_method,'') AND
                       COALESCE(new_row.retail_price,-1) = COALESCE(old_row.retail_price,-1) AND
                       COALESCE(new_row.retail_price_gst,-1) = COALESCE(old_row.retail_price_gst,-1) AND
                       COALESCE(new_row.price_error,'') = COALESCE(old_row.price_error,'');
        IF isDuplicate THEN
          -- Correct the superseded data on the previous row
          --RAISE NOTICE 'Update superseded_date_time=% for id=%', new_row.superseded_date_time, old_row.id;
          UPDATE product_price_history SET superseded_date_time = new_row.superseded_date_time WHERE id = old_row.id;
          
          -- Delete the duplicate
          --RAISE NOTICE 'Delete duplicate row: %', new_row;
	  DELETE FROM product_price_history WHERE CURRENT OF pph_cursor;

	  cntRemoveDuplicates := cntRemoveDuplicates + 1;

	  EXIT WHEN cntRemoveDuplicates >= maxRemoveDuplicates;
	  
        ELSE
          --RAISE NOTICE 'Change found with product_price: %', new_row;
          old_row := new_row;    
        END IF;                     
      ELSE
        --RAISE NOTICE 'Next (not first) product_price: %', new_row;
        old_row := new_row;    
      END IF;

    ELSE
      old_row := new_row;    
    END IF;

  END LOOP;

  CLOSE pph_cursor;

  RETURN cntRemoveDuplicates;
END;
$$;


--
-- Name: revert_catalogue_import(bigint, timestamp without time zone, timestamp without time zone); Type: FUNCTION; Schema: gnm; Owner: -
--

CREATE FUNCTION revert_catalogue_import(p_supplier_id bigint, p_from_superseded_date_time timestamp without time zone, p_to_superseded_date_time timestamp without time zone) RETURNS void
    LANGUAGE plpgsql
    AS $$
-- Revert all catalogue prices updated by a catalogue import for the given supplier that were superseded in the given interval
DECLARE
  m_product_coll_id BIGINT;
  m_product_id VARCHAR(255);
  m_override_retail_price NUMERIC(19,2);
  m_override_retail_price_gst NUMERIC(19,2);
  m_override_markup_factor NUMERIC(5,2);
  m_override_margin_percent NUMERIC(5,2);
  m_price_override_method VARCHAR(30);
  count_rows_to_update BIGINT;
  count_rows_updated BIGINT;
  total_rows_updated BIGINT := 0;
  last_product_coll_id BIGINT := 0;
  last_product_id VARCHAR(255) := '0';
    
BEGIN
  SELECT COUNT(*) INTO count_rows_to_update FROM product_price_history
    WHERE superseded_date_time BETWEEN p_from_superseded_date_time AND p_to_superseded_date_time 
    AND price_change_reason = 'API import'       
    AND product_id IN (SELECT id FROM product WHERE supplier_id = p_supplier_id);

  RAISE NOTICE 'Total product_price rows that will be reverted = %', count_rows_to_update;
  
  FOR m_product_coll_id, m_product_id, m_override_retail_price, m_override_retail_price_gst, 
      m_override_markup_factor, m_override_margin_percent, m_price_override_method
  IN      
    SELECT product_coll_id, product_id, override_retail_price, override_retail_price_gst, override_markup_factor, override_margin_percent,
      price_override_method FROM product_price_history
    WHERE superseded_date_time BETWEEN p_from_superseded_date_time AND p_to_superseded_date_time 
    AND price_change_reason = 'API import'       
    AND product_id IN (SELECT id FROM product WHERE supplier_id = p_supplier_id)
    ORDER BY product_id, product_coll_id, modified_date_time
  LOOP
    IF m_product_coll_id != last_product_coll_id OR m_product_id != last_product_id THEN
      WITH ROWS AS (
        UPDATE product_price SET
          override_retail_price = m_override_retail_price,
          override_retail_price_gst = m_override_retail_price_gst,
          override_markup_factor = m_override_markup_factor,
          override_margin_percent = m_override_margin_percent,
          price_override_method = m_price_override_method,
          price_change_reason = 'Revert catalogue import',
          modified_date_time = NOW() AT TIME ZONE 'utc'
        WHERE product_coll_id = m_product_coll_id
        AND product_id = m_product_id
        RETURNING 1
      )
      SELECT COUNT(*) INTO count_rows_updated FROM rows;
      total_rows_updated := total_rows_updated + count_rows_updated;
      --RAISE NOTICE 'product_coll_id = %, product_id = %, m_override_retail_price = %, m_override_retail_price_gst = %, count_rows_updated = %', 
      --  m_product_coll_id, m_product_id, m_override_retail_price, m_override_retail_price_gst, count_rows_updated;
      
    END IF;

    last_product_coll_id = m_product_coll_id;
    last_product_id = m_product_id;

  END LOOP;

  RAISE NOTICE 'Total product_price rows actually updated = %', total_rows_updated;
  
  RETURN;
END;
$$;


--
-- Name: round_retail_price(numeric); Type: FUNCTION; Schema: gnm; Owner: -
--

CREATE FUNCTION round_retail_price(retail_price numeric) RETURNS numeric
    LANGUAGE plpgsql
    AS $_$
-- For rounding the product retail price that has been determined
DECLARE
  remainder DECIMAL;
BEGIN
  -- If rrp >= $400, round to the nearest 5
  IF retail_price >= 400 THEN
    remainder := retail_price % 5;
    IF remainder >= 2.50 THEN
      retail_price := retail_price + 5 - remainder;
    ELSE
      retail_price := retail_price - remainder;
    END IF;

  -- If 25 < rrp < $400, round to the nearest 9
  ELSEIF retail_price > 25 THEN
    remainder := retail_price % 10;
    IF remainder > 4 THEN
      retail_price := retail_price + 9 - remainder;
    ELSE
      retail_price := retail_price - remainder - 1;
    END IF;

  END IF;

  RETURN ROUND(retail_price);
END;
$_$;


--
-- Name: save_product_cost_history(); Type: FUNCTION; Schema: gnm; Owner: -
--

CREATE FUNCTION save_product_cost_history() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
-- Function to detect if any changes have been made to product cost fields and if so write a row to the
-- product_cost_history table
DECLARE
  field_changed BOOLEAN;

BEGIN
  -- Determine if any of the cost related fields have changed.
  field_changed = TG_OP = 'INSERT' OR
                  COALESCE(NEW.price_change_reason,'') != COALESCE(OLD.price_change_reason,'') OR
                  COALESCE(NEW.price_error,'') != COALESCE(OLD.price_error,'') OR
                  COALESCE(NEW.actual_cost,-1) != COALESCE(OLD.actual_cost,-1) OR
                  COALESCE(NEW.actual_cost_gst,-1) != COALESCE(OLD.actual_cost_gst,-1) OR
                  COALESCE(NEW.calculated_retail_price,-1) != COALESCE(OLD.calculated_retail_price,-1) OR
                  COALESCE(NEW.calculated_retail_price_gst,-1) != COALESCE(OLD.calculated_retail_price_gst,-1) OR
                  COALESCE(NEW.supplier_rrp,-1) != COALESCE(OLD.supplier_rrp,-1) OR
                  COALESCE(NEW.supplier_rrp_gst,-1) != COALESCE(OLD.supplier_rrp_gst,-1) OR
                  COALESCE(NEW.supplier_cost,-1) != COALESCE(OLD.supplier_cost,-1) OR
                  COALESCE(NEW.supplier_cost_gst,-1) != COALESCE(OLD.supplier_cost_gst,-1) OR
                  COALESCE(NEW.override_cost,-1) != COALESCE(OLD.override_cost,-1) OR
                  COALESCE(NEW.override_cost_gst,-1) != COALESCE(OLD.override_cost_gst,-1) OR
                  COALESCE(NEW.price_policy_id,-1) != COALESCE(OLD.price_policy_id,-1);

  IF field_changed THEN
    -- Supersede the old row
    UPDATE product_cost_history SET superseded_date_time = NEW.modified_date_time
    WHERE product_id = NEW.product_id AND superseded_date_time IS NULL;

    -- Save a copy of the new product cost fields
    INSERT INTO product_cost_history (
      product_id, price_policy_id,
      supplier_cost, supplier_cost_gst,
      actual_cost, actual_cost_gst,
      supplier_rrp, supplier_rrp_gst,
      calculated_retail_price, calculated_retail_price_gst,
      override_cost, override_cost_gst,
      price_change_reason, price_error,
      modified_user, modified_date_time, superseded_date_time)
    VALUES (
      NEW.product_id, NEW.price_policy_id,
      NEW.supplier_cost, NEW.supplier_cost_gst,
      NEW.actual_cost, NEW.actual_cost_gst,
      NEW.supplier_rrp, NEW.supplier_rrp_gst,
      NEW.calculated_retail_price, NEW.calculated_retail_price_gst,
      NEW.override_cost, NEW.override_cost_gst,
      NEW.price_change_reason, NEW.price_error,
      NEW.modified_user, NEW.modified_date_time, NULL
    );

  END IF;

  RETURN NULL;

END;
$$;


--
-- Name: save_product_price_history(); Type: FUNCTION; Schema: gnm; Owner: -
--

CREATE FUNCTION save_product_price_history() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
-- Function that writes to the product_price_history table any changes made to the product_price table

BEGIN
  -- Only save history if there has been a genuine change
  IF TG_OP = 'INSERT' OR product_pricing_changed(OLD, NEW) THEN
    -- Supersede the old row
    UPDATE product_price_history SET superseded_date_time = NEW.modified_date_time
    WHERE product_coll_id = NEW.product_coll_id AND product_id = NEW.product_id AND superseded_date_time IS NULL;

    INSERT INTO product_price_history (
      product_coll_id, product_id,
      override_retail_price, override_retail_price_gst, override_markup_factor, override_margin_percent, price_override_method,
      retail_price, retail_price_gst,
      price_change_reason, price_error,
      modified_user, modified_date_time, superseded_date_time)
    VALUES (
      NEW.product_coll_id, NEW.product_id,
      NEW.override_retail_price, NEW.override_retail_price_gst, NEW.override_markup_factor, NEW.override_margin_percent, NEW.price_override_method,
      NEW.retail_price, NEW.retail_price_gst,
      NEW.price_change_reason, NEW.price_error,
      NEW.modified_user, NEW.modified_date_time, NULL
    );

  END IF;

  RETURN NULL;

END;
$$;


--
-- Name: selector_assigned_to_range(); Type: FUNCTION; Schema: gnm; Owner: -
--

CREATE FUNCTION selector_assigned_to_range() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
-- Function to ensure products specified by a Selector are added to catalogues whenever a Selector is assigned to a
-- range (catalogue source collection)
DECLARE
  price_change_reason VARCHAR;

BEGIN
  -- Verify this is a range-selector parent-child relationship being inserted or updated
  IF EXISTS(SELECT id FROM product_coll WHERE id = NEW.parent_id AND coll_type = 'CatalogueSource')
     AND
     EXISTS(SELECT id FROM product_coll WHERE id = NEW.child_id AND coll_type = 'DynamicFilter') THEN

    IF TG_OP = 'INSERT' THEN
      price_change_reason = 'Selector assigned to catalogue collection (range)';
    ELSE
      price_change_reason = 'Update to selector assigned to catalogue collection (range)';
    END IF;

    -- Add any missing products to the catalogues
    PERFORM add_selector_products_to_catalogues(NEW.child_id, price_change_reason, NEW.created_user, NEW.created_date_time);

  END IF;

  RETURN NULL;
END;
$$;


--
-- Name: selector_updated(); Type: FUNCTION; Schema: gnm; Owner: -
--

CREATE FUNCTION selector_updated() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
-- When a Selector has been modified, ensure any new products in the Selector selection are added to catalogues
-- populated from this Selector
BEGIN
  -- Verify this is a Selector
  IF EXISTS(SELECT id FROM product_coll WHERE id = NEW.id AND coll_type = 'DynamicFilter') THEN

    -- Add any missing products to the catalogues
    PERFORM add_selector_products_to_catalogues(NEW.id, 'Selector updated', NEW.modified_user, NEW.modified_date_time);

  END IF;

  RETURN NULL;
END;
$$;


--
-- Name: supplier_product_prices_propagate(); Type: FUNCTION; Schema: gnm; Owner: -
--

CREATE FUNCTION supplier_product_prices_propagate() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
-- Ensure retail prices are updated after a change has been to the supplier pricing
DECLARE
  preserve_prices BOOLEAN := FALSE;
  old_product product%ROWTYPE;
  new_product product%ROWTYPE;

BEGIN
  --RAISE NOTICE 'supplier_product_prices_propagate: old_product = %, new_product = %', old_product, new_product;

  -- Determine if retail_price should be preserved with this update
  IF TG_OP = 'UPDATE' THEN
    old_product := OLD;
    new_product := NEW;
    SELECT preserve_prices_for_actual_price_policy(old_product, new_product, FALSE) INTO preserve_prices;
  END IF;

  --RAISE NOTICE 'supplier_product_prices_propagate: TG_OP = %, preserve_prices = %', TG_OP, preserve_prices;

  -- Changes to supplier related prices could affect retail prices for the product.
  -- Update all related product_price rows to force a recalculation of retail prices.
  UPDATE product_price
  SET price_change_reason = NEW.price_change_reason,
      modified_user = NEW.modified_user,
      modified_date_time = NEW.modified_date_time,
      override_retail_price = CASE WHEN preserve_prices THEN retail_price ELSE override_retail_price END
   WHERE product_id = NEW.id;

  RETURN NEW;

END;
$$;


--
-- Name: sync_catalogues(bigint); Type: FUNCTION; Schema: gnm; Owner: -
--

CREATE FUNCTION sync_catalogues(catalogue_source_id bigint) RETURNS void
    LANGUAGE plpgsql
    AS $$
-- Populate all the catalogues from the products in a catalogue source
-- Note this does not remove products from catalogues which are no longer in the catalogue source
DECLARE
  pc_row product_coll%ROWTYPE;
  catalogue_source_count BIGINT;
  catalogue_count BIGINT;

BEGIN
  FOR pc_row IN
    SELECT * FROM product_coll WHERE coll_type = 'Catalogue' AND id IN 
      (SELECT product_coll_id FROM product_coll_attribute WHERE name = 'catalogueSourceId' AND value = catalogue_source_id::VARCHAR)
  LOOP
    RAISE NOTICE 'catalogue source id = %, catalogue id = %', catalogue_source_id, pc_row.id;

    SELECT count(*) INTO catalogue_source_count FROM product_coll_all_member WHERE product_coll_id = catalogue_source_id;
    SELECT count(*) INTO catalogue_count FROM product_coll_all_member WHERE product_coll_id = pc_row.id;

    RAISE NOTICE 'Before sync catalogue source count = %, catalogue count = %', catalogue_source_count, catalogue_count;

    -- Insert any missing products into the catalogue
    INSERT INTO product_price (product_coll_id,product_id)
    SELECT pc_row.id, product_id
    FROM product_coll_all_member 
    WHERE product_coll_id = catalogue_source_id
    AND product_id NOT IN (
      SELECT product_id FROM product_price
      WHERE product_coll_id = pc_row.id
    );

    SELECT count(*) INTO catalogue_source_count FROM product_coll_all_member WHERE product_coll_id = catalogue_source_id;
    SELECT count(*) INTO catalogue_count FROM product_coll_all_member WHERE product_coll_id = pc_row.id;

    RAISE NOTICE 'Before sync catalogue source count = %, catalogue count = %', catalogue_source_count, catalogue_count;
    
  END LOOP;
END;
$$;


--
-- Name: sync_selectors(bigint); Type: FUNCTION; Schema: gnm; Owner: -
--

CREATE FUNCTION sync_selectors(catalogue_source_id bigint) RETURNS void
    LANGUAGE plpgsql
    AS $$
-- Populate all static child selectors for a catalogue source from their partner dynamic filter collections
-- Note this does not remove products from selectors which are no longer in the selector partners (e.g when product type is changed from frame to sunglasses)
DECLARE
  pct_row product_coll_tree%ROWTYPE;
  selector_partner_id BIGINT;
  selector_count BIGINT;
  selector_partner_count BIGINT;

BEGIN
  FOR pct_row IN
    SELECT * FROM product_coll_tree WHERE parent_id = catalogue_source_id
  LOOP
    -- Get it's partner dynamic filter
    SELECT value::BIGINT INTO selector_partner_id FROM product_coll_attribute WHERE product_coll_id = pct_row.child_id AND NAME = 'partnerProductCollId';

    RAISE NOTICE 'selector id = %, selector partner id = %', pct_row.child_id, selector_partner_id;

    SELECT count(*) INTO selector_count FROM product_coll_all_member WHERE product_coll_id = pct_row.child_id;
    SELECT count(*) INTO selector_partner_count FROM product_coll_all_member WHERE product_coll_id = selector_partner_id;

    RAISE NOTICE 'Before sync selector count = %, selector partner count = %', selector_count, selector_partner_count;

    -- Insert any missing products into the selector static collection
    INSERT INTO product_coll_member (product_coll_id,product_id)
    SELECT pct_row.child_id, product_id
    FROM product_coll_all_member 
    WHERE product_coll_id = selector_partner_id
    AND product_id NOT IN (
      SELECT product_id FROM product_coll_member
      WHERE product_coll_id = pct_row.child_id
    );

    SELECT count(*) INTO selector_count FROM product_coll_all_member WHERE product_coll_id = pct_row.child_id;
    SELECT count(*) INTO selector_partner_count FROM product_coll_all_member WHERE product_coll_id = selector_partner_id;

    RAISE NOTICE 'After sync selector count = %, selector partner count = %', selector_count, selector_partner_count;
    
  END LOOP;
END;
$$;


--
-- Name: temp_migrate_all_existing_orders(); Type: FUNCTION; Schema: gnm; Owner: -
--

CREATE FUNCTION temp_migrate_all_existing_orders() RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE
  order_row "order"%ROWTYPE;
BEGIN
  -- Fetch all orders that do not have order_info records prefixed with 'product['. This will prevent migration of
  -- orders created after composite jobs were added.
  FOR order_row IN SELECT * FROM "order" WHERE id NOT IN (SELECT order_id FROM order_info WHERE name LIKE 'product[%')
  LOOP
    PERFORM temp_migrate_existing_order(order_row);
  END LOOP;
END;
$$;


--
-- Name: order; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE "order" (
    id character varying(255) NOT NULL,
    order_id character varying(255) NOT NULL,
    invoice_id character varying(255),
    customer_id bigint NOT NULL,
    store_id bigint,
    submitted_date_time timestamp with time zone NOT NULL,
    queue character varying(255) NOT NULL,
    status character varying(255) NOT NULL,
    created_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    created_date_time timestamp with time zone DEFAULT now() NOT NULL,
    modified_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    modified_date_time timestamp with time zone DEFAULT now() NOT NULL,
    address_id bigint,
    workflow character varying(255),
    order_type_id character varying(40)
);


--
-- Name: temp_migrate_existing_order("order"); Type: FUNCTION; Schema: gnm; Owner: -
--

CREATE FUNCTION temp_migrate_existing_order(order_row "order") RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE
  job_row job%ROWTYPE;
  product_row product%ROWTYPE;
  product_frame_row product_frame%ROWTYPE;
  product_lens_row product_lens%ROWTYPE;
  product_alias_row product_alias%ROWTYPE;
  supplier_row supplier%ROWTYPE;
  iJob INTEGER;
  namePrefix VARCHAR;
  isLeftLens BOOLEAN := false;
  isRightLens BOOLEAN := false;  
  attrValue VARCHAR;
BEGIN
  RAISE NOTICE 'Migrating order ID=%', order_row.id;
    
  -- Remove all old order_info rows for this order
  DELETE FROM order_info WHERE order_id = order_row.id;

  iJob := 0;

  FOR job_row IN SELECT * FROM job WHERE order_id = order_row.id ORDER BY id
  LOOP

    RAISE NOTICE 'Migrating job ID=% for order ID=%, iJob=%', job_row.id, order_row.id, iJob;

    SELECT * FROM product INTO product_row WHERE id = job_row.product_id;
    IF product_row.type = 'frame' THEN
      SELECT * FROM product_frame INTO product_frame_row WHERE id = product_row.id;
    ELSE
      product_frame_row := NULL;
    END IF;
    IF product_row.type = 'lens' THEN
      SELECT * FROM product_lens INTO product_lens_row WHERE id = product_row.id;
    ELSE
      product_lens_row := NULL;
    END IF;
    SELECT * FROM product_alias INTO product_alias_row WHERE product_id = product_row.id LIMIT 1;
    SELECT * FROM supplier INTO supplier_row WHERE id = product_row.supplier_id;

    --
    -- Add new order_info rows
    --

    namePrefix := 'product[' || trim(to_char(iJob, '9')) || '].';

    -- Common product attributes
    INSERT INTO order_info (order_id,name,type,value) VALUES
    (order_row.id, namePrefix || 'id', 'STRING', product_row.id),
    (order_row.id, namePrefix || 'name', 'STRING', product_row.name),
    (order_row.id, namePrefix || 'supplierSku', 'STRING', product_alias_row.alias_id),
    (order_row.id, namePrefix || 'brand', 'STRING', product_row.brand),
    (order_row.id, namePrefix || 'description', 'STRING', product_row.description),
    (order_row.id, namePrefix || 'type', 'STRING', product_row.type),
    (order_row.id, namePrefix || 'supplier.name', 'STRING', supplier_row.name),
    (order_row.id, namePrefix || 'imageId', 'INTEGER', product_row.image_id);

    namePrefix := namePrefix || 'attr.';

    -- Frame attributes
    -- IF product_frame_row IS NOT NULL THEN means all fields must be non-null!
    IF NOT product_frame_row IS NULL THEN
      INSERT INTO order_info (order_id,name,type,value) VALUES
      (order_row.id, namePrefix || 'frame_material', 'STRING', product_frame_row.material),
      (order_row.id, namePrefix || 'frame_bridge_size', 'STRING', product_frame_row.bridge_size),
      (order_row.id, namePrefix || 'frame_colour', 'STRING', product_frame_row.colour),
      (order_row.id, namePrefix || 'frame_distance_between_lenses', 'STRING', product_frame_row.distance_between_lenses),
      (order_row.id, namePrefix || 'frame_diagonal', 'STRING', product_frame_row.diagonal),
      (order_row.id, namePrefix || 'frame_rx', 'STRING', product_frame_row.rx),
      (order_row.id, namePrefix || 'frame_type', 'STRING', product_frame_row.type),
      (order_row.id, namePrefix || 'frame_pantoscopic_angle', 'STRING', product_frame_row.pantoscopic_angle),
      (order_row.id, namePrefix || 'frame_gender', 'STRING', product_frame_row.gender),
      (order_row.id, namePrefix || 'frame_model', 'STRING', product_frame_row. model),
      (order_row.id, namePrefix || 'frame_depth', 'STRING', product_frame_row.depth),
      (order_row.id, namePrefix || 'frame_eye_size', 'STRING', product_frame_row.eye_size),
      (order_row.id, namePrefix || 'frame_wrap_angle', 'STRING', product_frame_row.wrap_angle),
      (order_row.id, namePrefix || 'frame_temple_length', 'STRING', product_frame_row.temple_length);
    END IF;

    -- Lens attributes
    -- IF product_lens_row IS NOT NULL THEN means all fields must be non-null!
    IF NOT product_lens_row IS NULL THEN
      INSERT INTO order_info (order_id,name,type,value) VALUES
      (order_row.id, namePrefix || 'treatment', 'STRING', product_lens_row.treatment),
      (order_row.id, namePrefix || 'focal_type', 'STRING', product_lens_row.focal_type),
      (order_row.id, namePrefix || 'high_power', 'STRING', product_lens_row.high_power),
      (order_row.id, namePrefix || 'type', 'STRING', product_lens_row.type),
      (order_row.id, namePrefix || 'segment_height', 'STRING', product_lens_row.segment_height),
      (order_row.id, namePrefix || 'calculation_type', 'STRING', product_lens_row.calculation_type),
      (order_row.id, namePrefix || 'low_add', 'STRING', product_lens_row.low_add),
      (order_row.id, namePrefix || 'segment_size', 'STRING', product_lens_row.segment_size),
      (order_row.id, namePrefix || 'material', 'STRING', product_lens_row.material),
      (order_row.id, namePrefix || 'size', 'STRING', product_lens_row.size),
      (order_row.id, namePrefix || 'stock_or_grind', 'STRING', product_lens_row.stock_or_grind),
      (order_row.id, namePrefix || 'high_add', 'STRING', product_lens_row.high_add),
      (order_row.id, namePrefix || 'refractive_index', 'STRING', product_lens_row.refractive_index),
      (order_row.id, namePrefix || 'eyetalk_id', 'STRING', product_lens_row.eyetalk_id),
      (order_row.id, namePrefix || 'low_power', 'STRING', product_lens_row.low_power);
    END IF;

    --
    -- Add new job attribute rows
    --

    namePrefix := product_row.type;
    
    -- For lens jobs need to prefix with left or right
    IF product_row.type = 'lens' THEN
      SELECT EXISTS(SELECT * FROM job_attribute WHERE job_id = job_row.id AND name like 'left_%') INTO isLeftLens;
      SELECT EXISTS(SELECT * FROM job_attribute WHERE job_id = job_row.id AND name like 'right_%') INTO isRightLens;
      IF isLeftLens THEN
        namePrefix := 'left_' || namePrefix;
      ELSEIF isRightLens THEN
        namePrefix := 'right_' || namePrefix;
      END IF;
    END IF;

    INSERT INTO job_attribute (job_id,name,type,value) VALUES
    (job_row.id, namePrefix || '.productId', 'string', product_row.id),
    (job_row.id, 'jobTypeId', 'string', job_row.job_type_id),
    (job_row.id, 'jobProductType', 'string', product_row.type);

    -- Also add extra attributes requested by Jim
    INSERT INTO job_attribute (job_id,name,type,value) VALUES
    (job_row.id, namePrefix || '_supplier_sku', 'string', product_alias_row.alias_id);
    IF NOT product_lens_row IS NULL THEN
      INSERT INTO job_attribute (job_id,name,type,value) VALUES
      (job_row.id, namePrefix || '_calculation_type', 'string', product_lens_row.calculation_type);
    END IF;

    iJob := iJob + 1;

  END LOOP;
END;
$$;


--
-- Name: update_order_queue(character varying, character varying, character varying); Type: FUNCTION; Schema: gnm; Owner: -
--

CREATE FUNCTION update_order_queue(p_order_id character varying, p_queue character varying, p_comment character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
-- Updates the queue for an order
DECLARE
  queue_id BIGINT;
  status_id BIGINT;
  
BEGIN
  IF p_queue NOT IN ('READY_TO_PROCESS','SUPPLIER_SUBMITTED','RESUBMIT_ENABLED','SUSPENDED','SHIPPED','AWAITING_PICKUP','COLLECTED','LENSES','FITTED','LAB','CANCEL_REQUESTED','CANCELLED','ERROR','CLOSED' ) THEN
    RAISE EXCEPTION 'Invalid queue value %', queue;
  END IF;  

  INSERT INTO queue_update (queue, comment) VALUES (p_queue, p_comment) RETURNING id INTO queue_id;
  INSERT INTO order_queue (order_id, queue_id) VALUES (p_order_id, queue_id);
  INSERT INTO status_update (action, comment) VALUES (p_queue, p_comment) RETURNING id INTO status_id;
  INSERT INTO order_status (order_id, status_id) VALUES (p_order_id, status_id);
  UPDATE "order" SET queue = p_queue, status = p_queue WHERE order_id = p_order_id;

  RETURN;
END;
$$;


--
-- Name: user_has_any_role(character varying[]); Type: FUNCTION; Schema: gnm; Owner: -
--

CREATE FUNCTION user_has_any_role(VARIADIC roles character varying[]) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
-- Returns TRUE if the current app user has any of the specified roles
DECLARE
  has_any_role BOOLEAN := FALSE;

BEGIN
  SELECT
    EXISTS(
      SELECT * FROM user_role
      WHERE (user_id)::text = current_setting('gnm.userId')
      AND role_id = ANY (roles)
    )
  INTO
    has_any_role;

  RETURN has_any_role;
END;
$$;


--
-- Name: user_is_me(bigint); Type: FUNCTION; Schema: gnm; Owner: -
--

CREATE FUNCTION user_is_me(user_id bigint) RETURNS boolean
    LANGUAGE sql
    AS $$
-- Returns TRUE if the user_id matches the app setting
SELECT (user_id)::text = current_setting('gnm.userId');
$$;


SET search_path = public, pg_catalog;

--
-- Name: fn_create_viewbydate(text, text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION fn_create_viewbydate(tablename text, datecolumnname text) RETURNS void
    LANGUAGE plpgsql
    AS $_$
begin
	EXECUTE fn_create_viewbydate_string($1, $2);
end
$_$;


--
-- Name: fn_create_viewbydate(text, text, text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION fn_create_viewbydate(tablename text, datecolumnname text, keycolumn text) RETURNS void
    LANGUAGE plpgsql
    AS $_$
begin
	EXECUTE fn_create_viewbydate_string($1, $2,$3);
end
$_$;


--
-- Name: fn_create_viewbydate_string(text, text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION fn_create_viewbydate_string(tablename text, datecolumnname text) RETURNS text
    LANGUAGE sql
    AS $_$
select 'CREATE OR REPLACE VIEW vbyd_' || $1 || ' as select date_part(''year'',sm.' || $2 || ')as year,date_part(''doy'',sm.' 
|| $2 || ') as doy,date_part(''month'',sm.' || $2 || ' )as month,date_part(''dow'',sm.' || $2 || ') as dow,date_part(''day'',sm.'|| $2 ||') as day, sk 
from ' || $1 || ' sm 
group by date_part(''year'',sm.' || $2 || '),date_part(''doy'',sm.' || $2 || '),date_part(''month'',sm.' || $2 || '),date_part(''dow'',sm.' || $2 || '),date_part(''day'',sm.' || $2 || '), sk' as view_string $_$;


--
-- Name: fn_create_viewbydate_string(text, text, text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION fn_create_viewbydate_string(tablename text, datecolumnname text, keycolumn text) RETURNS text
    LANGUAGE sql
    AS $_$
select 'CREATE OR REPLACE VIEW vbyd_' || $1 || ' as select date_part(''year'',sm.' || $2 || ')as year,date_part(''doy'',sm.' 
|| $2 || ') as doy,date_part(''month'',sm.' || $2 || ' )as month,date_part(''dow'',sm.' || $2 || ') as dow,date_part(''day'',sm.'|| $2 ||') as day, ' || $3 || '  
from ' || $1 || ' sm 
group by date_part(''year'',sm.' || $2 || '),date_part(''doy'',sm.' || $2 || '),date_part(''month'',sm.' || $2 || '),date_part(''dow'',sm.' || $2 || '),date_part(''day'',sm.' || $2 || '),' || $3 as view_string $_$;


--
-- Name: fn_create_viewbydate_string2(regclass, regclass); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION fn_create_viewbydate_string2(tablename regclass, datecolumnname regclass) RETURNS text
    LANGUAGE sql
    AS $_$
select 'CREATE OR REPLACE VIEW vbyd_' || $1 || ' as select date_part(''year'',sm.' || $2 || ')as year,date_part(''doy'',sm.' 
|| $2 || ') as doy,date_part(''month'',sm.' || $2 || ')as month2,date_part(''dow'',sm.' || $2 || ') as dow,date_part(''day'',sm.' || $2 || ') as day, sk from ' || $1 || 
' sm group by date_part(''year'',sm.' || $2 || '),date_part(''doy'',sm.' || $2 || '),date_part(''month'',sm.' || $2 || '),date_part(''dow'',sm.' 
|| $2 || '),date_part(''day'',sm.' || $2 || '),sk' as view_string $_$;


--
-- Name: fn_create_viewbydate_string2(character varying, character varying); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION fn_create_viewbydate_string2(tablename character varying, datecolumnname character varying) RETURNS character varying
    LANGUAGE sql
    AS $_$
select 'CREATE OR REPLACE VIEW vbyd_' || $1 || ' as select date_part(''year'',sm.' || $2 || ')as year,date_part(''doy'',sm.' 
|| $2 || ') as doy,date_part(''month'',sm.' || $2 || ' )as month,date_part(''dow'',sm.' || $2 || ') as dow,date_part(''day'',sm.'|| $2 ||') as day, sk 
from ' || $1 || ' sm 
group by date_part(''year'',sm.' || $2 || '),date_part(''doy'',sm.' || $2 || '),date_part(''month'',sm.' || $2 || '),date_part(''dow'',sm.' || $2 || '),date_part(''day'',sm.' || $2 || '),sk' 
as view_string $_$;


--
-- Name: within_dow_range(timestamp without time zone, timestamp without time zone); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION within_dow_range(d1 timestamp without time zone, d2 timestamp without time zone) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
declare
	td1 timestamp;
	td2 timestamp;
begin	
	if date_part('doy',d1) = date_part('doy',d2) and date_part('year',d1) = date_part('year',d2) then
		return true;
	elsif d2 >= (d1 - interval '1 days') and d2 <= (d1 + interval '1 day') then
		return true;
	elsif date_part('dow',d1) = 1 and d2 >= (d1 - interval '3 days') and d2 <= (d1 + interval '1 day') then
		return true;
	elsif date_part('dow',d1) = 5 and d2 >= (d1 - interval '1 days') and d2 <= (d1 + interval '3 day') then
		return true;
	elsif date_part('dow',d1) = 6 and d2 >= (d1 - interval '1 days') and d2 <= (d1 + interval '2 day') then
		return true;
	else
		return false;
	end if;
end;
$$;


--
-- Name: within_dow_range(timestamp without time zone, timestamp without time zone, integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION within_dow_range(d1 timestamp without time zone, d2 timestamp without time zone, bounds integer) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
begin	
	if d2 >= (d1 - interval '1d'* bounds) and d2 <= (d1 + interval '1d'* bounds) then
		return true;
	else
		return within_dow_range(d1,d2);
	end if;
end;
$$;


SET search_path = stage, pg_catalog;

--
-- Name: get_latest_data_by_tablename(character); Type: FUNCTION; Schema: stage; Owner: -
--

CREATE FUNCTION get_latest_data_by_tablename(_tablename character) RETURNS TABLE(data jsonb, parameters jsonb)
    LANGUAGE plpgsql
    AS $$

declare
sql varchar;
begin
SELECT 'SELECT jsonb_array_elements("data") as data,parameters FROM stage.' || _tablename || ' T where T.runid in (
select runid from (
select parameters->>''source'',runid,createddate,row_number() over (partition by parameters->>''source'' order by createddate desc) as r from stage.' || _tablename || ') S where r = 1);' 
into sql;

return query execute sql;

end;

$$;


SET search_path = gnm, pg_catalog;

--
-- Name: address; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE address (
    id bigint NOT NULL,
    type character varying(255) NOT NULL,
    attention character varying(255),
    number character varying(255),
    street1 character varying(255) NOT NULL,
    street2 character varying(255),
    street3 character varying(255),
    suburb character varying(255) NOT NULL,
    state character varying(255) NOT NULL,
    postcode character varying(255) NOT NULL
);


--
-- Name: address_id_seq; Type: SEQUENCE; Schema: gnm; Owner: -
--

CREATE SEQUENCE address_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: address_id_seq; Type: SEQUENCE OWNED BY; Schema: gnm; Owner: -
--

ALTER SEQUENCE address_id_seq OWNED BY address.id;


--
-- Name: brand; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE brand (
    id bigint NOT NULL,
    name character varying(50) NOT NULL,
    supplier_id bigint NOT NULL,
    brand_tier_id bigint,
    created_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    created_date_time timestamp with time zone DEFAULT now() NOT NULL,
    modified_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    modified_date_time timestamp with time zone DEFAULT now() NOT NULL
);


--
-- Name: brand_id_seq; Type: SEQUENCE; Schema: gnm; Owner: -
--

CREATE SEQUENCE brand_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: brand_id_seq; Type: SEQUENCE OWNED BY; Schema: gnm; Owner: -
--

ALTER SEQUENCE brand_id_seq OWNED BY brand.id;


--
-- Name: brand_tier; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE brand_tier (
    id bigint NOT NULL,
    name character varying(50) NOT NULL,
    supplier_id bigint NOT NULL,
    created_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    created_date_time timestamp with time zone DEFAULT now() NOT NULL,
    modified_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    modified_date_time timestamp with time zone DEFAULT now() NOT NULL
);


--
-- Name: brand_tier_id_seq; Type: SEQUENCE; Schema: gnm; Owner: -
--

CREATE SEQUENCE brand_tier_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: brand_tier_id_seq; Type: SEQUENCE OWNED BY; Schema: gnm; Owner: -
--

ALTER SEQUENCE brand_tier_id_seq OWNED BY brand_tier.id;


--
-- Name: capture20170919_product_cost_history; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE capture20170919_product_cost_history (
    id bigint,
    product_id character varying(255),
    price_policy_id bigint,
    supplier_cost numeric(19,2),
    supplier_cost_gst numeric(19,2),
    actual_cost numeric(19,2),
    actual_cost_gst numeric(19,2),
    supplier_rrp numeric(19,2),
    supplier_rrp_gst numeric(19,2),
    calculated_retail_price numeric(19,2),
    calculated_retail_price_gst numeric(19,2),
    override_cost numeric(19,2),
    override_cost_gst numeric(19,2),
    price_change_reason character varying(255),
    modified_user character varying(255),
    price_error character varying(255),
    modified_date_time timestamp without time zone,
    superseded_date_time timestamp without time zone
);


--
-- Name: capture20170919_product_price_history; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE capture20170919_product_price_history (
    id bigint,
    product_coll_id bigint,
    product_id character varying(255),
    override_retail_price numeric(19,2),
    override_retail_price_gst numeric(19,2),
    override_markup_factor numeric(5,2),
    override_margin_percent numeric(5,2),
    price_override_method character varying(30),
    retail_price numeric(19,2),
    retail_price_gst numeric(19,2),
    price_change_reason character varying(255),
    price_error character varying(255),
    modified_user character varying(255),
    modified_date_time timestamp without time zone,
    superseded_date_time timestamp without time zone
);


--
-- Name: coll_filter; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE coll_filter (
    name character varying(255) NOT NULL,
    description character varying(255),
    created_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    created_date_time timestamp without time zone DEFAULT timezone('utc'::text, now()) NOT NULL,
    modified_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    modified_date_time timestamp without time zone DEFAULT timezone('utc'::text, now()) NOT NULL
);


--
-- Name: coll_type; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE coll_type (
    name character varying(255) NOT NULL,
    description character varying(255),
    created_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    created_date_time timestamp without time zone DEFAULT timezone('utc'::text, now()) NOT NULL,
    modified_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    modified_date_time timestamp without time zone DEFAULT timezone('utc'::text, now()) NOT NULL
);


--
-- Name: gnm_order_id_sequence; Type: SEQUENCE; Schema: gnm; Owner: -
--

CREATE SEQUENCE gnm_order_id_sequence
    START WITH 1000000000
    INCREMENT BY 1
    MINVALUE 1000000000
    NO MAXVALUE
    CACHE 1;


--
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: gnm; Owner: -
--

CREATE SEQUENCE hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: image; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE image (
    id bigint NOT NULL,
    relative_path character varying(255),
    created_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    created_date_time timestamp with time zone DEFAULT now() NOT NULL,
    modified_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    modified_date_time timestamp with time zone DEFAULT now() NOT NULL,
    external_url character varying(2048),
    external_last_modified_time timestamp without time zone,
    width integer,
    height integer
);


--
-- Name: image_id_seq; Type: SEQUENCE; Schema: gnm; Owner: -
--

CREATE SEQUENCE image_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: image_id_seq; Type: SEQUENCE OWNED BY; Schema: gnm; Owner: -
--

ALTER SEQUENCE image_id_seq OWNED BY image.id;


--
-- Name: job; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE job (
    id character varying(255) NOT NULL,
    job_id character varying(255) NOT NULL,
    order_id character varying(255),
    product_id character varying(255) NOT NULL,
    supplier_id bigint NOT NULL,
    quantity bigint NOT NULL,
    status character varying(255) NOT NULL,
    created_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    created_date_time timestamp with time zone DEFAULT now() NOT NULL,
    modified_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    modified_date_time timestamp with time zone DEFAULT now() NOT NULL,
    address_id bigint,
    queue character varying(255) NOT NULL,
    override_address_id bigint,
    job_type_id character varying(40) NOT NULL
);


--
-- Name: job_attribute; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE job_attribute (
    id bigint NOT NULL,
    job_id character varying(255),
    name character varying(255) NOT NULL,
    type character varying(255) NOT NULL,
    value character varying(4000)
);


--
-- Name: job_attribute_id_seq; Type: SEQUENCE; Schema: gnm; Owner: -
--

CREATE SEQUENCE job_attribute_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: job_attribute_id_seq; Type: SEQUENCE OWNED BY; Schema: gnm; Owner: -
--

ALTER SEQUENCE job_attribute_id_seq OWNED BY job_attribute.id;


--
-- Name: job_id_sequence; Type: SEQUENCE; Schema: gnm; Owner: -
--

CREATE SEQUENCE job_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 9999999999
    CACHE 1;


--
-- Name: job_queue; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE job_queue (
    job_id character varying(255) NOT NULL,
    queue_id bigint NOT NULL
);


--
-- Name: job_status; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE job_status (
    job_id character varying(255) NOT NULL,
    status_id bigint NOT NULL
);


--
-- Name: job_type; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE job_type (
    id character varying(40) NOT NULL,
    description character varying(60) NOT NULL
);


--
-- Name: order_type_rule; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE order_type_rule (
    id bigint NOT NULL,
    order_type_id character varying(40) NOT NULL,
    product_type character varying(25) NOT NULL,
    job_type_id character varying(40) NOT NULL,
    required boolean NOT NULL
);


--
-- Name: job_type_lookup_id_seq; Type: SEQUENCE; Schema: gnm; Owner: -
--

CREATE SEQUENCE job_type_lookup_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: job_type_lookup_id_seq; Type: SEQUENCE OWNED BY; Schema: gnm; Owner: -
--

ALTER SEQUENCE job_type_lookup_id_seq OWNED BY order_type_rule.id;


--
-- Name: login_event; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE login_event (
    id bigint NOT NULL,
    user_name character varying(255),
    user_id bigint,
    authentication_type character varying(20),
    message character varying(255),
    request_ip_address character varying(50),
    inserted_time timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: login_event_id_seq; Type: SEQUENCE; Schema: gnm; Owner: -
--

CREATE SEQUENCE login_event_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: login_event_id_seq; Type: SEQUENCE OWNED BY; Schema: gnm; Owner: -
--

ALTER SEQUENCE login_event_id_seq OWNED BY login_event.id;


--
-- Name: model; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE model (
    id bigint NOT NULL,
    model_code character varying(50) NOT NULL,
    name character varying(50) NOT NULL,
    description character varying(255),
    permit_additional_fields boolean NOT NULL,
    parent_id bigint,
    created_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    created_date_time timestamp without time zone DEFAULT timezone('utc'::text, now()) NOT NULL,
    modified_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    modified_date_time timestamp without time zone DEFAULT timezone('utc'::text, now()) NOT NULL
);


--
-- Name: model_field; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE model_field (
    id bigint NOT NULL,
    model_field_code character varying(50) NOT NULL,
    model_id bigint NOT NULL,
    name character varying(50) NOT NULL,
    label character varying(50) NOT NULL,
    description character varying(255),
    sequ integer NOT NULL,
    required boolean NOT NULL,
    allow_empty boolean NOT NULL,
    min_length integer,
    max_length integer,
    type character varying(20) NOT NULL,
    format character varying(100),
    max_value character varying(30),
    min_value character varying(30),
    created_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    created_date_time timestamp without time zone DEFAULT timezone('utc'::text, now()) NOT NULL,
    modified_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    modified_date_time timestamp without time zone DEFAULT timezone('utc'::text, now()) NOT NULL,
    usage character varying(20) NOT NULL,
    CONSTRAINT model_field_usage_check CHECK (((usage)::text = ANY (ARRAY[('ImportOnly'::character varying)::text, ('ExportOnly'::character varying)::text, ('ImportAndExport'::character varying)::text])))
);


--
-- Name: model_field_id_seq; Type: SEQUENCE; Schema: gnm; Owner: -
--

CREATE SEQUENCE model_field_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: model_field_id_seq; Type: SEQUENCE OWNED BY; Schema: gnm; Owner: -
--

ALTER SEQUENCE model_field_id_seq OWNED BY model_field.id;


--
-- Name: model_id_seq; Type: SEQUENCE; Schema: gnm; Owner: -
--

CREATE SEQUENCE model_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: model_id_seq; Type: SEQUENCE OWNED BY; Schema: gnm; Owner: -
--

ALTER SEQUENCE model_id_seq OWNED BY model.id;


--
-- Name: optometrist; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE optometrist (
    id bigint NOT NULL,
    person_id bigint NOT NULL,
    provider_number character varying(50),
    created_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    created_date_time timestamp with time zone DEFAULT now() NOT NULL,
    modified_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    modified_date_time timestamp with time zone DEFAULT now() NOT NULL
);


--
-- Name: optometrist_id_seq; Type: SEQUENCE; Schema: gnm; Owner: -
--

CREATE SEQUENCE optometrist_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: optometrist_id_seq; Type: SEQUENCE OWNED BY; Schema: gnm; Owner: -
--

ALTER SEQUENCE optometrist_id_seq OWNED BY optometrist.id;


--
-- Name: order_customer; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE order_customer (
    id bigint NOT NULL,
    customer_id character varying(255) NOT NULL,
    first_name character varying(255) NOT NULL,
    last_name character varying(255),
    telephone character varying(255) NOT NULL
);


--
-- Name: order_customer_id_seq; Type: SEQUENCE; Schema: gnm; Owner: -
--

CREATE SEQUENCE order_customer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: order_customer_id_seq; Type: SEQUENCE OWNED BY; Schema: gnm; Owner: -
--

ALTER SEQUENCE order_customer_id_seq OWNED BY order_customer.id;


--
-- Name: order_info; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE order_info (
    id bigint NOT NULL,
    order_id character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    type character varying(255) NOT NULL,
    value character varying(255)
);


--
-- Name: order_info_id_seq; Type: SEQUENCE; Schema: gnm; Owner: -
--

CREATE SEQUENCE order_info_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: order_info_id_seq; Type: SEQUENCE OWNED BY; Schema: gnm; Owner: -
--

ALTER SEQUENCE order_info_id_seq OWNED BY order_info.id;


--
-- Name: order_queue; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE order_queue (
    order_id character varying(255) NOT NULL,
    queue_id bigint NOT NULL
);


--
-- Name: order_status; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE order_status (
    order_id character varying(255) NOT NULL,
    status_id bigint NOT NULL
);


--
-- Name: order_type; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE order_type (
    id character varying(40) NOT NULL,
    description character varying(60) NOT NULL
);


--
-- Name: person; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE person (
    id bigint NOT NULL,
    first_name character varying(255),
    last_name character varying(255),
    created_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    created_date_time timestamp with time zone DEFAULT now() NOT NULL,
    modified_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    modified_date_time timestamp with time zone DEFAULT now() NOT NULL
);


--
-- Name: person_certifications; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE person_certifications (
    person_id bigint NOT NULL,
    name character varying(255) NOT NULL
);


--
-- Name: person_id_seq; Type: SEQUENCE; Schema: gnm; Owner: -
--

CREATE SEQUENCE person_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: person_id_seq; Type: SEQUENCE OWNED BY; Schema: gnm; Owner: -
--

ALTER SEQUENCE person_id_seq OWNED BY person.id;


--
-- Name: price_policy_id_seq; Type: SEQUENCE; Schema: gnm; Owner: -
--

CREATE SEQUENCE price_policy_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: price_policy_id_seq; Type: SEQUENCE OWNED BY; Schema: gnm; Owner: -
--

ALTER SEQUENCE price_policy_id_seq OWNED BY price_policy.id;


--
-- Name: product_alias; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE product_alias (
    id bigint NOT NULL,
    product_id character varying(255) NOT NULL,
    alias_prefix character varying(255) NOT NULL,
    alias_id character varying(255) NOT NULL,
    created_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    created_date_time timestamp with time zone DEFAULT now() NOT NULL,
    modified_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    modified_date_time timestamp with time zone DEFAULT now() NOT NULL
);


--
-- Name: product_alias_id_seq; Type: SEQUENCE; Schema: gnm; Owner: -
--

CREATE SEQUENCE product_alias_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: product_alias_id_seq; Type: SEQUENCE OWNED BY; Schema: gnm; Owner: -
--

ALTER SEQUENCE product_alias_id_seq OWNED BY product_alias.id;


--
-- Name: product_assortment; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE product_assortment (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    description character varying(255) NOT NULL,
    created_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    created_date_time timestamp with time zone DEFAULT now() NOT NULL,
    modified_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    modified_date_time timestamp with time zone DEFAULT now() NOT NULL
);


--
-- Name: product_assortment_id_seq; Type: SEQUENCE; Schema: gnm; Owner: -
--

CREATE SEQUENCE product_assortment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: product_assortment_id_seq; Type: SEQUENCE OWNED BY; Schema: gnm; Owner: -
--

ALTER SEQUENCE product_assortment_id_seq OWNED BY product_assortment.id;


--
-- Name: product_assortment_product; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE product_assortment_product (
    assortment_id bigint NOT NULL,
    product_id character varying(255) NOT NULL
);


--
-- Name: product_assortment_store; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE product_assortment_store (
    assortment_id bigint NOT NULL,
    store_id bigint NOT NULL
);


--
-- Name: product_attribute; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE product_attribute (
    id bigint NOT NULL,
    product_id character varying(255),
    name character varying(255) NOT NULL,
    type character varying(255) NOT NULL,
    value character varying(255)
);


--
-- Name: product_attribute_id_seq; Type: SEQUENCE; Schema: gnm; Owner: -
--

CREATE SEQUENCE product_attribute_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: product_attribute_id_seq; Type: SEQUENCE OWNED BY; Schema: gnm; Owner: -
--

ALTER SEQUENCE product_attribute_id_seq OWNED BY product_attribute.id;


--
-- Name: product_coll; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE product_coll (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    description character varying(255),
    coll_type character varying(255) NOT NULL,
    created_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    created_date_time timestamp without time zone DEFAULT timezone('utc'::text, now()) NOT NULL,
    modified_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    modified_date_time timestamp without time zone DEFAULT timezone('utc'::text, now()) NOT NULL,
    total_members bigint DEFAULT 0,
    members_modified_date_time timestamp without time zone
);


--
-- Name: product_coll_all_member; Type: VIEW; Schema: gnm; Owner: -
--

CREATE VIEW product_coll_all_member AS
 SELECT pc.id AS product_coll_id,
    epcam.product_id
   FROM product_coll pc,
    LATERAL execute_product_coll_all_member(pc.id) epcam(product_coll_id, product_id);


--
-- Name: product_coll_attribute; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE product_coll_attribute (
    id bigint NOT NULL,
    product_coll_id bigint NOT NULL,
    name character varying(255) NOT NULL,
    type character varying(255) NOT NULL,
    value character varying(1024) NOT NULL
);


--
-- Name: product_coll_attribute_id_seq; Type: SEQUENCE; Schema: gnm; Owner: -
--

CREATE SEQUENCE product_coll_attribute_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: product_coll_attribute_id_seq; Type: SEQUENCE OWNED BY; Schema: gnm; Owner: -
--

ALTER SEQUENCE product_coll_attribute_id_seq OWNED BY product_coll_attribute.id;


--
-- Name: product_coll_filter; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE product_coll_filter (
    id bigint NOT NULL,
    coll_filter character varying(255) NOT NULL,
    coll_filter_parameter_json jsonb,
    last_refresh_date_time timestamp without time zone,
    created_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    created_date_time timestamp without time zone DEFAULT timezone('utc'::text, now()) NOT NULL,
    modified_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    modified_date_time timestamp without time zone DEFAULT timezone('utc'::text, now()) NOT NULL
);


--
-- Name: product_coll_filter_member; Type: VIEW; Schema: gnm; Owner: -
--

CREATE VIEW product_coll_filter_member AS
 SELECT pc.id AS product_coll_id,
    epcf.product_id
   FROM product_coll pc,
    LATERAL execute_product_coll_filter(pc.id) epcf(product_coll_id, product_id);


--
-- Name: product_coll_id_seq; Type: SEQUENCE; Schema: gnm; Owner: -
--

CREATE SEQUENCE product_coll_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: product_coll_id_seq; Type: SEQUENCE OWNED BY; Schema: gnm; Owner: -
--

ALTER SEQUENCE product_coll_id_seq OWNED BY product_coll.id;


--
-- Name: product_coll_member; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE product_coll_member (
    id bigint NOT NULL,
    product_coll_id bigint NOT NULL,
    product_id character varying(255) NOT NULL,
    created_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    created_date_time timestamp without time zone DEFAULT timezone('utc'::text, now()) NOT NULL
);


--
-- Name: product_coll_member_id_seq; Type: SEQUENCE; Schema: gnm; Owner: -
--

CREATE SEQUENCE product_coll_member_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: product_coll_member_id_seq; Type: SEQUENCE OWNED BY; Schema: gnm; Owner: -
--

ALTER SEQUENCE product_coll_member_id_seq OWNED BY product_coll_member.id;


--
-- Name: product_coll_tree; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE product_coll_tree (
    id bigint NOT NULL,
    parent_id bigint NOT NULL,
    child_id bigint NOT NULL,
    created_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    created_date_time timestamp without time zone DEFAULT timezone('utc'::text, now()) NOT NULL
);


--
-- Name: product_coll_tree_id_seq; Type: SEQUENCE; Schema: gnm; Owner: -
--

CREATE SEQUENCE product_coll_tree_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: product_coll_tree_id_seq; Type: SEQUENCE OWNED BY; Schema: gnm; Owner: -
--

ALTER SEQUENCE product_coll_tree_id_seq OWNED BY product_coll_tree.id;


--
-- Name: product_contact_lens; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE product_contact_lens (
    id character varying(255) NOT NULL,
    design character varying(255),
    feature character varying(255),
    material character varying(255),
    base_curve character varying(255),
    diameter character varying(255),
    power_range character varying(255),
    packet_size character varying(255),
    water_content character varying(255),
    axis character varying(255),
    wearing_days character varying(255),
    cylinder character varying(255),
    add_power character varying(255),
    colour character varying(255),
    ring_colour character varying(255)
);


--
-- Name: product_cost_history; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE product_cost_history (
    id bigint NOT NULL,
    product_id character varying(255) NOT NULL,
    price_policy_id bigint,
    supplier_cost numeric(19,2),
    supplier_cost_gst numeric(19,2),
    actual_cost numeric(19,2),
    actual_cost_gst numeric(19,2),
    supplier_rrp numeric(19,2),
    supplier_rrp_gst numeric(19,2),
    calculated_retail_price numeric(19,2),
    calculated_retail_price_gst numeric(19,2),
    override_cost numeric(19,2),
    override_cost_gst numeric(19,2),
    price_change_reason character varying(255) NOT NULL,
    modified_user character varying(255) NOT NULL,
    price_error character varying(255),
    modified_date_time timestamp without time zone NOT NULL,
    superseded_date_time timestamp without time zone
);


--
-- Name: product_frame; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE product_frame (
    id character varying(255) NOT NULL,
    bridge_size character varying(255),
    colour_description character varying(255),
    depth character varying(255),
    diagonal character varying(255),
    eye_size character varying(255),
    gender character varying(255),
    material character varying(255),
    model character varying(255),
    temple_length character varying(255),
    wrap_angle character varying(255),
    age_group character varying(255),
    colour_code character varying(255),
    construction character varying(255),
    tech character varying(255),
    segment character varying(255),
    asian_fit character varying(255)
);


--
-- Name: product_id_sequence; Type: SEQUENCE; Schema: gnm; Owner: -
--

CREATE SEQUENCE product_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 99999999
    CACHE 1;


--
-- Name: product_lens; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE product_lens (
    id character varying(255) NOT NULL,
    material character varying(255),
    type character varying(255),
    high_power character varying(255),
    low_power character varying(255),
    high_add character varying(255),
    low_add character varying(255),
    focal_type character varying(255),
    treatment character varying(255),
    refractive_index character varying(255),
    eyetalk_id character varying(255),
    size character varying(255),
    segment_size character varying(255),
    segment_height character varying(255),
    calculation_type character varying(255),
    stock_or_grind character varying(255)
);


--
-- Name: product_price_history; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE product_price_history (
    id bigint NOT NULL,
    product_coll_id bigint NOT NULL,
    product_id character varying(255) NOT NULL,
    override_retail_price numeric(19,2),
    override_retail_price_gst numeric(19,2),
    override_markup_factor numeric(5,2),
    override_margin_percent numeric(5,2),
    price_override_method character varying(30),
    retail_price numeric(19,2),
    retail_price_gst numeric(19,2),
    price_change_reason character varying(255),
    price_error character varying(255),
    modified_user character varying(255) NOT NULL,
    modified_date_time timestamp without time zone NOT NULL,
    superseded_date_time timestamp without time zone
);


--
-- Name: product_price_history_id_seq; Type: SEQUENCE; Schema: gnm; Owner: -
--

CREATE SEQUENCE product_price_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: product_price_history_id_seq; Type: SEQUENCE OWNED BY; Schema: gnm; Owner: -
--

ALTER SEQUENCE product_price_history_id_seq OWNED BY product_cost_history.id;


--
-- Name: product_price_history_id_seq1; Type: SEQUENCE; Schema: gnm; Owner: -
--

CREATE SEQUENCE product_price_history_id_seq1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: product_price_history_id_seq1; Type: SEQUENCE OWNED BY; Schema: gnm; Owner: -
--

ALTER SEQUENCE product_price_history_id_seq1 OWNED BY product_price_history.id;


--
-- Name: product_price_id_seq; Type: SEQUENCE; Schema: gnm; Owner: -
--

CREATE SEQUENCE product_price_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: product_price_id_seq; Type: SEQUENCE OWNED BY; Schema: gnm; Owner: -
--

ALTER SEQUENCE product_price_id_seq OWNED BY product_price.id;


--
-- Name: product_state; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE product_state (
    id bigint NOT NULL,
    product_id character varying(255) NOT NULL,
    state_type character varying(255) NOT NULL,
    scheduled_end_date_time timestamp without time zone,
    reason character varying(255),
    reason_request_id bigint,
    created_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    created_date_time timestamp without time zone DEFAULT timezone('utc'::text, now()) NOT NULL
);


--
-- Name: product_state_id_seq; Type: SEQUENCE; Schema: gnm; Owner: -
--

CREATE SEQUENCE product_state_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: product_state_id_seq; Type: SEQUENCE OWNED BY; Schema: gnm; Owner: -
--

ALTER SEQUENCE product_state_id_seq OWNED BY product_state.id;


--
-- Name: product_state_request; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE product_state_request (
    id bigint NOT NULL,
    product_coll_id bigint,
    product_id character varying(255) NOT NULL,
    from_state_id bigint NOT NULL,
    to_state_type character varying(255) NOT NULL,
    to_scheduled_end_date_time timestamp without time zone,
    request_state_type character varying(255) NOT NULL,
    request_reason character varying(255),
    resolution_reason character varying(255),
    created_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    created_date_time timestamp without time zone DEFAULT timezone('utc'::text, now()) NOT NULL,
    modified_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    modified_date_time timestamp without time zone DEFAULT timezone('utc'::text, now()) NOT NULL
);


--
-- Name: product_state_request_id_seq; Type: SEQUENCE; Schema: gnm; Owner: -
--

CREATE SEQUENCE product_state_request_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: product_state_request_id_seq; Type: SEQUENCE OWNED BY; Schema: gnm; Owner: -
--

ALTER SEQUENCE product_state_request_id_seq OWNED BY product_state_request.id;


--
-- Name: product_state_request_state_type; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE product_state_request_state_type (
    name character varying(255) NOT NULL,
    description character varying(255) NOT NULL,
    created_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    created_date_time timestamp without time zone DEFAULT timezone('utc'::text, now()) NOT NULL,
    modified_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    modified_date_time timestamp without time zone DEFAULT timezone('utc'::text, now()) NOT NULL
);


--
-- Name: product_state_type; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE product_state_type (
    name character varying(255) NOT NULL,
    description character varying(255) NOT NULL,
    created_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    created_date_time timestamp without time zone DEFAULT timezone('utc'::text, now()) NOT NULL,
    modified_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    modified_date_time timestamp without time zone DEFAULT timezone('utc'::text, now()) NOT NULL
);


--
-- Name: product_sunglasses; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE product_sunglasses (
    id character varying(255) NOT NULL,
    bridge_size character varying(255),
    colour_description character varying(255),
    depth character varying(255),
    diagonal character varying(255),
    eye_size character varying(255),
    gender character varying(255),
    material character varying(255),
    model character varying(255),
    temple_length character varying(255),
    wrap_angle character varying(255),
    age_group character varying(255),
    colour_code character varying(255),
    construction character varying(255),
    tech character varying(255),
    segment character varying(255),
    asian_fit character varying(255),
    uv_rating character varying(255),
    lens_colour_code character varying(255),
    lens_colour_description character varying(255),
    lens_material character varying(255),
    rxable character varying(255)
);


--
-- Name: queue_update; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE queue_update (
    id bigint NOT NULL,
    queue character varying(31) NOT NULL,
    comment character varying(2000) NOT NULL,
    created_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    created_date_time timestamp with time zone DEFAULT now() NOT NULL,
    modified_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    modified_date_time timestamp with time zone DEFAULT now() NOT NULL
);


--
-- Name: queue_update_id_seq; Type: SEQUENCE; Schema: gnm; Owner: -
--

CREATE SEQUENCE queue_update_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: queue_update_id_seq; Type: SEQUENCE OWNED BY; Schema: gnm; Owner: -
--

ALTER SEQUENCE queue_update_id_seq OWNED BY queue_update.id;


--
-- Name: region; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE region (
    id bigint NOT NULL,
    name character varying(100) NOT NULL,
    manager_id bigint,
    created_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    created_date_time timestamp with time zone DEFAULT now() NOT NULL,
    modified_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    modified_date_time timestamp with time zone DEFAULT now() NOT NULL
);


--
-- Name: region_id_seq; Type: SEQUENCE; Schema: gnm; Owner: -
--

CREATE SEQUENCE region_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: region_id_seq; Type: SEQUENCE OWNED BY; Schema: gnm; Owner: -
--

ALTER SEQUENCE region_id_seq OWNED BY region.id;


--
-- Name: revinfo; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE revinfo (
    rev integer NOT NULL,
    revtstmp bigint
);


--
-- Name: role; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE role (
    id character varying(20) NOT NULL,
    name character varying(255) NOT NULL,
    require_user_password boolean NOT NULL,
    created_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    created_date_time timestamp with time zone DEFAULT now() NOT NULL,
    modified_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    modified_date_time timestamp with time zone DEFAULT now() NOT NULL
);


--
-- Name: schema_version; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE schema_version (
    version_rank integer NOT NULL,
    installed_rank integer NOT NULL,
    version character varying(50) NOT NULL,
    description character varying(200) NOT NULL,
    type character varying(20) NOT NULL,
    script character varying(1000) NOT NULL,
    checksum integer,
    installed_by character varying(100) NOT NULL,
    installed_on timestamp without time zone DEFAULT now() NOT NULL,
    execution_time integer NOT NULL,
    success boolean NOT NULL
);


--
-- Name: static; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE static (
    id bigint NOT NULL,
    type character varying(255) NOT NULL,
    value character varying(255) NOT NULL
);


--
-- Name: static_id_seq; Type: SEQUENCE; Schema: gnm; Owner: -
--

CREATE SEQUENCE static_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: static_id_seq; Type: SEQUENCE OWNED BY; Schema: gnm; Owner: -
--

ALTER SEQUENCE static_id_seq OWNED BY static.id;


--
-- Name: status_update; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE status_update (
    id bigint NOT NULL,
    action character varying(31) NOT NULL,
    comment character varying(2000) NOT NULL,
    created_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    created_date_time timestamp with time zone DEFAULT now() NOT NULL,
    modified_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    modified_date_time timestamp with time zone DEFAULT now() NOT NULL
);


--
-- Name: status_update_id_seq; Type: SEQUENCE; Schema: gnm; Owner: -
--

CREATE SEQUENCE status_update_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: status_update_id_seq; Type: SEQUENCE OWNED BY; Schema: gnm; Owner: -
--

ALTER SEQUENCE status_update_id_seq OWNED BY status_update.id;


--
-- Name: stock; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE stock (
    id bigint NOT NULL,
    storage_type character varying(31) NOT NULL,
    storage_id character varying(255) NOT NULL,
    product_id character varying(255) NOT NULL,
    quantity bigint NOT NULL,
    retail_price numeric(19,2),
    retail_price_tax numeric(19,2),
    wholesale_price numeric(19,2),
    wholesale_price_tax numeric(19,2),
    created_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    created_date_time timestamp with time zone DEFAULT now() NOT NULL,
    modified_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    modified_date_time timestamp with time zone DEFAULT now() NOT NULL
);


--
-- Name: stock_aud; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE stock_aud (
    id bigint NOT NULL,
    rev integer NOT NULL,
    revtype smallint NOT NULL,
    storage_type character varying(31) NOT NULL,
    storage_id character varying(255) NOT NULL,
    product_id character varying(255) NOT NULL,
    quantity bigint NOT NULL,
    retail_price numeric(19,2),
    retail_price_tax numeric(19,2),
    wholesale_price numeric(19,2),
    wholesale_price_tax numeric(19,2),
    created_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    created_date_time timestamp with time zone DEFAULT now() NOT NULL,
    modified_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    modified_date_time timestamp with time zone DEFAULT now() NOT NULL
);


--
-- Name: stock_id_seq; Type: SEQUENCE; Schema: gnm; Owner: -
--

CREATE SEQUENCE stock_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: stock_id_seq; Type: SEQUENCE OWNED BY; Schema: gnm; Owner: -
--

ALTER SEQUENCE stock_id_seq OWNED BY stock.id;


--
-- Name: stocktake; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE stocktake (
    id bigint NOT NULL,
    store_id bigint NOT NULL,
    planned_stocktake_start_date_time timestamp with time zone NOT NULL,
    planned_stocktake_end_date_time timestamp with time zone NOT NULL,
    actual_stocktake_start_date_time timestamp with time zone,
    actual_stocktake_end_date_time timestamp with time zone,
    created_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    created_date_time timestamp without time zone DEFAULT timezone('utc'::text, now()) NOT NULL,
    modified_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    modified_date_time timestamp without time zone DEFAULT timezone('utc'::text, now()) NOT NULL
);


--
-- Name: stocktake_id_seq; Type: SEQUENCE; Schema: gnm; Owner: -
--

CREATE SEQUENCE stocktake_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: stocktake_id_seq; Type: SEQUENCE OWNED BY; Schema: gnm; Owner: -
--

ALTER SEQUENCE stocktake_id_seq OWNED BY stocktake.id;


--
-- Name: stocktake_item; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE stocktake_item (
    id bigint NOT NULL,
    stocktake_id bigint NOT NULL,
    product_id character varying(255),
    pms_product_id character varying(255) NOT NULL,
    pms_supplier_id character varying(255) NOT NULL,
    expected_qty integer NOT NULL,
    actual_qty integer NOT NULL,
    avg_cost_incl_gst numeric(19,2) NOT NULL,
    avg_cost_gst numeric(19,2) NOT NULL,
    pms_user character varying(25) NOT NULL,
    last_purchased_date timestamp without time zone,
    created_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    created_date_time timestamp without time zone DEFAULT timezone('utc'::text, now()) NOT NULL,
    modified_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    modified_date_time timestamp without time zone DEFAULT timezone('utc'::text, now()) NOT NULL
);


--
-- Name: stocktake_item_id_seq; Type: SEQUENCE; Schema: gnm; Owner: -
--

CREATE SEQUENCE stocktake_item_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: stocktake_item_id_seq; Type: SEQUENCE OWNED BY; Schema: gnm; Owner: -
--

ALTER SEQUENCE stocktake_item_id_seq OWNED BY stocktake_item.id;


--
-- Name: store; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE store (
    id bigint NOT NULL,
    store_id character varying(18) NOT NULL,
    name character varying(100) NOT NULL,
    street1 character varying(255),
    street2 character varying(255),
    suburb character varying(255),
    state character varying(255),
    postcode character varying(255),
    telephone character varying(20),
    fax character varying(20),
    email character varying(255),
    chain character varying(100),
    space bigint,
    established integer,
    owner character varying(255),
    created_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    created_date_time timestamp with time zone DEFAULT now() NOT NULL,
    modified_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    modified_date_time timestamp with time zone DEFAULT now() NOT NULL,
    region_id bigint,
    latitude double precision,
    longitude double precision,
    acquisition_date date,
    consult_rooms integer,
    status character varying(20) DEFAULT 'PENDING'::character varying,
    go_bookings_location_id character varying(255)
);


--
-- Name: store_attribute; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE store_attribute (
    id bigint NOT NULL,
    store_id bigint NOT NULL,
    name character varying(255) NOT NULL,
    type character varying(255) NOT NULL,
    value character varying(1024) NOT NULL
);


--
-- Name: store_attribute_id_seq; Type: SEQUENCE; Schema: gnm; Owner: -
--

CREATE SEQUENCE store_attribute_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: store_attribute_id_seq; Type: SEQUENCE OWNED BY; Schema: gnm; Owner: -
--

ALTER SEQUENCE store_attribute_id_seq OWNED BY store_attribute.id;


--
-- Name: store_hours; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE store_hours (
    store_id bigint NOT NULL,
    day character varying(255) NOT NULL,
    open character varying(255),
    close character varying(255),
    is_trading boolean DEFAULT false NOT NULL
);


--
-- Name: store_id_seq; Type: SEQUENCE; Schema: gnm; Owner: -
--

CREATE SEQUENCE store_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: store_id_seq; Type: SEQUENCE OWNED BY; Schema: gnm; Owner: -
--

ALTER SEQUENCE store_id_seq OWNED BY store.id;


--
-- Name: store_manager; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE store_manager (
    store_id bigint NOT NULL,
    person_id bigint NOT NULL
);


--
-- Name: store_optometrist; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE store_optometrist (
    store_id bigint NOT NULL,
    optometrist_id bigint NOT NULL
);


--
-- Name: store_product_coll; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE store_product_coll (
    id bigint NOT NULL,
    store_id bigint NOT NULL,
    product_coll_id bigint NOT NULL,
    created_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    created_date_time timestamp without time zone DEFAULT timezone('utc'::text, now()) NOT NULL
);


--
-- Name: store_product_coll_id_seq; Type: SEQUENCE; Schema: gnm; Owner: -
--

CREATE SEQUENCE store_product_coll_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: store_product_coll_id_seq; Type: SEQUENCE OWNED BY; Schema: gnm; Owner: -
--

ALTER SEQUENCE store_product_coll_id_seq OWNED BY store_product_coll.id;


--
-- Name: store_services; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE store_services (
    store_id bigint NOT NULL,
    name character varying(255) NOT NULL
);


--
-- Name: supplier; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE supplier (
    id bigint NOT NULL,
    supplier_id character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    email character varying(255),
    fax character varying(255),
    telephone character varying(255),
    contact_person_id bigint,
    created_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    created_date_time timestamp with time zone DEFAULT now() NOT NULL,
    modified_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    modified_date_time timestamp with time zone DEFAULT now() NOT NULL
);


--
-- Name: supplier_address; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE supplier_address (
    supplier_id bigint NOT NULL,
    address_id bigint NOT NULL
);


--
-- Name: supplier_id_seq; Type: SEQUENCE; Schema: gnm; Owner: -
--

CREATE SEQUENCE supplier_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: supplier_id_seq; Type: SEQUENCE OWNED BY; Schema: gnm; Owner: -
--

ALTER SEQUENCE supplier_id_seq OWNED BY supplier.id;


--
-- Name: task; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE task (
    id bigint NOT NULL,
    user_id bigint NOT NULL,
    type character varying(50) NOT NULL,
    description character varying(255) NOT NULL,
    status character varying(20) NOT NULL,
    message character varying(1024),
    inserted_time timestamp without time zone NOT NULL,
    scheduled_time timestamp without time zone NOT NULL,
    started_time timestamp without time zone,
    updated_time timestamp without time zone,
    completed_time timestamp without time zone,
    percent_completed integer
);


--
-- Name: task_attribute; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE task_attribute (
    id bigint NOT NULL,
    task_id bigint,
    name character varying(255) NOT NULL,
    value character varying(1024) NOT NULL
);


--
-- Name: task_attribute_id_seq; Type: SEQUENCE; Schema: gnm; Owner: -
--

CREATE SEQUENCE task_attribute_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: task_attribute_id_seq; Type: SEQUENCE OWNED BY; Schema: gnm; Owner: -
--

ALTER SEQUENCE task_attribute_id_seq OWNED BY task_attribute.id;


--
-- Name: task_id_sequence; Type: SEQUENCE; Schema: gnm; Owner: -
--

CREATE SEQUENCE task_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 9999999999
    CACHE 1;


--
-- Name: task_log; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE task_log (
    id bigint NOT NULL,
    task_id bigint NOT NULL,
    "row" bigint,
    message character varying(2048) NOT NULL,
    inserted_time timestamp without time zone NOT NULL,
    task_message_type character varying(5) DEFAULT 'info'::character varying
);


--
-- Name: task_log_id_seq; Type: SEQUENCE; Schema: gnm; Owner: -
--

CREATE SEQUENCE task_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: task_log_id_seq; Type: SEQUENCE OWNED BY; Schema: gnm; Owner: -
--

ALTER SEQUENCE task_log_id_seq OWNED BY task_log.id;


--
-- Name: uploaded_file; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE uploaded_file (
    id bigint NOT NULL,
    user_id bigint NOT NULL,
    original_name character varying(255) NOT NULL,
    relative_path character varying(2048) NOT NULL,
    uploaded_time timestamp without time zone NOT NULL,
    task_id bigint
);


--
-- Name: uploaded_file_id_sequence; Type: SEQUENCE; Schema: gnm; Owner: -
--

CREATE SEQUENCE uploaded_file_id_sequence
    START WITH 100
    INCREMENT BY 1
    MINVALUE 100
    MAXVALUE 99999999
    CACHE 1;


--
-- Name: user; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE "user" (
    id bigint NOT NULL,
    person_id bigint,
    password character varying(255) DEFAULT md5((random())::text),
    email_address character varying(255) NOT NULL,
    created_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    created_date_time timestamp with time zone DEFAULT now() NOT NULL,
    modified_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    modified_date_time timestamp with time zone DEFAULT now() NOT NULL,
    password_change_required boolean DEFAULT false NOT NULL,
    last_activity_time timestamp with time zone,
    account_locked boolean DEFAULT false NOT NULL
);


--
-- Name: user_id_seq; Type: SEQUENCE; Schema: gnm; Owner: -
--

CREATE SEQUENCE user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: user_id_seq; Type: SEQUENCE OWNED BY; Schema: gnm; Owner: -
--

ALTER SEQUENCE user_id_seq OWNED BY "user".id;


--
-- Name: user_password_reset; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE user_password_reset (
    id bigint NOT NULL,
    email_address character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    expiry_time timestamp without time zone NOT NULL
);


--
-- Name: user_password_reset_id_seq; Type: SEQUENCE; Schema: gnm; Owner: -
--

CREATE SEQUENCE user_password_reset_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: user_password_reset_id_seq; Type: SEQUENCE OWNED BY; Schema: gnm; Owner: -
--

ALTER SEQUENCE user_password_reset_id_seq OWNED BY user_password_reset.id;


--
-- Name: user_preference; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE user_preference (
    id bigint NOT NULL,
    user_id bigint NOT NULL,
    key character varying(50) NOT NULL,
    preference text NOT NULL,
    created_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    created_date_time timestamp with time zone DEFAULT now() NOT NULL,
    modified_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    modified_date_time timestamp with time zone DEFAULT now() NOT NULL
);


--
-- Name: user_preference_id_seq; Type: SEQUENCE; Schema: gnm; Owner: -
--

CREATE SEQUENCE user_preference_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: user_preference_id_seq; Type: SEQUENCE OWNED BY; Schema: gnm; Owner: -
--

ALTER SEQUENCE user_preference_id_seq OWNED BY user_preference.id;


--
-- Name: user_role; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE user_role (
    id bigint NOT NULL,
    user_id bigint NOT NULL,
    role_id character varying(20) NOT NULL,
    created_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    created_date_time timestamp with time zone DEFAULT now() NOT NULL,
    modified_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    modified_date_time timestamp with time zone DEFAULT now() NOT NULL
);


--
-- Name: user_role_id_seq; Type: SEQUENCE; Schema: gnm; Owner: -
--

CREATE SEQUENCE user_role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: user_role_id_seq; Type: SEQUENCE OWNED BY; Schema: gnm; Owner: -
--

ALTER SEQUENCE user_role_id_seq OWNED BY user_role.id;


--
-- Name: user_store; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE user_store (
    id bigint NOT NULL,
    user_id bigint NOT NULL,
    store_id bigint NOT NULL,
    created_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    created_date_time timestamp with time zone DEFAULT now() NOT NULL,
    modified_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    modified_date_time timestamp with time zone DEFAULT now() NOT NULL
);


--
-- Name: user_store_id_seq; Type: SEQUENCE; Schema: gnm; Owner: -
--

CREATE SEQUENCE user_store_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: user_store_id_seq; Type: SEQUENCE OWNED BY; Schema: gnm; Owner: -
--

ALTER SEQUENCE user_store_id_seq OWNED BY user_store.id;


--
-- Name: user_supplier; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE user_supplier (
    id bigint NOT NULL,
    user_id bigint NOT NULL,
    supplier_id bigint NOT NULL,
    created_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    created_date_time timestamp with time zone DEFAULT now() NOT NULL,
    modified_user character varying(255) DEFAULT 'system'::character varying NOT NULL,
    modified_date_time timestamp with time zone DEFAULT now() NOT NULL
);


--
-- Name: user_supplier_id_seq; Type: SEQUENCE; Schema: gnm; Owner: -
--

CREATE SEQUENCE user_supplier_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: user_supplier_id_seq; Type: SEQUENCE OWNED BY; Schema: gnm; Owner: -
--

ALTER SEQUENCE user_supplier_id_seq OWNED BY user_supplier.id;


--
-- Name: zeiss_retextra; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE zeiss_retextra (
    stock character varying(18) NOT NULL,
    descrip character varying(30),
    barcode character varying(15),
    q numeric(5,0),
    sell numeric(8,2),
    cost_ea numeric(8,2),
    salqtymtd numeric(5,0),
    salqtyytd numeric(6,0),
    salqtylast numeric(6,0),
    mtdsales numeric(10,2),
    lastsales numeric(10,2),
    ytdsales numeric(10,2),
    reorder numeric(3,0),
    maximum numeric(3,0),
    cost_mtd numeric(10,2),
    cost_ytd numeric(10,2),
    on_order numeric(5,0),
    ord_date character varying(8),
    supplier character varying(6),
    kgroup character varying(2),
    retextra_group character varying(6),
    mtdbq numeric(5,0),
    ytdbq numeric(5,0),
    mtdbval numeric(8,2),
    ytdbval numeric(8,2),
    bln character varying(6),
    strike numeric(3,0),
    ordered numeric(3,0),
    visible boolean,
    inventory boolean
);


--
-- Name: zeiss_retextra_20170918; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE zeiss_retextra_20170918 (
    stock character varying(18),
    descrip character varying(30),
    barcode character varying(15),
    q numeric(5,0),
    sell numeric(8,2),
    cost_ea numeric(8,2),
    salqtymtd numeric(5,0),
    salqtyytd numeric(6,0),
    salqtylast numeric(6,0),
    mtdsales numeric(10,2),
    lastsales numeric(10,2),
    ytdsales numeric(10,2),
    reorder numeric(3,0),
    maximum numeric(3,0),
    cost_mtd numeric(10,2),
    cost_ytd numeric(10,2),
    on_order numeric(5,0),
    ord_date character varying(8),
    supplier character varying(6),
    kgroup character varying(2),
    retextra_group character varying(6),
    mtdbq numeric(5,0),
    ytdbq numeric(5,0),
    mtdbval numeric(8,2),
    ytdbval numeric(8,2),
    bln character varying(6),
    strike numeric(3,0),
    ordered numeric(3,0),
    visible boolean,
    inventory boolean
);


--
-- Name: zeiss_sphcyl; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE zeiss_sphcyl (
    id bigint NOT NULL,
    keyrange character varying(7),
    sphere character varying(4),
    mincyl numeric(5,2),
    maxcyl numeric(5,2)
);


--
-- Name: zeiss_sphcyl_20170918; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE zeiss_sphcyl_20170918 (
    id bigint,
    keyrange character varying(7),
    sphere character varying(4),
    mincyl numeric(5,2),
    maxcyl numeric(5,2)
);


--
-- Name: zeiss_sphcyl_id_seq; Type: SEQUENCE; Schema: gnm; Owner: -
--

CREATE SEQUENCE zeiss_sphcyl_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zeiss_sphcyl_id_seq; Type: SEQUENCE OWNED BY; Schema: gnm; Owner: -
--

ALTER SEQUENCE zeiss_sphcyl_id_seq OWNED BY zeiss_sphcyl.id;


--
-- Name: zeiss_tlr; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE zeiss_tlr (
    keyrange character varying(6) NOT NULL,
    calcs character varying(1),
    title character varying(25),
    tlr_order numeric(2,0),
    matcode numeric(2,0),
    execbw numeric(2,0),
    edtperc numeric(2,0),
    segdiam numeric(2,0),
    basech character varying(8),
    sagch character varying(8),
    segch character varying(8),
    fitadj numeric(1,0),
    rndtool boolean,
    trange numeric(1,0),
    addfin numeric(3,1),
    prthin boolean,
    isrl boolean,
    genalias character varying(1),
    rethipwr numeric(6,2),
    retlopwr numeric(6,2),
    kvalue numeric(4,2),
    retorder numeric(2,0),
    bowlsize numeric(4,1),
    carrier numeric(4,2),
    finech character varying(5),
    mercorder numeric(2,0),
    specialcod character varying(1),
    hiadd numeric(6,2),
    loadd numeric(6,2),
    istenths boolean,
    manufdes character varying(1)
);


--
-- Name: zeiss_tlr_20170918; Type: TABLE; Schema: gnm; Owner: -
--

CREATE TABLE zeiss_tlr_20170918 (
    keyrange character varying(6),
    calcs character varying(1),
    title character varying(25),
    tlr_order numeric(2,0),
    matcode numeric(2,0),
    execbw numeric(2,0),
    edtperc numeric(2,0),
    segdiam numeric(2,0),
    basech character varying(8),
    sagch character varying(8),
    segch character varying(8),
    fitadj numeric(1,0),
    rndtool boolean,
    trange numeric(1,0),
    addfin numeric(3,1),
    prthin boolean,
    isrl boolean,
    genalias character varying(1),
    rethipwr numeric(6,2),
    retlopwr numeric(6,2),
    kvalue numeric(4,2),
    retorder numeric(2,0),
    bowlsize numeric(4,1),
    carrier numeric(4,2),
    finech character varying(5),
    mercorder numeric(2,0),
    specialcod character varying(1),
    hiadd numeric(6,2),
    loadd numeric(6,2),
    istenths boolean,
    manufdes character varying(1)
);


SET search_path = public, pg_catalog;

--
-- Name: adjustment_type; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE adjustment_type (
    sk integer NOT NULL,
    adjustment_typeid character varying(30) NOT NULL,
    name character varying(100) DEFAULT 'Other'::character varying NOT NULL,
    othername character varying(100) NOT NULL
);


--
-- Name: adjustment_type_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE adjustment_type_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: adjustment_type_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE adjustment_type_sk_seq OWNED BY adjustment_type.sk;


--
-- Name: appointment; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE appointment (
    sk integer NOT NULL,
    appointmentid character varying(100),
    storesk integer NOT NULL,
    createddate timestamp without time zone NOT NULL,
    booking_startdate timestamp without time zone NOT NULL,
    booking_enddate timestamp without time zone NOT NULL,
    employeesk integer NOT NULL,
    customersk integer NOT NULL,
    statussk integer NOT NULL,
    appointment_typesk integer NOT NULL,
    mins integer,
    modified timestamp without time zone,
    prospect_firstname character varying(50),
    prospect_lastname character varying(50),
    prospect_title character varying(10),
    prospect_email character varying(150),
    prospect_homephone character varying(150),
    prospect_mobile character varying(150),
    prospect_workphone character varying(150),
    isdeleted boolean DEFAULT false NOT NULL
);


--
-- Name: appointment_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE appointment_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: appointment_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE appointment_sk_seq OWNED BY appointment.sk;


--
-- Name: appointment_type; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE appointment_type (
    sk integer NOT NULL,
    appointment_typeid character varying(30) NOT NULL,
    name character varying(100) NOT NULL
);


--
-- Name: appointment_type_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE appointment_type_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: appointment_type_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE appointment_type_sk_seq OWNED BY appointment_type.sk;


--
-- Name: invoice_orders; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE invoice_orders (
    sk integer NOT NULL,
    store_sk integer NOT NULL,
    invoice_sk integer NOT NULL,
    order_sk integer NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: invoiceitem_old; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE invoiceitem_old (
    invoicesk integer NOT NULL,
    pos integer NOT NULL,
    productsk integer NOT NULL,
    storesk integer NOT NULL,
    employeesk integer NOT NULL,
    serviceemployeesk integer NOT NULL,
    customersk integer NOT NULL,
    createddate timestamp without time zone NOT NULL,
    quantity integer NOT NULL,
    amount_incl_gst numeric(19,4),
    modified timestamp without time zone,
    amount_gst numeric(19,4),
    discount_incl_gst numeric(19,4),
    discount_gst numeric(19,4),
    cost_incl_gst numeric(19,4),
    cost_gst numeric(19,4),
    description character varying(500),
    payeesk integer,
    invoiceitemid character varying(60) DEFAULT ''::character varying NOT NULL,
    return_incl_gst numeric(19,4),
    return_gst numeric(19,4),
    bulkbillref character varying(100)
);


--
-- Name: order_history; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE order_history (
    sk integer NOT NULL,
    store_sk integer NOT NULL,
    customer_sk integer NOT NULL,
    source_id character varying(100),
    order_type character varying(100) NOT NULL,
    queue character varying(100),
    status character varying(100),
    source_created timestamp without time zone NOT NULL,
    source_modified timestamp without time zone NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: stock_price_history; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE stock_price_history (
    sk integer NOT NULL,
    store_sk integer NOT NULL,
    source_id character varying(100) NOT NULL,
    takeon_storeproduct_sk integer NOT NULL,
    supplier_cost numeric(19,2),
    supplier_cost_gst numeric(19,2),
    actual_cost numeric(19,2),
    actual_cost_gst numeric(19,2),
    supplier_rrp numeric(19,2),
    supplier_rrp_gst numeric(19,2),
    calculated_retail_price numeric(19,2),
    calculated_retail_price_gst numeric(19,2),
    override_retail_price numeric(19,2),
    override_retail_price_gst numeric(19,2),
    retail_price numeric(19,2),
    retail_price_gst numeric(19,2),
    created timestamp without time zone DEFAULT now() NOT NULL,
    product_gnm_sk integer
);


--
-- Name: store_stockmovement; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE store_stockmovement (
    sk integer NOT NULL,
    store_sk integer NOT NULL,
    source_id character varying(100) NOT NULL,
    takeon_storeproduct_sk integer NOT NULL,
    adjustmentdate timestamp without time zone,
    stock_price_history_sk integer NOT NULL,
    adjustment integer NOT NULL,
    reason character varying(20) NOT NULL,
    invoice_sk integer,
    customer_sk integer,
    avg_cost_price numeric(19,2),
    shipping_ref character varying(100),
    shipping_operator character varying(100),
    comments character varying(500),
    employee_sk integer,
    created timestamp without time zone DEFAULT now() NOT NULL,
    adjustmentdate_sk integer,
    product_gnm_sk integer,
    adjustment_type_sk integer DEFAULT 0 NOT NULL,
    actual_cost numeric(19,2),
    actual_cost_value numeric(19,2),
    moving_quantity numeric(19,2),
    moving_actual_cost_value numeric(19,2),
    moving_cost_per_unit numeric(19,2)
);


--
-- Name: storeproduct; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE storeproduct (
    sk integer NOT NULL,
    storeid character varying(12),
    productid character varying(500),
    type character varying(100),
    datecreated timestamp without time zone,
    name character varying(500),
    description character varying(500),
    retailprice numeric(19,4),
    wholesaleprice numeric(19,4),
    modified timestamp without time zone,
    last_purchase timestamp without time zone,
    last_sale timestamp without time zone,
    quantity integer,
    frame_model character varying(100) DEFAULT ''::character varying,
    frame_colour character varying(100) DEFAULT ''::character varying,
    frame_brand character varying(100) DEFAULT ''::character varying,
    frame_temple numeric(8,2) DEFAULT 0,
    frame_bridge numeric(8,2) DEFAULT 0,
    frame_eye_size numeric(8,2) DEFAULT 0,
    frame_depth numeric(8,2) DEFAULT 0,
    frame_material character varying(100) DEFAULT ''::character varying,
    lens_type character varying(100) DEFAULT ''::character varying,
    lens_refractive_index numeric(19,4) DEFAULT 0,
    lens_stock_grind character varying(10) DEFAULT ''::character varying,
    lens_size numeric(4,2) DEFAULT 0,
    lens_segment_size numeric(4,2) DEFAULT 0,
    internal_sku character varying(100),
    distributor character varying(100),
    product_gnm_sk integer
);


--
-- Name: cogs_sales; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW cogs_sales AS
 SELECT ii.invoicesk,
    ii.pos,
    ii.productsk,
    ii.storesk,
    ii.employeesk,
    ii.serviceemployeesk,
    ii.customersk,
    ii.createddate,
    ii.quantity,
    ii.amount_incl_gst,
    ii.modified,
    ii.amount_gst,
    ii.discount_incl_gst,
    ii.discount_gst,
    ii.cost_incl_gst,
    ii.cost_gst,
    ii.description,
    ii.payeesk,
    ii.invoiceitemid,
    ii.return_incl_gst,
    ii.return_gst,
    ii.bulkbillref,
    ( SELECT ph.actual_cost
           FROM stock_price_history ph
          WHERE ((ph.takeon_storeproduct_sk = ii.productsk) AND (ph.created <= ii.createddate))
          ORDER BY ph.created DESC
         LIMIT 1) AS takeon_current_cost,
    ( SELECT ph.actual_cost
           FROM stock_price_history ph
          WHERE ((ph.product_gnm_sk = tp.product_gnm_sk) AND (ph.created <= ii.createddate))
          ORDER BY ph.created DESC
         LIMIT 1) AS gnm_current_cost,
    ( SELECT m.actual_cost
           FROM store_stockmovement m
          WHERE ((m.invoice_sk = ii.invoicesk) AND (m.takeon_storeproduct_sk = ii.productsk))
         LIMIT 1) AS average_cost,
    ( SELECT oh.order_type
           FROM (invoice_orders io
             JOIN order_history oh ON ((io.order_sk = oh.sk)))
          WHERE (io.invoice_sk = ii.invoicesk)
          ORDER BY oh.source_modified DESC
         LIMIT 1) AS order_type
   FROM (invoiceitem_old ii
     JOIN storeproduct tp ON ((ii.productsk = tp.sk)))
  ORDER BY ii.createddate DESC;


--
-- Name: consult; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE consult (
    sk integer NOT NULL,
    consultid character varying(100),
    consultdate timestamp without time zone NOT NULL,
    storesk integer NOT NULL,
    employeesk integer NOT NULL,
    customersk integer NOT NULL,
    modified timestamp without time zone NOT NULL
);


--
-- Name: consult_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE consult_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: consult_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE consult_sk_seq OWNED BY consult.sk;


--
-- Name: consult_type; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE consult_type (
    sk integer NOT NULL,
    consult_typeid character varying(30) NOT NULL,
    description character varying(100) NOT NULL
);


--
-- Name: consult_type_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE consult_type_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: consult_type_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE consult_type_sk_seq OWNED BY consult_type.sk;


--
-- Name: consultitem; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE consultitem (
    sk integer NOT NULL,
    consultsk integer NOT NULL,
    consult_typesk integer NOT NULL
);


--
-- Name: consultitem_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE consultitem_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: consultitem_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE consultitem_sk_seq OWNED BY consultitem.sk;


--
-- Name: customer; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE customer (
    sk integer NOT NULL,
    createddate timestamp without time zone NOT NULL,
    customerid character varying(500),
    firstname character varying(50) NOT NULL,
    lastname character varying(50),
    middlename character varying(50),
    title character varying(10) NOT NULL,
    gender character varying(10) NOT NULL,
    birthdate date,
    preferredname character varying(50),
    preferredcontactmethod character varying(50),
    email character varying(150) NOT NULL,
    emailcreated timestamp without time zone DEFAULT ('now'::text)::date NOT NULL,
    phonecountryid integer,
    phonecreated timestamp without time zone DEFAULT ('now'::text)::date NOT NULL,
    address1 character varying(250),
    address2 character varying(250),
    address3 character varying(250),
    state character varying(60),
    suburb character varying(60),
    postcode integer,
    firstvisit timestamp without time zone,
    lastvisit timestamp without time zone,
    lastconsult timestamp without time zone,
    lastrecall timestamp without time zone,
    nextrecall timestamp without time zone,
    lastrecalldesc character varying(250),
    nextrecalldesc character varying(250),
    lastoptomseen integer,
    storesk integer DEFAULT 0 NOT NULL,
    healthfund character varying(50),
    modified timestamp without time zone,
    optout_recall boolean DEFAULT false NOT NULL,
    optout_marketing boolean DEFAULT false NOT NULL,
    deceased boolean DEFAULT false NOT NULL,
    homephone character varying(150),
    mobile character varying(150),
    workphone character varying(150),
    isaddrvalid boolean
);


--
-- Name: customer_ext; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE customer_ext (
    sk integer NOT NULL,
    customersk integer NOT NULL,
    s_recall character varying(1),
    s_cmsms character varying(1),
    s_cmmobile character varying(1),
    s_cmwphone character varying(1),
    s_cmhphone character varying(1),
    s_cmemail character varying(1),
    s_cmletter character varying(1),
    s_cmfno boolean,
    s_cmnoemail boolean,
    s_apnotif integer,
    s_apreminder integer,
    o_pref_recallint character varying(20),
    o_pref_recallsub character varying(20),
    o_pref_ordercoll character varying(20),
    o_pref_appointments character varying(20),
    o_pref_marketing character varying(20),
    o_pref_telephone character varying(20),
    o_badaddress boolean,
    o_badphone boolean,
    o_bademail boolean,
    o_noemail boolean,
    modified timestamp without time zone,
    o_inactive boolean,
    o_inactive_reason character varying(255)
);


--
-- Name: customer_ext_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE customer_ext_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: customer_ext_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE customer_ext_sk_seq OWNED BY customer_ext.sk;


--
-- Name: customer_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE customer_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: customer_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE customer_sk_seq OWNED BY customer.sk;


--
-- Name: d_date; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE d_date (
    sk integer NOT NULL,
    date_dim_id integer NOT NULL,
    date_actual date NOT NULL,
    epoch bigint NOT NULL,
    day_suffix character varying(4) NOT NULL,
    day_name character varying(9) NOT NULL,
    day_of_week integer NOT NULL,
    day_of_month integer NOT NULL,
    day_of_quarter integer NOT NULL,
    day_of_year integer NOT NULL,
    week_of_month integer NOT NULL,
    week_of_year integer NOT NULL,
    week_of_year_iso character(10) NOT NULL,
    month_actual integer NOT NULL,
    month_name character varying(9) NOT NULL,
    month_name_abbreviated character(3) NOT NULL,
    year_month_name character varying(8) NOT NULL,
    quarter_actual integer NOT NULL,
    quarter_name character varying(9) NOT NULL,
    year_calendar integer NOT NULL,
    year_calendar_name character varying(7) NOT NULL,
    year_financial integer NOT NULL,
    year_financial_name character varying(7) NOT NULL,
    first_day_of_week date NOT NULL,
    last_day_of_week date NOT NULL,
    first_day_of_month date NOT NULL,
    last_day_of_month date NOT NULL,
    first_day_of_quarter date NOT NULL,
    last_day_of_quarter date NOT NULL,
    first_day_of_year date NOT NULL,
    last_day_of_year date NOT NULL,
    yyyymm character(6) NOT NULL,
    yyyyq character(5) NOT NULL,
    weekend_indr boolean NOT NULL
);


--
-- Name: d_date_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE d_date_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: d_date_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE d_date_sk_seq OWNED BY d_date.sk;


--
-- Name: daily_invoice_bucket; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE daily_invoice_bucket (
    sk integer NOT NULL,
    daily_store_bucket_sk integer NOT NULL,
    invoice_sk integer NOT NULL,
    invoiceitemid character varying(50) NOT NULL
);


--
-- Name: daily_invoice_bucket_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE daily_invoice_bucket_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: daily_invoice_bucket_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE daily_invoice_bucket_sk_seq OWNED BY daily_invoice_bucket.sk;


--
-- Name: daily_job_bucket; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE daily_job_bucket (
    sk integer NOT NULL,
    daily_store_bucket_sk integer NOT NULL,
    job_sk integer NOT NULL
);


--
-- Name: daily_job_bucket_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE daily_job_bucket_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: daily_job_bucket_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE daily_job_bucket_sk_seq OWNED BY daily_job_bucket.sk;


--
-- Name: daily_matched_bucket; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE daily_matched_bucket (
    sk integer NOT NULL,
    invoice_daily_store_bucket_sk integer,
    job_daily_store_bucket_sk integer,
    stockmove_daily_store_bucket_sk integer,
    invoice_sk integer,
    invoiceitemid character varying(50),
    job_sk integer,
    store_stockmovement_sk integer,
    confidence_stockmovement integer,
    confidence_job integer,
    confidence_job_stockmovement integer
);


--
-- Name: daily_matched_bucket_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE daily_matched_bucket_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: daily_matched_bucket_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE daily_matched_bucket_sk_seq OWNED BY daily_matched_bucket.sk;


--
-- Name: daily_stockmovement_bucket; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE daily_stockmovement_bucket (
    sk integer NOT NULL,
    daily_store_bucket_sk integer NOT NULL,
    store_stockmovement_sk integer NOT NULL
);


--
-- Name: daily_stockmovement_bucket_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE daily_stockmovement_bucket_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: daily_stockmovement_bucket_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE daily_stockmovement_bucket_sk_seq OWNED BY daily_stockmovement_bucket.sk;


--
-- Name: daily_store_bucket; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE daily_store_bucket (
    sk integer NOT NULL,
    d_date_sk integer NOT NULL,
    store_sk integer NOT NULL,
    prod_ref_sk integer NOT NULL
);


--
-- Name: daily_store_bucket_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE daily_store_bucket_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: daily_store_bucket_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE daily_store_bucket_sk_seq OWNED BY daily_store_bucket.sk;


--
-- Name: dailysnapshot_cost; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE dailysnapshot_cost (
    createddate_sk integer,
    today date,
    cost numeric
);


--
-- Name: dailysnapshot_cost_stores; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE dailysnapshot_cost_stores (
    storeid character varying(12),
    today date,
    cost numeric
);


--
-- Name: dailysnapshot_revenue; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE dailysnapshot_revenue (
    createddate_sk integer,
    today date,
    revenue numeric
);


--
-- Name: dailysnapshot_revenue_stores; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE dailysnapshot_revenue_stores (
    storeid character varying(12),
    today date,
    revenue numeric
);


--
-- Name: dailytemp; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE dailytemp (
    year double precision,
    doy double precision,
    store_sk integer,
    product_gnm_sk integer,
    storeproduct_sk integer
);


--
-- Name: discountcode; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE discountcode (
    sk integer NOT NULL,
    storeid character varying(12) DEFAULT 'All'::character varying NOT NULL,
    discountitemcode character varying(100) NOT NULL,
    description character varying(150),
    valid_from timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone,
    valid_to timestamp without time zone DEFAULT '2099-12-31 00:00:00'::timestamp without time zone
);


--
-- Name: discountcode_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE discountcode_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: discountcode_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE discountcode_sk_seq OWNED BY discountcode.sk;


--
-- Name: employee; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE employee (
    sk integer NOT NULL,
    createddate timestamp without time zone NOT NULL,
    employeeid character varying(500),
    employeetype character varying(50),
    employmentstartdate date,
    employmentenddate date,
    firstname character varying(50) NOT NULL,
    lastname character varying(50),
    middlename character varying(50),
    title character varying(10) NOT NULL,
    gender character varying(10) NOT NULL,
    birthdate date,
    preferredname character varying(50),
    preferredcontactmethod character varying(10),
    email character varying(150) NOT NULL,
    emailcreated timestamp without time zone DEFAULT ('now'::text)::date NOT NULL,
    phonecountryid integer,
    phonenumber character varying(150),
    phonerawnumber character varying(150),
    phonemobile boolean NOT NULL,
    phonesmsenabled boolean NOT NULL,
    phonefax boolean NOT NULL,
    phonelandline boolean NOT NULL,
    phonecreated timestamp without time zone DEFAULT ('now'::text)::date NOT NULL,
    storesk integer DEFAULT 0 NOT NULL
);


--
-- Name: employee_providerno; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE employee_providerno (
    sk integer NOT NULL,
    employeesk integer NOT NULL,
    storesk integer NOT NULL,
    providerno character varying(30),
    modified timestamp without time zone DEFAULT timezone('Australia/Sydney'::text, now()) NOT NULL
);


--
-- Name: employee_providerno_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE employee_providerno_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: employee_providerno_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE employee_providerno_sk_seq OWNED BY employee_providerno.sk;


--
-- Name: employee_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE employee_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: employee_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE employee_sk_seq OWNED BY employee.sk;


--
-- Name: expense; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE expense (
    sk integer NOT NULL,
    expenseid character varying(100),
    storesk integer NOT NULL,
    createddate timestamp without time zone NOT NULL,
    employeesk integer NOT NULL,
    expense_typesk integer NOT NULL,
    description character varying(100),
    amount_incl_gst numeric(19,4),
    amount_gst numeric(19,4),
    modified timestamp without time zone
);


--
-- Name: expense_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE expense_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: expense_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE expense_sk_seq OWNED BY expense.sk;


--
-- Name: expense_type; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE expense_type (
    sk integer NOT NULL,
    expense_typeid character varying(30) NOT NULL,
    name character varying(100) NOT NULL
);


--
-- Name: expense_type_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE expense_type_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: expense_type_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE expense_type_sk_seq OWNED BY expense_type.sk;


--
-- Name: gnm_invoice_orders; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE gnm_invoice_orders (
    orderid character varying(100),
    customer_sk integer,
    order_date timestamp without time zone,
    jobid character varying(100),
    job_date timestamp without time zone,
    product_sk integer,
    supplier_sk integer,
    productid character varying(500),
    type character varying(100)
);


--
-- Name: inventory_snapshot; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE inventory_snapshot (
    h integer,
    store_sk integer,
    date_sk integer,
    accumalitive_adjustment bigint,
    accumalitive_actual_value numeric,
    total_receipt bigint,
    total_sale bigint,
    total_return bigint,
    total_writeoff bigint,
    total_theft bigint,
    total_receipt_actual_value numeric,
    total_sale_actual_value numeric,
    total_return_actual_value numeric,
    total_writeoff_actual_value numeric,
    total_theft_actual_value numeric,
    takeon_storeproduct_sk integer
);


--
-- Name: invoice; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE invoice (
    sk integer NOT NULL,
    invoiceid character varying(100),
    createddate timestamp without time zone NOT NULL,
    storesk integer NOT NULL,
    employeesk integer NOT NULL,
    customersk integer NOT NULL,
    amount_incl_gst numeric(19,4),
    modified timestamp without time zone,
    amount_gst numeric(19,4),
    reversalref character varying(100),
    rebateref character varying(100)
);


--
-- Name: invoice_dailysnapshot; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE invoice_dailysnapshot (
    createddate_sk integer,
    today date,
    revenue numeric
);


--
-- Name: invoice_dailysnapshot_all; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE invoice_dailysnapshot_all (
    createddate_sk integer,
    today date,
    today_revenue numeric,
    yesterday_revenue numeric,
    today_revenue_py numeric,
    yesterday_revenue_py numeric,
    day_lastweek_revenue numeric,
    day_lastweek_revenue_py numeric
);


--
-- Name: invoice_order_job; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE invoice_order_job (
    sk integer NOT NULL,
    store_sk integer NOT NULL,
    invoice_sk integer NOT NULL,
    order_id_nk character varying(100),
    job_id_nk character varying(100),
    created timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: invoice_order_job_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE invoice_order_job_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: invoice_order_job_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE invoice_order_job_sk_seq OWNED BY invoice_order_job.sk;


--
-- Name: invoice_orders_reconcile; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE invoice_orders_reconcile (
    pmsorderid text,
    invoicesk integer,
    framesk integer,
    right_lenssk integer,
    left_lenssk integer,
    storesk integer,
    pms_invoicenum character varying(100),
    orderid character varying(100),
    customer_sk integer,
    order_date timestamp without time zone,
    jobid character varying(100),
    job_date timestamp without time zone,
    product_sk integer,
    supplier_sk integer,
    productid character varying(500),
    type character varying(100)
);


--
-- Name: invoice_orders_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE invoice_orders_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: invoice_orders_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE invoice_orders_sk_seq OWNED BY invoice_orders.sk;


--
-- Name: invoice_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE invoice_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: invoice_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE invoice_sk_seq OWNED BY invoice.sk;


--
-- Name: invoiceitem; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE invoiceitem (
    invoicesk integer NOT NULL,
    pos integer NOT NULL,
    productsk integer NOT NULL,
    storesk integer NOT NULL,
    employeesk integer NOT NULL,
    serviceemployeesk integer NOT NULL,
    customersk integer NOT NULL,
    createddate timestamp without time zone NOT NULL,
    quantity integer NOT NULL,
    amount_incl_gst numeric(19,4),
    modified timestamp without time zone,
    amount_gst numeric(19,4),
    discount_incl_gst numeric(19,4),
    discount_gst numeric(19,4),
    cost_incl_gst numeric(19,4),
    cost_gst numeric(19,4),
    description character varying(500),
    payeesk integer,
    invoiceitemid character varying(60) DEFAULT ''::character varying NOT NULL,
    return_incl_gst numeric(19,4),
    return_gst numeric(19,4),
    bulkbillref character varying(100),
    createddate_sk integer,
    pms_cost numeric(19,2),
    gnm_current_cost numeric(19,2),
    takeon_avg_cost numeric(19,2),
    order_type character varying(90),
    order_id character varying(90)
);


--
-- Name: product_gnm; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE product_gnm (
    sk integer NOT NULL,
    productid character varying(500),
    type character varying(100),
    datecreated timestamp without time zone,
    name character varying(500),
    description character varying(500),
    retailprice numeric(19,4),
    wholesaleprice numeric(19,4),
    modified timestamp without time zone,
    last_purchase timestamp without time zone,
    last_sale timestamp without time zone,
    quantity integer,
    frame_model character varying(100) DEFAULT ''::character varying,
    frame_colour character varying(100) DEFAULT ''::character varying,
    frame_brand character varying(100) DEFAULT ''::character varying,
    frame_temple numeric(8,2) DEFAULT 0,
    frame_bridge numeric(8,2) DEFAULT 0,
    frame_eye_size numeric(8,2) DEFAULT 0,
    frame_depth numeric(8,2) DEFAULT 0,
    frame_material character varying(100) DEFAULT ''::character varying,
    lens_type character varying(100) DEFAULT ''::character varying,
    lens_refractive_index numeric(19,4) DEFAULT 0,
    lens_stock_grind character varying(10) DEFAULT ''::character varying,
    lens_size numeric(4,2) DEFAULT 0,
    lens_segment_size numeric(4,2) DEFAULT 0,
    internal_sku character varying(100),
    distributor character varying(100),
    created timestamp without time zone DEFAULT now() NOT NULL,
    brand_name character varying(100),
    consignment boolean,
    apn character varying(100),
    supplier_sku character varying(100),
    eyetalk_id character varying(100)
);


--
-- Name: store; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE store (
    sk integer NOT NULL,
    storeid character varying(12) NOT NULL,
    name character varying(100) NOT NULL,
    chainid character varying(100) DEFAULT ''::character varying NOT NULL,
    isactive boolean DEFAULT false NOT NULL,
    pms character varying(20),
    store_nk character varying(60),
    acquisition_date timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL,
    nk character varying(60)
);


--
-- Name: invoiceitem_cogs_view; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW invoiceitem_cogs_view AS
 SELECT s.store_nk,
    ii.invoiceitemid,
    e.employeeid,
    ii.createddate,
    ii.modified,
    ii.productsk,
    ii.invoicesk,
    ii.storesk,
    sp.description AS frame_description,
    sp.productid,
    pg.name,
    ii.description,
    ii.amount_gst,
    ii.amount_incl_gst,
    ii.discount_gst,
    ii.discount_incl_gst,
    ii.cost_incl_gst,
    ii.cost_gst,
    ii.bulkbillref
   FROM ((((invoiceitem ii
     JOIN store s ON ((s.sk = ii.storesk)))
     JOIN storeproduct sp ON (((sp.sk = ii.productsk) AND ((sp.type)::text = 'Frame'::text))))
     LEFT JOIN employee e ON ((e.sk = ii.employeesk)))
     LEFT JOIN product_gnm pg ON ((pg.sk = sp.product_gnm_sk)));


--
-- Name: job_history; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE job_history (
    sk integer NOT NULL,
    source_id character varying(100),
    order_id character varying(100) NOT NULL,
    order_sk integer NOT NULL,
    job_type character varying(100) NOT NULL,
    product_sk integer NOT NULL,
    supplier_sk integer NOT NULL,
    queue character varying(100),
    status character varying(100),
    source_created timestamp without time zone NOT NULL,
    source_modified timestamp without time zone NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: job_history_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE job_history_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: job_history_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE job_history_sk_seq OWNED BY job_history.sk;


--
-- Name: job_process_status; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE job_process_status (
    sk integer NOT NULL,
    jobname character varying(255) NOT NULL,
    status character varying(10) NOT NULL,
    description character varying(255),
    lastexecution timestamp without time zone NOT NULL
);


--
-- Name: job_process_status_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE job_process_status_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: job_process_status_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE job_process_status_sk_seq OWNED BY job_process_status.sk;


--
-- Name: last_job; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW last_job AS
 SELECT jh.sk,
    jh.source_id,
    jh.order_id,
    jh.order_sk,
    jh.job_type,
    jh.product_sk,
    jh.supplier_sk,
    jh.queue,
    jh.status,
    jh.source_created,
    jh.source_modified,
    jh.created
   FROM (job_history jh
     JOIN ( SELECT jh_1.source_id,
            max(jh_1.sk) AS max_sk,
            max(jh_1.source_modified) AS max_date,
            count(*) AS count
           FROM job_history jh_1
          GROUP BY jh_1.source_id) x ON ((x.max_sk = jh.sk)))
  ORDER BY jh.source_id;


--
-- Name: prod_ref; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE prod_ref (
    sk integer NOT NULL,
    brand character varying(200) NOT NULL,
    model character varying(200) NOT NULL,
    colour character varying(200) NOT NULL,
    total integer
);


--
-- Name: mdaily_invoice; Type: MATERIALIZED VIEW; Schema: public; Owner: -
--

CREATE MATERIALIZED VIEW mdaily_invoice AS
 SELECT dsb.sk AS dsb_sk,
    dsb.d_date_sk,
    dsb.prod_ref_sk,
    dsb.store_sk AS dsb_store_sk,
    pr.brand,
    pr.model,
    pr.colour,
    dd.date_actual,
    dd.day_name,
    dd.day_of_year AS doy,
    dd.year_calendar AS year,
    s.storeid AS store_name,
    i.invoiceid,
        CASE
            WHEN (sp.product_gnm_sk = 0) THEN 'takeon'::text
            ELSE 'gnm'::text
        END AS source,
        CASE
            WHEN ((pg.productid)::text = ''::text) THEN (sp.productid)::text
            ELSE (((sp.productid)::text || '-'::text) || (pg.productid)::text)
        END AS gnm_productid,
    ii.invoicesk,
    ii.pos,
    ii.productsk,
    ii.storesk,
    ii.employeesk,
    ii.serviceemployeesk,
    ii.customersk,
    ii.createddate,
    ii.quantity,
    ii.amount_incl_gst,
    ii.modified,
    ii.amount_gst,
    ii.discount_incl_gst,
    ii.discount_gst,
    ii.cost_incl_gst,
    ii.cost_gst,
    ii.description,
    ii.payeesk,
    ii.invoiceitemid,
    ii.return_incl_gst,
    ii.return_gst,
    ii.bulkbillref,
    ii.createddate_sk,
    ii.pms_cost,
    ii.gnm_current_cost,
    ii.takeon_avg_cost,
    ii.order_type,
    ii.order_id
   FROM ((((((((daily_invoice_bucket dib
     JOIN daily_store_bucket dsb ON ((dsb.sk = dib.daily_store_bucket_sk)))
     JOIN invoiceitem ii ON ((((ii.invoiceitemid)::text = (dib.invoiceitemid)::text) AND (ii.invoicesk = dib.invoice_sk))))
     JOIN d_date dd ON ((dd.sk = dsb.d_date_sk)))
     JOIN prod_ref pr ON ((pr.sk = dsb.prod_ref_sk)))
     JOIN store s ON ((s.sk = ii.storesk)))
     JOIN storeproduct sp ON ((sp.sk = ii.productsk)))
     JOIN invoice i ON ((i.sk = ii.invoicesk)))
     JOIN product_gnm pg ON ((pg.sk = sp.product_gnm_sk)))
  WITH NO DATA;


--
-- Name: mdaily_job; Type: MATERIALIZED VIEW; Schema: public; Owner: -
--

CREATE MATERIALIZED VIEW mdaily_job AS
 SELECT dsb.sk AS dsb_sk,
    dsb.d_date_sk,
    dsb.prod_ref_sk,
    dsb.store_sk AS dsb_store_sk,
    pr.brand,
    pr.model,
    pr.colour,
    dd.date_actual,
    dd.day_name,
    dd.day_of_year AS doy,
    dd.year_calendar AS year,
    jh.sk,
    jh.source_id,
    jh.order_id,
    jh.order_sk,
    jh.job_type,
    jh.product_sk,
    jh.supplier_sk,
    jh.queue,
    jh.status,
    jh.source_created,
    jh.source_modified,
    jh.created
   FROM ((((daily_job_bucket dib
     JOIN daily_store_bucket dsb ON ((dsb.sk = dib.daily_store_bucket_sk)))
     JOIN job_history jh ON ((jh.sk = dib.job_sk)))
     JOIN d_date dd ON ((dd.sk = dsb.d_date_sk)))
     JOIN prod_ref pr ON ((pr.sk = dsb.prod_ref_sk)))
  WITH NO DATA;


--
-- Name: mdaily_stockmovement; Type: MATERIALIZED VIEW; Schema: public; Owner: -
--

CREATE MATERIALIZED VIEW mdaily_stockmovement AS
 SELECT dsb.sk AS dsb_sk,
    dsb.d_date_sk,
    dsb.prod_ref_sk,
    dsb.store_sk AS dsb_store_sk,
    pr.brand,
    pr.model,
    pr.colour,
    dd.date_actual,
    dd.day_name,
    dd.day_of_year AS doy,
    dd.year_calendar AS year,
    s.storeid AS store_name,
    sp.productid,
    pg.productid AS gnm_productid,
    i.invoiceid,
    a.name AS adjustment_type,
    ssm.sk,
    ssm.store_sk,
    ssm.source_id,
    ssm.takeon_storeproduct_sk,
    ssm.adjustmentdate,
    ssm.stock_price_history_sk,
    ssm.adjustment,
    ssm.reason,
    ssm.invoice_sk,
    ssm.customer_sk,
    ssm.avg_cost_price,
    ssm.shipping_ref,
    ssm.shipping_operator,
    ssm.comments,
    ssm.employee_sk,
    ssm.created,
    ssm.adjustmentdate_sk,
    ssm.product_gnm_sk,
    ssm.adjustment_type_sk,
    ssm.actual_cost,
    ssm.actual_cost_value,
    ssm.moving_quantity,
    ssm.moving_actual_cost_value,
    ssm.moving_cost_per_unit
   FROM (((((((((daily_stockmovement_bucket dib
     JOIN daily_store_bucket dsb ON ((dsb.sk = dib.daily_store_bucket_sk)))
     JOIN store_stockmovement ssm ON ((ssm.sk = dib.store_stockmovement_sk)))
     JOIN d_date dd ON ((dd.sk = dsb.d_date_sk)))
     JOIN prod_ref pr ON ((pr.sk = dsb.prod_ref_sk)))
     JOIN storeproduct sp ON ((sp.sk = ssm.takeon_storeproduct_sk)))
     JOIN invoice i ON ((i.sk = ssm.invoice_sk)))
     JOIN store s ON ((s.sk = dsb.store_sk)))
     JOIN adjustment_type a ON ((a.sk = ssm.adjustment_type_sk)))
     JOIN product_gnm pg ON ((pg.sk = sp.product_gnm_sk)))
  WITH NO DATA;


--
-- Name: order_history_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE order_history_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: order_history_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE order_history_sk_seq OWNED BY order_history.sk;


--
-- Name: payee; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE payee (
    sk integer NOT NULL,
    storeid character varying(12),
    payeeid character varying(100),
    name character varying(200),
    payeetype character varying(150),
    chainid character varying(100) DEFAULT ''::character varying NOT NULL
);


--
-- Name: payee_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE payee_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: payee_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE payee_sk_seq OWNED BY payee.sk;


--
-- Name: payment; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE payment (
    sk integer NOT NULL,
    paymentid character varying(100),
    createddate timestamp without time zone NOT NULL,
    amount_incl_gst numeric(19,4),
    modified timestamp without time zone,
    amount_gst numeric(19,4),
    storeid character varying(100) DEFAULT ''::character varying,
    payeeid character varying(100) DEFAULT ''::character varying,
    employeeid character varying(100) DEFAULT ''::character varying,
    customerid character varying(100) DEFAULT ''::character varying,
    payment_typeid character varying(100) DEFAULT ''::character varying
);


--
-- Name: payment_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE payment_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: payment_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE payment_sk_seq OWNED BY payment.sk;


--
-- Name: payment_type; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE payment_type (
    sk integer NOT NULL,
    payment_typeid character varying(30) NOT NULL,
    name character varying(100) NOT NULL
);


--
-- Name: payment_type_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE payment_type_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: payment_type_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE payment_type_sk_seq OWNED BY payment_type.sk;


--
-- Name: paymentitem; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE paymentitem (
    storesk integer NOT NULL,
    paymentsk integer NOT NULL,
    invoicesk integer NOT NULL,
    customersk integer NOT NULL,
    payment_typesk integer NOT NULL,
    payment_employeesk integer NOT NULL,
    invoice_employeesk integer NOT NULL,
    payment_createddate timestamp without time zone NOT NULL,
    invoice_createddate timestamp without time zone NOT NULL,
    payment_amount_incl_gst numeric(19,4),
    invoice_amount_incl_gst numeric(19,4),
    modified timestamp without time zone,
    payment_amount_gst numeric(19,4),
    paymentitemid character varying(60) DEFAULT ''::character varying NOT NULL,
    payeesk integer,
    rebateref character varying(30)
);


--
-- Name: pms_invoice_orders; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE pms_invoice_orders (
    orderid text,
    ordersk integer,
    invoicesk integer,
    framesk integer,
    right_lenssk integer,
    left_lenssk integer,
    storesk integer,
    pms_invoicenum character varying(100)
);


--
-- Name: prod_common; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE prod_common (
    sk integer NOT NULL,
    prod_ref_sk integer NOT NULL,
    product_gnm_sk integer NOT NULL,
    storeproduct_sk integer NOT NULL
);


--
-- Name: prod_common_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE prod_common_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: prod_common_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE prod_common_sk_seq OWNED BY prod_common.sk;


--
-- Name: prod_ref_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE prod_ref_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: prod_ref_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE prod_ref_sk_seq OWNED BY prod_ref.sk;


--
-- Name: product_gnm_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE product_gnm_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: product_gnm_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE product_gnm_sk_seq OWNED BY product_gnm.sk;


--
-- Name: product_price_history; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE product_price_history (
    id bigint NOT NULL,
    product_id character varying(8) NOT NULL,
    supplier_cost numeric(19,2),
    supplier_cost_gst numeric(19,2),
    actual_cost numeric(19,2),
    actual_cost_gst numeric(19,2),
    supplier_rrp numeric(19,2),
    supplier_rrp_gst numeric(19,2),
    calculated_retail_price numeric(19,2),
    calculated_retail_price_gst numeric(19,2),
    override_retail_price numeric(19,2),
    override_retail_price_gst numeric(19,2),
    retail_price numeric(19,2),
    retail_price_gst numeric(19,2),
    effective_from_time timestamp without time zone NOT NULL,
    effective_to_time timestamp without time zone
);


--
-- Name: product_price_history_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE product_price_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: product_price_history_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE product_price_history_id_seq OWNED BY product_price_history.id;


--
-- Name: schema_version; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE schema_version (
    installed_rank integer NOT NULL,
    version character varying(50),
    description character varying(200) NOT NULL,
    type character varying(20) NOT NULL,
    script character varying(1000) NOT NULL,
    checksum integer,
    installed_by character varying(100) NOT NULL,
    installed_on timestamp without time zone DEFAULT now() NOT NULL,
    execution_time integer NOT NULL,
    success boolean NOT NULL
);


--
-- Name: seqtab; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE seqtab (
    i integer NOT NULL,
    x integer
);


--
-- Name: seqtab_i_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE seqtab_i_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: seqtab_i_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE seqtab_i_seq OWNED BY seqtab.i;


--
-- Name: spectaclejob; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE spectaclejob (
    sk integer NOT NULL,
    invoicesk integer,
    framesk integer,
    right_lenssk integer,
    left_lenssk integer,
    storesk integer NOT NULL,
    createddate timestamp without time zone NOT NULL,
    orderdate timestamp without time zone,
    receiveddate timestamp without time zone,
    pickupdate timestamp without time zone,
    modified timestamp without time zone NOT NULL,
    spectaclejobid character varying(60) DEFAULT ''::character varying NOT NULL,
    pms_invoicenum character varying(100)
);


--
-- Name: spectaclejob_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE spectaclejob_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: spectaclejob_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE spectaclejob_sk_seq OWNED BY spectaclejob.sk;


--
-- Name: status; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE status (
    sk integer NOT NULL,
    statusid character varying(30) NOT NULL,
    name character varying(100) NOT NULL
);


--
-- Name: status_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE status_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: status_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE status_sk_seq OWNED BY status.sk;


--
-- Name: stock_price_history_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE stock_price_history_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: stock_price_history_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE stock_price_history_sk_seq OWNED BY stock_price_history.sk;


--
-- Name: store_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE store_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: store_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE store_sk_seq OWNED BY store.sk;


--
-- Name: store_stock_history; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE store_stock_history (
    sk integer NOT NULL,
    store_sk integer NOT NULL,
    source_id character varying(100) NOT NULL,
    takeon_storeproduct_sk integer NOT NULL,
    product_gnm_sk integer NOT NULL,
    total integer NOT NULL,
    prior_total integer NOT NULL,
    reason character varying(20) NOT NULL,
    comments character varying(500),
    employee_sk integer,
    created timestamp without time zone DEFAULT now()
);


--
-- Name: store_stock_history_adj; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE store_stock_history_adj (
    sk integer NOT NULL,
    store_sk integer NOT NULL,
    nk character varying(100),
    takeon_storeproduct_sk integer,
    total integer NOT NULL,
    prior_total integer NOT NULL,
    employee_sk integer,
    source_staged timestamp without time zone NOT NULL,
    created timestamp without time zone DEFAULT now(),
    product_gnm_sk integer
);


--
-- Name: store_stock_history_adj_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE store_stock_history_adj_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: store_stock_history_adj_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE store_stock_history_adj_sk_seq OWNED BY store_stock_history_adj.sk;


--
-- Name: store_stock_history_sk_adj_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE store_stock_history_sk_adj_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: store_stockmovement_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE store_stockmovement_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: store_stockmovement_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE store_stockmovement_sk_seq OWNED BY store_stockmovement.sk;


--
-- Name: storeproduct_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE storeproduct_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: storeproduct_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE storeproduct_sk_seq OWNED BY storeproduct.sk;


--
-- Name: supplier_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE supplier_sk_seq
    START WITH 60
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: supplier; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE supplier (
    sk integer DEFAULT nextval('supplier_sk_seq'::regclass) NOT NULL,
    externalref bigint NOT NULL,
    supplierid character varying(255) NOT NULL,
    name character varying(255) NOT NULL
);


--
-- Name: supplierproduct_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE supplierproduct_sk_seq
    START WITH 500
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: supplierproduct; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE supplierproduct (
    sk integer DEFAULT nextval('supplierproduct_sk_seq'::regclass) NOT NULL,
    suppliersku character varying(255) NOT NULL,
    suppliersk integer NOT NULL,
    frame_model character varying(255),
    frame_colour character varying(100),
    internal_sku character varying(100),
    frame_brand character varying(100)
);


--
-- Name: t1; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW t1 AS
 SELECT d.sk,
    d.date_dim_id,
    d.date_actual,
    d.epoch,
    d.day_suffix,
    d.day_name,
    d.day_of_week,
    d.day_of_month,
    d.day_of_quarter,
    d.day_of_year,
    d.week_of_month,
    d.week_of_year,
    d.week_of_year_iso,
    d.month_actual,
    d.month_name,
    d.month_name_abbreviated,
    d.year_month_name,
    d.quarter_actual,
    d.quarter_name,
    d.year_calendar,
    d.year_calendar_name,
    d.year_financial,
    d.year_financial_name,
    d.first_day_of_week,
    d.last_day_of_week,
    d.first_day_of_month,
    d.last_day_of_month,
    d.first_day_of_quarter,
    d.last_day_of_quarter,
    d.first_day_of_year,
    d.last_day_of_year,
    d.yyyymm,
    d.yyyyq,
    d.weekend_indr,
    m.adjustment AS measure
   FROM (store_stockmovement m
     JOIN d_date d ON ((m.adjustmentdate_sk = d.sk)));


--
-- Name: takeon_product; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE takeon_product (
    sk integer NOT NULL,
    store_sk integer NOT NULL,
    supplier_sk integer NOT NULL,
    source_id character varying(100) NOT NULL,
    supplier_sku character varying(100) NOT NULL,
    name character varying(500) NOT NULL,
    description character varying(500) NOT NULL,
    model character varying(500) NOT NULL,
    brand character varying(500) NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: takeon_product_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE takeon_product_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: takeon_product_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE takeon_product_sk_seq OWNED BY takeon_product.sk;


--
-- Name: tmp_storeprod1; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE tmp_storeprod1 (
    sk integer,
    pname text,
    productid character varying(500),
    storeid character varying(12),
    name character varying(500),
    description character varying(500),
    internal_sku character varying(100)
);


--
-- Name: unmatched_orders; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE unmatched_orders (
    sk integer,
    store_sk integer,
    customer_sk integer,
    source_id character varying(100),
    order_type character varying(100),
    queue character varying(100),
    status character varying(100),
    source_created timestamp without time zone,
    source_modified timestamp without time zone,
    created timestamp without time zone
);


--
-- Name: v_cogs_first_receipt; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW v_cogs_first_receipt AS
 SELECT y.sk,
    y.adjustmentdate,
    y.product_gnm_sk,
    y.adjustment,
    y.actual_cost,
    ((y.adjustment)::numeric * y.actual_cost) AS actual_cost_value,
    ((y.adjustment)::numeric * y.actual_cost) AS moving_actual_cost_value,
    y.actual_cost AS moving_cost_per_unit,
    ( SELECT im.sk
           FROM store_stockmovement im
          WHERE ((im.product_gnm_sk = y.product_gnm_sk) AND (im.adjustmentdate >= y.adjustmentdate) AND (im.sk <> y.sk))
          ORDER BY im.adjustmentdate, im.sk
         LIMIT 1) AS n1
   FROM ( SELECT t.sk,
            t.store_sk,
            t.source_id,
            t.takeon_storeproduct_sk,
            t.adjustmentdate,
            t.stock_price_history_sk,
            t.adjustment,
            t.reason,
            t.invoice_sk,
            t.customer_sk,
            t.avg_cost_price,
            t.shipping_ref,
            t.shipping_operator,
            t.comments,
            t.employee_sk,
            t.created,
            t.adjustmentdate_sk,
            t.product_gnm_sk,
            t.adjustment_type_sk,
            t.actual_cost,
            row_number() OVER (PARTITION BY t.product_gnm_sk ORDER BY t.adjustmentdate, t.sk) AS r
           FROM ( SELECT m.sk,
                    m.store_sk,
                    m.source_id,
                    m.takeon_storeproduct_sk,
                    m.adjustmentdate,
                    m.stock_price_history_sk,
                    m.adjustment,
                    m.reason,
                    m.invoice_sk,
                    m.customer_sk,
                    m.avg_cost_price,
                    m.shipping_ref,
                    m.shipping_operator,
                    m.comments,
                    m.employee_sk,
                    m.created,
                    m.adjustmentdate_sk,
                    m.product_gnm_sk,
                    m.adjustment_type_sk,
                    ( SELECT h.actual_cost
                           FROM stock_price_history h
                          WHERE ((h.product_gnm_sk = m.product_gnm_sk) AND (h.created <= m.adjustmentdate))
                          ORDER BY h.created DESC
                         LIMIT 1) AS actual_cost
                   FROM (store_stockmovement m
                     JOIN adjustment_type a ON (((m.adjustment_type_sk = a.sk) AND ((a.adjustment_typeid)::text = 'RESTOCK'::text))))
                  WHERE (m.product_gnm_sk <> 0)) t
          WHERE (t.actual_cost IS NOT NULL)) y
  WHERE (y.r = 1);


--
-- Name: vbyd_invoice; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW vbyd_invoice AS
 SELECT date_part('year'::text, sm.createddate) AS year,
    date_part('doy'::text, sm.createddate) AS doy,
    date_part('month'::text, sm.createddate) AS month,
    date_part('dow'::text, sm.createddate) AS dow,
    date_part('day'::text, sm.createddate) AS day,
    sm.sk
   FROM invoice sm
  GROUP BY (date_part('year'::text, sm.createddate)), (date_part('doy'::text, sm.createddate)), (date_part('month'::text, sm.createddate)), (date_part('dow'::text, sm.createddate)), (date_part('day'::text, sm.createddate)), sm.sk;


--
-- Name: vbyd_invoiceitem; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW vbyd_invoiceitem AS
 SELECT date_part('year'::text, sm.createddate) AS year,
    date_part('doy'::text, sm.createddate) AS doy,
    date_part('month'::text, sm.createddate) AS month,
    date_part('dow'::text, sm.createddate) AS dow,
    date_part('day'::text, sm.createddate) AS day,
    sm.invoicesk,
    sm.pos
   FROM invoiceitem sm
  GROUP BY (date_part('year'::text, sm.createddate)), (date_part('doy'::text, sm.createddate)), (date_part('month'::text, sm.createddate)), (date_part('dow'::text, sm.createddate)), (date_part('day'::text, sm.createddate)), sm.invoicesk, sm.pos;


--
-- Name: vbyd_invoiceitem_reconcile; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW vbyd_invoiceitem_reconcile AS
 SELECT date_part('year'::text, sm.createddate) AS year,
    date_part('doy'::text, sm.createddate) AS doy,
    date_part('month'::text, sm.createddate) AS month,
    date_part('dow'::text, sm.createddate) AS dow,
    date_part('day'::text, sm.createddate) AS day,
    sm.invoicesk
   FROM invoiceitem sm
  GROUP BY (date_part('year'::text, sm.createddate)), (date_part('doy'::text, sm.createddate)), (date_part('month'::text, sm.createddate)), (date_part('dow'::text, sm.createddate)), (date_part('day'::text, sm.createddate)), sm.invoicesk;


--
-- Name: vbyd_job_history; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW vbyd_job_history AS
 SELECT date_part('year'::text, sm.source_created) AS year,
    date_part('doy'::text, sm.source_created) AS doy,
    date_part('month'::text, sm.source_created) AS month,
    date_part('dow'::text, sm.source_created) AS dow,
    date_part('day'::text, sm.source_created) AS day,
    sm.sk
   FROM job_history sm
  GROUP BY (date_part('year'::text, sm.source_created)), (date_part('doy'::text, sm.source_created)), (date_part('month'::text, sm.source_created)), (date_part('dow'::text, sm.source_created)), (date_part('day'::text, sm.source_created)), sm.sk;


--
-- Name: vbyd_order_history; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW vbyd_order_history AS
 SELECT date_part('year'::text, sm.source_created) AS year,
    date_part('doy'::text, sm.source_created) AS doy,
    date_part('month'::text, sm.source_created) AS month,
    date_part('dow'::text, sm.source_created) AS dow,
    date_part('day'::text, sm.source_created) AS day,
    sm.sk
   FROM order_history sm
  GROUP BY (date_part('year'::text, sm.source_created)), (date_part('doy'::text, sm.source_created)), (date_part('month'::text, sm.source_created)), (date_part('dow'::text, sm.source_created)), (date_part('day'::text, sm.source_created)), sm.sk;


--
-- Name: vbyd_stock_price_history; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW vbyd_stock_price_history AS
 SELECT date_part('year'::text, sm.created) AS year,
    date_part('doy'::text, sm.created) AS doy,
    date_part('month'::text, sm.created) AS month,
    date_part('dow'::text, sm.created) AS dow,
    date_part('day'::text, sm.created) AS day,
    sm.sk
   FROM stock_price_history sm
  GROUP BY (date_part('year'::text, sm.created)), (date_part('doy'::text, sm.created)), (date_part('month'::text, sm.created)), (date_part('dow'::text, sm.created)), (date_part('day'::text, sm.created)), sm.sk;


--
-- Name: vbyd_store_stockmovement; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW vbyd_store_stockmovement AS
 SELECT date_part('year'::text, sm.adjustmentdate) AS year,
    date_part('doy'::text, sm.adjustmentdate) AS doy,
    date_part('month'::text, sm.adjustmentdate) AS month,
    date_part('dow'::text, sm.adjustmentdate) AS dow,
    date_part('day'::text, sm.adjustmentdate) AS day,
    sm.sk
   FROM store_stockmovement sm
  GROUP BY (date_part('year'::text, sm.adjustmentdate)), (date_part('doy'::text, sm.adjustmentdate)), (date_part('month'::text, sm.adjustmentdate)), (date_part('dow'::text, sm.adjustmentdate)), (date_part('day'::text, sm.adjustmentdate)), sm.sk;


--
-- Name: vdaily_invoice; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW vdaily_invoice AS
 SELECT mdaily_invoice.dsb_sk,
    mdaily_invoice.d_date_sk,
    mdaily_invoice.prod_ref_sk,
    mdaily_invoice.dsb_store_sk,
    mdaily_invoice.brand,
    mdaily_invoice.model,
    mdaily_invoice.colour,
    mdaily_invoice.date_actual,
    mdaily_invoice.day_name,
    mdaily_invoice.doy,
    mdaily_invoice.year,
    mdaily_invoice.store_name,
    mdaily_invoice.invoiceid,
    mdaily_invoice.source,
    mdaily_invoice.gnm_productid,
    mdaily_invoice.invoicesk,
    mdaily_invoice.pos,
    mdaily_invoice.productsk,
    mdaily_invoice.storesk,
    mdaily_invoice.employeesk,
    mdaily_invoice.serviceemployeesk,
    mdaily_invoice.customersk,
    mdaily_invoice.createddate,
    mdaily_invoice.quantity,
    mdaily_invoice.amount_incl_gst,
    mdaily_invoice.modified,
    mdaily_invoice.amount_gst,
    mdaily_invoice.discount_incl_gst,
    mdaily_invoice.discount_gst,
    mdaily_invoice.cost_incl_gst,
    mdaily_invoice.cost_gst,
    mdaily_invoice.description,
    mdaily_invoice.payeesk,
    mdaily_invoice.invoiceitemid,
    mdaily_invoice.return_incl_gst,
    mdaily_invoice.return_gst,
    mdaily_invoice.bulkbillref,
    mdaily_invoice.createddate_sk,
    mdaily_invoice.pms_cost,
    mdaily_invoice.gnm_current_cost,
    mdaily_invoice.takeon_avg_cost,
    mdaily_invoice.order_type,
    mdaily_invoice.order_id
   FROM mdaily_invoice;


--
-- Name: vdaily_job; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW vdaily_job AS
 SELECT mdaily_job.dsb_sk,
    mdaily_job.d_date_sk,
    mdaily_job.prod_ref_sk,
    mdaily_job.dsb_store_sk,
    mdaily_job.brand,
    mdaily_job.model,
    mdaily_job.colour,
    mdaily_job.date_actual,
    mdaily_job.day_name,
    mdaily_job.doy,
    mdaily_job.year,
    mdaily_job.sk,
    mdaily_job.source_id,
    mdaily_job.order_id,
    mdaily_job.order_sk,
    mdaily_job.job_type,
    mdaily_job.product_sk,
    mdaily_job.supplier_sk,
    mdaily_job.queue,
    mdaily_job.status,
    mdaily_job.source_created,
    mdaily_job.source_modified,
    mdaily_job.created
   FROM mdaily_job;


--
-- Name: vdaily_stockmovement; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW vdaily_stockmovement AS
 SELECT mdaily_stockmovement.dsb_sk,
    mdaily_stockmovement.d_date_sk,
    mdaily_stockmovement.prod_ref_sk,
    mdaily_stockmovement.dsb_store_sk,
    mdaily_stockmovement.brand,
    mdaily_stockmovement.model,
    mdaily_stockmovement.colour,
    mdaily_stockmovement.date_actual,
    mdaily_stockmovement.day_name,
    mdaily_stockmovement.doy,
    mdaily_stockmovement.year,
    mdaily_stockmovement.store_name,
    mdaily_stockmovement.productid,
    mdaily_stockmovement.gnm_productid,
    mdaily_stockmovement.invoiceid,
    mdaily_stockmovement.adjustment_type,
    mdaily_stockmovement.sk,
    mdaily_stockmovement.store_sk,
    mdaily_stockmovement.source_id,
    mdaily_stockmovement.takeon_storeproduct_sk,
    mdaily_stockmovement.adjustmentdate,
    mdaily_stockmovement.stock_price_history_sk,
    mdaily_stockmovement.adjustment,
    mdaily_stockmovement.reason,
    mdaily_stockmovement.invoice_sk,
    mdaily_stockmovement.customer_sk,
    mdaily_stockmovement.avg_cost_price,
    mdaily_stockmovement.shipping_ref,
    mdaily_stockmovement.shipping_operator,
    mdaily_stockmovement.comments,
    mdaily_stockmovement.employee_sk,
    mdaily_stockmovement.created,
    mdaily_stockmovement.adjustmentdate_sk,
    mdaily_stockmovement.product_gnm_sk,
    mdaily_stockmovement.adjustment_type_sk,
    mdaily_stockmovement.actual_cost,
    mdaily_stockmovement.actual_cost_value,
    mdaily_stockmovement.moving_quantity,
    mdaily_stockmovement.moving_actual_cost_value,
    mdaily_stockmovement.moving_cost_per_unit
   FROM mdaily_stockmovement;


--
-- Name: view_store_stock_history_gnm; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW view_store_stock_history_gnm AS
 SELECT st.store_sk,
    st.takeon_storeproduct_sk,
    st.product_gnm_sk,
    st.total,
    st.prior_total,
    st.employee_sk,
    st.source_staged,
    st.created,
    st.productid,
    st.type,
    st.name,
    st.description,
    st.frame_model,
    st.frame_colour,
    st.frame_brand,
    s.name AS storename,
    s.chainid,
    s.store_nk
   FROM (( SELECT ssh.store_sk,
            ssh.takeon_storeproduct_sk,
            ssh.product_gnm_sk,
            ssh.total,
            ssh.prior_total,
            ssh.employee_sk,
            ssh.source_staged,
            ssh.created,
            pg.productid,
            pg.type,
            pg.name,
            pg.description,
            pg.frame_model,
            pg.frame_colour,
            pg.frame_brand
           FROM ((store_stock_history_adj ssh
             JOIN ( SELECT ssh_1.nk AS last_nk,
                    ssh_1.store_sk AS last_store_sk,
                    ssh_1.product_gnm_sk AS last_product_gnm_sk,
                    max(ssh_1.source_staged) AS last_source_staged
                   FROM store_stock_history_adj ssh_1
                  GROUP BY ssh_1.nk, ssh_1.store_sk, ssh_1.product_gnm_sk) ls ON (((ls.last_store_sk = ssh.store_sk) AND (ls.last_product_gnm_sk = ssh.product_gnm_sk) AND (ssh.source_staged = ls.last_source_staged))))
             LEFT JOIN product_gnm pg ON ((pg.sk = ssh.product_gnm_sk)))) st
     LEFT JOIN store s ON ((s.sk = st.store_sk)));


--
-- Name: view_store_stock_history_takeon; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW view_store_stock_history_takeon AS
 SELECT st.store_sk,
    st.takeon_storeproduct_sk,
    st.product_gnm_sk,
    st.total,
    st.prior_total,
    st.employee_sk,
    st.source_staged,
    st.created,
    st.productid,
    st.type,
    st.name,
    st.description,
    st.frame_model,
    st.frame_colour,
    st.frame_brand,
    s.name AS storename,
    s.chainid,
    s.store_nk
   FROM (( SELECT ssh.store_sk,
            ssh.takeon_storeproduct_sk,
            ssh.product_gnm_sk,
            ssh.total,
            ssh.prior_total,
            ssh.employee_sk,
            ssh.source_staged,
            ssh.created,
            sp.productid,
            sp.type,
            sp.name,
            sp.description,
            sp.frame_model,
            sp.frame_colour,
            sp.frame_brand
           FROM ((store_stock_history_adj ssh
             JOIN ( SELECT ssh_1.nk AS last_nk,
                    ssh_1.store_sk AS last_store_sk,
                    ssh_1.takeon_storeproduct_sk AS last_takeon_storeproduct_sk,
                    max(ssh_1.source_staged) AS last_source_staged
                   FROM store_stock_history_adj ssh_1
                  GROUP BY ssh_1.nk, ssh_1.store_sk, ssh_1.takeon_storeproduct_sk) ls ON (((ls.last_store_sk = ssh.store_sk) AND (ls.last_takeon_storeproduct_sk = ssh.takeon_storeproduct_sk) AND (ssh.source_staged = ls.last_source_staged))))
             LEFT JOIN storeproduct sp ON ((sp.sk = ssh.takeon_storeproduct_sk)))) st
     LEFT JOIN store s ON ((s.sk = st.store_sk)));


--
-- Name: viewsmbyperiod; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW viewsmbyperiod AS
 SELECT date_part('year'::text, sm.adjustmentdate) AS year,
    date_part('doy'::text, sm.adjustmentdate) AS doy,
    date_part('month'::text, sm.adjustmentdate) AS month,
    date_part('dow'::text, sm.adjustmentdate) AS dow,
    date_part('day'::text, sm.adjustmentdate) AS day,
    sm.sk
   FROM store_stockmovement sm
  GROUP BY (date_part('year'::text, sm.adjustmentdate)), (date_part('doy'::text, sm.adjustmentdate)), (date_part('month'::text, sm.adjustmentdate)), (date_part('dow'::text, sm.adjustmentdate)), (date_part('day'::text, sm.adjustmentdate)), sm.sk;


SET search_path = stage, pg_catalog;

--
-- Name: gnm_sunix_vcode; Type: TABLE; Schema: stage; Owner: -
--

CREATE TABLE gnm_sunix_vcode (
    runid uuid,
    createddate timestamp without time zone DEFAULT now(),
    configname character varying(100),
    pipename character varying(100),
    parameters jsonb,
    data jsonb
);


--
-- Name: gnm_sunix_vframe; Type: TABLE; Schema: stage; Owner: -
--

CREATE TABLE gnm_sunix_vframe (
    runid uuid,
    createddate timestamp without time zone DEFAULT now(),
    configname character varying(100),
    pipename character varying(100),
    parameters jsonb,
    data jsonb
);


--
-- Name: gnm_sunix_vfrstake; Type: TABLE; Schema: stage; Owner: -
--

CREATE TABLE gnm_sunix_vfrstake (
    runid uuid,
    createddate timestamp without time zone DEFAULT now(),
    configname character varying(100),
    pipename character varying(100),
    parameters jsonb,
    data jsonb
);


--
-- Name: gnm_sunix_vrestock; Type: TABLE; Schema: stage; Owner: -
--

CREATE TABLE gnm_sunix_vrestock (
    runid uuid,
    createddate timestamp without time zone DEFAULT now(),
    configname character varying(100),
    pipename character varying(100),
    parameters jsonb,
    data jsonb
);


--
-- Name: job_stage; Type: TABLE; Schema: stage; Owner: -
--

CREATE TABLE job_stage (
    sk integer NOT NULL,
    run_id character varying(100),
    job_id character varying(100),
    order_id character varying(100),
    product_id character varying(100),
    supplier_id character varying(100),
    quantity character varying(100),
    job_type character varying(100),
    queue character varying(100),
    status character varying(100),
    source_created character varying(100),
    source_modified character varying(100),
    created timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: job_stage_sk_seq; Type: SEQUENCE; Schema: stage; Owner: -
--

CREATE SEQUENCE job_stage_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: job_stage_sk_seq; Type: SEQUENCE OWNED BY; Schema: stage; Owner: -
--

ALTER SEQUENCE job_stage_sk_seq OWNED BY job_stage.sk;


--
-- Name: order_stage; Type: TABLE; Schema: stage; Owner: -
--

CREATE TABLE order_stage (
    sk integer NOT NULL,
    run_id character varying(100),
    order_id character varying(100),
    cust_firstname character varying(100),
    cust_lastname character varying(100),
    cust_midname character varying(100),
    cust_phone character varying(100),
    store_id character varying(100),
    cust_sk integer,
    order_type character varying(100),
    queue character varying(100),
    status character varying(100),
    source_created character varying(100),
    source_modified character varying(100),
    created timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: order_stage_sk_seq; Type: SEQUENCE; Schema: stage; Owner: -
--

CREATE SEQUENCE order_stage_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: order_stage_sk_seq; Type: SEQUENCE OWNED BY; Schema: stage; Owner: -
--

ALTER SEQUENCE order_stage_sk_seq OWNED BY order_stage.sk;


--
-- Name: today_gnm_sunix_vcode; Type: VIEW; Schema: stage; Owner: -
--

CREATE VIEW today_gnm_sunix_vcode AS
 SELECT (get_latest_data_by_tablename.data ->> 'COFLAG'::text) AS "COFLAG",
    (get_latest_data_by_tablename.data ->> 'DESCPTN'::text) AS "DESCPTN",
    (get_latest_data_by_tablename.data ->> 'NOTE'::text) AS "NOTE",
    (get_latest_data_by_tablename.data ->> 'PKEY'::text) AS "PKEY",
    (get_latest_data_by_tablename.data ->> 'XFIELD3'::text) AS "XFIELD3",
    (get_latest_data_by_tablename.data ->> 'XFER'::text) AS "XFER",
    (get_latest_data_by_tablename.data ->> 'LOGSTR'::text) AS "LOGSTR",
    (get_latest_data_by_tablename.data ->> 'CODATE2'::text) AS "CODATE2",
    (get_latest_data_by_tablename.data ->> 'ADDR3'::text) AS "ADDR3",
    (get_latest_data_by_tablename.data ->> 'XFIELD2'::text) AS "XFIELD2",
    (get_latest_data_by_tablename.data ->> 'XFIELD1'::text) AS "XFIELD1",
    (get_latest_data_by_tablename.data ->> 'CODELEN'::text) AS "CODELEN",
    (get_latest_data_by_tablename.data ->> 'NUM1'::text) AS "NUM1",
    (get_latest_data_by_tablename.data ->> 'CODE2'::text) AS "CODE2",
    (get_latest_data_by_tablename.data ->> 'AMOUNT22'::text) AS "AMOUNT22",
    (get_latest_data_by_tablename.data ->> 'CODATE'::text) AS "CODATE",
    (get_latest_data_by_tablename.data ->> 'AMOUNT'::text) AS "AMOUNT",
    (get_latest_data_by_tablename.data ->> 'XFIELD4'::text) AS "XFIELD4",
    (get_latest_data_by_tablename.data ->> 'FAMOUNT'::text) AS "FAMOUNT",
    (get_latest_data_by_tablename.data ->> 'ADDR1'::text) AS "ADDR1",
    (get_latest_data_by_tablename.data ->> 'DESCPTN2'::text) AS "DESCPTN2",
    (get_latest_data_by_tablename.data ->> 'EXCOSTPR1'::text) AS "EXCOSTPR1",
    (get_latest_data_by_tablename.data ->> 'CODE'::text) AS "CODE",
    (get_latest_data_by_tablename.data ->> 'POSTCODE'::text) AS "POSTCODE",
    (get_latest_data_by_tablename.data ->> 'COSTPRICE'::text) AS "COSTPRICE",
    (get_latest_data_by_tablename.data ->> 'PHONE'::text) AS "PHONE",
    (get_latest_data_by_tablename.data ->> 'LOCATION'::text) AS "LOCATION",
    (get_latest_data_by_tablename.data ->> 'NOTE1'::text) AS "NOTE1",
    (get_latest_data_by_tablename.data ->> 'AMOUNT1'::text) AS "AMOUNT1",
    (get_latest_data_by_tablename.data ->> 'GROUP'::text) AS "GROUP",
    (get_latest_data_by_tablename.data ->> 'PICT'::text) AS "PICT",
    (get_latest_data_by_tablename.data ->> 'MODKEY'::text) AS "MODKEY",
    (get_latest_data_by_tablename.data ->> 'EXCOSTPR'::text) AS "EXCOSTPR",
    (get_latest_data_by_tablename.data ->> 'AMOUNT2'::text) AS "AMOUNT2",
    (get_latest_data_by_tablename.data ->> 'ITEMNO'::text) AS "ITEMNO",
    (get_latest_data_by_tablename.data ->> 'ITEMNO1'::text) AS "ITEMNO1",
    (get_latest_data_by_tablename.data ->> 'DELFLAG'::text) AS "DELFLAG",
    (get_latest_data_by_tablename.data ->> 'ALIAS'::text) AS "ALIAS",
    (get_latest_data_by_tablename.data ->> 'SELECT'::text) AS "SELECT",
    (get_latest_data_by_tablename.data ->> 'DESCLEN'::text) AS "DESCLEN",
    (get_latest_data_by_tablename.data ->> 'COSTPRICE1'::text) AS "COSTPRICE1",
    (get_latest_data_by_tablename.data ->> 'ADDR2'::text) AS "ADDR2",
    (get_latest_data_by_tablename.parameters ->> 'customer_frame_product_id'::text) AS customer_frame_product_id,
    (get_latest_data_by_tablename.parameters ->> 'store'::text) AS store,
    (get_latest_data_by_tablename.parameters ->> 'source'::text) AS source,
    (get_latest_data_by_tablename.parameters ->> 'view'::text) AS view,
    (get_latest_data_by_tablename.parameters ->> 'gnm_account_code'::text) AS gnm_account_code
   FROM get_latest_data_by_tablename('gnm_sunix_vcode'::bpchar) get_latest_data_by_tablename(data, parameters);


--
-- Name: today_gnm_sunix_vframe; Type: VIEW; Schema: stage; Owner: -
--

CREATE VIEW today_gnm_sunix_vframe AS
 SELECT (get_latest_data_by_tablename.data ->> 'FIRSTPUR'::text) AS "FIRSTPUR",
    (get_latest_data_by_tablename.data ->> 'PHOTONAME'::text) AS "PHOTONAME",
    (get_latest_data_by_tablename.data ->> 'LASTSALE'::text) AS "LASTSALE",
    (get_latest_data_by_tablename.data ->> 'FRAMENUM'::text) AS "FRAMENUM",
    (get_latest_data_by_tablename.data ->> 'WSALEIT'::text) AS "WSALEIT",
    (get_latest_data_by_tablename.data ->> 'REORDDATE'::text) AS "REORDDATE",
    (get_latest_data_by_tablename.data ->> 'STKTAKE'::text) AS "STKTAKE",
    (get_latest_data_by_tablename.data ->> 'LLABORDER'::text) AS "LLABORDER",
    (get_latest_data_by_tablename.data ->> 'LDOWNLOAD'::text) AS "LDOWNLOAD",
    (get_latest_data_by_tablename.data ->> 'EXAVGCOST'::text) AS "EXAVGCOST",
    (get_latest_data_by_tablename.data ->> 'LIFESTYLE'::text) AS "LIFESTYLE",
    (get_latest_data_by_tablename.data ->> 'NOTE'::text) AS "NOTE",
    (get_latest_data_by_tablename.data ->> 'PKEY'::text) AS "PKEY",
    (get_latest_data_by_tablename.data ->> 'TAXPC'::text) AS "TAXPC",
    (get_latest_data_by_tablename.data ->> 'DISPC'::text) AS "DISPC",
    (get_latest_data_by_tablename.data ->> 'FRSTATUS'::text) AS "FRSTATUS",
    (get_latest_data_by_tablename.data ->> 'PHOTO'::text) AS "PHOTO",
    (get_latest_data_by_tablename.data ->> 'SUPPLIER'::text) AS "SUPPLIER",
    (get_latest_data_by_tablename.data ->> 'FGID'::text) AS "FGID",
    (get_latest_data_by_tablename.data ->> 'ISTOCKCODE'::text) AS "ISTOCKCODE",
    (get_latest_data_by_tablename.data ->> 'XFER'::text) AS "XFER",
    (get_latest_data_by_tablename.data ->> 'LOGSTR'::text) AS "LOGSTR",
    (get_latest_data_by_tablename.data ->> 'LISTPRICE'::text) AS "LISTPRICE",
    (get_latest_data_by_tablename.data ->> 'RELEASE'::text) AS "RELEASE",
    (get_latest_data_by_tablename.data ->> 'CHANGE'::text) AS "CHANGE",
    (get_latest_data_by_tablename.data ->> 'DIAG'::text) AS "DIAG",
    (get_latest_data_by_tablename.data ->> 'DQTY'::text) AS "DQTY",
    (get_latest_data_by_tablename.data ->> 'FRDESC'::text) AS "FRDESC",
    (get_latest_data_by_tablename.data ->> 'RETURNBY'::text) AS "RETURNBY",
    (get_latest_data_by_tablename.data ->> 'RRPSRV'::text) AS "RRPSRV",
    (get_latest_data_by_tablename.data ->> 'DPREEXCOST'::text) AS "DPREEXCOST",
    (get_latest_data_by_tablename.data ->> 'EXPREVCOST'::text) AS "EXPREVCOST",
    (get_latest_data_by_tablename.data ->> 'SUPBARCODE'::text) AS "SUPBARCODE",
    (get_latest_data_by_tablename.data ->> 'SXFRAME'::text) AS "SXFRAME",
    (get_latest_data_by_tablename.data ->> 'REFRESH'::text) AS "REFRESH",
    (get_latest_data_by_tablename.data ->> 'REORDQTY'::text) AS "REORDQTY",
    (get_latest_data_by_tablename.data ->> 'QTYONAPPRO'::text) AS "QTYONAPPRO",
    (get_latest_data_by_tablename.data ->> 'TEMPLE'::text) AS "TEMPLE",
    (get_latest_data_by_tablename.data ->> 'PSCREATED'::text) AS "PSCREATED",
    (get_latest_data_by_tablename.data ->> 'FCOLOUR'::text) AS "FCOLOUR",
    (get_latest_data_by_tablename.data ->> 'BASEC'::text) AS "BASEC",
    (get_latest_data_by_tablename.data ->> 'LIFECYCLE'::text) AS "LIFECYCLE",
    (get_latest_data_by_tablename.data ->> 'LASTPUR'::text) AS "LASTPUR",
    (get_latest_data_by_tablename.data ->> 'FRAMENUM0'::text) AS "FRAMENUM0",
    (get_latest_data_by_tablename.data ->> 'ORDERAGAIN'::text) AS "ORDERAGAIN",
    (get_latest_data_by_tablename.data ->> 'FRAMEGROUP'::text) AS "FRAMEGROUP",
    (get_latest_data_by_tablename.data ->> 'QTYAPPRO'::text) AS "QTYAPPRO",
    (get_latest_data_by_tablename.data ->> 'PSUPDATEAT'::text) AS "PSUPDATEAT",
    (get_latest_data_by_tablename.data ->> 'USER'::text) AS "USER",
    (get_latest_data_by_tablename.data ->> 'MINPD'::text) AS "MINPD",
    (get_latest_data_by_tablename.data ->> 'EXLISTPR'::text) AS "EXLISTPR",
    (get_latest_data_by_tablename.data ->> 'DPRECOST'::text) AS "DPRECOST",
    (get_latest_data_by_tablename.data ->> 'MODEL'::text) AS "MODEL",
    (get_latest_data_by_tablename.data ->> 'FROTHER2'::text) AS "FROTHER2",
    (get_latest_data_by_tablename.data ->> 'BARCODE'::text) AS "BARCODE",
    (get_latest_data_by_tablename.data ->> 'PHOTOEXT'::text) AS "PHOTOEXT",
    (get_latest_data_by_tablename.data ->> 'AVAILFROM'::text) AS "AVAILFROM",
    (get_latest_data_by_tablename.data ->> 'DEPTH'::text) AS "DEPTH",
    (get_latest_data_by_tablename.data ->> 'QTY3'::text) AS "QTY3",
    (get_latest_data_by_tablename.data ->> 'LASTSALE2'::text) AS "LASTSALE2",
    (get_latest_data_by_tablename.data ->> 'SPH2'::text) AS "SPH2",
    (get_latest_data_by_tablename.data ->> 'FROTHER'::text) AS "FROTHER",
    (get_latest_data_by_tablename.data ->> 'PSSUPFIT'::text) AS "PSSUPFIT",
    (get_latest_data_by_tablename.data ->> 'SIZE'::text) AS "SIZE",
    (get_latest_data_by_tablename.data ->> 'COSTPRICE'::text) AS "COSTPRICE",
    (get_latest_data_by_tablename.data ->> 'EXRRPSRV'::text) AS "EXRRPSRV",
    (get_latest_data_by_tablename.data ->> 'LOCATION'::text) AS "LOCATION",
    (get_latest_data_by_tablename.data ->> 'LOCATION2'::text) AS "LOCATION2",
    (get_latest_data_by_tablename.data ->> 'CYL2'::text) AS "CYL2",
    (get_latest_data_by_tablename.data ->> 'RRP'::text) AS "RRP",
    (get_latest_data_by_tablename.data ->> 'LISTSRV'::text) AS "LISTSRV",
    (get_latest_data_by_tablename.data ->> 'AVAILTILL'::text) AS "AVAILTILL",
    (get_latest_data_by_tablename.data ->> 'BASECURVE'::text) AS "BASECURVE",
    (get_latest_data_by_tablename.data ->> 'MODKEY'::text) AS "MODKEY",
    (get_latest_data_by_tablename.data ->> 'SUNGRX'::text) AS "SUNGRX",
    (get_latest_data_by_tablename.data ->> 'PVINACTIVE'::text) AS "PVINACTIVE",
    (get_latest_data_by_tablename.data ->> 'EXCOSTPR'::text) AS "EXCOSTPR",
    (get_latest_data_by_tablename.data ->> 'QUANTITY'::text) AS "QUANTITY",
    (get_latest_data_by_tablename.data ->> 'SUPPLIER2'::text) AS "SUPPLIER2",
    (get_latest_data_by_tablename.data ->> 'SRVCHARGE'::text) AS "SRVCHARGE",
    (get_latest_data_by_tablename.data ->> 'QTYONORDER'::text) AS "QTYONORDER",
    (get_latest_data_by_tablename.data ->> 'DELFLAG'::text) AS "DELFLAG",
    (get_latest_data_by_tablename.data ->> 'PROSUPPLY'::text) AS "PROSUPPLY",
    (get_latest_data_by_tablename.data ->> 'SPH1'::text) AS "SPH1",
    (get_latest_data_by_tablename.data ->> 'PKEY0'::text) AS "PKEY0",
    (get_latest_data_by_tablename.data ->> 'AVGCOST'::text) AS "AVGCOST",
    (get_latest_data_by_tablename.data ->> 'MANUFACT'::text) AS "MANUFACT",
    (get_latest_data_by_tablename.data ->> 'FRAMETYPE'::text) AS "FRAMETYPE",
    (get_latest_data_by_tablename.data ->> 'APPORDER'::text) AS "APPORDER",
    (get_latest_data_by_tablename.data ->> 'FRANGE'::text) AS "FRANGE",
    (get_latest_data_by_tablename.data ->> 'WSALEET'::text) AS "WSALEET",
    (get_latest_data_by_tablename.data ->> 'FRSTATUS2'::text) AS "FRSTATUS2",
    (get_latest_data_by_tablename.data ->> 'MODIFIED'::text) AS "MODIFIED",
    (get_latest_data_by_tablename.data ->> 'PROVISION'::text) AS "PROVISION",
    (get_latest_data_by_tablename.data ->> 'REORDER'::text) AS "REORDER",
    (get_latest_data_by_tablename.data ->> 'SUPSTATUS'::text) AS "SUPSTATUS",
    (get_latest_data_by_tablename.data ->> 'LASTINV'::text) AS "LASTINV",
    (get_latest_data_by_tablename.data ->> 'EXLISTSRV'::text) AS "EXLISTSRV",
    (get_latest_data_by_tablename.data ->> 'PREVCOST'::text) AS "PREVCOST",
    (get_latest_data_by_tablename.data ->> 'CYL1'::text) AS "CYL1",
    (get_latest_data_by_tablename.data ->> 'LOCATION3'::text) AS "LOCATION3",
    (get_latest_data_by_tablename.parameters ->> 'customer_frame_product_id'::text) AS customer_frame_product_id,
    (get_latest_data_by_tablename.parameters ->> 'store'::text) AS store,
    (get_latest_data_by_tablename.parameters ->> 'source'::text) AS source,
    (get_latest_data_by_tablename.parameters ->> 'view'::text) AS view,
    (get_latest_data_by_tablename.parameters ->> 'gnm_account_code'::text) AS gnm_account_code
   FROM get_latest_data_by_tablename('gnm_sunix_vframe'::bpchar) get_latest_data_by_tablename(data, parameters);


--
-- Name: today_gnm_sunix_vfrstake; Type: VIEW; Schema: stage; Owner: -
--

CREATE VIEW today_gnm_sunix_vfrstake AS
 SELECT (get_latest_data_by_tablename.data ->> 'FRAMENUM'::text) AS "FRAMENUM",
    (get_latest_data_by_tablename.data ->> 'EXAVGCOST'::text) AS "EXAVGCOST",
    (get_latest_data_by_tablename.data ->> 'SCANLOC'::text) AS "SCANLOC",
    (get_latest_data_by_tablename.data ->> 'ENTRYORDER'::text) AS "ENTRYORDER",
    (get_latest_data_by_tablename.data ->> 'QTY'::text) AS "QTY",
    (get_latest_data_by_tablename.data ->> 'BARCODE'::text) AS "BARCODE",
    (get_latest_data_by_tablename.data ->> 'LOCATION'::text) AS "LOCATION",
    (get_latest_data_by_tablename.data ->> 'RRP'::text) AS "RRP",
    (get_latest_data_by_tablename.data ->> 'DIFF'::text) AS "DIFF",
    (get_latest_data_by_tablename.data ->> 'DESC'::text) AS "DESC",
    (get_latest_data_by_tablename.data ->> 'QTYSET'::text) AS "QTYSET",
    (get_latest_data_by_tablename.data ->> 'AVGCOST'::text) AS "AVGCOST",
    (get_latest_data_by_tablename.data ->> 'QTYINSTK'::text) AS "QTYINSTK",
    (get_latest_data_by_tablename.parameters ->> 'customer_frame_product_id'::text) AS customer_frame_product_id,
    (get_latest_data_by_tablename.parameters ->> 'store'::text) AS store,
    (get_latest_data_by_tablename.parameters ->> 'source'::text) AS source,
    (get_latest_data_by_tablename.parameters ->> 'view'::text) AS view,
    (get_latest_data_by_tablename.parameters ->> 'gnm_account_code'::text) AS gnm_account_code
   FROM get_latest_data_by_tablename('gnm_sunix_vfrstake'::bpchar) get_latest_data_by_tablename(data, parameters);


--
-- Name: today_gnm_sunix_vrestock; Type: VIEW; Schema: stage; Owner: -
--

CREATE VIEW today_gnm_sunix_vrestock AS
 SELECT (get_latest_data_by_tablename.data ->> 'XFERTO'::text) AS "XFERTO",
    (get_latest_data_by_tablename.data ->> 'ID'::text) AS "ID",
    (get_latest_data_by_tablename.data ->> 'CREATED'::text) AS "CREATED",
    (get_latest_data_by_tablename.data ->> 'KEY'::text) AS "KEY",
    (get_latest_data_by_tablename.data ->> 'LOCATION0'::text) AS "LOCATION0",
    (get_latest_data_by_tablename.data ->> 'NOTE'::text) AS "NOTE",
    (get_latest_data_by_tablename.data ->> 'TAXPC'::text) AS "TAXPC",
    (get_latest_data_by_tablename.data ->> 'DISPC'::text) AS "DISPC",
    (get_latest_data_by_tablename.data ->> 'STAFFNAME'::text) AS "STAFFNAME",
    (get_latest_data_by_tablename.data ->> 'SUPPLIER'::text) AS "SUPPLIER",
    (get_latest_data_by_tablename.data ->> 'SALELOC'::text) AS "SALELOC",
    (get_latest_data_by_tablename.data ->> 'XFER'::text) AS "XFER",
    (get_latest_data_by_tablename.data ->> 'LOGSTR'::text) AS "LOGSTR",
    (get_latest_data_by_tablename.data ->> 'LISTPRICE'::text) AS "LISTPRICE",
    (get_latest_data_by_tablename.data ->> 'RETURNBY'::text) AS "RETURNBY",
    (get_latest_data_by_tablename.data ->> 'REFRESH'::text) AS "REFRESH",
    (get_latest_data_by_tablename.data ->> 'MARKER'::text) AS "MARKER",
    (get_latest_data_by_tablename.data ->> 'TYPE'::text) AS "TYPE",
    (get_latest_data_by_tablename.data ->> 'RECTYPE'::text) AS "RECTYPE",
    (get_latest_data_by_tablename.data ->> 'STOCKTYPE'::text) AS "STOCKTYPE",
    (get_latest_data_by_tablename.data ->> 'JOBTYPE'::text) AS "JOBTYPE",
    (get_latest_data_by_tablename.data ->> 'STAFF'::text) AS "STAFF",
    (get_latest_data_by_tablename.data ->> 'XFERFROM'::text) AS "XFERFROM",
    (get_latest_data_by_tablename.data ->> 'QTY_NEW'::text) AS "QTY_NEW",
    (get_latest_data_by_tablename.data ->> 'QTY'::text) AS "QTY",
    (get_latest_data_by_tablename.data ->> 'USER'::text) AS "USER",
    (get_latest_data_by_tablename.data ->> 'EXLISTPR'::text) AS "EXLISTPR",
    (get_latest_data_by_tablename.data ->> 'JOBNUM'::text) AS "JOBNUM",
    (get_latest_data_by_tablename.data ->> 'REFNUM'::text) AS "REFNUM",
    (get_latest_data_by_tablename.data ->> 'FCOST1'::text) AS "FCOST1",
    (get_latest_data_by_tablename.data ->> 'FKEY'::text) AS "FKEY",
    (get_latest_data_by_tablename.data ->> 'CODE'::text) AS "CODE",
    (get_latest_data_by_tablename.data ->> 'BCPRINTED'::text) AS "BCPRINTED",
    (get_latest_data_by_tablename.data ->> 'COSTPRICE'::text) AS "COSTPRICE",
    (get_latest_data_by_tablename.data ->> 'LOCATION'::text) AS "LOCATION",
    (get_latest_data_by_tablename.data ->> 'LOCATION2'::text) AS "LOCATION2",
    (get_latest_data_by_tablename.data ->> 'RRP'::text) AS "RRP",
    (get_latest_data_by_tablename.data ->> 'STOCKCODE'::text) AS "STOCKCODE",
    (get_latest_data_by_tablename.data ->> 'DATE'::text) AS "DATE",
    (get_latest_data_by_tablename.data ->> 'JOBLOC'::text) AS "JOBLOC",
    (get_latest_data_by_tablename.data ->> 'MODUSER'::text) AS "MODUSER",
    (get_latest_data_by_tablename.data ->> 'MODKEY'::text) AS "MODKEY",
    (get_latest_data_by_tablename.data ->> 'INVNO'::text) AS "INVNO",
    (get_latest_data_by_tablename.data ->> 'EXCOSTPR'::text) AS "EXCOSTPR",
    (get_latest_data_by_tablename.data ->> 'REASON'::text) AS "REASON",
    (get_latest_data_by_tablename.data ->> 'NOTE2'::text) AS "NOTE2",
    (get_latest_data_by_tablename.data ->> 'DELFLAG'::text) AS "DELFLAG",
    (get_latest_data_by_tablename.data ->> 'QTY_OLD'::text) AS "QTY_OLD",
    (get_latest_data_by_tablename.data ->> 'MODIFIED'::text) AS "MODIFIED",
    (get_latest_data_by_tablename.data ->> 'CODE1'::text) AS "CODE1",
    (get_latest_data_by_tablename.data ->> 'XFERLOC'::text) AS "XFERLOC",
    (get_latest_data_by_tablename.data ->> 'ENTERED'::text) AS "ENTERED",
    (get_latest_data_by_tablename.parameters ->> 'customer_frame_product_id'::text) AS customer_frame_product_id,
    (get_latest_data_by_tablename.parameters ->> 'store'::text) AS store,
    (get_latest_data_by_tablename.parameters ->> 'source'::text) AS source,
    (get_latest_data_by_tablename.parameters ->> 'view'::text) AS view,
    (get_latest_data_by_tablename.parameters ->> 'gnm_account_code'::text) AS gnm_account_code
   FROM get_latest_data_by_tablename('gnm_sunix_vrestock'::bpchar) get_latest_data_by_tablename(data, parameters);


SET search_path = gnm, pg_catalog;

--
-- Name: address id; Type: DEFAULT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY address ALTER COLUMN id SET DEFAULT nextval('address_id_seq'::regclass);


--
-- Name: brand id; Type: DEFAULT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY brand ALTER COLUMN id SET DEFAULT nextval('brand_id_seq'::regclass);


--
-- Name: brand_tier id; Type: DEFAULT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY brand_tier ALTER COLUMN id SET DEFAULT nextval('brand_tier_id_seq'::regclass);


--
-- Name: image id; Type: DEFAULT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY image ALTER COLUMN id SET DEFAULT nextval('image_id_seq'::regclass);


--
-- Name: job_attribute id; Type: DEFAULT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY job_attribute ALTER COLUMN id SET DEFAULT nextval('job_attribute_id_seq'::regclass);


--
-- Name: login_event id; Type: DEFAULT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY login_event ALTER COLUMN id SET DEFAULT nextval('login_event_id_seq'::regclass);


--
-- Name: model id; Type: DEFAULT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY model ALTER COLUMN id SET DEFAULT nextval('model_id_seq'::regclass);


--
-- Name: model_field id; Type: DEFAULT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY model_field ALTER COLUMN id SET DEFAULT nextval('model_field_id_seq'::regclass);


--
-- Name: optometrist id; Type: DEFAULT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY optometrist ALTER COLUMN id SET DEFAULT nextval('optometrist_id_seq'::regclass);


--
-- Name: order_customer id; Type: DEFAULT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY order_customer ALTER COLUMN id SET DEFAULT nextval('order_customer_id_seq'::regclass);


--
-- Name: order_info id; Type: DEFAULT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY order_info ALTER COLUMN id SET DEFAULT nextval('order_info_id_seq'::regclass);


--
-- Name: order_type_rule id; Type: DEFAULT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY order_type_rule ALTER COLUMN id SET DEFAULT nextval('job_type_lookup_id_seq'::regclass);


--
-- Name: person id; Type: DEFAULT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY person ALTER COLUMN id SET DEFAULT nextval('person_id_seq'::regclass);


--
-- Name: price_policy id; Type: DEFAULT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY price_policy ALTER COLUMN id SET DEFAULT nextval('price_policy_id_seq'::regclass);


--
-- Name: product_alias id; Type: DEFAULT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_alias ALTER COLUMN id SET DEFAULT nextval('product_alias_id_seq'::regclass);


--
-- Name: product_assortment id; Type: DEFAULT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_assortment ALTER COLUMN id SET DEFAULT nextval('product_assortment_id_seq'::regclass);


--
-- Name: product_attribute id; Type: DEFAULT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_attribute ALTER COLUMN id SET DEFAULT nextval('product_attribute_id_seq'::regclass);


--
-- Name: product_coll id; Type: DEFAULT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_coll ALTER COLUMN id SET DEFAULT nextval('product_coll_id_seq'::regclass);


--
-- Name: product_coll_attribute id; Type: DEFAULT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_coll_attribute ALTER COLUMN id SET DEFAULT nextval('product_coll_attribute_id_seq'::regclass);


--
-- Name: product_coll_member id; Type: DEFAULT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_coll_member ALTER COLUMN id SET DEFAULT nextval('product_coll_member_id_seq'::regclass);


--
-- Name: product_coll_tree id; Type: DEFAULT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_coll_tree ALTER COLUMN id SET DEFAULT nextval('product_coll_tree_id_seq'::regclass);


--
-- Name: product_cost_history id; Type: DEFAULT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_cost_history ALTER COLUMN id SET DEFAULT nextval('product_price_history_id_seq'::regclass);


--
-- Name: product_price id; Type: DEFAULT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_price ALTER COLUMN id SET DEFAULT nextval('product_price_id_seq'::regclass);


--
-- Name: product_price_history id; Type: DEFAULT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_price_history ALTER COLUMN id SET DEFAULT nextval('product_price_history_id_seq1'::regclass);


--
-- Name: product_state id; Type: DEFAULT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_state ALTER COLUMN id SET DEFAULT nextval('product_state_id_seq'::regclass);


--
-- Name: product_state_request id; Type: DEFAULT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_state_request ALTER COLUMN id SET DEFAULT nextval('product_state_request_id_seq'::regclass);


--
-- Name: queue_update id; Type: DEFAULT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY queue_update ALTER COLUMN id SET DEFAULT nextval('queue_update_id_seq'::regclass);


--
-- Name: region id; Type: DEFAULT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY region ALTER COLUMN id SET DEFAULT nextval('region_id_seq'::regclass);


--
-- Name: static id; Type: DEFAULT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY static ALTER COLUMN id SET DEFAULT nextval('static_id_seq'::regclass);


--
-- Name: status_update id; Type: DEFAULT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY status_update ALTER COLUMN id SET DEFAULT nextval('status_update_id_seq'::regclass);


--
-- Name: stock id; Type: DEFAULT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY stock ALTER COLUMN id SET DEFAULT nextval('stock_id_seq'::regclass);


--
-- Name: stocktake id; Type: DEFAULT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY stocktake ALTER COLUMN id SET DEFAULT nextval('stocktake_id_seq'::regclass);


--
-- Name: stocktake_item id; Type: DEFAULT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY stocktake_item ALTER COLUMN id SET DEFAULT nextval('stocktake_item_id_seq'::regclass);


--
-- Name: store id; Type: DEFAULT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY store ALTER COLUMN id SET DEFAULT nextval('store_id_seq'::regclass);


--
-- Name: store_attribute id; Type: DEFAULT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY store_attribute ALTER COLUMN id SET DEFAULT nextval('store_attribute_id_seq'::regclass);


--
-- Name: store_product_coll id; Type: DEFAULT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY store_product_coll ALTER COLUMN id SET DEFAULT nextval('store_product_coll_id_seq'::regclass);


--
-- Name: supplier id; Type: DEFAULT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY supplier ALTER COLUMN id SET DEFAULT nextval('supplier_id_seq'::regclass);


--
-- Name: task_attribute id; Type: DEFAULT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY task_attribute ALTER COLUMN id SET DEFAULT nextval('task_attribute_id_seq'::regclass);


--
-- Name: task_log id; Type: DEFAULT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY task_log ALTER COLUMN id SET DEFAULT nextval('task_log_id_seq'::regclass);


--
-- Name: user id; Type: DEFAULT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY "user" ALTER COLUMN id SET DEFAULT nextval('user_id_seq'::regclass);


--
-- Name: user_password_reset id; Type: DEFAULT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY user_password_reset ALTER COLUMN id SET DEFAULT nextval('user_password_reset_id_seq'::regclass);


--
-- Name: user_preference id; Type: DEFAULT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY user_preference ALTER COLUMN id SET DEFAULT nextval('user_preference_id_seq'::regclass);


--
-- Name: user_role id; Type: DEFAULT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY user_role ALTER COLUMN id SET DEFAULT nextval('user_role_id_seq'::regclass);


--
-- Name: user_store id; Type: DEFAULT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY user_store ALTER COLUMN id SET DEFAULT nextval('user_store_id_seq'::regclass);


--
-- Name: user_supplier id; Type: DEFAULT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY user_supplier ALTER COLUMN id SET DEFAULT nextval('user_supplier_id_seq'::regclass);


--
-- Name: zeiss_sphcyl id; Type: DEFAULT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY zeiss_sphcyl ALTER COLUMN id SET DEFAULT nextval('zeiss_sphcyl_id_seq'::regclass);


SET search_path = public, pg_catalog;

--
-- Name: adjustment_type sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY adjustment_type ALTER COLUMN sk SET DEFAULT nextval('adjustment_type_sk_seq'::regclass);


--
-- Name: appointment sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY appointment ALTER COLUMN sk SET DEFAULT nextval('appointment_sk_seq'::regclass);


--
-- Name: appointment_type sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY appointment_type ALTER COLUMN sk SET DEFAULT nextval('appointment_type_sk_seq'::regclass);


--
-- Name: consult sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY consult ALTER COLUMN sk SET DEFAULT nextval('consult_sk_seq'::regclass);


--
-- Name: consult_type sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY consult_type ALTER COLUMN sk SET DEFAULT nextval('consult_type_sk_seq'::regclass);


--
-- Name: consultitem sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY consultitem ALTER COLUMN sk SET DEFAULT nextval('consultitem_sk_seq'::regclass);


--
-- Name: customer sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY customer ALTER COLUMN sk SET DEFAULT nextval('customer_sk_seq'::regclass);


--
-- Name: customer_ext sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY customer_ext ALTER COLUMN sk SET DEFAULT nextval('customer_ext_sk_seq'::regclass);


--
-- Name: d_date sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY d_date ALTER COLUMN sk SET DEFAULT nextval('d_date_sk_seq'::regclass);


--
-- Name: daily_invoice_bucket sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY daily_invoice_bucket ALTER COLUMN sk SET DEFAULT nextval('daily_invoice_bucket_sk_seq'::regclass);


--
-- Name: daily_job_bucket sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY daily_job_bucket ALTER COLUMN sk SET DEFAULT nextval('daily_job_bucket_sk_seq'::regclass);


--
-- Name: daily_matched_bucket sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY daily_matched_bucket ALTER COLUMN sk SET DEFAULT nextval('daily_matched_bucket_sk_seq'::regclass);


--
-- Name: daily_stockmovement_bucket sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY daily_stockmovement_bucket ALTER COLUMN sk SET DEFAULT nextval('daily_stockmovement_bucket_sk_seq'::regclass);


--
-- Name: daily_store_bucket sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY daily_store_bucket ALTER COLUMN sk SET DEFAULT nextval('daily_store_bucket_sk_seq'::regclass);


--
-- Name: discountcode sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY discountcode ALTER COLUMN sk SET DEFAULT nextval('discountcode_sk_seq'::regclass);


--
-- Name: employee sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY employee ALTER COLUMN sk SET DEFAULT nextval('employee_sk_seq'::regclass);


--
-- Name: employee_providerno sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY employee_providerno ALTER COLUMN sk SET DEFAULT nextval('employee_providerno_sk_seq'::regclass);


--
-- Name: expense sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY expense ALTER COLUMN sk SET DEFAULT nextval('expense_sk_seq'::regclass);


--
-- Name: expense_type sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY expense_type ALTER COLUMN sk SET DEFAULT nextval('expense_type_sk_seq'::regclass);


--
-- Name: invoice sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY invoice ALTER COLUMN sk SET DEFAULT nextval('invoice_sk_seq'::regclass);


--
-- Name: invoice_order_job sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY invoice_order_job ALTER COLUMN sk SET DEFAULT nextval('invoice_order_job_sk_seq'::regclass);


--
-- Name: invoice_orders sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY invoice_orders ALTER COLUMN sk SET DEFAULT nextval('invoice_orders_sk_seq'::regclass);


--
-- Name: job_history sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY job_history ALTER COLUMN sk SET DEFAULT nextval('job_history_sk_seq'::regclass);


--
-- Name: job_process_status sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY job_process_status ALTER COLUMN sk SET DEFAULT nextval('job_process_status_sk_seq'::regclass);


--
-- Name: order_history sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY order_history ALTER COLUMN sk SET DEFAULT nextval('order_history_sk_seq'::regclass);


--
-- Name: payee sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY payee ALTER COLUMN sk SET DEFAULT nextval('payee_sk_seq'::regclass);


--
-- Name: payment sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY payment ALTER COLUMN sk SET DEFAULT nextval('payment_sk_seq'::regclass);


--
-- Name: payment_type sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY payment_type ALTER COLUMN sk SET DEFAULT nextval('payment_type_sk_seq'::regclass);


--
-- Name: prod_common sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY prod_common ALTER COLUMN sk SET DEFAULT nextval('prod_common_sk_seq'::regclass);


--
-- Name: prod_ref sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY prod_ref ALTER COLUMN sk SET DEFAULT nextval('prod_ref_sk_seq'::regclass);


--
-- Name: product_gnm sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY product_gnm ALTER COLUMN sk SET DEFAULT nextval('product_gnm_sk_seq'::regclass);


--
-- Name: product_price_history id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY product_price_history ALTER COLUMN id SET DEFAULT nextval('product_price_history_id_seq'::regclass);


--
-- Name: seqtab i; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY seqtab ALTER COLUMN i SET DEFAULT nextval('seqtab_i_seq'::regclass);


--
-- Name: spectaclejob sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY spectaclejob ALTER COLUMN sk SET DEFAULT nextval('spectaclejob_sk_seq'::regclass);


--
-- Name: status sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY status ALTER COLUMN sk SET DEFAULT nextval('status_sk_seq'::regclass);


--
-- Name: stock_price_history sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY stock_price_history ALTER COLUMN sk SET DEFAULT nextval('stock_price_history_sk_seq'::regclass);


--
-- Name: store sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY store ALTER COLUMN sk SET DEFAULT nextval('store_sk_seq'::regclass);


--
-- Name: store_stock_history_adj sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stock_history_adj ALTER COLUMN sk SET DEFAULT nextval('store_stock_history_adj_sk_seq'::regclass);


--
-- Name: store_stockmovement sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stockmovement ALTER COLUMN sk SET DEFAULT nextval('store_stockmovement_sk_seq'::regclass);


--
-- Name: storeproduct sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY storeproduct ALTER COLUMN sk SET DEFAULT nextval('storeproduct_sk_seq'::regclass);


--
-- Name: takeon_product sk; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY takeon_product ALTER COLUMN sk SET DEFAULT nextval('takeon_product_sk_seq'::regclass);


SET search_path = stage, pg_catalog;

--
-- Name: job_stage sk; Type: DEFAULT; Schema: stage; Owner: -
--

ALTER TABLE ONLY job_stage ALTER COLUMN sk SET DEFAULT nextval('job_stage_sk_seq'::regclass);


--
-- Name: order_stage sk; Type: DEFAULT; Schema: stage; Owner: -
--

ALTER TABLE ONLY order_stage ALTER COLUMN sk SET DEFAULT nextval('order_stage_sk_seq'::regclass);


SET search_path = gnm, pg_catalog;

--
-- Name: address address_pkey; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY address
    ADD CONSTRAINT address_pkey PRIMARY KEY (id);


--
-- Name: job_attribute job_attribute_pkey; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY job_attribute
    ADD CONSTRAINT job_attribute_pkey PRIMARY KEY (id);


--
-- Name: job job_pkey; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY job
    ADD CONSTRAINT job_pkey PRIMARY KEY (id);


--
-- Name: order_customer order_customer_pkey; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY order_customer
    ADD CONSTRAINT order_customer_pkey PRIMARY KEY (id);


--
-- Name: order order_pkey; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY "order"
    ADD CONSTRAINT order_pkey PRIMARY KEY (id);


--
-- Name: person person_pkey; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY person
    ADD CONSTRAINT person_pkey PRIMARY KEY (id);


--
-- Name: brand pk_brand; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY brand
    ADD CONSTRAINT pk_brand PRIMARY KEY (id);


--
-- Name: brand_tier pk_brand_tier; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY brand_tier
    ADD CONSTRAINT pk_brand_tier PRIMARY KEY (id);


--
-- Name: coll_filter pk_coll_filter; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY coll_filter
    ADD CONSTRAINT pk_coll_filter PRIMARY KEY (name);


--
-- Name: coll_type pk_coll_type; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY coll_type
    ADD CONSTRAINT pk_coll_type PRIMARY KEY (name);


--
-- Name: image pk_image; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY image
    ADD CONSTRAINT pk_image PRIMARY KEY (id);


--
-- Name: job_type pk_job_type; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY job_type
    ADD CONSTRAINT pk_job_type PRIMARY KEY (id);


--
-- Name: login_event pk_login_event; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY login_event
    ADD CONSTRAINT pk_login_event PRIMARY KEY (id);


--
-- Name: model pk_model; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY model
    ADD CONSTRAINT pk_model PRIMARY KEY (id);


--
-- Name: model_field pk_model_field; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY model_field
    ADD CONSTRAINT pk_model_field PRIMARY KEY (id);


--
-- Name: optometrist pk_optometrist; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY optometrist
    ADD CONSTRAINT pk_optometrist PRIMARY KEY (id);


--
-- Name: order_info pk_order_info; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY order_info
    ADD CONSTRAINT pk_order_info PRIMARY KEY (id);


--
-- Name: order_type pk_order_type; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY order_type
    ADD CONSTRAINT pk_order_type PRIMARY KEY (id);


--
-- Name: price_policy pk_price_policy; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY price_policy
    ADD CONSTRAINT pk_price_policy PRIMARY KEY (id);


--
-- Name: product_coll pk_product_coll; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_coll
    ADD CONSTRAINT pk_product_coll PRIMARY KEY (id);


--
-- Name: product_coll_attribute pk_product_coll_attribute; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_coll_attribute
    ADD CONSTRAINT pk_product_coll_attribute PRIMARY KEY (id);


--
-- Name: product_coll_filter pk_product_coll_filter; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_coll_filter
    ADD CONSTRAINT pk_product_coll_filter PRIMARY KEY (id);


--
-- Name: product_coll_member pk_product_coll_member; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_coll_member
    ADD CONSTRAINT pk_product_coll_member PRIMARY KEY (id);


--
-- Name: product_coll_tree pk_product_coll_tree; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_coll_tree
    ADD CONSTRAINT pk_product_coll_tree PRIMARY KEY (id);


--
-- Name: product_contact_lens pk_product_contact_lens; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_contact_lens
    ADD CONSTRAINT pk_product_contact_lens PRIMARY KEY (id);


--
-- Name: product_cost_history pk_product_cost_history; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_cost_history
    ADD CONSTRAINT pk_product_cost_history PRIMARY KEY (id);


--
-- Name: product_frame pk_product_frame; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_frame
    ADD CONSTRAINT pk_product_frame PRIMARY KEY (id);


--
-- Name: product_lens pk_product_lens; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_lens
    ADD CONSTRAINT pk_product_lens PRIMARY KEY (id);


--
-- Name: product_price pk_product_price; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_price
    ADD CONSTRAINT pk_product_price PRIMARY KEY (id);


--
-- Name: product_price_history pk_product_price_history; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_price_history
    ADD CONSTRAINT pk_product_price_history PRIMARY KEY (id);


--
-- Name: product_state pk_product_state; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_state
    ADD CONSTRAINT pk_product_state PRIMARY KEY (id);


--
-- Name: product_state_request pk_product_state_request; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_state_request
    ADD CONSTRAINT pk_product_state_request PRIMARY KEY (id);


--
-- Name: product_state_request_state_type pk_product_state_request_state_type; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_state_request_state_type
    ADD CONSTRAINT pk_product_state_request_state_type PRIMARY KEY (name);


--
-- Name: product_state_type pk_product_state_type; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_state_type
    ADD CONSTRAINT pk_product_state_type PRIMARY KEY (name);


--
-- Name: product_sunglasses pk_product_sunglasses; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_sunglasses
    ADD CONSTRAINT pk_product_sunglasses PRIMARY KEY (id);


--
-- Name: role pk_role; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY role
    ADD CONSTRAINT pk_role PRIMARY KEY (id);


--
-- Name: stocktake pk_stocktake; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY stocktake
    ADD CONSTRAINT pk_stocktake PRIMARY KEY (id);


--
-- Name: stocktake_item pk_stocktake_item; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY stocktake_item
    ADD CONSTRAINT pk_stocktake_item PRIMARY KEY (id);


--
-- Name: store_attribute pk_store_attribute; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY store_attribute
    ADD CONSTRAINT pk_store_attribute PRIMARY KEY (id);


--
-- Name: store_optometrist pk_store_optometrist; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY store_optometrist
    ADD CONSTRAINT pk_store_optometrist PRIMARY KEY (optometrist_id, store_id);


--
-- Name: store_product_coll pk_store_product_coll; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY store_product_coll
    ADD CONSTRAINT pk_store_product_coll PRIMARY KEY (id);


--
-- Name: task pk_task; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY task
    ADD CONSTRAINT pk_task PRIMARY KEY (id);


--
-- Name: task_attribute pk_task_attribute; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY task_attribute
    ADD CONSTRAINT pk_task_attribute PRIMARY KEY (id);


--
-- Name: task_log pk_task_log; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY task_log
    ADD CONSTRAINT pk_task_log PRIMARY KEY (id);


--
-- Name: uploaded_file pk_uploaded_file; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY uploaded_file
    ADD CONSTRAINT pk_uploaded_file PRIMARY KEY (id);


--
-- Name: user pk_user; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT pk_user PRIMARY KEY (id);


--
-- Name: user_password_reset pk_user_password_reset; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY user_password_reset
    ADD CONSTRAINT pk_user_password_reset PRIMARY KEY (id);


--
-- Name: user_preference pk_user_preference; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY user_preference
    ADD CONSTRAINT pk_user_preference PRIMARY KEY (id);


--
-- Name: user_role pk_user_role; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY user_role
    ADD CONSTRAINT pk_user_role PRIMARY KEY (id);


--
-- Name: user_store pk_user_store; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY user_store
    ADD CONSTRAINT pk_user_store PRIMARY KEY (id);


--
-- Name: user_supplier pk_user_suppler; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY user_supplier
    ADD CONSTRAINT pk_user_suppler PRIMARY KEY (id);


--
-- Name: zeiss_retextra pk_zeiss_retextra; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY zeiss_retextra
    ADD CONSTRAINT pk_zeiss_retextra PRIMARY KEY (stock);


--
-- Name: zeiss_tlr pk_zeiss_tlr; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY zeiss_tlr
    ADD CONSTRAINT pk_zeiss_tlr PRIMARY KEY (keyrange);


--
-- Name: product_alias product_alias_pkey; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_alias
    ADD CONSTRAINT product_alias_pkey PRIMARY KEY (id);


--
-- Name: product_assortment product_assortment_pkey; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_assortment
    ADD CONSTRAINT product_assortment_pkey PRIMARY KEY (id);


--
-- Name: product_attribute product_attribute_pkey; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_attribute
    ADD CONSTRAINT product_attribute_pkey PRIMARY KEY (id);


--
-- Name: product product_pkey; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product
    ADD CONSTRAINT product_pkey PRIMARY KEY (id);


--
-- Name: queue_update queue_update_pkey; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY queue_update
    ADD CONSTRAINT queue_update_pkey PRIMARY KEY (id);


--
-- Name: region region_pkey; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY region
    ADD CONSTRAINT region_pkey PRIMARY KEY (id);


--
-- Name: revinfo revinfo_pkey; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY revinfo
    ADD CONSTRAINT revinfo_pkey PRIMARY KEY (rev);


--
-- Name: schema_version schema_version_pk; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY schema_version
    ADD CONSTRAINT schema_version_pk PRIMARY KEY (version);


--
-- Name: static static_pkey; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY static
    ADD CONSTRAINT static_pkey PRIMARY KEY (id);


--
-- Name: status_update status_update_pkey; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY status_update
    ADD CONSTRAINT status_update_pkey PRIMARY KEY (id);


--
-- Name: stock_aud stock_aud_pkey; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY stock_aud
    ADD CONSTRAINT stock_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: stock stock_pkey; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY stock
    ADD CONSTRAINT stock_pkey PRIMARY KEY (id);


--
-- Name: store_manager store_manager_pkey; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY store_manager
    ADD CONSTRAINT store_manager_pkey PRIMARY KEY (store_id, person_id);


--
-- Name: store store_pkey; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY store
    ADD CONSTRAINT store_pkey PRIMARY KEY (id);


--
-- Name: supplier supplier_pkey; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY supplier
    ADD CONSTRAINT supplier_pkey PRIMARY KEY (id);


--
-- Name: brand uk_brand__supplier_id_name; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY brand
    ADD CONSTRAINT uk_brand__supplier_id_name UNIQUE (supplier_id, name);


--
-- Name: brand_tier uk_brand_tier__supplier_id_name; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY brand_tier
    ADD CONSTRAINT uk_brand_tier__supplier_id_name UNIQUE (supplier_id, name);


--
-- Name: job uk_job_job_id; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY job
    ADD CONSTRAINT uk_job_job_id UNIQUE (job_id);


--
-- Name: order_type_rule uk_job_type_lookup; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY order_type_rule
    ADD CONSTRAINT uk_job_type_lookup UNIQUE (order_type_id, product_type, job_type_id);


--
-- Name: model_field uk_model_field__model_id_model_field_code; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY model_field
    ADD CONSTRAINT uk_model_field__model_id_model_field_code UNIQUE (model_id, model_field_code);


--
-- Name: model_field uk_model_field__model_id_sequ; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY model_field
    ADD CONSTRAINT uk_model_field__model_id_sequ UNIQUE (model_id, sequ);


--
-- Name: model uk_model_model_code; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY model
    ADD CONSTRAINT uk_model_model_code UNIQUE (model_code);


--
-- Name: optometrist uk_optometrist__person_id; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY optometrist
    ADD CONSTRAINT uk_optometrist__person_id UNIQUE (person_id);


--
-- Name: order uk_order__order_id; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY "order"
    ADD CONSTRAINT uk_order__order_id UNIQUE (order_id);


--
-- Name: order_info uk_order_info__order_id_name; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY order_info
    ADD CONSTRAINT uk_order_info__order_id_name UNIQUE (order_id, name);


--
-- Name: price_policy uk_price_policy__brand_id_active_id; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY price_policy
    ADD CONSTRAINT uk_price_policy__brand_id_active_id UNIQUE (brand_id, active_id);


--
-- Name: price_policy uk_price_policy__brand_tier_id_active_id; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY price_policy
    ADD CONSTRAINT uk_price_policy__brand_tier_id_active_id UNIQUE (brand_tier_id, active_id);


--
-- Name: price_policy uk_price_policy__product_id_active_id; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY price_policy
    ADD CONSTRAINT uk_price_policy__product_id_active_id UNIQUE (product_id, active_id);


--
-- Name: product uk_product__supplier_id_supplier_sku; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product
    ADD CONSTRAINT uk_product__supplier_id_supplier_sku UNIQUE (supplier_id, supplier_sku);


--
-- Name: product_alias uk_product_alias__alias_prefix_id; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_alias
    ADD CONSTRAINT uk_product_alias__alias_prefix_id UNIQUE (alias_prefix, alias_id);


--
-- Name: product_assortment_product uk_product_assortment_product__assortment_id_product_id; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_assortment_product
    ADD CONSTRAINT uk_product_assortment_product__assortment_id_product_id UNIQUE (assortment_id, product_id);


--
-- Name: product_coll uk_product_coll__name; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_coll
    ADD CONSTRAINT uk_product_coll__name UNIQUE (name);


--
-- Name: product_coll_attribute uk_product_coll_attribute__product_coll_id_name; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_coll_attribute
    ADD CONSTRAINT uk_product_coll_attribute__product_coll_id_name UNIQUE (product_coll_id, name);


--
-- Name: product_coll_member uk_product_coll_member__product_coll_id_product_id; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_coll_member
    ADD CONSTRAINT uk_product_coll_member__product_coll_id_product_id UNIQUE (product_coll_id, product_id);


--
-- Name: product_coll_tree uk_product_coll_tree__parent_id_child_id; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_coll_tree
    ADD CONSTRAINT uk_product_coll_tree__parent_id_child_id UNIQUE (parent_id, child_id);


--
-- Name: product_price uk_product_price__product_coll_id_product_id; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_price
    ADD CONSTRAINT uk_product_price__product_coll_id_product_id UNIQUE (product_coll_id, product_id);


--
-- Name: product_state_request uk_product_state_request__product_coll_id_product_id; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_state_request
    ADD CONSTRAINT uk_product_state_request__product_coll_id_product_id UNIQUE (product_coll_id, product_id);


--
-- Name: role uk_role__name; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY role
    ADD CONSTRAINT uk_role__name UNIQUE (name);


--
-- Name: store uk_store__store_id; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY store
    ADD CONSTRAINT uk_store__store_id UNIQUE (store_id);


--
-- Name: store_attribute uk_store_attribute__store_id_name; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY store_attribute
    ADD CONSTRAINT uk_store_attribute__store_id_name UNIQUE (store_id, name);


--
-- Name: store_product_coll uk_store_product_coll__store_id_product_coll_id; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY store_product_coll
    ADD CONSTRAINT uk_store_product_coll__store_id_product_coll_id UNIQUE (store_id, product_coll_id);


--
-- Name: supplier uk_supplier__supplier_id; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY supplier
    ADD CONSTRAINT uk_supplier__supplier_id UNIQUE (supplier_id);


--
-- Name: supplier_address uk_supplier_address; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY supplier_address
    ADD CONSTRAINT uk_supplier_address UNIQUE (supplier_id, address_id);


--
-- Name: user uk_user__email_address; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT uk_user__email_address UNIQUE (email_address);


--
-- Name: user_password_reset uk_user_password_reset__token; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY user_password_reset
    ADD CONSTRAINT uk_user_password_reset__token UNIQUE (token);


--
-- Name: user_preference uk_user_preference__user_id_key; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY user_preference
    ADD CONSTRAINT uk_user_preference__user_id_key UNIQUE (user_id, key);


--
-- Name: zeiss_sphcyl uk_zeiss_sphcyl__keyrange_sphere; Type: CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY zeiss_sphcyl
    ADD CONSTRAINT uk_zeiss_sphcyl__keyrange_sphere UNIQUE (keyrange, sphere);


SET search_path = public, pg_catalog;

--
-- Name: adjustment_type adjustment_type_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY adjustment_type
    ADD CONSTRAINT adjustment_type_pkey PRIMARY KEY (sk);


--
-- Name: appointment appointment_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY appointment
    ADD CONSTRAINT appointment_pkey PRIMARY KEY (sk);


--
-- Name: appointment_type appointment_type_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY appointment_type
    ADD CONSTRAINT appointment_type_pkey PRIMARY KEY (sk);


--
-- Name: consult_type constult_type_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY consult_type
    ADD CONSTRAINT constult_type_pkey PRIMARY KEY (sk);


--
-- Name: consult consult_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY consult
    ADD CONSTRAINT consult_pkey PRIMARY KEY (sk);


--
-- Name: consultitem consultitem_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY consultitem
    ADD CONSTRAINT consultitem_pkey PRIMARY KEY (sk);


--
-- Name: customer_ext customer_ext_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY customer_ext
    ADD CONSTRAINT customer_ext_pkey PRIMARY KEY (sk);


--
-- Name: customer customer_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY customer
    ADD CONSTRAINT customer_pkey PRIMARY KEY (sk);


--
-- Name: daily_invoice_bucket daily_invoice_bucket_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY daily_invoice_bucket
    ADD CONSTRAINT daily_invoice_bucket_pkey PRIMARY KEY (sk);


--
-- Name: daily_job_bucket daily_job_bucket_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY daily_job_bucket
    ADD CONSTRAINT daily_job_bucket_pkey PRIMARY KEY (sk);


--
-- Name: daily_stockmovement_bucket daily_stockmovement_bucket_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY daily_stockmovement_bucket
    ADD CONSTRAINT daily_stockmovement_bucket_pkey PRIMARY KEY (sk);


--
-- Name: daily_store_bucket daily_store_bucket_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY daily_store_bucket
    ADD CONSTRAINT daily_store_bucket_pkey PRIMARY KEY (sk);


--
-- Name: expense expense_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY expense
    ADD CONSTRAINT expense_pkey PRIMARY KEY (sk);


--
-- Name: expense_type expense_type_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY expense_type
    ADD CONSTRAINT expense_type_pkey PRIMARY KEY (sk);


--
-- Name: supplier externalref_ukey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY supplier
    ADD CONSTRAINT externalref_ukey UNIQUE (externalref);


--
-- Name: invoice_order_job invoice_order_job_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY invoice_order_job
    ADD CONSTRAINT invoice_order_job_pkey PRIMARY KEY (sk);


--
-- Name: invoice_orders invoice_orders_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY invoice_orders
    ADD CONSTRAINT invoice_orders_pkey PRIMARY KEY (sk);


--
-- Name: invoice invoice_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY invoice
    ADD CONSTRAINT invoice_pkey PRIMARY KEY (sk);


--
-- Name: invoiceitem invoiceitem_skey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY invoiceitem
    ADD CONSTRAINT invoiceitem_skey PRIMARY KEY (invoiceitemid);


--
-- Name: job_history job_history_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY job_history
    ADD CONSTRAINT job_history_pkey PRIMARY KEY (sk);


--
-- Name: order_history order_history_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY order_history
    ADD CONSTRAINT order_history_pkey PRIMARY KEY (sk);


--
-- Name: payee payee_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY payee
    ADD CONSTRAINT payee_pkey PRIMARY KEY (sk);


--
-- Name: payment payment_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY payment
    ADD CONSTRAINT payment_pkey PRIMARY KEY (sk);


--
-- Name: payment_type payment_type_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY payment_type
    ADD CONSTRAINT payment_type_pkey PRIMARY KEY (sk);


--
-- Name: paymentitem paymentitem_skey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY paymentitem
    ADD CONSTRAINT paymentitem_skey PRIMARY KEY (paymentitemid);


--
-- Name: d_date pk_d_date; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY d_date
    ADD CONSTRAINT pk_d_date PRIMARY KEY (sk);


--
-- Name: employee pk_employee; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY employee
    ADD CONSTRAINT pk_employee PRIMARY KEY (sk);


--
-- Name: prod_common prod_common_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY prod_common
    ADD CONSTRAINT prod_common_pkey PRIMARY KEY (sk);


--
-- Name: prod_ref prod_ref_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY prod_ref
    ADD CONSTRAINT prod_ref_pkey PRIMARY KEY (sk);


--
-- Name: product_gnm product_gnm_primary_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY product_gnm
    ADD CONSTRAINT product_gnm_primary_key PRIMARY KEY (sk);


--
-- Name: schema_version schema_version_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY schema_version
    ADD CONSTRAINT schema_version_pk PRIMARY KEY (installed_rank);


--
-- Name: seqtab seqtab_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY seqtab
    ADD CONSTRAINT seqtab_pkey PRIMARY KEY (i);


--
-- Name: spectaclejob spectaclejob_skey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY spectaclejob
    ADD CONSTRAINT spectaclejob_skey PRIMARY KEY (spectaclejobid);


--
-- Name: status status_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY status
    ADD CONSTRAINT status_pkey PRIMARY KEY (sk);


--
-- Name: stock_price_history stock_price_history_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY stock_price_history
    ADD CONSTRAINT stock_price_history_pkey PRIMARY KEY (sk);


--
-- Name: store store_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store
    ADD CONSTRAINT store_pkey PRIMARY KEY (sk);


--
-- Name: store_stock_history store_stock_history_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stock_history
    ADD CONSTRAINT store_stock_history_pkey PRIMARY KEY (sk);


--
-- Name: store_stockmovement store_stockmovement_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stockmovement
    ADD CONSTRAINT store_stockmovement_pkey PRIMARY KEY (sk);


--
-- Name: storeproduct storeproduct_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY storeproduct
    ADD CONSTRAINT storeproduct_pkey PRIMARY KEY (sk);


--
-- Name: supplier supplier_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY supplier
    ADD CONSTRAINT supplier_pkey PRIMARY KEY (sk);


--
-- Name: supplierproduct supplierproduct_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY supplierproduct
    ADD CONSTRAINT supplierproduct_pkey PRIMARY KEY (sk);


--
-- Name: takeon_product takeon_product_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY takeon_product
    ADD CONSTRAINT takeon_product_pkey PRIMARY KEY (sk);


SET search_path = stage, pg_catalog;

--
-- Name: job_stage job_stage_pkey; Type: CONSTRAINT; Schema: stage; Owner: -
--

ALTER TABLE ONLY job_stage
    ADD CONSTRAINT job_stage_pkey PRIMARY KEY (sk);


--
-- Name: order_stage order_stage_pkey; Type: CONSTRAINT; Schema: stage; Owner: -
--

ALTER TABLE ONLY order_stage
    ADD CONSTRAINT order_stage_pkey PRIMARY KEY (sk);


SET search_path = gnm, pg_catalog;

--
-- Name: fki_stocktake__store_id -> store; Type: INDEX; Schema: gnm; Owner: -
--

CREATE INDEX "fki_stocktake__store_id -> store" ON stocktake USING btree (store_id);


--
-- Name: fki_stocktake_item__stocktake_id; Type: INDEX; Schema: gnm; Owner: -
--

CREATE INDEX fki_stocktake_item__stocktake_id ON stocktake_item USING btree (stocktake_id);


--
-- Name: idx_price_policy__supplier_id_active_id; Type: INDEX; Schema: gnm; Owner: -
--

CREATE UNIQUE INDEX idx_price_policy__supplier_id_active_id ON price_policy USING btree ((
CASE ((brand_tier_id IS NOT NULL) OR (brand_id IS NOT NULL) OR (product_id IS NOT NULL))
    WHEN true THEN NULL::bigint
    ELSE supplier_id
END), active_id);


--
-- Name: idx_product_coll_member__product_coll_id_product_id; Type: INDEX; Schema: gnm; Owner: -
--

CREATE UNIQUE INDEX idx_product_coll_member__product_coll_id_product_id ON product_coll_member USING btree (product_coll_id, product_id);


--
-- Name: idx_product_price__product_coll_id_product_id; Type: INDEX; Schema: gnm; Owner: -
--

CREATE UNIQUE INDEX idx_product_price__product_coll_id_product_id ON product_price USING btree (product_coll_id, product_id);


--
-- Name: idx_task__status_scheduled_time; Type: INDEX; Schema: gnm; Owner: -
--

CREATE INDEX idx_task__status_scheduled_time ON task USING btree (status, scheduled_time);


--
-- Name: product_image_idx; Type: INDEX; Schema: gnm; Owner: -
--

CREATE INDEX product_image_idx ON product USING btree (image_id);


--
-- Name: schema_version_ir_idx; Type: INDEX; Schema: gnm; Owner: -
--

CREATE INDEX schema_version_ir_idx ON schema_version USING btree (installed_rank);


--
-- Name: schema_version_s_idx; Type: INDEX; Schema: gnm; Owner: -
--

CREATE INDEX schema_version_s_idx ON schema_version USING btree (success);


--
-- Name: schema_version_vr_idx; Type: INDEX; Schema: gnm; Owner: -
--

CREATE INDEX schema_version_vr_idx ON schema_version USING btree (version_rank);


SET search_path = public, pg_catalog;

--
-- Name: appointment_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX appointment_nkey ON appointment USING btree (appointmentid);


--
-- Name: appointment_storesk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX appointment_storesk ON appointment USING hash (storesk);


--
-- Name: appointment_type_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX appointment_type_nkey ON appointment_type USING btree (appointment_typeid);


--
-- Name: consult_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX consult_nkey ON consult USING btree (consultid);


--
-- Name: consult_type_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX consult_type_nkey ON consult_type USING btree (consult_typeid);


--
-- Name: customer_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX customer_nkey ON customer USING btree (customerid);


--
-- Name: employee_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX employee_nkey ON employee USING btree (employeeid);


--
-- Name: expense_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX expense_nkey ON expense USING btree (expenseid);


--
-- Name: expense_type_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX expense_type_nkey ON expense_type USING btree (expense_typeid);


--
-- Name: fki_supplierproduct_supplier_fkey; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX fki_supplierproduct_supplier_fkey ON supplierproduct USING btree (suppliersk);


--
-- Name: idx_adjustment_type_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX idx_adjustment_type_id ON adjustment_type USING btree (adjustment_typeid);


--
-- Name: idx_adjustment_type_name; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_adjustment_type_name ON adjustment_type USING btree (name);


--
-- Name: idx_d_date_dateactual; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_d_date_dateactual ON d_date USING btree (date_actual);


--
-- Name: idx_d_date_day_of_year; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_d_date_day_of_year ON d_date USING btree (day_of_year);


--
-- Name: idx_d_date_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX idx_d_date_id ON d_date USING btree (date_dim_id);


--
-- Name: idx_d_date_year_calendar; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_d_date_year_calendar ON d_date USING btree (year_calendar);


--
-- Name: idx_invoice_sk_store_stockmovement; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_invoice_sk_store_stockmovement ON store_stockmovement USING btree (invoice_sk);


--
-- Name: idx_invoiceitemid_invoiceite; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_invoiceitemid_invoiceite ON invoiceitem USING btree (invoicesk, invoiceitemid);


--
-- Name: idx_invoiceitemwithdate_createddate; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_invoiceitemwithdate_createddate ON invoiceitem USING btree (createddate);


--
-- Name: idx_invoiceitemwithdate_customer_sk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_invoiceitemwithdate_customer_sk ON invoiceitem USING btree (customersk);


--
-- Name: idx_invoiceitemwithdate_employee_sk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_invoiceitemwithdate_employee_sk ON invoiceitem USING btree (employeesk);


--
-- Name: idx_invoiceitemwithdate_invoice_sk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_invoiceitemwithdate_invoice_sk ON invoiceitem USING btree (invoicesk);


--
-- Name: idx_invoiceitemwithdate_payee_sk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_invoiceitemwithdate_payee_sk ON invoiceitem USING btree (payeesk);


--
-- Name: idx_invoiceitemwithdate_product_sk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_invoiceitemwithdate_product_sk ON invoiceitem USING btree (productsk);


--
-- Name: idx_invoiceitemwithdate_serviceemployee_sk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_invoiceitemwithdate_serviceemployee_sk ON invoiceitem USING btree (serviceemployeesk);


--
-- Name: idx_invoiceitemwithdate_store_sk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_invoiceitemwithdate_store_sk ON invoiceitem USING btree (storesk);


--
-- Name: idx_model; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_model ON supplierproduct USING btree (frame_model);


--
-- Name: idx_order_history_sourceid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_order_history_sourceid ON order_history USING btree (source_id);


--
-- Name: idx_orderid_order_history; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_orderid_order_history ON order_history USING btree (source_id);


--
-- Name: idx_stock_price_history_gnmproduct_date; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_stock_price_history_gnmproduct_date ON stock_price_history USING btree (product_gnm_sk, created DESC);


--
-- Name: idx_stock_price_history_source_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX idx_stock_price_history_source_id ON stock_price_history USING btree (source_id);


--
-- Name: idx_stock_price_history_takeon_product_date; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_stock_price_history_takeon_product_date ON stock_price_history USING btree (takeon_storeproduct_sk, created DESC);


--
-- Name: idx_store_nk; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX idx_store_nk ON store USING btree (store_nk);


--
-- Name: idx_store_stock_history_storeproduct_date; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_store_stock_history_storeproduct_date ON store_stock_history USING btree (product_gnm_sk, created DESC);


--
-- Name: idx_store_stock_history_takeon_product_date; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_store_stock_history_takeon_product_date ON store_stock_history USING btree (takeon_storeproduct_sk, created DESC);


--
-- Name: idx_store_stockmovement_adjustmentdate; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_store_stockmovement_adjustmentdate ON store_stockmovement USING btree (product_gnm_sk, adjustmentdate);


--
-- Name: idx_store_stockmovement_adjustmentdate2; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_store_stockmovement_adjustmentdate2 ON store_stockmovement USING btree (takeon_storeproduct_sk, adjustmentdate);


--
-- Name: idx_store_stockmovement_adjustmentdate_sk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_store_stockmovement_adjustmentdate_sk ON store_stockmovement USING btree (adjustmentdate_sk);


--
-- Name: idx_store_stockmovement_adjustmenttype_sk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_store_stockmovement_adjustmenttype_sk ON store_stockmovement USING btree (adjustment_type_sk);


--
-- Name: idx_store_stockmovement_invoice_sk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_store_stockmovement_invoice_sk ON store_stockmovement USING btree (invoice_sk);


--
-- Name: idx_store_stockmovement_source_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX idx_store_stockmovement_source_id ON store_stockmovement USING btree (source_id);


--
-- Name: idx_store_stockmovement_store_sk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_store_stockmovement_store_sk ON store_stockmovement USING btree (store_sk);


--
-- Name: idx_store_stockmovement_takeon_storeproduct_sk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_store_stockmovement_takeon_storeproduct_sk ON store_stockmovement USING btree (takeon_storeproduct_sk);


--
-- Name: idx_suppliersku; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_suppliersku ON supplierproduct USING btree (suppliersku);


--
-- Name: index_daily_store_bucket_product; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_daily_store_bucket_product ON daily_store_bucket USING btree (store_sk, prod_ref_sk);


--
-- Name: invoice_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX invoice_nkey ON invoice USING btree (invoiceid);


--
-- Name: invoice_storesk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX invoice_storesk ON invoice USING hash (storesk);


--
-- Name: invoiceitem_createddate_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX invoiceitem_createddate_idx ON invoiceitem_old USING btree (createddate);


--
-- Name: invoiceitem_storesk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX invoiceitem_storesk ON invoiceitem USING hash (storesk);


--
-- Name: payee_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX payee_nkey ON payee USING btree (payeeid);


--
-- Name: payment_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX payment_nkey ON payment USING btree (paymentid);


--
-- Name: payment_storeid_ix; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX payment_storeid_ix ON payment USING btree (storeid);


--
-- Name: payment_type_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX payment_type_nkey ON payment_type USING btree (payment_typeid);


--
-- Name: paymentitem_storesk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX paymentitem_storesk ON paymentitem USING hash (storesk);


--
-- Name: productid_on_product; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX productid_on_product ON storeproduct USING btree (productid);


--
-- Name: productid_on_product_gnm; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX productid_on_product_gnm ON product_gnm USING btree (productid);


--
-- Name: schema_version_s_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX schema_version_s_idx ON schema_version USING btree (success);


--
-- Name: status_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX status_nkey ON status USING btree (statusid);


--
-- Name: store_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX store_nkey ON store USING btree (storeid);


--
-- Name: storeproduct_nkey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX storeproduct_nkey ON storeproduct USING btree (productid);


--
-- Name: unique_index_daily_invoice_bucket; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX unique_index_daily_invoice_bucket ON daily_invoice_bucket USING btree (daily_store_bucket_sk, invoice_sk, invoiceitemid);


--
-- Name: unique_index_daily_job_bucket; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX unique_index_daily_job_bucket ON daily_job_bucket USING btree (daily_store_bucket_sk, job_sk);


--
-- Name: unique_index_daily_stockmovement_bucket; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX unique_index_daily_stockmovement_bucket ON daily_stockmovement_bucket USING btree (daily_store_bucket_sk, store_stockmovement_sk);


--
-- Name: unique_index_daily_store_bucket; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX unique_index_daily_store_bucket ON daily_store_bucket USING btree (d_date_sk, store_sk, prod_ref_sk);


--
-- Name: unique_index_prod_common; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX unique_index_prod_common ON prod_common USING btree (prod_ref_sk, product_gnm_sk, storeproduct_sk);


--
-- Name: unique_index_prod_common1; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX unique_index_prod_common1 ON prod_common USING btree (product_gnm_sk);


--
-- Name: unique_index_prod_common2; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX unique_index_prod_common2 ON prod_common USING btree (storeproduct_sk);


--
-- Name: unique_index_prod_ref; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX unique_index_prod_ref ON prod_ref USING btree (brand, model, colour);


SET search_path = gnm, pg_catalog;

--
-- Name: brand brand_tier_propagate; Type: TRIGGER; Schema: gnm; Owner: -
--

CREATE TRIGGER brand_tier_propagate AFTER UPDATE ON brand FOR EACH ROW EXECUTE PROCEDURE brand_tier_propagate();


--
-- Name: product_price calculate_product_retail_prices; Type: TRIGGER; Schema: gnm; Owner: -
--

CREATE TRIGGER calculate_product_retail_prices BEFORE INSERT OR UPDATE ON product_price FOR EACH ROW EXECUTE PROCEDURE calculate_product_retail_prices();


--
-- Name: product calculate_supplier_product_prices; Type: TRIGGER; Schema: gnm; Owner: -
--

CREATE TRIGGER calculate_supplier_product_prices BEFORE INSERT OR UPDATE ON product FOR EACH ROW EXECUTE PROCEDURE calculate_supplier_product_prices();


--
-- Name: price_policy price_policy_propagate; Type: TRIGGER; Schema: gnm; Owner: -
--

CREATE TRIGGER price_policy_propagate AFTER INSERT OR DELETE OR UPDATE ON price_policy FOR EACH ROW EXECUTE PROCEDURE price_policy_propagate();


--
-- Name: product_coll_member product_coll_member_delete; Type: TRIGGER; Schema: gnm; Owner: -
--

CREATE TRIGGER product_coll_member_delete AFTER DELETE ON product_coll_member FOR EACH ROW EXECUTE PROCEDURE product_coll_member_delete();


--
-- Name: product_coll_member product_coll_member_insert; Type: TRIGGER; Schema: gnm; Owner: -
--

CREATE TRIGGER product_coll_member_insert AFTER INSERT ON product_coll_member FOR EACH ROW EXECUTE PROCEDURE product_coll_member_insert();


--
-- Name: product_coll_tree product_coll_tree_delete; Type: TRIGGER; Schema: gnm; Owner: -
--

CREATE TRIGGER product_coll_tree_delete AFTER DELETE ON product_coll_tree FOR EACH ROW EXECUTE PROCEDURE product_coll_tree_delete();


--
-- Name: product_coll_tree product_coll_tree_insert; Type: TRIGGER; Schema: gnm; Owner: -
--

CREATE TRIGGER product_coll_tree_insert AFTER INSERT ON product_coll_tree FOR EACH ROW EXECUTE PROCEDURE product_coll_tree_insert();


--
-- Name: product_coll product_coll_update; Type: TRIGGER; Schema: gnm; Owner: -
--

CREATE TRIGGER product_coll_update AFTER UPDATE ON product_coll FOR EACH ROW EXECUTE PROCEDURE product_coll_update();


--
-- Name: product_price product_price_delete; Type: TRIGGER; Schema: gnm; Owner: -
--

CREATE TRIGGER product_price_delete AFTER DELETE ON product_price FOR EACH ROW EXECUTE PROCEDURE product_price_delete();


--
-- Name: product_price product_price_insert; Type: TRIGGER; Schema: gnm; Owner: -
--

CREATE TRIGGER product_price_insert AFTER INSERT ON product_price FOR EACH ROW EXECUTE PROCEDURE product_price_insert();


--
-- Name: product_state_request product_state_request_delete; Type: TRIGGER; Schema: gnm; Owner: -
--

CREATE TRIGGER product_state_request_delete AFTER DELETE ON product_state_request FOR EACH ROW EXECUTE PROCEDURE product_state_request_delete();


--
-- Name: product_state_request product_state_request_insert; Type: TRIGGER; Schema: gnm; Owner: -
--

CREATE TRIGGER product_state_request_insert AFTER INSERT ON product_state_request FOR EACH ROW EXECUTE PROCEDURE product_state_request_insert();


--
-- Name: product save_product_cost_history; Type: TRIGGER; Schema: gnm; Owner: -
--

CREATE TRIGGER save_product_cost_history AFTER INSERT OR UPDATE ON product FOR EACH ROW EXECUTE PROCEDURE save_product_cost_history();


--
-- Name: product_price save_product_price_history; Type: TRIGGER; Schema: gnm; Owner: -
--

CREATE TRIGGER save_product_price_history AFTER INSERT OR UPDATE ON product_price FOR EACH ROW EXECUTE PROCEDURE save_product_price_history();


--
-- Name: product_coll_tree selector_assigned_to_range; Type: TRIGGER; Schema: gnm; Owner: -
--

CREATE TRIGGER selector_assigned_to_range AFTER INSERT OR UPDATE ON product_coll_tree FOR EACH ROW EXECUTE PROCEDURE selector_assigned_to_range();


--
-- Name: product_coll_filter selector_updated; Type: TRIGGER; Schema: gnm; Owner: -
--

CREATE TRIGGER selector_updated AFTER UPDATE ON product_coll_filter FOR EACH ROW EXECUTE PROCEDURE selector_updated();


--
-- Name: product supplier_product_prices_propagate; Type: TRIGGER; Schema: gnm; Owner: -
--

CREATE TRIGGER supplier_product_prices_propagate AFTER INSERT OR UPDATE ON product FOR EACH ROW EXECUTE PROCEDURE supplier_product_prices_propagate();


--
-- Name: supplier_address fk_address__address_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY supplier_address
    ADD CONSTRAINT fk_address__address_id FOREIGN KEY (address_id) REFERENCES address(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: brand fk_brand__brand_tier_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY brand
    ADD CONSTRAINT fk_brand__brand_tier_id FOREIGN KEY (brand_tier_id) REFERENCES brand_tier(id) ON DELETE SET NULL;


--
-- Name: brand fk_brand__supplier_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY brand
    ADD CONSTRAINT fk_brand__supplier_id FOREIGN KEY (supplier_id) REFERENCES supplier(id);


--
-- Name: brand_tier fk_brand_tier__supplier_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY brand_tier
    ADD CONSTRAINT fk_brand_tier__supplier_id FOREIGN KEY (supplier_id) REFERENCES supplier(id);


--
-- Name: job fk_job__job_type_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY job
    ADD CONSTRAINT fk_job__job_type_id FOREIGN KEY (job_type_id) REFERENCES job_type(id) ON UPDATE CASCADE;


--
-- Name: job fk_job__order_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY job
    ADD CONSTRAINT fk_job__order_id FOREIGN KEY (order_id) REFERENCES "order"(id);


--
-- Name: job fk_job__product_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY job
    ADD CONSTRAINT fk_job__product_id FOREIGN KEY (product_id) REFERENCES product(id);


--
-- Name: job fk_job__supplier_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY job
    ADD CONSTRAINT fk_job__supplier_id FOREIGN KEY (supplier_id) REFERENCES supplier(id);


--
-- Name: job_attribute fk_job_attribute__job_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY job_attribute
    ADD CONSTRAINT fk_job_attribute__job_id FOREIGN KEY (job_id) REFERENCES job(id) ON UPDATE CASCADE;


--
-- Name: job_queue fk_job_queue__job_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY job_queue
    ADD CONSTRAINT fk_job_queue__job_id FOREIGN KEY (job_id) REFERENCES job(id) ON UPDATE CASCADE;


--
-- Name: job_status fk_job_status__job_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY job_status
    ADD CONSTRAINT fk_job_status__job_id FOREIGN KEY (job_id) REFERENCES job(id) ON UPDATE CASCADE;


--
-- Name: job_status fk_job_status__status_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY job_status
    ADD CONSTRAINT fk_job_status__status_id FOREIGN KEY (status_id) REFERENCES status_update(id);


--
-- Name: order_type_rule fk_job_type_lookup__job_type_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY order_type_rule
    ADD CONSTRAINT fk_job_type_lookup__job_type_id FOREIGN KEY (job_type_id) REFERENCES job_type(id) ON UPDATE CASCADE;


--
-- Name: order_type_rule fk_job_type_lookup__order_type_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY order_type_rule
    ADD CONSTRAINT fk_job_type_lookup__order_type_id FOREIGN KEY (order_type_id) REFERENCES order_type(id) ON UPDATE CASCADE;


--
-- Name: login_event fk_login_event__user_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY login_event
    ADD CONSTRAINT fk_login_event__user_id FOREIGN KEY (user_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: model_field fk_model_field__model_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY model_field
    ADD CONSTRAINT fk_model_field__model_id FOREIGN KEY (model_id) REFERENCES model(id) ON DELETE CASCADE;


--
-- Name: model fk_model_parent_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY model
    ADD CONSTRAINT fk_model_parent_id FOREIGN KEY (parent_id) REFERENCES model(id) ON DELETE RESTRICT;


--
-- Name: optometrist fk_optometrist__person_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY optometrist
    ADD CONSTRAINT fk_optometrist__person_id FOREIGN KEY (person_id) REFERENCES person(id) ON DELETE CASCADE;


--
-- Name: order fk_order__customer_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY "order"
    ADD CONSTRAINT fk_order__customer_id FOREIGN KEY (customer_id) REFERENCES order_customer(id);


--
-- Name: order fk_order__order_type_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY "order"
    ADD CONSTRAINT fk_order__order_type_id FOREIGN KEY (order_type_id) REFERENCES order_type(id) ON UPDATE CASCADE;


--
-- Name: order fk_order__store_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY "order"
    ADD CONSTRAINT fk_order__store_id FOREIGN KEY (store_id) REFERENCES store(id);


--
-- Name: order_info fk_order_info__order_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY order_info
    ADD CONSTRAINT fk_order_info__order_id FOREIGN KEY (order_id) REFERENCES "order"(id) ON DELETE CASCADE;


--
-- Name: order_queue fk_order_queue__order_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY order_queue
    ADD CONSTRAINT fk_order_queue__order_id FOREIGN KEY (order_id) REFERENCES "order"(id);


--
-- Name: order_queue fk_order_queue__queue_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY order_queue
    ADD CONSTRAINT fk_order_queue__queue_id FOREIGN KEY (queue_id) REFERENCES queue_update(id);


--
-- Name: job_queue fk_order_queue__queue_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY job_queue
    ADD CONSTRAINT fk_order_queue__queue_id FOREIGN KEY (queue_id) REFERENCES queue_update(id);


--
-- Name: order_status fk_order_status__order_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY order_status
    ADD CONSTRAINT fk_order_status__order_id FOREIGN KEY (order_id) REFERENCES "order"(id);


--
-- Name: order_status fk_order_status__status_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY order_status
    ADD CONSTRAINT fk_order_status__status_id FOREIGN KEY (status_id) REFERENCES status_update(id);


--
-- Name: person_certifications fk_person_certifications__person_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY person_certifications
    ADD CONSTRAINT fk_person_certifications__person_id FOREIGN KEY (person_id) REFERENCES person(id);


--
-- Name: price_policy fk_price_policy__brand_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY price_policy
    ADD CONSTRAINT fk_price_policy__brand_id FOREIGN KEY (brand_tier_id) REFERENCES brand(id);


--
-- Name: price_policy fk_price_policy__brand_tier_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY price_policy
    ADD CONSTRAINT fk_price_policy__brand_tier_id FOREIGN KEY (brand_tier_id) REFERENCES brand_tier(id);


--
-- Name: price_policy fk_price_policy__product_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY price_policy
    ADD CONSTRAINT fk_price_policy__product_id FOREIGN KEY (product_id) REFERENCES product(id);


--
-- Name: price_policy fk_price_policy__supplier_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY price_policy
    ADD CONSTRAINT fk_price_policy__supplier_id FOREIGN KEY (supplier_id) REFERENCES supplier(id);


--
-- Name: product fk_product__brand_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product
    ADD CONSTRAINT fk_product__brand_id FOREIGN KEY (brand_id) REFERENCES brand(id);


--
-- Name: product fk_product__current_state_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product
    ADD CONSTRAINT fk_product__current_state_id FOREIGN KEY (current_state_id) REFERENCES product_state(id);


--
-- Name: product fk_product__image_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product
    ADD CONSTRAINT fk_product__image_id FOREIGN KEY (image_id) REFERENCES image(id) ON DELETE SET NULL;


--
-- Name: product fk_product__pending_state_request_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product
    ADD CONSTRAINT fk_product__pending_state_request_id FOREIGN KEY (pending_state_request_id) REFERENCES product_state_request(id);


--
-- Name: product fk_product__price_policy_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product
    ADD CONSTRAINT fk_product__price_policy_id FOREIGN KEY (price_policy_id) REFERENCES price_policy(id);


--
-- Name: product fk_product__supplier_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product
    ADD CONSTRAINT fk_product__supplier_id FOREIGN KEY (supplier_id) REFERENCES supplier(id) ON DELETE CASCADE;


--
-- Name: product_alias fk_product_alias__product_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_alias
    ADD CONSTRAINT fk_product_alias__product_id FOREIGN KEY (product_id) REFERENCES product(id) ON DELETE CASCADE;


--
-- Name: product_assortment_product fk_product_assortment_product__assortment_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_assortment_product
    ADD CONSTRAINT fk_product_assortment_product__assortment_id FOREIGN KEY (assortment_id) REFERENCES product_assortment(id);


--
-- Name: product_assortment_product fk_product_assortment_product__product_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_assortment_product
    ADD CONSTRAINT fk_product_assortment_product__product_id FOREIGN KEY (product_id) REFERENCES product(id);


--
-- Name: product_assortment_store fk_product_assortment_product__store_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_assortment_store
    ADD CONSTRAINT fk_product_assortment_product__store_id FOREIGN KEY (store_id) REFERENCES store(id);


--
-- Name: product_assortment_store fk_product_assortment_store__assortment_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_assortment_store
    ADD CONSTRAINT fk_product_assortment_store__assortment_id FOREIGN KEY (assortment_id) REFERENCES product_assortment(id);


--
-- Name: product_attribute fk_product_attribute__product_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_attribute
    ADD CONSTRAINT fk_product_attribute__product_id FOREIGN KEY (product_id) REFERENCES product(id) ON DELETE CASCADE;


--
-- Name: product_coll fk_product_coll__coll_type; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_coll
    ADD CONSTRAINT fk_product_coll__coll_type FOREIGN KEY (coll_type) REFERENCES coll_type(name) ON UPDATE CASCADE;


--
-- Name: product_coll_attribute fk_product_coll_attribute__product_coll_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_coll_attribute
    ADD CONSTRAINT fk_product_coll_attribute__product_coll_id FOREIGN KEY (product_coll_id) REFERENCES product_coll(id) ON DELETE CASCADE;


--
-- Name: product_coll_filter fk_product_coll_filter__coll_filter; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_coll_filter
    ADD CONSTRAINT fk_product_coll_filter__coll_filter FOREIGN KEY (coll_filter) REFERENCES coll_filter(name);


--
-- Name: product_coll_filter fk_product_coll_filter__id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_coll_filter
    ADD CONSTRAINT fk_product_coll_filter__id FOREIGN KEY (id) REFERENCES product_coll(id);


--
-- Name: product_coll_member fk_product_coll_member__product_coll_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_coll_member
    ADD CONSTRAINT fk_product_coll_member__product_coll_id FOREIGN KEY (product_coll_id) REFERENCES product_coll(id);


--
-- Name: product_coll_member fk_product_coll_member__product_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_coll_member
    ADD CONSTRAINT fk_product_coll_member__product_id FOREIGN KEY (product_id) REFERENCES product(id);


--
-- Name: product_coll_tree fk_product_coll_tree__child_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_coll_tree
    ADD CONSTRAINT fk_product_coll_tree__child_id FOREIGN KEY (child_id) REFERENCES product_coll(id);


--
-- Name: product_coll_tree fk_product_coll_tree__parent_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_coll_tree
    ADD CONSTRAINT fk_product_coll_tree__parent_id FOREIGN KEY (parent_id) REFERENCES product_coll(id);


--
-- Name: product_contact_lens fk_product_contact_lens__id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_contact_lens
    ADD CONSTRAINT fk_product_contact_lens__id FOREIGN KEY (id) REFERENCES product(id) ON DELETE CASCADE;


--
-- Name: product_cost_history fk_product_cost_history__price_policy_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_cost_history
    ADD CONSTRAINT fk_product_cost_history__price_policy_id FOREIGN KEY (price_policy_id) REFERENCES price_policy(id);


--
-- Name: product_cost_history fk_product_cost_history__product_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_cost_history
    ADD CONSTRAINT fk_product_cost_history__product_id FOREIGN KEY (product_id) REFERENCES product(id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: product_frame fk_product_frame__id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_frame
    ADD CONSTRAINT fk_product_frame__id FOREIGN KEY (id) REFERENCES product(id) ON DELETE CASCADE;


--
-- Name: product_lens fk_product_lens__product_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_lens
    ADD CONSTRAINT fk_product_lens__product_id FOREIGN KEY (id) REFERENCES product(id) ON DELETE CASCADE;


--
-- Name: product_price fk_product_price__product_coll_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_price
    ADD CONSTRAINT fk_product_price__product_coll_id FOREIGN KEY (product_coll_id) REFERENCES product_coll(id);


--
-- Name: product_price fk_product_price__product_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_price
    ADD CONSTRAINT fk_product_price__product_id FOREIGN KEY (product_id) REFERENCES product(id);


--
-- Name: product_price_history fk_product_price_history__product_coll_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_price_history
    ADD CONSTRAINT fk_product_price_history__product_coll_id FOREIGN KEY (product_coll_id) REFERENCES product_coll(id);


--
-- Name: product_price_history fk_product_price_history__product_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_price_history
    ADD CONSTRAINT fk_product_price_history__product_id FOREIGN KEY (product_id) REFERENCES product(id);


--
-- Name: product_state fk_product_state__product_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_state
    ADD CONSTRAINT fk_product_state__product_id FOREIGN KEY (product_id) REFERENCES product(id);


--
-- Name: product_state fk_product_state__reason_request_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_state
    ADD CONSTRAINT fk_product_state__reason_request_id FOREIGN KEY (reason_request_id) REFERENCES product_state_request(id);


--
-- Name: product_state fk_product_state__state_type; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_state
    ADD CONSTRAINT fk_product_state__state_type FOREIGN KEY (state_type) REFERENCES product_state_type(name) ON UPDATE CASCADE;


--
-- Name: product_state_request fk_product_state_request__from_state_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_state_request
    ADD CONSTRAINT fk_product_state_request__from_state_id FOREIGN KEY (from_state_id) REFERENCES product_state(id);


--
-- Name: product_state_request fk_product_state_request__product_coll_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_state_request
    ADD CONSTRAINT fk_product_state_request__product_coll_id FOREIGN KEY (product_coll_id) REFERENCES product_coll(id);


--
-- Name: product_state_request fk_product_state_request__product_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_state_request
    ADD CONSTRAINT fk_product_state_request__product_id FOREIGN KEY (product_id) REFERENCES product(id);


--
-- Name: product_state_request fk_product_state_request__request_state_type; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_state_request
    ADD CONSTRAINT fk_product_state_request__request_state_type FOREIGN KEY (request_state_type) REFERENCES product_state_request_state_type(name);


--
-- Name: product_state_request fk_product_state_request__to_state_type; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_state_request
    ADD CONSTRAINT fk_product_state_request__to_state_type FOREIGN KEY (to_state_type) REFERENCES product_state_type(name);


--
-- Name: product_sunglasses fk_product_sunglasses__id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY product_sunglasses
    ADD CONSTRAINT fk_product_sunglasses__id FOREIGN KEY (id) REFERENCES product(id) ON DELETE CASCADE;


--
-- Name: region fk_region__manager_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY region
    ADD CONSTRAINT fk_region__manager_id FOREIGN KEY (manager_id) REFERENCES person(id);


--
-- Name: stock fk_stock__product_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY stock
    ADD CONSTRAINT fk_stock__product_id FOREIGN KEY (product_id) REFERENCES product(id);


--
-- Name: stock_aud fk_stock_aud__rev; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY stock_aud
    ADD CONSTRAINT fk_stock_aud__rev FOREIGN KEY (rev) REFERENCES revinfo(rev);


--
-- Name: stocktake fk_stocktake__store_id -> store; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY stocktake
    ADD CONSTRAINT "fk_stocktake__store_id -> store" FOREIGN KEY (store_id) REFERENCES store(id);


--
-- Name: stocktake_item fk_stocktake_item__stocktake_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY stocktake_item
    ADD CONSTRAINT fk_stocktake_item__stocktake_id FOREIGN KEY (stocktake_id) REFERENCES stocktake(id);


--
-- Name: store fk_store__region_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY store
    ADD CONSTRAINT fk_store__region_id FOREIGN KEY (region_id) REFERENCES region(id);


--
-- Name: store_attribute fk_store_attribute__store_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY store_attribute
    ADD CONSTRAINT fk_store_attribute__store_id FOREIGN KEY (store_id) REFERENCES store(id) ON DELETE CASCADE;


--
-- Name: store_hours fk_store_hours__store_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY store_hours
    ADD CONSTRAINT fk_store_hours__store_id FOREIGN KEY (store_id) REFERENCES store(id);


--
-- Name: store_manager fk_store_manager__person_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY store_manager
    ADD CONSTRAINT fk_store_manager__person_id FOREIGN KEY (person_id) REFERENCES person(id);


--
-- Name: store_manager fk_store_manager__store_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY store_manager
    ADD CONSTRAINT fk_store_manager__store_id FOREIGN KEY (store_id) REFERENCES store(id);


--
-- Name: store_optometrist fk_store_optometrist__optometrist_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY store_optometrist
    ADD CONSTRAINT fk_store_optometrist__optometrist_id FOREIGN KEY (optometrist_id) REFERENCES optometrist(id) ON DELETE CASCADE;


--
-- Name: store_optometrist fk_store_optometrist__store_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY store_optometrist
    ADD CONSTRAINT fk_store_optometrist__store_id FOREIGN KEY (store_id) REFERENCES store(id);


--
-- Name: store_product_coll fk_store_product_coll__product_coll_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY store_product_coll
    ADD CONSTRAINT fk_store_product_coll__product_coll_id FOREIGN KEY (product_coll_id) REFERENCES product_coll(id);


--
-- Name: store_product_coll fk_store_product_coll__store_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY store_product_coll
    ADD CONSTRAINT fk_store_product_coll__store_id FOREIGN KEY (store_id) REFERENCES store(id);


--
-- Name: store_services fk_store_services__store_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY store_services
    ADD CONSTRAINT fk_store_services__store_id FOREIGN KEY (store_id) REFERENCES store(id);


--
-- Name: supplier fk_supplier__contact_person_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY supplier
    ADD CONSTRAINT fk_supplier__contact_person_id FOREIGN KEY (contact_person_id) REFERENCES person(id);


--
-- Name: supplier_address fk_supplier__supplier_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY supplier_address
    ADD CONSTRAINT fk_supplier__supplier_id FOREIGN KEY (supplier_id) REFERENCES supplier(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: task fk_task__user_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY task
    ADD CONSTRAINT fk_task__user_id FOREIGN KEY (user_id) REFERENCES "user"(id) ON DELETE CASCADE;


--
-- Name: task_attribute fk_task_attribute__task_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY task_attribute
    ADD CONSTRAINT fk_task_attribute__task_id FOREIGN KEY (task_id) REFERENCES task(id) ON DELETE CASCADE;


--
-- Name: task_log fk_task_log__task_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY task_log
    ADD CONSTRAINT fk_task_log__task_id FOREIGN KEY (task_id) REFERENCES task(id) ON DELETE CASCADE;


--
-- Name: uploaded_file fk_uploaded_file__task_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY uploaded_file
    ADD CONSTRAINT fk_uploaded_file__task_id FOREIGN KEY (task_id) REFERENCES task(id) ON DELETE SET NULL;


--
-- Name: uploaded_file fk_uploaded_file__user_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY uploaded_file
    ADD CONSTRAINT fk_uploaded_file__user_id FOREIGN KEY (user_id) REFERENCES "user"(id) ON DELETE CASCADE;


--
-- Name: user fk_user__person_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT fk_user__person_id FOREIGN KEY (person_id) REFERENCES person(id) ON DELETE CASCADE;


--
-- Name: user_preference fk_user_preference__user_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY user_preference
    ADD CONSTRAINT fk_user_preference__user_id FOREIGN KEY (user_id) REFERENCES "user"(id) ON DELETE CASCADE;


--
-- Name: user_role fk_user_role__role_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY user_role
    ADD CONSTRAINT fk_user_role__role_id FOREIGN KEY (role_id) REFERENCES role(id) ON DELETE CASCADE;


--
-- Name: user_role fk_user_role__user_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY user_role
    ADD CONSTRAINT fk_user_role__user_id FOREIGN KEY (user_id) REFERENCES "user"(id) ON DELETE CASCADE;


--
-- Name: user_store fk_user_store__store_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY user_store
    ADD CONSTRAINT fk_user_store__store_id FOREIGN KEY (store_id) REFERENCES store(id) ON DELETE CASCADE;


--
-- Name: user_store fk_user_store__user_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY user_store
    ADD CONSTRAINT fk_user_store__user_id FOREIGN KEY (user_id) REFERENCES "user"(id) ON DELETE CASCADE;


--
-- Name: user_supplier fk_user_suppler__supplier_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY user_supplier
    ADD CONSTRAINT fk_user_suppler__supplier_id FOREIGN KEY (supplier_id) REFERENCES supplier(id) ON DELETE CASCADE;


--
-- Name: user_supplier fk_user_suppler__user_id; Type: FK CONSTRAINT; Schema: gnm; Owner: -
--

ALTER TABLE ONLY user_supplier
    ADD CONSTRAINT fk_user_suppler__user_id FOREIGN KEY (user_id) REFERENCES "user"(id) ON DELETE CASCADE;


SET search_path = public, pg_catalog;

--
-- Name: daily_invoice_bucket daily_invoice_bucket_daily_store_bucket_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY daily_invoice_bucket
    ADD CONSTRAINT daily_invoice_bucket_daily_store_bucket_sk_fkey FOREIGN KEY (daily_store_bucket_sk) REFERENCES daily_store_bucket(sk);


--
-- Name: daily_invoice_bucket daily_invoice_bucket_invoice_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY daily_invoice_bucket
    ADD CONSTRAINT daily_invoice_bucket_invoice_sk_fkey FOREIGN KEY (invoice_sk) REFERENCES invoice(sk);


--
-- Name: daily_job_bucket daily_job_bucket_daily_store_bucket_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY daily_job_bucket
    ADD CONSTRAINT daily_job_bucket_daily_store_bucket_sk_fkey FOREIGN KEY (daily_store_bucket_sk) REFERENCES daily_store_bucket(sk);


--
-- Name: daily_job_bucket daily_job_bucket_job_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY daily_job_bucket
    ADD CONSTRAINT daily_job_bucket_job_sk_fkey FOREIGN KEY (job_sk) REFERENCES job_history(sk);


--
-- Name: daily_matched_bucket daily_matched_bucket_invoice_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY daily_matched_bucket
    ADD CONSTRAINT daily_matched_bucket_invoice_sk_fkey FOREIGN KEY (invoice_sk) REFERENCES invoice(sk);


--
-- Name: daily_matched_bucket daily_matched_bucket_job_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY daily_matched_bucket
    ADD CONSTRAINT daily_matched_bucket_job_sk_fkey FOREIGN KEY (job_sk) REFERENCES job_history(sk);


--
-- Name: daily_matched_bucket daily_matched_bucket_store_stockmovement_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY daily_matched_bucket
    ADD CONSTRAINT daily_matched_bucket_store_stockmovement_sk_fkey FOREIGN KEY (store_stockmovement_sk) REFERENCES store_stockmovement(sk);


--
-- Name: daily_stockmovement_bucket daily_stockmovement_bucket_daily_store_bucket_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY daily_stockmovement_bucket
    ADD CONSTRAINT daily_stockmovement_bucket_daily_store_bucket_sk_fkey FOREIGN KEY (daily_store_bucket_sk) REFERENCES daily_store_bucket(sk);


--
-- Name: daily_stockmovement_bucket daily_stockmovement_bucket_store_stockmovement_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY daily_stockmovement_bucket
    ADD CONSTRAINT daily_stockmovement_bucket_store_stockmovement_sk_fkey FOREIGN KEY (store_stockmovement_sk) REFERENCES store_stockmovement(sk);


--
-- Name: daily_store_bucket daily_store_bucket_d_date_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY daily_store_bucket
    ADD CONSTRAINT daily_store_bucket_d_date_sk_fkey FOREIGN KEY (d_date_sk) REFERENCES d_date(sk);


--
-- Name: daily_store_bucket daily_store_bucket_prod_ref_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY daily_store_bucket
    ADD CONSTRAINT daily_store_bucket_prod_ref_sk_fkey FOREIGN KEY (prod_ref_sk) REFERENCES prod_ref(sk);


--
-- Name: daily_store_bucket daily_store_bucket_store_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY daily_store_bucket
    ADD CONSTRAINT daily_store_bucket_store_sk_fkey FOREIGN KEY (store_sk) REFERENCES store(sk);


--
-- Name: invoice_order_job invoice_order_job_invoice_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY invoice_order_job
    ADD CONSTRAINT invoice_order_job_invoice_sk_fkey FOREIGN KEY (invoice_sk) REFERENCES invoice(sk);


--
-- Name: invoice_order_job invoice_order_job_store_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY invoice_order_job
    ADD CONSTRAINT invoice_order_job_store_sk_fkey FOREIGN KEY (store_sk) REFERENCES store(sk);


--
-- Name: invoice_orders invoice_orders_invoice_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY invoice_orders
    ADD CONSTRAINT invoice_orders_invoice_sk_fkey FOREIGN KEY (invoice_sk) REFERENCES invoice(sk);


--
-- Name: invoice_orders invoice_orders_order_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY invoice_orders
    ADD CONSTRAINT invoice_orders_order_sk_fkey FOREIGN KEY (order_sk) REFERENCES order_history(sk);


--
-- Name: invoice_orders invoice_orders_store_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY invoice_orders
    ADD CONSTRAINT invoice_orders_store_sk_fkey FOREIGN KEY (store_sk) REFERENCES store(sk);


--
-- Name: job_history job_history_product_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY job_history
    ADD CONSTRAINT job_history_product_sk_fkey FOREIGN KEY (product_sk) REFERENCES product_gnm(sk);


--
-- Name: job_history job_history_supplier_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY job_history
    ADD CONSTRAINT job_history_supplier_sk_fkey FOREIGN KEY (supplier_sk) REFERENCES supplier(sk);


--
-- Name: order_history order_history_customer_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY order_history
    ADD CONSTRAINT order_history_customer_sk_fkey FOREIGN KEY (customer_sk) REFERENCES customer(sk);


--
-- Name: order_history order_history_store_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY order_history
    ADD CONSTRAINT order_history_store_sk_fkey FOREIGN KEY (store_sk) REFERENCES store(sk);


--
-- Name: prod_common prod_common_prod_ref_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY prod_common
    ADD CONSTRAINT prod_common_prod_ref_sk_fkey FOREIGN KEY (prod_ref_sk) REFERENCES prod_ref(sk);


--
-- Name: prod_common prod_common_product_gnm_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY prod_common
    ADD CONSTRAINT prod_common_product_gnm_sk_fkey FOREIGN KEY (product_gnm_sk) REFERENCES product_gnm(sk);


--
-- Name: prod_common prod_common_storeproduct_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY prod_common
    ADD CONSTRAINT prod_common_storeproduct_sk_fkey FOREIGN KEY (storeproduct_sk) REFERENCES storeproduct(sk);


--
-- Name: stock_price_history stock_price_history_store_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY stock_price_history
    ADD CONSTRAINT stock_price_history_store_sk_fkey FOREIGN KEY (store_sk) REFERENCES store(sk);


--
-- Name: stock_price_history stock_price_history_takeon_storeproduct_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY stock_price_history
    ADD CONSTRAINT stock_price_history_takeon_storeproduct_sk_fkey FOREIGN KEY (takeon_storeproduct_sk) REFERENCES storeproduct(sk);


--
-- Name: store_stock_history_adj store_stock_history_adj_product_gnm_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stock_history_adj
    ADD CONSTRAINT store_stock_history_adj_product_gnm_sk_fkey FOREIGN KEY (product_gnm_sk) REFERENCES product_gnm(sk);


--
-- Name: store_stock_history_adj store_stock_history_adj_takeon_storeproduct_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stock_history_adj
    ADD CONSTRAINT store_stock_history_adj_takeon_storeproduct_sk_fkey FOREIGN KEY (takeon_storeproduct_sk) REFERENCES storeproduct(sk);


--
-- Name: store_stock_history store_stock_history_employee_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stock_history
    ADD CONSTRAINT store_stock_history_employee_sk_fkey FOREIGN KEY (employee_sk) REFERENCES employee(sk);


--
-- Name: store_stock_history store_stock_history_product_gnm_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stock_history
    ADD CONSTRAINT store_stock_history_product_gnm_sk_fkey FOREIGN KEY (product_gnm_sk) REFERENCES product_gnm(sk);


--
-- Name: store_stock_history store_stock_history_store_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stock_history
    ADD CONSTRAINT store_stock_history_store_sk_fkey FOREIGN KEY (store_sk) REFERENCES store(sk);


--
-- Name: store_stock_history store_stock_history_takeon_storeproduct_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stock_history
    ADD CONSTRAINT store_stock_history_takeon_storeproduct_sk_fkey FOREIGN KEY (takeon_storeproduct_sk) REFERENCES storeproduct(sk);


--
-- Name: store_stockmovement store_stockmovement_customer_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stockmovement
    ADD CONSTRAINT store_stockmovement_customer_sk_fkey FOREIGN KEY (customer_sk) REFERENCES customer(sk);


--
-- Name: store_stockmovement store_stockmovement_employee_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stockmovement
    ADD CONSTRAINT store_stockmovement_employee_sk_fkey FOREIGN KEY (employee_sk) REFERENCES employee(sk);


--
-- Name: store_stockmovement store_stockmovement_invoice_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stockmovement
    ADD CONSTRAINT store_stockmovement_invoice_sk_fkey FOREIGN KEY (invoice_sk) REFERENCES invoice(sk);


--
-- Name: store_stockmovement store_stockmovement_stock_price_history_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stockmovement
    ADD CONSTRAINT store_stockmovement_stock_price_history_sk_fkey FOREIGN KEY (stock_price_history_sk) REFERENCES stock_price_history(sk);


--
-- Name: store_stockmovement store_stockmovement_store_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stockmovement
    ADD CONSTRAINT store_stockmovement_store_sk_fkey FOREIGN KEY (store_sk) REFERENCES store(sk);


--
-- Name: store_stockmovement store_stockmovement_takeon_storeproduct_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY store_stockmovement
    ADD CONSTRAINT store_stockmovement_takeon_storeproduct_sk_fkey FOREIGN KEY (takeon_storeproduct_sk) REFERENCES storeproduct(sk);


--
-- Name: supplierproduct supplierproduct_supplier_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY supplierproduct
    ADD CONSTRAINT supplierproduct_supplier_fkey FOREIGN KEY (suppliersk) REFERENCES supplier(sk);


--
-- Name: takeon_product takeon_product_store_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY takeon_product
    ADD CONSTRAINT takeon_product_store_sk_fkey FOREIGN KEY (store_sk) REFERENCES store(sk);


--
-- Name: takeon_product takeon_product_supplier_sk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY takeon_product
    ADD CONSTRAINT takeon_product_supplier_sk_fkey FOREIGN KEY (supplier_sk) REFERENCES supplier(sk);


SET search_path = gnm, pg_catalog;

--
-- Name: brand; Type: ROW SECURITY; Schema: gnm; Owner: -
--

ALTER TABLE brand ENABLE ROW LEVEL SECURITY;

--
-- Name: brand_tier; Type: ROW SECURITY; Schema: gnm; Owner: -
--

ALTER TABLE brand_tier ENABLE ROW LEVEL SECURITY;

--
-- Name: order; Type: ROW SECURITY; Schema: gnm; Owner: -
--

ALTER TABLE "order" ENABLE ROW LEVEL SECURITY;

--
-- Name: price_policy; Type: ROW SECURITY; Schema: gnm; Owner: -
--

ALTER TABLE price_policy ENABLE ROW LEVEL SECURITY;

--
-- Name: product; Type: ROW SECURITY; Schema: gnm; Owner: -
--

ALTER TABLE product ENABLE ROW LEVEL SECURITY;

--
-- Name: product_assortment_store; Type: ROW SECURITY; Schema: gnm; Owner: -
--

ALTER TABLE product_assortment_store ENABLE ROW LEVEL SECURITY;

--
-- Name: product_cost_history; Type: ROW SECURITY; Schema: gnm; Owner: -
--

ALTER TABLE product_cost_history ENABLE ROW LEVEL SECURITY;

--
-- Name: product_price; Type: ROW SECURITY; Schema: gnm; Owner: -
--

ALTER TABLE product_price ENABLE ROW LEVEL SECURITY;

--
-- Name: product_price_history; Type: ROW SECURITY; Schema: gnm; Owner: -
--

ALTER TABLE product_price_history ENABLE ROW LEVEL SECURITY;

--
-- Name: product_state; Type: ROW SECURITY; Schema: gnm; Owner: -
--

ALTER TABLE product_state ENABLE ROW LEVEL SECURITY;

--
-- Name: product_state_request; Type: ROW SECURITY; Schema: gnm; Owner: -
--

ALTER TABLE product_state_request ENABLE ROW LEVEL SECURITY;

--
-- Name: brand restrict_rows; Type: POLICY; Schema: gnm; Owner: -
--

CREATE POLICY restrict_rows ON brand USING (((NOT ('supplier_user'::text IN ( SELECT user_role.role_id
   FROM user_role
  WHERE ((user_role.user_id)::text = current_setting('gnm.userId'::text))))) OR (EXISTS ( SELECT us.id,
    us.user_id,
    us.supplier_id,
    us.created_user,
    us.created_date_time,
    us.modified_user,
    us.modified_date_time
   FROM user_supplier us
  WHERE (((us.user_id)::text = current_setting('gnm.userId'::text)) AND (us.supplier_id = brand.supplier_id))))));


--
-- Name: brand_tier restrict_rows; Type: POLICY; Schema: gnm; Owner: -
--

CREATE POLICY restrict_rows ON brand_tier USING (((NOT ('supplier_user'::text IN ( SELECT user_role.role_id
   FROM user_role
  WHERE ((user_role.user_id)::text = current_setting('gnm.userId'::text))))) OR (EXISTS ( SELECT us.id,
    us.user_id,
    us.supplier_id,
    us.created_user,
    us.created_date_time,
    us.modified_user,
    us.modified_date_time
   FROM user_supplier us
  WHERE (((us.user_id)::text = current_setting('gnm.userId'::text)) AND (us.supplier_id = brand_tier.supplier_id))))));


--
-- Name: order restrict_rows; Type: POLICY; Schema: gnm; Owner: -
--

CREATE POLICY restrict_rows ON "order" USING (
CASE ( SELECT user_role.role_id
       FROM user_role
      WHERE (((user_role.user_id)::text = current_setting('gnm.userId'::text)) AND ((user_role.role_id)::text = ANY (ARRAY[('store_user'::character varying)::text, ('supplier_user'::character varying)::text])))
     LIMIT 1)
    WHEN 'store_user'::text THEN (EXISTS ( SELECT us.id,
        us.user_id,
        us.store_id,
        us.created_user,
        us.created_date_time,
        us.modified_user,
        us.modified_date_time
       FROM user_store us
      WHERE (((us.user_id)::text = current_setting('gnm.userId'::text)) AND ("order".store_id = us.store_id))))
    WHEN 'supplier_user'::text THEN (EXISTS ( SELECT j.id,
        j.job_id,
        j.order_id,
        j.product_id,
        j.supplier_id,
        j.quantity,
        j.status,
        j.created_user,
        j.created_date_time,
        j.modified_user,
        j.modified_date_time,
        j.address_id,
        j.queue,
        j.override_address_id,
        j.job_type_id,
        us.id,
        us.user_id,
        us.supplier_id,
        us.created_user,
        us.created_date_time,
        us.modified_user,
        us.modified_date_time
       FROM job j,
        user_supplier us
      WHERE (((us.user_id)::text = current_setting('gnm.userId'::text)) AND (j.supplier_id = us.supplier_id) AND (("order".order_id)::text = (j.order_id)::text))))
    ELSE true
END);


--
-- Name: price_policy restrict_rows; Type: POLICY; Schema: gnm; Owner: -
--

CREATE POLICY restrict_rows ON price_policy USING (((NOT ('supplier_user'::text IN ( SELECT user_role.role_id
   FROM user_role
  WHERE ((user_role.user_id)::text = current_setting('gnm.userId'::text))))) OR (EXISTS ( SELECT us.id,
    us.user_id,
    us.supplier_id,
    us.created_user,
    us.created_date_time,
    us.modified_user,
    us.modified_date_time
   FROM user_supplier us
  WHERE (((us.user_id)::text = current_setting('gnm.userId'::text)) AND (us.supplier_id = price_policy.supplier_id))))));


--
-- Name: product restrict_rows; Type: POLICY; Schema: gnm; Owner: -
--

CREATE POLICY restrict_rows ON product USING (((NOT ('supplier_user'::text IN ( SELECT user_role.role_id
   FROM user_role
  WHERE ((user_role.user_id)::text = current_setting('gnm.userId'::text))))) OR (EXISTS ( SELECT us.id,
    us.user_id,
    us.supplier_id,
    us.created_user,
    us.created_date_time,
    us.modified_user,
    us.modified_date_time
   FROM user_supplier us
  WHERE (((us.user_id)::text = current_setting('gnm.userId'::text)) AND (us.supplier_id = product.supplier_id))))));


--
-- Name: product_assortment_store restrict_rows; Type: POLICY; Schema: gnm; Owner: -
--

CREATE POLICY restrict_rows ON product_assortment_store USING (((NOT ('store_user'::text IN ( SELECT user_role.role_id
   FROM user_role
  WHERE ((user_role.user_id)::text = current_setting('gnm.userId'::text))))) OR (EXISTS ( SELECT us.id,
    us.user_id,
    us.store_id,
    us.created_user,
    us.created_date_time,
    us.modified_user,
    us.modified_date_time
   FROM user_store us
  WHERE (((us.user_id)::text = current_setting('gnm.userId'::text)) AND (product_assortment_store.store_id = us.store_id))))));


--
-- Name: store_product_coll restrict_rows; Type: POLICY; Schema: gnm; Owner: -
--

CREATE POLICY restrict_rows ON store_product_coll USING (((NOT ('store_user'::text IN ( SELECT user_role.role_id
   FROM user_role
  WHERE ((user_role.user_id)::text = current_setting('gnm.userId'::text))))) OR (EXISTS ( SELECT us.id,
    us.user_id,
    us.store_id,
    us.created_user,
    us.created_date_time,
    us.modified_user,
    us.modified_date_time
   FROM user_store us
  WHERE (((us.user_id)::text = current_setting('gnm.userId'::text)) AND (store_product_coll.store_id = us.store_id))))));


--
-- Name: supplier restrict_rows; Type: POLICY; Schema: gnm; Owner: -
--

CREATE POLICY restrict_rows ON supplier USING (((NOT ('supplier_user'::text IN ( SELECT user_role.role_id
   FROM user_role
  WHERE ((user_role.user_id)::text = current_setting('gnm.userId'::text))))) OR (EXISTS ( SELECT us.id,
    us.user_id,
    us.supplier_id,
    us.created_user,
    us.created_date_time,
    us.modified_user,
    us.modified_date_time
   FROM user_supplier us
  WHERE (((us.user_id)::text = current_setting('gnm.userId'::text)) AND (us.supplier_id = supplier.id))))));


--
-- Name: task restrict_rows; Type: POLICY; Schema: gnm; Owner: -
--

CREATE POLICY restrict_rows ON task USING (
CASE ( SELECT user_role.role_id
       FROM user_role
      WHERE (((user_role.user_id)::text = current_setting('gnm.userId'::text)) AND ((user_role.role_id)::text = ANY (ARRAY[('store_user'::character varying)::text, ('supplier_user'::character varying)::text])))
     LIMIT 1)
    WHEN 'supplier_user'::text THEN (EXISTS ( SELECT us1.id,
        us1.user_id,
        us1.supplier_id,
        us1.created_user,
        us1.created_date_time,
        us1.modified_user,
        us1.modified_date_time,
        us2.id,
        us2.user_id,
        us2.supplier_id,
        us2.created_user,
        us2.created_date_time,
        us2.modified_user,
        us2.modified_date_time
       FROM user_supplier us1,
        user_supplier us2
      WHERE (((us1.user_id)::text = current_setting('gnm.userId'::text)) AND (us2.supplier_id = us1.supplier_id) AND (us2.user_id = task.user_id))))
    WHEN 'store_user'::text THEN (EXISTS ( SELECT us1.id,
        us1.user_id,
        us1.store_id,
        us1.created_user,
        us1.created_date_time,
        us1.modified_user,
        us1.modified_date_time,
        us2.id,
        us2.user_id,
        us2.store_id,
        us2.created_user,
        us2.created_date_time,
        us2.modified_user,
        us2.modified_date_time
       FROM user_store us1,
        user_store us2
      WHERE (((us1.user_id)::text = current_setting('gnm.userId'::text)) AND (us2.store_id = us1.store_id) AND (us2.user_id = task.user_id))))
    ELSE true
END);


--
-- Name: user_preference restrict_rows; Type: POLICY; Schema: gnm; Owner: -
--

CREATE POLICY restrict_rows ON user_preference USING ((EXISTS ( SELECT u.id,
    u.person_id,
    u.password,
    u.email_address,
    u.created_user,
    u.created_date_time,
    u.modified_user,
    u.modified_date_time,
    u.password_change_required,
    u.last_activity_time
   FROM "user" u
  WHERE (u.id = user_preference.user_id))));


--
-- Name: uploaded_file restrict_rows; Type: POLICY; Schema: gnm; Owner: -
--

CREATE POLICY restrict_rows ON uploaded_file USING (
CASE ( SELECT user_role.role_id
       FROM user_role
      WHERE (((user_role.user_id)::text = current_setting('gnm.userId'::text)) AND ((user_role.role_id)::text = ANY (ARRAY[('store_user'::character varying)::text, ('supplier_user'::character varying)::text])))
     LIMIT 1)
    WHEN 'supplier_user'::text THEN (EXISTS ( SELECT us1.id,
        us1.user_id,
        us1.supplier_id,
        us1.created_user,
        us1.created_date_time,
        us1.modified_user,
        us1.modified_date_time,
        us2.id,
        us2.user_id,
        us2.supplier_id,
        us2.created_user,
        us2.created_date_time,
        us2.modified_user,
        us2.modified_date_time
       FROM user_supplier us1,
        user_supplier us2
      WHERE (((us1.user_id)::text = current_setting('gnm.userId'::text)) AND (us2.supplier_id = us1.supplier_id) AND (us2.user_id = uploaded_file.user_id))))
    WHEN 'store_user'::text THEN (EXISTS ( SELECT us1.id,
        us1.user_id,
        us1.store_id,
        us1.created_user,
        us1.created_date_time,
        us1.modified_user,
        us1.modified_date_time,
        us2.id,
        us2.user_id,
        us2.store_id,
        us2.created_user,
        us2.created_date_time,
        us2.modified_user,
        us2.modified_date_time
       FROM user_store us1,
        user_store us2
      WHERE (((us1.user_id)::text = current_setting('gnm.userId'::text)) AND (us2.store_id = us1.store_id) AND (us2.user_id = uploaded_file.user_id))))
    ELSE true
END);


--
-- Name: product_cost_history restrict_rows; Type: POLICY; Schema: gnm; Owner: -
--

CREATE POLICY restrict_rows ON product_cost_history USING ((EXISTS ( SELECT p.id,
    p.product_id,
    p.name,
    p.type,
    p.apn,
    p.description,
    p.current_state_id,
    p.supplier_rrp,
    p.supplier_rrp_gst,
    p.supplier_cost,
    p.supplier_cost_gst,
    p.created_user,
    p.created_date_time,
    p.modified_user,
    p.modified_date_time,
    p.supplier_id,
    p.image_id,
    p.supplier_sku,
    p.brand_id,
    p.price_policy_id,
    p.actual_cost,
    p.actual_cost_gst,
    p.calculated_retail_price,
    p.calculated_retail_price_gst,
    p.override_cost,
    p.override_cost_gst,
    p.price_change_reason,
    p.price_error
   FROM product p
  WHERE ((p.product_id)::text = (product_cost_history.product_id)::text))));


--
-- Name: product_price restrict_rows; Type: POLICY; Schema: gnm; Owner: -
--

CREATE POLICY restrict_rows ON product_price USING ((EXISTS ( SELECT p.id,
    p.product_id,
    p.name,
    p.type,
    p.apn,
    p.description,
    p.current_state_id,
    p.supplier_rrp,
    p.supplier_rrp_gst,
    p.supplier_cost,
    p.supplier_cost_gst,
    p.created_user,
    p.created_date_time,
    p.modified_user,
    p.modified_date_time,
    p.supplier_id,
    p.image_id,
    p.supplier_sku,
    p.brand_id,
    p.price_policy_id,
    p.actual_cost,
    p.actual_cost_gst,
    p.calculated_retail_price,
    p.calculated_retail_price_gst,
    p.override_cost,
    p.override_cost_gst,
    p.price_change_reason,
    p.price_error
   FROM product p
  WHERE ((p.product_id)::text = (product_price.product_id)::text))));


--
-- Name: product_price_history restrict_rows; Type: POLICY; Schema: gnm; Owner: -
--

CREATE POLICY restrict_rows ON product_price_history USING ((EXISTS ( SELECT p.id,
    p.product_id,
    p.name,
    p.type,
    p.apn,
    p.description,
    p.current_state_id,
    p.supplier_rrp,
    p.supplier_rrp_gst,
    p.supplier_cost,
    p.supplier_cost_gst,
    p.created_user,
    p.created_date_time,
    p.modified_user,
    p.modified_date_time,
    p.supplier_id,
    p.image_id,
    p.supplier_sku,
    p.brand_id,
    p.price_policy_id,
    p.actual_cost,
    p.actual_cost_gst,
    p.calculated_retail_price,
    p.calculated_retail_price_gst,
    p.override_cost,
    p.override_cost_gst,
    p.price_change_reason,
    p.price_error
   FROM product p
  WHERE ((p.product_id)::text = (product_price_history.product_id)::text))));


--
-- Name: product_state restrict_rows; Type: POLICY; Schema: gnm; Owner: -
--

CREATE POLICY restrict_rows ON product_state USING ((EXISTS ( SELECT p.id,
    p.product_id,
    p.name,
    p.type,
    p.apn,
    p.description,
    p.current_state_id,
    p.supplier_rrp,
    p.supplier_rrp_gst,
    p.supplier_cost,
    p.supplier_cost_gst,
    p.created_user,
    p.created_date_time,
    p.modified_user,
    p.modified_date_time,
    p.supplier_id,
    p.image_id,
    p.supplier_sku,
    p.brand_id,
    p.price_policy_id,
    p.actual_cost,
    p.actual_cost_gst,
    p.calculated_retail_price,
    p.calculated_retail_price_gst,
    p.override_cost,
    p.override_cost_gst,
    p.price_change_reason,
    p.price_error
   FROM product p
  WHERE ((p.product_id)::text = (product_state.product_id)::text))));


--
-- Name: product_state_request restrict_rows; Type: POLICY; Schema: gnm; Owner: -
--

CREATE POLICY restrict_rows ON product_state_request USING ((EXISTS ( SELECT p.id,
    p.product_id,
    p.name,
    p.type,
    p.apn,
    p.description,
    p.current_state_id,
    p.supplier_rrp,
    p.supplier_rrp_gst,
    p.supplier_cost,
    p.supplier_cost_gst,
    p.created_user,
    p.created_date_time,
    p.modified_user,
    p.modified_date_time,
    p.supplier_id,
    p.image_id,
    p.supplier_sku,
    p.brand_id,
    p.price_policy_id,
    p.actual_cost,
    p.actual_cost_gst,
    p.calculated_retail_price,
    p.calculated_retail_price_gst,
    p.override_cost,
    p.override_cost_gst,
    p.price_change_reason,
    p.price_error
   FROM product p
  WHERE ((p.product_id)::text = (product_state_request.product_id)::text))));


--
-- Name: user restrict_rows_delete; Type: POLICY; Schema: gnm; Owner: -
--

CREATE POLICY restrict_rows_delete ON "user" FOR DELETE USING ((NOT user_has_any_role(VARIADIC ARRAY['store_user'::character varying, 'supplier_user'::character varying])));


--
-- Name: user restrict_rows_insert; Type: POLICY; Schema: gnm; Owner: -
--

CREATE POLICY restrict_rows_insert ON "user" FOR INSERT WITH CHECK ((NOT user_has_any_role(VARIADIC ARRAY['store_user'::character varying, 'supplier_user'::character varying])));


--
-- Name: user restrict_rows_select; Type: POLICY; Schema: gnm; Owner: -
--

CREATE POLICY restrict_rows_select ON "user" FOR SELECT USING ((user_is_me(id) OR
CASE ( SELECT user_role.role_id
       FROM user_role
      WHERE (((user_role.user_id)::text = current_setting('gnm.userId'::text)) AND ((user_role.role_id)::text = ANY (ARRAY[('store_user'::character varying)::text, ('supplier_user'::character varying)::text])))
     LIMIT 1)
    WHEN 'supplier_user'::text THEN (EXISTS ( SELECT us1.id,
        us1.user_id,
        us1.supplier_id,
        us1.created_user,
        us1.created_date_time,
        us1.modified_user,
        us1.modified_date_time,
        us2.id,
        us2.user_id,
        us2.supplier_id,
        us2.created_user,
        us2.created_date_time,
        us2.modified_user,
        us2.modified_date_time
       FROM user_supplier us1,
        user_supplier us2
      WHERE (((us1.user_id)::text = current_setting('gnm.userId'::text)) AND (us2.supplier_id = us1.supplier_id) AND (us2.user_id = "user".id))))
    WHEN 'store_user'::text THEN (EXISTS ( SELECT us1.id,
        us1.user_id,
        us1.store_id,
        us1.created_user,
        us1.created_date_time,
        us1.modified_user,
        us1.modified_date_time,
        us2.id,
        us2.user_id,
        us2.store_id,
        us2.created_user,
        us2.created_date_time,
        us2.modified_user,
        us2.modified_date_time
       FROM user_store us1,
        user_store us2
      WHERE (((us1.user_id)::text = current_setting('gnm.userId'::text)) AND (us2.store_id = us1.store_id) AND (us2.user_id = "user".id))))
    ELSE true
END));


--
-- Name: user restrict_rows_update_using; Type: POLICY; Schema: gnm; Owner: -
--

CREATE POLICY restrict_rows_update_using ON "user" FOR UPDATE USING ((user_is_me(id) OR (NOT user_has_any_role(VARIADIC ARRAY['store_user'::character varying, 'supplier_user'::character varying]))));


--
-- Name: user restrict_rows_update_with_check; Type: POLICY; Schema: gnm; Owner: -
--

CREATE POLICY restrict_rows_update_with_check ON "user" FOR UPDATE WITH CHECK ((user_is_me(id) OR (NOT user_has_any_role(VARIADIC ARRAY['store_user'::character varying, 'supplier_user'::character varying]))));


--
-- Name: store_product_coll; Type: ROW SECURITY; Schema: gnm; Owner: -
--

ALTER TABLE store_product_coll ENABLE ROW LEVEL SECURITY;

--
-- Name: supplier; Type: ROW SECURITY; Schema: gnm; Owner: -
--

ALTER TABLE supplier ENABLE ROW LEVEL SECURITY;

--
-- Name: task; Type: ROW SECURITY; Schema: gnm; Owner: -
--

ALTER TABLE task ENABLE ROW LEVEL SECURITY;

--
-- Name: uploaded_file; Type: ROW SECURITY; Schema: gnm; Owner: -
--

ALTER TABLE uploaded_file ENABLE ROW LEVEL SECURITY;

--
-- Name: user; Type: ROW SECURITY; Schema: gnm; Owner: -
--

ALTER TABLE "user" ENABLE ROW LEVEL SECURITY;

--
-- Name: user_preference; Type: ROW SECURITY; Schema: gnm; Owner: -
--

ALTER TABLE user_preference ENABLE ROW LEVEL SECURITY;

--
-- PostgreSQL database dump complete
--

