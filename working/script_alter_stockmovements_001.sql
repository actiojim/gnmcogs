

SET search_path = public, pg_catalog;


begin;
--- DELETE LEGACY DEFINITONS OF TABLES

drop table  if exists store_stockmovement CASCADE;
drop table  if exists store_stock_history CASCADE;
drop table  if exists invoice_orders CASCADE;
drop table  if exists stock_price_history CASCADE;
drop table  if exists takeon_product CASCADE;
drop table  if exists order_job_history CASCADE;
drop table  if exists order_history CASCADE;
drop table  if exists invoice_orders cascade;
drop table if exists takeon_product;

--- select * from order_history

--- ALTER TABLES

alter table storeproduct ADD column product_gnm_sk integer; 

alter table invoiceitem add column createddate_sk integer;
alter table invoiceitem add column pms_cost numeric(19,2);
alter table invoiceitem add column gnm_current_cost numeric(19,2);
alter table invoiceitem add column takeon_avg_cost numeric(19,2);
alter table invoiceitem add column order_type character varying(90);
alter table invoiceitem add column order_id character varying(90);

alter table "store" add column     nk character varying(60);
alter table "store" add column     acquisition_date timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT null;


-- rollback
-- commit

