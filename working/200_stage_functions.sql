-- FUNCTION: stage.generate_view_for_table(character varying)

-- DROP FUNCTION stage.generate_view_for_table(character varying);

CREATE OR REPLACE FUNCTION stage.generate_view_for_table(
	_tname character varying)
    RETURNS void
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 0
AS $BODY$

declare
sql varchar;
begin

SELECT 'CREATE VIEW stage.today_' || _tname || ' AS SELECT ' || string_agg(pick || '->>''' || col || ''' as "' || col || '"',',') || ' FROM stage.get_latest_data_by_tablename(''' || _tname || ''')'  as sel
FROM (
select distinct 'data' as pick, jsonb_object_keys("data") as col from stage.get_latest_data_by_tablename(_tname)
union all 
select distinct 'parameters' as pick, jsonb_object_keys("parameters") as col from stage.get_latest_data_by_tablename(_tname)
) T into sql;

execute sql;

end;

$BODY$;

ALTER FUNCTION stage.generate_view_for_table(character varying)
    OWNER TO postgres;


-- FUNCTION: stage.get_latest_data_by_tablename(character)

-- DROP FUNCTION stage.get_latest_data_by_tablename(character);

CREATE OR REPLACE FUNCTION stage.get_latest_data_by_tablename(
	_tablename character)
    RETURNS TABLE(data jsonb, parameters jsonb) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$

declare
sql varchar;
begin
SELECT 'SELECT jsonb_array_elements("data") as data,parameters FROM stage.' || _tablename || ' T where T.runid in (
select runid from (
select parameters->>''source'',runid,createddate,row_number() over (partition by parameters->>''source'' order by createddate desc) as r from stage.' || _tablename || ') S where r = 1);' 
into sql;

return query execute sql;

end;

$BODY$;

ALTER FUNCTION stage.get_latest_data_by_tablename(character)
    OWNER TO postgres;

