


--
-- Name: cogs_sales; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW cogs_sales AS
 SELECT ii.invoicesk,
    ii.pos,
    ii.productsk,
    ii.storesk,
    ii.employeesk,
    ii.serviceemployeesk,
    ii.customersk,
    ii.createddate,
    ii.quantity,
    ii.amount_incl_gst,
    ii.modified,
    ii.amount_gst,
    ii.discount_incl_gst,
    ii.discount_gst,
    ii.cost_incl_gst,
    ii.cost_gst,
    ii.description,
    ii.payeesk,
    ii.invoiceitemid,
    ii.return_incl_gst,
    ii.return_gst,
    ii.bulkbillref,
    ( SELECT ph.actual_cost
           FROM stock_price_history ph
          WHERE ((ph.takeon_storeproduct_sk = ii.productsk) AND (ph.created <= ii.createddate))
          ORDER BY ph.created DESC
         LIMIT 1) AS takeon_current_cost,
    ( SELECT ph.actual_cost
           FROM stock_price_history ph
          WHERE ((ph.product_gnm_sk = tp.product_gnm_sk) AND (ph.created <= ii.createddate))
          ORDER BY ph.created DESC
         LIMIT 1) AS gnm_current_cost,
    ( SELECT m.actual_cost
           FROM store_stockmovement m
          WHERE ((m.invoice_sk = ii.invoicesk) AND (m.takeon_storeproduct_sk = ii.productsk))
         LIMIT 1) AS average_cost,
    ( SELECT oh.order_type
           FROM (invoice_orders io
             JOIN order_history oh ON ((io.order_sk = oh.sk)))
          WHERE (io.invoice_sk = ii.invoicesk)
          ORDER BY oh.source_modified DESC
         LIMIT 1) AS order_type
   FROM (invoiceitem_old ii
     JOIN storeproduct tp ON ((ii.productsk = tp.sk)))
  ORDER BY ii.createddate DESC;


 ------------------------------------------------
  
  
CREATE VIEW last_job AS
 SELECT jh.sk,
    jh.source_id,
    jh.order_id,
    jh.order_sk,
    jh.job_type,
    jh.product_sk,
    jh.supplier_sk,
    jh.queue,
    jh.status,
    jh.source_created,
    jh.source_modified,
    jh.created
   FROM (job_history jh
     LEFT JOIN ( SELECT jh_1.source_id,
            max(jh_1.sk) AS max_sk,
            max(jh_1.source_modified) AS max_date,
            count(*) AS count
           FROM job_history jh_1
          GROUP BY jh_1.source_id) x ON ((x.max_sk = jh.sk)));

----------------------------------------------------------
          

--
-- Name: t1; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW t1 AS
 SELECT d.sk,
    d.date_dim_id,
    d.date_actual,
    d.epoch,
    d.day_suffix,
    d.day_name,
    d.day_of_week,
    d.day_of_month,
    d.day_of_quarter,
    d.day_of_year,
    d.week_of_month,
    d.week_of_year,
    d.week_of_year_iso,
    d.month_actual,
    d.month_name,
    d.month_name_abbreviated,
    d.year_month_name,
    d.quarter_actual,
    d.quarter_name,
    d.year_calendar,
    d.year_calendar_name,
    d.year_financial,
    d.year_financial_name,
    d.first_day_of_week,
    d.last_day_of_week,
    d.first_day_of_month,
    d.last_day_of_month,
    d.first_day_of_quarter,
    d.last_day_of_quarter,
    d.first_day_of_year,
    d.last_day_of_year,
    d.yyyymm,
    d.yyyyq,
    d.weekend_indr,
    m.adjustment AS measure
   FROM (store_stockmovement m
     JOIN d_date d ON ((m.adjustmentdate_sk = d.sk)));


-------------------------------------------------------------------------
     

--
-- Name: v_cogs_first_receipt; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW v_cogs_first_receipt AS
 SELECT y.sk,
    y.adjustmentdate,
    y.product_gnm_sk,
    y.adjustment,
    y.actual_cost,
    ((y.adjustment)::numeric * y.actual_cost) AS actual_cost_value,
    ((y.adjustment)::numeric * y.actual_cost) AS moving_actual_cost_value,
    y.actual_cost AS moving_cost_per_unit,
    ( SELECT im.sk
           FROM store_stockmovement im
          WHERE ((im.product_gnm_sk = y.product_gnm_sk) AND (im.adjustmentdate >= y.adjustmentdate) AND (im.sk <> y.sk))
          ORDER BY im.adjustmentdate, im.sk
         LIMIT 1) AS n1
   FROM ( SELECT t.sk,
            t.store_sk,
            t.source_id,
            t.takeon_storeproduct_sk,
            t.adjustmentdate,
            t.stock_price_history_sk,
            t.adjustment,
            t.adjustmenttype,
            t.reason,
            t.invoice_sk,
            t.customer_sk,
            t.avg_cost_price,
            t.shipping_ref,
            t.shipping_operator,
            t.comments,
            t.employee_sk,
            t.created,
            t.adjustmentdate_sk,
            t.product_gnm_sk,
            t.adjustment_type_sk,
            t.actual_cost,
            row_number() OVER (PARTITION BY t.product_gnm_sk ORDER BY t.adjustmentdate, t.sk) AS r
           FROM ( SELECT m.sk,
                    m.store_sk,
                    m.source_id,
                    m.takeon_storeproduct_sk,
                    m.adjustmentdate,
                    m.stock_price_history_sk,
                    m.adjustment,
                    m.adjustmenttype,
                    m.reason,
                    m.invoice_sk,
                    m.customer_sk,
                    m.avg_cost_price,
                    m.shipping_ref,
                    m.shipping_operator,
                    m.comments,
                    m.employee_sk,
                    m.created,
                    m.adjustmentdate_sk,
                    m.product_gnm_sk,
                    m.adjustment_type_sk,
                    ( SELECT h.actual_cost
                           FROM stock_price_history h
                          WHERE ((h.product_gnm_sk = m.product_gnm_sk) AND (h.created <= m.adjustmentdate))
                          ORDER BY h.created DESC
                         LIMIT 1) AS actual_cost
                   FROM (store_stockmovement m
                     JOIN adjustment_type a ON (((m.adjustment_type_sk = a.sk) AND ((a.adjustment_typeid)::text = 'RESTOCK'::text))))
                  WHERE (m.product_gnm_sk <> 0)) t
          WHERE (t.actual_cost IS NOT NULL)) y
  WHERE (y.r = 1);


--
-- Name: vbyd_invoice; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW vbyd_invoice AS
 SELECT date_part('year'::text, sm.createddate) AS year,
    date_part('doy'::text, sm.createddate) AS doy,
    date_part('month'::text, sm.createddate) AS month,
    date_part('dow'::text, sm.createddate) AS dow,
    date_part('day'::text, sm.createddate) AS day,
    sm.sk
   FROM invoice sm
  GROUP BY (date_part('year'::text, sm.createddate)), (date_part('doy'::text, sm.createddate)), (date_part('month'::text, sm.createddate)), (date_part('dow'::text, sm.createddate)), (date_part('day'::text, sm.createddate)), sm.sk;


--
-- Name: vbyd_invoiceitem; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW vbyd_invoiceitem AS
 SELECT date_part('year'::text, sm.createddate) AS year,
    date_part('doy'::text, sm.createddate) AS doy,
    date_part('month'::text, sm.createddate) AS month,
    date_part('dow'::text, sm.createddate) AS dow,
    date_part('day'::text, sm.createddate) AS day,
    sm.invoicesk
   FROM invoiceitem_old sm
  GROUP BY (date_part('year'::text, sm.createddate)), (date_part('doy'::text, sm.createddate)), (date_part('month'::text, sm.createddate)), (date_part('dow'::text, sm.createddate)), (date_part('day'::text, sm.createddate)), sm.invoicesk;


--
-- Name: vbyd_job_history; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW vbyd_job_history AS
 SELECT date_part('year'::text, sm.source_created) AS year,
    date_part('doy'::text, sm.source_created) AS doy,
    date_part('month'::text, sm.source_created) AS month,
    date_part('dow'::text, sm.source_created) AS dow,
    date_part('day'::text, sm.source_created) AS day,
    sm.sk
   FROM job_history sm
  GROUP BY (date_part('year'::text, sm.source_created)), (date_part('doy'::text, sm.source_created)), (date_part('month'::text, sm.source_created)), (date_part('dow'::text, sm.source_created)), (date_part('day'::text, sm.source_created)), sm.sk;


--
-- Name: vbyd_order_history; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW vbyd_order_history AS
 SELECT date_part('year'::text, sm.source_created) AS year,
    date_part('doy'::text, sm.source_created) AS doy,
    date_part('month'::text, sm.source_created) AS month,
    date_part('dow'::text, sm.source_created) AS dow,
    date_part('day'::text, sm.source_created) AS day,
    sm.sk
   FROM order_history sm
  GROUP BY (date_part('year'::text, sm.source_created)), (date_part('doy'::text, sm.source_created)), (date_part('month'::text, sm.source_created)), (date_part('dow'::text, sm.source_created)), (date_part('day'::text, sm.source_created)), sm.sk;


--
-- Name: vbyd_stock_price_history; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW vbyd_stock_price_history AS
 SELECT date_part('year'::text, sm.created) AS year,
    date_part('doy'::text, sm.created) AS doy,
    date_part('month'::text, sm.created) AS month,
    date_part('dow'::text, sm.created) AS dow,
    date_part('day'::text, sm.created) AS day,
    sm.sk
   FROM stock_price_history sm
  GROUP BY (date_part('year'::text, sm.created)), (date_part('doy'::text, sm.created)), (date_part('month'::text, sm.created)), (date_part('dow'::text, sm.created)), (date_part('day'::text, sm.created)), sm.sk;


--
-- Name: vbyd_store_stockmovement; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW vbyd_store_stockmovement AS
 SELECT date_part('year'::text, sm.adjustmentdate) AS year,
    date_part('doy'::text, sm.adjustmentdate) AS doy,
    date_part('month'::text, sm.adjustmentdate) AS month,
    date_part('dow'::text, sm.adjustmentdate) AS dow,
    date_part('day'::text, sm.adjustmentdate) AS day,
    sm.sk
   FROM store_stockmovement sm
  GROUP BY (date_part('year'::text, sm.adjustmentdate)), (date_part('doy'::text, sm.adjustmentdate)), (date_part('month'::text, sm.adjustmentdate)), (date_part('dow'::text, sm.adjustmentdate)), (date_part('day'::text, sm.adjustmentdate)), sm.sk;


--
-- Name: viewsmbyperiod; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW viewsmbyperiod AS
 SELECT date_part('year'::text, sm.adjustmentdate) AS year,
    date_part('doy'::text, sm.adjustmentdate) AS doy,
    date_part('month'::text, sm.adjustmentdate) AS month,
    date_part('dow'::text, sm.adjustmentdate) AS dow,
    date_part('day'::text, sm.adjustmentdate) AS day,
    sm.sk
   FROM store_stockmovement sm
  GROUP BY (date_part('year'::text, sm.adjustmentdate)), (date_part('doy'::text, sm.adjustmentdate)), (date_part('month'::text, sm.adjustmentdate)), (date_part('dow'::text, sm.adjustmentdate)), (date_part('day'::text, sm.adjustmentdate)), sm.sk;

--
-- Name: test; Type: MATERIALIZED VIEW; Schema: public; Owner: -
--

CREATE MATERIALIZED VIEW test AS
 SELECT tt.sk,
    tt.today,
    tt.revenue
   FROM ( SELECT d.sk,
            d.date_actual AS today,
            sum(i.amount_gst) AS revenue
           FROM (invoiceitem i
             JOIN d_date d ON ((i.createddate_sk = d.sk)))
          GROUP BY d.sk, d.date_actual) tt
  WITH NO DATA;
