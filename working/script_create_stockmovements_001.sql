
-- SET search_path = public, pg_catalog;

-- begin;

-- rollback
-- commit
--- create stockmovements tables

--
-- Name: adjustment_type; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE adjustment_type (
    sk integer NOT NULL,
    adjustment_typeid character varying(30) NOT NULL,
    name character varying(100) DEFAULT 'Other'::character varying NOT NULL,
    othername character varying(100) NOT NULL
);


-- select * from adjustment_type

--
-- Name: adjustment_type_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE adjustment_type_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: adjustment_type_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE adjustment_type_sk_seq OWNED BY adjustment_type.sk;


-- Name: invoice_orders; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE invoice_orders (
    sk integer NOT NULL,
    store_sk integer NOT NULL,
    invoice_sk integer NOT NULL,
    order_sk integer NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: invoiceitem_old; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE invoiceitem_old (
    invoicesk integer NOT NULL,
    pos integer NOT NULL,
    productsk integer NOT NULL,
    storesk integer NOT NULL,
    employeesk integer NOT NULL,
    serviceemployeesk integer NOT NULL,
    customersk integer NOT NULL,
    createddate timestamp without time zone NOT NULL,
    quantity integer NOT NULL,
    amount_incl_gst numeric(19,4),
    modified timestamp without time zone,
    amount_gst numeric(19,4),
    discount_incl_gst numeric(19,4),
    discount_gst numeric(19,4),
    cost_incl_gst numeric(19,4),
    cost_gst numeric(19,4),
    description character varying(500),
    payeesk integer,
    invoiceitemid character varying(60) DEFAULT ''::character varying NOT NULL,
    return_incl_gst numeric(19,4),
    return_gst numeric(19,4),
    bulkbillref character varying(100)
);


--
-- Name: order_history; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE order_history (
    sk integer NOT NULL,
    store_sk integer NOT NULL,
    customer_sk integer NOT NULL,
    source_id character varying(100),
    order_type character varying(100) NOT NULL,
    queue character varying(100),
    status character varying(100),
    source_created timestamp without time zone NOT NULL,
    source_modified timestamp without time zone NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: stock_price_history; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE stock_price_history (
    sk integer NOT NULL,
    store_sk integer NOT NULL,
    source_id character varying(100) NOT NULL,
    takeon_storeproduct_sk integer NOT NULL,
    supplier_cost numeric(19,2),
    supplier_cost_gst numeric(19,2),
    actual_cost numeric(19,2),
    actual_cost_gst numeric(19,2),
    supplier_rrp numeric(19,2),
    supplier_rrp_gst numeric(19,2),
    calculated_retail_price numeric(19,2),
    calculated_retail_price_gst numeric(19,2),
    override_retail_price numeric(19,2),
    override_retail_price_gst numeric(19,2),
    retail_price numeric(19,2),
    retail_price_gst numeric(19,2),
    created timestamp without time zone DEFAULT now() NOT NULL,
    product_gnm_sk integer
);

--
-- Name: store_stock_history; Type: TABLE; Schema: public; Owner: -
--
CREATE TABLE store_stock_history (
    sk integer NOT NULL,
    store_sk integer NOT NULL,
    nk character varying(100) NOT NULL,
    takeon_storeproduct_sk integer,
    product_gnm_sk integer, 
    total integer NOT NULL,
    prior_total integer NOT NULL,
    employee_sk integer,
    source_staged timestamp without time zone NOT NULL,
    created timestamp without time zone DEFAULT now()
);

--
-- Name: store_stockmovement; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE store_stockmovement (
    sk integer NOT NULL,
    store_sk integer NOT NULL,
    source_id character varying(100) NOT NULL,
    takeon_storeproduct_sk integer NOT NULL,
    adjustmentdate timestamp without time zone,
    stock_price_history_sk integer NOT NULL,
    adjustment integer NOT NULL,
    -- adjustmenttype character varying(20) NOT NULL,
    reason character varying(20) NOT NULL,
    invoice_sk integer,
    customer_sk integer,
    avg_cost_price numeric(19,2),
    shipping_ref character varying(100),
    shipping_operator character varying(100),
    comments character varying(500),
    employee_sk integer,
    created timestamp without time zone DEFAULT now() NOT NULL,
    adjustmentdate_sk integer,
    product_gnm_sk integer,
    adjustment_type_sk integer DEFAULT 0 NOT NULL,
    actual_cost numeric(19,2),
    actual_cost_value numeric(19,2),
    moving_quantity numeric(19,2),
    moving_actual_cost_value numeric(19,2),
    moving_cost_per_unit numeric(19,2)
);


--
-- Name: d_date; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE d_date (
    sk integer NOT NULL,
    date_dim_id integer NOT NULL,
    date_actual date NOT NULL,
    epoch bigint NOT NULL,
    day_suffix character varying(4) NOT NULL,
    day_name character varying(9) NOT NULL,
    day_of_week integer NOT NULL,
    day_of_month integer NOT NULL,
    day_of_quarter integer NOT NULL,
    day_of_year integer NOT NULL,
    week_of_month integer NOT NULL,
    week_of_year integer NOT NULL,
    week_of_year_iso character(10) NOT NULL,
    month_actual integer NOT NULL,
    month_name character varying(9) NOT NULL,
    month_name_abbreviated character(3) NOT NULL,
    year_month_name character varying(8) NOT NULL,
    quarter_actual integer NOT NULL,
    quarter_name character varying(9) NOT NULL,
    year_calendar integer NOT NULL,
    year_calendar_name character varying(7) NOT NULL,
    year_financial integer NOT NULL,
    year_financial_name character varying(7) NOT NULL,
    first_day_of_week date NOT NULL,
    last_day_of_week date NOT NULL,
    first_day_of_month date NOT NULL,
    last_day_of_month date NOT NULL,
    first_day_of_quarter date NOT NULL,
    last_day_of_quarter date NOT NULL,
    first_day_of_year date NOT NULL,
    last_day_of_year date NOT NULL,
    yyyymm character(6) NOT NULL,
    yyyyq character(5) NOT NULL,
    weekend_indr boolean NOT NULL
);


--
-- Name: d_date_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE d_date_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: d_date_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE d_date_sk_seq OWNED BY d_date.sk;


--
-- Name: dailysnapshot_cost; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE dailysnapshot_cost (
    createddate_sk integer,
    today date,
    cost numeric
);


--
-- Name: dailysnapshot_cost_stores; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE dailysnapshot_cost_stores (
    storeid character varying(12),
    today date,
    cost numeric
);


--
-- Name: dailysnapshot_revenue; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE dailysnapshot_revenue (
    createddate_sk integer,
    today date,
    revenue numeric
);


--
-- Name: dailysnapshot_revenue_stores; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE dailysnapshot_revenue_stores (
    storeid character varying(12),
    today date,
    revenue numeric
);



--
-- Name: gnm_invoice_orders; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE gnm_invoice_orders (
    orderid character varying(100),
    customer_sk integer,
    order_date timestamp without time zone,
    jobid character varying(100),
    job_date timestamp without time zone,
    product_sk integer,
    supplier_sk integer,
    productid character varying(500),
    type character varying(100)
);


--
-- Name: inventory_snapshot; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE inventory_snapshot (
    product_gnm_sk integer,
    store_sk integer,
    date_sk integer,
    accumalitive_adjustment bigint,
    accumalitive_actual_value numeric,
    total_receipt bigint,
    total_sale bigint,
    total_return bigint,
    total_writeoff bigint,
    total_theft bigint,
    total_receipt_actual_value numeric,
    total_sale_actual_value numeric,
    total_return_actual_value numeric,
    total_writeoff_actual_value numeric,
    total_theft_actual_value numeric,
    takeon_storeproduct_sk integer
);


--
-- Name: invoice_dailysnapshot; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE invoice_dailysnapshot (
    createddate_sk integer,
    today date,
    revenue numeric
);


--
-- Name: invoice_dailysnapshot_all; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE invoice_dailysnapshot_all (
    createddate_sk integer,
    today date,
    today_revenue numeric,
    yesterday_revenue numeric,
    today_revenue_py numeric,
    yesterday_revenue_py numeric,
    day_lastweek_revenue numeric,
    day_lastweek_revenue_py numeric
);


--
-- Name: invoice_order_job; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE invoice_order_job (
    sk integer NOT NULL,
    store_sk integer NOT NULL,
    invoice_sk integer NOT NULL,
    order_id_nk character varying(100),
    job_id_nk character varying(100),
    created timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: invoice_order_job_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE invoice_order_job_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: invoice_order_job_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE invoice_order_job_sk_seq OWNED BY invoice_order_job.sk;


--
-- Name: invoice_orders_reconcile; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE invoice_orders_reconcile (
    pmsorderid text,
    invoicesk integer,
    framesk integer,
    right_lenssk integer,
    left_lenssk integer,
    storesk integer,
    pms_invoicenum character varying(100),
    orderid character varying(100),
    customer_sk integer,
    order_date timestamp without time zone,
    jobid character varying(100),
    job_date timestamp without time zone,
    product_sk integer,
    supplier_sk integer,
    productid character varying(500),
    type character varying(100)
);


--
-- Name: invoice_orders_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE invoice_orders_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: invoice_orders_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE invoice_orders_sk_seq OWNED BY invoice_orders.sk;

--
-- Name: job_history; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE job_history (
    sk integer NOT NULL,
    source_id character varying(100),
    order_id character varying(100) NOT NULL,
    order_sk integer NOT NULL,
    job_type character varying(100) NOT NULL,
    product_sk integer NOT NULL,
    supplier_sk integer NOT NULL,
    queue character varying(100),
    status character varying(100),
    source_created timestamp without time zone NOT NULL,
    source_modified timestamp without time zone NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: job_history_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE job_history_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: job_history_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE job_history_sk_seq OWNED BY job_history.sk;


--
-- Name: last_job; Type: VIEW; Schema: public; Owner: -
--

--
-- Name: order_history_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE order_history_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: order_history_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE order_history_sk_seq OWNED BY order_history.sk;


--
-- Name: pms_invoice_orders; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE pms_invoice_orders (
    orderid text,
    invoicesk integer,
    framesk integer,
    right_lenssk integer,
    left_lenssk integer,
    storesk integer,
    pms_invoicenum character varying(100)
);


--
-- Name: prod_gnm; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE prod_gnm (
    storeid character varying(12),
    description character varying(500),
    product_id character varying(255),
    count bigint
);


--
-- Name: prod_multi_store; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE prod_multi_store (
    pname text,
    count bigint
);


--
-- Name: product_gnm; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE product_gnm (
    sk integer NOT NULL,
    productid character varying(500),
    type character varying(100),
    datecreated timestamp without time zone,
    name character varying(500),
    description character varying(500),
    retailprice numeric(19,4),
    wholesaleprice numeric(19,4),
    modified timestamp without time zone,
    last_purchase timestamp without time zone,
    last_sale timestamp without time zone,
    quantity integer,
    frame_model character varying(100) DEFAULT ''::character varying,
    frame_colour character varying(100) DEFAULT ''::character varying,
    frame_brand character varying(100) DEFAULT ''::character varying,
    frame_temple numeric(8,2) DEFAULT 0,
    frame_bridge numeric(8,2) DEFAULT 0,
    frame_eye_size numeric(8,2) DEFAULT 0,
    frame_depth numeric(8,2) DEFAULT 0,
    frame_material character varying(100) DEFAULT ''::character varying,
    lens_type character varying(100) DEFAULT ''::character varying,
    lens_refractive_index numeric(19,4) DEFAULT 0,
    lens_stock_grind character varying(10) DEFAULT ''::character varying,
    lens_size numeric(4,2) DEFAULT 0,
    lens_segment_size numeric(4,2) DEFAULT 0,
    internal_sku character varying(100),
    distributor character varying(100),
    created timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: product_gnm_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE product_gnm_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: product_gnm_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE product_gnm_sk_seq OWNED BY product_gnm.sk;


--
-- Name: stock_price_history_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE stock_price_history_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: stock_price_history_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE stock_price_history_sk_seq OWNED BY stock_price_history.sk;



--
-- Name: store_stock_history_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE store_stock_history_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: store_stock_history_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE store_stock_history_sk_seq OWNED BY store_stock_history.sk;


--
-- Name: store_stockmovement_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE store_stockmovement_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: store_stockmovement_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE store_stockmovement_sk_seq OWNED BY store_stockmovement.sk;

--
-- Name: takeon_product; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE takeon_product (
    sk integer NOT NULL,
    store_sk integer NOT NULL,
    supplier_sk integer NOT NULL,
    source_id character varying(100) NOT NULL,
    supplier_sku character varying(100) NOT NULL,
    name character varying(500) NOT NULL,
    description character varying(500) NOT NULL,
    model character varying(500) NOT NULL,
    brand character varying(500) NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: takeon_product_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE takeon_product_sk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: takeon_product_sk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE takeon_product_sk_seq OWNED BY takeon_product.sk;


--
-- Name: tmp_storeprod1; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE tmp_storeprod1 (
    sk integer,
    pname text,
    productid character varying(500),
    storeid character varying(12),
    name character varying(500),
    description character varying(500),
    internal_sku character varying(100)
);


--
-- Name: unmatched_orders; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE unmatched_orders (
    sk integer,
    store_sk integer,
    customer_sk integer,
    source_id character varying(100),
    order_type character varying(100),
    queue character varying(100),
    status character varying(100),
    source_created timestamp without time zone,
    source_modified timestamp without time zone,
    created timestamp without time zone
);


   