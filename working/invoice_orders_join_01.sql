
select fn_create_viewbydate('store_stockmovement', 'adjustmentdate');
select fn_create_viewbydate('stock_price_history', 'created');
select fn_create_viewbydate('invoice','createddate');
select fn_create_viewbydate_string('invoiceitem','createddate','invoicesk');
select fn_create_viewbydate('order_history','source_created');
select fn_create_viewbydate('job_history','source_created');


CREATE OR REPLACE VIEW vbyd_invoiceitem as select date_part('year',sm.createddate)as year,date_part('doy',sm.createddate) as doy,date_part('month',sm.createddate )as month,date_part('dow',sm.createddate) as dow,date_part('day',sm.createddate) as day, invoicesk  
from invoiceitem sm 
group by date_part('year',sm.createddate),date_part('doy',sm.createddate),date_part('month',sm.createddate),date_part('dow',sm.createddate),date_part('day',sm.createddate),invoicesk

----- Join the invoices to the Order

select * from public.product_gnm

select * from public.invoiceitem ii 
inner join storeproduct sp on sp.sk = ii.productsk


select count(*) from public.order_history oh 
inner join public.job_history jh on jh.order_sk = oh.sk
--inner join public.product_gnm pg on pg.sk = jh.product_sk
inner join public.storeproduct sp on sp.product_gnm_sk = jh.product_sk
left join invoiceitem ii on ii.customersk = oh.customer_sk and ii.productsk = sp.sk

where ii.invoicesk is not null


select count(*) 
from public.order_history oh 
	inner join public.job_history jh on jh.order_sk = oh.sk
	inner join vbyd_job_history vjh on vjh.sk = jh.sk
	inner join public.storeproduct sp on sp.product_gnm_sk = jh.product_sk
	left join (select ii.*,year,day,month,dow,doy,day from  invoiceitem ii
	inner join vbyd_invoiceitem vii on vii.invoicesk = ii.invoicesk) ii on ii.customersk = oh.customer_sk and ii.productsk = sp.sk  and ii.storesk = oh.store_sk
where 
	ii."year" = vjh.year and ii.doy = vjh.doy and vjh.year = 2017
	
------------------------------------------------------------------------------
select ii.doy,vjh.doy,ii.dow,vjh.dow,ii.invoicesk,vjh.sk
from public.order_history oh 
	inner join public.job_history jh on jh.order_sk = oh.sk
	inner join vbyd_job_history vjh on vjh.sk = jh.sk
	inner join public.storeproduct sp on sp.product_gnm_sk = jh.product_sk
	left join (select ii.*,year,day,month,dow,doy,day from  invoiceitem ii
	inner join vbyd_invoiceitem vii on vii.invoicesk = ii.invoicesk) ii on ii.customersk = oh.customer_sk and ii.productsk = sp.sk and ii.storesk = oh.store_sk
where 
	ii."year" = vjh.year and ii.doy = vjh.doy and vjh.year = 2017
	
	
----------------------------------------------------------------------------
	
select ii.doy,vjh.doy,ii.dow,vjh.dow,ii.invoicesk,vjh.sk
from public.order_history oh 
	inner join public.job_history jh on jh.order_sk = oh.sk
	inner join vbyd_job_history vjh on vjh.sk = jh.sk
	inner join public.storeproduct sp on sp.product_gnm_sk = jh.product_sk
	left join (select ii.*,year,day,month,dow,doy from  invoiceitem ii
	inner join vbyd_invoiceitem vii on vii.invoicesk = ii.invoicesk) ii on ii.customersk = oh.customer_sk and ii.productsk = sp.sk and ii.storesk = oh.store_sk
where 
	ii."year" = vjh.year and ii.doy <> vjh.doy and vjh.year = 2017
--------------------------------------------------------------------------------
-- populate invoice_orders

insert into invoice_orders(store_sk,invoice_sk,order_sk) 
select oh.store_sk,ii.invoicesk,oh.sk
from public.order_history oh 
	inner join public.job_history jh on jh.order_sk = oh.sk
	inner join vbyd_job_history vjh on vjh.sk = jh.sk
	inner join public.storeproduct sp on sp.product_gnm_sk = jh.product_sk
	left join (select ii.*,year,day,month,dow,doy from  invoiceitem ii
	inner join vbyd_invoiceitem vii on vii.invoicesk = ii.invoicesk) ii on ii.customersk = oh.customer_sk and ii.productsk = sp.sk and ii.storesk = oh.store_sk
where 
	ii."year" = vjh.year and ii.doy <> vjh.doy and vjh.year = 2017
	
	

	
	
-- how many invoice orders
select * from invoice_orders
select count(*)	from (select  store_sk,invoice_sk,order_sk, count(*) from invoice_orders group by store_sk,invoice_sk,order_sk) x
	



-----------------------------------------------------------------------------------
(select * from  invoiceitem ii
inner join vbyd_invoiceitem vii on vii.invoicesk = ii.invoicesk) ii

select count(*) from public.order_history

select * from public.invoiceitem ii 
left join public.storeproduct sp on sp.sk = ii.productsk

select count(*) from public.invoiceitem ii 
left join public.storeproduct sp on sp.sk = ii.productsk
left join public.job_history jh on jh.product_sk = sp.product_gnm_sk
where 
	sp.sk is not null and jh.sk is not null 
	

-- vbyd_invoiceitem

-- 
select order_type,job_type, count(*) from public.order_history oh 
inner join job_history jh on jh.order_sk = oh.sk
inner join 
group by oh.order_type,jh.job_type order by count(*) desc

select job_type, count(*) from public.job_history oh group by oh.job_type order by count(*) desc;


--------------------------------------------------------------------------------------------------------
-- investigate missing 
-- 
select count(*) from store_stockmovement sm where invoice_sk <> 0



--- calculate price average -----

-- formula

-- product_cost
select count(*) from store_stockmovement where adjustment > 0 and product_gnm_sk > 0

-- stock movements by optomertrist

-- non gnm stock
select vss.year, vss.month, vss.doy,s.name,count(*) from store_stockmovement ss 
inner join vbyd_store_stockmovement vss on vss.sk = ss.sk
inner join public.store s on s.sk = ss.store_sk
where adjustment > 0 and product_gnm_sk = 0 and vss.year = 2017
group by vss.year, vss.month, vss.doy, s.name
order by vss.year, vss.month desc, vss.doy, s.name

--  gnm stock
select vss.year, vss.month, vss.doy,s.name,count(*) from store_stockmovement ss 
inner join vbyd_store_stockmovement vss on vss.sk = ss.sk
inner join public.store s on s.sk = ss.store_sk
where adjustment > 0 and product_gnm_sk > 0 and vss.year = 2017
group by vss.year, vss.month, vss.doy, s.name
order by vss.year, vss.month desc, vss.doy, s.name


-- umatched orders
select count(*) from order_history oh left join invoice_orders io on io.order_sk = oh.sk
where io.sk is  null

select count(*) 

-- 
select count(*) from invoice ii left join invoice_orders io on io.invoice_sk = ii.sk
where io.sk is  null

select * from order_history

SET search_path = public;

--- valid orders range by store
select s.sk as store_sk ,s.name ,order_type, min(source_created) as first_order_date,max(source_created) as last_order_date,count(*) as total 
into store_orders_period
from order_history oh 
	inner join "store" s on s.sk = oh.store_sk
	where date_part('year',source_created) >= 2017
group by s.sk,name,order_type order by name,min(source_created),s.name asc


select sum(total) from store_orders_period



select date_part('dow', '2017-09-15 12:00:00'::timestamp),
'2017-09-15 12:00:00'::timestamp  + interval '3 day',
'2017-09-15 12:00:00'::timestamp  - interval '1 day'

select within_dow_range ('2017-08-19 12:00:00'::timestamp,'2017-09-19 12:00:00'::timestamp) 

-- using near date function

select oh.store_sk,ii.invoicesk,oh.sk
from public.order_history oh 
	inner join public.job_history jh on jh.order_sk = oh.sk
	inner join public.storeproduct sp on sp.product_gnm_sk = jh.product_sk
	left join invoiceitem ii on ii.customersk = oh.customer_sk and ii.productsk = sp.sk and ii.storesk = oh.store_sk
where 
	within_dow_range(oh.source_created,ii.createddate)
	
select count(*)
from public.order_history oh 
	inner join public.job_history jh on jh.order_sk = oh.sk
	inner join public.storeproduct sp on sp.product_gnm_sk = jh.product_sk
	left join invoiceitem ii on ii.customersk = oh.customer_sk and ii.productsk = sp.sk and ii.storesk = oh.store_sk
where 
	ii.invoicesk is not null
	and not within_dow_range(oh.source_created,ii.createddate,30)
	
select *
from public.order_history oh 
	inner join public.job_history jh on jh.order_sk = oh.sk
	inner join public.storeproduct sp on sp.product_gnm_sk = jh.product_sk
	left join invoiceitem ii on ii.customersk = oh.customer_sk and ii.productsk = sp.sk and ii.storesk = oh.store_sk
where 
	ii.invoicesk is not null
	and not within_dow_range(oh.source_created,ii.createddate,30)

	
select count(*) from gnm.order

select count(*) from gnm."order" where store_id = 3

2446-004

select * from gnm."store"

select * from public.store where storeid = '2446-004'
 
select * from public.invoice_orders where store_sk = 3

select count(*) from public.invoice iv 
inner join public.invoiceitem ii on ii.invoicesk = iv.sk
where ii.storesk = 3


select * from public.store_stockmovment

SELECT sk, invoicesk, framesk, right_lenssk, left_lenssk, storesk, createddate, orderdate, receiveddate, pickupdate, modified, spectaclejobid, pms_invoicenum
FROM public.spectaclejob;


SELECT sk, store_sk, source_id, takeon_storeproduct_sk, adjustmentdate, stock_price_history_sk, adjustment, adjustmenttype, reason, invoice_sk,
customer_sk, avg_cost_price, shipping_ref, shipping_operator, comments, employee_sk, created, adjustmentdate_sk, product_gnm_sk, adjustment_type_sk, 
actual_cost, actual_cost_value, moving_quantity, moving_actual_cost_value, moving_cost_per_unit
FROM public.store_stockmovement sm
where 
	sm.invoice_sk not in (select invoice_sk from invoice_orders)
	

	select * from spectaclejob sp 
	
	
	drop table public.pms_invoice_orders;
	
select substring(spectaclejobid,10, length(spectaclejobid)) as orderid, invoicesk, framesk,right_lenssk,left_lenssk,storesk,pms_invoicenum 
into pms_invoice_orders
from spectaclejob sp 

select * from pms_invoice_orders
select * from order_history


select count(*) from pms_invoice_orders po where orderid in (select source_id from order_history)

select count(*) from pms_invoice_orders po where orderid in (select source_id from order_history)

select oh.* into unmatched_orders from order_history oh 
left join  pms_invoice_orders io on io.orderid = oh.source_id
where io.orderid is null

select * from unmatched_orders

-- create some indexes to improve performance

create index idx_orderid_order_history on order_history(source_id);

create index idx_orderid_pms_invoice_orders on pms_invoice_orders(orderid);


create view last_job as
select jh.* from public.job_history jh
left join 
(select source_id,max(sk) as max_sk,max(source_modified) as max_date, count(*) from public.job_history jh group by source_id) x
on x.max_sk = jh.sk

where x.max_date <> jh.source_modified
order by source_id


1. resestablish flow 
2. spectaclejob table - order & invoice & product & store & customer
3. matching view from gnm

drop table gnm_invoice_orders;

select oh.source_id as orderid, customer_sk, oh.source_created as order_date, jh.source_id as jobid, 
jh.source_created as job_date, jh.product_sk,jh.supplier_sk,pg.productid,pg.type
into gnm_invoice_orders
from public.order_history oh 
	inner join public.last_job jh on jh.order_sk = oh.sk
	inner join public.product_gnm pg on pg.sk = jh.product_sk and pg.type = 'frame'

	
select count(*) from gnm_invoice_orders
select * from pms_invoice_orders



drop table invoice_orders_reconcile

select po.orderid as pmsorderid, po.invoicesk, po.framesk, po.right_lenssk, po.left_lenssk, po.storesk, po.pms_invoicenum,gio.*
into invoice_orders_reconcile
from pms_invoice_orders po 
left join gnm_invoice_orders gio on gio.orderid = po.orderid
 
select count(*) from invoice_orders_reconcile 
where orderid is not null and invoicesk is null 

select ii.*,sp.*,pg.*,ior.* from invoice_orders_reconcile ior 
left join invoiceitem ii on ii.invoicesk = ior.invoicesk
left join storeproduct sp on sp.sk = ii.productsk
left join product_gnm pg on pg.sk = sp.product_gnm_sk
where ii.invoicesk is not null and ior.type = 'frame' and sp."type" = 'Frame' and pg.sk is not null 
and pg.sk <> ior.product_sk

--- order ids in hub and not in pms

--- subset of orders without/with invoice within this set


--- order ids in pms and not in hub
--- subset of orders without/with invoice within this set


--- orders in hub & pms without invoice


--- invoices without orders in hub


--- product id mismatch between pms & hub
select ior.orderid,ior.product_sk from invoice_orders_reconcile ior 
left join invoiceitem ii on ii.invoicesk = ior.invoicesk
left join storeproduct sp on sp.sk = ii.productsk
left join product_gnm pg on pg.sk = sp.product_gnm_sk
where ii.invoicesk is not null and ior.type = 'frame' and sp."type" = 'Frame' and pg.sk is not null 
and pg.sk <> ior.product_sk


--- Failure Buckets


-- scenario 1.
--- 

SET search_path = public;
 
select * from spectaclejob

select * from invoiceitem where invoicesk is not null;

drop view view_invoice_item_cogs;

create view view_invoice_item_cogs as
SELECT invoicesk, pos, productsk, storesk, employeesk, 
serviceemployeesk, customersk, createddate, quantity, 
amount_incl_gst, modified, amount_gst, discount_incl_gst, discount_gst, 
cost_incl_gst, cost_gst, description, payeesk, invoiceitemid, return_incl_gst, 
return_gst, bulkbillref, createddate_sk, 
pms_cost, gnm_current_cost, takeon_avg_cost, order_type, order_id
FROM public.invoiceitem ii
left join 

;

select count(*) from gnm_invoice_orders
select * from gnm_invoice_orders
select * from invoice_orders
select count(*) from pms_invoice_orders

select * from pms_invoice_orders

select * from store_stockmovement sm 
where product_gnm_sk is not null and product_gnm_sk <> 0
where avg_cost_price is not null

----------------------------------------------------------------------------------------

select * from invoiceitem ii 
left join invoice_orders io on io.invoice_sk = ii.invoicesk
left join last_job lj on lj.order_sk = io.order_sk
left join order_history oh on oh.sk = io.order_sk
left join product_gnm pg on pg.sk = lj.product_sk and pg.type = frame

select * from last_job lj 
left join product_gnm pg on pg.sk = product_sk

INSERT INTO public.product_gnm(
    sk, storeid, productid, type, datecreated, name, description, 
    retailprice, wholesaleprice, modified, last_purchase, last_sale, 
    quantity, frame_model, frame_colour, frame_brand, frame_temple, 
    frame_bridge, frame_eye_size, frame_depth, frame_material, lens_type, 
    lens_refractive_index, lens_stock_grind, lens_size, lens_segment_size, 
    internal_sku, distributor)
VALUES (0, '', '', '', now(), '', '', 
    0, 0, now(), now(), now(), 
    0, '', '', '', 0, 
    0, 0, 0, '', '', 
    0, '', 0, 0, 
    '', '');

INSERT INTO public.product_gnm( sk, productid, "type", "name", description)
VALUES (0, '', '', '', '');


  select * from  temp_store_stockmovement
  select * from storeproduct
  
select * from temp_store_stockmovement tss inner join product_gnm pg on pg.sk = tss.product_gnm_sk


    CONSTRAINT store_stockmovement_product_gnm_sk_fkey FOREIGN KEY (product_gnm_sk)
        REFERENCES public.storeproduct (sk) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO action
        
alter table store_stockmovement drop constraint store_stockmovement_product_gnm_sk_fkey;


-- Invoice item view

create view invoiceitem_cogs_view as 
SELECT invoicesk, pos, productsk, storesk, employeesk, serviceemployeesk, customersk, createddate, 
ii.quantity, amount_incl_gst,ii.modified, amount_gst, discount_incl_gst, discount_gst, cost_incl_gst, 
cost_gst, ii.description, payeesk, invoiceitemid, return_incl_gst, return_gst, bulkbillref, createddate_sk, 
pms_cost, gnm_current_cost, takeon_avg_cost, ssm.avg_cost_price as  avg_cost, oh.order_type, oh.source_id as order_id
	FROM public.invoiceitem ii
	left join store_stockmovement ssm on ssm.invoice_sk = ii.invoicesk -- and ssm.product_gnm_sk = ii.productsk
	left join storeproduct sp on ii.productsk = sp.sk
	left join invoice_orders io on io.invoice_sk = ii.invoicesk
	left join order_history oh on oh.sk = io.order_sk
	left join last_job lj on lj.order_sk = oh.sk
	
-- where
--	sp.product_gnm_sk = ssm.product_gnm_sk
	
	cdcd
	
	
	select count(*) from gnm_invoice_orders
	
	select count(*) from invoice_orders
	select * from invoice_orders
	
	select * from order_history
	
	select * from invoiceitem_cogs_view where order_id is not null
	
	
	
select * from store_stockmovement where adjustmenttype in ('Receipt') and avg_cost_price is not null


select * from store_stockmovement ssm where avg_cost_price is not null

select count(*) from store_stockmovement ssm where ssm.product_gnm_sk is not null 

select * from product_price_history pph inner join product_gnm ppg on ppg.productid = pph.product_id;

          select distinct upper("REASON") as adjustment_typeid, "REASON" as othername FROM stage.today_gnm_sunix_vrestock r
          left outer join public.adjustment_type a on upper("REASON") = a.adjustment_typeid
          where a.sk is null
          

select * from  stage.today_gnm_sunix_vrestock r

select * from adjustment_type 



insert into adjustment_type(adjustment_typeid,"name",othername) 
values ('RECEIPT','Receipt','Receipt')

select adjustment_type_sk,adjustmenttype,count(*) from store_stockmovement group by adjustment_type_sk,adjustmenttype order by count(*) desc

select * from  adjustment_type adjt right join  
(select adjustment_type_sk,adjustmenttype,count(*) from store_stockmovement group by adjustment_type_sk,adjustmenttype) x 
on x.adjustmenttype = adjt.adjustment_typeid

select adjustment_typeid, count(*) 
from adjustment_type
where upper(adjustment_typeid) like '%R%'
 group by adjustment_typeid 

select adjustment_typeid,othername from (
            SELECT upper("CODE") as adjustment_typeid,"DESCPTN" as othername,row_number() over (partition by upper("CODE")) as r
              FROM stage.today_gnm_sunix_vcode
              where "GROUP"='AJ'
            ) T where r = 1
            

            select distinct upper("REASON") as adjustment_typeid, "REASON" as othername FROM stage.today_gnm_sunix_vrestock r

insert into adjustment_type(adjustment_typeid,"name",othername) 
select upper(adjustmenttype),upper(adjustmenttype),adjustmenttype from store_stockmovement ssm
left join adjustment_type adjt on adjt.adjustment_typeid = upper(adjustmenttype) where adjustment_typeid is null
group by adjustmenttype


select upper(adjustmenttype), adjt.adjustment_typeid , count(*) from store_stockmovement ssm
left join adjustment_type adjt on adjt.adjustment_typeid = upper(adjustmenttype) 
-- where adjustment_typeid is null
group by adjustmenttype, adjt.adjustment_typeid 



select * from adjustment_type where adjustment_typeid = 'RECEIPT'

select  * from store_stockmovement  ssm 
inner join adjustment_type adjt on adjt.sk = ssm.adjustment_type_sk
where adjt.othername = 'Receipt'

begin ;
update  store_stockmovement set adjustment_type_sk = 311 where adjustmenttype like 'Receipt'


commit;

select * from adjustment_type
update adjustment_type set name = 'Receipt' where name = 'RECEIPT'

select count(*) from store_stockmovement ssm where avg_cost_price is not null



select adjustment_typeid,othername from (
            SELECT upper("CODE") as adjustment_typeid,"DESCPTN" as othername,row_number() over (partition by upper("CODE")) as r
              FROM stage.today_gnm_sunix_vcode
              where "GROUP"='AJ'
            ) T where r = 1


--- top issues 


            

            
            

