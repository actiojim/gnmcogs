

SET search_path = public, pg_catalog;
/*
drop table if exists store_stock_history_adj cascade;

CREATE TABLE store_stock_history_adj (
    sk serial NOT NULL,
    store_sk integer NOT NULL,
    nk character varying(100),
    storeproduct_sk integer REFERENCES storeproduct(sk),
    total integer NOT NULL,
    prior_total integer NOT NULL,
    employee_sk integer,
    source_staged timestamp without time zone NOT NULL,
    created timestamp without time zone DEFAULT now(),
    product_gnm_sk integer REFERENCES product_gnm(sk),
    primary key(sk)
);
*/
-------------------------------------------------------------------------
--- initial -------------------------------------------------------------

-- truncate table public.store_stock_history_adj;


--insert into public.store_stock_history_adj (store_sk,nk,storeproduct_sk,product_gnm_sk,total,prior_total,source_staged)

/*
select make_timestamp(2017,1,1,0,0,0.0);
select make_timestamp(2013, 7, 15, 8, 15, 23.5)

select yr::integer,mn::integer,dy::integer,make_timestamp(yr::integer,mn::integer,dy::integer,0,0,0.0) from (
select  
extract(year from adjustmentdate) yr,
extract(month from adjustmentdate) mn,
extract(day from adjustmentdate) dy
from store_stockmovement) x

select * from d_date

select sm.adjustmentdate,dd.date_actual from store_stockmovement sm 
inner join d_date dd on dd.sk = sm.adjustmentdate_sk
*/

--

--- 
select * from store_stock_history_adj;

select * from daily_adjustments_sum;

select store_sk, storeproduct_sk, coalesce(product_gnm_sk,0) product_gnm_sk, 
dd.date_actual, sum(adjustment) dailytotal 
into daily_adjustments_sum
from store_stockmovement sm
inner join d_date dd on dd.sk = sm.adjustmentdate_sk
where store_sk is not null  --product_gnm_sk is not null and product_gnm_sk != 0
group by store_sk, storeproduct_sk, product_gnm_sk, dd.date_actual 
-- order by sum(adjustment) desc;


--------------------working space----------------------------------------------------

--- trying to do cumulative daily totals
with recursive prior_total_adjustments as (
	select store_sk, storeproduct_sk, coalesce(product_gnm_sk,0) as product_gnm_sk , sum(adjustment) as total, dd.date_actual, sm.adjustmentdate_sk, count(*) c
	from store_stockmovement sm
	inner join d_date dd on dd.sk = sm.adjustmentdate_sk
	where store_sk is not null  and date_actual = '2016-11-30'
	group by store_sk, storeproduct_sk, product_gnm_sk,  dd.date_actual ,sm.adjustmentdate_sk
union all	
	select sm.store_sk, sm.storeproduct_sk, coalesce(sm.product_gnm_sk,0) as product_gnm_sk , adjustment +  as total, dd.date_actual,sm.adjustmentdate_sk, count(*) c
	from prior_total_adjustments psm 
	inner join store_stockmovement sm on sm.store_sk = psm.store_sk and sm.storeproduct_sk = psm.storeproduct_sk --and psm.adjustmentdate_sk = sm.date_actual
	inner join d_date dd on dd.sk = sm.adjustmentdate_sk
	where sm.store_sk is not null  and dd.date_actual = psm.date_actual + 1
	group by sm.store_sk, sm.storeproduct_sk, sm.product_gnm_sk, dd.date_actual ,sm.adjustmentdate_sk
)
select * from prior_total_adjustments order by date_actual

select * from d_date


select * from store_stockmovement sm
inner join 
(select ad."name",store_sk,,sm.adjustmentdate_sk,dd.date_actual ,count(*) from store_stockmovement sm 
inner join d_date dd on dd.sk = sm.adjustmentdate_sk
inner join adjustment_type ad on sm.adjustment_type_sk = ad.sk
where ad.name = 'Stocktake' -- and store_sk =1
group by store_sk, ad.name,sm.adjustmentdate_sk, dd.date_actual ) x on x.store_sk = 
order by store_sk, date_actual

























/*
--
-- Name: store_stock_history_adj_sk_seq; Type: SEQUENCE; Schema: public; Owner: -
--

drop view if exists view_store_stock_history_takeon;
drop view if exists view_store_stock_history_gnm;


create or replace view view_store_stock_history_takeon as
select st.*, s.name as storename ,s.chainid,s.store_nk from
(
select 
	store_sk, 
	storeproduct_sk, 
	ssh.product_gnm_sk, 
	total, prior_total, 
	employee_sk, 
	source_staged, 
	ssh.created,
	sp.productid, sp.type,sp.name,sp.description,sp.frame_model,sp.frame_colour,sp.frame_brand
from  public.store_stock_history_adj ssh 
	inner join  ( select nk as last_nk, store_sk as last_store_sk, storeproduct_sk as last_storeproduct_sk,max(source_staged) as last_source_staged 
   	from store_stock_history_adj ssh group by nk,store_sk,storeproduct_sk ) ls 
   		on ls.last_store_sk = ssh.store_sk and ls.last_storeproduct_sk = ssh.storeproduct_sk and ssh.source_staged = ls.last_source_staged
   	left join storeproduct sp on sp.sk = ssh.storeproduct_sk
) st
left join "store" s on s.sk = st.store_sk;


create or replace view view_store_stock_history_gnm as
select st.*, s.name as storename ,s.chainid,s.store_nk from
(
select  
	store_sk, 
	storeproduct_sk, 
	ssh.product_gnm_sk, 
	total, 
	prior_total, 
	employee_sk, 
	source_staged, 
	ssh.created,
	pg.productid, pg.type,pg.name,pg.description,pg.frame_model,pg.frame_colour,pg.frame_brand
from  public.store_stock_history_adj ssh 
	inner join ( select nk as last_nk, store_sk as last_store_sk, product_gnm_sk as last_product_gnm_sk,max(source_staged) as last_source_staged 
   	from store_stock_history_adj ssh group by nk,store_sk,product_gnm_sk ) ls 
   		on ls.last_store_sk = ssh.store_sk and ls.last_product_gnm_sk = ssh.product_gnm_sk and ssh.source_staged = ls.last_source_staged
   	left join product_gnm pg on pg.sk = ssh.product_gnm_sk
) st
left join "store" s on s.sk = st.store_sk;

*/



