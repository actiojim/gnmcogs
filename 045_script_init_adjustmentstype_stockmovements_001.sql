-- truncate adjustment_type
--- commit

SET search_path = public, pg_catalog;

insert into adjustment_type(adjustment_typeid,"name",othername) 
values ('RECEIPT','Receipt','Receipt');

insert into adjustment_type(adjustment_typeid,"name",othername) 
values ('SALE','Sale','Sale');

insert into adjustment_type(adjustment_typeid,"name",othername) 
values ('RETURN','Return','Return');

insert into adjustment_type(adjustment_typeid,"name",othername) 
values ('STOCKTAKE','Stocktake','Stocktake');

insert into adjustment_type(adjustment_typeid,"name",othername) 
values ('TRANSFEROUT','Transfer Out','Transfer Out');

insert into adjustment_type(adjustment_typeid,"name",othername) 
values ('TRANSFERIN','Transfer In','Transfer In');

insert into adjustment_type(adjustment_typeid,"name",othername) 
values ('RESET','Stocktake Reset','Stocktake Reset');

insert into adjustment_type(adjustment_typeid,"name",othername) 
values ('STOCKTAKE_IN','Receipt','Stocktake Receipt');


insert into adjustment_type  (adjustment_typeid,name,othername) values ('Restock', 'Receipt', 'Receipt');
insert into adjustment_type  (adjustment_typeid,name,othername) values ('RESTOCK' , 'Receipt', 'Receipt');
insert into adjustment_type  (adjustment_typeid,name,othername) values ('RE' , 'Return', 'Return');
insert into adjustment_type  (adjustment_typeid,name,othername) values ('CO' , 'Return','Return');
insert into adjustment_type  (adjustment_typeid,name,othername) values ('ST' , 'Stocktake','Stocktake');
insert into adjustment_type  (adjustment_typeid,name,othername) values ('CREDIT' , 'Return','Return');
insert into adjustment_type  (adjustment_typeid,name,othername) values ('REC' , 'Return','Return');
insert into adjustment_type  (adjustment_typeid,name,othername) values ('RTS' , 'Return','Return');
insert into adjustment_type  (adjustment_typeid,name,othername) values (upper('Sale Cancel') , 'Sale','Sale');
insert into adjustment_type  (adjustment_typeid,name,othername) values (upper('Cancel Sale') , 'Sale','Sale');
insert into adjustment_type  (adjustment_typeid,name,othername) values (upper('Sale - CO:DE') , 'Sale', 'Sale');
insert into adjustment_type  (adjustment_typeid,name,othername) values (upper('Sale - SA:DS') , 'Sale','Sale');
insert into adjustment_type  (adjustment_typeid,name,othername) values (upper('Sale - PO:DS') , 'Sale','Sale');
insert into adjustment_type  (adjustment_typeid,name,othername) values (upper('Sale Cancel - PO:DS') , 'Sale','Sale');
insert into adjustment_type  (adjustment_typeid,name,othername) values (upper('Sale Cancel - SA:DS') , 'Sale', 'Sale');
insert into adjustment_type  (adjustment_typeid,name,othername) values ('CASH SALE' , 'Sale','Sale');
insert into adjustment_type  (adjustment_typeid,name,othername) values ('CITY' , 'Transferout','Transferout');
insert into adjustment_type  (adjustment_typeid,name,othername) values ('BA' , 'Transferout','Transferout');
insert into adjustment_type  (adjustment_typeid,name,othername) values ('TLIVO' , 'Transferout', 'Transferout');
insert into adjustment_type  (adjustment_typeid,name,othername) values ('TO' , 'Transferout', 'Transferout');
insert into adjustment_type  (adjustment_typeid,name,othername) values ('TOP' , 'Transferout','Transferout');
insert into adjustment_type  (adjustment_typeid,name,othername) values ('TRANS' , 'Transferout','Transferout');
insert into adjustment_type  (adjustment_typeid,name,othername) values ('TTR' , 'Transferout', 'Transferout');
insert into adjustment_type  (adjustment_typeid,name,othername) values ('PDB' , 'Transferin','Transferin');
insert into adjustment_type  (adjustment_typeid,name,othername) values ('TFLIV' , 'Transferin', 'Transferin');
insert into adjustment_type  (adjustment_typeid,name,othername) values ('TI' , 'Transferin','Transferin');
insert into adjustment_type  (adjustment_typeid,name,othername) values ('TOPB' , 'Transferin','Transferin');
insert into adjustment_type  (adjustment_typeid,name,othername) values ('TH' , 'Theft', 'Theft');
insert into adjustment_type  (adjustment_typeid,name,othername) values ('WO' , 'Writeoff','Writeoff');
