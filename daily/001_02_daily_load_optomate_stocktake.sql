----------- optomate stocktake
set role scire;
SET search_path = public, stage, pg_catalog;

INSERT INTO public.stocktake
( nk,
store_sk, 
planned_stocktake_start_date_time, 
planned_stocktake_end_date_time, 
actual_stocktake_start_date_time, 
actual_stocktake_end_date_time, 
created_user, 
created_date_time, 
modified_user, 
modified_date_time, 
created)
select os."source" || '-' || os."STOCKTAKE_ID",s.sk,os."START_DATE_TIME"::timestamp ,os."FINISH_DATE_TIME"::timestamp,os."START_DATE_TIME"::timestamp,os."FINISH_DATE_TIME"::timestamp,os."USER_ADDED1",now(),os."USER_ADDED1", now(), now()
-- select *
FROM stage.today_gnm_optomate_v_stocktake_log os
 inner join store s on  s.store_nk = os."source" -- || '-'  || os."BRANCH_IDENTIFIER"
 left join stocktake stk on stk.nk = os."source" || '-' || os."STOCKTAKE_ID"
where 
	stk.sk is null
group by os."source" || '-' || os."STOCKTAKE_ID",s.sk,os."START_DATE_TIME" ,os."FINISH_DATE_TIME",os."START_DATE_TIME",os."FINISH_DATE_TIME",os."USER_ADDED1"


--- hack version for dave
/*
INSERT INTO public.stocktake
( nk,
store_sk, 
planned_stocktake_start_date_time, 
planned_stocktake_end_date_time, 
actual_stocktake_start_date_time, 
actual_stocktake_end_date_time, 
created_user, 
created_date_time, 
modified_user, 
modified_date_time, 
created)
select os."STOCKTAKE_ID",s.sk,os."START_DATE_TIME"::timestamp ,os."FINISH_DATE_TIME"::timestamp,os."START_DATE_TIME"::timestamp,os."FINISH_DATE_TIME"::timestamp,os."USER_ADDED1",now(),os."USER_ADDED1", now(), now()
-- select *
FROM stage.today_gnm_optomate_v_stocktake_log os
 inner join store s on  s.store_nk = '0000-001' -- || '-'  || os."BRANCH_IDENTIFIER"
 left join stocktake stk on stk.nk = os."STOCKTAKE_ID"
where 
	stk.sk is null and source = '0000-000'
group by os."STOCKTAKE_ID",s.sk,os."START_DATE_TIME" ,os."FINISH_DATE_TIME",os."START_DATE_TIME",os."FINISH_DATE_TIME",os."USER_ADDED1"
*/

/* 
select os."STOCKTAKE_ID",os."FINISH_DATE_TIME" , count(*)
from stage.today_gnm_optomate_v_stocktake_log os group by  os."STOCKTAKE_ID",os."FINISH_DATE_TIME" 

select  "STOCKTAKE_ID", count(*) from stage.today_gnm_optomate_v_stocktake_log st
group by "STOCKTAKE_ID",


----------------------------------------------------------------------------------
select os."source" || '-'  || os."BRANCH_IDENTIFIER",* from stage.today_gnm_optomate_v_stocktake_log os
inner join store s on s.store_nk = os."source" || '-'  || os."BRANCH_IDENTIFIER";

*/

----------------------------------------------------------------------------------
-- insert stock items for GNM products

INSERT INTO public.stocktake_item
(
	nk,
	stocktake_sk, 
	product_gnm_sk, 
	store_product_sk, 
	expected_qty, 
	actual_qty, 
	diff,
	avg_cost_incl_gst, 
	avg_cost_gst,
	pms_user,
	last_purchased_date, 
	created_user, 
	created_date_time, 
	modified_user, 
	modified_date_time, 
	created
)
select 
	 os."source" || '-' || os."ID" as nk,
	stk.sk as stocktake_sk,
	sp.product_gnm_sk,
	sp.sk as store_product_sk,
	os."PRE_COUNT_QOH"::integer as expected_qty,
	os."POST_COUNT_QOH"::integer as actual_qty,
	os."PRE_COUNT_QOH"::integer -  os."POST_COUNT_QOH"::integer as diff,
	null as avg_cost_incl_gst,
	null as avg_cost_gst,
	os."USER_ADDED" as pms_user,
	os."LAST_PURCHASED"::timestamp as last_purchased_date,
	os."USER_ADDED" as created_user,
	os."DATE_ADDED"::timestamp as created_date_time,
	os."USER_ADDED1"as modified_user,
	os."DATE_EDITED1"::timestamp as created_date_time,
	now()
	-- select  os."GM_PRODUCT_ID" ,sp.internal_sku ,s.storeid,sp.storeid ,* 
FROM stage.today_gnm_optomate_v_stocktake_log os
 inner join store s on  s.store_nk = os."source"
 inner join storeproduct sp on  
	os."GM_PRODUCT_ID" = sp.internal_sku and s.storeid = sp.storeid and internal_sku is not null and os."GM_PRODUCT_ID" is not null and internal_sku <> '' and os."GM_PRODUCT_ID" <> ''
 inner join stocktake stk on stk.nk = os."source" || '-' || os."STOCKTAKE_ID"
 left join stocktake_item si on si.nk = os."source" || '-' || os."ID" 
 where si.sk is null;


----------------------------------------------------------------------------------
--- insert stock items for NON GNM Products

INSERT INTO public.stocktake_item
(
	nk,
	stocktake_sk, 
	product_gnm_sk, 
	store_product_sk, 
	expected_qty, 
	actual_qty, 
	diff,
	avg_cost_incl_gst, 
	avg_cost_gst,
	pms_user,
	last_purchased_date, 
	created_user, 
	created_date_time, 
	modified_user, 
	modified_date_time, 
	created
)
select 
	 os."source" || '-' || os."ID" as nk,
	stk.sk as stocktake_sk,
	sp.product_gnm_sk,
	sp.sk as store_product_sk,
	os."PRE_COUNT_QOH"::integer as expected_qty,
	os."POST_COUNT_QOH"::integer as actual_qty,
	os."PRE_COUNT_QOH"::integer -  os."POST_COUNT_QOH"::integer as diff,
	null as avg_cost_incl_gst,
	null as avg_cost_gst,
	os."USER_ADDED" as pms_user,
	os."LAST_PURCHASED"::timestamp as last_purchased_date,
	os."USER_ADDED" as created_user,
	os."DATE_ADDED"::timestamp as created_date_time,
	os."USER_ADDED1"as modified_user,
	os."DATE_EDITED1"::timestamp as created_date_time,
	now()
	-- select  os."GM_PRODUCT_ID" ,* 
FROM stage.today_gnm_optomate_v_stocktake_log os
 inner join store s on  s.store_nk = os."source"
 inner join stocktake stk on stk.nk = os."source" || '-' || os."STOCKTAKE_ID"
 left join stocktake_item si on si.nk = os."source" || '-' || os."ID" 
 inner join storeproduct sp on  
	sp.productid = s.storeid || '-PF-' || os."BARCODE1" and os."BARCODE1" is not null and os."BARCODE1"  <> '' 
where  si.sk is null;

/*
 hack import for dave
 
 INSERT INTO public.stocktake_item
(
	nk,
	stocktake_sk, 
	product_gnm_sk, 
	store_product_sk, 
	expected_qty, 
	actual_qty, 
	diff,
	avg_cost_incl_gst, 
	avg_cost_gst,
	pms_user,
	last_purchased_date, 
	created_user, 
	created_date_time, 
	modified_user, 
	modified_date_time, 
	created
)
select 
	os."ID" as nk,
	stk.sk as stocktake_sk,
	sp.product_gnm_sk,
	sp.sk as store_product_sk,
	os."PRE_COUNT_QOH"::integer as expected_qty,
	os."POST_COUNT_QOH"::integer as actual_qty,
	os."PRE_COUNT_QOH"::integer -  os."POST_COUNT_QOH"::integer as diff,
	null as avg_cost_incl_gst,
	null as avg_cost_gst,
	os."USER_ADDED" as pms_user,
	os."LAST_PURCHASED"::timestamp as last_purchased_date,
	os."USER_ADDED" as created_user,
	os."DATE_ADDED"::timestamp as created_date_time,
	os."USER_ADDED1"as modified_user,
	os."DATE_EDITED1"::timestamp as created_date_time,
	now()
	-- select  os."GM_PRODUCT_ID" ,sp.internal_sku ,s.storeid,sp.storeid ,* 
FROM stage.today_gnm_optomate_v_stocktake_log os
 inner join store s on  s.store_nk = '0000-001' -- || '-'  || os."BRANCH_IDENTIFIER"
inner join storeproduct sp on  
	os."GM_PRODUCT_ID" = sp.internal_sku and s.storeid = sp.storeid and internal_sku is not null and os."GM_PRODUCT_ID" is not null and internal_sku <> '' and os."GM_PRODUCT_ID" <> ''
 inner join stocktake stk on stk.nk = os."STOCKTAKE_ID"
where 
	-- stk.sk is null and 
	source = '0000-000' ;
	
	
	select * from storeproduct;
	
 select * FROM stage.today_gnm_optomate_v_stocktake_log os where "GM_PRODUCT_ID" is not null and "GM_PRODUCT_ID" <> '';
 select os. FROM stage.today_gnm_optomate_v_stocktake_log os 
 
 
select * from storeproduct where storeid = '0000-001'
	select * from storeproduct;
	
 select * FROM stage.today_gnm_optomate_v_stocktake_log os where "GM_PRODUCT_ID" is not null and "GM_PRODUCT_ID" <> '';
 select os."BARCODE1" FROM stage.today_gnm_optomate_v_stocktake_log os 
 
 
select * from storeproduct where storeid = '0000-001'

select sp.productid, *
FROM stage.today_gnm_optomate_v_stocktake_log os
inner join store s on  s.store_nk = '0000-001' -- || '-'  || os."BRANCH_IDENTIFIER"
inner join storeproduct sp on  
	sp.productid = s.storeid || '-PF-' || os."BARCODE1" and os."BARCODE1" is not null and os."BARCODE1"  <> ''
 inner join stocktake stk on stk.nk = os."STOCKTAKE_ID"
where 
	-- stk.sk is null and 
	source = '0000-000' ;
	

 */


