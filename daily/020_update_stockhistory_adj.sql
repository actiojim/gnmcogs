
set role scire;
SET search_path = public, pg_catalog;


---- make sure all adjustment dates are set
update store_stockmovement sm set adjustmentdate_sk = dd.sk
from d_date dd
where dd.date_actual = TO_CHAR((sm.adjustmentdate::timestamp),'yyyymmdd')::date
and adjustmentdate_sk is null ;


-----------------------------------------------------------------------------------------------------------------------------------------------
/*

--- Recursive view for stock history
drop view if exists store_stock_history_view;

create view store_stock_history_view as
with recursive prior_total_adjustments as (

	select seq,store_sk,storeproduct_sk,product_gnm_sk,
	date_actual,
	(select takeon_total from store_stock_history_adj sh inner join store s on sh.store_sk = s.sk and s.acquisition_date = sh.source_staged where sh.storeproduct_sk = storeproduct_sk limit 1)
	as takeon_total, 
	dailytotal as cumulative_total, 
	0 as prior_total,
	case when (product_gnm_sk = 0 and date_actual > s.acquisition_date) then dailytotal else 0 end as offcat_total,
	s.acquisition_date
	from daily_adjustments_sum 
	inner join store s on store_sk = s.sk  
	where seq = 1
	
union all

	select ds.seq,ds.store_sk,ds.storeproduct_sk,ds.product_gnm_sk,
	ds.date_actual,
	psm.takeon_total, 
	psm.cumulative_total + ds.dailytotal, 
	psm.cumulative_total::integer as prior_total,
	case when (ds.product_gnm_sk = 0 and ds.date_actual > acquisition_date) then psm.cumulative_total + ds.dailytotal else 0 end as offcat_total,
	acquisition_date
	from prior_total_adjustments psm 
	inner join daily_adjustments_sum ds on 
		ds.store_sk = psm.store_sk and ds.storeproduct_sk = psm.storeproduct_sk and ds.product_gnm_sk = psm.product_gnm_sk
	where psm.seq + 1 = ds.seq
	
)
select seq, store_sk, storeproduct_sk, product_gnm_sk, date_actual, takeon_total, cumulative_total, prior_total, offcat_total from prior_total_adjustments order by date_actual;


*/


-----------------------------------------------------------------------------------------------------------------------------------------------
---- Deciding when to start accumulating from which record will dictate the correctness of the final total e.g. stocktake or receipt

--- needs to do an update if the movements have changed for a day
insert into daily_adjustments_sum(store_sk, 
	storeproduct_sk,
	product_gnm_sk,
	date_actual,
	dailytotal,
	seq)
select sm.store_sk, 
	sm.storeproduct_sk, 
	coalesce(sm.product_gnm_sk,0) 
	product_gnm_sk, 
	dd.date_actual, 
	sum(adjustment) dailytotal, 
	row_number() over (partition by sm.store_sk,sm.storeproduct_sk order by dd.date_actual ) r 
from store_stockmovement sm
inner join d_date dd on dd.sk = sm.adjustmentdate_sk
left join daily_adjustments_sum da on da.store_sk = sm.store_sk and sm.storeproduct_sk = da.storeproduct_sk and dd.date_actual = da.date_actual
where sm.store_sk is not null and da.store_sk is null
group by sm.store_sk, sm.storeproduct_sk, coalesce(sm.product_gnm_sk,0), dd.date_actual;

--- persist cumulative daily totals
insert into store_stock_history_adj (store_sk,storeproduct_sk,takeon_total,total,prior_total,offcatalogue_reciepted_total, source_staged,created,product_gnm_sk)
select sshv.store_sk, sshv.storeproduct_sk,sshv.takeon_total, sshv.cumulative_total,sshv.prior_total,sshv.offcat_total,sshv.date_actual,sshv.date_actual,sshv.product_gnm_sk 
from store_stock_history_view sshv 
left join store_stock_history_adj ssha on ssha.storeproduct_sk = sshv.storeproduct_sk and ssha.store_sk = sshv.store_sk and ssha.source_staged = sshv.date_actual
and ssha.takeon_total = sshv.takeon_total and ssha.total = sshv.cumulative_total and ssha.prior_total = sshv.prior_total and  ssha.offcatalogue_reciepted_total = sshv.offcat_total
where ssha.sk is null

--- select * from store_stock_history_adj order by source_staged desc

