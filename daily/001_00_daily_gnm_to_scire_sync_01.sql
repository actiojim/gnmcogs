set role scire;
SET search_path = public, pg_catalog;


--------------------------------------------------------------------------
---- insert store product from gnm, should make this historical, with view to current product

insert into public.product_gnm
(productid, "type", 
datecreated, modified, 
"name", description, internal_sku, distributor,apn,brand_name,consignment)
SELECT product_id, p."type", 
created_date_time::TimeSTAMP without time zone, modified_date_time::TimeSTAMP without time zone,
p."name",  p.description, p.supplier_sku, 0, p.apn, p.brandname, p.consignment::boolean
FROM stage.today_gnm_db_product p
left join product_gnm pg on pg.productid = p.product_id
where pg.sk is null;

---- need to add an update statement for changed fields


/*
select count(*) from (
SELECT product_id, "type", 
created_date_time, modified_date_time,
p."name",  description, supplier_sku, 0,apn,brandname
FROM stage.today_gnm_db_product p
--inner join public.supplier s on s.externalref = p.supplier_id;
) x;

select count(*) from (
SELECT * from 
product_gnm
) x;

*/


---- missing update of store product ---------------------------------------
-- DROP INDEX public.idx_stock_price_history_nk;
-------- insert stock price history ----------------------------------------
INSERT INTO stock_price_history
(store_sk, nk, storeproduct_sk, product_gnm_sk, 
supplier_cost, supplier_cost_gst, 
actual_cost, actual_cost_gst, 
supplier_rrp, supplier_rrp_gst, calculated_retail_price, calculated_retail_price_gst, 
override_retail_price, override_retail_price_gst, 
retail_price, retail_price_gst, 
created)
-- VALUES(nextval('stock_price_history_sk_seq'::regclass), 0, '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, now());
SELECT 
0, id, 0, sp.sk, 
pch.supplier_cost::numeric, pch.supplier_cost_gst::numeric, 
pch.actual_cost::numeric, pch.actual_cost_gst::numeric, 
pch.supplier_rrp::numeric, pch.supplier_rrp_gst::numeric, pch.calculated_retail_price::numeric, pch.calculated_retail_price_gst::numeric, 
0,0,
0,0,
pch.modified_date_time::TimeSTAMP without time zone
FROM stage.today_gnm_db_cost_history pch 
left join product_gnm sp on sp.productid = pch.product_id
left join stock_price_history sph on sph.created = pch.modified_date_time::TimeSTAMP without time zone and sp.sk = sph.product_gnm_sk
where sph.sk is null;


--- Stock price history clean up

-- set default end date in the future
update stock_price_history set end_date = '2030-01-01' where end_date is null;

-- calculate date range from history
update public.stock_price_history set end_date = Z.end_date
from (select x.sk,x1.created end_date,x1.product_gnm_sk from 
(select sk,storeproduct_sk, product_gnm_sk,created, row_number() over (partition by product_gnm_sk order by sh.created,sh.sk) r
from stock_price_history sh where product_gnm_sk <> 0) x 
inner join
(select sk,storeproduct_sk, product_gnm_sk,created, row_number() over (partition by product_gnm_sk order by sh.created,sh.sk) r
from stock_price_history sh where product_gnm_sk <> 0) x1 on x.product_gnm_sk = x1.product_gnm_sk and x.r+1 = x1.r
order by x.product_gnm_sk) Z 
where Z.sk = stock_price_history.sk;

---- 
-- process the orders
INSERT INTO order_history
(store_sk, 
customer_sk,
nk, 
order_type, 
queue, status, 
source_created, source_modified)
SELECT 
coalesce(s.sk, 0), 
coalesce(c.sk, 0),
order_id, 
coalesce(os.order_type_id,'UNKNOWN'), 
os.queue, os.status, 
-- to_timestamp(os.source_created,'YYYY-MM-DD HH24:MI:SS'), to_timestamp(os.source_modified,'YYYY-MM-DD HH24:MI:SS')
os.created_date_time::TimeSTAMP without time zone, os.modified_date_time::TimeSTAMP without time zone
FROM stage.today_gnm_db_order os 
left join "store" s on s.storeid = os.store_id
left join customer c on c.firstname = os.first_name and c.lastname = os.last_name and c.storesk = s.sk
left join order_history oh on oh.nk = os.order_id
where 
	oh.nk is null;
	
	
	-- select * from stage.today_gnm_db_order os 
	

INSERT INTO public.job_history
(nk, order_id, 
order_sk, 
job_type, 
product_sk, supplier_sk, 
queue, status, 
frame_glazing_action,
zeiss_glazing_action,
source_created, source_modified)
select
js.job_id, js.order_id,
-- order sk
oh.sk,
js.job_type_id,
coalesce(p.sk,0) as productsk, coalesce(s.sk,0) as storesk,
js.queue, js.status,
js.glazing_action,
case when  glazing_action = '5' then '3' else glazing_action end,
js.created_date_time::TimeSTAMP without time zone, js.modified_date_time::TimeSTAMP without time zone
-- select *
from stage.today_gnm_db_job js 
inner join order_history oh on oh.nk = js.order_id
inner join public.supplier s on CAST(s.externalref as varchar(100) ) = js.supplier_id
inner join public.product_gnm p on js.product_id =   p.productid --CAST( p.productid as varchar(100))
left join public.job_history jh on jh.nk = js.job_id
where 
	jh.sk is null;

-----------------------------------------------------------

--- make sure storeproduct points to the product_gnm where they match
update storeproduct as tp set product_gnm_sk = gp.sk 
from  public.product_gnm gp 
where tp.internal_sku <> '' AND upper(gp.productid) = upper(LPAD(tp.internal_sku,8,'0'));


update storeproduct as tp set product_gnm_sk = 0  where product_gnm_sk is null;






