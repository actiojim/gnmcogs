----------- 
set role scire;
SET search_path = public, pg_catalog;

INSERT INTO public.stocktake
( nk,
store_sk, 
planned_stocktake_start_date_time, 
planned_stocktake_end_date_time, 
actual_stocktake_start_date_time, 
actual_stocktake_end_date_time, 
created_user, 
created_date_time, 
modified_user, 
modified_date_time, 
created)
SELECT filename,s.sk,maxdate,maxdate,maxdate,maxdate,'system', now(), 'system', now(), now()
FROM stage.today_gnm_sunix_vfst st
inner join (select max(to_timestamp(substring(filename,4,10) || ' ' || overlay( replace(substring(filename,15,11),'-',':')  placing ' ' from 9 for 1 ) , 'DD-MM-YYYY HH12:MI:SS AM')) maxdate
 from stage.today_gnm_sunix_vfst ) x on x.maxdate = to_timestamp(substring(filename,4,10) || ' ' || overlay( replace(substring(filename,15,11),'-',':')  placing ' ' from 9 for 1 ) , 'DD-MM-YYYY HH12:MI:SS AM')
 inner join store s on st."source" = s.storeid
 left join stocktake stk on stk.nk = st.filename
where 
	stk.sk is null
group by st.filename, maxdate,s.sk;

INSERT INTO public.stocktake_item
( 
	nk,
	stocktake_sk, 
	product_gnm_sk, 
	store_product_sk, 
	expected_qty, 
	actual_qty, 
	diff,
	avg_cost_incl_gst, 
	avg_cost_gst,
	pms_user,
	last_purchased_date, 
	created_user, 
	created_date_time, 
	modified_user, 
	modified_date_time, 
	created
)
-- VALUES(nextval('stocktake_item_sk_seq'::regclass), '', 0, 0, 0, 0, 0, 0, 0, '', '', 'system'::character varying,timezone('utc'::text, now()), 'system'::character varying, timezone('utc'::text, now()), timezone('utc'::text, now()));
select "FRAMENUM",st.sk,sp.product_gnm_sk,sp.sk,case when x."QTY" = '' then null else x."QTY"::integer end,x."QTYINSTK"::integer ,
x."DIFF"::integer,x."AVGCOST"::numeric,null,'system',null,'system',now(),'system',now(),now()
--select sp.sk,x.*
from storeproduct sp 
inner join stage.today_gnm_sunix_vfst x on  x."FRAMENUM" = sp."description" and x.source = sp.storeid
inner join stocktake st on st.nk = x.filename 
left join stocktake_item sti on sti.stocktake_sk = st.sk and sti.nk = "FRAMENUM"
where  "FRAMENUM" <> ''  and sp."description" <> '' and sti.sk is null;





