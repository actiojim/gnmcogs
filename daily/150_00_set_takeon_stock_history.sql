
set role scire;
SET search_path = public, pg_catalog;


-- truncate table store_stock_history_adj;
--- update acquisition date totals    

insert into store_stock_history_adj(
	store_sk,
	storeproduct_sk,
	takeon_total,
	total,
	prior_total,
	source_staged,
	created,
	product_gnm_sk)
select store_sk, 
	storeproduct_sk,
	cumulative_total takeon_total,
	cumulative_total,
	prior_total,
	date_actual,
	date_actual,
	product_gnm_sk 
from store_stock_history_view hv
inner join store s on s.sk = hv.store_sk
-- where hv.date_actual < s.acquisition_date
where hv.date_actual < '2017-10-01'

