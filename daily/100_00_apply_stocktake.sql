
set role scire;
SET search_path = public, stage, pg_catalog;

insert into store_stockmovement 
(store_sk,
nk,
storeproduct_sk,
adjustmentdate,
adjustmentdate_sk,
product_gnm_sk,
adjustment_type_sk,
stock_price_history_sk,
adjustment,
reason,
stocktake_sk,
stocktake_item_sk) 
select s.store_sk, 
('SYS-' || si.sk) as nk, 
si.store_product_sk, 
s.actual_stocktake_end_date_time,
d.sk as adjustmentdate_sk,
si.product_gnm_sk, 
a.sk, 
coalesce((select sk from  stock_price_history sph where sph.storeproduct_sk = si.store_product_sk 
	-- and sph.created >= s.actual_stocktake_end_date_time and sph.end_date < s.actual_stocktake_end_date_time 
	limit 1),0) as sph_sk, 
si.expected_qty*-1 as adjustment, 
a.othername,
si.stocktake_sk,
si.sk as stocktake_item_sk
-- select  sm.sk, sm.adjustment_type_sk, a.sk,  *
from stocktake_item si 
inner join stocktake s on s.sk = si.stocktake_sk
inner join adjustment_type a on a.name = 'Stocktake Out'
left join store_stockmovement sm on sm.adjustment_type_sk = a.sk and sm.stocktake_item_sk = si.sk 
left join d_date d on (TO_CHAR((s.actual_stocktake_end_date_time::timestamp),'yyyymmdd')::INT = d.date_dim_id)
where s.state = 'APPLY_STOCKTAKE' and si.product_gnm_sk is not null
 and sm.sk is null;

insert into store_stockmovement 
(store_sk,
nk,
storeproduct_sk,
adjustmentdate,
adjustmentdate_sk,
product_gnm_sk,
adjustment_type_sk,
stock_price_history_sk,
adjustment,
reason,
stocktake_sk,
stocktake_item_sk) 
select s.store_sk, ('SYS-' || si.sk) as nk, si.store_product_sk, s.actual_stocktake_end_date_time,
d.sk as adjustmentdate_sk,
si.product_gnm_sk, 
a.sk, 
coalesce((select sk from  stock_price_history sph where sph.storeproduct_sk = si.store_product_sk 
	-- and sph.created >= s.actual_stocktake_end_date_time and sph.end_date < s.actual_stocktake_end_date_time 
	limit 1),0) as sph_sk, 
si.actual_qty, 
a.othername,
si.stocktake_sk,
si.sk as stocktake_item_sk
-- select *
from stocktake_item si 
inner join stocktake s on s.sk = si.stocktake_sk
inner join adjustment_type a on a.name = 'Stocktake Reset'
left join store_stockmovement sm on sm.adjustment_type_sk = a.sk and sm.stocktake_item_sk = si.sk 
left outer join d_date d on (TO_CHAR((s.actual_stocktake_end_date_time::timestamp),'yyyymmdd')::INT = d.date_dim_id)
where s.state = 'APPLY_STOCKTAKE' and si.product_gnm_sk is not null
 and sm.sk is null;

insert into store_stockmovement 
(store_sk,
nk,
storeproduct_sk,
adjustmentdate,
adjustmentdate_sk,
product_gnm_sk,
adjustment_type_sk,
stock_price_history_sk,
adjustment,
reason,
stocktake_sk,
stocktake_item_sk) 
select s.store_sk,  
('SYS-' || si.sk) as nk, 
si.store_product_sk, 
s.actual_stocktake_end_date_time,
d.sk as adjustmentdate_sk,
si.product_gnm_sk, 
a.sk, 
coalesce((select sk from  stock_price_history sph where sph.storeproduct_sk = si.store_product_sk 
	-- and sph.created >= s.actual_stocktake_end_date_time and sph.end_date < s.actual_stocktake_end_date_time 
	limit 1),0) as sph_sk, 
	si.actual_qty, 
	a.othername,
si.stocktake_sk,
si.sk as stocktake_item_sk
-- select *
from stocktake_item si 
inner join stocktake s on s.sk = si.stocktake_sk
inner join adjustment_type a on a.adjustment_typeid = 'STOCKTAKE_IN'
left join store_stockmovement sm on sm.adjustment_type_sk = a.sk and sm.stocktake_item_sk = si.sk 
left outer join d_date d on (TO_CHAR((s.actual_stocktake_end_date_time::timestamp),'yyyymmdd')::INT = d.date_dim_id)
where s.state = 'APPLY_STOCKTAKE' and si.product_gnm_sk is not null
 and sm.sk is null;


-- select * from adjustment_type order by sk desc
-- select * from stocktake order by sk desc
-- rollback;

