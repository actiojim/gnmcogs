----------- save into a table of cogs averages, update end_date ranges for the table
set role scire;
SET search_path = public, pg_catalog;

insert into product_gnm_average_cost_history_reset (   
 	storeproduct_sk,
 	product_gnm_sk,
 	store_sk,
    adjustmentdate,
    store_stockmovement_sk,
 	quantity, running_total,
	prior_avg_cost, adjustment,

    actual_cost , actual_cost_gst,
    average_cost, average_cost_gst
    )
select vah.storeproduct_sk,vah.product_gnm_sk,
	vah.store_sk,
	vah.adjustmentdate,
	vah.sk,
	vah.quantity,vah.running_total,
	vah.prior_avg_cost,vah.adjustment,
	vah.actual_cost,vah.actual_cost_gst,
	vah.average_cost,vah.average_cost_gst
from view_product_gnm_average_cost_history_reset vah 
left join product_gnm_average_cost_history_reset ph on ph.store_stockmovement_sk = vah.sk
where 
	ph.store_stockmovement_sk is null;
	
--select * from product_gnm_average_cost_history;
---------------------------------------------------------------
-- set the end date
update product_gnm_average_cost_history_reset set end_date = '2030-01-01' where end_date is null;

--- date ranges for product_gnm_average_cost_history
--begin;
update public.product_gnm_average_cost_history_reset set end_date = Z.end_date
-- select * 
from 
(select x.sk,x1.adjustmentdate end_date,x1.product_gnm_sk 
	from 
		(select sk,store_sk, product_gnm_sk,adjustmentdate, row_number() over (partition by store_sk,product_gnm_sk order by sh.adjustmentdate,sh.sk) r
			from product_gnm_average_cost_history_reset sh where product_gnm_sk <> 0) x 
		inner join (select sk,store_sk, product_gnm_sk,adjustmentdate, row_number() over (partition by store_sk,product_gnm_sk order by sh.adjustmentdate,sh.sk) r
			from product_gnm_average_cost_history_reset sh where product_gnm_sk <> 0) x1
				on x.product_gnm_sk = x1.product_gnm_sk and x.r+1 = x1.r and x.store_sk = x1.store_sk
	order by x.product_gnm_sk) Z 
				where Z.sk = product_gnm_average_cost_history_reset.sk;

----------------------------------------------------------------------------------
