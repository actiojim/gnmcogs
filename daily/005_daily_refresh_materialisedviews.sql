-- set role postgres;
set role scire;
SET search_path = public, pg_catalog;

--REFRESH MATERIALIZED view invoice_orders_frame;
--REFRESH MATERIALIZED view last_product_price;
--REFRESH MATERIALIZED view last_gnm_product_price;
--REFRESH MATERIALIZED view last_takeon_product_price;
--REFRESH MATERIALIZED view minvoice_orders_frame;
REFRESH MATERIALIZED view view_invoiceitem_cogs;
REFRESH MATERIALIZED view store_stockmovement_costs;

--- this needs to be changed to scire
set role postgres;
REFRESH MATERIALIZED view v_store_stock_latest_off_cat;
REFRESH MATERIALIZED view v_store_stock_latest_on_cat;
REFRESH MATERIALIZED view v_store_stock_movement_cost;

/*
select * from stage.today_gnm_sunix_vcode


select * from stage.today_gnm_sunix_vframe;


select * from stage.today_gnm_sunix_vfrstake;


select * from stage.today_gnm_sunix_vrestock;
 
storeproduct

product_gnm

product_gnm_average_cost_history

product_gnm_average_cost_history_reset

*/







